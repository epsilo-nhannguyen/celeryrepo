from celery import Celery, states
from celery.schedules import crontab
import subprocess
import os

dirname = os.path.dirname(__file__)
app = Celery('tasks', broker='pyamqp://guest@localhost//')
# app = Celery('tasks', backend='rpc://', broker='pyamqp://guest@localhost//')

# Cron tab normal job
@app.task(bind=True, max_retries=1)
def normal(self):
    try:
        proc = subprocess.check_output("php " + dirname + "/artisan index", shell=True, stderr=subprocess.STDOUT)
        print(proc)
    except subprocess.CalledProcessError as e:
        print('Retry')
        self.update_state(
            state = states.FAILURE,
            meta = 'REASON FOR FAILURE'
        )
        # ignore the task so no other state is recorded
        raise self.retry(countdown= 2 ** self.request.retries)

# Cron tab push queue job
@app.task(bind=True, max_retries=1)
def pushQueue(self):
    try:
        proc = subprocess.check_output("php " + dirname + "/artisan test-celery", shell=True, stderr=subprocess.STDOUT)
        print(proc)
    except subprocess.CalledProcessError as e:
        self.update_state(
            state = states.FAILURE,
            meta = 'REASON FOR FAILURE'
        )
        # ignore the task so no other state is recorded
        raise self.retry(countdown= 2 ** self.request.retries)

# Process job in queue
@app.task(bind=True, max_retries=1)      
def processJobInQueue(self, param1, param2):
    print("=======First param=======")
    print(param1)
    print("=======Second param=======")
    print(param2)
    print("=======Build command string=======")
    stringParams = " " + str(param1) + " " + str(param2)
    print("php " + dirname + "/artisan job:dispatch TestCeleryJob" + stringParams) 
     
    try:
        stringParams = " " + str(param1) + " " + str(param2)
        commandString = "php " + dirname + "/artisan job:dispatch TestCeleryJob" + stringParams
        proc = subprocess.check_output(commandString, shell=True, stderr=subprocess.STDOUT)
        print('=======Job Result=======')
        print(proc)
    except subprocess.CalledProcessError as e:
        self.update_state(
            state = states.FAILURE,
            meta = 'REASON FOR FAILURE'
        )
        # ignore the task so no other state is recorded
        raise self.retry(countdown= 2 ** self.request.retries)

# Celery crontab
app.conf.beat_schedule = {
    'normal': {
        'task': 'tasks.normal',
        # 'schedule': 5,
        'schedule': crontab(),
    },
    'pushQueue': {
        'task': 'tasks.pushQueue',
        # 'schedule': 10,
        'schedule': crontab(),
    }
}

