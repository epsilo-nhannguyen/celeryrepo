import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import numpy as np
from pandas import read_csv
from datetime import datetime
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Embedding
from keras.layers.wrappers import TimeDistributed
import math
from sklearn.metrics import mean_squared_error

columns = ["Keyword", "SKU_observe", "Time", "Prices", "Position","Position_SKU1", "Position_SKU2", "Position_SKU3",  "Position_SKU4", "Position_SKU5", "Position_SKU6", "Position_SKU7", "Position_SKU8", "Position_SKU9", "Position_SKU10"]
dataset = pd.read_csv('%nameData%', header=None, names=columns, usecols=["Time","Position", "Prices", "Position_SKU1", "Position_SKU2", "Position_SKU3", "Position_SKU4", "Position_SKU5", "Position_SKU6", "Position_SKU7", "Position_SKU8", "Position_SKU9", "Position_SKU10"], index_col=0)
dataset.fillna(21,inplace=True)

def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = pd.DataFrame(data)
    cols, names = list(), list()
# input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))        
        names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
# forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
# put it all together
    agg = pd.concat(cols, axis=1)
    agg.columns = names
# drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg
    
 
values = dataset.values
encoder = LabelEncoder()
values = values.astype("float32")
scaler = MinMaxScaler(feature_range=(0, 1))
scaler.fit(values)
scaled = scaler.transform(values)
reframed = series_to_supervised(scaled, 1, 1) 
reframed.drop(reframed.columns[[13,14,15,16,17,18,19,20,21,22,23]], axis=1, inplace=True)

values = reframed.values
n_train_day = dataset.shape[0]-100
train = values[:n_train_day, :]
test = values[n_train_day:, :]

train_X, train_y = train[:, :-1], train[:, -1]
test_X, test_y = test[:, :-1], test[:, -1]

train_X = train_X.reshape((train_X.shape[0], 1, train_X.shape[1]))
test_X = test_X.reshape((test_X.shape[0], 1, test_X.shape[1]))

model = Sequential()
model.add(LSTM(50, input_shape=(train_X.shape[1], train_X.shape[2]))) # 1 sample has 8 feature
model.add(Dense(1))
model.compile(loss='mae', optimizer='adam')
# history = model.fit(train_X, train_y, epochs=50, batch_size=72, validation_data=(test_X, test_y), verbose=2, shuffle=False)

# model.summary()

# plot history
#plt.figure(figsize=(10,5))
#plt.plot(history.history['loss'], label='train')
#plt.plot(history.history['val_loss'], label='test')
#plt.xlabel("Number step")
#plt.ylabel("Price")
#plt.legend()
#plt.show()

# Test value_true and value_predict
# Invert scaling for value_predict actual
y_predict_test = model.predict(test_X)
test_X_now = test_X.reshape((test_X.shape[0], test_X.shape[2]))
inv_y_predict_test = np.concatenate((y_predict_test, test_X_now[:, 1:]), axis=1)
inv_y_predict_test = scaler.inverse_transform(inv_y_predict_test)
inv_y_predict_test = inv_y_predict_test[:,0]
# Invert scaling for value_true actual
test_y = test_y.reshape((len(test_y), 1))
inv_y = np.concatenate((test_y, test_X_now[:, 1:]), axis=1)
inv_y = scaler.inverse_transform(inv_y)
inv_y = inv_y[:,0]
rmse = math.sqrt(mean_squared_error(inv_y, inv_y_predict_test))
#print('Test RMSE: %.3f' % rmse)
#print('Val_loss train')
# model.evaluate(train_X, train_y)
#print('Val_loss test')
# model.evaluate(test_X, test_y)


dataset_new = read_csv('%namePick%', index_col= 0, header=None, names=["Time","Position", "Prices", "Position_SKU1", "Position_SKU2", "Position_SKU3",  "Position_SKU4", "Position_SKU5", "Position_SKU6", "Position_SKU7", "Position_SKU8", "Position_SKU9", "Position_SKU10"])
dataset_new.head()

values_new = dataset_new.values
values_new = values_new.astype('float32')
scaled_new = scaler.transform(values_new)
reframed_new = series_to_supervised(scaled_new, 1, 1)
reframed_new.drop(reframed_new.columns[[13,14,15,16,17,18,19,20,21,22,23]], axis=1, inplace=True)
values_new = reframed_new.values
new_pre = values_new[:, :-1]
new_pre = new_pre.reshape((new_pre.shape[0], 1, new_pre.shape[1]))

yhat_new_pre = model.predict(new_pre)
# invert scaling for actual
yhat_new_pre = yhat_new_pre.reshape((len(yhat_new_pre), 1))
new_pre_now = new_pre.reshape((new_pre.shape[0], new_pre.shape[2]))
yhat_new_pre = np.concatenate((yhat_new_pre, new_pre_now[:, 1:]), axis=1)
yhat_new_pre = scaler.inverse_transform(yhat_new_pre)
yhat_new_pre = yhat_new_pre[:,0]
print(yhat_new_pre)

# dataset_new = dataset_new.drop(dataset_new.index[0])
# new_column = pd.Series(yhat_new_pre, name='predict', index=dataset_new.index)
# print(new_column)