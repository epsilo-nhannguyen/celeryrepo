<?php


use App\Library;
use App\Jobs;


return [

    // Queue Name for Jobs
    'handlerQueue' => [
        Jobs\InitShopData::class => env('QUEUE_NAME_INIT_SHOP_DATA', Library\QueueName::INIT_SHOP_DATA),
        Jobs\RawCategory::class => env('QUEUE_NAME_RAW_CATEGORY', Library\QueueName::RAW_CATEGORY),
        Jobs\RawKeywordProduct::class => env('QUEUE_NAME_RAW_KEYWORD_PRODUCT', Library\QueueName::RAW_KEYWORD_PRODUCT),
        Jobs\RawCrawlerAds::class => env('QUEUE_NAME_RAW_CRAWLER_ADS', Library\QueueName::RAW_CRAWLER_ADS),
        Jobs\RawRankingKeyword::class => env('QUEUE_NAME_RAW_RANKING_KEYWORD', Library\QueueName::RAW_RANKING_KEYWORD),
        Jobs\PullProduct::class => env('QUEUE_NAME_PULL_PRODUCT', Library\QueueName::PULL_PRODUCT),
        Jobs\PullOrder::class => env('QUEUE_NAME_PULL_ORDER', Library\QueueName::PULL_ORDER),
        Jobs\RawKeywordSuggestShopee::class => env('QUEUE_NAME_RAW_KEYWORD_SUGGEST_SHOPEE', Library\QueueName::RAW_KEYWORD_SUGGEST_SHOPEE),
        Jobs\RawKeywordMachineShopee::class => env('QUEUE_NAME_RAW_KEYWORD_MACHINE_SHOPEE', Library\QueueName::RAW_KEYWORD_MACHINE_SHOPEE),
        Jobs\RawKeywordBiddingShopee::class => env('QUEUE_NAME_RAW_KEYWORD_BIDDING_SHOPEE', Library\QueueName::RAW_KEYWORD_BIDDING_SHOPEE),
        Jobs\RawCrawlerShopAdsKeyword::class => env('RAW_CRAWL_SHOP_ADS_KEYWORD', Library\QueueName::RAW_CRAWL_SHOP_ADS_KEYWORD),
        Jobs\RawCrawlerShopAdsKeywordPerformance::class => env('RAW_CRAWL_SHOP_ADS_KEYWORD_PERFORMANCE', Library\QueueName::RAW_CRAWL_SHOP_ADS_KEYWORD_PERFORMANCE),

        #region Tokoepdia
        Jobs\CrawlTokopediaGroupAds::class => env('CRAWL_TOKOPEDIA_GROUP_ADS', Library\QueueName::CRAWL_TOKOPEDIA_GROUP_ADS),
        Jobs\CrawlTokopediaProductAds::class => env('CRAWL_TOKOPEDIA_PRODUCT_ADS', Library\QueueName::CRAWL_TOKOPEDIA_PRODUCT_ADS),
        Jobs\CrawlTokopediaKeywordAds::class => env('CRAWL_TOKOPEDIA_KEYWORD_ADS', Library\QueueName::CRAWL_TOKOPEDIA_KEYWORD_ADS),
        Jobs\CrawlTokopediaGroupAdsPerformance::class => env('CRAWL_TOKOPEDIA_GROUP_ADS_PERFORMANCE', Library\QueueName::CRAWL_TOKOPEDIA_GROUP_ADS_PERFORMANCE),
        Jobs\CrawlTokopediaProductAdsPerformance::class => env('CRAWL_TOKOPEDIA_PRODUCT_ADS_PERFORMANCE', Library\QueueName::CRAWL_TOKOPEDIA_PRODUCT_ADS_PERFORMANCE),
        Jobs\CrawlTokopediaKeywordAdsPerformance::class => env('CRAWL_TOKOPEDIA_KEYWORD_ADS_PERFORMANCE', Library\QueueName::CRAWL_TOKOPEDIA_KEYWORD_ADS_PERFORMANCE),
        Jobs\CrawlTokopediaListProduct::class => env('CRAWL_TOKOPEDIA_LIST_PRODUCT', Library\QueueName::CRAWL_TOKOPEDIA_LIST_PRODUCT),
        #endregion

        'tokopedia_save' => 'tokopedia_save',
    ],

    #region Tiki
    'tiki' => [
        'crawl_data' => 'tiki_crawl_data',
        'save_data' => 'tiki_save_data',
        'import_data' => 'tiki_import_data'
    ],
    #endregion

    /*
    |--------------------------------------------------------------------------
    | Default Queue Driver
    |--------------------------------------------------------------------------
    |
    | The Laravel queue API supports a variety of back-ends via an unified
    | API, giving you convenient access to each back-end using the same
    | syntax for each one. Here you may set the default queue driver.
    |
    | Supported: "null", "sync", "database", "beanstalkd", "sqs", "redis"
    |
    */

    'default' => env('QUEUE_DRIVER', 'redis'),

    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */

    'connections' => [

        'sync' => [
            'driver' => 'sync',
        ],

        'database' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'default',
            'retry_after' => 60,
        ],

        'beanstalkd' => [
            'driver' => 'beanstalkd',
            'host' => 'localhost',
            'queue' => 'default',
            'retry_after' => 60,
        ],

        'sqs' => [
            'driver' => 'sqs',
            'key' => 'your-public-key',
            'secret' => 'your-secret-key',
            'queue' => 'your-queue-url',
            'region' => 'us-east-1',
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => env('QUEUE_REDIS_CONNECTION', 'default'),
            'queue' => 'default',
            'retry_after' => 60,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */

    'failed' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => env('QUEUE_FAILED_TABLE', 'failed_jobs'),
    ],

];
