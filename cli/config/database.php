<?php
return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'master_chart'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */
    'connections' => [
        /*'config' => [
            'driver' => env('DB_CONFIG_DRIVER', 'mysql'),
            'write' => [
                'host' => env('DB_CONFIG_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_CONFIG_WRITE_USERNAME', 'remote'),
                'password' => env('DB_CONFIG_WRITE_PASSWORD', 'Remote@123456'),
            ],
            'read' => [
                'host' => env('DB_CONFIG_READ_HOST', '127.0.0.1'),
                'username' => env('DB_CONFIG_READ_USERNAME', 'remote'),
                'password' => env('DB_CONFIG_READ_PASSWORD', 'Remote@123456'),
            ],
            'port' => env('DB_CONFIG_PORT', '3306'),
            'database' => env('DB_CONFIG_DATABASE', 'buffsell'),
            'unix_socket' => env('DB_CONFIG_SOCKET', ''),
            'charset' => env('DB_CONFIG_CHARSET', 'utf8mb4'),
            'collation' => env('DB_CONFIG_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_CONFIG_PREFIX', ''),
            'strict' => env('DB_CONFIG_STRICT', false),
            'sticky' => env('DB_CONFIG_STICKY', true),
        ],*/

        'master_stat' => [
            'driver' => env('DB_MASTER_DRIVER', 'mysql'),
            'write' => [
                'host' => env('DB_MASTER_STAT_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_STAT_WRITE_USERNAME', 'remote'),
                'password' => env('DB_MASTER_STAT_WRITE_PASSWORD', 'Remote@123456'),
            ],
            'read' => [
                'host' => env('DB_MASTER_STAT_READ_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_STAT_READ_USERNAME', 'remote'),
                'password' => env('DB_MASTER_STAT_READ_PASSWORD', 'Remote@123456'),
            ],
            'port' => env('DB_MASTER_PORT', '3306'),
            'database' => env('DB_MASTER_STAT_DATABASE', 'stat'),
            'unix_socket' => env('DB_MASTER_SOCKET', ''),
            'charset' => env('DB_MASTER_CHARSET', 'utf8mb4'),
            'collation' => env('DB_MASTER_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_MASTER_PREFIX', ''),
            'strict' => env('DB_MASTER_STRICT', false),
            'sticky' => env('DB_MASTER_STICKY', true),
            'options' => [PDO::ATTR_EMULATE_PREPARES => true]
        ],

        'master_stat_datasource' => [
            'driver' => env('DB_MASTER_DRIVER', 'mysql'),
            'write' => [
                'host' => env('DB_MASTER_STAT_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_STAT_WRITE_USERNAME', 'remote'),
                'password' => env('DB_MASTER_STAT_WRITE_PASSWORD', 'Remote@123456'),
            ],
            'read' => [
                'host' => env('DB_MASTER_STAT_READ_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_STAT_READ_USERNAME', 'remote'),
                'password' => env('DB_MASTER_STAT_READ_PASSWORD', 'Remote@123456'),
            ],
            'port' => env('DB_MASTER_PORT', '3306'),
            'database' => env('DB_MASTER_STAT_DATASOURCE_DATABASE', 'stat_datasource'),
            'unix_socket' => env('DB_MASTER_SOCKET', ''),
            'charset' => env('DB_MASTER_CHARSET', 'utf8mb4'),
            'collation' => env('DB_MASTER_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_MASTER_PREFIX', ''),
            'strict' => env('DB_MASTER_STRICT', false),
            'sticky' => env('DB_MASTER_STICKY', true),
            'options' => [PDO::ATTR_EMULATE_PREPARES => true]
        ],

        'master_chart' => [
            'driver' => env('DB_MASTER_DRIVER', 'mysql'),
            'write' => [
                'host' => env('DB_MASTER_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_WRITE_USERNAME', 'remote'),
                'password' => env('DB_MASTER_WRITE_PASSWORD', 'Remote@123456'),
            ],
            'read' => [
                'host' => env('DB_MASTER_READ_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_READ_USERNAME', 'remote'),
                'password' => env('DB_MASTER_READ_PASSWORD', 'Remote@123456'),
            ],
            'port' => env('DB_MASTER_PORT', '3306'),
            'database' => env('DB_MASTER_DATABASE', 'buffsell'),
            'unix_socket' => env('DB_MASTER_SOCKET', ''),
            'charset' => env('DB_MASTER_CHARSET', 'utf8mb4'),
            'collation' => env('DB_MASTER_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_MASTER_PREFIX', ''),
            'strict' => env('DB_MASTER_STRICT', false),
            'sticky' => env('DB_MASTER_STICKY', true),
            'options' => [PDO::ATTR_EMULATE_PREPARES => true]
        ],
        'master_business' => [
            'driver' => env('DB_MASTER_DRIVER', 'mysql'),
            'write' => [
                'host' => env('DB_MASTER_BUSINESS_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_BUSINESS_WRITE_USERNAME', 'remote'),
                'password' => env('DB_MASTER_BUSINESS_WRITE_PASSWORD', 'Remote@123456'),
            ],
            'read' => [
                'host' => env('DB_MASTER_BUSINESS_READ_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_BUSINESS_READ_USERNAME', 'remote'),
                'password' => env('DB_MASTER_BUSINESS_READ_PASSWORD', 'Remote@123456'),
            ],
            'port' => env('DB_MASTER_PORT', '3306'),
            'database' => env('DB_MASTER_BUSINESS_DATABASE', 'buffsell'),
            'unix_socket' => env('DB_MASTER_SOCKET', ''),
            'charset' => env('DB_MASTER_CHARSET', 'utf8mb4'),
            'collation' => env('DB_MASTER_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_MASTER_PREFIX', ''),
            'strict' => env('DB_MASTER_STRICT', false),
            'sticky' => env('DB_MASTER_STICKY', true),
            'options' => [PDO::ATTR_EMULATE_PREPARES => true]
        ],
        'bi' => [
            'driver' => env('DB_MASTER_DRIVER', 'mysql'),
            'write' => [],
            'read' => [
                'host' => env('DB_BI_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_BI_WRITE_USERNAME', 'remote'),
                'password' => env('DB_BI_WRITE_PASSWORD', 'Remote@123456'),
            ],
            'port' => env('DB_MASTER_PORT', '3306'),
            'database' => env('DB_BI_WRITE_DATABASE', 'buffsell'),
            'unix_socket' => env('DB_MASTER_SOCKET', ''),
            'charset' => env('DB_MASTER_CHARSET', 'utf8mb4'),
            'collation' => env('DB_MASTER_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_MASTER_PREFIX', ''),
            'strict' => env('DB_MASTER_STRICT', false),
            'sticky' => env('DB_MASTER_STICKY', true),
            'options' => [PDO::ATTR_EMULATE_PREPARES => true]
        ],
        'master_chart_shop_ads' => [
            'driver' => env('DB_MASTER_DRIVER', 'mysql'),
            'write' => [
                'host' => env('DB_MASTER_BUSINESS_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_BUSINESS_WRITE_USERNAME', 'remote'),
                'password' => env('DB_MASTER_BUSINESS_WRITE_PASSWORD', 'Remote@123456'),
            ],
            'read' => [
                'host' => env('DB_MASTER_BUSINESS_READ_HOST', '127.0.0.1'),
                'username' => env('DB_MASTER_BUSINESS_READ_USERNAME', 'remote'),
                'password' => env('DB_MASTER_BUSINESS_READ_PASSWORD', 'Remote@123456'),
            ],
            'port' => env('DB_MASTER_PORT', '3306'),
            'database' => env('DB_MASTER_CHART_SHOPADS_DATABASE', 'buffsell'),
            'unix_socket' => env('DB_MASTER_SOCKET', ''),
            'charset' => env('DB_MASTER_CHARSET', 'utf8mb4'),
            'collation' => env('DB_MASTER_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_MASTER_PREFIX', ''),
            'strict' => env('DB_MASTER_STRICT', false),
            'sticky' => env('DB_MASTER_STICKY', true),
            'options' => [PDO::ATTR_EMULATE_PREPARES => true]
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => env('REDIS_CLUSTER', false),

        /*'default' => [
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'port'     => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DATABASE', 0),
            'password' => env('REDIS_PASSWORD', null),
        ],*/

        'client' => 'predis',

        'redis_for_cache' => [
            'host' => env('CACHE_REDIS_HOST', '127.0.0.1'),
            'port' => env('CACHE_REDIS_PORT', 6379),
            #'database' => env('CACHE_REDIS_DATABASE', 0),
            'database' => 0,
            'password' => env('CACHE_REDIS_PASSWORD', null),
        ],

        'redis_for_queue' => [
            'host'     => env('QUEUE_REDIS_HOST', '127.0.0.1'),
            'port'     => env('QUEUE_REDIS_PORT', 6379),
            #'database' => env('QUEUE_REDIS_DATABASE', 0),
            'database' => 0,
            'password' => env('QUEUE_REDIS_PASSWORD', null),
        ],
    ],

    'mongodb' => [
        'epsilo_m' => [
            'host'     => env('MONGO_EPSILO_M_HOST', 'localhost'),
            'port'     => env('MONGO_EPSILO_M_PORT', 27017),
            'database' => env('MONGO_EPSILO_M_DATABASE', 'epsilo'),
            'username' => env('MONGO_EPSILO_M_USERNAME'),
            'password' => env('MONGO_EPSILO_M_PASSWORD'),
        ],
        'crawler_log' => [
            'host'     => env('MONGO_CRAWLER_LOG_HOST', 'localhost'),
            'port'     => env('MONGO_CRAWLER_LOG_PORT', 27017),
            'database' => env('MONGO_CRAWLER_LOG_DATABASE', 'epsilo'),
            'username' => env('MONGO_CRAWLER_LOG_USERNAME'),
            'password' => env('MONGO_CRAWLER_LOG_PASSWORD'),
        ]
    ],

];