<?php

return array(
    'dsn' => env('SENTRY_DSN', 'https://cfb2663a0b90452489cc06528c31ebcd@sentry.io/2621131'),

    'environment' => env('SENTRY_ENV', 'local'),

    // capture release as git sha
    // 'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,

    // Capture default user context
    'user_context' => true,

    'curl_method' => 'async'
);