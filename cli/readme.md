# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)



## Integrate Celery to Epsilo M CLI

#### Do to install list:
Install packages to OS:

	rabbitmq-server:
		sudo apt-get install rabbitmq-server
	python-pip: (on Python 2.7)
		sudo apt install python-pip
	celery:
		sudo apt install python-celery-common
	php7.0-amqp: 
		sudo apt-get install php7.0-amqp (Ubuntu/Linux) - (If have issue - update soon)

Optional:

	flower: (Celery monitoring tool)
		pip install flower

At folder /cli run install or update (Install new package – massivescale/celery-php:2.1):

	composer install 
	composer update

#### Step to test:
	(Terminal #1)
	Run this command at /cli folder to start celery worker listening on default queue (celery)
		celery -A tasks worker –loglevel=info
	
	(Terminal #2)
	Run Celery monitoring tool (Optional) 
		celery flower -A tasks --address=127.0.0.1 –port=5555
		Open browser and access link localhost:5555 to monitor

	(Terminal #3)
	Run celery schedule (beat) 
		celery -A tasks beat –loglevel=info
		Wait beat push test tasks to queue
		View task processing at (Terminal #1)
		Or view on Celery monitoring tool

#### Some command and reference:

	Celery command: (Run at folder /cli of project)
		Worker
			celery -A tasks worker –loglevel=info
		Beat (cron schedule)
			celery -A tasks beat --loglevel=info
		Celery monitoring
			celery flower -A tasks --address=127.0.0.1 --port=5555
			Note:  “tasks” is celery project name. 
			Define in “app = Celery('tasks', broker='pyamqp://guest@localhost//')”
		Reference: 
		https://docs.celeryproject.org/en/latest/getting-started/introduction.html (Celery)
		https://flower.readthedocs.io/en/latest/index.html (Flower)
		https://github.com/gjedeer/celery-php (Celery-PHP)

	RabbitMQ command:
		sudo rabbitmqctl list_queues (To view list queue in rabbitMQ)
		…
		Reference: https://www.rabbitmq.com/rabbitmqctl.8.html

#### Integrate Celery to Epsilo M project workflow: 

    Celery crontab (beat schedule)
        To cron time define push celery tasks to celery queue (rabbitMQ)
    Celery worker
        Normal crontab job:
            Process task in queue
            Task trigger - crontab job command (command of PHP – Ex: php artisan index)
            PHP run job
            => Task done || failure
            
        Push queue crontab job:
            ==== Push queue task ====
            Process task in queue
            Task trigger - crontab push job command 
            (command of PHP – Ex: php artisan test-celery)
            PHP run job push (n) celery job (triggerJob) with params into rabbitMQ
            => Task done || failure
            
            ==== Process task in queue ====
            Process triggerJob 
            Add logic to mapping params to dispatch job command
            triggerJob task trigger - job dispatch command 
            (command of PHP – Ex: php artisan job:dispatch <JobName> ...<params>)
            PHP run dispatch job command
            => Task done || failure