Hi <strong>{{$fullName}}</strong>,
<br>
<p>Your password has been changed by the system for a reason: <strong>Requires a password change</strong></p>
<br>
<p>The new Password: <strong>{{$password}}</strong></p>
<br>
<p>Happy selling,<br>Epsilo Team.</p>
