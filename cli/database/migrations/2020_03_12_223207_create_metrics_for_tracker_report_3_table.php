<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMetricsForTrackerReport3Table extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_2_2_513_0291',
        'daily_2_2_513_0292',
        'daily_2_2_513_0293',
        'daily_2_1_513_0069',
        'daily_2_2_511_0300',

        'weekly_2_2_513_0291',
        'weekly_2_2_513_0292',
        'weekly_2_2_513_0293',
        'weekly_2_1_513_0069',
        'weekly_2_2_511_0300',

        'monthly_2_2_513_0291',
        'monthly_2_2_513_0292',
        'monthly_2_2_513_0293',
        'monthly_2_1_513_0069',
        'monthly_2_2_511_0300',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->create($metricTable, function (Blueprint $table) use ($metricTable) {
                $table->increments('id')->unsigned();
                $table->unsignedDecimal('value', 14, 2);
                $table->string('time', 100);
                $table->unsignedInteger('shop_id')->nullable();
                $table->unsignedInteger('created_at');

                $table->unique(['time', 'shop_id'], 'unique_'.$metricTable);

                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->dropIfExists($metricTable);
        }
    }
}