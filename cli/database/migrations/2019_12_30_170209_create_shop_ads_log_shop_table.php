<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateShopAdsLogShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_ads_log_shop', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('shop_ads_id')->unsigned();
            $table->integer('shop_ads_action_id')->unsigned();
            $table->integer('created_at')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->integer('shop_ads_rule_id')->unsigned()->nullable();
            $table->string('checkpoint', 500)->nullable();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_ads_log_shop');
    }
}
