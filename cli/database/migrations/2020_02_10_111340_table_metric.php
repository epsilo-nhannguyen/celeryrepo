<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableMetric extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dashboard_metric', function (Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->string('code');
            $blueprint->string('name');
            $blueprint->enum('type_data', ['primary_data', 'secondary_data']);
            $blueprint->timestamp('modify_at')->useCurrent();
            $blueprint->string('expression')->nullable()->comment('only secondary_data have');
            return $blueprint->create();
        });

        $data = [
            ['impression', 'Impression', 'primary_data', ''],
            ['click', 'Click', 'primary_data', ''],
            ['item_sold', 'Item Sold', 'primary_data', ''],
            ['gmv', 'GMV', 'primary_data', ''],
            ['cost', 'Cost', 'primary_data', ''],
            ['ctr', 'Ctr', 'secondary_data', '{click} / {impression}'],
            ['cr', 'Cr', 'secondary_data', '{item_sold} / {click}'],
            ['cpc', 'Cpc', 'secondary_data', '{cost} / {click}'],
            ['cpi', 'Cpi', 'secondary_data', '{cost} / {item_sold}'],
            ['cir', 'Cir', 'secondary_data', '{cost} / {gmv}'],
            ['total_sku', '#SKU', 'primary_data', ''],
            ['total_keyword', '#KW', 'primary_data', ''],
            ['total_gmv', 'Total GMV', 'primary_data', ''],
            ['total_item_sold', 'Total Item Sold', 'primary_data', ''],
            ['percent_gmv_division_total', '%GMV/Total', 'secondary_data', '{gmv} / {total_gmv} * 100'],
            ['percent_item_sold_division_total', '%Item Sold/Total', 'secondary_data', '{item_sold} / {total_item_sold} * 100'],
            ['wallet_balance', 'Wallet', 'primary_data', ''],
        ];

        $dataInsert = [];
        $id = 1;
        foreach ($data as $line) {
            list ($code, $name, $type, $expression) = $line;
            $dataInsert[] = [
                'id' => $id,
                'code' => $code,
                'name' => $name,
                'type_data' => $type,
                'expression' => $expression
            ];

            $id++;
        }

        DB::table('dashboard_metric')->insert($dataInsert);

        foreach (\App\Models\Currency::getAll() as $row) {
            \App\Services\Dashboard\Shop\Database\Database::initSchemaCampaign('daily', $row->currency_code);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dashboard_metric', function (Blueprint $blueprint) {
            $blueprint->dropIfExists();
        });

        foreach (\App\Models\Currency::getAll() as $row) {
            \App\Services\Dashboard\Shop\Database\Database::initSchemaCampaign('daily', $row->currency_code);
        }
    }
}
