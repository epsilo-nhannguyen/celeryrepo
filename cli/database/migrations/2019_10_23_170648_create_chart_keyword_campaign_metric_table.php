<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartKeywordCampaignMetricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->create('chart_keyword_campaign_metric', function (Blueprint $table) {
            $table->increments('id')->unsigned()->nullable(false);
            $table->integer('shop_channel_id')->unsigned()->nullable(false);
            $table->integer('mak_programmatic_campaign_id')->unsigned()->nullable(false);
            $table->date('date')->nullable(false);
            $table->integer('updated_at')->unsigned()->nullable();
            $table->integer('impression')->unsigned()->nullable()->comment('crawl from seller center');
            $table->integer('click')->unsigned()->nullable()->comment('crawl from seller center');
            $table->integer('item_sold')->unsigned()->nullable()->comment('crawl from seller center');
            $table->decimal('gmv', 14, 3)->unsigned()->nullable()->comment('crawl from seller center');
            $table->decimal('cost', 14, 3)->unsigned()->nullable()->comment('crawl from seller center');
            $table->decimal('ctr', 14, 3)->unsigned()->nullable()->comment('click / impression');
            $table->decimal('cr', 14, 3)->unsigned()->nullable()->comment('item sold / click');
            $table->decimal('cpc', 14, 3)->unsigned()->nullable()->comment('cost / click');
            $table->decimal('cpi', 14, 3)->unsigned()->nullable()->comment('cost / item sold');
            $table->decimal('cir', 14, 3)->unsigned()->nullable()->comment('cost / item price (in paid ads data)');
            $table->integer('total_sku')->unsigned()->nullable()->comment('total SKU (unique in date)');
            $table->integer('total_keyword')->unsigned()->nullable()->comment('total SKU-keyword (unique in date)');
            $table->decimal('total_gmv', 14, 3)->unsigned()->nullable()->comment('total gmv of shop on channel');
            $table->integer('total_item_sold')->unsigned()->nullable()->comment('total item sold of shop on channel');
            $table->decimal('percent_gmv_division_total', 14, 3)->unsigned()->nullable()->comment('% gmv paid ads / total gmv');
            $table->decimal('percent_item_sold_division_total', 14, 3)->unsigned()->nullable()->comment('% item sold paid ads / total item sold');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->dropIfExists('chart_keyword_campaign_metric');
    }
}
