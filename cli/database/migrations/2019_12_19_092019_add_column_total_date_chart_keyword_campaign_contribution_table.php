<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class AddColumnTotalDateChartKeywordCampaignContributionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_contribution', function (Blueprint $table) {
            $table->integer('total_date')->nullable(false)->comment('total date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_contribution', function (Blueprint $table) {
            $table->dropColumn('total_date');
        });
    }
}
