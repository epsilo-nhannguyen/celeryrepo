<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartKeywordKeywordMetricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->create('chart_keyword_keyword_metric', function (Blueprint $table) {
              $table->increments('id')->unsigned()->nullable(false);
              $table->integer('shop_channel_id')->unsigned()->nullable(false);
              $table->date('date')->nullable(false);
              $table->integer('updated_at')->unsigned()->nullable();
              $table->integer('total_keyword')->unsigned()->nullable()->comment('total keyword width state on-going & impression > 0');
              $table->integer('total_keyword_applied')->unsigned()->nullable()->comment('total on-going SKU-keyword');
              $table->decimal('percent_keyword_sale_keyword', 14, 3)->unsigned()->nullable()->comment('(keyword has item sold > 0 that day / total #keyword) * 100');
              $table->decimal('percent_keyword_sale_keyword_sale', 14, 3)->unsigned()->nullable()->comment('(keyword has item sold > 0 that day/total #keyword have sale in L30D) * 100');
              $table->decimal('percent_keyword_top_position', 14, 3)->unsigned()->nullable()->comment('(SKU-keyword has average position today <= 10 / total SKU-keyword) * 100');
              $table->decimal('percent_keyword_ctr_larger_avg', 14, 3)->unsigned()->nullable()->comment('(SKU-keyword has %ctr > avg %ctr L30D in shop/ total SKU-keyword) * 100');
              $table->tinyInteger('is_have_item_sold_l30d')->unsigned()->nullable(false)
                ->comment('0 is no sales , 1 is has sale');
              $table->tinyInteger('is_have_impression_l30d')->unsigned()->nullable(false)
                ->comment('0 is no impression , 1 is has impression');
              $table->engine = 'InnoDB';
              $table->charset = 'utf8';
              $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->dropIfExists('chart_keyword_keyword_metric');
    }
}
