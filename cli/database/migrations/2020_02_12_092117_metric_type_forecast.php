<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MetricTypeForecast extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dashboard_metric', function (Blueprint $blueprint) {
            $blueprint->string('type_forecast', 50)->default('default');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dashboard_metric', function (Blueprint $blueprint) {
            $blueprint->dropColumn('type_forecast');
        });
    }
}
