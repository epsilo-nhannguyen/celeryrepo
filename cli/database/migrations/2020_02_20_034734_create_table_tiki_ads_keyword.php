<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTikiAdsKeyword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('tiki_ads_keyword', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('shop_channel_id');
            $table->string('channel_keyword_id');
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('created_by');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiki_ads_keyword');
    }
}
