<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTikiAdsGroupAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('tiki_ads_group_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('campaign_id')->nullable();
            $table->string('channel_campaign_id');
            $table->string('channel_group_ads_id');
            $table->unsignedInteger('shop_channel_id');
            $table->enum('status', ['ongoing','paused','ended','scheduled', 'deleted']);
            $table->string('type', 100)->nullable();
            $table->string('name');
            $table->decimal('price', 14, 3)->unsigned()->nullable();
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_at')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiki_ads_group_ads');
    }
}
