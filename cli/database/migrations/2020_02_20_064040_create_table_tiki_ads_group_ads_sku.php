<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTikiAdsGroupAdsSku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('tiki_ads_group_ads_sku', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_ads_id');
            $table->unsignedInteger('sku_id');
            $table->string('channel_sku_creative_id');
            $table->enum('status', ['ongoing', 'paused', 'ended', 'scheduled', 'out_of_stock', 'deleted']);
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_at')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiki_ads_group_ads_sku');
    }
}
