<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTikiAdsGroupAdsKeyword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('tiki_ads_group_ads_keyword', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_ads_id');
            $table->unsignedInteger('keyword_id');
            $table->unsignedDecimal('bidding_price', 14, 3);
            $table->enum('match_type', ['broad_match', 'exact_match','phrase']);
            $table->unsignedTinyInteger('status')->default(1);  // 0 - Deleted , 1 - Active, 2 - Pause
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_at')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiki_ads_group_ads_keyword');
    }
}
