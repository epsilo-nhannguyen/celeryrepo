<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WalletAllowNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $allCurrencyCode = \App\Models\Currency::getAll()->pluck('currency_code');
        foreach ($allCurrencyCode as $currencyCode) {
            $currencyCode = strtoupper($currencyCode);
            $table = "dashboard_campaign_metric_daily_reserved_{$currencyCode}";

            Schema::table($table, function (Blueprint $blueprint) {
                $blueprint->decimal('wallet_balance', 14,3)->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
