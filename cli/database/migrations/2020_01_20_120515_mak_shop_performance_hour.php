<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakShopPerformanceHour extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('mak_shop_performance_hour', function (Blueprint $blueprint){
            $blueprint->increments('id')->unsigned();
            $blueprint->integer('shop_channel_id');
            $blueprint->integer('impression');
            $blueprint->integer('shop_item_impression');
            $blueprint->integer('order_amount');
            $blueprint->integer('click');
            $blueprint->integer('order');
            $blueprint->integer('shop_item_click');
            $blueprint->decimal('cost', 14, 3);
            $blueprint->decimal('order_gmv', 14, 3);
            $blueprint->integer('date');
            $blueprint->integer('hour');
            $blueprint->integer('timestamp');


            $blueprint->unique(['shop_channel_id', 'date', 'hour'], 'u_mak_shop_performance_hour_shop_date_hour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->drop('mak_shop_performance_hour');
    }
}
