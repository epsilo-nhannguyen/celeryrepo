<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumEnumTypeCampain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->table('mak_programmatic_campaign', function (Blueprint $table) {
            //
            $table->enum('type_campaign',['smart_campaign'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->table('mak_programmatic_campaign', function (Blueprint $table) {
            //
            $table->dropColumn('type_campaign');
        });
    }
}
