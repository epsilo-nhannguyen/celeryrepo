<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueToShopAdsKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->table('shop_ads_keywords', function (Blueprint $table) {
            $table->string('hex_keyword_name')->nullable();
            $table->unique(['shop_ads_id', 'hex_keyword_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->table('shop_ads_keywords', function (Blueprint $table) {
            $table->dropUnique(['shop_ads_id', 'hex_keyword_name']);
            $table->dropColumn('hex_keyword_name');
        });
    }
}

