<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakRecommendationLogKeywordSuggest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mak_recommendation_log_keyword_suggest', function (Blueprint $table) {
            $table->increments('mak_recommendation_log_keyword_suggest_id');
            $table->unsignedInteger('fk_shop_channel');
            $table->string('fk_product_shop_channel');
            $table->json('mak_recommendation_log_keyword_suggest_json');
            $table->unsignedInteger('mak_recommendation_log_keyword_suggest_created_at');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mak_recommendation_log_keyword_suggest');
    }
}
