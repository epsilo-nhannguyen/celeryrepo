<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateShopAdsMetricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('shop_ads_metric', function (Blueprint $table) {
            $table->increments('id')->nullable(false)->unsigned();
            $table->string('name', 255);
            $table->integer('shop_ads_objective_id')->unsigned();
            $table->string('value', 100)->nullable();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_ads_metric');
    }
}
