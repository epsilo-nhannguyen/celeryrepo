<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMetricsForTrackerReportSkuKeyword1Table extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_2_2_513_0355',
        'daily_2_2_513_0361',
        'daily_2_2_511_0367',
        'daily_2_2_513_0351',
        'daily_2_2_511_0353',
        'daily_2_2_51ID_0357',

        'weekly_2_2_513_0355',
        'weekly_2_2_513_0361',
        'weekly_2_2_511_0367',
        'weekly_2_2_513_0351',
        'weekly_2_2_511_0353',
        'weekly_2_2_51ID_0357',

        'monthly_2_2_513_0355',
        'monthly_2_2_513_0361',
        'monthly_2_2_511_0367',
        'monthly_2_2_513_0351',
        'monthly_2_2_511_0353',
        'monthly_2_2_51ID_0357',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat_datasource')->create($metricTable, function (Blueprint $table) use ($metricTable) {
                $table->increments('id')->unsigned();
                $table->unsignedDecimal('value', 14, 2);
                $table->string('time', 100);
                $table->unsignedInteger('shop_id')->nullable();
                $table->unsignedInteger('product_keyword_id');
                $table->unsignedInteger('product_id');
                $table->string('item_id');
                $table->unsignedInteger('keyword_id')->nullable();
                $table->string('keyword_name');
                $table->unsignedInteger('created_at');

                $table->unique(['time', 'product_keyword_id'], 'unique_'.$metricTable);

                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat_datasource')->dropIfExists($metricTable);
        }
    }
}