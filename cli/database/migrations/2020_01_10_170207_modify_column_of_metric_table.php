<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class ModifyColumnOfMetricTable extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_2_2_211_0245',
        'daily_2_2_211_0246',
        'daily_2_2_212_0247',
        'daily_2_2_212_0248',
        'daily_2_2_212_0249',
        'daily_2_2_211_0250',
        'daily_2_2_211_0251',
        'daily_2_2_212_0252',
        'daily_2_2_212_0253',
        'daily_2_2_212_0254',
        'daily_2_2_211_0255',
        'daily_2_2_211_0256',
        'daily_2_2_212_0257',
        'daily_2_2_212_0258',
        'daily_2_2_212_0259',

        'weekly_2_2_211_0245',
        'weekly_2_2_211_0246',
        'weekly_2_2_212_0247',
        'weekly_2_2_212_0248',
        'weekly_2_2_212_0249',
        'weekly_2_2_211_0250',
        'weekly_2_2_211_0251',
        'weekly_2_2_212_0252',
        'weekly_2_2_212_0253',
        'weekly_2_2_212_0254',
        'weekly_2_2_211_0255',
        'weekly_2_2_211_0256',
        'weekly_2_2_212_0257',
        'weekly_2_2_212_0258',
        'weekly_2_2_212_0259',

        'monthly_2_2_211_0245',
        'monthly_2_2_211_0246',
        'monthly_2_2_212_0247',
        'monthly_2_2_212_0248',
        'monthly_2_2_212_0249',
        'monthly_2_2_211_0250',
        'monthly_2_2_211_0251',
        'monthly_2_2_212_0252',
        'monthly_2_2_212_0253',
        'monthly_2_2_212_0254',
        'monthly_2_2_211_0255',
        'monthly_2_2_211_0256',
        'monthly_2_2_212_0257',
        'monthly_2_2_212_0258',
        'monthly_2_2_212_0259',

        'daily_1_2_211_0047',
        'daily_1_2_211_0062',
        'daily_1_2_213_0116',
        'daily_1_2_213_0121',
        'daily_1_2_213_0148',
        'daily_1_2_213_0153',
        'daily_1_2_213_0158',

        'weekly_1_2_211_0047',
        'weekly_1_2_211_0062',
        'weekly_1_2_213_0116',
        'weekly_1_2_213_0121',
        'weekly_1_2_213_0148',
        'weekly_1_2_213_0153',
        'weekly_1_2_213_0158',

        'monthly_1_2_211_0047',
        'monthly_1_2_211_0062',
        'monthly_1_2_213_0116',
        'monthly_1_2_213_0121',
        'monthly_1_2_213_0148',
        'monthly_1_2_213_0153',
        'monthly_1_2_213_0158',

        'daily_1_2_211_0260',
        'daily_1_2_213_0261',
        'daily_1_2_213_0262',

        'weekly_1_2_211_0260',
        'weekly_1_2_213_0261',
        'weekly_1_2_213_0262',

        'monthly_1_2_211_0260',
        'monthly_1_2_213_0261',
        'monthly_1_2_213_0262',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->table($metricTable, function (Blueprint $table) {
                $table->decimal('value', 14, 2)->unsigned()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}