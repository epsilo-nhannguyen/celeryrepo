<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class AddColumnIsForecastToCampaignMetricIntradayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_metric_intraday', function (Blueprint $table) {
            $table->tinyInteger('is_forecast')->unsigned()->default(0)->comment('0 is live / 1 is forecast');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_metric_intraday', function (Blueprint $table) {
            $table->dropColumn('is_forecast');
        });
    }
}
