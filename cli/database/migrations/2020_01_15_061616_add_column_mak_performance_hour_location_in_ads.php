<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMakPerformanceHourLocationInAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->table('mak_performance_hour', function (Blueprint $blueprint) {
            $blueprint->integer('location_in_ads')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->table('mak_performance_hour', function (Blueprint $blueprint) {
            $blueprint->dropColumn('location_in_ads');
        });
    }
}
