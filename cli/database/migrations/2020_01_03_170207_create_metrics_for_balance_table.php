<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMetricsForBalanceTable extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_2_2_211_0245',
        'daily_2_2_211_0246',
        'daily_2_2_212_0247',
        'daily_2_2_212_0248',
        'daily_2_2_212_0249',
        'daily_2_2_211_0250',
        'daily_2_2_211_0251',
        'daily_2_2_212_0252',
        'daily_2_2_212_0253',
        'daily_2_2_212_0254',
        'daily_2_2_211_0255',
        'daily_2_2_211_0256',
        'daily_2_2_212_0257',
        'daily_2_2_212_0258',
        'daily_2_2_212_0259',

        'weekly_2_2_211_0245',
        'weekly_2_2_211_0246',
        'weekly_2_2_212_0247',
        'weekly_2_2_212_0248',
        'weekly_2_2_212_0249',
        'weekly_2_2_211_0250',
        'weekly_2_2_211_0251',
        'weekly_2_2_212_0252',
        'weekly_2_2_212_0253',
        'weekly_2_2_212_0254',
        'weekly_2_2_211_0255',
        'weekly_2_2_211_0256',
        'weekly_2_2_212_0257',
        'weekly_2_2_212_0258',
        'weekly_2_2_212_0259',

        'monthly_2_2_211_0245',
        'monthly_2_2_211_0246',
        'monthly_2_2_212_0247',
        'monthly_2_2_212_0248',
        'monthly_2_2_212_0249',
        'monthly_2_2_211_0250',
        'monthly_2_2_211_0251',
        'monthly_2_2_212_0252',
        'monthly_2_2_212_0253',
        'monthly_2_2_212_0254',
        'monthly_2_2_211_0255',
        'monthly_2_2_211_0256',
        'monthly_2_2_212_0257',
        'monthly_2_2_212_0258',
        'monthly_2_2_212_0259',
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->create($metricTable, function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->decimal('value', 14, 3)->unsigned();
                $table->string('time', 100);
                $table->integer('shop_id')->unsigned()->nullable();
                $table->integer('created_at')->unsigned();

                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->dropIfExists($metricTable);
        }
    }
}