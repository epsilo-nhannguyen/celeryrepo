<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopChannelIdToTokoAdsSkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->table('toko_ads_sku', function (Blueprint $table) {
            $table->unsignedInteger('shop_channel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->table('toko_ads_sku', function (Blueprint $table) {
            $table->dropColumn('shop_channel_id');
        });
    }

}
