<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexForKeywordToko extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('master_business')->table('toko_ads_keyword_performance', function (Blueprint $table) {
            //
            $table->index('channel_create_date');
            $table->index('keyword_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection('master_business')->table('toko_ads_keyword_performance', function (Blueprint $table) {
            //
            $table->dropIndex('toko_ads_keyword_performance_channel_create_date_index');
            $table->dropIndex('toko_ads_keyword_performance_keyword_id_index');
        });
    }
}
