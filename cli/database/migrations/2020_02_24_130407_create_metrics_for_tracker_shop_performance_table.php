<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMetricsForTrackerShopPerformanceTable extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_2_2_513_0278',
        'daily_2_2_511_0282',
        'daily_2_2_513_0284',
        'daily_2_2_512_0286',
        'daily_2_2_512_0288',
        'daily_2_2_512_0291',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->create($metricTable, function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->unsignedDecimal('value', 14, 2);
                $table->string('time', 100);
                $table->unsignedInteger('shop_id')->nullable();
                $table->unsignedInteger('created_at');

                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->dropIfExists($metricTable);
        }
    }
}