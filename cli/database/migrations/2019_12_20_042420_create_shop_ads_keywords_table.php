<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopAdsKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_ads_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_ads_id');
            $table->string('keyword_name');
            $table->boolean('is_active');
            $table->unsignedInteger('mak_programmatic_keyword_id')->nullable();
            $table->unsignedInteger('bidding_price');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();;
            $table->enum('match_type', ['broad_match', 'exact_match']);
            $table->json('data');
            $table->integer('created_at')->unsigned()->nullable();
            $table->integer('updated_at')->unsigned()->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_ads_keywords');
    }
}
