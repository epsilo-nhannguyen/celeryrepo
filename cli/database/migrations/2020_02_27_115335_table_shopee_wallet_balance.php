<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableShopeeWalletBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopee_wallet_balance', function (Blueprint $blueprint) {
            $blueprint->integer('id')->autoIncrement();
            $blueprint->integer('shop_channel_id');
            $blueprint->decimal('value', 14, 3);
            $blueprint->integer('collected_at');
            $blueprint->index('collected_at', 'i_shopee_wallet_balance_collected_at');
            $blueprint->index('shop_channel_id', 'i_shopee_wallet_balance_shop_channel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopee_wallet_balance');
    }
}
