<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateChartKeywordCampaignContributionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->create('chart_keyword_campaign_contribution', function (Blueprint $table) {
            $table->increments('id')->nullable(false)->unsigned();
            $table->integer('shop_channel_id')->nullable(false);
            $table->integer('mak_programmatic_campaign_id')->nullable(false);
            $table->date('date')->nullable(false);
            $table->tinyInteger('hour')->nullable(false)->unsigned()->comment('from 0 to 23 hour');
            $table->integer('updated_at')->unsigned()->nullable();
            $table->integer('impression')->unsigned()->nullable()->comment('% contribution of impression');
            $table->integer('click')->unsigned()->nullable()->comment('% contribution of click');
            $table->integer('item_sold')->unsigned()->nullable()->comment('% contribution of item_sold');
            $table->integer('gmv')->unsigned()->nullable()->comment('% contribution of gmv');
            $table->integer('cost')->unsigned()->nullable()->comment('% contribution of cost');
            $table->integer('total_sku')->unsigned()->nullable()->comment('% contribution of total_sku');
            $table->integer('total_keyword')->unsigned()->nullable()->comment('% contribution of total_keyword');
            $table->integer('total_gmv')->unsigned()->nullable()->comment('% contribution of total_gmv');
            $table->integer('total_item_sold')->unsigned()->nullable()->comment('% contribution of total_item_sold');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->dropIfExists('chart_keyword_campaign_contribution');
    }
}
