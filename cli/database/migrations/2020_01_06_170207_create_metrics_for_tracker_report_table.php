<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMetricsForTrackerReportTable extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_1_2_211_0047',
        'daily_1_2_211_0062',
        'daily_1_2_213_0116',
        'daily_1_2_213_0121',
        'daily_1_2_213_0148',
        'daily_1_2_213_0153',
        'daily_1_2_213_0158',

        'weekly_1_2_211_0047',
        'weekly_1_2_211_0062',
        'weekly_1_2_213_0116',
        'weekly_1_2_213_0121',
        'weekly_1_2_213_0148',
        'weekly_1_2_213_0153',
        'weekly_1_2_213_0158',

        'monthly_1_2_211_0047',
        'monthly_1_2_211_0062',
        'monthly_1_2_213_0116',
        'monthly_1_2_213_0121',
        'monthly_1_2_213_0148',
        'monthly_1_2_213_0153',
        'monthly_1_2_213_0158',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->create($metricTable, function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->decimal('value', 14, 3)->unsigned();
                $table->string('time', 100);
                $table->integer('shop_id')->unsigned()->nullable();
                $table->integer('created_at')->unsigned();

                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->dropIfExists($metricTable);
        }
    }
}