<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSellableStockToProductDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->table('product_discount', function (Blueprint $table) {
            $table->integer('sellable_stock')->unsigned()->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->table('product_discount', function (Blueprint $table) {
            $table->dropColumn('sellable_stock');
        });
    }
}
