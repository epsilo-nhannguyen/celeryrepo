<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartKeywordCampaignActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->create('chart_keyword_campaign_action', function (Blueprint $table) {
            $table->increments('id')->unsigned()->nullable(false);
            $table->integer('shop_channel_id')->unsigned()->nullable(false);
            $table->integer('mak_programmatic_campaign_id')->unsigned()->nullable(false);
            $table->tinyInteger('mak_programmatic_action_id')->unsigned()->nullable(false);
            $table->date('date')->nullable(false);
            $table->integer('created_at')->unsigned()->nullable(false);
            $table->string('campaign_name')->nullable(false)->comment('campaign name');
            $table->string('action_name')->nullable(false)->comment('action name');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->dropIfExists('chart_keyword_campaign_action');
    }
}
