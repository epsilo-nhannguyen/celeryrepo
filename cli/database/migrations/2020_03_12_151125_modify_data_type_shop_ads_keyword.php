<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDataTypeShopAdsKeyword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->table('shop_ads_keywords', function (Blueprint $table) {
            //
            DB::connection('master_business')->statement('alter table shop_ads_keywords modify column bidding_price decimal(14, 3) unsigned');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->table('shop_ads_keywords', function (Blueprint $table) {
            //
            DB::connection('master_business')->statement('alter table shop_ads_keywords modify column bidding_price int unsigned');
        });
    }
}
