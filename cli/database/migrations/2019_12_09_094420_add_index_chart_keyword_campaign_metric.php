<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexChartKeywordCampaignMetric extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_metric', function (Blueprint $blueprint) {
            $blueprint->index('shop_channel_id', 'chart_keyword_campaign_metric_shop_channel_id');
            $blueprint->index('date', 'chart_keyword_campaign_metric_date');
            $blueprint->index('currency_id', 'chart_keyword_campaign_metric_currency_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_metric', function (Blueprint $blueprint) {
            $blueprint->dropIndex('chart_keyword_campaign_metric_shop_channel_id');
            $blueprint->index('chart_keyword_campaign_metric_date');
            $blueprint->index('chart_keyword_campaign_metric_currency_id');
        });
    }
}
