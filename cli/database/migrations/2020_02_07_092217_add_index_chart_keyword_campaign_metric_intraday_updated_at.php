<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexChartKeywordCampaignMetricIntradayUpdatedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chart_keyword_campaign_metric_intraday', function (Blueprint $blueprint) {
            $blueprint->index('updated_at', 'index_chart_keyword_campaign_metric_intraday_updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chart_keyword_campaign_metric_intraday', function (Blueprint $blueprint) {
            $blueprint->dropIndex('index_chart_keyword_campaign_metric_intraday_updated_at');
        });
    }
}
