<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMetricsForTrackerReport2Table extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'weekly_2_2_513_0278',
        'weekly_2_2_513_0284',
        'weekly_2_2_511_0282',
        'weekly_2_2_512_0286',
        'weekly_2_2_512_0288',
        'weekly_2_2_512_0291',
        'weekly_2_2_513_0280',
        'weekly_2_2_513_0295',
        'weekly_2_2_513_0296',
        'weekly_2_2_513_0297',
        'weekly_2_2_511_0298',

        'monthly_2_2_513_0278',
        'monthly_2_2_513_0284',
        'monthly_2_2_511_0282',
        'monthly_2_2_512_0286',
        'monthly_2_2_512_0288',
        'monthly_2_2_512_0291',
        'monthly_2_2_513_0280',
        'monthly_2_2_513_0295',
        'monthly_2_2_513_0296',
        'monthly_2_2_513_0297',
        'monthly_2_2_511_0298',

    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->create($metricTable, function (Blueprint $table) use ($metricTable) {
                $table->increments('id')->unsigned();
                $table->unsignedDecimal('value', 14, 2);
                $table->string('time', 100);
                $table->unsignedInteger('shop_id')->nullable();
                $table->unsignedInteger('created_at');

                $table->unique(['time', 'shop_id'], 'unique_'.$metricTable);

                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->dropIfExists($metricTable);
        }
    }
}