<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTikiAdsCampaignPerformance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('tiki_ads_campaign_performance', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('campaign_id')->nullable();
            $table->string('channel_campaign_id');
            $table->unsignedInteger('impression');
            $table->unsignedInteger('click');
            $table->unsignedInteger('item_sold');
            $table->decimal('cost', 14, 3)->unsigned();
            $table->decimal('gmv', 14, 3)->unsigned();
            $table->date('channel_create_date');
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('updated_at');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->dropIfExists('tiki_ads_campaign_performance');
    }
}
