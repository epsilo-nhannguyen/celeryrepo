<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMetricsForTrackerTotalShopPerformanceTable extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_1_2_211_0260',
        'daily_1_2_213_0261',
        'daily_1_2_213_0262',

        'weekly_1_2_211_0260',
        'weekly_1_2_213_0261',
        'weekly_1_2_213_0262',

        'monthly_1_2_211_0260',
        'monthly_1_2_213_0261',
        'monthly_1_2_213_0262',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->create($metricTable, function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->decimal('value', 14, 3)->unsigned();
                $table->string('time', 100);
                $table->integer('shop_id')->unsigned()->nullable();
                $table->integer('created_at')->unsigned();

                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->dropIfExists($metricTable);
        }
    }
}