<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakRecommendationSkSelectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->create('mak_recommendation_sk_select', function (Blueprint $table) {
            $table->increments('mak_recommendation_sk_select_id');
            $table->unsignedInteger('fk_shop_channel');
            $table->json('mak_recommendation_sk_select_json');
            $table->unsignedInteger('mak_recommendation_sk_select_created_at');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->dropIfExists('mak_recommendation_sk_select');
    }
}
