<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokoAdsSkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('toko_ads_sku', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('campaign_id')->nullable();
            $table->unsignedInteger('product_id')->nullable();
            $table->unsignedInteger('channel_product_id')->nullable();
            $table->enum('status', ['ongoing','paused','ended','scheduled','closed','not_delivered'])->nullable();
            $table->decimal('max_cost', 14, 3)->unsigned();
            $table->unsignedInteger('from');
            $table->unsignedInteger('to')->nullable();
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_at')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->dropIfExists('toko_ads_sku');
    }
}
