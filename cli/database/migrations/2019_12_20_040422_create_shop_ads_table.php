<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['pause', 'on-going', 'ended', 'schedule']);
            $table->unsignedInteger('amount_used');
            $table->unsignedInteger('adsid');
            $table->boolean('is_daily_budget');
            $table->boolean('is_total_budget');
            $table->boolean('is_no_limit_budget');
            $table->unsignedInteger('budget_amount');
            $table->unsignedInteger('time_line_from');
            $table->unsignedInteger('time_line_to');
            $table->unsignedInteger('shop_channel_id');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->json('data');
            $table->integer('created_at')->unsigned()->nullable();
            $table->integer('updated_at')->unsigned()->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_ads');
    }
}
