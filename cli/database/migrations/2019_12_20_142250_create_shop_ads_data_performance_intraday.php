<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopAdsDataPerformanceIntraday extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_ads_data_performance_intraday', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_ads_keyword_id')->nullable(true);
            $table->unsignedInteger('shop_ads_id')->nullable();
            $table->string('keyword_name');
            $table->date('create_date');
            $table->unsignedInteger('hour');
            $table->decimal('gmv',14,3);
            $table->decimal('expense',14,3);
            $table->unsignedInteger('sold');
            $table->unsignedInteger('shop_item_click');
            $table->decimal('order_amount', 14 , 3);
            $table->unsignedInteger('view');
            $table->unsignedInteger('click');
            $table->json('data');
            $table->integer('created_at')->unsigned()->nullable();
            $table->integer('updated_at')->unsigned()->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_ads_data_performance_intraday');
    }
}
