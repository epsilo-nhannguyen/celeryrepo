<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTikiAdsSkuPerformance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('tiki_ads_group_ads_sku_performance', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_ads_sku_id')->nullable();
            $table->string('channel_group_ads_id');
            $table->string('channel_sku_id');
            $table->string('channel_sku_creative_id');
            $table->unsignedInteger('impression');
            $table->unsignedInteger('click');
            $table->unsignedInteger('item_sold');
            $table->decimal('cost', 14, 3)->unsigned();
            $table->decimal('gmv', 14, 3)->unsigned();
            $table->date('channel_create_date');
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('updated_at');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiki_ads_group_ads_sku_performance');
    }
}