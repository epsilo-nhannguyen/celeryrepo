<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakRecommendationBiddingPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->create('mak_recommendation_bidding_price', function (Blueprint $table) {
            $table->increments('mak_recommendation_bidding_price_id');
            $table->unsignedInteger('fk_shop_channel');
            $table->unsignedInteger('fk_ads_id');
            $table->json('mak_recommendation_bidding_price_keyword_json');
            $table->unsignedInteger('mak_recommendation_bidding_price_created_at');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->dropIfExists('mak_recommendation_bidding_price');
    }
}
