<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartKeywordSkuMetricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->create('chart_keyword_sku_metric', function (Blueprint $table) {
            $table->increments('id')->unsigned()->nullable(false);
            $table->integer('shop_channel_id')->unsigned()->nullable(false);
            $table->date('date')->nullable(false);
            $table->integer('updated_at')->unsigned()->nullable();
            $table->integer('total_sku')->unsigned()->nullable()->comment('sku width on-going status & impression > 0 (unique in date)');
            $table->integer('avg_keyword_sku')->unsigned()->nullable()->comment('total SKU-keyword / total #SKU');
            $table->decimal('percent_sku_sale_total_sku', 14 , 3)->unsigned()->nullable()->comment('(SKU has item sold > 0 that day / total #SKU) * 100');
            $table->decimal('percent_sku_keyword_sale_total_sku', 14, 3)->unsigned()->nullable()->comment('(SKU-keyword has item sold > 0 that day / total SKU-keyword) * 100');
            $table->decimal('avg_percent_discount', 14 , 3)->unsigned()->nullable()->comment('(1 - SP / RRP) * 100 (get average amount snapshot)');
            $table->decimal('percent_sku_sellable_stock', 14 , 3)->unsigned()->nullable()->comment('(SKU sellable stock > 0 / SKU have impression > 0 in L30D) * 100');
            $table->decimal('percent_sku_sale_sku_sale_in_l30d', 14, 3)->unsigned()->nullable()->comment('(SKU has item sold > 0 that day / SKU have sales in L30D) * 100');
            $table->decimal('percent_keyword_sale_keyword_sale', 14, 3)->unsigned()->nullable()->comment('(SKU-keyword has item sold > 0 that day / SKU-keyword have sale in L30D) * 100');
            $table->tinyInteger('is_have_item_sold_l14d')->unsigned()->nullable(false)
                ->comment('0 is has no sale, 1 is has sale');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->dropIfExists('chart_keyword_sku_metric');
    }
}
