<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexCampaignIntraday extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_metric_intraday', function (Blueprint $blueprint) {
            $blueprint->index('shop_channel_id', 'intraday_campaign_metric_shop_channel_id');
            $blueprint->index('date', 'chart_keyword_campaign_metric_date');
            $blueprint->index('currency_id', 'chart_keyword_campaign_metric_currency_id');
            $blueprint->index('mak_programmatic_campaign_id', 'campaign_intraday_mak_programmatic_campaign_id');
            $blueprint->index('hour', 'campaign_intraday_hour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_metric_intraday', function (Blueprint $blueprint) {
            $blueprint->dropIndex('intraday_campaign_metric_shop_channel_id');
            $blueprint->dropIndex('chart_keyword_campaign_metric_date');
            $blueprint->dropIndex('chart_keyword_campaign_metric_currency_id');
            $blueprint->dropIndex('campaign_intraday_mak_programmatic_campaign_id');
            $blueprint->dropIndex('campaign_intraday_hour');
        });
    }
}
