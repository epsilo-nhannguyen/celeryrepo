<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToChartKeywordSkuMetricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->table('chart_keyword_sku_metric', function (Blueprint $table) {
            $table->integer('total_sku_impression_l30')->unsigned()->nullable();
            $table->integer('total_sku_sale_l30d')->unsigned()->nullable();
            $table->integer('total_sku_keyword_sale_l30d')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->table('chart_keyword_sku_metric', function (Blueprint $table) {
            $table->dropColumn('total_sku_impression_l30');
            $table->dropColumn('total_sku_sale_l30d');
            $table->dropColumn('total_sku_keyword_sale_l30d');
        });
    }
}
