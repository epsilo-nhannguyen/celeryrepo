<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableDashboardTool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dashboard_tool', function (Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->string('name');
            $blueprint->string('channel_code');
            $blueprint->create();
        });

        DB::table('dashboard_tool')->insert([
            [
                'id' => 1,
                'name' => 'Keyword Bidding',
                'channel_code' => 'SHOPEE'
            ],
            [
                'id' => 2,
                'name' => 'Shop Ads',
                'channel_code' => 'SHOPEE'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dashboard_tool', function (Blueprint $blueprint) {
            $blueprint->dropIfExists();
        });
    }
}
