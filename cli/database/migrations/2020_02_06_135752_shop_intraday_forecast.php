<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopIntradayForecast extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_daily_forecast_log', function (Blueprint $blueprint) {
            $blueprint->increments('id')->unsigned();
            $blueprint->timestamp('created_at');
            $blueprint->integer('shop_channel_id');
            $blueprint->date('date');
            $blueprint->integer('hour');
            $blueprint->json('snapshot_current')->comment('snapshot intraday current hour - 1 hour');
            $blueprint->json('snapshot_contribution')->comment('snapshot contribution');
            $blueprint->json('snapshot_result')->comment('snapshot result');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_forecast_log');
    }
}
