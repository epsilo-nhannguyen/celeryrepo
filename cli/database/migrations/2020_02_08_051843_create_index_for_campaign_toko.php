<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexForCampaignToko extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('master_business')->table('toko_ads_campaign', function (Blueprint $table) {
            //
            $table->index('shop_channel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection('master_business')->table('toko_ads_campaign', function (Blueprint $table) {
            //
            $table->dropIndex('toko_ads_campaign_shop_channel_id_index');
        });
    }
}
