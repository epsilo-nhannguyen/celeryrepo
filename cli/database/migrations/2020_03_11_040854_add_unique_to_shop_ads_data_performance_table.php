<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueToShopAdsDataPerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->table('shop_ads_data_performance', function (Blueprint $table) {
            $table->string('hex_keyword_name')->nullable();
            $table->unique(['shop_ads_id', 'create_date', 'hex_keyword_name'], 'unique_keyword_name_by_shop');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->table('shop_ads_data_performance', function (Blueprint $table) {
            $table->dropUnique('unique_keyword_name_by_shop');
            $table->dropColumn('hex_keyword_name');
        });
    }
}
