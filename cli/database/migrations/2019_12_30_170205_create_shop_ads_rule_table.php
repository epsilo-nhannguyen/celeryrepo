<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateShopAdsRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_ads_rule', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 255);
            $table->date('validity_from');
            $table->date('validity_to');
            $table->json('extend')->nullable();
            $table->integer('organization_id')->unsigned();
            $table->integer('shop_ads_action_id')->unsigned();
            $table->integer('config_active_id')->unsigned()->default(1);
            $table->integer('created_at')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_at')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_ads_rule');
    }
}
