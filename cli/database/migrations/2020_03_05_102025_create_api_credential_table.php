<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiCredentialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('api_credential', function (Blueprint $table) {
            $table->increments('api_credential_id');
            $table->string('api_credential_fullname', 255);
            $table->string('api_credential_email', 255);
            $table->string('api_credential_password', 255);
            $table->unsignedTinyInteger('fk_config_active')->default(1);
            $table->unsignedInteger('api_credential_created_at');
            $table->unsignedInteger('api_credential_updated_at')->nullable();
            $table->unsignedInteger('api_credential_created_by');
            $table->unsignedInteger('api_credential_updated_by')->nullable();

            $table->foreign('fk_config_active')->references('config_active_id')->on('config_active');

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_credential');
    }
}
