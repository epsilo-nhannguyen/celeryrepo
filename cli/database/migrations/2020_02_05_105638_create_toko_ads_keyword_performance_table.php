<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokoAdsKeywordPerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('toko_ads_keyword_performance', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('keyword_id')->nullable();
            $table->unsignedInteger('campaign_id')->nullable();
            $table->unsignedInteger('channel_keyword_id');
            $table->unsignedInteger('channel_campaign_id');
            $table->unsignedInteger('impression');
            $table->unsignedInteger('click');
            $table->unsignedInteger('item_sold');
            $table->unsignedInteger('cost');
            $table->decimal('gmv', 14, 3)->unsigned()->nullable();
            $table->decimal('ctr', 14, 3)->unsigned();
            $table->unsignedInteger('all_sold');
            $table->decimal('average', 14, 3)->unsigned();
            $table->date('channel_create_date');
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('updated_at');
            $table->unsignedTinyInteger('is_auto')->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->dropIfExists('toko_ads_keyword_performance');
    }
}
