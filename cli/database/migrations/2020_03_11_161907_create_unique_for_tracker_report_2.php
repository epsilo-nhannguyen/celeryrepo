<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniqueForTrackerReport2 extends Migration
{
    # to total shop
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_1_1_211_0057',
        'daily_1_1_213_0058',
        'daily_1_1_213_0059',
        'daily_1_2_211_0260',
        'daily_1_2_213_0261',
        'daily_1_2_213_0262',

        'weekly_1_1_211_0057',
        'weekly_1_1_213_0058',
        'weekly_1_1_213_0059',
        'weekly_1_2_211_0260',
        'weekly_1_2_213_0261',
        'weekly_1_2_213_0262',

        'monthly_1_1_211_0057',
        'monthly_1_1_213_0058',
        'monthly_1_1_213_0059',
        'monthly_1_2_211_0260',
        'monthly_1_2_213_0261',
        'monthly_1_2_213_0262',

        'daily_1_1_223_0060',
        'weekly_1_1_223_0060',
        'monthly_1_1_223_0060',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->table($metricTable, function (Blueprint $table) use ($metricTable) {
                $table->unique(['time'], 'unique_'.$metricTable);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->table($metricTable, function (Blueprint $table) use ($metricTable) {
                $table->dropUnique('unique_'.$metricTable);
            });
        }
    }
}