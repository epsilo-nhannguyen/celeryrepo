<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypeToChartKeywordSkuMetricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->table('chart_keyword_sku_metric', function (Blueprint $table) {
            $table->integer('type')->unsigned()->nullable()->comment('for filter apply');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->table('chart_keyword_sku_metric', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
