<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakRecommendationPriceRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->create('mak_recommendation_price_range', function (Blueprint $table) {
            $table->increments('mak_recommendation_price_range_id');
            $table->unsignedInteger('fk_mak_programmatic_keyword_product');
            $table->unsignedInteger('fk_shop_channel');
            $table->json('mak_recommendation_price_range_json');
            $table->decimal('mak_recommendation_price_range_bidding',14,3)->nullable();
            $table->date('mak_recommendation_price_range_date');
            $table->unsignedInteger('mak_recommendation_price_range_created_at');
            $table->unsignedInteger('mak_recommendation_price_range_updated_at');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->dropIfExists('mak_recommendation_price_range');
    }
}
