<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniqueForTrackerReport1 extends Migration
{
    # to per shop
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_2_2_513_0278',
        'daily_2_2_513_0284',
        'daily_2_2_511_0282',
        'daily_2_2_512_0286',
        'daily_2_2_512_0288',
        'daily_2_2_512_0291',
        'daily_2_2_513_0280',
        'daily_2_2_513_0295',
        'daily_2_2_513_0296',
        'daily_2_2_513_0297',
        'daily_2_2_511_0298',

        'daily_1_2_211_0131',
        'daily_1_2_211_0132',
        'daily_1_2_211_0047',
        'daily_1_2_211_0062',
        'daily_1_2_213_0116',
        'daily_1_2_213_0121',
        'daily_1_2_213_0148',
        'daily_1_2_213_0153',

        'weekly_1_2_211_0131',
        'weekly_1_2_211_0132',
        'weekly_1_2_211_0047',
        'weekly_1_2_211_0062',
        'weekly_1_2_213_0116',
        'weekly_1_2_213_0121',
        'weekly_1_2_213_0148',
        'weekly_1_2_213_0153',

        'monthly_1_2_211_0131',
        'monthly_1_2_211_0132',
        'monthly_1_2_211_0047',
        'monthly_1_2_211_0062',
        'monthly_1_2_213_0116',
        'monthly_1_2_213_0121',
        'monthly_1_2_213_0148',
        'monthly_1_2_213_0153',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->table($metricTable, function (Blueprint $table) use ($metricTable) {
                $table->unique(['time', 'shop_id'], 'unique_'.$metricTable);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->table($metricTable, function (Blueprint $table) use ($metricTable) {
                $table->dropUnique('unique_'.$metricTable);
            });
        }
    }
}