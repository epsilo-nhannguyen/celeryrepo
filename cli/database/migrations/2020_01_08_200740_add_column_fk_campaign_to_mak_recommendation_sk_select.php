<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFkCampaignToMakRecommendationSkSelect extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->table('mak_recommendation_sk_select', function (Blueprint $table) {
            $table->integer('fk_mak_programmatic_campaign')->unsigned()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Library\Model\Sql\Manager::SQL_MASTER_CONNECT)->table('mak_recommendation_sk_select', function (Blueprint $table) {
            $table->dropColumn('fk_mak_programmatic_campaign');
        });
    }
}
