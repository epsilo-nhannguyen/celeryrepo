<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakRecommendationKeywordSuggest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mak_recommendation_keyword_suggest', function (Blueprint $table) {
            $table->increments('mak_recommendation_keyword_suggest_id');
            $table->unsignedInteger('fk_mak_programmatic_keyword_product');
            $table->unsignedInteger('mak_recommendation_keyword_suggest_quality');
            $table->unsignedInteger('mak_recommendation_keyword_suggest_search');
            $table->decimal('mak_recommendation_keyword_suggest_price',14,3);
            $table->unsignedInteger('mak_recommendation_keyword_suggest_created_at');
            $table->unsignedInteger('mak_recommendation_keyword_suggest_update_at');
            $table->unsignedInteger('mak_recommendation_keyword_suggest_created_by');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mak_recommendation_keyword_suggest');
    }
}
