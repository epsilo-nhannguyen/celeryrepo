<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMetricsForTrackerReport1Table extends Migration
{
    /**
     * @var array
     */
    private $metricsTableArray = [
        'daily_2_2_513_0280',
        'daily_2_2_513_0295',
        'daily_2_2_513_0296',
        'daily_2_2_513_0297',
        'daily_2_2_511_0298',
        'daily_1_2_211_0131',
        'daily_1_2_211_0132',

        'weekly_1_2_211_0131',
        'weekly_1_2_211_0132',

        'monthly_1_2_211_0131',
        'monthly_1_2_211_0132',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->create($metricTable, function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->unsignedDecimal('value', 14, 2);
                $table->string('time', 100);
                $table->unsignedInteger('shop_id')->nullable();
                $table->unsignedInteger('created_at');

                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->metricsTableArray as $metricTable) {
            Schema::connection('master_stat')->dropIfExists($metricTable);
        }
    }
}