<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductShopItemIdToTikiAdsSkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->table('tiki_ads_sku', function (Blueprint $table) {
            $table->string('product_shop_item_id', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->table('tiki_ads_sku', function (Blueprint $table) {
            $table->dropColumn('product_shop_item_id');
        });
    }
}
