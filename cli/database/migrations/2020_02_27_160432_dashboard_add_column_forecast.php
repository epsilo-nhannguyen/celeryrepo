<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DashboardAddColumnForecast extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $allCurrencyCode = \App\Models\Currency::getAll()->pluck('currency_code');
        foreach ($allCurrencyCode as $currencyCode) {
            $currencyCode = strtoupper($currencyCode);
            $table = "dashboard_campaign_metric_daily_reserved_{$currencyCode}";

            Schema::table($table, function (Blueprint $blueprint) {
                $blueprint->json('primary_data_metrics_forecast');
            });

            $table = "dashboard_campaign_metric_daily_{$currencyCode}";

            Schema::table($table, function (Blueprint $blueprint) {
                $blueprint->json('primary_data_metrics_forecast');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $allCurrencyCode = \App\Models\Currency::getAll()->pluck('currency_code');
        foreach ($allCurrencyCode as $currencyCode) {
            $currencyCode = strtoupper($currencyCode);
            $table = "dashboard_campaign_metric_daily_reserved_{$currencyCode}";

            Schema::table($table, function (Blueprint $blueprint) {
                $blueprint->dropColumn('primary_data_metrics_forecast');
            });

            $table = "dashboard_campaign_metric_daily_{$currencyCode}";

            Schema::table($table, function (Blueprint $blueprint) {
                $blueprint->dropColumn('primary_data_metrics_forecast');
            });
        }
    }
}
