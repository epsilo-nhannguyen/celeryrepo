<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokoAdsCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_business')->create('toko_ads_campaign', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('channel_campaign_id')->nullable();
            $table->unsignedInteger('shop_channel_id');
            $table->unsignedTinyInteger('config_active_id');
            $table->string('code', 100);
            $table->string('name');
            $table->decimal('daily_budget', 14, 3)->unsigned()->nullable();
            $table->decimal('max_cost', 14, 3)->unsigned()->nullable();
            $table->date('from');
            $table->date('to')->nullable();
            $table->unsignedTinyInteger('is_auto');
            $table->unsignedInteger('created_at');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_at')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_business')->dropIfExists('toko_ads_campaign');
    }
}
