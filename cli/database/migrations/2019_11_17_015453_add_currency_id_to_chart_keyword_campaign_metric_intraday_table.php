<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyIdToChartKeywordCampaignMetricIntradayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_metric_intraday', function (Blueprint $table) {
            $table->integer('currency_id')->unsigned()->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->table('chart_keyword_campaign_metric_intraday', function (Blueprint $table) {
            $table->dropColumn('currency_id');
        });
    }
}
