<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexForSkuToko extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('master_business')->table('toko_ads_sku_performance', function (Blueprint $table) {
            //
            $table->index('product_id');
            $table->index('channel_create_date');
        });
        Schema::connection('master_business')->table('toko_ads_sku', function (Blueprint $table) {
            //
            $table->index('campaign_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection('master_business')->table('toko_ads_sku_performance', function (Blueprint $table) {
            //
            $table->dropIndex('toko_ads_sku_performance_product_id_index');
            $table->dropIndex('toko_ads_sku_performance_channel_create_date_index');
        });
        Schema::connection('master_business')->table('toko_ads_sku', function (Blueprint $table) {
            //
            $table->dropIndex('toko_ads_sku_campaign_id_index');
        });
    }
}
