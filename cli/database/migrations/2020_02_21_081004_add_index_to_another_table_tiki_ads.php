<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToAnotherTableTikiAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('master_business')->table('tiki_ads_campaign', function (Blueprint $table) {
            //
            $table->index('shop_channel_id', 'shop_channel_id');
            $table->index('name', 'name');
            $table->index('status', 'status');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads', function (Blueprint $table) {
            //
            $table->index('shop_channel_id', 'shop_channel_id');
            $table->index('name', 'name');
            $table->index('campaign_id', 'campaign_id');
            $table->index('status', 'status');
        });

        Schema::connection('master_business')->table('tiki_ads_sku', function (Blueprint $table) {
            //
            $table->index('shop_channel_id', 'shop_channel_id');
            $table->index('product_id', 'product_id');
        });

        Schema::connection('master_business')->table('tiki_ads_keyword', function (Blueprint $table) {
            //
            $table->index('shop_channel_id', 'shop_channel_id');
            $table->index('name', 'name');
        });

        Schema::connection('master_business')->table('tiki_ads_campaign_performance', function (Blueprint $table) {
            //
            $table->index('campaign_id', 'campaign_id');
            $table->index('channel_create_date', 'channel_create_date');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_performance', function (Blueprint $table) {
            //
            $table->index('group_id', 'group_id');
            $table->index('channel_create_date', 'channel_create_date');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_sku_performance', function (Blueprint $table) {
            //
            $table->index('group_ads_sku_id', 'group_ads_sku_id');
            $table->index('channel_create_date', 'channel_create_date');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_keyword_performance', function (Blueprint $table) {
            //
            $table->index('group_ads_keyword_id', 'group_ads_keyword_id');
            $table->index('channel_create_date', 'channel_create_date');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_keyword', function (Blueprint $table) {
            //
            $table->index('keyword_id', 'keyword_id');
            $table->index('group_ads_id', 'group_ads_id');
            $table->index('status', 'status');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_sku', function (Blueprint $table) {
            //
            $table->index('sku_id', 'sku_id');
            $table->index('group_ads_id', 'group_ads_id');
            $table->index('status', 'status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection('master_business')->table('tiki_ads_campaign', function (Blueprint $table) {
            $table->dropIndex('shop_channel_id');
            $table->dropIndex('name');
            $table->dropIndex('status');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads', function (Blueprint $table) {
            //
            $table->dropIndex('shop_channel_id');
            $table->dropIndex('name');
            $table->dropIndex('campaign_id');
            $table->dropIndex('status');
        });

        Schema::connection('master_business')->table('tiki_ads_sku', function (Blueprint $table) {
            //
            $table->dropIndex('shop_channel_id');
            $table->dropIndex('product_id');
        });

        Schema::connection('master_business')->table('tiki_ads_keyword', function (Blueprint $table) {
            //
            $table->dropIndex('shop_channel_id');
            $table->dropIndex('name');
        });

        Schema::connection('master_business')->table('tiki_ads_campaign_performance', function (Blueprint $table) {
            //
            $table->dropIndex('campaign_id');
            $table->dropIndex('channel_create_date');
        });
//
        Schema::connection('master_business')->table('tiki_ads_group_ads_performance', function (Blueprint $table) {
            //
            $table->dropIndex('channel_create_date');
            $table->dropIndex('group_id');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_sku_performance', function (Blueprint $table) {
            //
            $table->dropIndex('group_ads_sku_id');
            $table->dropIndex('channel_create_date');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_keyword_performance', function (Blueprint $table) {
            //
            $table->dropIndex('group_ads_keyword_id');
            $table->dropIndex('channel_create_date');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_keyword', function (Blueprint $table) {
            //
            $table->dropIndex('keyword_id', 'keyword_id');
            $table->dropIndex('group_ads_id', 'group_ads_id');
            $table->dropIndex('status');
        });

        Schema::connection('master_business')->table('tiki_ads_group_ads_sku', function (Blueprint $table) {
            //
            $table->dropIndex('sku_id', 'sku_id');
            $table->dropIndex('group_ads_id', 'group_ads_id');
            $table->dropIndex('status');
        });

    }
}
