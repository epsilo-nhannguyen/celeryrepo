<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('master_chart')->create('product_discount', function (Blueprint $table) {
            $table->increments('id')->unsigned()->nullable(false);
            $table->integer('fk_product_shop_channel')->unsigned()->nullable(false);
            $table->date('date')->nullable(false);
            $table->decimal('value', 14, 3)->unsigned()->nullable()->comment('unit %');
            $table->integer('updated_at')->unsigned()->nullable();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('master_chart')->dropIfExists('product_discount');
    }
}
