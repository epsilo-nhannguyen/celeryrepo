<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataMetricExpressionOfGroup2AndGroup3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        \Illuminate\Support\Facades\DB::connection('master_business')->table('dashboard_metric')
            ->where('code', 'percent_gmv_division_total')
            ->update(['expression' => '{gmv} / {total_gmv} * 100']);

        \Illuminate\Support\Facades\DB::connection('master_business')->table('dashboard_metric')
            ->where('code', 'percent_item_sold_division_total')
            ->update(['expression' => '{item_sold} / {total_item_sold} * 100']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
