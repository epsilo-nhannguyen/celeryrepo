<?php

use Illuminate\Database\Seeder;

class dashboard_metric_roas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::table('dashboard_metric')->select()->where('code', 'roas')->get()->first();
        if (!$data) {
            DB::table('dashboard_metric')->insert([
                'code' => 'roas',
                'name' => 'Roas',
                'type_data' => 'secondary_data',
                'expression' => '{gmv} / {cost}',
            ]);
        }
    }
}
