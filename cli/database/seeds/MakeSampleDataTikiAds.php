<?php

use Illuminate\Database\Seeder;

class MakeSampleDataTikiAds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Start import tiki ads campaign' . PHP_EOL;
        $arrayShopChannelId = [132, 131];
        $arrayStatusCampaign = array('ongoing', 'paused', 'ended', 'scheduled');
        $arrayImportCampaign = [];
        for ($i = 0; $i <= 1000; $i++) {
            $arrayImportCampaign[] = [
                'shop_channel_id' => $arrayShopChannelId[array_rand($arrayShopChannelId)],
                'status' => $arrayStatusCampaign[array_rand($arrayStatusCampaign)],
                'code' => 'campaign-code-' . str_shuffle('qwertyuiopasfdhjkl'),
                'name' => 'campaign-name-' . str_shuffle('qwertyuiopasfdhjkl'),
                'daily_budget' => random_int(10, 100) / 100,
                'from' => date('Y-m-d'),
                'to' => date('Y-m-d', strtotime('+' . $i . 'days')),
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
        }

        DB::connection('master_business')->table('tiki_ads_campaign')
            ->insert($arrayImportCampaign);
        unset($arrayImportCampaign);
        echo 'Done import tiki ads campaign' . PHP_EOL;

        echo 'Start import tiki ads campaign performance' . PHP_EOL;
        $arrayImportCampaignPerformance = [];
        $arrayChannelCreateDate = ['2020-02-12', '2020-02-14', '2020-02-19', '2020-02-22', '2020-02-29'];
        $arrayCampaignId = DB::connection('master_business')->table('tiki_ads_campaign')->select(['id'])->pluck('id');
        for ($i = 0; $i <= 2000; $i++) {
            $arrayImportCampaignPerformance[] = [
                'channel_campaign_id' => random_int(1, 10000),
                'campaign_id' => $arrayCampaignId[array_rand($arrayCampaignId->toArray())],
                'impression' => random_int(10, 1000),
                'click' => random_int(10, 1000),
                'item_sold' => random_int(10, 1000),
                'cost' => random_int(10, 1000) / 100,
                'gmv' => random_int(10, 1000),
                'channel_create_date' => $arrayChannelCreateDate[array_rand($arrayChannelCreateDate)],
                'created_at' => strtotime('now')
            ];
        }
        DB::connection('master_business')->table('tiki_ads_campaign_performance')->insert($arrayImportCampaignPerformance);
        unset($arrayImportCampaignPerformance);
        unset($arrayCampaignId);
        echo 'Done import tiki ads campaign performance' . PHP_EOL;

        echo 'Start import tiki group ads' . PHP_EOL;
        $arrayImportGroupAds = [];
        $arrayCampaignObject = DB::connection('master_business')->table('tiki_ads_campaign')->select(['id','shop_channel_id'])->get();
        $tempI = 0;
        for ($i = 0; $i <= 2000; $i++) {
            if ($tempI > 1000) {
                $tempI = 0;
            }
            $arrayImportGroupAds[] = [
                'channel_campaign_id' => random_int(1, 10000),
                'campaign_id' => $arrayCampaignObject[$tempI]->id,
                'channel_group_ads_id' => random_int(10, 1000),
                'shop_channel_id' => $arrayCampaignObject[$tempI]->shop_channel_id,
                'status' => $arrayStatusCampaign[array_rand($arrayStatusCampaign)],
                'name' => 'group_ads-name-' . str_shuffle('qwertyuiopasfdhjkl'),
                'price' => random_int(10, 100) / 100,
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
            $tempI++;
        }

        DB::connection('master_business')->table('tiki_ads_group_ads')->insert($arrayImportGroupAds);
        unset($arrayImportGroupAds);
        echo 'Done import tiki ads group ads' . PHP_EOL;

        echo 'Start import tiki ads group ads performance' . PHP_EOL;
        $arrayGroupAdsId = DB::connection('master_business')->table('tiki_ads_group_ads')->select(['id'])->pluck('id');
        $arrayImportGroupAdsPerformance = [];
        for ($i = 0; $i <= 2000; $i++) {
            $arrayImportGroupAdsPerformance[] = [
                'channel_campaign_id' => random_int(1, 10000),
                'channel_group_ads_id' => random_int(1, 10000),
                'group_id' => $arrayGroupAdsId[array_rand($arrayGroupAdsId->toArray())],
                'impression' => random_int(10, 1000),
                'click' => random_int(10, 1000),
                'item_sold' => random_int(10, 1000),
                'cost' => random_int(10, 1000) / 100,
                'gmv' => random_int(10, 1000) / 100,
                'channel_create_date' => $arrayChannelCreateDate[array_rand($arrayChannelCreateDate)],
                'created_at' => strtotime('now')
            ];
        }
        DB::connection('master_business')->table('tiki_ads_group_ads_performance')->insert($arrayImportGroupAdsPerformance);
        unset($arrayGroupAdsId);
        unset($arrayImportGroupAdsPerformance);
        echo 'Done import tiki ads group ads performance' . PHP_EOL;

        echo 'Start import tiki ads sku' . PHP_EOL;
        $arrayImportSku = [];
        $arrayProductShopChannelId_131 = DB::connection('master_business')
            ->table('product_shop_channel')->where('fk_shop_channel', 131)
            ->select(['product_shop_channel_id'])->pluck('product_shop_channel_id');

        $arrayProductShopChannelId_132 = DB::connection('master_business')
            ->table('product_shop_channel')->where('fk_shop_channel', 132)
            ->select(['product_shop_channel_id'])->pluck('product_shop_channel_id');

        foreach ($arrayProductShopChannelId_131 as $id) {
            $arrayImportSku[] = [
                'product_id' => $id,
                'shop_channel_id' => 131,
                'channel_sku_id' => random_int(1, 10000),
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
        }

        foreach ($arrayProductShopChannelId_132 as $id) {
            $arrayImportSku[] = [
                'product_id' => $id,
                'shop_channel_id' => 132,
                'channel_sku_id' => random_int(1, 10000),
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
        }

        DB::connection('master_business')->table('tiki_ads_sku')->insert($arrayImportSku);
        unset($arrayImportSku);
        unset($arrayProductShopChannelId_131);
        unset($arrayProductShopChannelId_132);
        echo 'Done import tiki ads sku' . PHP_EOL;

        echo 'Start import group ads sku' . PHP_EOL;
        $arrayStatusSku = array('ongoing', 'paused', 'ended', 'scheduled', 'out_of_stock', 'not_activated');

        $arrayGroupAdsIdChannel131 = DB::connection('master_business')->table('tiki_ads_group_ads')
            ->where('shop_channel_id', 131)
            ->select(['id'])->pluck('id')->toArray();
        $arrayGroupAdsIdChannel132 = DB::connection('master_business')->table('tiki_ads_group_ads')
            ->where('shop_channel_id', 132)
            ->select(['id'])->pluck('id')->toArray();
        $arraySkuIdChannel131 = DB::connection('master_business')->table('tiki_ads_sku')
            ->where('shop_channel_id', 131)
            ->select(['id'])->pluck('id')->toArray();
        $arraySkuIdChannel132 = DB::connection('master_business')->table('tiki_ads_sku')
            ->where('shop_channel_id', 132)
            ->select(['id'])->pluck('id')->toArray();

        $arrayImportGroupAdsSku = [];
        foreach ($arraySkuIdChannel131 as $index => $id131) {
            $arrayImportGroupAdsSku[] = [
                'group_ads_id' => $arrayGroupAdsIdChannel131[$index],
                'sku_id' => $id131,
                'status' => $arrayStatusSku[array_rand($arrayStatusSku)],
                'channel_sku_creative_id' => 0,
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
        }

        foreach ($arraySkuIdChannel132 as $index => $id132) {
            $arrayImportGroupAdsSku[] = [
                'group_ads_id' => $arrayGroupAdsIdChannel132[$index],
                'sku_id' => $id132,
                'channel_sku_creative_id' => 0,
                'status' => $arrayStatusSku[array_rand($arrayStatusSku)],
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
        }


        DB::connection('master_business')->table('tiki_ads_group_ads_sku')
            ->insert($arrayImportGroupAdsSku);
        unset($arrayImportGroupAdsSku);
        unset($arraySkuIdChannel132);
        unset($arraySkuIdChannel131);

        echo 'Done import tiki_ads_group_ads_sku' . PHP_EOL;

        echo 'Start import tiki ads sku performance' . PHP_EOL;
        $arrayImportSkuPerformance = [];
        $arrayGroupAdsSkuId = DB::connection('master_business')->table('tiki_ads_group_ads_sku')
            ->select(['id'])->pluck('id')->toArray();
        for ($i = 0; $i < 2000; $i++) {
            $arrayImportSkuPerformance[] = [
                'group_ads_sku_id' => $arrayGroupAdsSkuId[array_rand($arrayGroupAdsSkuId)],
                'channel_sku_id' => random_int(1, 10000),
                'channel_group_ads_id' => random_int(1, 10000),
                'impression' => random_int(10, 1000),
                'click' => random_int(10, 1000),
                'item_sold' => random_int(10, 1000),
                'cost' => random_int(10, 1000) / 100,
                'gmv' => random_int(10, 1000),
                'channel_create_date' => $arrayChannelCreateDate[array_rand($arrayChannelCreateDate)],
                'created_at' => strtotime('now')
            ];
        }

        DB::connection('master_business')->table('tiki_ads_group_ads_sku_performance')
            ->insert($arrayImportSkuPerformance);
        unset($arrayImportSkuPerformance);
        unset($arrayGroupAdsSkuId);
        echo 'Done import tiki ads sku performance' . PHP_EOL;

        echo 'Start import tiki ads keyword ' . PHP_EOL;
        $arrayImportKeyword = [];
        $arrayMatchTypeKeyword = array('broad_match', 'exact_match', 'phrase');
        for ($i = 0; $i < 2000; $i++) {
            $arrayImportKeyword[] = [
                'name' => 'keyword-name-' . str_shuffle('qwertyuiopasfdhjkl'),
                'shop_channel_id' => $arrayShopChannelId[array_rand($arrayShopChannelId)],
                'channel_keyword_id' => random_int(10, 1000),
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
        }

        DB::connection('master_business')->table('tiki_ads_keyword')->insert($arrayImportKeyword);
        unset($arrayImportKeyword);

        echo 'Done import tiki ads keyword ' . PHP_EOL;

        echo 'Start import tiki ads group ads keyword' . PHP_EOL;
        $arrayKeywordIdChannel131 = DB::connection('master_business')->table('tiki_ads_sku')
            ->where('shop_channel_id', 131)
            ->select(['id'])->pluck('id')->toArray();
        $arrayKeywordIdChannel132 = DB::connection('master_business')->table('tiki_ads_sku')
            ->where('shop_channel_id', 132)
            ->select(['id'])->pluck('id')->toArray();
        $arrayImportGroupAdsKeyword = [];

        foreach ($arrayKeywordIdChannel131 as $index => $id131) {
            $arrayImportGroupAdsKeyword[] = [
                'group_ads_id' => $arrayGroupAdsIdChannel131[$index],
                'keyword_id' => $id131,
                'bidding_price' => random_int(10, 1000) / 100,
                'status' => random_int(0, 1),
                'match_type' => $arrayMatchTypeKeyword[array_rand($arrayMatchTypeKeyword)],
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
        }

        foreach ($arrayKeywordIdChannel132 as $index => $id132) {
            $arrayImportGroupAdsKeyword[] = [
                'group_ads_id' => $arrayGroupAdsIdChannel132[$index],
                'keyword_id' => $id132,
                'bidding_price' => random_int(10, 1000) / 100,
                'status' => random_int(0, 1),
                'match_type' => $arrayMatchTypeKeyword[array_rand($arrayMatchTypeKeyword)],
                'created_at' => strtotime('now'),
                'created_by' => 106
            ];
        }

        DB::connection('master_business')->table('tiki_ads_group_ads_keyword')->insert($arrayImportGroupAdsKeyword);
        unset($arrayGroupAdsIdChannel132);
        unset($arrayGroupAdsIdChannel131);
        unset($arrayImportGroupAdsKeyword);
        echo 'Done import tiki ads group ads keyword' . PHP_EOL;

        echo 'Start import tiki ads keyword performance' . PHP_EOL;
        $arrayImportKeywordPerformance = [];
        $arrayGroupAdsKeywordId = DB::connection('master_business')->table('tiki_ads_group_ads_keyword')
            ->select(['id'])->pluck('id')->toArray();
        for ($i = 0; $i < 2000; $i++) {
            $arrayImportKeywordPerformance[] = [
                'group_ads_keyword_id' => $arrayGroupAdsKeywordId[array_rand($arrayGroupAdsKeywordId)],
                'channel_keyword_id' => random_int(1, 10000),
                'channel_group_ads_id' => random_int(1, 10000),
                'impression' => random_int(10, 1000),
                'click' => random_int(10, 1000),
                'item_sold' => random_int(10, 1000),
                'cost' => random_int(10, 1000) / 100,
                'gmv' => random_int(10, 1000),
                'channel_create_date' => $arrayChannelCreateDate[array_rand($arrayChannelCreateDate)],
                'created_at' => strtotime('now')
            ];
        }
        DB::connection('master_business')->table('tiki_ads_group_ads_keyword_performance')->insert($arrayImportKeywordPerformance);
        unset($arrayImportKeywordPerformance);
        unset($arrayGroupAdsKeywordId);
        echo 'Done import tiki ads keyword performance' . PHP_EOL;
    }
}
