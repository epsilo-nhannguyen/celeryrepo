<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ShopAdsObjectiveTableSeeder');
        $this->call('ShopAdsActionTableSeeder');
        $this->call('ShopAdsMetricTableSeeder');
        $this->call('ShopAdsOperatorTableSeeder');
        $this->call('AddChannelSeeder');
        $this->call('ApiCredentialTableSeeder');
    }
}
