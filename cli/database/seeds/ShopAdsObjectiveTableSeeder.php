<?php

use Illuminate\Database\Seeder;

class ShopAdsObjectiveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Shop - Keyword'
            ],
            [
                'id' => 2,
                'name' => 'Shop'
            ]
        ];

        DB::connection('master_business')->table('shop_ads_objective')->insert($data);
    }
}
