<?php

use Illuminate\Database\Seeder;

class ShopAdsMetricTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $impression = 'Impression';
        $click = 'Click';
        $itemSold = 'Item Sold';
        $gmv = 'GMV';
        $costSpent = 'Cost Spent';

        $dateNumber3 = 3;
        $dateNumber7 = 7;
        $dateNumber14 = 14;
        $dateNumber30 = 30;

        $shopAdsKeyword = 1;
        $shopAds = 2;

        $data = [
            [
                'id' => 1,
                'name' => $impression,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 2,
                'name' => $impression,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 3,
                'name' => $impression,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 4,
                'name' => $impression,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],

            [
                'id' => 5,
                'name' => $click,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 6,
                'name' => $click,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 7,
                'name' => $click,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 8,
                'name' => $click,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],

            [
                'id' => 9,
                'name' => $itemSold,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 10,
                'name' => $itemSold,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 11,
                'name' => $itemSold,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 12,
                'name' => $itemSold,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],

            [
                'id' => 13,
                'name' => $gmv,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 14,
                'name' => $gmv,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 15,
                'name' => $gmv,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 16,
                'name' => $gmv,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],

            [
                'id' => 17,
                'name' => $costSpent,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 18,
                'name' => $costSpent,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 19,
                'name' => $costSpent,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            [
                'id' => 20,
                'name' => $costSpent,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],

            [
                'id' => 41,
                'name' => 'Top 1',
                'value' => null,
                'shop_ads_objective_id' => $shopAdsKeyword
            ],
            # keyword

            [
                'id' => 21,
                'name' => $impression,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 22,
                'name' => $impression,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 23,
                'name' => $impression,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 24,
                'name' => $impression,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAds
            ],

            [
                'id' => 25,
                'name' => $click,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 26,
                'name' => $click,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 27,
                'name' => $click,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 28,
                'name' => $click,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAds
            ],

            [
                'id' => 29,
                'name' => $itemSold,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 30,
                'name' => $itemSold,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 31,
                'name' => $itemSold,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 32,
                'name' => $itemSold,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAds
            ],

            [
                'id' => 33,
                'name' => $gmv,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 34,
                'name' => $gmv,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 35,
                'name' => $gmv,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 36,
                'name' => $gmv,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAds
            ],

            [
                'id' => 37,
                'name' => $costSpent,
                'value' => $dateNumber3,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 38,
                'name' => $costSpent,
                'value' => $dateNumber7,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 39,
                'name' => $costSpent,
                'value' => $dateNumber14,
                'shop_ads_objective_id' => $shopAds
            ],
            [
                'id' => 40,
                'name' => $costSpent,
                'value' => $dateNumber30,
                'shop_ads_objective_id' => $shopAds
            ],
            # shop ads
        ];

        DB::connection('master_business')->table('shop_ads_metric')->insert($data);
    }
}
