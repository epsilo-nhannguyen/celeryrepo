<?php

use Illuminate\Database\Seeder;

class AddChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('master_business')->table('channel')->insert([
            [
                'channel_id' => '213',
                'channel_name' => 'Tokopedia',
                'channel_code' => 'TOKOPEDIA',
                'fk_config_active' => 1,
                'channel_created_at' => time(),
                'fk_venture' => '5',
                'channel_configuration_rrp' => 0.00,
                'channel_configuration_selling_price' => 0.00,
                'channel_is_offline' => 0,
                'channel_request_packing' => 1
            ]
        ]);
    }
}
