<?php

use Illuminate\Database\Seeder;

class ShopAdsOperatorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => '='
            ],
            [
                'id' => 2,
                'name' => '>'
            ],
            [
                'id' => 3,
                'name' => '<'
            ],
            [
                'id' => 4,
                'name' => '>='
            ],
            [
                'id' => 5,
                'name' => '<='
            ],
        ];

        DB::connection('master_business')->table('shop_ads_operator')->insert($data);
    }
}
