<?php

use Illuminate\Database\Seeder;

class ShopAdsActionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Delete Shop Ads-Keyword',
                'shop_ads_objective_id' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Restore Shop Ads-Keyword',
                'shop_ads_objective_id' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Increase Bidding Price',
                'shop_ads_objective_id' => 1,
            ],
            [
                'id' => 4,
                'name' => 'Decrease Bidding Price',
                'shop_ads_objective_id' => 1,
            ],
            [
                'id' => 5,
                'name' => 'Add Shop Ads',
                'shop_ads_objective_id' => 2,
            ],
            [
                'id' => 6,
                'name' => 'Pause Shop Ads',
                'shop_ads_objective_id' => 2,
            ],
            [
                'id' => 7,
                'name' => 'Resume Shop Ads',
                'shop_ads_objective_id' => 2,
            ],
            [
                'id' => 8,
                'name' => 'Add Shop Ads-Keyword',
                'shop_ads_objective_id' => 2,
            ],
        ];

        DB::connection('master_business')->table('shop_ads_action')->insert($data);
    }
}
