<?php

use Illuminate\Database\Seeder;

class ApiCredentialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('master_business')->table('api_credential')->insert([
            [
                'api_credential_fullname' => 'Operation',
                'api_credential_email' => 'operation@epsilo.io',
                'api_credential_password' => md5(123456),
                'fk_config_active' => 1,
                'api_credential_created_at' => 1568651400,
                'api_credential_updated_at' => null,
                'api_credential_created_by' => 1,
                'api_credential_updated_by' => null
            ],
            [
                'api_credential_fullname' => 'Marketing',
                'api_credential_email' => 'marketing@epsilo.io',
                'api_credential_password' => md5(123456),
                'fk_config_active' => 1,
                'api_credential_created_at' => 1568651400,
                'api_credential_updated_at' => null,
                'api_credential_created_by' => 1,
                'api_credential_updated_by' => null
            ],
            [
                'api_credential_fullname' => 'Passport',
                'api_credential_email' => 'passport@epsilo.io',
                'api_credential_password' => md5(123456),
                'fk_config_active' => 1,
                'api_credential_created_at' => 1568651400,
                'api_credential_updated_at' => null,
                'api_credential_created_by' => 1,
                'api_credential_updated_by' => null
            ]
        ]);
    }
}
