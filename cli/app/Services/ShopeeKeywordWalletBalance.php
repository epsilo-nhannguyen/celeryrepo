<?php


namespace App\Services;


use App\Library\ElasticSearch;
use Illuminate\Support\Collection;

class ShopeeKeywordWalletBalance
{
    protected $shopChannelId;

    protected $elasticSearch;

    protected static $cacheResponse = [];

    public function __construct($shopChannelId)
    {
        $this->shopChannelId = $shopChannelId;
        $this->elasticSearch = new ElasticSearch();
    }

    /**
     * @param $date
     * @return Collection
     */
    public function getListByDate($date)
    {
        if (isset(static::$cacheResponse[$this->shopChannelId][$date])) {
            return static::$cacheResponse[$this->shopChannelId][$date];
        }
        $dateFrom = strtotime($date.' 00:00:00');
        $dateTo = strtotime($date.' 23:59:59');
        $list = $this->getData($dateFrom, $dateTo);

        $listGrouped = $list->groupBy(function ($item) {
            return date('H', $item['Timestamp']);
        });

        $listGrouped->transform(function ($item, $key) {
            $arrayWalletValue = [];
            foreach ($item as $i) {
                $arrayWalletValue[] = $i['wallet'] ?? 0;
            }

            return min($arrayWalletValue);
        });

        static::$cacheResponse[$this->shopChannelId][$date] = $listGrouped;

        return $listGrouped;
    }

    public function getData($dateFrom, $dateTo)
    {
        $size = 500;
        $result = collect([]);
        $balanceData = $this->_getData(0, $size, $dateFrom, $dateTo);
        $result = $result->merge($this->_processBalanceData($balanceData));

        $totalPage = $balanceData['hits']['total'] ?? 0;
        for ($i = 1; $i < ceil($totalPage/$size); $i ++) {
            $page = $size * $i + 1;
            $balanceData = $this->_getData($page, $size, $dateFrom, $dateTo);
            $result = $result->merge($this->_processBalanceData($balanceData));
        }

        return $result;
    }

    protected function _getData($from, $size, $dateFrom, $dateTo)
    {
        $response = $this->elasticSearch->search('balance-*', [
            '_source' => [
                'includes' => ['wallet', 'Timestamp', 'shopChannelId']
            ],
            'from' => $from,
            'size' => $size,
            'query' => [
                'bool' => [
                    'filter' => [
                        'range' => [
                            'Timestamp' => [
                                'gte' => $dateFrom,
                                'lte' => $dateTo,
                            ]
                        ]
                    ],
                    'must' => [
                        'match' => [
                            'shopChannelId' => $this->shopChannelId
                        ]
                    ],
                ],

            ],
        ]);

        return $response;
    }

    /**
     * process Balance Data
     * @param array $balanceData
     */
    private function _processBalanceData($balanceData)
    {
        return array_column($balanceData['hits']['hits'] ?? [], '_source');
    }

}