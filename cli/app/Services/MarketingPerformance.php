<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 10:36
 */

namespace App\Services;


use App\Library;
use App\Models;


class MarketingPerformance
{

    # ItemSold
    /**
     * get Item Sold Keyword
     * @param int $keywordProductId
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     */
    public static function getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo)
    {
        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD;
        return intval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    # View
    /**
     * get View Keyword
     * @param int $keywordProductId
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     */
    public static function getViewKeyword($keywordProductId, $dateFrom, $dateTo)
    {
        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_VIEW;
        return intval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    # Click
    /**
     * get Click Keyword
     * @param int $keywordProductId
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     */
    public static function getClickKeyword($keywordProductId, $dateFrom, $dateTo)
    {
        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_CLICKS;
        return intval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    # Cost
    /**
     * get Cost Keyword
     * @param int $keywordProductId
     * @param string $dateFrom
     * @param string $dateTo
     * @return float
     */
    public static function getCostKeyword($keywordProductId, $dateFrom, $dateTo)
    {
        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_EXPENSE;
        return floatval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    # Gmv
    /**
     * get Gmv Keyword
     * @param int $keywordProductId
     * @param string $dateFrom
     * @param string $dateTo
     * @return float
     */
    public static function getGmvKeyword($keywordProductId, $dateFrom, $dateTo)
    {
        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_GMV;
        return floatval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    # ItemSold
    /**
     * get Item Sold
     * @param int $productShopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     */
    public static function getItemSoldProduct($productShopChannelId, $dateFrom, $dateTo)
    {
        $keywordProductId = array_column(
            Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByProductShopChannelId($productShopChannelId)),
            Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
        );

        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD;
        return intval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    # View
    /**
     * get View
     * @param int $productShopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     */
    public static function getViewProduct($productShopChannelId, $dateFrom, $dateTo)
    {
        $keywordProductId = array_column(
            Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByProductShopChannelId($productShopChannelId)),
            Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
        );

        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_VIEW;
        return intval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    # Click
    /**
     * get Click
     * @param int $productShopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     */
    public static function getClickProduct($productShopChannelId, $dateFrom, $dateTo)
    {
        $keywordProductId = array_column(
            Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByProductShopChannelId($productShopChannelId)),
            Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
        );

        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_CLICKS;
        return intval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    # Cost
    /**
     * get Cost
     * @param int $productShopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return float
     */
    public static function getCostProduct($productShopChannelId, $dateFrom, $dateTo)
    {
        $keywordProductId = array_column(
            Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByProductShopChannelId($productShopChannelId)),
            Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
        );

        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_EXPENSE;
        return floatval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

    #gmv
    /**
     * get Gmv
     * @param int $productShopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return float
     */
    public static function getGmvProduct($productShopChannelId, $dateFrom, $dateTo)
    {
        $keywordProductId = array_column(
            Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByProductShopChannelId($productShopChannelId)),
            Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
        );

        $columnSum = Models\MakPerformance::COL_MAK_PERFORMANCE_GMV;
        return floatval(Models\MakPerformance::sumColumnByDateFromTo($columnSum, $keywordProductId, $dateFrom, $dateTo));
    }

}