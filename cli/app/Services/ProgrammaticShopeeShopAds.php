<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 10:32
 */

namespace App\Services;


use App\Library;
use App\Models;
use App\Repository\RepositoryBusiness\DTO\DTOShopAds;
use App\Repository\RepositoryBusiness\DTO\DTOShopAdsKeyword;


class ProgrammaticShopeeShopAds
{
    /**
     * turn Status Shop (Stop, Resume)
     * @param int $shopChannelId
     */
    public static function turnStatusShop($shopChannelId)
    {
        $shopAdsInfo = Models\ModelBusiness\ShopAds::getByShopChannelId($shopChannelId);
        if ( ! $shopAdsInfo) return;

        $objectiveId = Models\ModelBusiness\ShopAdsObjective::SHOP;

        $assocMetricIdData = [];
        $metricData = Models\ModelBusiness\ShopAdsMetric::searchByObjectiveId($objectiveId);
        foreach ($metricData as $metricInfo) {
            $assocMetricIdData[$metricInfo->id] = $metricInfo;
        }

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $dateTo = date('Y-m-d');
        $actionIdArray = [
            Models\ModelBusiness\ShopAdsAction::PAUSE_SHOP_ADS,
            Models\ModelBusiness\ShopAdsAction::RESUME_SHOP_ADS,
        ];

        $shopAdsId = $shopAdsInfo->id;

        $settingShopData = Models\ModelBusiness\ShopAdsSettingShop::searchByShopAdsId($shopAdsId, $actionIdArray);
        foreach ($settingShopData as $settingShopInfo) {
            $ruleId = $settingShopInfo->id;
            $actionId = $settingShopInfo->shop_ads_action_id;

            $logShopArray = [];
            $isValid = true;

            $conditionData = Models\ModelBusiness\ShopAdsCondition::searchByRuleId($ruleId);
            foreach ($conditionData as $conditionInfo) {
                $metricId = $conditionInfo->shop_ads_metric_id;
                $operatorId = $conditionInfo->shop_ads_operator_id;
                $ruleValue = $conditionInfo->value;

                $metricName = $assocMetricIdData[$metricId]->name;
                $metricValue = $assocMetricIdData[$metricId]->value;

                $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));
                list($columnPerformance, $ruleValue) = self::_convertMetricToShopPerformance($metricName, $ruleValue);

                $data = Models\ModelBusiness\ShopDataPerformance::sumColumnByDateFromTo($columnPerformance, $shopChannelId, $dateFrom, $dateTo);

                $isValidData = self::_compareDataWithRuleValue($operatorId, $data, $ruleValue);

                if ($isValidData) {
                    $checkpoint = $metricName.' last '.$metricValue.' days = '.$data;
                    $logShopArray[] = Models\ModelBusiness\ShopAdsLogShop::buildDataInsert(
                        $shopAdsId, $actionId, Models\Admin::EPSILO_SYSTEM, $ruleId, $checkpoint
                    );
                }

                $isValid = $isValid && $isValidData;
            }

            if ($isValid && count($conditionData) > 0) {
                if ( ! $isLogin) $isLogin = $crawler->login();
                if ( ! $isLogin) $isLogin = $crawler->login();
                if ( ! $isLogin) $isLogin = $crawler->login();

                $shopAdsInfo = $crawler->crawlShopAdsKeyword();
                $statusChannel = $shopAdsInfo['data']['campaign_ads_list'][0]['campaign']['status'] ?? 0;
                if ( ! $statusChannel) {
                    $shopAdsInfo = $crawler->crawlShopAdsKeyword();
                    $statusChannel = $shopAdsInfo['data']['campaign_ads_list'][0]['campaign']['status'] ?? 0;
                }
                if ( ! $statusChannel) {
                    $shopAdsInfo = $crawler->crawlShopAdsKeyword();
                    $statusChannel = $shopAdsInfo['data']['campaign_ads_list'][0]['campaign']['status'] ?? 0;
                }
                if ( ! $statusChannel) {
                    $shopAdsInfo = $crawler->crawlShopAdsKeyword();
                    $statusChannel = $shopAdsInfo['data']['campaign_ads_list'][0]['campaign']['status'] ?? 0;
                }
                # 1 = ongoing, 2 paused
                if ($actionId == Models\ModelBusiness\ShopAdsAction::PAUSE_SHOP_ADS) {
                    if ( ! $statusChannel || $statusChannel != DTOShopAds::ONGOING) continue;

                    $shopAdsDto = new DTOShopAds();
                    $shopAdsDto->setStatus(DTOShopAds::PAUSE);

                    $DTOResponse = $crawler->editShopAds($shopAdsDto);
                    if($DTOResponse->getIsSuccess()) {
                        Models\ModelBusiness\ShopAds::updateStatusById($shopAdsId, 'pause', json_encode($shopAdsInfo), Models\Admin::EPSILO_SYSTEM);

                        Models\ModelBusiness\ShopAdsLogShop::batchInsert($logShopArray);
                    }
                } elseif ($actionId == Models\ModelBusiness\ShopAdsAction::RESUME_SHOP_ADS) {
                    # 1 = ongoing, 2 paused
                    if ( ! $statusChannel || $statusChannel != DTOShopAds::PAUSE) continue;
                    $shopAdsDto = new DTOShopAds();
                    $shopAdsDto->setStatus(DTOShopAds::ONGOING);

                    $DTOResponse = $crawler->editShopAds($shopAdsDto);
                    if($DTOResponse->getIsSuccess()) {
                        Models\ModelBusiness\ShopAds::updateStatusById($shopAdsId, 'on-going', json_encode($shopAdsInfo), Models\Admin::EPSILO_SYSTEM);

                        Models\ModelBusiness\ShopAdsLogShop::batchInsert($logShopArray);
                    }
                }
            }
        }
    }

    /**
     * turn Status Shop Ads Keyword (Stop, Resume)
     * @param int $shopChannelId
     */
    public static function turnStatusShopAdsKeyword($shopChannelId)
    {
        $shopAdsInfo = Models\ModelBusiness\ShopAds::getByShopChannelId($shopChannelId);
        if ( ! $shopAdsInfo) return;

        $objectiveId = Models\ModelBusiness\ShopAdsObjective::SHOP_KEYWORD;
        
        $assocMetricIdData = [];
        $metricData = Models\ModelBusiness\ShopAdsMetric::searchByObjectiveId($objectiveId);
        foreach ($metricData as $metricInfo) {
            $assocMetricIdData[$metricInfo->id] = $metricInfo;
        }

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $assocKeywordStatus = [];

        $dateTo = date('Y-m-d');
        $actionIdArray = [
            Models\ModelBusiness\ShopAdsAction::RESTORE_SHOP_ADS_KEYWORD,
            Models\ModelBusiness\ShopAdsAction::DELETE_SHOP_ADS_KEYWORD,
        ];
        $shopAdsId = $shopAdsInfo->id;

        $settingKeywordData = Models\ModelBusiness\ShopAdsSettingKeyword::searchByShopAdsId($shopAdsId, $actionIdArray);
        foreach ($settingKeywordData as $settingKeywordInfo) {
            $ruleId = $settingKeywordInfo->id;
            $ruleExtend = $settingKeywordInfo->extend;
            $actionId = $settingKeywordInfo->shop_ads_action_id;

            $conditionData = Models\ModelBusiness\ShopAdsCondition::searchByRuleId($ruleId);
            $ruleKeywordData = Models\ModelBusiness\ShopAdsSettingKeyword::searchByRuleId($ruleId);
            foreach ($ruleKeywordData as $ruleKeywordInfo) {
                $keywordTurnOnArray = $keywordTurnOffArray = ['keyword' => [], 'shopAdsKeywordId' => []];

                $assocActionIdLogKeyword = [];

                $isValid = true;
                $logKeywordArray = [];

                $keywordName = $ruleKeywordInfo->mak_programmatic_keyword_name;
                $shopAdsKeywordId = $ruleKeywordInfo->id;
                $isPosition = $ruleKeywordInfo->is_shop_ads_position;

                foreach ($conditionData as $conditionInfo) {
                    $metricId = $conditionInfo->shop_ads_metric_id;
                    $operatorId = $conditionInfo->shop_ads_operator_id;
                    $ruleValue = $conditionInfo->value;

                    $metricName = $assocMetricIdData[$metricId]->name;
                    $metricValue = $assocMetricIdData[$metricId]->value;

                    if ($metricId != Models\ModelBusiness\ShopAdsMetric::KEYWORD_TOP_1) {
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));
                        list($columnPerformance, $ruleValue) = self::_convertMetricToKeywordPerformance($metricName, $ruleValue);
                        $data = Models\ModelBusiness\ShopAdsKeywordPerformance::sumColumnByDateFromTo($columnPerformance, $shopAdsKeywordId, $dateFrom, $dateTo);
                    } else {
                        $data = $isPosition;
                    }
                    $isValidData = self::_compareDataWithRuleValue($operatorId, $data, $ruleValue);

                    if ($isValidData) {
                        if ($metricId != Models\ModelBusiness\ShopAdsMetric::KEYWORD_TOP_1) {
                            $checkpoint = $metricName.' last '.$metricValue.' days = '.$data;
                        } else {
                            $checkpoint = 'Is keyword top 1 ? '. ($data ? '(Yes)' : '(No)');
                        }

                        $logKeywordArray[] = Models\ModelBusiness\ShopAdsLogKeyword::buildDataInsert(
                            $shopAdsKeywordId, $actionId, Models\Admin::EPSILO_SYSTEM, $ruleId, $checkpoint
                        );
                    }

                    $isValid = $isValid && $isValidData;
                }
                if ($isValid && count($conditionData) > 0) {
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();

                    if ( ! $assocKeywordStatus) {
                        $shopAdsInfo = $crawler->crawlShopAdsKeyword();
                        $assocKeywordStatus = $shopAdsInfo['data']['campaign_ads_list'][0]['advertisements'][0]['extinfo']['keywords'] ?? [];
                        $assocKeywordStatus = array_column($assocKeywordStatus,'status', 'keyword');
                    }
                    if ( ! $assocKeywordStatus) {
                        $shopAdsInfo = $crawler->crawlShopAdsKeyword();
                        $assocKeywordStatus = $shopAdsInfo['data']['campaign_ads_list'][0]['advertisements'][0]['extinfo']['keywords'] ?? [];
                        $assocKeywordStatus = array_column($assocKeywordStatus,'status', 'keyword');
                    }
                    if ( ! $assocKeywordStatus) {
                        $shopAdsInfo = $crawler->crawlShopAdsKeyword();
                        $assocKeywordStatus = $shopAdsInfo['data']['campaign_ads_list'][0]['advertisements'][0]['extinfo']['keywords'] ?? [];
                        $assocKeywordStatus = array_column($assocKeywordStatus,'status', 'keyword');
                    }
                    // 1 restore, 0 deleted
                    if ($actionId == Models\ModelBusiness\ShopAdsAction::RESTORE_SHOP_ADS_KEYWORD) {
                       if (isset($assocKeywordStatus[$keywordName]) && $assocKeywordStatus[$keywordName] == 0) {
                           $keywordTurnOnArray['keyword'][] = $keywordName;
                           $keywordTurnOnArray['shopAdsKeywordId'][] = $shopAdsKeywordId;
                           $assocActionIdLogKeyword[$actionId] = $logKeywordArray;    
                       }
                    } elseif ($actionId == Models\ModelBusiness\ShopAdsAction::DELETE_SHOP_ADS_KEYWORD) {
                        if (isset($assocKeywordStatus[$keywordName]) && $assocKeywordStatus[$keywordName] == 1) {
                            $keywordTurnOffArray['keyword'][] = $keywordName;
                            $keywordTurnOffArray['shopAdsKeywordId'][] = $shopAdsKeywordId;
                            $assocActionIdLogKeyword[$actionId] = $logKeywordArray;
                        }
                    }
                }

                $DTOShopAdsKeywordArray = [];
                if ($keywordTurnOnArray['keyword']) {
                    foreach ($keywordTurnOnArray['keyword'] as $keyword) {
                        $DTOShopAdsKeyword = new DTOShopAdsKeyword();
                        $DTOShopAdsKeyword->setKeywordName($keyword);
                        $DTOShopAdsKeyword->setStatus(1);

                        $DTOShopAdsKeywordArray[] = $DTOShopAdsKeyword;
                    }
                }

                if ($keywordTurnOffArray['keyword']) {
                    foreach ($keywordTurnOffArray['keyword'] as $keyword) {
                        $DTOShopAdsKeyword = new DTOShopAdsKeyword();
                        $DTOShopAdsKeyword->setKeywordName($keyword);
                        $DTOShopAdsKeyword->setStatus(0);

                        $DTOShopAdsKeywordArray[] = $DTOShopAdsKeyword;
                    }
                }

                $DTOResponse = $crawler->editShopAds(null, $DTOShopAdsKeywordArray);
                if($DTOResponse->getIsSuccess()) {
                    if ($keywordTurnOnArray['shopAdsKeywordId']) {
                        Models\ModelBusiness\ShopAdsKeyword::updateStatusByIdArray(
                            $keywordTurnOnArray['shopAdsKeywordId'], Models\ConfigActive::ACTIVE, json_encode($DTOResponse->getData()), Models\Admin::EPSILO_SYSTEM
                        );

                        $logKeyword = $assocActionIdLogKeyword[Models\ModelBusiness\ShopAdsAction::RESTORE_SHOP_ADS_KEYWORD] ?? [];
                        if ($logKeyword) {
                            Models\ModelBusiness\ShopAdsLogKeyword::batchInsert($logKeyword);
                        }
                    }

                    if ($keywordTurnOffArray['shopAdsKeywordId']) {
                        Models\ModelBusiness\ShopAdsKeyword::updateStatusByIdArray(
                            $keywordTurnOffArray['shopAdsKeywordId'], Models\ConfigActive::INACTIVE, json_encode($DTOResponse->getData()), Models\Admin::EPSILO_SYSTEM
                        );

                        $logKeyword = $assocActionIdLogKeyword[Models\ModelBusiness\ShopAdsAction::DELETE_SHOP_ADS_KEYWORD] ?? [];
                        if ($logKeyword) {
                            Models\ModelBusiness\ShopAdsLogKeyword::batchInsert($logKeyword);
                        }
                    }
                }
            }
        }
    }

    /**change Bidding Price (Decrease, Increase)
     * @param int $shopChannelId
     */
    public static function changeBiddingPrice($shopChannelId)
    {
        $shopAdsInfo = Models\ModelBusiness\ShopAds::getByShopChannelId($shopChannelId);
        if ( ! $shopAdsInfo) return;

        $objectiveId = Models\ModelBusiness\ShopAdsObjective::SHOP_KEYWORD;

        $assocMetricIdData = [];
        $metricData = Models\ModelBusiness\ShopAdsMetric::searchByObjectiveId($objectiveId);
        foreach ($metricData as $metricInfo) {
            $assocMetricIdData[$metricInfo->id] = $metricInfo;
        }

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $dateTo = date('Y-m-d');
        $actionIdArray = [
            Models\ModelBusiness\ShopAdsAction::INCREASE_BIDDING_PRICE,
            Models\ModelBusiness\ShopAdsAction::DECREASE_BIDDING_PRICE,
        ];
        $shopAdsId = $shopAdsInfo->id;

        $settingKeywordData = Models\ModelBusiness\ShopAdsSettingKeyword::searchByShopAdsId($shopAdsId, $actionIdArray);
        foreach ($settingKeywordData as $settingKeywordInfo) {
            $ruleId = $settingKeywordInfo->id;
            $ruleExtend = $settingKeywordInfo->extend;
            $actionId = $settingKeywordInfo->shop_ads_action_id;

            $conditionData = Models\ModelBusiness\ShopAdsCondition::searchByRuleId($ruleId);
            $ruleKeywordData = Models\ModelBusiness\ShopAdsSettingKeyword::searchByRuleId($ruleId);
            foreach ($ruleKeywordData as $ruleKeywordInfo) {
                $biddingPriceIncreaseArray = $biddingPriceDecreaseArray = ['assocKeywordPrice' => [], 'assocShopAdsKeywordIdPrice' => []];

                $assocActionIdLogKeyword = [];

                $isValid = true;
                $logKeywordArray = [];

                $keywordName = $ruleKeywordInfo->mak_programmatic_keyword_name;
                $shopAdsKeywordId = $ruleKeywordInfo->id;
                $biddingPrice = $ruleKeywordInfo->bidding_price;
                $isPosition = $ruleKeywordInfo->is_shop_ads_position;

                foreach ($conditionData as $conditionInfo) {
                    $metricId = $conditionInfo->shop_ads_metric_id;
                    $operatorId = $conditionInfo->shop_ads_operator_id;
                    $ruleValue = $conditionInfo->value;

                    $metricName = $assocMetricIdData[$metricId]->name;
                    $metricValue = $assocMetricIdData[$metricId]->value;

                    if ($metricId != Models\ModelBusiness\ShopAdsMetric::KEYWORD_TOP_1) {
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));
                        list($columnPerformance, $ruleValue) = self::_convertMetricToKeywordPerformance($metricName, $ruleValue);
                        $data = Models\ModelBusiness\ShopAdsKeywordPerformance::sumColumnByDateFromTo($columnPerformance, $shopAdsKeywordId, $dateFrom, $dateTo);
                    } else {
                        $data = $isPosition;
                    }
                    $isValidData = self::_compareDataWithRuleValue($operatorId, $data, $ruleValue);

                    if ($isValidData) {
                        if ($metricId != Models\ModelBusiness\ShopAdsMetric::KEYWORD_TOP_1) {
                            $checkpoint = $metricName.' last '.$metricValue.' days = '.$data;
                        } else {
                            $checkpoint = 'Is keyword top 1 ? '. ($data ? '(Yes)' : '(No)');
                        }

                        $logKeywordArray[] = Models\ModelBusiness\ShopAdsLogKeyword::buildDataInsert(
                            $shopAdsKeywordId, $actionId, Models\Admin::EPSILO_SYSTEM, $ruleId, $checkpoint
                        );
                    }

                    $isValid = $isValid && $isValidData;
                }

                if ($isValid && count($conditionData) > 0) {
                    if ($actionId == Models\ModelBusiness\ShopAdsAction::INCREASE_BIDDING_PRICE && $ruleExtend) {
                        $ruleExtendArray = json_decode($ruleExtend, true);
                        $param = trim($ruleExtendArray['param']);
                        $value = floatval($ruleExtendArray['value']);
                        $min = floatval($ruleExtendArray['min']);
                        $max = floatval($ruleExtendArray['max']);
                        if ($param == 'absolute') {
                            $biddingPrice += $value;
                        } else {
                            $biddingPrice += ($biddingPrice * $value/100);
                        }

                        if ($biddingPrice >= $min && $biddingPrice <= $max) {
                            $biddingPriceIncreaseArray['assocKeywordPrice'][$keywordName] = $biddingPrice;
                            $biddingPriceIncreaseArray['assocShopAdsKeywordIdPrice'][$shopAdsKeywordId] = $biddingPrice;
                            $assocActionIdLogKeyword[$actionId] = $logKeywordArray;
                        }

                    } elseif ($actionId == Models\ModelBusiness\ShopAdsAction::DECREASE_BIDDING_PRICE && $ruleExtend) {
                        $ruleExtendArray = json_decode($ruleExtend, true);
                        $param = trim($ruleExtendArray['param']);
                        $value = floatval($ruleExtendArray['value']);
                        $min = floatval($ruleExtendArray['min']);
                        $max = floatval($ruleExtendArray['max']);
                        if ($param == 'absolute') {
                            $biddingPrice -= $value;
                        } else {
                            $biddingPrice -= ($biddingPrice * $value/100);
                        }

                        if ($biddingPrice >= $min && $biddingPrice <= $max) {
                            $biddingPriceDecreaseArray['assocKeywordPrice'][$keywordName] = $biddingPrice;
                            $biddingPriceDecreaseArray['assocShopAdsKeywordIdPrice'][$shopAdsKeywordId] = $biddingPrice;
                            $assocActionIdLogKeyword[$actionId] = $logKeywordArray;
                        }
                    }
                }

                $totalIncreaseBiddingPrice = 0;
                $DTOShopAdsKeywordIncreaseArray = [];
                if ($biddingPriceIncreaseArray['assocKeywordPrice']) {
                    foreach ($biddingPriceIncreaseArray['assocKeywordPrice'] as $keyword => $biddingPrice) {
                        $totalIncreaseBiddingPrice = $totalIncreaseBiddingPrice + $biddingPrice;
                        $DTOShopAdsKeyword = new DTOShopAdsKeyword();
                        $DTOShopAdsKeyword->setKeywordName($keyword);
                        $DTOShopAdsKeyword->setBiddingPrice($biddingPrice);

                        $DTOShopAdsKeywordIncreaseArray[] = $DTOShopAdsKeyword;
                    }
                }

                $DTOShopAdsKeywordDecreaseArray = [];
                if ($biddingPriceDecreaseArray['assocKeywordPrice']) {
                    foreach ($biddingPriceDecreaseArray['assocKeywordPrice'] as $keyword => $biddingPrice) {
                        $DTOShopAdsKeyword = new DTOShopAdsKeyword();
                        $DTOShopAdsKeyword->setKeywordName($keyword);
                        $DTOShopAdsKeyword->setBiddingPrice($biddingPrice);

                        $DTOShopAdsKeywordDecreaseArray[] = $DTOShopAdsKeyword;
                    }
                }

                if ( ! $isLogin) $isLogin = $crawler->login();
                if ( ! $isLogin) $isLogin = $crawler->login();
                if ( ! $isLogin) $isLogin = $crawler->login();

                $balance = $crawler->getBalance();

                if ($biddingPriceIncreaseArray['assocKeywordPrice'] && $balance >= $totalIncreaseBiddingPrice) {
                    $DTOResponse = $crawler->editShopAds(null, $DTOShopAdsKeywordIncreaseArray);

                    if($DTOResponse->getIsSuccess()) {
                        foreach ((array) $biddingPriceIncreaseArray['assocShopAdsKeywordIdPrice'] as $shopKeywordId => $biddingPrice) {
                            Models\ModelBusiness\ShopAdsKeyword::updateBiddingPriceById(
                                $shopKeywordId, $biddingPrice, json_encode($DTOResponse->getData()),Models\Admin::EPSILO_SYSTEM
                            );
                        }

                        $logKeyword = $assocActionIdLogKeyword[Models\ModelBusiness\ShopAdsAction::INCREASE_BIDDING_PRICE] ?? [];
                        if ($logKeyword) {
                            Models\ModelBusiness\ShopAdsLogKeyword::batchInsert($logKeyword);
                        }
                    }
                }

                if ($biddingPriceDecreaseArray['assocKeywordPrice']) {
                    $DTOResponse = $crawler->editShopAds(null, $DTOShopAdsKeywordDecreaseArray);

                    if($DTOResponse->getIsSuccess()) {
                        foreach ((array) $biddingPriceDecreaseArray['assocShopAdsKeywordIdPrice'] as $shopKeywordId => $biddingPrice) {
                            Models\ModelBusiness\ShopAdsKeyword::updateBiddingPriceById(
                                $shopKeywordId, $biddingPrice, json_encode($DTOResponse->getData()), Models\Admin::EPSILO_SYSTEM
                            );
                        }

                        $logKeyword = $assocActionIdLogKeyword[Models\ModelBusiness\ShopAdsAction::DECREASE_BIDDING_PRICE] ?? [];
                        if ($logKeyword) {
                            Models\ModelBusiness\ShopAdsLogKeyword::batchInsert($logKeyword);
                        }
                    }
                }
            }
        }
    }

    /**
     * _convert Metric To Shop Performance
     * @param string $metricName
     * @param mixed $ruleValue
     * @return array
     */
    private static function _convertMetricToShopPerformance($metricName, $ruleValue)
    {
        $columnPerformance = '';
        switch ($metricName) {
            case 'Impression':
                $columnPerformance = 'view';
                $ruleValue = intval($ruleValue);
                break;
            case 'Click':
                $columnPerformance = 'click';
                $ruleValue = intval($ruleValue);
                break;
            case 'Item Sold':
                $columnPerformance = 'sold';
                $ruleValue = intval($ruleValue);
                break;
            case 'GMV':
                $columnPerformance = 'gmv';
                $ruleValue = floatval($ruleValue);
                break;
            case 'Cost Spent':
                $columnPerformance = 'cost';
                $ruleValue = floatval($ruleValue);
                break;
        }

        return [$columnPerformance, $ruleValue];
    }

    /**
     * _convert Metric To Keyword Performance
     * @param string $metricName
     * @param mixed $ruleValue
     * @return array
     */
    private static function _convertMetricToKeywordPerformance($metricName, $ruleValue)
    {
        $columnPerformance = '';
        switch ($metricName) {
            case 'Impression':
                $columnPerformance = 'view';
                $ruleValue = intval($ruleValue);
                break;
            case 'Click':
                $columnPerformance = 'click';
                $ruleValue = intval($ruleValue);
                break;
            case 'Item Sold':
                $columnPerformance = 'sold';
                $ruleValue = intval($ruleValue);
                break;
            case 'GMV':
                $columnPerformance = 'gmv';
                $ruleValue = floatval($ruleValue);
                break;
            case 'Cost Spent':
                $columnPerformance = 'expense';
                $ruleValue = floatval($ruleValue);
                break;
        }

        return [$columnPerformance, $ruleValue];
    }

    /**
     * _compare Data With Rule Value
     * @param int $operatorId
     * @param mixed $data
     * @param mixed $ruleValue
     * @return bool
     */
    private static function _compareDataWithRuleValue($operatorId, $data, $ruleValue)
    {
        $isValidData = false;

        switch ($operatorId) {
            case Models\ModelBusiness\ShopAdsOperator::EQUALS:
                $isValidData = $data == $ruleValue;
                break;
            case Models\ModelBusiness\ShopAdsOperator::GREATER_THAN:
                $isValidData = $data > $ruleValue;
                break;
            case Models\ModelBusiness\ShopAdsOperator::LESS_THAN:
                $isValidData = $data < $ruleValue;
                break;
            case Models\ModelBusiness\ShopAdsOperator::GREATER_THAN_OR_EQUAL_TO:
                $isValidData = $data >= $ruleValue;
                break;
            case Models\ModelBusiness\ShopAdsOperator::LESS_THAN_OR_EQUAL_TO:
                $isValidData = $data <= $ruleValue;
                break;
        }

        return $isValidData;
    }

}