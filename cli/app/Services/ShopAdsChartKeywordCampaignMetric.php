<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 03/12/2019
 * Time: 10:01
 */

namespace App\Services;


use App\Library;
use App\Models;


class ShopAdsChartKeywordCampaignMetric
{
    /**
     * @var array
     */
    private $actionSku = [
        Models\MakProgrammaticAction::PAUSE_SKU,
        Models\MakProgrammaticAction::RESUME_SKU
    ];
    private $actionKeyword = [
        Models\MakProgrammaticAction::DEACTIVATE_KEYWORD,
        Models\MakProgrammaticAction::ACTIVATE_KEYWORD
    ];

    /**
     * @var int
     */
    protected $shopChannelId = 0;
    protected $shopId = 0;
    protected $channelId = 0;
    protected $ventureId = 0;

    /**
     * @var array
     */
    protected $dateArray = [];

    /**
     * @var bool
     */
    protected $isRunAgain = false;

    /**
     * @return int
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     * @return $this
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
        return $this;
    }

    /**
     * @return int
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param int $channelId
     * @return $this
     */
    public function setChannelId($channelId)
    {
        $this->channelId = $channelId;
        return $this;
    }

    /**
     * @return int
     */
    public function getVentureId()
    {
        return $this->ventureId;
    }

    /**
     * @param int $ventureId
     * @return $this
     */
    public function setVentureId($ventureId)
    {
        $this->ventureId = $ventureId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRunAgain()
    {
        return $this->isRunAgain;
    }

    /**
     * @param bool $isRunAgain
     * @return $this
     */
    public function setIsRunAgain($isRunAgain)
    {
        $this->isRunAgain = $isRunAgain;
        return $this;
    }

    /**
     * @return int
     */
    public function getShopChannelId()
    {
        return $this->shopChannelId;
    }

    /**
     * @param int $shopChannelId
     * @return $this
     */
    public function setShopChannelId($shopChannelId)
    {
        $this->shopChannelId = $shopChannelId;
        return $this;
    }

    /**
     * @return array
     */
    public function getDateArray()
    {
        return $this->dateArray;
    }

    /**
     * @param array $dateArray
     * @return $this
     */
    public function setDateArray($dateArray)
    {
        $this->dateArray = $dateArray;
        return $this;
    }

    /**
     * @var array
     */
    private $currencyIdArray = [];

    public function __construct()
    {
        $this->currencyIdArray = array_column(
            Library\Formater::stdClassToArray(Models\Currency::getAll()),
            Models\Currency::COL_CURRENCY_ID
        );
    }

    /**
     * execute
     */
    public function execute()
    {
        $shopChannelId = $this->getShopChannelId();
        $ventureId = $this->getVentureId();

        foreach ($this->getDateArray() as $date) {
            $campaignMetricInsertArray = $campaignMetricUpdateCase = [];

            # begin process campaign metric
            $yearMonth = date('Y-m', strtotime($date));
            $campaignMetricData = Models\ModelChartShopAds\ChartKeywordCampaignMetric::searchByShopChannelIdAndDate($shopChannelId, $date);

            echo PHP_EOL, 'begin get performance campaign ' . microtime(), PHP_EOL;
            $performanceData = Models\ModelBusiness\ShopDataPerformance::getPerformance($shopChannelId, $date);
            foreach ($performanceData as $performance) {
                $campaignId = $performance->id;
                $impression = $performance->view;
                $click = $performance->click;
                $itemSold = $performance->sold;
                $gmv = $performance->gmv;
                $cost = $performance->cost;

                $ctr = $impression > 0 ? $click / $impression * 100 : 0;
                $cr = $click > 0 ? $itemSold / $click * 100 : 0;
                $cpc = $click > 0 ? $cost / $click : 0;
                $cpi = $itemSold > 0 ? $cost / $itemSold : 0;
                $cir = $gmv > 0 ? $cost / $gmv * 100 : 0;

                $totalKeyword = count(Models\ModelBusiness\ShopAdsKeyword::getByShopAdsId($campaignId)->toArray());
                list($totalSku, $totalKeyword) = [0, $totalKeyword];
                $saleOrderData = Models\Mongo\SaleOrder::getDataSaleOrderDateFromTo(
                    $this->getShopId(), $this->getChannelId(), Library\Common::dateFrom($date), Library\Common::dateTo($date)
                );
                $saleOrderData = (array) current((array) $saleOrderData);

                $gmvChannel = floatval($saleOrderData['totalPrice'] ?? 0);
                $itemSoldChannel = intval($saleOrderData['totalItem'] ?? 0);
                $percentGmvGmvChannel = $gmvChannel > 0 ? $gmv / $gmvChannel * 100 : 0;
                $percentItemSoldItemSoldChannel = $itemSoldChannel > 0 ? $itemSold / $itemSoldChannel * 100 : 0;

                $assocCurrencyIdGmv = Library\UniversalCurrency::buildCurrencyData($gmv, $yearMonth, $ventureId);
                $assocCurrencyIdCost = Library\UniversalCurrency::buildCurrencyData($cost, $yearMonth, $ventureId);
                $assocCurrencyIdCpc = Library\UniversalCurrency::buildCurrencyData($cpc, $yearMonth, $ventureId);
                $assocCurrencyIdCpi = Library\UniversalCurrency::buildCurrencyData($cpi, $yearMonth, $ventureId);
                $assocCurrencyIdGmvChannel = Library\UniversalCurrency::buildCurrencyData($gmvChannel, $yearMonth, $ventureId);

                foreach ($this->currencyIdArray as $currencyId) {
                    $campaignMetricId = null;
                    foreach ($campaignMetricData as $campaignMetric) {
                        if ($campaignMetric->{Models\ModelChart\ChartKeywordCampaignMetric::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID} == $campaignId
                            && $campaignMetric->{Models\ModelChart\ChartKeywordCampaignMetric::COL_CURRENCY_ID} == $currencyId) {
                            $campaignMetricId = $campaignMetric->{Models\ModelChart\ChartKeywordCampaignMetric::COL_ID};
                            break;
                        }
                    }

                    if ($campaignMetricId) {
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_IMPRESSION][$campaignMetricId] = $impression;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_CLICK][$campaignMetricId] = $click;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_ITEM_SOLD][$campaignMetricId] = $itemSold;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_GMV][$campaignMetricId] = $assocCurrencyIdGmv[$currencyId];
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_COST][$campaignMetricId] = $assocCurrencyIdCost[$currencyId];
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_CTR][$campaignMetricId] = $ctr;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_CR][$campaignMetricId] = $cr;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_CPC][$campaignMetricId] = $assocCurrencyIdCpc[$currencyId];
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_CPI][$campaignMetricId] = $assocCurrencyIdCpi[$currencyId];
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_CIR][$campaignMetricId] = $cir;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_TOTAL_SKU][$campaignMetricId] = $totalSku;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_TOTAL_KEYWORD][$campaignMetricId] = $totalKeyword;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_TOTAL_GMV][$campaignMetricId] = $assocCurrencyIdGmvChannel[$currencyId];
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_TOTAL_ITEM_SOLD][$campaignMetricId] = $itemSoldChannel;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_PERCENT_GMV_DIVISION_TOTAL][$campaignMetricId] = $percentGmvGmvChannel;
                        $campaignMetricUpdateCase[Models\ModelChartShopAds\ChartKeywordCampaignMetric::COL_PERCENT_ITEM_SOLD_DIVISION_TOTAL][$campaignMetricId] = $percentItemSoldItemSoldChannel;
                    } else {
                        $campaignMetricInsertArray[] = Models\ModelChartShopAds\ChartKeywordCampaignMetric::buildDataInsert(
                            $shopChannelId, $campaignId, $date, $impression, $click, $itemSold, $assocCurrencyIdGmv[$currencyId], $assocCurrencyIdCost[$currencyId],
                            $ctr, $cr, $assocCurrencyIdCpc[$currencyId], $assocCurrencyIdCpi[$currencyId], $cir, $totalSku, $totalKeyword,
                            $assocCurrencyIdGmvChannel[$currencyId], $itemSoldChannel, $percentGmvGmvChannel, $percentItemSoldItemSoldChannel, $currencyId
                        );
                    }
                }
            }
            # end process campaign metric
            echo 'end get performance campaign ' . microtime(), PHP_EOL;

            # begin insert campaign metric
            echo '# begin insert campaign metric ' . microtime(), PHP_EOL;
            $campaignMetricInsertChunk = array_chunk($campaignMetricInsertArray, 200);
            foreach ($campaignMetricInsertChunk as $chunk) {
                Models\ModelChartShopAds\ChartKeywordCampaignMetric::batchInsert($chunk);
            }
            echo '# end insert campaign metric ' . microtime(), PHP_EOL;
            # end insert campaign metric

            $this->_updateBatch($campaignMetricUpdateCase);
        }

    }

    /**
     * update Batch
     * @param $data
     */
    private function _updateBatch($data)
    {
        # begin update campaign metric
        echo '# begin update campaign metric ' . microtime(), PHP_EOL;
        $campaignMetricIdUpdate = [];
        foreach ($data as $columnUpdated => $assocIdValue) {
            if ( ! $campaignMetricIdUpdate) $campaignMetricIdUpdate = array_keys($assocIdValue);

            $assocIdValueChunk = array_chunk($assocIdValue, 200, true);
            foreach ($assocIdValueChunk as $chunk) {
                $updateCase = new Models\ModelChart\ChartKeywordCampaignMetric();
                $updateCase->setTableUpdated(Models\ModelChart\ChartKeywordCampaignMetric::TABLE_NAME);
                $updateCase->setColumnCondition(Models\ModelChart\ChartKeywordCampaignMetric::COL_ID);
                $updateCase->setColumnUpdated($columnUpdated);
                foreach ($chunk as $id => $value) {
                    $updateCase->addCase($id, $value);
                }

                $sql = $updateCase->assemble();
                Models\ModelChartShopAds\ChartKeywordCampaignMetric::updateCase($sql);
            }
        }
        if ($campaignMetricIdUpdate) {
            Models\ModelChartShopAds\ChartKeywordCampaignMetric::snapshotUpdateTime($campaignMetricIdUpdate);
        }
        echo '# end update campaign metric ' . microtime(), PHP_EOL;
        # end update campaign metric
    }

    /**
     * get Total Sku And Keyword
     * @param int $shopChannelId
     * @param int $campaignId
     * @param string $dateFrom
     * @param string $dateTo
     * @param bool $isRunAgain
     * @return array
     */
    private function _getTotalSkuAndKeyword($shopChannelId, $campaignId, $dateFrom, $dateTo, $isRunAgain)
    {
        if ( ! $isRunAgain) {
            $dateFrom = Library\Common::dateFrom($dateFrom);
            $dateTo = Library\Common::dateTo($dateTo);

            $productIdArrayInLog = array_values(array_unique(array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticLogSku::searchCampaignIdByDateFromTo(
                    $campaignId, $dateFrom, $dateTo, $this->actionSku
                )),
                Models\MakProgrammaticLogSku::COL_FK_PRODUCT_SHOP_CHANNEL
            )));

            $productIdArray = array_column(
                Library\Formater::stdClassToArray(Models\ProductAds::searchByCampaignIdAndState($campaignId, Models\ProductAds::STATE_ONGOING)),
                Models\ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL
            );

            $totalSku = count(array_diff($productIdArray, $productIdArrayInLog)) + count($productIdArrayInLog);

            $keywordProductIdArrayInLog = array_values(array_unique(array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticLogKeyword::searchCampaignIdByDateFromTo(
                    $campaignId, $dateFrom, $dateTo, $this->actionKeyword
                )),
                Models\MakProgrammaticLogKeyword::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )));

            $keywordProductIdArray = array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByCampaignIdAndState(
                    $campaignId, Models\MakProgrammaticKeywordProduct::STATUS_RESTORE
                )),
                Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            );

            $totalKeyword = count(array_diff($keywordProductIdArray, $keywordProductIdArrayInLog)) + count($keywordProductIdArrayInLog);
        } else {
            $keywordProductData = Library\Formater::stdClassToArray(Models\MakPerformance::searchByShopChannelIdAndViewGreater0($shopChannelId, $dateFrom));
            $assocCampaignIdProduct = array_count_values(array_column(
                $keywordProductData,
                Models\MakPerformance::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                Models\MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            ));
            $assocCampaignIdKeywordProduct = array_count_values(array_column(
                $keywordProductData,
                Models\MakPerformance::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            ));

            $totalSku = $assocCampaignIdProduct[$campaignId] ?? 0;
            $totalKeyword = $assocCampaignIdKeywordProduct[$campaignId] ?? 0;
        }

        return [$totalSku, $totalKeyword];
    }

}