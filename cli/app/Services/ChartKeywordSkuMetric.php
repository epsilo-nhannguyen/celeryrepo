<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 03/12/2019
 * Time: 10:01
 */

namespace App\Services;


use App\Library;
use App\Models;


class ChartKeywordSkuMetric
{
    /**
     * @var int
     */
    protected $shopChannelId = 0;
    protected $shopId = 0;
    protected $channelId = 0;

    /**
     * @var array
     */
    protected $dateArray = [];

    /**
     * @return int
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     * @return $this
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
        return $this;
    }

    /**
     * @return int
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param int $channelId
     * @return $this
     */
    public function setChannelId($channelId)
    {
        $this->channelId = $channelId;
        return $this;
    }

    /**
     * @return int
     */
    public function getShopChannelId()
    {
        return $this->shopChannelId;
    }

    /**
     * @param int $shopChannelId
     * @return $this
     */
    public function setShopChannelId($shopChannelId)
    {
        $this->shopChannelId = $shopChannelId;
        return $this;
    }

    /**
     * @return array
     */
    public function getDateArray()
    {
        return $this->dateArray;
    }

    /**
     * @param array $dateArray
     * @return $this
     */
    public function setDateArray($dateArray)
    {
        $this->dateArray = $dateArray;
        return $this;
    }

    public function __construct()
    {

    }

    /**
     * execute
     */
    public function execute()
    {
        $shopChannelId = $this->getShopChannelId();

        $filterSkuAll = 0;
        $filterSkuHaveSaleL14d = 1;
        $filterSkuNoSaleL14d = 2;
        $typeSkuArray = [
            $filterSkuAll, $filterSkuHaveSaleL14d, $filterSkuNoSaleL14d
        ];

        $assocProductLog = $this->_buildProductLog();
        foreach ($this->getDateArray() as $date) {
            $skuMetricInsertArray = $skuMetricUpdateCase = [];

            # begin for filter sku
            $assocProductIdItemSoldHaveSaleL14d = [];
            $before13days = Library\Common::convertTimestampToDate(strtotime($date.' -13 days'));
            echo PHP_EOL, 'begin get ProductShopChannel::sumPerformanceByShopChannelIdAndDateFromTo ' . microtime(), PHP_EOL;
            $performanceSkuL14dData = Models\ProductShopChannel::sumPerformanceByShopChannelIdAndDateFromTo($shopChannelId, $before13days, $date);
            echo 'end get ProductShopChannel::sumPerformanceByShopChannelIdAndDateFromTo ' . microtime(), PHP_EOL;
            foreach ($performanceSkuL14dData as $performance) {
                $productId = $performance->{Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID};
                $itemSold = $performance->{Models\MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD};

                if ($itemSold == 0) continue;

                if (isset($assocProductIdItemSoldHaveSaleL14d[$productId])) {
                    $assocProductIdItemSoldHaveSaleL14d[$productId] += $itemSold;
                } else {
                    $assocProductIdItemSoldHaveSaleL14d[$productId] = $itemSold;
                }
            }
            # end for filter sku

            # begin for filter keyword
            $assocProductIdHaveImpressionL30d = $assocProductIdItemSoldHaveSaleL30d = $assocProductIdSkuKeywordHaveSaleL30d = [];
            echo PHP_EOL, 'begin get MakProgrammaticKeywordProduct::sumPerformanceByShopChannelIdAndDateFromTo ' . microtime(), PHP_EOL;
            $before29days = Library\Common::convertTimestampToDate(strtotime($date.' -29 days'));
            $performanceL30dData = Models\MakProgrammaticKeywordProduct::sumPerformanceByShopChannelIdAndDateFromTo($shopChannelId, $before29days, $date);
            foreach ($performanceL30dData as $performance) {
                $productId = $performance->{Models\MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL};
                $keywordProductId = $performance->{Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID};
                $impression = $performance->{Models\MakPerformance::COL_MAK_PERFORMANCE_VIEW};
                $itemSold = $performance->{Models\MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD};

                if (isset($assocProductIdHaveImpressionL30d[$productId])) {
                    $assocProductIdHaveImpressionL30d[$productId] += $impression;
                } else {
                    $assocProductIdHaveImpressionL30d[$productId] = $impression;
                }

                if ($itemSold > 0) {
                    if (isset($assocProductIdItemSoldHaveSaleL30d[$productId])) {
                        $assocProductIdItemSoldHaveSaleL30d[$productId] += $itemSold;
                    } else {
                        $assocProductIdItemSoldHaveSaleL30d[$productId] = $itemSold;
                    }

                    $assocProductIdSkuKeywordHaveSaleL30d[$productId][$keywordProductId] = $itemSold;
                }
            }
            # end for filter keyword
            echo 'end get MakProgrammaticKeywordProduct::sumPerformanceByShopChannelIdAndDateFromTo ' . microtime(), PHP_EOL;

            # begin calculator total Sku
            $assocTypeSkuProductId = $assocTypeSkuKeywordProductId = [];
            $assocTypeSkuProductIdItemSoldHaveSale = $assocTypeSkuKeywordProductIdItemSoldHaveSale = [];
            $assocTypeSkuProductIdPresentDiscount = $assocTypeSkuProductIdHaveSellableStock = [];
            $assocTypeSkuProductIdHaveImpressionL30d = $assocTypeSkuProductIdHaveItemSoldL30d = [];
            $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale = [];

            $performanceData = Models\MakProgrammaticKeywordProduct::searchPerformanceByShopChannelIdAndDate($shopChannelId, $date);
            foreach ($performanceData as $performance) {
                $productId = $performance->{Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID};
                $keywordProductId = $performance->{Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID};
                $itemSold = $performance->{Models\MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD};

                # total number sku
                $isHaveSaleSku = isset($assocProductIdItemSoldHaveSaleL14d[$productId]) ?? null;
                if ($isHaveSaleSku) {
                    # $assocTypeSkuProductId: unique when cal
                    # total number sku
                    $assocTypeSkuProductId[$filterSkuHaveSaleL14d][] = $productId;
                    $assocTypeSkuKeywordProductId[$filterSkuHaveSaleL14d][] = $keywordProductId;
                    if ($itemSold > 0) {
                        if (isset($assocTypeSkuProductIdItemSoldHaveSale[$filterSkuHaveSaleL14d][$productId])) {
                            $assocTypeSkuProductIdItemSoldHaveSale[$filterSkuHaveSaleL14d][$productId] += $itemSold;
                        } else {
                            $assocTypeSkuProductIdItemSoldHaveSale[$filterSkuHaveSaleL14d][$productId] = $itemSold;
                        }
                        $assocTypeSkuKeywordProductIdItemSoldHaveSale[$filterSkuHaveSaleL14d][$keywordProductId] = $itemSold;
                    }

                    $assocHourProduct = $assocProductLog[$productId][$date] ?? [];
                    $sellableStockArray = array_column($assocHourProduct,1);
                    $sellableStock = intval(end($sellableStockArray) ?: 0);
                    if ( ! isset($assocTypeSkuProductIdPresentDiscount[$filterSkuHaveSaleL14d][$productId])) {
                        $presentDiscount = count($assocHourProduct) > 0 ? array_sum(array_column($assocHourProduct,0)) / count($assocHourProduct) : 0;
                        $assocTypeSkuProductIdPresentDiscount[$filterSkuHaveSaleL14d][$productId] = $presentDiscount;
                    }

                    if ( ! isset($assocTypeSkuProductIdHaveSellableStock[$filterSkuHaveSaleL14d][$productId]) && $sellableStock > 0) {
                        $assocTypeSkuProductIdHaveSellableStock[$filterSkuHaveSaleL14d][$productId] = $sellableStock;
                    }

                    if ( ! isset($assocTypeSkuProductIdHaveImpressionL30d[$filterSkuHaveSaleL14d][$productId])) {
                        $assocTypeSkuProductIdHaveImpressionL30d[$filterSkuHaveSaleL14d][$productId] = $assocProductIdHaveImpressionL30d[$productId] ?? [];
                    }

                    if ( ! isset($assocTypeSkuProductIdHaveItemSoldL30d[$filterSkuHaveSaleL14d][$productId])) {
                        $assocTypeSkuProductIdHaveItemSoldL30d[$filterSkuHaveSaleL14d][$productId] = $assocProductIdItemSoldHaveSaleL30d[$productId] ?? [];
                    }

                    if ( ! isset($assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuHaveSaleL14d])) {
                        $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuHaveSaleL14d] = [];
                    }
                    $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuHaveSaleL14d] = array_merge(
                        $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuHaveSaleL14d],
                        $assocProductIdSkuKeywordHaveSaleL30d[$productId] ?? []
                    );
                } else {
                    $assocTypeSkuProductId[$filterSkuNoSaleL14d][] = $productId;
                    $assocTypeSkuKeywordProductId[$filterSkuNoSaleL14d][] = $keywordProductId;
                    if ($itemSold > 0) {
                        if (isset($assocTypeSkuProductIdItemSoldHaveSale[$filterSkuNoSaleL14d][$productId])) {
                            $assocTypeSkuProductIdItemSoldHaveSale[$filterSkuNoSaleL14d][$productId] += $itemSold;
                        } else {
                            $assocTypeSkuProductIdItemSoldHaveSale[$filterSkuNoSaleL14d][$productId] = $itemSold;
                        }
                        $assocTypeSkuKeywordProductIdItemSoldHaveSale[$filterSkuNoSaleL14d][$keywordProductId] = $itemSold;
                    }

                    $assocHourProduct = $assocProductLog[$productId][$date] ?? [];
                    $sellableStockArray = array_column($assocHourProduct,1);
                    $sellableStock = intval(end($sellableStockArray) ?: 0);
                    if ( ! isset($assocTypeSkuProductIdPresentDiscount[$filterSkuNoSaleL14d][$productId])) {
                        $presentDiscount = count($assocHourProduct) > 0 ? array_sum(array_column($assocHourProduct,0)) / count($assocHourProduct) : 0;
                        $assocTypeSkuProductIdPresentDiscount[$filterSkuNoSaleL14d][$productId] = $presentDiscount;
                    }

                    if ( ! isset($assocTypeSkuProductIdHaveSellableStock[$filterSkuNoSaleL14d][$productId]) && $sellableStock > 0) {
                        $assocTypeSkuProductIdHaveSellableStock[$filterSkuNoSaleL14d][$productId] = $sellableStock;
                    }

                    if ( ! isset($assocTypeSkuProductIdHaveImpressionL30d[$filterSkuNoSaleL14d][$productId])) {
                        $assocTypeSkuProductIdHaveImpressionL30d[$filterSkuNoSaleL14d][$productId] = $assocProductIdHaveImpressionL30d[$productId] ?? [];
                    }

                    if ( ! isset($assocTypeSkuProductIdHaveItemSoldL30d[$filterSkuNoSaleL14d][$productId])) {
                        $assocTypeSkuProductIdHaveItemSoldL30d[$filterSkuNoSaleL14d][$productId] = $assocProductIdItemSoldHaveSaleL30d[$productId] ?? [];
                    }

                    if ( ! isset($assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuNoSaleL14d])) {
                        $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuNoSaleL14d] = [];
                    }
                    $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuNoSaleL14d] = array_merge(
                        $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuNoSaleL14d],
                        $assocProductIdSkuKeywordHaveSaleL30d[$productId] ?? []
                    );
                }
                $assocTypeSkuProductId[$filterSkuAll][] = $productId;
                $assocTypeSkuKeywordProductId[$filterSkuAll][] = $keywordProductId;
                if ($itemSold > 0) {
                    if (isset($assocTypeSkuProductIdItemSoldHaveSale[$filterSkuAll][$productId])) {
                        $assocTypeSkuProductIdItemSoldHaveSale[$filterSkuAll][$productId] += $itemSold;
                    } else {
                        $assocTypeSkuProductIdItemSoldHaveSale[$filterSkuAll][$productId] = $itemSold;
                    }
                    $assocTypeSkuKeywordProductIdItemSoldHaveSale[$filterSkuAll][$keywordProductId] = $itemSold;
                }
                $assocHourProduct = $assocProductLog[$productId][$date] ?? [];
                $sellableStockArray = array_column($assocHourProduct,1);
                $sellableStock = intval(end($sellableStockArray) ?: 0);
                if ( ! isset($assocTypeSkuProductIdPresentDiscount[$filterSkuAll][$productId])) {
                    $presentDiscount = count($assocHourProduct) > 0 ? array_sum(array_column($assocHourProduct,0)) / count($assocHourProduct) : 0;
                    $assocTypeSkuProductIdPresentDiscount[$filterSkuAll][$productId] = $presentDiscount;
                }

                if ( ! isset($assocTypeSkuProductIdHaveSellableStock[$filterSkuAll][$productId]) && $sellableStock > 0) {
                    $assocTypeSkuProductIdHaveSellableStock[$filterSkuAll][$productId] = $sellableStock;
                }

                if ( ! isset($assocTypeSkuProductIdHaveImpressionL30d[$filterSkuAll][$productId])) {
                    $assocTypeSkuProductIdHaveImpressionL30d[$filterSkuAll][$productId] = $assocProductIdHaveImpressionL30d[$productId] ?? [];
                }

                if ( ! isset($assocTypeSkuProductIdHaveItemSoldL30d[$filterSkuAll][$productId])) {
                    $assocTypeSkuProductIdHaveItemSoldL30d[$filterSkuAll][$productId] = $assocProductIdItemSoldHaveSaleL30d[$productId] ?? [];
                }

                if ( ! isset($assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuAll])) {
                    $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuAll] = [];
                }
                $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuAll] = array_merge(
                    $assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$filterSkuAll],
                    $assocProductIdSkuKeywordHaveSaleL30d[$productId] ?? []
                );
            }
            echo PHP_EOL, 'begin get ChartKeywordSkuMetric::searchByShopChannelIdAndDate ' . microtime(), PHP_EOL;
            # begin process sku metric
            $skuMetricData = Models\ChartKeywordSkuMetric::searchByShopChannelIdAndDate($shopChannelId, $date);
            echo 'end get ChartKeywordSkuMetric::searchByShopChannelIdAndDate ' . microtime(), PHP_EOL;
            foreach ($typeSkuArray as $type) {
                $totalSku = count(array_unique($assocTypeSkuProductId[$type] ?? []));
                $totalSkuKeyword = count($assocTypeSkuKeywordProductId[$type] ?? []);
                $totalSkuHaveSale = count($assocTypeSkuProductIdItemSoldHaveSale[$type] ?? []);
                $totalSkuKeywordHaveSale = count($assocTypeSkuKeywordProductIdItemSoldHaveSale[$type] ?? []);
                $totalPresentDiscount = array_sum($assocTypeSkuProductIdPresentDiscount[$type] ?? []);
                $totalSkuHaveSellableStock = count($assocTypeSkuProductIdHaveSellableStock[$type] ?? []);
                $totalSkuHaveImpressionL30d = count($assocTypeSkuProductIdHaveImpressionL30d[$type] ?? []);
                $totalSkuHaveSaleL30d = count($assocTypeSkuProductIdHaveItemSoldL30d[$type] ?? []);
                $totalSkuKeywordHaveSaleL30d = count($assocTypeSkuProductIdKeywordProductIdItemSoldHaveSale[$type] ?? []);

                $skuMetricId = null;
                foreach ($skuMetricData as $skuMetric) {
                    if ($skuMetric->{Models\ChartKeywordKeywordMetric::COL_TYPE} == $type) {
                        $skuMetricId = $skuMetric->{Models\ChartKeywordKeywordMetric::COL_ID};
                        break;
                    }
                }
                if ($skuMetricId) {
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_TOTAL_SKU][$skuMetricId] = $totalSku;
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_TOTAL_SKU_KEYWORD][$skuMetricId] = $totalSkuKeyword;
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_PERCENT_SKU_SALE_TOTAL_SKU][$skuMetricId] = $totalSkuHaveSale;
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_TOTAL_SKU_KEYWORD_SALE][$skuMetricId] = $totalSkuKeywordHaveSale;
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_AVG_PERCENT_DISCOUNT][$skuMetricId] = $totalPresentDiscount;
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_PERCENT_SKU_SELLABLE_STOCK][$skuMetricId] = $totalSkuHaveSellableStock;
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_TOTAL_SKU_IMPRESSION_L30][$skuMetricId] = $totalSkuHaveImpressionL30d;
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_TOTAL_SKU_SALE_L30][$skuMetricId] = $totalSkuHaveSaleL30d;
                    $skuMetricUpdateCase[Models\ChartKeywordSkuMetric::COL_TOTAL_SKU_KEYWORD_SALE_L30][$skuMetricId] = $totalSkuKeywordHaveSaleL30d;
                } else {
                    $skuMetricInsertArray[] = Models\ChartKeywordSkuMetric::buildDataInsert(
                        $shopChannelId, $date, $totalSku, $totalSkuKeyword, $totalSkuHaveSale, $totalSkuKeywordHaveSale, $totalPresentDiscount,
                        $totalSkuHaveSellableStock, $totalSkuHaveImpressionL30d, $totalSkuHaveSaleL30d, $totalSkuKeywordHaveSaleL30d, $type
                    );
                }
            }

            # begin insert sku metric
            echo '# begin insert sku metric ' . microtime(), PHP_EOL;
            $skuMetricInsertChunk = array_chunk($skuMetricInsertArray, 200);
            foreach ($skuMetricInsertChunk as $chunk) {
                Models\ChartKeywordSkuMetric::batchInsert($chunk);
            }
            echo '# end insert sku metric ' . microtime(), PHP_EOL;
            # end insert sku metric

            $this->_updateBatch($skuMetricUpdateCase);
        }
    }

    /**
     * update Batch
     * @param $data
     */
    private function _updateBatch($data)
    {
        echo '# begin update sku metric ' . microtime(), PHP_EOL;
        $skuMetricIdUpdate = [];
        foreach ($data as $columnUpdated => $assocIdValue) {
            if ( ! $skuMetricIdUpdate) $skuMetricIdUpdate = array_keys($assocIdValue);

            $assocIdValueChunk = array_chunk($assocIdValue, 200, true);
            foreach ($assocIdValueChunk as $chunk) {
                Models\ChartKeywordSkuMetric::setTableUpdated(Models\ChartKeywordSkuMetric::TABLE_NAME);
                Models\ChartKeywordSkuMetric::setColumnCondition(Models\ChartKeywordSkuMetric::COL_ID);
                Models\ChartKeywordSkuMetric::setColumnUpdated($columnUpdated);
                foreach ($chunk as $id => $value) {
                    Models\ChartKeywordSkuMetric::addCase($id, $value);
                }

                $sql = Models\ChartKeywordSkuMetric::assemble();
                Models\ChartKeywordSkuMetric::updateCase($sql);
            }
        }
        if ($skuMetricIdUpdate) {
            Models\ChartKeywordSkuMetric::snapshotUpdateTime($skuMetricIdUpdate);
        }
        echo '# end update sku metric ' . microtime(), PHP_EOL;
    }

    /**
     * build Product Log Data
     * @return array|void
     * @throws \MongoDB\Driver\Exception\Exception
     */
    private function _buildProductLog()
    {
        $dateArray = $this->getDateArray();
        $dateMin = current($dateArray);
        $dateMax = end($dateArray);

        if ( ! $dateMin || ! $dateMax) {
            return;
        }

        $assocProductLog = [];

        $productLogData = Models\Mongo\ProductLog::searchByShopIdChannelId($this->getShopId(), $this->getChannelId(), $dateMin, $dateMax);
        foreach ($productLogData as $productLog) {
            $productShopChannelId = $productLog['productShopChannelId'];
            $date = $productLog['date'];
            $hour = $productLog['hour'];
            $presentDiscount = $productLog['presentDiscount'];
            $sellableStock = $productLog['sellableStock'];
            $assocProductLog[$productShopChannelId][$date][$hour] = [$presentDiscount, $sellableStock];
        }

        return $assocProductLog;
    }

}