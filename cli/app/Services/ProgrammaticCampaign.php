<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 17:23
 */

namespace App\Services;


use App\Library;
use App\Models;


class ProgrammaticCampaign
{
    /**
     * @var array
     */
    public $arrayProductShopChannelId;
    public $dataProductAds;


    public function __construct($campaignId)
    {
        $productIdArray = array_column(
            Library\Formater::stdClassToArray(Models\ProductShopChannelCampaign::searchByCampaignId($campaignId)),
            Models\ProductShopChannelCampaign::COL_FK_PRODUCT_SHOP_CHANNEL
        );
        $dataProductAds = Library\Formater::stdClassToArray(Models\ProductAds::searchByProductIdArray($productIdArray));

        $this->arrayProductShopChannelId = array_column($dataProductAds, Models\ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL);
        $this->dataProductAds = $dataProductAds;
    }

    /**
     * calculate Cost Spend
     * @param string $dateFrom
     * @param string $dateTo
     * @return float
     */
    public function calculateCostSpend($dateFrom, $dateTo)
    {
        $totalCostSpend = 0;
        $dateArray = Library\Common::dayOfBetween($dateFrom, $dateTo, '+1 day');

        $arrayProductShopChannelId = $this->arrayProductShopChannelId;
        foreach ($arrayProductShopChannelId as $productShopChannelId) {
            $keywordProductIdArray = array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByProductShopChannelId($productShopChannelId)),
                Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            );

            $totalCostSpend += floatval(Models\MakPerformance::sumColumnByDateArray(Models\MakPerformance::COL_MAK_PERFORMANCE_EXPENSE, $keywordProductIdArray, $dateArray));
        }

        return $totalCostSpend;
    }

    /**
     * generate Campaign Code
     * @param int $organizationId
     * @return string
     */
    public static function generateCampaignCode($organizationId)
    {
        $codeMax = Models\MakProgrammaticCampaign::getMaxCampaignCode($organizationId);
        $nextSequence = str_replace('CAMP', '', $codeMax);
        $currentMax = intval(ltrim($nextSequence, '0'));
        return 'CAMP'.str_pad($currentMax + 1, 6, 0, STR_PAD_LEFT);
    }

}