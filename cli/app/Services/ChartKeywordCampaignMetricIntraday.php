<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 03/12/2019
 * Time: 10:01
 */

namespace App\Services;


use App\Application\Functions;
use App\Library;
use App\Models;


class ChartKeywordCampaignMetricIntraday
{

    # region
    /**
     * @var string
     */
    const MINUTE_MAX = ':59:59';
    const MINUTE_MIN = ':00:00';

    /**
     * @var int
     */
    const HOUR_MAX = 23;

    /**
     * @var array
     */
    private $actionSku = [
        Models\MakProgrammaticAction::PAUSE_SKU,
        Models\MakProgrammaticAction::RESUME_SKU
    ];
    private $actionKeyword = [
        Models\MakProgrammaticAction::DEACTIVATE_KEYWORD,
        Models\MakProgrammaticAction::ACTIVATE_KEYWORD
    ];

    /**
     * @var int
     */
    protected $shopChannelId = 0;
    protected $shopChannelCreatedAt = 0;
    protected $shopId = 0;
    protected $channelId = 0;
    protected $ventureId = 0;
    protected $forceIntraday = 0;
    protected $dateBeginForecast = null;

    /**
     * @var array
     */
    protected $dateArray = [];

    /**
     * @var bool
     */
    protected $isRunAgain = false;

    /**
     * @return int
     */
    public function getShopChannelCreatedAt()
    {
        return $this->shopChannelCreatedAt;
    }

    /**
     * @param int $shopChannelCreatedAt
     * @return $this
     */
    public function setShopChannelCreatedAt($shopChannelCreatedAt)
    {
        $this->shopChannelCreatedAt = $shopChannelCreatedAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     * @return $this
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
        return $this;
    }

    /**
     * @return int
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param int $channelId
     * @return $this
     */
    public function setChannelId($channelId)
    {
        $this->channelId = $channelId;
        return $this;
    }

    /**
     * @return int
     */
    public function getVentureId()
    {
        return $this->ventureId;
    }

    /**
     * @param int $ventureId
     * @return $this
     */
    public function setVentureId($ventureId)
    {
        $this->ventureId = $ventureId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRunAgain()
    {
        return $this->isRunAgain;
    }

    /**
     * @param bool $isRunAgain
     * @return $this
     */
    public function setIsRunAgain($isRunAgain)
    {
        $this->isRunAgain = $isRunAgain;
        return $this;
    }

    /**
     * @return int
     */
    public function getShopChannelId()
    {
        return $this->shopChannelId;
    }

    /**
     * @param int $shopChannelId
     * @return $this
     */
    public function setShopChannelId($shopChannelId)
    {
        $this->shopChannelId = $shopChannelId;
        return $this;
    }

    /**
     * @return array
     */
    public function getDateArray()
    {
        return $this->dateArray;
    }

    /**
     * @param array $dateArray
     * @return $this
     */
    public function setDateArray($dateArray)
    {
        $this->dateArray = $dateArray;
        return $this;
    }

    /**
     * @var bool
     */
    protected $isImportCampaignFuture = false;

    /**
     * @var array
     */
    private $currencyIdArray = [];

    public function __construct()
    {
        $this->currencyIdArray = array_column(
            Library\Formater::stdClassToArray(Models\Currency::getAll()),
            Models\Currency::COL_CURRENCY_ID
        );

    }

    /**
     * @return int
     */
    public function getForceIntraday()
    {
        return $this->forceIntraday;
    }

    /**
     * @param int $forceIntraday
     */
    public function setForceIntraday($forceIntraday)
    {
        $this->forceIntraday = $forceIntraday;
    }

    /**
     * @return null
     */
    public function getDateBeginForecast()
    {
        return $this->dateBeginForecast;
    }

    /**
     * @param null $dateBeginForecast
     */
    public function setDateBeginForecast($dateBeginForecast)
    {
        $this->dateBeginForecast = $dateBeginForecast;
    }

    /**
     * execute
     */
    public function execute()
    {
        $shopChannelId = $this->getShopChannelId();
        $ventureId = $this->getVentureId();

        $campaignIdArray = [];
        $serviceWalletBalance = new ShopeeKeywordWalletBalance($shopChannelId);
        foreach ($this->getDateArray() as $date) {
            $walletData = $serviceWalletBalance->getListByDate($date)->toArray();
            $campaignMetricIntradayInsertArray = $campaignMetricIntradayUpdateCase = [];

            $now = Library\Common::getCurrentTimestamp();
            if ($this->getForceIntraday()) {
                $currentDate = date('Y-m-d');
                $currentHour = 0;
            } else {
                $currentDate = Library\Common::convertTimestampToDate($now);
                $currentHour = date('H', $now);
            }

            $yearMonth = date('Y-m', strtotime($date));
            # begin process campaign metric intraday
            $datetimeArray = $this->_buildDatetime($date, $currentDate, $currentHour);
            $firstCampaign = Models\MakProgrammaticCampaign::getByShopChannelId($shopChannelId);
            if (!$firstCampaign) {
                return;
            }
            for ($i = 0; $i < count($datetimeArray); $i += 2) {
                $dateFrom = $datetimeArray[$i];
                $dateTo = $datetimeArray[$i+1];

                $campaignMetricIntradayDate = Library\Common::getDateFromDatetime($dateFrom);
                $campaignMetricIntradayHour = Library\Common::getHourFromDatetime($dateFrom);

                $campaignMetricIntradayData = Models\ModelChart\ChartKeywordCampaignMetricIntraday::searchByShopChannelIdAndDateHour(
                    $shopChannelId, $campaignMetricIntradayDate, $campaignMetricIntradayHour
                );

                echo PHP_EOL, 'begin get performance campaign intraday ' . microtime(), PHP_EOL;
                $performanceHourData = Models\ModelBusiness\MakShopPerformanceHour::performanceByDateHour(
                    $shopChannelId, $campaignMetricIntradayDate, $campaignMetricIntradayHour
                );

                if ( ! $performanceHourData->count()) {
                    $initData = new \stdClass();
                    $initData->total_view = 0;
                    $initData->total_click = 0;
                    $initData->total_item_sold = 0;
                    $initData->total_gmv = 0;
                    $initData->total_cost = 0;

                    $performanceHourData = collect([$initData]);
                }

                foreach ($performanceHourData as $performanceHour) {
                    $walletValue = $walletData[$campaignMetricIntradayHour] ?? null;
                    $campaignId = $firstCampaign->mak_programmatic_campaign_id;
                    $campaignIdArray[] = $campaignId;

                    $impression = $performanceHour->total_view;
                    $click = $performanceHour->total_click;
                    $itemSold = $performanceHour->total_item_sold;
                    $gmv = $performanceHour->total_gmv;
                    $cost = $performanceHour->total_cost;

                    $ctr = $impression > 0 ? $click / $impression * 100 : 0;
                    $cr = $click > 0 ? $itemSold / $click * 100 : 0;
                    $cpc = $click > 0 ? $cost / $click : 0;
                    $cpi = $itemSold > 0 ? $cost / $itemSold : 0;
                    $cir = $gmv > 0 ? $cost / $gmv * 100 : 0;

                    echo PHP_EOL, 'begin get _getTotalSkuAndKeyword intraday ' . microtime(), PHP_EOL;
                    list($totalSku, $totalKeyword) = $this->_getTotalSkuAndKeyword($shopChannelId, $campaignId, $dateFrom, $dateTo, $this->isRunAgain());
                    echo 'end get _getTotalSkuAndKeyword intraday ' . microtime(), PHP_EOL;

                    echo PHP_EOL, 'begin get getDataSaleOrderDateFromTo intraday ' . microtime(), PHP_EOL;
                    $saleOrderData = Models\Mongo\SaleOrder::getDataSaleOrderDateFromTo(
                        $this->getShopId(), $this->getChannelId(),
                        Library\Common::convertDatetimeToTimestamp($dateFrom), Library\Common::convertDatetimeToTimestamp($dateTo)
                    );
                    echo 'end get getDataSaleOrderDateFromTo intraday ' . microtime(), PHP_EOL;
                    $saleOrderData = (array) current((array) $saleOrderData);

                    $gmvChannel = floatval($saleOrderData['totalPrice'] ?? 0);
                    $itemSoldChannel = intval($saleOrderData['totalItem'] ?? 0);
                    $percentGmvGmvChannel = $gmvChannel > 0 ? $gmv / $gmvChannel * 100 : 0;
                    $percentItemSoldItemSoldChannel = $itemSoldChannel > 0 ? $itemSold / $itemSoldChannel * 100 : 0;

                    $assocCurrencyIdGmv = Library\UniversalCurrency::buildCurrencyData($gmv, $yearMonth, $ventureId);
                    $assocCurrencyIdCost = Library\UniversalCurrency::buildCurrencyData($cost, $yearMonth, $ventureId);
                    $assocCurrencyIdCpc = Library\UniversalCurrency::buildCurrencyData($cpc, $yearMonth, $ventureId);
                    $assocCurrencyIdCpi = Library\UniversalCurrency::buildCurrencyData($cpi, $yearMonth, $ventureId);
                    $assocCurrencyIdGmvChannel = Library\UniversalCurrency::buildCurrencyData($gmvChannel, $yearMonth, $ventureId);
                    $assocCurrencyIdWalletData = Library\UniversalCurrency::buildCurrencyData($walletValue, $yearMonth, $ventureId);

                    foreach ($this->currencyIdArray as $currencyId) {
                        $campaignMetricIntradayId = null;
                        foreach ($campaignMetricIntradayData as $campaignMetricIntraday) {
                            if ($campaignMetricIntraday->{Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID} == $campaignId
                                && $campaignMetricIntraday->{Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CURRENCY_ID} == $currencyId) {
                                $campaignMetricIntradayId = $campaignMetricIntraday->{Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_ID};
                                break;
                            }
                        }

                        if ($campaignMetricIntradayId) {
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_IMPRESSION][$campaignMetricIntradayId] = $impression;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CLICK][$campaignMetricIntradayId] = $click;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_ITEM_SOLD][$campaignMetricIntradayId] = $itemSold;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_GMV][$campaignMetricIntradayId] = $assocCurrencyIdGmv[$currencyId];
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_COST][$campaignMetricIntradayId] = $assocCurrencyIdCost[$currencyId];
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CTR][$campaignMetricIntradayId] = $ctr;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CR][$campaignMetricIntradayId] = $cr;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CPC][$campaignMetricIntradayId] = $assocCurrencyIdCpc[$currencyId];
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CPI][$campaignMetricIntradayId] = $assocCurrencyIdCpi[$currencyId];
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CIR][$campaignMetricIntradayId] = $cir;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_TOTAL_SKU][$campaignMetricIntradayId] = $totalSku;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_TOTAL_KEYWORD][$campaignMetricIntradayId] = $totalKeyword;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_TOTAL_GMV][$campaignMetricIntradayId] = $assocCurrencyIdGmvChannel[$currencyId];
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_TOTAL_ITEM_SOLD][$campaignMetricIntradayId] = $itemSoldChannel;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_PERCENT_GMV_TOTAL][$campaignMetricIntradayId] = $percentGmvGmvChannel;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_PERCENT_ITEM_SOLD_TOTAL][$campaignMetricIntradayId] = $percentItemSoldItemSoldChannel;
                            $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_WALLET_BALANCE][$campaignMetricIntradayId] = $assocCurrencyIdWalletData[$currencyId];
                        } else {
                            $campaignMetricIntradayInsertArray[] = Models\ModelChart\ChartKeywordCampaignMetricIntraday::buildDataInsert(
                                $shopChannelId, $campaignId, $campaignMetricIntradayDate, $campaignMetricIntradayHour, $impression, $click, $itemSold, $assocCurrencyIdGmv[$currencyId],
                                $assocCurrencyIdCost[$currencyId], $ctr, $cr, $assocCurrencyIdCpc[$currencyId], $assocCurrencyIdCpi[$currencyId], $cir, $totalSku,
                                $totalKeyword, $assocCurrencyIdGmvChannel[$currencyId], $itemSoldChannel, $percentGmvGmvChannel, $percentItemSoldItemSoldChannel, $currencyId, $assocCurrencyIdWalletData[$currencyId]
                            );
                        }
                    }
                }
                echo 'end get performance campaign intraday ' . microtime(), PHP_EOL;
            }
            # end process campaign metric intraday

            # begin insert campaign metric intraday
            echo '# begin insert campaign metric intraday ' . microtime(), PHP_EOL;
            $campaignMetricIntradayInsertChunk = array_chunk($campaignMetricIntradayInsertArray, 200);
            foreach ($campaignMetricIntradayInsertChunk as $chunk) {
                Models\ModelChart\ChartKeywordCampaignMetricIntraday::batchInsert($chunk);
            }
            echo '# end insert campaign metric intraday ' . microtime(), PHP_EOL;
            # end insert campaign metric intraday
            $this->_updateBatch($campaignMetricIntradayUpdateCase);
        }

        if ($this->isImportCampaignFuture) {
            sleep(1);

            $campaignIdArray = array_values(array_unique($campaignIdArray));

            $this->_importCampaignFutureIntraday($shopChannelId, $campaignIdArray);
        }
    }
    #endregion
    /**
     * import Campaign Future Intraday
     * @param int $shopChannelId
     * @param array $campaignIdArray
     */
    private function _importCampaignFutureIntraday($shopChannelId, $campaignIdArray)
    {
        $serviceWalletBalance = new ShopeeKeywordWalletBalance($shopChannelId);
        $now = Library\Common::getCurrentTimestamp();
        $currentDate = Library\Common::convertTimestampToDate($now);
        $currentHour = date('H', $now);

        $assocIntraday_hourCampaignIdCurrency_metric = $this->_getCampaignIntraday($shopChannelId, $currentDate);
        $assocContribution_hourCampaignIdMetric = $this->_getContribution($shopChannelId, $currentDate);
        sleep(1);

        #tinh lai contribution
        $contributionPaidAds = Models\ModelBusiness\MakShopPerformanceHour::getContribution($shopChannelId);
        $contributionPaidAds = $contributionPaidAds->pluck(null, 'hour');

        $isForeCast = Models\ModelChart\ChartKeywordCampaignMetricIntraday::IS_FORECAST;

        $campaignMetricIntradayInsertArray = $campaignMetricIntradayUpdateCase = [];
        foreach (range($currentHour, 23, 1) as $hourForecast) { # $hourForecast = T

            // chua build logic cho luc 0h, va luc 1h se thi T - 2 all metric = 0
            $assocContribution_campaignIdBeforeOneHourForecast = $assocContribution_hourCampaignIdMetric[$hourForecast - 1] ?? []; # T - 1
            $assocContribution_campaignIdHourForecast = $assocContribution_hourCampaignIdMetric[$hourForecast] ?? []; # T

            $assocIntradayForecast_campaignIdCampaignMetric = $assocIntraday_hourCampaignIdMetric[$hourForecast] ?? []; # T
            $assocIntradayBeforeOneHourForecast_campaignIdCampaignMetric = $assocIntraday_hourCampaignIdMetric[$hourForecast - 1] ?? []; # T - 1
            #$assocIntradayBeforeTwoHourForecast_campaignIdCampaignMetric = $assocIntraday_hourCampaignIdMetric[$hourForecast - 2] ?? []; # T - 2

            foreach ($campaignIdArray as $campaignId) {
                if (!isset($assocContribution_campaignIdBeforeOneHourForecast[$campaignId])) continue;

                foreach ($this->currencyIdArray as $currencyId) {
                    if ($hourForecast == $currentHour) {
                        /*
                        # if theo dieu kien o tren thi $campaignMetricIntraday_beforeOneHourForecast luon co cho condition $hourForecast == $currentHour
                        # if theo dieu kien o tren thi $campaignMetricIntraday_beforeTwoHourForecast thi luc 1h all metric se = 0

                        $campaignMetricIntraday_beforeOneHourForecast = $assocIntradayBeforeOneHourForecast_campaignIdCampaignMetric[$campaignId] ?? new \stdClass();
                        $campaignMetricIntraday_beforeTwoHourForecast = $assocIntradayBeforeTwoHourForecast_campaignIdCampaignMetric[$campaignId] ?? new \stdClass();
                        */

                        $campaignIntraday_beforeOneHour = $assocIntradayBeforeOneHourForecast_campaignIdCampaignMetric[$campaignId][$currencyId] ?? new \stdClass(); # T - 1
                        $campaignIntraday_beforeTwoHour = $assocIntradayBeforeTwoHourForecast_campaignIdCampaignMetric[$campaignId][$currencyId] ?? new \stdClass(); # T - 2
                    } else {
                        $campaignIntraday_beforeOneHour = $assocIntraday_hourCampaignIdMetricForecast[$hourForecast - 1][$campaignId][$currencyId] ?? new \stdClass();
                        $campaignIntraday_beforeTwoHour = $assocIntraday_hourCampaignIdMetricForecast[$hourForecast - 2][$campaignId][$currencyId] ?? new \stdClass();
                    }

                    $assocCampaignIntraday_metricValue = $this->_calculateDataForecast(
                        $campaignIntraday_beforeOneHour, $campaignIntraday_beforeTwoHour, $assocMetricValue
                    );

                    if ($hourForecast == 0) {

                    } else {
                        $hourForecast_subtractOne = $hourForecast - 1;
                    }

                    $contributionPaidAdsHour = $contributionPaidAds[$hourForecast_subtractOne] ?? 0;


                    $impression = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_IMPRESSION];
                    $click = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_CLICK];
                    $itemSold = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_ITEM_SOLD];
                    $gmv = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_GMV];
                    $cost = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_COST];
                    $totalSku = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_SKU];
                    $totalKeyword = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_KEYWORD];
                    $gmvChannel = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_GMV];
                    $itemSoldChannel = $assocCampaignIntraday_metricValue[Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_ITEM_SOLD];
                    $ctr = $impression > 0 ? $click / $impression * 100 : 0;
                    $cr = $click > 0 ? $itemSold / $click * 100 : 0;
                    $cpc = $click > 0 ? $cost / $click : 0;
                    $cpi = $itemSold > 0 ? $cost / $itemSold : 0;
                    $cir = $gmv > 0 ? $cost / $gmv * 100 : 0;
                    $percentGmvGmvChannel = $gmvChannel > 0 ? $gmv / $gmvChannel * 100 : 0;
                    $percentItemSoldItemSoldChannel = $itemSoldChannel > 0 ? $itemSold / $itemSoldChannel * 100 : 0;

                    $assocIntraday_hourCampaignIdMetricForecast[$hourForecast][$campaignId][$currencyId] = Functions\Arrays::transformToStdClass([
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_IMPRESSION => $impression,
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_CLICK => $click,
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_ITEM_SOLD => $itemSold,
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_GMV => $gmv,
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_COST => $cost,
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_SKU => $totalSku,
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_KEYWORD => $totalKeyword,
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_GMV => $gmvChannel,
                        Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_ITEM_SOLD => $itemSoldChannel,
                    ]);

                    $assocIntraday_hourCampaignIdMetricForecast[$hourForecast - 1][$campaignId][$currencyId] = $campaignIntraday_beforeOneHour;

                    $campaignIntraday_forecast = $assocIntradayForecast_campaignIdCampaignMetric[$campaignId][$currencyId] ?? new \stdClass();
                    $campaignMetricIntradayId = $campaignIntraday_forecast->{Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_ID} ?? null;

                    if ($campaignMetricIntradayId) {
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_IMPRESSION][$campaignMetricIntradayId] = $impression;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CLICK][$campaignMetricIntradayId] = $click;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_ITEM_SOLD][$campaignMetricIntradayId] = $itemSold;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_GMV][$campaignMetricIntradayId] = $gmv;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_COST][$campaignMetricIntradayId] = $cost;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CTR][$campaignMetricIntradayId] = $ctr;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CR][$campaignMetricIntradayId] = $cr;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CPC][$campaignMetricIntradayId] = $cpc;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CPI][$campaignMetricIntradayId] = $cpi;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CIR][$campaignMetricIntradayId] = $cir;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_TOTAL_SKU][$campaignMetricIntradayId] = $totalSku;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_TOTAL_KEYWORD][$campaignMetricIntradayId] = $totalKeyword;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_TOTAL_GMV][$campaignMetricIntradayId] = $gmvChannel;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_TOTAL_ITEM_SOLD][$campaignMetricIntradayId] = $itemSoldChannel;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_PERCENT_GMV_TOTAL][$campaignMetricIntradayId] = $percentGmvGmvChannel;
                        $campaignMetricIntradayUpdateCase[Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_PERCENT_ITEM_SOLD_TOTAL][$campaignMetricIntradayId] = $percentItemSoldItemSoldChannel;
                    } else {
                        $campaignMetricIntradayInsertArray[] = Models\ModelChart\ChartKeywordCampaignMetricIntraday::buildDataInsert(
                            $shopChannelId, $campaignId, $currentDate, $hourForecast, $impression, $click, $itemSold, $gmv, $cost, $ctr, $cr, $cpc, $cpi, $cir,
                            $totalSku, $totalKeyword, $gmvChannel, $itemSoldChannel, $percentGmvGmvChannel, $percentItemSoldItemSoldChannel, $currencyId, $isForeCast
                        );
                    }
                }
            }
        }

        # begin insert campaign metric intraday
        $campaignMetricIntradayInsertChunk = array_chunk($campaignMetricIntradayInsertArray, 200);
        foreach ($campaignMetricIntradayInsertChunk as $chunk) {
            Models\ModelChart\ChartKeywordCampaignMetricIntraday::batchInsert($chunk);
        }
        # end insert campaign metric intraday

        $this->_updateBatch($campaignMetricIntradayUpdateCase);
    }

    #region
    /**
     * _get Campaign Intraday
     * @param int $shopChannelId
     * @param string $currentDate
     * @return array
     */
    private function _getCampaignIntraday($shopChannelId, $currentDate)
    {
        $assocIntraday_hourCampaignIdMetric = [];
        $campaignMetricIntradayData = Models\ModelChart\ChartKeywordCampaignMetricIntraday::searchByShopChannelIdAndDateHour(
            $shopChannelId, $currentDate
        );
        foreach ($campaignMetricIntradayData as $campaignMetricIntraday) {
            $hour = $campaignMetricIntraday->{Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_HOUR};
            #if ($hour < $currentHour - 3) continue;

            $campaignId = $campaignMetricIntraday->{Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID};
            $currencyId = $campaignMetricIntraday->{Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_CURRENCY_ID};

            $assocIntraday_hourCampaignIdMetric[$hour][$campaignId][$currencyId] = $campaignMetricIntraday;
        }

        return $assocIntraday_hourCampaignIdMetric;
    }

    /**
     * get Contribution
     * @param int $shopChannelId
     * @param string $currentDate
     * @return array
     */
    private function _getContribution($shopChannelId, $currentDate)
    {
        $campaignContributionData = Models\ModelChart\ChartKeywordCampaignContribution::searchByShopChannelIdAndDate($shopChannelId, $currentDate, $this->getDateBeginForecast());

        $assocContribution_hourCampaignIdMetric = [];
        foreach ($campaignContributionData as $campaignContribution) {
            $hour = $campaignContribution->{Models\ModelChart\ChartKeywordCampaignContribution::COL_HOUR};
            #if ($hour < $currentHour - 2) continue;
            $campaignId = $campaignContribution->{Models\ModelChart\ChartKeywordCampaignContribution::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID};
            $assocContribution_hourCampaignIdMetric[$hour][$campaignId] = $campaignContribution;
        }

        return $assocContribution_hourCampaignIdMetric;
    }

    /**
     * _calculate Data Forecast
     * @param $campaignIntraday_beforeOneHour
     * @param $campaignIntraday_beforeTwoHour
     * @param $assocMetricValue
     * @return array
     */
    private function _calculateDataForecast($campaignIntraday_beforeOneHour, $campaignIntraday_beforeTwoHour, $assocMetricValue)
    {
        $assocCampaignIntraday_metricValue = [];

        foreach ($assocMetricValue as $metric => $value) {
            $metric_beforeOneHour = $campaignIntraday_beforeOneHour->{$metric};
            $metric_beforeTwoHour = $campaignIntraday_beforeTwoHour->{$metric} ?? 0;

            #$assocCampaignIntraday_metricValue[$metric] = $metric_beforeOneHour + $value * ($metric_beforeOneHour - $metric_beforeTwoHour);
            $assocCampaignIntraday_metricValue[$metric] = $value * ($metric_beforeOneHour);
        }

        return $assocCampaignIntraday_metricValue;
    }

    /**
     * _calculate Contribution Forecast
     * @param $campaignContributionForecast
     * @param $campaignContributionBeforeOneHour
     * @return array
     */
    private function _calculateContributionForecast($campaignContributionForecast, $campaignContributionBeforeOneHour)
    {
        $columnNoCalculateArray = [
            Models\ModelChart\ChartKeywordCampaignContribution::COL_ID,
            Models\ModelChart\ChartKeywordCampaignContribution::COL_SHOP_CHANNEL_ID,
            Models\ModelChart\ChartKeywordCampaignContribution::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
            Models\ModelChart\ChartKeywordCampaignContribution::COL_DATE,
            Models\ModelChart\ChartKeywordCampaignContribution::COL_TOTAL_DATE,
            Models\ModelChart\ChartKeywordCampaignContribution::COL_HOUR,
            Models\ModelChart\ChartKeywordCampaignContribution::COL_UPDATED_AT
        ];

        $assocMetricValue = [];
        foreach ($campaignContributionBeforeOneHour as $column => $value) {
            if (in_array($column, $columnNoCalculateArray)) continue;

            $assocMetricValue[$column] = $value > 0 ? $campaignContributionForecast->{$column} / $value : 0;
        }

        return $assocMetricValue;
    }

    /**
     * update Batch
     * @param $data
     */
    private function _updateBatch($data)
    {
        # begin update campaign metric intraday
        echo '# begin update campaign metric intraday ' . microtime(), PHP_EOL;
        $campaignMetricIntradayIdUpdate = [];
        foreach ($data as $columnUpdated => $assocIdValue) {
            if ( ! $campaignMetricIntradayIdUpdate) $campaignMetricIntradayIdUpdate = array_keys($assocIdValue);

            $assocIdValueChunk = array_chunk($assocIdValue, 200, true);
            foreach ($assocIdValueChunk as $chunk) {
                $updateCase = new Models\ModelChart\ChartKeywordCampaignMetricIntraday();
                $updateCase->setTableUpdated(Models\ModelChart\ChartKeywordCampaignMetricIntraday::TABLE_NAME);
                $updateCase->setColumnCondition(Models\ModelChart\ChartKeywordCampaignMetricIntraday::COL_ID);
                $updateCase->setColumnUpdated($columnUpdated);
                foreach ($chunk as $id => $value) {
                    $updateCase->addCase($id, $value);
                }

                $sql = $updateCase->assemble();
                Models\ModelChart\ChartKeywordCampaignMetricIntraday::updateCase($sql);
            }
        }
        if ($campaignMetricIntradayIdUpdate) {
            Models\ModelChart\ChartKeywordCampaignMetricIntraday::snapshotUpdateTime($campaignMetricIntradayIdUpdate);
        }
        echo '# end update campaign metric intraday ' . microtime(), PHP_EOL;
        # end update campaign metric intraday
    }

    /**
     * get Total Sku And Keyword
     * @param int $shopChannelId
     * @param int $campaignId
     * @param string $dateFrom
     * @param string $dateTo
     * @param bool $isRunAgain
     * @return array
     */
    private function _getTotalSkuAndKeyword($shopChannelId, $campaignId, $dateFrom, $dateTo, $isRunAgain)
    {
        if ( ! $isRunAgain) {
            $dateFrom = Library\Common::convertDatetimeToTimestamp($dateFrom);
            $dateTo = Library\Common::convertDatetimeToTimestamp($dateTo);

            $productIdArrayInLog = array_values(array_unique(array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticLogSku::searchCampaignIdByDateFromTo(
                    $campaignId, $dateFrom, $dateTo, $this->actionSku
                )),
                Models\MakProgrammaticLogSku::COL_FK_PRODUCT_SHOP_CHANNEL
            )));

            $productIdArray = array_column(
                Library\Formater::stdClassToArray(Models\ProductAds::searchByCampaignIdAndState($campaignId, Models\ProductAds::STATE_ONGOING)),
                Models\ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL
            );

            $totalSku = count(array_diff($productIdArray, $productIdArrayInLog)) + count($productIdArrayInLog);

            $keywordProductIdArrayInLog = array_values(array_unique(array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticLogKeyword::searchCampaignIdByDateFromTo(
                    $campaignId, $dateFrom, $dateTo, $this->actionKeyword
                )),
                Models\MakProgrammaticLogKeyword::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )));

            $keywordProductIdArray = array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByCampaignIdAndState(
                    $campaignId, Models\MakProgrammaticKeywordProduct::STATUS_RESTORE
                )),
                Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            );

            $totalKeyword = count(array_diff($keywordProductIdArray, $keywordProductIdArrayInLog)) + count($keywordProductIdArrayInLog);
        } else {
            $keywordProductData = Library\Formater::stdClassToArray(
                Models\MakPerformanceHour::searchByViewGreater0(
                    $shopChannelId, Library\Common::getDateFromDatetime($dateFrom), intval(Library\Common::getHourFromDatetime($dateFrom))
                )
            );
            $assocCampaignIdProduct = array_count_values(array_column(
                $keywordProductData,
                Models\MakPerformanceHour::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                Models\MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            ));
            $assocCampaignIdKeywordProduct = array_count_values(array_column(
                $keywordProductData,
                Models\MakPerformanceHour::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            ));

            $totalSku = $assocCampaignIdProduct[$campaignId] ?? 0;
            $totalKeyword = $assocCampaignIdKeywordProduct[$campaignId] ?? 0;
        }

        return [$totalSku, $totalKeyword];
    }

    /**
     * build Datetime
     * @param $date
     * @param $currentDate
     * @param $currentHour
     * @return array
     */
    private function _buildDatetime($date, $currentDate, $currentHour)
    {
        $datetimeArray = [];
        if ( ! $this->isRunAgain() && !$this->getForceIntraday()) {
            if ($date == $currentDate) {
                $this->isImportCampaignFuture = true;

                if ($currentHour == 0) {
                    return $datetimeArray;
                } else {
                    $hour = $currentHour - 1;
                }
            } else {
                $hour = self::HOUR_MAX;
            }

            $datetimeMin = $date.' '.$hour.self::MINUTE_MIN;
            $datetimeMax = $date.' '.$hour.self::MINUTE_MAX;

            array_push($datetimeArray, $datetimeMin, $datetimeMax);
        } else {
            if ($date < $currentDate) {
                $hourMax = self::HOUR_MAX;
            } else {
                $hourMax = $currentHour;
            }

            if ($this->getForceIntraday()) {
                $hourMax = self::HOUR_MAX;
                $this->isImportCampaignFuture = true;
            }

            foreach (range(0, $hourMax) as $hour) {
                $hour = str_pad($hour, 2, 0, STR_PAD_LEFT);
                $datetimeMin = $date.' '.$hour.self::MINUTE_MIN;
                $datetimeMax = $date.' '.$hour.self::MINUTE_MAX;

                array_push($datetimeArray, $datetimeMin, $datetimeMax);
            }
        }

        return $datetimeArray;
    }
    #endregion

}