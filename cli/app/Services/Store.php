<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 16/10/2019
 * Time: 16:16
 */

namespace App\Services;

use App\Library;
use App\Models;


class Store
{
    /**
     * generate Code By Name
     * @param string $name
     * @param int $organization
     * @param int $venture
     * @return string
     * @throws \Exception
     */
    public static function generateCodeByName($name, $organization, $venture)
    {
        $explode = explode(' ', $name);
        $explode = array_filter($explode);
        $allStore = Library\Formater::stdClassToArray(Models\ShopMaster::getAll());
        $storeByOrganizationVenture = [];
        foreach ($allStore as $store) {
            if ( ! ($store[Models\ShopMaster::COL_FK_ORGANIZATION] == $organization && $store[Models\ShopMaster::COL_FK_VENTURE] == $venture)) continue;

            $storeByOrganizationVenture[] = $store;
        }

        $arrayStoreCodeInDb = array_column($storeByOrganizationVenture, Models\ShopMaster::COL_SHOP_MASTER_CODE);

        if (count($explode) >= 3) {
            $code = strtoupper(sprintf('%s%s%s', $explode[0][0], $explode[1][0], $explode[2][0]));
            if (!in_array($code, $arrayStoreCodeInDb)) {
                return $code;
            }
        }

        $newName = implode($explode);
        if (strlen($newName) > 3) {
            $code = strtoupper(sprintf('%s%s%s', $newName[0], $newName[1], $newName[2]));
            if (!in_array($code, $arrayStoreCodeInDb)) {
                return $code;
            }
        }

        for ($i = 0; $i < 10; $i++) {
            $random = random_int(1, 46655);
            Library\Common::convert10SystemTo36System($random, $newCode);
            if (!in_array($newCode, $storeByOrganizationVenture)) {
                return $newCode;
            }
        }

        throw new \Exception('');
    }
}