<?php


namespace App\Services;

use App\Library\Formater;
use App\Library;
use App\Models;
use App\Models\CrawlerLog;
use Illuminate\Database\Eloquent\Model;

class Recommendation
{
    private $configDayCount = 30;

    private $debugFlag = false;
    private $scriptBeginTime = 0;
    private $scriptRunningTime = 0;

    private $keyword;
    private $shopId;
    private $itemId;
    private $startTime;

    public function debug($bool = true)
    {
        $this->debugFlag = $bool;
        return $this;
    }

    public function __construct($keyword, $shopId, $itemId, $from = null)
    {
        $this->keyword = $keyword;
        $this->shopId = $shopId;
        $this->itemId = $itemId;
        $this->startTime = $from ? $from : strtotime('-' . $this->configDayCount . ' day');
    }

    public function run()
    {
        $this->scriptBeginTime = $this->millisecond();
        $result = [];
        $masterData = Models\MakCrawRanking::searchSkuRankRecommendationData($this->startTime, $this->keyword, $this->itemId, $this->shopId,Library\Model\Sql\Manager::SQL_MASTER_CONNECT);

        if (!empty($masterData)) {
            $this->printTime();
            $shopChannelId = current($masterData)[Models\ShopChannel::COL_SHOP_CHANNEL_ID];
            $maxTime = intval(max(array_column($masterData, 'timestamp')));
            $rtbDataRaw = CrawlerLog\Dataline::searchData($this->startTime, $shopChannelId, $this->keyword, $this->itemId);
            $this->printTime();

            $rtbData['list'] = [];
            $rtbData['time'] = [];
            foreach ($rtbDataRaw as $item) {
                $rtbData['list'][$item->timestamp] = $item->price;
                $rtbData['time'][] = $item->timestamp;
            }
            $topData = CrawlerLog\ProductRankingShopee::topDataQuery($this->keyword, $this->startTime, $maxTime);
            $this->printTime();
            $topSkuList = [];
            foreach ($topData as $key => $item) {
                $topSkuList[] = $item->_id->sku;
                $topData[$key] = Formater::stdClassToArray($item);
            }
            $skuDataList = CrawlerLog\ProductRankingShopee::topSkuPosition($this->keyword, $topSkuList, $this->startTime, $maxTime);
            $this->printTime();
            $assocData = [];
            foreach ($skuDataList as $item) {
                $itemSku = $item->itemid;
                $assocData[$itemSku][] = $item;
            }
            foreach ($topData as $key => $item) {
                $itemSku = $item['_id']['sku'];
                $topData[$key]['pos_list'] = [];
                $topData[$key]['pos_time'] = [];
                $this->printDebug(sprintf('initialize: %s COUNT: %s' . PHP_EOL, ($key + 1), count($assocData[$itemSku] ?? [])));
                foreach ($assocData[$itemSku] ?? [] as $assocItem) {
                    $topData[$key]['pos_list'][$assocItem->timeRaw] = $assocItem->isSponsorPosition;
                    $topData[$key]['pos_time'][] = $assocItem->timeRaw;
                }
                sort($topData[$key]['pos_time']);
                $this->printTime();
            }
            foreach ($masterData as $data) {
                $data['position'] = intval($data['position']);
                $data['position'] = is_nan($data['position']) || $data['position'] == 0 ? null : $data['position'];
                $filter = [];
                if ($data['position'] != null) {
                    $filter[$data['position']] = [[
                        'index' => null,
                        'timestamp' => $data['timestamp']
                    ]];
                }
                $rtbTimeIndex = $this->binarySearchSmaller($rtbData['time'], $data['timestamp'], 0, count($rtbData['time']) - 1);
                if ($rtbTimeIndex < 0) {
                    $data['price'] = null;
                } else {
                    $data['price'] = $rtbData['list'][$rtbData['time'][$rtbTimeIndex]] ?? null;
                }
                for ($i = 1; $i <= 10; $i++) {
                    if (!($topData[$i - 1] ?? null)) continue;
                    $timeIndex = $this->binarySearchSmaller($topData[$i - 1]['pos_time'], $data['timestamp'], 0, count($topData[$i - 1]['pos_time']) - 1);
                    if ($timeIndex < 0) {
                        $data['sku_no_' . $i] = null;
                    } else {
                        $data['sku_no_' . $i] = $topData[$i - 1]['pos_list'][$topData[$i - 1]['pos_time'][$timeIndex]] ?? null;
                    }
                    if ($data['sku_no_' . $i] != null) {
                        $filter[$data['sku_no_' . $i]][] = [
                            'index' => $i,
                            'timestamp' => $topData[$i - 1]['pos_time'][$timeIndex]
                        ];

                    }
                }
                // /*/
                foreach ($filter as $keyItem => $valueKey) {
                    if (count($filter[$keyItem]) > 1) {
                        $maxI = 0;
                        foreach ($filter[$keyItem] as $key => $value) {
                            if ($filter[$keyItem][$maxI]['timestamp'] < $filter[$keyItem][$key]['timestamp']) {
                                $maxI = $key;
                            }
                        }
                        foreach ($filter[$keyItem] as $key => $value) {
                            if ($key != $maxI) {
                                if ($filter[$keyItem][$key]['index']) {
                                    $data['sku_no_' . $filter[$keyItem][$key]['index']] = null;
                                }
                            }
                        }
                    }
                }//*/
                $str = json_encode([
                    $data['keyword'],
                    $data['sku'],
                    date('Y-m-d H:i:s', $data['timestamp']),
                    $data['position'],
                    $data['price'],
                    $data['sku_no_1'] ?? null,
                    $data['sku_no_2'] ?? null,
                    $data['sku_no_3'] ?? null,
                    $data['sku_no_4'] ?? null,
                    $data['sku_no_5'] ?? null,
                    $data['sku_no_6'] ?? null,
                    $data['sku_no_7'] ?? null,
                    $data['sku_no_8'] ?? null,
                    $data['sku_no_9'] ?? null,
                    $data['sku_no_10'] ?? null]);
                $newStr = trim($str, '[]');
                $result[] = json_decode($str);
                #$this->printDebug($newStr);
            }
            $this->printDebug($result);
        }

        return $result;
    }

    private function binarySearchSmaller($array, $value, $i, $j)
    {
        if (count($array) == 0) return -1;
        $x = floor(($i + $j) / 2);
        $y = $x + 1;
        if ($array[$x] <= $value && $array[$y] >= $value) {
            return $x;
        }
        if ($x == $i && $y == $j) {
            if ($array[$y] <= $value) {
                return $y;
            }
            return -1;
        }
        if ($array[$x] >= $value) {
            return $this->binarySearchSmaller($array, $value, $i, $x);
        } else {
            return $this->binarySearchSmaller($array, $value, $x, $j);
        }
    }

    private function printDebug($item)
    {
        if ($this->debugFlag) {
            print_r($item);
            print_r(PHP_EOL);
        }
    }

    private function printTime()
    {
        $now = $this->millisecond();
        $this->scriptRunningTime = $this->scriptRunningTime == 0 ? $this->scriptBeginTime : $this->scriptRunningTime;
        $this->printDebug(sprintf('RUNNING: %sms TOTAL: %sms' . PHP_EOL, ($now - $this->scriptRunningTime), ($now - $this->scriptBeginTime)));
        $this->scriptRunningTime = $now;
    }

    private function millisecond()
    {
        return intval(microtime(true) * 1000);
    }
}