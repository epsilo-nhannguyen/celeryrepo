<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 10:32
 */

namespace App\Services;


use App\Library;
use App\Models;


class ProgrammaticShopeeKeywordBidding
{

    /**
     * @var int
     */
    const CURRENT_RANK_DEFAULT = 1000;

    /**
     * @var int
     */
    const AMOUNT_CURRENT_RANK = 20;

    # sku
    /**
     * turn Status Sku (Stop, Resume)
     * @param int $shopChannelId
     */
    public static function turnStatusSku($shopChannelId)
    {
        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getById($shopChannelId));

        Library\Common::setTimezoneByChannelId($shopChannelInfo[Models\ShopChannel::COL_FK_CHANNEL]);

        $objectiveId = Models\MakProgrammaticObjective::SKU;

        $itemSold = [
            Models\MakProgrammaticMetric::SKU_ITEM_SOLD_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_ITEM_SOLD_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_ITEM_SOLD_IN_LAST_14_DAYS
        ];

        $gmv = [
            Models\MakProgrammaticMetric::SKU_GMV_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_GMV_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_GMV_IN_LAST_14_DAYS
        ];

        $cost = [
            Models\MakProgrammaticMetric::SKU_COST_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_COST_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_COST_IN_LAST_14_DAYS
        ];

        $stockCoverage = [
            Models\MakProgrammaticMetric::SKU_STOCK_COVERAGE_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_STOCK_COVERAGE_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_STOCK_COVERAGE_IN_LAST_14_DAYS
        ];

        $impression = [
            Models\MakProgrammaticMetric::SKU_IMPRESSION_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_IMPRESSION_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_IMPRESSION_IN_LAST_14_DAYS
        ];

        $click = [
            Models\MakProgrammaticMetric::SKU_CLICK_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_CLICK_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_CLICK_IN_LAST_14_DAYS
        ];

        $cpi = [
            Models\MakProgrammaticMetric::SKU_CPI_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_CPI_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_CPI_IN_LAST_14_DAYS
        ];

        $ctr = [
            Models\MakProgrammaticMetric::SKU_CTR_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_CTR_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_CTR_IN_LAST_14_DAYS
        ];

        $cr = [
            Models\MakProgrammaticMetric::SKU_CR_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::SKU_CR_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::SKU_CR_IN_LAST_14_DAYS
        ];

        $discount = [
            Models\MakProgrammaticMetric::SKU_PERCENTAGE_DISCOUNT
        ];

        $sellableStock = [
            Models\MakProgrammaticMetric::SKU_SELLABLE_STOCK
        ];

        $activatedDuration = [
            Models\MakProgrammaticMetric::SKU_ACTIVATED_DURATION
        ];

        $activatedKeyword = [
            Models\MakProgrammaticMetric::SKU_ACTIVATED_KEYWORD
        ];

        $assocMetricIdData = [];
        $metricData = Library\Formater::stdClassToArray(Models\MakProgrammaticMetric::searchByObjectiveId($objectiveId));
        foreach ($metricData as $metricInfo) {
            $metricId = $metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_ID];
            if (in_array($metricId, $itemSold)) {
                $metricInfo['option'] = 'itemSold';
            } elseif (in_array($metricId, $gmv)) {
                $metricInfo['option'] = 'gmv';
            } elseif (in_array($metricId, $cost)) {
                $metricInfo['option'] = 'cost';
            } elseif (in_array($metricId, $stockCoverage)) {
                $metricInfo['option'] = 'stockCoverage';
            } elseif (in_array($metricId, $impression)) {
                $metricInfo['option'] = 'impression';
            } elseif (in_array($metricId, $click)) {
                $metricInfo['option'] = 'click';
            } elseif (in_array($metricId, $cpi)) {
                $metricInfo['option'] = 'cpi';
            } elseif (in_array($metricId, $ctr)) {
                $metricInfo['option'] = 'ctr';
            } elseif (in_array($metricId, $cr)) {
                $metricInfo['option'] = 'cr';
            } elseif (in_array($metricId, $discount)) {
                $metricInfo['option'] = 'discount';
            } elseif (in_array($metricId, $sellableStock)) {
                $metricInfo['option'] = 'sellableStock';
            } elseif (in_array($metricId, $activatedDuration)) {
                $metricInfo['option'] = 'activatedDuration';
            } elseif (in_array($metricId, $activatedKeyword)) {
                $metricInfo['option'] = 'activatedKeyword';
            } else {
                $metricInfo['option'] = '';
            }

            unset($metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_ID]);
            unset($metricInfo[Models\MakProgrammaticMetric::COL_FK_MAK_PROGRAMMATIC_OBJECTIVE]);
            unset($metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_UNIT]);

            $assocMetricIdData[$metricId] = $metricInfo;
        }

        $assocProductShopChannelIdCampaignId = array_column(
            Library\Formater::stdClassToArray(Models\ProductShopChannelCampaign::searchByShopChannelId($shopChannelId)),
            Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
            Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID
        );

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $assocChannelIdStatus = [];

        $dateTo = date('Y-m-d');
        $actionIdArray = [
            Models\MakProgrammaticAction::PAUSE_SKU,
            Models\MakProgrammaticAction::RESUME_SKU,
        ];
        $settingSkuData = Library\Formater::stdClassToArray(Models\MakProgrammaticSettingSku::searchByShopChannelId($shopChannelId, $actionIdArray));
        foreach ($settingSkuData as $settingSkuInfo) {
            $ruleId = $settingSkuInfo[Models\MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID];
            $actionId = $settingSkuInfo[Models\MakProgrammaticRule::COL_FK_MAK_PROGRAMMATIC_ACTION];

            $conditionData = Library\Formater::stdClassToArray(Models\MakProgrammaticCondition::searchByRuleId($ruleId));
            $ruleSkuData = Library\Formater::stdClassToArray(Models\MakProgrammaticSettingSku::searchByRuleId($ruleId));
            foreach ($ruleSkuData as $ruleSkuInfo) {

                $logSkuArray = [];
                $isValid = true;

                $productShopChannelId = $ruleSkuInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID];
                $allocationStock = intval($ruleSkuInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK]);
                $rrpChannel = floatval($ruleSkuInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_PRICE_RRP_CHANNEL]);
                $postsubChannel = floatval($ruleSkuInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_PRICE_POSTSUB_CHANNEL]);

                $productAdsId = $ruleSkuInfo[Models\ProductAds::COL_PRODUCT_ADS_ID];
                $productAdsState = $ruleSkuInfo[Models\ProductAds::COL_PRODUCT_ADS_STATE];
                $channelAdsId = $ruleSkuInfo[Models\ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID];
                $productCreatedAt = intval($ruleSkuInfo[Models\ProductAds::COL_PRODUCT_ADS_CREATED_AT]);

                foreach ($conditionData as $conditionInfo) {
                    $metricId = $conditionInfo[Models\MakProgrammaticCondition::COL_FK_MAK_PROGRAMMATIC_METRIC];
                    $operatorId = $conditionInfo[Models\MakProgrammaticCondition::COL_FK_MAK_PROGRAMMATIC_OPERATOR];
                    $ruleValue = $conditionInfo[Models\MakProgrammaticCondition::COL_MAK_PROGRAMMATIC_CONDITION_VALUE];

                    $metricOption = $assocMetricIdData[$metricId]['option'];
                    $metricValue = $assocMetricIdData[$metricId][Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_VALUE];
                    $metricName = $assocMetricIdData[$metricId][Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_NAME];

                    #region
                    $data = null;
                    if ($metricOption == 'itemSold') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getItemSoldProduct($productShopChannelId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'gmv') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getGmvProduct($productShopChannelId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'cost') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getCostProduct($productShopChannelId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'stockCoverage') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $itemSold = MarketingPerformance::getItemSoldProduct($productShopChannelId, $dateFrom, $dateTo);
                        if ($metricValue > 0 && $itemSold > 0) {
                            $data = $allocationStock/($itemSold/$metricValue);
                        }
                    } elseif ($metricOption == 'impression') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getViewProduct($productShopChannelId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'click') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getClickProduct($productShopChannelId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'cpi') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $cost = MarketingPerformance::getCostProduct($productShopChannelId, $dateFrom, $dateTo);
                        $itemSold = MarketingPerformance::getItemSoldProduct($productShopChannelId, $dateFrom, $dateTo);
                        if ($itemSold > 0) {
                            $data = $cost/$itemSold;
                        }
                    } elseif ($metricOption == 'ctr') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $click = MarketingPerformance::getClickProduct($productShopChannelId, $dateFrom, $dateTo);
                        $impression = MarketingPerformance::getViewProduct($productShopChannelId, $dateFrom, $dateTo);
                        if ($impression > 0) {
                            $data = ($click/$impression) * 100;
                        }
                    } elseif ($metricOption == 'cr') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $itemSold = MarketingPerformance::getItemSoldProduct($productShopChannelId, $dateFrom, $dateTo);
                        $click = MarketingPerformance::getClickProduct($productShopChannelId, $dateFrom, $dateTo);
                        if ($click > 0) {
                            $data = ($itemSold / $click) * 100;
                        }
                    } elseif ($metricOption == 'activatedDuration') {
                        $ruleValue = intval($ruleValue); # date number

                        $data = $productCreatedAt + (60 * 60 * 24 * $ruleValue);

                        $ruleValue = strtotime('now'); # now timestamp
                    } elseif ($metricOption == 'activatedKeyword') {
                        $ruleValue = intval($ruleValue);

                        $data = count(Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByProductShopChannelId(
                            $productShopChannelId, Models\MakProgrammaticKeywordProduct::STATUS_RESTORE
                        )));
                    } elseif ($metricOption == 'sellableStock') {
                        $ruleValue = intval($ruleValue);

                        $data = $allocationStock;
                    } elseif ($metricOption == 'discount') {
                        $ruleValue = floatval($ruleValue);

                        $data = 0;
                        if ($rrpChannel) {
                            $data = (1 - $postsubChannel / $rrpChannel) * 100;
                        }
                    }
                    #endregion

                    $isValidData = false;
                    #region
                    if ($data !== null) {
                        if ($operatorId == Models\MakProgrammaticOperator::EQUALS) {
                            $isValidData = $data == $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::GREATER_THAN) {
                            $isValidData = $data > $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::LESS_THAN) {
                            $isValidData = $data < $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::GREATER_THAN_OR_EQUAL_TO) {
                            $isValidData = $data >= $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::LESS_THAN_OR_EQUAL_TO) {
                            $isValidData = $data <= $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::NOR_EQUAL_TO) {
                            $isValidData = $data != $ruleValue;
                        }
                    }
                    #endregion

                    if ($isValidData) {
                        if ($metricOption == 'activatedDuration') {
                            $checkpoint = 'Activated at '. Library\Common::convertTimestampToDatetime($productCreatedAt);
                        } else {
                            $checkpoint = $metricName.' = '.$data;
                        }

                        $campaignId = $assocProductShopChannelIdCampaignId[$productShopChannelId] ?? null;
                        $logSkuArray[] = Models\MakProgrammaticLogSku::buildDataInsert(
                            $productShopChannelId, $actionId, $ruleId, $checkpoint, $campaignId, Models\Admin::EPSILO_SYSTEM
                        );
                    }

                    $isValid = $isValid && $isValidData;
                }

                if ($isValid && count($conditionData) > 0) {
                    if ($actionId == Models\MakProgrammaticAction::PAUSE_SKU) {
                        if ( ! $isLogin) $isLogin = $crawler->login();
                        if ( ! $isLogin) $isLogin = $crawler->login();
                        if ( ! $isLogin) $isLogin = $crawler->login();

                        if ( ! $assocChannelIdStatus) {
                            $assocChannelIdStatus = $crawler->searchChannelIdAndStatus();
                        }

                        if ( ! isset($assocChannelIdStatus[$channelAdsId]) ||
                            $assocChannelIdStatus[$channelAdsId] != Library\Crawler\Channel\Shopee::SKU_RESUME) continue;

                        $isSuccess = $crawler->turnSkuWithStatus($channelAdsId, Library\Crawler\Channel\Shopee::SKU_PAUSE);
                        if ($isSuccess) {
                            Models\ProductAds::updateStateByIdArray(
                                [$productAdsId], Models\ProductAds::STATE_PAUSED, Models\Admin::EPSILO_SYSTEM
                            );

                            Models\MakProgrammaticLogSku::batchInsert($logSkuArray);

                        }
                    } elseif ($actionId == Models\MakProgrammaticAction::RESUME_SKU) {
                        if ( ! $isLogin) $isLogin = $crawler->login();
                        if ( ! $isLogin) $isLogin = $crawler->login();
                        if ( ! $isLogin) $isLogin = $crawler->login();

                        if ( ! $assocChannelIdStatus) {
                            $assocChannelIdStatus = $crawler->searchChannelIdAndStatus();
                        }
                        if ( ! isset($assocChannelIdStatus[$channelAdsId]) ||
                            $assocChannelIdStatus[$channelAdsId] != Library\Crawler\Channel\Shopee::SKU_PAUSE) continue;

                        $isSuccess = $crawler->turnSkuWithStatus($channelAdsId, Library\Crawler\Channel\Shopee::SKU_RESUME);
                        if ($isSuccess) {
                            Models\ProductAds::updateStateByIdArray(
                                [$productAdsId], Models\ProductAds::STATE_ONGOING, Models\Admin::EPSILO_SYSTEM
                            );

                            Models\MakProgrammaticLogSku::batchInsert($logSkuArray);
                        }
                    }
                }
            }
        }
    }

    # sku - keyword
    /**
     * turn Status Sku Keyword (Stop, Resume)
     * @param int $shopChannelId
     */
    public static function turnStatusSkuKeyword($shopChannelId)
    {
        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getById($shopChannelId));

        Library\Common::setTimezoneByChannelId($shopChannelInfo[Models\ShopChannel::COL_FK_CHANNEL]);

        $objectiveId = Models\MakProgrammaticObjective::KEYWORD_SKU;

        $itemSold = [
            Models\MakProgrammaticMetric::KEYWORD_ITEM_SOLD_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_ITEM_SOLD_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_ITEM_SOLD_IN_LAST_14_DAYS
        ];

        $gmv = [
            Models\MakProgrammaticMetric::KEYWORD_GMV_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_GMV_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_GMV_IN_LAST_14_DAYS
        ];

        $cost = [
            Models\MakProgrammaticMetric::KEYWORD_COST_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_COST_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_COST_IN_LAST_14_DAYS
        ];

        $stockCoverage = [
            Models\MakProgrammaticMetric::KEYWORD_STOCK_COVERAGE_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_STOCK_COVERAGE_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_STOCK_COVERAGE_IN_LAST_14_DAYS
        ];

        $impression = [
            Models\MakProgrammaticMetric::KEYWORD_IMPRESSION_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_IMPRESSION_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_IMPRESSION_IN_LAST_14_DAYS
        ];

        $click = [
            Models\MakProgrammaticMetric::KEYWORD_CLICK_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CLICK_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CLICK_IN_LAST_14_DAYS
        ];

        $cpi = [
            Models\MakProgrammaticMetric::KEYWORD_CPI_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CPI_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CPI_IN_LAST_14_DAYS
        ];

        $ctr = [
            Models\MakProgrammaticMetric::KEYWORD_CTR_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CTR_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CTR_IN_LAST_14_DAYS
        ];

        $cr = [
            Models\MakProgrammaticMetric::KEYWORD_CR_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CR_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CR_IN_LAST_14_DAYS
        ];

        $keywordRank = [
            Models\MakProgrammaticMetric::KEYWORD_SKU_KEYWORD_RANK
        ];

        $assocMetricIdData = [];
        $metricData = Library\Formater::stdClassToArray(Models\MakProgrammaticMetric::searchByObjectiveId($objectiveId));
        foreach ($metricData as $metricInfo) {
            $metricId = $metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_ID];
            if (in_array($metricId, $itemSold)) {
                $metricInfo['option'] = 'itemSold';
            } elseif (in_array($metricId, $gmv)) {
                $metricInfo['option'] = 'gmv';
            } elseif (in_array($metricId, $cost)) {
                $metricInfo['option'] = 'cost';
            } elseif (in_array($metricId, $stockCoverage)) {
                $metricInfo['option'] = 'stockCoverage';
            } elseif (in_array($metricId, $impression)) {
                $metricInfo['option'] = 'impression';
            } elseif (in_array($metricId, $click)) {
                $metricInfo['option'] = 'click';
            } elseif (in_array($metricId, $cpi)) {
                $metricInfo['option'] = 'cpi';
            } elseif (in_array($metricId, $ctr)) {
                $metricInfo['option'] = 'ctr';
            } elseif (in_array($metricId, $cr)) {
                $metricInfo['option'] = 'cr';
            } elseif (in_array($metricId, $keywordRank)) {
                $metricInfo['option'] = 'keywordRank';
            } else {
                $metricInfo['option'] = '';
            }

            unset($metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_ID]);
            unset($metricInfo[Models\MakProgrammaticMetric::COL_FK_MAK_PROGRAMMATIC_OBJECTIVE]);
            unset($metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_UNIT]);

            $assocMetricIdData[$metricId] = $metricInfo;
        }

        $assocKeywordProductIdCampaignId = array_column(
            Library\Formater::stdClassToArray(Models\ProductShopChannelCampaign::searchByShopChannelId($shopChannelId, true)),
            Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
            Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
        );

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $assocChannelAdsIdKeywordData = [];

        $dateTo = date('Y-m-d');
        $actionIdArray = [
            Models\MakProgrammaticAction::DEACTIVATE_KEYWORD,
            Models\MakProgrammaticAction::ACTIVATE_KEYWORD,
        ];
        $settingKeywordData = Library\Formater::stdClassToArray(Models\MakProgrammaticSettingKeyword::searchByShopChannelId($shopChannelId, $actionIdArray));
        foreach ($settingKeywordData as $settingKeywordInfo) {
            $ruleId = $settingKeywordInfo[Models\MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID];
            $ruleExtend = $settingKeywordInfo[Models\MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_EXTEND];
            $actionId = $settingKeywordInfo[Models\MakProgrammaticRule::COL_FK_MAK_PROGRAMMATIC_ACTION];

            $conditionData = Library\Formater::stdClassToArray(Models\MakProgrammaticCondition::searchByRuleId($ruleId));
            $ruleKeywordData = Library\Formater::stdClassToArray(Models\MakProgrammaticSettingKeyword::searchByRuleId($ruleId));
            foreach ($ruleKeywordData as $ruleKeywordInfo) {
                $keywordTurnOnArray = $keywordTurnOffArray = ['keyword' => [], 'keywordProductId' => []];

                $assocActionIdLogKeyword = [];

                $isValid = true;
                $logKeywordArray = [];

                $productShopChannelId = $ruleKeywordInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID];
                $allocationStock = intval($ruleKeywordInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK]);
                $keywordProductId = $ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID];
                $keywordProductStatus = trim($ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS]);
                $keywordCreatedAt = intval($ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_AT]);
                $keywordBiddingPrice = floatval($ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_BIDDING_PRICE]);
                $currentRank = intval($ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CURRENT_RANK] ?: self::CURRENT_RANK_DEFAULT);
                $keywordId = $ruleKeywordInfo[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID];
                $keyword = $ruleKeywordInfo[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME];
                $channelAdsId = $ruleKeywordInfo[Models\ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID];

                foreach ($conditionData as $conditionInfo) {
                    $metricId = $conditionInfo[Models\MakProgrammaticCondition::COL_FK_MAK_PROGRAMMATIC_METRIC];
                    $operatorId = $conditionInfo[Models\MakProgrammaticCondition::COL_FK_MAK_PROGRAMMATIC_OPERATOR];
                    $ruleValue = $conditionInfo[Models\MakProgrammaticCondition::COL_MAK_PROGRAMMATIC_CONDITION_VALUE];

                    $metricOption = $assocMetricIdData[$metricId]['option'];
                    $metricValue = $assocMetricIdData[$metricId][Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_VALUE];
                    $metricName = $assocMetricIdData[$metricId][Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_NAME];

                    #region
                    $data = null;
                    if ($metricOption == 'itemSold') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'gmv') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getGmvKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'cost') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getCostKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'stockCoverage') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $itemSold = MarketingPerformance::getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo);
                        if ($metricValue > 0 && $itemSold > 0) {
                            $data = $allocationStock/($itemSold/$metricValue);
                        }
                    } elseif ($metricOption == 'impression') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getViewKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'click') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getClickKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'cpi') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $cost = MarketingPerformance::getCostKeyword($keywordProductId, $dateFrom, $dateTo);
                        $itemSold = MarketingPerformance::getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo);
                        if ($itemSold > 0) {
                            $data = $cost/$itemSold;
                        }
                    } elseif ($metricOption == 'ctr') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $click = MarketingPerformance::getClickKeyword($keywordProductId, $dateFrom, $dateTo);
                        $impression = MarketingPerformance::getViewKeyword($keywordProductId, $dateFrom, $dateTo);
                        if ($impression > 0) {
                            $data = ($click/$impression) * 100;
                        }
                    } elseif ($metricOption == 'cr') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $itemSold = MarketingPerformance::getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo);
                        $click = MarketingPerformance::getClickKeyword($keywordProductId, $dateFrom, $dateTo);
                        if ($click > 0) {
                            $data = ($itemSold/$click) * 100;
                        }
                    } elseif ($metricOption == 'keywordRank') {
                        $ruleValue = intval($ruleValue);

                        $data = $currentRank;
                    }
                    #endregion

                    $isValidData = false;

                    #region
                    if ($data !== null) {
                        if ($operatorId == Models\MakProgrammaticOperator::EQUALS) {
                            $isValidData = $data == $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::GREATER_THAN) {
                            $isValidData = $data > $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::LESS_THAN) {
                            $isValidData = $data < $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::GREATER_THAN_OR_EQUAL_TO) {
                            $isValidData = $data >= $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::LESS_THAN_OR_EQUAL_TO) {
                            $isValidData = $data <= $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::NOR_EQUAL_TO) {
                            $isValidData = $data != $ruleValue;
                        }
                    }
                    #endregion

                    if ($isValidData) {
                        $checkpoint = $metricName.' = '.$data;

                        $campaignId = $assocKeywordProductIdCampaignId[$keywordProductId] ?? null;
                        $logKeywordArray[] = Models\MakProgrammaticLogKeyword::buildDataInsert(
                            $keywordProductId, $actionId, $ruleId, $checkpoint, $campaignId, Models\Admin::EPSILO_SYSTEM
                        );
                    }

                    $isValid = $isValid && $isValidData;
                }
                if ($isValid && count($conditionData) > 0) {
                    if ($actionId == Models\MakProgrammaticAction::ACTIVATE_KEYWORD) {
                        if ( ! $isLogin) $isLogin = $crawler->login();
                        if ( ! $isLogin) $isLogin = $crawler->login();
                        if ( ! $isLogin) $isLogin = $crawler->login();

                        if ( ! $assocChannelAdsIdKeywordData) {
                            $assocChannelAdsIdKeywordData = $crawler->searchKeywordAndStatus();
                        }
                        if (isset($assocChannelAdsIdKeywordData[intval($channelAdsId)][$keyword]) &&
                            $assocChannelAdsIdKeywordData[intval($channelAdsId)][$keyword] == Library\Crawler\Channel\Shopee::KEYWORD_TURN_OFF) {
                            $keywordTurnOnArray['keyword'][] = $keyword;
                            $keywordTurnOnArray['keywordProductId'][] = $keywordProductId;
                            $assocActionIdLogKeyword[$actionId] = $logKeywordArray;
                        }
                    } elseif ($actionId == Models\MakProgrammaticAction::DEACTIVATE_KEYWORD) {
                        if ( ! $isLogin) $isLogin = $crawler->login();
                        if ( ! $isLogin) $isLogin = $crawler->login();
                        if ( ! $isLogin) $isLogin = $crawler->login();

                        if ( ! $assocChannelAdsIdKeywordData) {
                            $assocChannelAdsIdKeywordData = $crawler->searchKeywordAndStatus();
                        }
                        if (isset($assocChannelAdsIdKeywordData[intval($channelAdsId)][$keyword]) &&
                            $assocChannelAdsIdKeywordData[intval($channelAdsId)][$keyword] == Library\Crawler\Channel\Shopee::KEYWORD_TURN_ON) {
                            $keywordTurnOffArray['keyword'][] = $keyword;
                            $keywordTurnOffArray['keywordProductId'][] = $keywordProductId;
                            $assocActionIdLogKeyword[$actionId] = $logKeywordArray;
                        }
                    }
                }

                if ($keywordTurnOnArray['keyword']) {
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();

                    $isSuccess = $crawler->turnKeywordWithStatus($channelAdsId, $keywordTurnOnArray['keyword'],Library\Crawler\Channel\Shopee::KEYWORD_TURN_ON);
                    if ($isSuccess) {
                        Models\MakProgrammaticKeywordProduct::updateStatusByIdArray(
                            $keywordTurnOnArray['keywordProductId'], Models\MakProgrammaticKeywordProduct::STATUS_RESTORE, Models\Admin::EPSILO_SYSTEM
                        );

                        $logKeyword = $assocActionIdLogKeyword[Models\MakProgrammaticAction::ACTIVATE_KEYWORD] ?? [];
                        if ($logKeyword) {
                            Models\MakProgrammaticLogKeyword::batchInsert($logKeyword);
                        }
                    }
                }

                if ($keywordTurnOffArray['keyword']) {
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();

                    $isSuccess = $crawler->turnKeywordWithStatus($channelAdsId, $keywordTurnOffArray['keyword'],Library\Crawler\Channel\Shopee::KEYWORD_TURN_OFF);
                    if ($isSuccess) {
                        Models\MakProgrammaticKeywordProduct::updateStatusByIdArray(
                            $keywordTurnOffArray['keywordProductId'], Models\MakProgrammaticKeywordProduct::STATUS_DELETED, Models\Admin::EPSILO_SYSTEM
                        );

                        $logKeyword = $assocActionIdLogKeyword[Models\MakProgrammaticAction::DEACTIVATE_KEYWORD] ?? [];
                        if ($logKeyword) {
                            Models\MakProgrammaticLogKeyword::batchInsert($logKeyword);
                        }
                    }
                }

            }
        }
    }

    /**change Bidding Price (Decrease, Increase)
     * @param int $shopChannelId
     */
    public static function changeBiddingPrice($shopChannelId)
    {
        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getById($shopChannelId));

        Library\Common::setTimezoneByChannelId($shopChannelInfo[Models\ShopChannel::COL_FK_CHANNEL]);

        $objectiveId = Models\MakProgrammaticObjective::KEYWORD_SKU;

        $itemSold = [
            Models\MakProgrammaticMetric::KEYWORD_ITEM_SOLD_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_ITEM_SOLD_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_ITEM_SOLD_IN_LAST_14_DAYS
        ];

        $gmv = [
            Models\MakProgrammaticMetric::KEYWORD_GMV_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_GMV_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_GMV_IN_LAST_14_DAYS
        ];

        $cost = [
            Models\MakProgrammaticMetric::KEYWORD_COST_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_COST_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_COST_IN_LAST_14_DAYS
        ];

        $stockCoverage = [
            Models\MakProgrammaticMetric::KEYWORD_STOCK_COVERAGE_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_STOCK_COVERAGE_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_STOCK_COVERAGE_IN_LAST_14_DAYS
        ];

        $impression = [
            Models\MakProgrammaticMetric::KEYWORD_IMPRESSION_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_IMPRESSION_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_IMPRESSION_IN_LAST_14_DAYS
        ];

        $click = [
            Models\MakProgrammaticMetric::KEYWORD_CLICK_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CLICK_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CLICK_IN_LAST_14_DAYS
        ];

        $cpi = [
            Models\MakProgrammaticMetric::KEYWORD_CPI_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CPI_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CPI_IN_LAST_14_DAYS
        ];

        $ctr = [
            Models\MakProgrammaticMetric::KEYWORD_CTR_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CTR_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CTR_IN_LAST_14_DAYS
        ];

        $cr = [
            Models\MakProgrammaticMetric::KEYWORD_CR_IN_LAST_3_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CR_IN_LAST_7_DAYS,
            Models\MakProgrammaticMetric::KEYWORD_CR_IN_LAST_14_DAYS
        ];

        $keywordRank = [
            Models\MakProgrammaticMetric::KEYWORD_SKU_KEYWORD_RANK
        ];

        $assocMetricIdData = [];
        $metricData = Library\Formater::stdClassToArray(Models\MakProgrammaticMetric::searchByObjectiveId($objectiveId));
        foreach ($metricData as $metricInfo) {
            $metricId = $metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_ID];
            if (in_array($metricId, $itemSold)) {
                $metricInfo['option'] = 'itemSold';
            } elseif (in_array($metricId, $gmv)) {
                $metricInfo['option'] = 'gmv';
            } elseif (in_array($metricId, $cost)) {
                $metricInfo['option'] = 'cost';
            } elseif (in_array($metricId, $stockCoverage)) {
                $metricInfo['option'] = 'stockCoverage';
            } elseif (in_array($metricId, $impression)) {
                $metricInfo['option'] = 'impression';
            } elseif (in_array($metricId, $click)) {
                $metricInfo['option'] = 'click';
            } elseif (in_array($metricId, $cpi)) {
                $metricInfo['option'] = 'cpi';
            } elseif (in_array($metricId, $ctr)) {
                $metricInfo['option'] = 'ctr';
            } elseif (in_array($metricId, $cr)) {
                $metricInfo['option'] = 'cr';
            } elseif (in_array($metricId, $keywordRank)) {
                $metricInfo['option'] = 'keywordRank';
            } else {
                $metricInfo['option'] = '';
            }

            unset($metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_ID]);
            unset($metricInfo[Models\MakProgrammaticMetric::COL_FK_MAK_PROGRAMMATIC_OBJECTIVE]);
            unset($metricInfo[Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_UNIT]);

            $assocMetricIdData[$metricId] = $metricInfo;
        }

        $assocKeywordProductIdCampaignId = array_column(
            Library\Formater::stdClassToArray(Models\ProductShopChannelCampaign::searchByShopChannelId($shopChannelId, true)),
            Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
            Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
        );

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $dateTo = date('Y-m-d');
        $actionIdArray = [
            Models\MakProgrammaticAction::INCREASE_BIDDING_PRICE,
            Models\MakProgrammaticAction::DECREASE_BIDDING_PRICE,
        ];
        $settingKeywordData = Library\Formater::stdClassToArray(Models\MakProgrammaticSettingKeyword::searchByShopChannelId($shopChannelId, $actionIdArray));
        foreach ($settingKeywordData as $settingKeywordInfo) {
            $ruleId = $settingKeywordInfo[Models\MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID];
            $ruleExtend = $settingKeywordInfo[Models\MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_EXTEND];
            $actionId = $settingKeywordInfo[Models\MakProgrammaticRule::COL_FK_MAK_PROGRAMMATIC_ACTION];

            $conditionData = Library\Formater::stdClassToArray(Models\MakProgrammaticCondition::searchByRuleId($ruleId));
            $ruleKeywordData = Library\Formater::stdClassToArray(Models\MakProgrammaticSettingKeyword::searchByRuleId($ruleId));
            foreach ($ruleKeywordData as $ruleKeywordInfo) {
                $biddingPriceIncreaseArray = $biddingPriceDecreaseArray = ['assocKeywordPrice' => [], 'assocKeywordProductIdPrice' => []];

                $assocActionIdLogKeyword = [];

                $isValid = true;
                $logKeywordArray = [];

                $productShopChannelId = $ruleKeywordInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID];
                $allocationStock = intval($ruleKeywordInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK]);
                $keywordProductId = $ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID];
                $keywordProductStatus = trim($ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS]);
                $keywordCreatedAt = intval($ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_AT]);
                $keywordBiddingPrice = floatval($ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_BIDDING_PRICE]);
                $currentRank = intval($ruleKeywordInfo[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CURRENT_RANK] ?: self::CURRENT_RANK_DEFAULT);
                $keywordId = $ruleKeywordInfo[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID];
                $keyword = $ruleKeywordInfo[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME];
                $channelAdsId = $ruleKeywordInfo[Models\ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID];

                foreach ($conditionData as $conditionInfo) {
                    $metricId = $conditionInfo[Models\MakProgrammaticCondition::COL_FK_MAK_PROGRAMMATIC_METRIC];
                    $operatorId = $conditionInfo[Models\MakProgrammaticCondition::COL_FK_MAK_PROGRAMMATIC_OPERATOR];
                    $ruleValue = $conditionInfo[Models\MakProgrammaticCondition::COL_MAK_PROGRAMMATIC_CONDITION_VALUE];

                    $metricOption = $assocMetricIdData[$metricId]['option'];
                    $metricValue = $assocMetricIdData[$metricId][Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_VALUE];
                    $metricName = $assocMetricIdData[$metricId][Models\MakProgrammaticMetric::COL_MAK_PROGRAMMATIC_METRIC_NAME];

                    #region
                    $data = null;
                    if ($metricOption == 'itemSold') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'gmv') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getGmvKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'cost') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getCostKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'stockCoverage') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $itemSold = MarketingPerformance::getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo);
                        if ($metricValue > 0 && $itemSold > 0) {
                            $data = $allocationStock/($itemSold/$metricValue);
                        }
                    } elseif ($metricOption == 'impression') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getViewKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'click') {
                        $ruleValue = intval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = MarketingPerformance::getClickKeyword($keywordProductId, $dateFrom, $dateTo);
                    } elseif ($metricOption == 'cpi') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $cost = MarketingPerformance::getCostKeyword($keywordProductId, $dateFrom, $dateTo);
                        $itemSold = MarketingPerformance::getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo);
                        if ($itemSold > 0) {
                            $data = $cost/$itemSold;
                        }
                    } elseif ($metricOption == 'ctr') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $click = MarketingPerformance::getClickKeyword($keywordProductId, $dateFrom, $dateTo);
                        $impression = MarketingPerformance::getViewKeyword($keywordProductId, $dateFrom, $dateTo);
                        if ($impression > 0) {
                            $data = ($click/$impression) * 100;
                        }
                    } elseif ($metricOption == 'cr') {
                        $ruleValue = floatval($ruleValue);

                        $metricValue = intval($metricValue);
                        $dateFrom = Library\Common::convertTimestampToDate(strtotime('today - '.($metricValue - 1).' days'));

                        $data = 0;
                        $itemSold = MarketingPerformance::getItemSoldKeyword($keywordProductId, $dateFrom, $dateTo);
                        $click = MarketingPerformance::getClickKeyword($keywordProductId, $dateFrom, $dateTo);
                        if ($click > 0) {
                            $data = ($itemSold/$click) * 100;
                        }
                    } elseif ($metricOption == 'keywordRank') {
                        $ruleValue = intval($ruleValue);

                        $data = $currentRank;
                    }
                    #endregion

                    $isValidData = false;

                    #region
                    if ($data !== null) {
                        if ($operatorId == Models\MakProgrammaticOperator::EQUALS) {
                            $isValidData = $data == $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::GREATER_THAN) {
                            $isValidData = $data > $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::LESS_THAN) {
                            $isValidData = $data < $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::GREATER_THAN_OR_EQUAL_TO) {
                            $isValidData = $data >= $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::LESS_THAN_OR_EQUAL_TO) {
                            $isValidData = $data <= $ruleValue;
                        } elseif ($operatorId == Models\MakProgrammaticOperator::NOR_EQUAL_TO) {
                            $isValidData = $data != $ruleValue;
                        }
                    }
                    #endregion

                    if ($isValidData) {
                        if ($metricId == Models\MakProgrammaticMetric::KEYWORD_SKU_KEYWORD_RANK && $data > self::AMOUNT_CURRENT_RANK) {
                            $checkpoint = $metricName.' > '.self::AMOUNT_CURRENT_RANK;
                        } else {
                            $checkpoint = $metricName.' = '.$data;
                        }

                        $campaignId = $assocKeywordProductIdCampaignId[$keywordProductId] ?? null;
                        $logKeywordArray[] = Models\MakProgrammaticLogKeyword::buildDataInsert(
                            $keywordProductId, $actionId, $ruleId, $checkpoint, $campaignId, Models\Admin::EPSILO_SYSTEM
                        );
                    }

                    $isValid = $isValid && $isValidData;
                }
                if ($isValid && count($conditionData) > 0) {
                    if ($actionId == Models\MakProgrammaticAction::INCREASE_BIDDING_PRICE && $ruleExtend) {
                        $ruleExtendArray = json_decode($ruleExtend, true);
                        $param = trim($ruleExtendArray['param']);
                        $value = floatval($ruleExtendArray['value']);
                        $min = floatval($ruleExtendArray['min']);
                        $max = floatval($ruleExtendArray['max']);
                        if ($param == 'absolute') {
                            $keywordBiddingPrice += $value;
                        } else {
                            $keywordBiddingPrice += ($keywordBiddingPrice * $value/100);
                        }

                        if ($keywordBiddingPrice >= $min && $keywordBiddingPrice <= $max) {
                            $biddingPriceIncreaseArray['assocKeywordPrice'][$keyword] = $keywordBiddingPrice;
                            $biddingPriceIncreaseArray['assocKeywordProductIdPrice'][$keywordProductId] = $keywordBiddingPrice;
                            $assocActionIdLogKeyword[$actionId] = $logKeywordArray;
                        }

                    } elseif ($actionId == Models\MakProgrammaticAction::DECREASE_BIDDING_PRICE && $ruleExtend) {
                        $ruleExtendArray = json_decode($ruleExtend, true);
                        $param = trim($ruleExtendArray['param']);
                        $value = floatval($ruleExtendArray['value']);
                        $min = floatval($ruleExtendArray['min']);
                        $max = floatval($ruleExtendArray['max']);
                        if ($param == 'absolute') {
                            $keywordBiddingPrice -= $value;
                        } else {
                            $keywordBiddingPrice -= ($keywordBiddingPrice * $value/100);
                        }

                        if ($keywordBiddingPrice >= $min && $keywordBiddingPrice <= $max) {
                            $biddingPriceDecreaseArray['assocKeywordPrice'][$keyword] = $keywordBiddingPrice;
                            $biddingPriceDecreaseArray['assocKeywordProductIdPrice'][$keywordProductId] = $keywordBiddingPrice;
                            $assocActionIdLogKeyword[$actionId] = $logKeywordArray;
                        }
                    }
                }

                if ($biddingPriceIncreaseArray['assocKeywordPrice']) {
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();

                    $isSuccess = $crawler->changeBiddingPrice($channelAdsId, $biddingPriceIncreaseArray['assocKeywordPrice']);
                    if ($isSuccess) {
                        foreach ((array) $biddingPriceIncreaseArray['assocKeywordProductIdPrice'] as $keywordProductId => $keywordBiddingPrice) {
                            Models\MakProgrammaticKeywordProduct::updateBiddingPriceById($keywordProductId, $keywordBiddingPrice, Models\Admin::EPSILO_SYSTEM);
                        }

                        $logKeyword = $assocActionIdLogKeyword[Models\MakProgrammaticAction::INCREASE_BIDDING_PRICE] ?? [];
                        if ($logKeyword) {
                            Models\MakProgrammaticLogKeyword::batchInsert($logKeyword);
                        }
                    }
                }

                if ($biddingPriceDecreaseArray['assocKeywordPrice']) {
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();
                    if ( ! $isLogin) $isLogin = $crawler->login();

                    $isSuccess = $crawler->changeBiddingPrice($channelAdsId, $biddingPriceDecreaseArray['assocKeywordPrice']);
                    if ($isSuccess) {
                        foreach ((array) $biddingPriceDecreaseArray['assocKeywordProductIdPrice'] as $keywordProductId => $keywordBiddingPrice) {
                            Models\MakProgrammaticKeywordProduct::updateBiddingPriceById($keywordProductId, $keywordBiddingPrice, Models\Admin::EPSILO_SYSTEM);
                        }

                        $logKeyword = $assocActionIdLogKeyword[Models\MakProgrammaticAction::DECREASE_BIDDING_PRICE] ?? [];
                        if ($logKeyword) {
                            Models\MakProgrammaticLogKeyword::batchInsert($logKeyword);
                        }
                    }
                }
            }
        }
    }
    # sku - keyword

}