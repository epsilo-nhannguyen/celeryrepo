<?php


namespace App\Services\Dashboard\Shop\Daily;


class MetricModel
{
    protected $shopId;

    protected $currencyCode;

    protected $date;

    protected $hour;

    protected $toolId;

    protected $assocMetricValue;

    /**
     * @return mixed
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param mixed $shopId
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param mixed $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * @param mixed $hour
     */
    public function setHour($hour)
    {
        $this->hour = $hour;
    }

    /**
     * @return mixed
     */
    public function getToolId()
    {
        return $this->toolId;
    }

    /**
     * @param mixed $toolId
     */
    public function setToolId($toolId)
    {
        $this->toolId = $toolId;
    }

    /**
     * @return mixed
     */
    public function getAssocMetricValue()
    {
        return $this->assocMetricValue;
    }

    /**
     * @param mixed $assocMetricValue
     */
    public function setAssocMetricValue($assocMetricValue)
    {
        $this->assocMetricValue = $assocMetricValue;
    }
}