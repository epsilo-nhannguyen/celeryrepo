<?php


namespace App\Services\Dashboard\Shop\Database;


use App\Library\QueryBuilder\MassUpdate;
use App\Models\ModelChart\ChartKeywordCampaignMetricIntraday;
use App\Models\ShopChannel;
use App\Services\Dashboard\MetricManager;
use App\Services\Dashboard\Shop\Daily\MetricModel;
use App\Services\Dashboard\Shop\PrimaryData;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Database
{
    public static function initSchemaCampaign($timeRange, $currencyCode)
    {
        if ($timeRange == 'daily') {
            $table = "dashboard_campaign_metric_daily_reserved_{$currencyCode}";
            \Schema::dropIfExists($table);
            \Schema::create($table, function (Blueprint $blueprint) use ($table) {
                $blueprint->increments('id')->unsigned()	;
                $blueprint->integer('shop_id');
                $blueprint->date('date');
                $blueprint->integer('hour');

                $blueprint->decimal('total_gmv', 14, 3);
                $blueprint->integer('total_item_sold');
                $blueprint->decimal('wallet_balance', 14 ,3);

                $blueprint->timestamp('timestamps')->useCurrent();

                $blueprint->unique(['shop_id', 'date', 'hour'], 'u_'.$table);
            });

            $table = "dashboard_campaign_metric_daily_{$currencyCode}";
            \Schema::dropIfExists($table);
            \Schema::create($table, function (Blueprint $blueprint) use ($table) {
                $blueprint->increments('id')->unsigned()	;
                $blueprint->integer('shop_id');
                $blueprint->integer('tool');
                $blueprint->date('date');
                $blueprint->integer('hour');

                $blueprint->json('primary_data_metrics');

                $blueprint->timestamp('timestamps')->useCurrent();

                $blueprint->unique(['shop_id', 'date', 'hour', 'tool'], 'u_'.$table);
            });
        }
    }

    /**
     * @param $toolName
     * @param $channelCode
     * @return mixed
     */
    public static function getToolId($toolName, $channelCode)
    {
        if (! $toolName) {
            return null;
        }
        $toolName = Str::upper($toolName);
        $channelCode = Str::upper($channelCode);

        return DB::table('dashboard_tool')->get()->filter(function ($item)  use ($toolName, $channelCode){
            return Str::upper($item->name) == $toolName && $item->channel_code == $channelCode;
        })->first()->id;
    }

    public static function getDateHasSaleOnlyPrimary($shopId, $currencyCode, $metric, $limit = 30, $tool = null)
    {
        $currencyCode = Str::upper($currencyCode);
        $metricArray = MetricManager::getInstance()->loadDependency($metric);

        if ($metricArray) {
            return null;
        }

        if (MetricManager::getInstance()->isReservedMetric($metric)) {
            $table = "dashboard_campaign_metric_daily_reserved_{$currencyCode}";
            $select = DB::table($table)
                ->where($metric, '>', 0)
                ->where('shop_id', $shopId)
                ->select(['date'])
                ->limit($limit)
            ;

            $listDate = $select->get()->pluck('date');
        } else {
            $table = "dashboard_campaign_metric_daily_{$currencyCode}";
            $select = DB::table($table)
                ->where('shop_id', $shopId)
                ->select([
                    'date',
                    DB::raw('sum(json_extract(primary_data_metrics, \'$.'.$metric.'\')) as total_data')
                ])
                ->groupBy('date')
                ->having('total_data', '>', 0)
                ->limit($limit)
            ;

            if ($tool) {
                $select->where('tool', $tool);
            }
            $listDate = $select->get()->pluck('date');
        }

        return $listDate;
    }

    public static function loadDaily($arrayDate, $arrayShopId, $currencyCode, $metric, $tool = null)
    {
        $currencyCode = Str::upper($currencyCode);
        $metricArray = MetricManager::getInstance()->loadDependency($metric);
        if (! $metricArray) {
            $metricArray = [$metric];
        }

        $metricsReserved = collect();
        $metricNormal = collect();
        foreach ($metricArray as $metricInLoop) {
            if (MetricManager::getInstance()->isReservedMetric($metricInLoop)) {
                $metricsReserved->push($metricInLoop);
            } else {
                $metricNormal->push($metricInLoop);
            }
        }
        $dataReserved = $dataNormal = collect();

        if ($metricsReserved->count()) {
            $table = "dashboard_campaign_metric_daily_reserved_{$currencyCode}";
            $select = DB::table($table)
                ->whereIn('shop_id', $arrayShopId)
                ->whereIn('date', $arrayDate)
                ->select(array_merge($metricArray, [
                    'date', 'hour', 'shop_id'
                ]))
            ;
            $dataReserved = $select->get()->groupBy(['shop_id', 'date', 'hour']);
        }

        if ($metricNormal->count()) {
            $metricArrayQuery = collect($metricArray)->map(function ($item) {
                return DB::raw('primary_data_metrics->"$.'.$item.'" as '.$item);
            })->all();
            $table = "dashboard_campaign_metric_daily_{$currencyCode}";
            $select = DB::table($table)
                ->whereIn('shop_id', $arrayShopId)
                ->whereIn('date', $arrayDate)
                ->select(array_merge($metricArrayQuery, [
                    'date', 'hour', 'tool', 'shop_id'
                ]))
            ;
            if ($tool) {
                $select->where('tool', $tool);
            }
            $dataNormal = $select->get()->groupBy(['shop_id', 'date', 'hour', 'tool']);
        }
        $result = collect();

        foreach ($arrayShopId as $shopId) {
            foreach ($arrayDate as $dateString) {
                foreach (range(0, 23) as $hour) {
                    if (MetricManager::getInstance()->isReservedMetric($metric)) {
                        $bindingValue = [];
                        foreach ($metricArray as $metricCalculator) {
                            if (!isset($dataReserved[$shopId][$dateString][$hour])) {
                                continue;
                            }
                            $bindingValue[$metricCalculator] = $dataReserved[$shopId][$dateString][$hour]->first()->{$metricCalculator} ?? 0;
                            $model = new MetricModel();
                            $model->setShopId($shopId);
                            $model->setDate($dateString);
                            $model->setHour($hour);
                            $model->setCurrencyCode($currencyCode);
                            $model->setToolId(null);
                            $model->setAssocMetricValue([
                                $metric => MetricManager::getInstance()->calculateMetric($metric, $bindingValue)]
                            );
                            $result->push($model);
                        }
                    } else {
                        $assocToolData = $dataNormal[$shopId][$dateString][$hour] ?? [];

                        foreach ($assocToolData as $toolLoop => $dataTool) {
                            if ($tool && $tool != $toolLoop) {
                                continue;
                            }

                            $bindingValue = [];
                            foreach ($metricArray as $metricCalculator) {
                                if (MetricManager::getInstance()->isReservedMetric($metricCalculator)) {
                                    $bindingValue[$metricCalculator] = $dataReserved[$shopId][$dateString][$hour]->first()->{$metricCalculator} ?? 0;
                                } else {
                                    $bindingValue[$metricCalculator] = $dataNormal[$shopId][$dateString][$hour][$toolLoop]->first()->{$metricCalculator} ?? 0;
                                }
                            }
                            $model = new MetricModel();
                            $model->setShopId($shopId);
                            $model->setDate($dateString);
                            $model->setHour($hour);
                            $model->setCurrencyCode($currencyCode);
                            $model->setToolId($toolLoop);
                            $model->setAssocMetricValue([
                                $metric => MetricManager::getInstance()->calculateMetric($metric, $bindingValue)]
                            );
                            $result->push($model);
                        }
                    }

                }
            }
        }

        return $result;
    }


    public static function saveDaily(Collection $collection, $currencyCode, $toolId)
    {
        $collection = $collection->filter(function ($item) {
            return $item instanceof MetricModel;
        });

        $collectionMetricNormal = collect();
        $collectionMetricReserved = collect();
        foreach ($collection as $metricModel) {
            /** @var MetricModel $metricModel */
            $collectionMetric = collect($metricModel->getAssocMetricValue());
            $listMetricReserved = $collectionMetric->filter(function ($item, $key) {
                return MetricManager::getInstance()->isReservedMetric($key);
            });

            $listMetricNormal = $collectionMetric->filter(function ($item, $key) {
                return ! MetricManager::getInstance()->isReservedMetric($key);
            });

            if ($listMetricReserved->count()) {

                $newMetric = clone $metricModel;
                $newMetric->setAssocMetricValue($listMetricReserved->all());

                $collectionMetricReserved->push($newMetric);
            }

            if ($listMetricNormal->count()) {
                $newMetric = clone $metricModel;
                $newMetric->setAssocMetricValue($listMetricNormal->all());

                $collectionMetricNormal->push($newMetric);
            }
        }

        if ($collectionMetricReserved->count()) {
            $table = "dashboard_campaign_metric_daily_reserved_{$currencyCode}";

            $collectionMetricReservedGrouped = $collectionMetricReserved->groupBy([
                function (MetricModel $metricModel) {
                    return $metricModel->getShopId();
                },
                function (MetricModel $metricModel) {
                    return $metricModel->getDate();
                }
            ]);
            $assocShopIdDate = $collectionMetricReserved->mapToGroups(function (MetricModel $item) {
                return [$item->getShopId() => $item->getDate()];
            })->all();

            $dataInDb = static::findDailyMetric($assocShopIdDate, $table, null)->groupBy([
                'shop_id', 'date', 'hour'
            ]);

            $dataInsert = collect();
            $dataUpdatedGmv = new MassUpdate();
            $dataUpdatedWallet = new MassUpdate();
            $dataUpdatedItemSold = new MassUpdate();

            $dataUpdatedGmv->setTableUpdated($table);
            $dataUpdatedGmv->setColumnCondition('id');
            $dataUpdatedGmv->setColumnUpdated('total_gmv');

            $dataUpdatedWallet->setTableUpdated($table);
            $dataUpdatedWallet->setColumnCondition('id');
            $dataUpdatedWallet->setColumnUpdated('wallet_balance');

            $dataUpdatedItemSold->setTableUpdated($table);
            $dataUpdatedItemSold->setColumnCondition('id');
            $dataUpdatedItemSold->setColumnUpdated('total_item_sold');

            foreach ($collectionMetricReservedGrouped as $shopId => $assocDate) {
                foreach ($assocDate as $date => $assocHour) {
                    foreach ($assocHour as $hour => $dataSave) {
                        /** @var MetricModel $dataSave */
                        $_hour = intval($dataSave->getHour());
                        if ( ! isset($dataInDb[$shopId][$date][$_hour])) {
                            $dataInsert->push([
                                'shop_id' => $shopId,
                                'date' => $date,
                                'hour' => $_hour,
                                'total_gmv' => $dataSave->getAssocMetricValue()['total_gmv'] ?? 0,
                                'wallet_balance' => $dataSave->getAssocMetricValue()['wallet_balance'] ?? null,
                                'total_item_sold' => $dataSave->getAssocMetricValue()['total_item_sold'] ?? 0,
                            ]);
                        } else {
                            $idUpdated = $dataInDb[$shopId][$date][$_hour]->first()->id;
                            $dataUpdatedGmv->addCase($idUpdated, $dataSave->getAssocMetricValue()['total_gmv'] ?? 0);
                            $dataUpdatedWallet->addCase($idUpdated, $dataSave->getAssocMetricValue()['wallet_balance'] ?? null);
                            $dataUpdatedItemSold->addCase($idUpdated, $dataSave->getAssocMetricValue()['total_item_sold'] ?? 0);
                        }
                    }

                }
            }
            if ($dataUpdatedGmv->getRowsUpdated()) {
                DB::statement($dataUpdatedGmv->assemble());
            }

            if ($dataUpdatedWallet->getRowsUpdated()) {
                DB::statement($dataUpdatedWallet->assemble());
            }

            if ($dataUpdatedItemSold->getRowsUpdated()) {
                DB::statement($dataUpdatedItemSold->assemble());
            }

            if ($dataInsert->count()) {
                DB::table($table)->insert($dataInsert->all());
            }
        }

        if ($collectionMetricNormal->count()) {
            $table = "dashboard_campaign_metric_daily_{$currencyCode}";

            $collectionMetricNormalGrouped = $collectionMetricNormal->groupBy([
                function (MetricModel $metricModel) {
                    return $metricModel->getShopId();
                },
                function (MetricModel $metricModel) {
                    return $metricModel->getDate();
                }
            ]);

            $assocShopIdDate = $collectionMetricNormal->mapToGroups(function (MetricModel $item) {
                return [$item->getShopId() => $item->getDate()];
            })->all();

            $dataInDb = static::findDailyMetric($assocShopIdDate, $table, $toolId)->groupBy([
                'shop_id', 'date', 'hour'
            ]);

            $dataInsert = collect();
            $dataUpdatedPrimaryMetrics = new MassUpdate();
            $dataUpdatedPrimaryMetrics->setTableUpdated($table);
            $dataUpdatedPrimaryMetrics->setColumnUpdated('primary_data_metrics');
            $dataUpdatedPrimaryMetrics->setColumnCondition('id');

            foreach ($collectionMetricNormalGrouped as $shopId => $assocDate) {
                foreach ($assocDate as $date => $assocHour) {
                    foreach ($assocHour as $hour => $dataSave) {
                        /** @var MetricModel $dataSave */
                        $_hour = intval($dataSave->getHour());
                        if ( ! isset($dataInDb[$shopId][$date][$_hour])) {
                            $dataInsert->push([
                                'shop_id' => $shopId,
                                'date' => $dataSave->getDate(),
                                'hour' => $_hour,
                                'tool' => $dataSave->getToolId(),
                                'primary_data_metrics' => json_encode($dataSave->getAssocMetricValue())
                            ]);
                        } else {
                            // bug laravel can not update json field
                            $valueMetric = json_encode($dataSave->getAssocMetricValue());
                            $dataUpdatedPrimaryMetrics->addCase($dataInDb[$shopId][$date][$_hour]->first()->id, $valueMetric);
                        }
                    }

                }
            }

            if ($dataUpdatedPrimaryMetrics->getRowsUpdated()) {
                DB::statement($dataUpdatedPrimaryMetrics->assemble());
            }

            if ($dataInsert->count()) {
                DB::table($table)->insert($dataInsert->all());
            }
        }
    }

    public static function getByShopDateHour($shop, $date, $hour, $metric, $tool, $currencyCode)
    {
        $currencyCode = strtoupper($currencyCode);
        $result = null;
        if (MetricManager::getInstance()->isReservedMetric($metric)) {
            $table = 'dashboard_campaign_metric_daily_reserved_'.$currencyCode;
            $resultSql = DB::table($table)->select()
                ->where('shop_id', $shop)
                ->where('date', $date)
                ->where('hour', $hour)
                ->get()
                ->first()
            ;
            if ($resultSql) {
                $result = $resultSql->{$metric} ?? 0;
            }
        } else {
            $table = 'dashboard_campaign_metric_daily_'.$currencyCode;
            $resultSql = DB::table($table)->select()
                ->where('shop_id', $shop)
                ->where('date', $date)
                ->where('hour', $hour)
                ->where('tool', $tool)
                ->get()
                ->first()
            ;
            if ($resultSql) {
                $json = json_decode($resultSql->primary_data_metrics, true);
                $result = $json[$metric] ?? 0;
            }

        }

        return $result;
    }

    /**
     * @param $assocShopIdArrayDate
     * @param $table
     * @param $toolId
     * @return Collection
     */
    public static function findDailyMetric($assocShopIdArrayDate, $table, $toolId)
    {
        $select = DB::table($table)
            ->select(['id', 'hour', 'shop_id', 'date'])
        ;

        $whereTool = '';
        if ($toolId) {
            $whereTool = 'tool = '.$toolId;
        }

        $_where = [];
        foreach ($assocShopIdArrayDate as $shopId => $arrayDate) {
            /** @var Collection $arrayDate */
            $_where[] = '`shop_id` = ' . $shopId. ' and `date` in (' . implode(',', $arrayDate->unique()->map(function ($item) {
                    return "'".$item."'";
                })->all()).')';
        }

        $where = [];
        if ($whereTool) {
            $where[] = $whereTool;
        }

        if ($_where) {
            $where[] = '(' . implode(') or (', $_where) .')';
        }

        $select->whereRaw(DB::raw(implode(' and ', $where)));
        return $select->get();
    }
}