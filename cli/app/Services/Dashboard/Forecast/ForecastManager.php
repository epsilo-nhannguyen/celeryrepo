<?php


namespace App\Services\Dashboard\Forecast;


use App\Models\ModelChart\ChartKeywordCampaignMetricIntraday;
use App\Models\ShopChannel;
use App\Services\Dashboard\Forecast;
use App\Services\Dashboard\Shop\Daily\MetricModel;
use App\Services\Dashboard\Shop\Database\Database;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ForecastManager
{
    protected $typeForecast = 'default';
    protected $forecastStrategy;
    public function __construct($typeForecast = 'default')
    {
        $this->typeForecast = $typeForecast;
        switch ($typeForecast) {
            default:
                $this->forecastStrategy = new Forecast\Strategy\Contribution();
                break;
        }
    }

    public function forecast($metric, $currentValue, $currencyCode, $tool, $shopId)
    {
        $dataPrepare = $this->forecastStrategy->prepareData($metric, $currencyCode, $shopId, $tool);
        /*$dataPrepare = $dataPrepare->mapWithKeys(function (MetricModel $item) use ($metric) {
            $key = $item->getDate() . ' | ' . $item->getHour();
            return [$key => ($item->getAssocMetricValue()[$metric] ?? 0)];
        });*/
        #$dataPrepare = static::buildInterquartileRange($dataPrepare);
        #print_r($dataPrepare);die;
        $data =  $this->forecastStrategy->doForecast($dataPrepare, $metric, $currentValue);
        return $data;
    }


    /**
     * @param Collection $range
     * @return Collection
     * @link : https://www.mathworks.com/matlabcentral/cody/problems/42485-eliminate-outliers-using-interquartile-range
     */
    public static function buildInterquartileRange(Collection $range)
    {
        $newRange = self::_buildInterquartileRange($range);

        if ($newRange->count() < $range->count()) {
            $newRange = self::_buildInterquartileRange($newRange);
        }

        return $newRange;
    }

    /**
     * @param Collection $range
     * @return Collection
     */
    public static function _buildInterquartileRange(Collection $range)
    {
        $rangeSorted = $range->sort()->split(2);
        $medianList = $rangeSorted->map(function (Collection $item) {
            return $item->median();
        });

        $avg = $range->avg();
        $iqr = $medianList->last() - $medianList->first();
        foreach ($range as $key => $value) {
            if (abs($value - $avg) > 1.5 * $iqr) {
                $range->forget($key);
            }
        }
        return $range;
    }
}