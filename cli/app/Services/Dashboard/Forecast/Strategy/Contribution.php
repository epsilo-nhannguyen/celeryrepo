<?php


namespace App\Services\Dashboard\Forecast\Strategy;


use App\Services\Dashboard\Forecast\ForecastManager;
use App\Services\Dashboard\Forecast\IForecast;
use App\Services\Dashboard\Shop\Daily\MetricModel;
use App\Services\Dashboard\Shop\Database\Database;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SebastianBergmann\CodeCoverage\Report\PHP;

class Contribution implements IForecast
{
    public function prepareData($metricName, $currencyCode, $shopId, $tool)
    {
        /*$listDate = Database::getDateHasSaleOnlyPrimary($shopId, $currencyCode, $metricName, 30, $tool)->all();
        if (!$listDate) return collect();*/
        $listDate = collect();
        $period = CarbonPeriod::create(date('Y-m-d', strtotime('-30 days')), date('Y-m-d', strtotime('-1 days')));
        foreach ($period as $date) {
            /** @var \DateTime $date */
            $listDate->push($date->format('Y-m-d'));
        }
        return Database::loadDaily(
            $listDate->all(), [$shopId], $currencyCode, $metricName, $tool
        );
    }


    public function doForecast(Collection $dataBefore30days, $metricName, $currentValue)
    {
        $dataBefore30daysGrouped = $dataBefore30days->groupBy(function (MetricModel $item) {
            return $item->getDate();
        })->map(function (Collection $itemGrouped) use ($metricName) {
            return $itemGrouped->sum(function (MetricModel $child) use ($metricName) {
                return $child->getAssocMetricValue()[$metricName] ?? 0;
            });
        });

        /** @var Collection $percentHourToDate */
        $percentHourToDate = $dataBefore30days->reduce(function (Collection $result, MetricModel $item) use ($dataBefore30daysGrouped, $metricName) {
            $value = $item->getAssocMetricValue()[$metricName] ?? 0;
            $valueByDate = $dataBefore30daysGrouped->get($item->getDate());
            if (!$valueByDate) {
                return $result;
            }
            $percent = $value / $valueByDate;
            if (! $result->get($item->getHour())) {
                $result->put($item->getHour(), collect());
            }
            $result->put(
                $item->getHour(),
                $result->get($item->getHour())->push($percent)
            );

            return $result;
        }, collect());

        $percentHourContribution = $percentHourToDate->map(function (Collection $item) {
            return $item->avg();
        });

        $result = collect();
        $currentHour = intval(date('H'));
        $first = $dataBefore30days->first();
        /** @var MetricModel $first */

        foreach (range($currentHour + 1, 23) as $hour) {

            $percentCurrent = $percentHourContribution[$currentHour] ?? 0;
            $percentByHour = $percentHourContribution[$hour] ?? 0;
            if (!$percentCurrent || !$percentByHour) {
                continue;
            }

            $forecastValue = $currentValue / $percentCurrent * $percentByHour;
            $model = new MetricModel();
            $model->setShopId($first->getShopId());
            $model->setCurrencyCode($first->getCurrencyCode());
            $model->setHour($hour);
            $model->setToolId($first->getToolId());
            $model->setDate(date('Y-m-d'));
            $model->setAssocMetricValue([$metricName => $forecastValue]);
            $result->push($model);
        }
        return $result;
    }
}