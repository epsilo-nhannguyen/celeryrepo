<?php


namespace App\Services\Dashboard;


use App\Library\Algorithm\ShuntingYard\Math;
use Illuminate\Support\Facades\DB;

class MetricManager
{
    /**
     * @var \Illuminate\Support\Collection
     */
    protected static $metrics;

    protected static $instance = null;

    /**
     * @var Math
     */
    protected static $math;

    protected function __construct()
    {
        self::$metrics = DB::table('dashboard_metric')->get();
        static::$math = new Math();
    }

    /**
     * @return $this
     */
    public static function getInstance()
    {
        if ( ! static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function isPrimaryMetric($metricCode)
    {
        $metricFound = self::$metrics->pluck('type_data', 'code');
        $type = $metricFound[$metricCode] ?? null;
        return $type == 'primary_data';
    }

    public function isReservedMetric($metric)
    {
        return in_array($metric, ['total_gmv', 'total_item_sold', 'wallet_balance']);
    }

    public function loadDependency($metric)
    {
        $expression = static::$metrics->pluck('expression', 'code')->get($metric);
        $result = [];
        $index = 0;
        $flag = false;
        for ($i = 0; $i < strlen($expression); $i++) {
            $char = $expression[$i];
            if ($char == '{') {
                $result[$index] = '';
                $flag = true;
                continue;
            }

            if ($char == '}') {
                $flag = false;
                $index++;
                continue;
            }

            if ($flag) {
                $result[$index] .= $char;
            }
        }

        return array_unique(array_values($result));
    }

    public function calculateMetric($metric, array $bindingValue)
    {
        $result = null;
        if ($this->isPrimaryMetric($metric)) {
            $result = $bindingValue[$metric] ?? null;
        } else {

            $expression = static::$metrics->pluck('expression', 'code')->get($metric);
            foreach ($bindingValue as $metricDepend => $value) {
                $expression = str_replace("{{$metricDepend}}", $value, $expression);
            }

            $result = static::$math->evaluate($expression);
        }

        return trim($result, '"');
    }

    public function getMetricForecast($metric, $currentMetricValue)
    {
        $typeForecast = static::$metrics->pluck('type_forecast', 'code')->get($metric);


    }
}