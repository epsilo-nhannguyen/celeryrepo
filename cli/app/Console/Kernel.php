<?php

namespace App\Console;

use App\Console;
use App\Jobs\Dashboard;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Console\Commands\TestTikiToken::class,
        Console\Commands\TestPingPong::class,
        Console\Commands\Index::class,
        Console\Commands\RawCategory::class,
        Console\Commands\ImportCategory::class,
        Console\Commands\PullProduct::class,
        Console\Commands\ImportProduct::class,
        Console\Commands\MappingProductChannelCategory::class,
        Console\Commands\CampaignBudget::class,
        Console\Commands\CampaignTimeline::class,
        Console\Commands\ShopChannelBalance::class,
        Console\Commands\RawKeywordProduct::class,
        Console\Commands\ImportProductAds::class,
        Console\Commands\RawCrawlerAds::class,
        Console\Commands\ImportCrawlerAds::class,
        Console\Commands\PullOrder::class,
        Console\Commands\Dashboard\KeywordBidding\FetchChartKeywordMetric::class,
        Console\Commands\Dashboard\KeywordBidding\FetchChartKeywordAction::class,
        Console\Commands\SnapshotProductLog::class,

        Console\Commands\CommandsReport\PullBalanceElasticSearch::class,
        Console\Commands\CommandsReport\ReportTotalShop::class,
        Console\Commands\CommandsReport\ReportTrackerGroup1::class,
        Console\Commands\CommandsReport\ReportTrackerPerformance::class,
        Console\Commands\CommandsReport\ReportTrackerSaleOrder::class,

        Console\Commands\CommandsReport\ReportTrackerSkuKeywordGroup1::class,

        Console\Commands\CommandsTikiAd\RawPerformance::class,

        Console\Commands\RawKeywordSuggestShopee::class,
        #region Shopee
        Console\Commands\RawCrawlShopAdsKeyword::class,
        Console\Commands\RawCrawlShopAdsKeywordPerformance::class,
        Console\Commands\RawCrawlShopAdsPosition::class,
        Console\Commands\RawKeywordMachineShopee::class,
        Console\Commands\RawKeywordBiddingShopee::class,

        Console\Commands\Dashboard\ShopAds\ShopAdsFetchChartKeywordMetric::class,
        Console\Corrector\ShopAdsKeyword::class,
        #Commands\Shopee\KeywordBidding\CrawlerPerformanceHour::class,

        Commands\Shopee\KeywordBidding\ShopPerformanceHourly::class,
        Console\Commands\Shopee\KeywordBidding\GetKeyword::class,
        #endregion

        #region Tokopedia
        Console\Commands\Dashboard\CalculateReservedMetric::class,
        Console\Commands\Shopee\ShopAds\ShopAdsPushDataDashboard::class,

        #region Tokopedia
        Console\Commands\Toko\KeywordAds\CrawProduct::class,
        Console\Commands\Toko\KeywordAds\CrawCampaignData::class,
        Console\Commands\Toko\KeywordAds\CampaignTimeline::class,
        Console\Commands\CrawlTokopediaGroupAds::class,
        Console\Commands\CrawlTokopediaProductAds::class,
        Console\Commands\CrawlTokopediaKeywordAds::class,
        Console\Commands\CrawlTokopediaGroupAdsPerformance::class,
        Console\Commands\CrawlTokopediaProductAdsPerformance::class,
        Console\Commands\CrawlTokopediaKeywordAdsPerformance::class,
        Console\Commands\CrawlTokopediaListProduct::class,
        Console\Commands\CrawlTokopediaKeywordAdsPerformance::class,
        Console\Commands\Toko\KeywordAds\CrawCampaignPerformanceData::class,
        Console\Commands\Toko\KeywordAds\CrawCampaignPerformanceData::class,
        #endregion

        #region Tiki
        Console\Commands\CommandsTikiAd\RawPerformance::class,
        Console\Commands\CommandsTikiAd\ProductShopChannel::class,
        Console\Commands\CommandsTikiAd\KeywordAds::class,
        #endregion

        Console\Commands\Shopee\FillShopeeWalletBalance::class,
        Console\Commands\Shopee\CrawlWalletBalance::class,
        Console\Commands\DispatchJob::class,
        Console\Commands\CeleryCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        # Stat
        $schedule->command(Console\Commands\CommandsReport\PullBalanceElasticSearch::class)->dailyAt('04:00');
        $schedule->command(Console\Commands\CommandsReport\ReportTotalShop::class)->dailyAt('04:00');
        $schedule->command(Console\Commands\CommandsReport\ReportTrackerGroup1::class)->dailyAt('04:00');
        $schedule->command(Console\Commands\CommandsReport\ReportTrackerPerformance::class)->dailyAt('04:00');
        $schedule->command(Console\Commands\CommandsReport\ReportTrackerSaleOrder::class)->dailyAt('04:00');

        $schedule->command(Console\Commands\CommandsReport\ReportTrackerSkuKeywordGroup1::class)->dailyAt('04:00');

        # end Stat
        $schedule->command(Console\Commands\Dashboard\KeywordBidding\FetchChartKeywordMetric::class)->hourlyAt(59);
//        $schedule->command(Console\Commands\CommandsTikiAd\RawPerformance::class)->dailyAt('05:00')->skip(function () {
//            $today = date('Y-m-d');
//            return $today > '2020-01-26';
//        });
        $schedule->command(Console\Commands\Shopee\KeywordBidding\ShopPerformanceHourly::class)->everyFifteenMinutes();
        $schedule->command(Console\Commands\Shopee\ShopAds\ShopAdsPushDataDashboard::class)->everyFifteenMinutes();
        $schedule->command(Console\Commands\Dashboard\CalculateReservedMetric::class)->everyFifteenMinutes();

        $schedule->command(Console\Commands\Dashboard\Shop\SyncToMetricDaily::class)->everyTenMinutes();

        #region Tiki
        $schedule->command(Console\Commands\CommandsTikiAd\ProductShopChannel::class)->everyFiveMinutes();
        $schedule->command(Console\Commands\CommandsTikiAd\KeywordAds::class)->everyTenMinutes();
        #endregion

        # cront toko
        $schedule->command(Console\Commands\Toko\KeywordAds\CrawProduct::class)->everyTenMinutes();
        $schedule->command(Console\Commands\Toko\KeywordAds\CrawCampaignData::class)->everyTenMinutes();
        $schedule->command(Console\Commands\Toko\KeywordAds\CrawCampaignPerformanceData::class)->everyTenMinutes();
        # $schedule->command(Console\Commands\Toko\KeywordAds\CampaignTimeline::class)->hourly();
        $schedule->command(Console\Commands\Toko\KeywordAds\CampaignTimeline::class)->everyTenMinutes(); # for QC test, after will be delete this row
    }
}
