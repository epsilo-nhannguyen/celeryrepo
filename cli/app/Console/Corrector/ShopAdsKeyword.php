<?php

namespace App\Console\Corrector;

use App\Library\Crawler;
use App\Library;
use App\Models;
use App\Models\ModelBusiness;
use Exception;
use Illuminate\Console\Command;

class ShopAdsKeyword extends Command
{
    const IS_SHOP_ADS = 3;

    /**
     * @uses:
     * php artisan corrector:shop-ads-keyword
     * example: php artisan corrector:shop-ads-keyword
     */

    /**
     * @var string
     */
    protected $signature = 'corrector:shop-ads-keyword';

    /**
     * @var string
     */
    protected $description = 'corrector shop ads keyword pull data from shopee';

    public function handle()
    {
        ini_set('memory_limit', -1);
        # Pre data
        $assocShopAdsId_shopData = [];
        $arrayKeywordPerformance = ModelBusiness\ShopAdsKeywordPerformance::getByKeywordIsNull(10000);
        $allShopActive = ModelBusiness\ShopAds::getAllShopActiveWithOrganizationIdAndVentureId()->toArray();
        if (!$allShopActive) {
            //something error
            $dataLog = [
                "Error" => 'Error no shop active!!!',
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            throw new Exception('Error no shop active!!!');
        }

        foreach ($allShopActive as $shopActive) {
            $assocShopAdsId_shopData[$shopActive->id] = $shopActive;
        }

        /**
         * @var $cacheShopeeService Crawler\Channel\Shopee[]
         */
        $cacheShopeeService = [];
        $assocShopAdsId_arrayKeywordName = [];

        # Process get mapping keyword shop missing
        foreach ($arrayKeywordPerformance as $keywordPerformance) {
            $recordId = $keywordPerformance->id;
            $shopAdsId = $keywordPerformance->shop_ads_id;
            $keywordName = $keywordPerformance->keyword_name;
            $_shopData = $assocShopAdsId_shopData[$shopAdsId] ?? null;
            # Verify shop data
            if (!$_shopData) {
                //something error
                $dataLog = [
                    "ShopAdsId" => $shopAdsId,
                    "Error" => 'Shop Ads not have data!!!',
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                continue;
            }
            if (!isset($cacheShopeeService[$shopAdsId])) {
                if (!$_shopData->shop_channel_id) {
                    //something error
                    $dataLog = [
                        "ShopAdsId" => $shopAdsId,
                        "Error" => 'Shop Ads not have shop channel Id!!!',
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
                    continue;
                }
                $cacheShopeeService[$shopAdsId] = new Crawler\Channel\Shopee($_shopData->shop_channel_id);
            }
            $assocShopAdsId_arrayKeywordName[$shopAdsId][$keywordName][] = intval($recordId);
        }

        # Process try crawl shop ads keyword again (insertGetId) for each shop
        foreach ($cacheShopeeService as $shopAdsId => $shopeeService) {
            # Pre data use to mapping fk and Update shop ads keyword id
            $listKeywordNameAndRecordIdToUpdate = $assocShopAdsId_arrayKeywordName[$shopAdsId];
            $allShopAdsKeywordByShop = ModelBusiness\ShopAdsKeyword::getAllByShopAdsId($shopAdsId);
            $shopData = $assocShopAdsId_shopData[$shopAdsId];
            $listKeywordExist = Library\Formater::stdClassToArray(Models\MakProgrammaticKeyword::searchByOrganizationIdAndVentureId($shopData->fk_organization, $shopData->fk_venture));
            $listKeywordName = [];
            $listShopAdsKeywordExist = [];
            foreach ($listKeywordExist as $keyword) {
                $listKeywordName[$keyword[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME]] = $keyword[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID];
            }
            foreach ($allShopAdsKeywordByShop as $shopAdsKeyword)
            {
                $listShopAdsKeywordExist[$shopAdsKeyword->keyword_name] = $shopAdsKeyword->id;
            }

            # Login to get cookie
            $isLoginSuccess = $shopeeService->login();
            if (!$isLoginSuccess)
                $isLoginSuccess = $shopeeService->login();
            if (!$isLoginSuccess)
                $isLoginSuccess = $shopeeService->login();
            if (!$isLoginSuccess) {
                $dataLog = [
                    "ShopAdsId" => $shopAdsId,
                    "Error" => 'Corrector Shop Ads Keyword Login Failure!!!',
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                throw new Exception('Login failure!!!');
            }

            # Crawl data (Retry 3 time)
            $crawlData = $shopeeService->crawlShopAdsKeyword();
            if (!$crawlData)
                $crawlData = $shopeeService->crawlShopAdsKeyword();
            if (!$crawlData)
                $crawlData = $shopeeService->crawlShopAdsKeyword();

            # Verify and Parse response
            $listAdsKeyword = [];
            if (!$crawlData || $crawlData['err_code'] != 0) {
                $dataLog = [
                    "ShopAdsId" => $shopAdsId,
                    "Error" => 'Crawl with null value or error',
                    "Data" => $crawlData,
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                continue;
            }
            if (!isset($crawlData['data'])) {
                $dataLog = [
                    "ShopAdsId" => $shopAdsId,
                    "Error" => 'Channel change params response',
                    "Data" => $crawlData,
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                throw new Exception('Channel change params response');
            }
            if (!isset($crawlData['data']['campaign_ads_list'])) {
                $dataLog = [
                    "ShopAdsId" => $shopAdsId,
                    "Error" => 'Channel change params response',
                    "Data" => $crawlData,
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                throw new Exception('Channel change params response');
            }

            $campaignAdsList = $crawlData['data']['campaign_ads_list'];

            foreach ($campaignAdsList as $campaign) {
                if (!isset($campaign['advertisements'])) {
                    $dataLog = [
                        "ShopAdsId" => $shopAdsId,
                        "Error" => 'Channel change params response',
                        "Data" => $crawlData,
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
                    throw new Exception('Channel change params response');
                }
                if (!isset($campaign['campaign'])) {
                    $dataLog = [
                        "ShopAdsId" => $shopAdsId,
                        "Error" => 'Channel change params response',
                        "Data" => $crawlData,
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
                    throw new Exception('Channel change params response');
                }

                // Get list ads key word & ads id & campaign id - On Advertisements
                foreach ($campaign['advertisements'] as $advertisement) {
                    if ($advertisement['placement'] == self::IS_SHOP_ADS) {
                        if (!isset($advertisement['extinfo']) || !isset($advertisement['extinfo']['keywords'])) {
                            $dataLog = [
                                "ShopAdsId" => $shopAdsId,
                                "Error" => 'Channel change params response',
                                "Data" => $crawlData,
                            ];
                            Library\LogError::logErrorToTextFile($dataLog);
                            throw new Exception('Channel change params response');
                        }
                        $listAdsKeyword = $advertisement['extinfo']['keywords'];
                        unset($advertisement['extinfo']);
                    }
                }
            }

            # Process insert or update data
            foreach ($listAdsKeyword as $adsKeyword) {
                if (!array_key_exists($adsKeyword['keyword'], $listShopAdsKeywordExist)) {
                    $recordInsert = Models\ModelBusiness\ShopAdsKeyword::buildDataInsert(
                        $shopAdsId,
                        $listKeywordName[$adsKeyword['keyword']] ?? null,
                        $adsKeyword['keyword'],
                        $adsKeyword['status'],
                        $adsKeyword['match_type'],
                        $adsKeyword['price'],
                        $adsKeyword
                    );
                    $keywordIdInsertSuccess = Models\ModelBusiness\ShopAdsKeyword::insertGetId($recordInsert);
                    if (!$keywordIdInsertSuccess) {
                        $dataLog = [
                            "ShopAdsId" => $shopAdsId,
                            "Error" => 'Insert shop ads keyword failure!!',
                            "Data" => $recordInsert
                        ];
                        Library\LogError::logErrorToTextFile($dataLog);
                        continue;
                    } else {
                        # Update keyword pfm with shop ads keyword id = null
                        if(array_key_exists($adsKeyword['keyword'],$listKeywordNameAndRecordIdToUpdate)) {
                            $isUpdateIdSuccess = ModelBusiness\ShopAdsKeywordPerformance::updateShopAdsKeywordIdByKeywordName($listKeywordNameAndRecordIdToUpdate[$adsKeyword['keyword']],$keywordIdInsertSuccess);
                            if(!$isUpdateIdSuccess) {
                                $dataLog = [
                                    "ShopAdsId" => $shopAdsId,
                                    "KeywordName" => $adsKeyword['keyword'],
                                    "Error" => 'Update shop ads keyword id failure!!',
                                    "RecordId" => $listKeywordNameAndRecordIdToUpdate[$adsKeyword['keyword']]
                                ];
                                Library\LogError::logErrorToTextFile($dataLog);
                            }
                        }
                    }
                } else {
                    # Update keyword pfm with shop ads keyword id = null
                    if(array_key_exists($adsKeyword['keyword'],$listKeywordNameAndRecordIdToUpdate)) {
                        $isUpdateIdSuccess = ModelBusiness\ShopAdsKeywordPerformance::updateShopAdsKeywordIdByKeywordName($listKeywordNameAndRecordIdToUpdate[$adsKeyword['keyword']],$listShopAdsKeywordExist[$adsKeyword['keyword']]);
                        if(!$isUpdateIdSuccess) {
                            $dataLog = [
                                "ShopAdsId" => $shopAdsId,
                                "KeywordName" => $adsKeyword['keyword'],
                                "Error" => 'Update shop ads keyword id failure!!',
                                "RecordId" => $listKeywordNameAndRecordIdToUpdate[$adsKeyword['keyword']]
                            ];
                            Library\LogError::logErrorToTextFile($dataLog);
                        }
                    }
                }
            }
        }
    }
}