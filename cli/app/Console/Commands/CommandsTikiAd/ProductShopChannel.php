<?php

namespace App\Console\Commands\CommandsTikiAd;

use App\Jobs\Tiki\CrawlProductInfo;
use Illuminate\Console\Command;
use App\Library\LogError;
use App\Models;
use App\Library;

class ProductShopChannel extends Command
{
    /**
     * @uses: php artisan tiki-ad:product-shop-channel --arrayShopId=shop-master-id
     * example: php artisan tiki-ad:product-shop-channel --arrayShopId=532,321,... (nullable)
     */

    protected $signature = 'tiki-ad:product-shop-channel {--arrayShopId=}';

    protected $description = 'Crawl and import product shop channel Tiki';

    public function handle()
    {
        $arrayShopId = $this->option('arrayShopId');
        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $channelTiki = Models\Channel::getChannelTiKi();

        $allShopChannel = Models\ModelBusiness\ShopChannel::getAllShopChannelLinkedByChannelId($channelTiki->channel_id);

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }

        foreach ($allShopChannel as $shopChannel) {
            $credential = json_decode($shopChannel->shop_channel_api_credential, true);
            if (!isset($credential['username']) || !isset($credential['password'])) {
                LogError::getInstance()->slack('Cron Product - Some thing error - Shop not have credential - Shop channel: ' . $shopChannel->shop_channel_id, config('slack.tiki'));
                continue;
            }

            $tiki = new Library\Crawler\Channel\Tiki($shopChannel->shop_channel_id);
            $loginSuccess = $tiki->login();
            if (!$loginSuccess->getIsSuccess()) {
                LogError::getInstance()->slack('Cron Product - Some thing error - Login failure - Shop channel: ' . $shopChannel->shop_channel_id, config('slack.tiki'));
                continue;
            }

            $job = new CrawlProductInfo($shopChannel->shop_channel_id, $loginSuccess->getData(), $shopChannel->venture_timezone);
            dispatch($job->onQueue(config('queue.tiki.crawl_data')));
        }
    }
}