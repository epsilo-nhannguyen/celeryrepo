<?php
namespace App\Console\Commands\CommandsTikiAd;

use App\Jobs\Tiki\CrawlCampaign;
use App\Library\LogError;
use Illuminate\Console\Command;
use App\Models;
use App\Library;

class KeywordAds extends Command
{
    /**
     * @uses: php artisan tiki-ad:keyword-ads --dateFrom=date-from --dateTo=date-to  --arrayShopId=shop-master-id
     * example: php artisan tiki-ad:keyword-ads --dateFrom=2019-09-01 --dateTo=2019-09-30  --arrayShopId=532,321,
     */

    protected $signature = 'tiki-ad:keyword-ads {--dateFrom=} {--dateTo=} {--arrayShopId=}';

    protected $description = 'Crawl and import all data info and performance Tiki Ads';

    public function handle()
    {
        Library\Common::setTimezone('Asia/Saigon');

        $arrayShopId = $this->option('arrayShopId');
        $dateFrom = $this->option('dateFrom') ?? date('Y-m-d');
        $dateTo = $this->option('dateTo') ?? date('Y-m-d');
        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $channelTiki = Models\Channel::getChannelTiKi();

        $allShopChannel = Models\ModelBusiness\ShopChannel::getAllShopChannelLinkedByChannelId($channelTiki->channel_id);

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }

        foreach ($allShopChannel as $shopChannel) {
            $credential = json_decode($shopChannel->shop_channel_api_credential, true);
            if (!isset($credential['username']) || !isset($credential['password'])) {
                LogError::getInstance()->slack('Cron Keyword Ads - Some thing error - Shop not have credential - Shop channel: ' . $shopChannel->shop_channel_id, config('slack.tiki'));
                continue;
            }

            $tiki = new Library\Crawler\Channel\Tiki($shopChannel->shop_channel_id);
            $loginSuccess = $tiki->login();
            if (!$loginSuccess->getIsSuccess()) {
                LogError::getInstance()->slack('Cron Keyword Ads - Some thing error - Login failure - Shop channel: ' . $shopChannel->shop_channel_id, config('slack.tiki'));
                continue;
            }

            $job = new CrawlCampaign($shopChannel->shop_channel_id, $loginSuccess->getData(), $shopChannel->venture_timezone, $dateFrom, $dateTo);
            dispatch($job->onQueue(config('queue.tiki.crawl_data')));
        }
    }
}