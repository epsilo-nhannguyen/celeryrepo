<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:42
 */

namespace App\Console\Commands\CommandsTikiAd;

use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;
use App\Library;
use App\Models;


class RawPerformance extends Command
{

    /**
     * @uses: php artisan tiki-ad:raw-performance --dateFrom=date-from --dateTo=date-to --token=token --account=account --shopChannelName=shopChannelName --isShowMessage=is-show-message
     * example: php artisan tiki-ad:raw-performance --dateFrom=2019-09-01 --dateTo=2019-09-30 --token=token --account=account --shopChannelName=shopChannelName --isShowMessage=0
     */

    protected $signature = 'tiki-ad:raw-performance {--dateFrom=} {--dateTo=} {--token=} {--account=} {--shopChannelName=} {--isShowMessage=0}';

    protected $description = 'Raw performance Tiki Ad';

    public function handle()
    {
        Library\Common::setTimezone('Asia/Ho_Chi_Minh');

        $yesterday = Library\Common::convertTimestampToDate(strtotime('yesterday'));

        $dateFrom = $this->option('dateFrom') ?? $yesterday;
        $dateTo = $this->option('dateTo') ?? $yesterday;
        $token = $this->option('token') ?? env('TIKI_AD_TOKEN', 'z2s2w254d4u4x28454o2t2s2v2z213s266z2n5s2544454k5s2s254y2j5k5');
        $account = $this->option('account') ?? env('TIKI_AD_ACCOUNT', '1600041663');
        $shopChannelName = $this->option('shopChannelName') ?? 'Biore';
        $isShowMessage = $this->option('isShowMessage');

        Library\Common::setIsShowMessage($isShowMessage);

        Library\Common::showMessage('Start '.Library\Common::getCurrentDatetime().PHP_EOL);

        $timestamp = Library\Common::getCurrentTimestamp();
        $fileNameDaily = 'TikiAdPerformance_Date_'.$yesterday.'_'.$timestamp.'.csv';

        $weekNumber = intval(date('W')) - 1;
        $fileNameWeekly = 'TikiAdPerformance_Week_'.date('Y').'-w'.$weekNumber.'_'.$timestamp.'.csv';

        $ventureId = Models\Venture::VIETNAM;

        $userId = $account;
        $accountId = $account;
        $cookiePath = '';

        $crawlerTiki = new Library\Crawler\Channel\Tiki();
        $crawlerTiki->setCredential($cookiePath, $token, $userId, $accountId, $ventureId);

        $this->_insertHeader($fileNameDaily, 'daily');
        $this->_buildData($crawlerTiki, $dateFrom, $dateTo, $fileNameDaily, 'daily', $shopChannelName);
        $pathAttachmentFileArray = [$fileNameDaily];

        $dayText = date('D'); # Mon through Sun
        if ($dayText == 'Mon') {
            $dateFromWeek = Library\Common::convertTimestampToDate(strtotime("{$yesterday} -6 days"));;
            $dateToWeek = $yesterday;

            $this->_insertHeader($fileNameWeekly, 'weekly');
            $this->_buildData($crawlerTiki, $dateFromWeek, $dateToWeek, $fileNameWeekly, 'weekly', $shopChannelName);
            $pathAttachmentFileArray[] = $fileNameWeekly;
        }

        $toEmail = 'hieu.dang@epsilo.io';
        $ccEmailArray = [
            'vu.tran@epsilo.io',
            'truc.nguyen@epsilo.io',
            'vy.phan@epsilo.io',
            'nhan.nguyen@epsilo.io',
        ];

        $email = $toEmail;
        $fullName = 'Hieu Dang';
        $subject = 'Report Tiki Ad Performance';
        $pathTemplate = 'template-email.report-performance'; #ex: email.user
        $assocKeyData = ['date' => $yesterday];

        Mail::to($email, $fullName)->cc($ccEmailArray)->send(new Library\Email($subject, $pathTemplate, $assocKeyData, $pathAttachmentFileArray));

        if (file_exists($fileNameDaily)) {
            unlink($fileNameDaily);
        }

        if (file_exists($fileNameWeekly)) {
            unlink($fileNameWeekly);
        }

        Library\Common::showMessage('End '.Library\Common::getCurrentDatetime().PHP_EOL);
    }

    /**
     * _build Data
     * @param Library\Crawler\Channel\Tiki $crawlerTiki
     * @param string $dateFrom
     * @param string $dateTo
     * @param string $fileName
     * @param string $type
     * @param string $shopChannelName
     */
    private function _buildData(Library\Crawler\Channel\Tiki $crawlerTiki, $dateFrom, $dateTo, $fileName, $type, $shopChannelName)
    {
        $campaignData = $crawlerTiki->rawDataCampaign($dateFrom, $dateTo);
        $assocLineitem_idName = array_column(array_column($campaignData,'lineitem_name'), 'title', 'lineitemId');

        $assocLineitemId_campaignIdData = [];
        foreach ($assocLineitem_idName as $lineitemId => $lineitemName) {
            $adGroupData = $crawlerTiki->rawDataAdGroup($dateFrom, $dateTo, $lineitemId);

            if ( ! isset($assocLineitemId_campaignIdData[$lineitemId])) {
                $campaignIdArray = array_column($adGroupData, 'campaign_id');

                foreach ($campaignIdArray as $campaignId) {
                    $productData = $crawlerTiki->rawDataProduct($dateFrom, $dateTo, $campaignId);
                    $keywordData = $crawlerTiki->rawDataKeyword($dateFrom, $dateTo, $campaignId);

                    $assocLineitemId_campaignIdData[$lineitemId][$campaignId] = [
                        'totalProduct' => count($productData),
                        'totalKeyword' => count($keywordData),
                    ];
                }
            }

            foreach ($adGroupData as $adGroup) {
                $campaignId = $adGroup['campaign_id'];
                $adGroupName = $adGroup['campaign_name']['title'];
                $totalProduct = $assocLineitemId_campaignIdData[$lineitemId][$campaignId]['totalProduct'];
                $totalKeyword = $assocLineitemId_campaignIdData[$lineitemId][$campaignId]['totalKeyword'];
                $status = $adGroup['status'];
                $targetingType = $adGroup['campaign_type'];
                $defaultMaxCPC = $adGroup['cpc_bid_price'];
                $impression = $adGroup['impression'];
                $addToCart = $adGroup['ot_add_cart_ins'];
                $purchaseRevenue = $adGroup['ot_pur_revenue_ins'];
                $cost = $adGroup['proceeds'];
                $acos = $adGroup['acos'];
                $click = $adGroup['click'];
                $avgCPC = $adGroup['ecpc'];
                $quantity = $adGroup['ot_item_sold_ins'];

                $recordArray = [
                    date('m', strtotime($dateFrom)),
                    $type == 'daily' ? $dateFrom : '2020-w'.date('W', strtotime($dateFrom)),
                    $shopChannelName,
                    $lineitemName,
                    $adGroupName,
                    $totalProduct,
                    $totalKeyword,
                    $status,
                    $targetingType,
                    $defaultMaxCPC,
                    $impression,
                    $addToCart,
                    $purchaseRevenue,
                    $cost,
                    $acos,
                    $click,
                    $avgCPC,
                    $quantity,
                ];

                $string = implode(';', $recordArray).PHP_EOL;
                Library\Common::showMessage($string);
                file_put_contents($fileName, $string, FILE_APPEND);
            }
        }
    }

    /**
     * _insert Header
     * @param string $fileName
     * @param string $type
     */
    private function _insertHeader($fileName, $type)
    {
        $headerArray = [
            'Month',
            $type == 'daily' ? 'Date' : 'Week',
            'Shop name',
            'Campaign name',
            'Ad group name',
            '# of SKU',
            '# of KW',
            'Status',
            'Targeting type',
            'Default Max CPC',
            'Impression',
            'Adds to Cart',
            'Purchase Revenue',
            'Cost',
            'ACoS',
            'Clicks',
            'Avg. CPC',
            'Quantity'
        ];

        $string = implode(';', $headerArray).PHP_EOL;
        Library\Common::showMessage($string);
        file_put_contents($fileName, $string, FILE_APPEND);
    }

}