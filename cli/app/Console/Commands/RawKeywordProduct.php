<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 28/09/2019
 * Time: 18:10
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use App\Jobs;
use Illuminate\Console\Command;
use Exception;


class RawKeywordProduct extends Command
{
    /**
     * @uses php artisan raw-keyword-product --shop=shop-id --channel=channel-id
     * example: php artisan raw-keyword-product --shop=62 --channel=3
     */
    protected $signature = 'raw-keyword-product {--shop=} {--channel=}';

    protected $description = 'Raw keyword product from Shopee to mongo database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];

        Library\Common::setTimezoneByVentureId($ventureId);

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];
        $message = json_encode([
            'shopId' => $shopId,
            'channelId' => $channelId,
            'shopChannelId' => $shopChannelId,
            'ventureId' => $ventureId,
        ]);

        dispatch((new Jobs\RawKeywordProduct($message))->onQueue(Library\QueueName::getByClass(Jobs\RawKeywordProduct::class)));

    }

}