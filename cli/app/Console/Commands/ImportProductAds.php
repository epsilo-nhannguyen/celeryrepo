<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 09:19
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use Illuminate\Console\Command;
use Exception;


class ImportProductAds extends Command
{
    /**
     * @uses php artisan import-product-ads --shop=shop-id --channel=channel-id
     * example: php artisan import-product-ads --shop=62 --channel=3
     */
    protected $signature = 'import-product-ads {--shop=} {--channel=}';

    protected $description = 'Import product ads from mongo to mysql database';

    /**
     * @var array
     */
    protected $_assocProductIdentifyProductShopChannelId = [];

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $keywordProductMongo = Models\Mongo\KeywordProduct::searchForImport($shopId, $channelId);
        if ( ! $keywordProductMongo) return;

        $shopInfo = Library\Formater::stdClassToArray(Models\ShopMaster::getById($shopId));
        $organizationId = $shopInfo[Models\ShopMaster::COL_FK_ORGANIZATION];
        $ventureId = $shopInfo[Models\ShopMaster::COL_FK_VENTURE];
        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];

        Library\Common::setTimezoneByVentureId($ventureId);

        $this->_assocProductIdentifyProductShopChannelId = array_column(
            Library\Formater::stdClassToArray(Models\ProductShopChannel::searchByShopChannelId($shopChannelId)),
            Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
            Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID
        );

        $allKeyword = $assocIdentifyProductShopChannelId = $arrayProductShopChannelIdInCampaign = [];
        foreach ($keywordProductMongo as $keywordProduct) {
            $itemData = $keywordProduct['data'];
            $itemIdentify = $keywordProduct['productIdentify'];
            $allKeyword = array_merge($allKeyword, array_column($itemData['keywords'], 'keyword'));

            $productShopChannelId = $this->_assocProductIdentifyProductShopChannelId[$itemIdentify] ?? null;
            if ( ! $productShopChannelId) continue;

            $productAdsDb = Library\Formater::stdClassToArray(Models\ProductAds::getByProductShopChannelIdAndAdsId($productShopChannelId, $itemData['adsId'], $shopChannelId));
            $isInsert = ! boolval($productAdsDb);

            $timeStart = $itemData['startTime'] ? Library\Common::convertDatetimeToTimestamp($itemData['startTime']) : null;
            $timeEnd = $itemData['endTime'] ? Library\Common::convertDatetimeToTimestamp($itemData['endTime']) : null;
            $quotaTotal = floatval($itemData['totalQuota']) ? floatval($itemData['totalQuota']) : null;
            $quotaDaily = floatval($itemData['dailyQuota']) ? floatval($itemData['dailyQuota']) : null;

            $productAdsRecord = [
                Models\ProductAds::COL_PRODUCT_ADS_TIME_START => $timeStart,
                Models\ProductAds::COL_PRODUCT_ADS_TIME_END => $timeEnd,
                Models\ProductAds::COL_PRODUCT_ADS_QUOTA_TOTAL => $quotaTotal,
                Models\ProductAds::COL_PRODUCT_ADS_QUOTA_DAILY => $quotaDaily,
                Models\ProductAds::COL_PRODUCT_ADS_STATE => $itemData['state'],
                Models\ProductAds::COL_PRODUCT_ADS_STATE_PAST => $itemData['state'],
                Models\ProductAds::COL_PRODUCT_ADS_STATE_UPDATED_BY => Models\Admin::EPSILO_SYSTEM,
                Models\ProductAds::COL_PRODUCT_ADS_STATE_UPDATED_AT => Library\Common::getCurrentTimestamp(),
                Models\ProductAds::COL_PRODUCT_ADS_MODIFY_AT => Library\Common::getCurrentTimestamp()
            ];

            if ($isInsert) {
                $productAdsRecord[Models\ProductAds::COL_PRODUCT_ADS_CREATED_BY] = Models\Admin::EPSILO_SYSTEM;
                $productAdsRecord[Models\ProductAds::COL_PRODUCT_ADS_CREATED_AT] = Library\Common::getCurrentTimestamp();
            }

            $productAdsFilter = [
                Models\ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL => $productShopChannelId,
                Models\ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID => $itemData['adsId'],
                Models\ProductAds::COL_FK_SHOP_CHANNEL => $shopChannelId
            ];

            Models\ProductAds::import($productAdsFilter, $productAdsRecord);

            $arrayProductShopChannelIdInCampaign[] = $productShopChannelId;
            $assocIdentifyProductShopChannelId[$itemIdentify] = $productShopChannelId;
        }

        $campaignInfo = Library\Formater::stdClassToArray(Models\MakProgrammaticCampaign::getByShopChannelId($shopChannelId));
        if ( ! $campaignInfo) {

            /*$campaignBudget = $campaignFrom = $campaignTo = null;
            $campaignBudget = $campaignBudget !== null ? floatval($campaignBudget) : null;
            $campaignFrom = $campaignFrom ? Library\Formater::date($campaignFrom) : null;
            $campaignTo = $campaignTo ? Library\Formater::date($campaignTo) : null;*/

            $campaignDefaultId = Models\MakProgrammaticCampaign::insertGetId(
                $organizationId, $shopChannelId, Models\MakProgrammaticCampaign::CAMPAIGN_NAME_DEFAULT, null, null, null, Models\Admin::EPSILO_SYSTEM
            );
            sleep(1);

            $arrayProductShopChannelIdNeedInsert = $arrayProductShopChannelIdInCampaign;
        } else {
            $campaignDefaultId = $campaignInfo[Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID];
            $productShopChannelIdArrayInDb = array_column(
                Library\Formater::stdClassToArray(Models\ProductShopChannelCampaign::searchByCampaignId($campaignDefaultId)),
                Models\ProductShopChannelCampaign::COL_FK_PRODUCT_SHOP_CHANNEL
            );

            $arrayProductShopChannelIdNeedInsert = array_diff($arrayProductShopChannelIdInCampaign, $productShopChannelIdArrayInDb);
        }

        $productShopChannelCampaignArray = [];
        foreach ($arrayProductShopChannelIdNeedInsert as $productShopChannelId) {
            $productShopChannelCampaignArray[] = Models\ProductShopChannelCampaign::buildDataInsert(
                $productShopChannelId, $campaignDefaultId, Models\Admin::EPSILO_SYSTEM
            );
        }

        if ($productShopChannelCampaignArray) {
            Models\ProductShopChannelCampaign::batchInsert($productShopChannelCampaignArray);
        }

        $allKeywordDb = array_column(
            Library\Formater::stdClassToArray(Models\MakProgrammaticKeyword::searchByOrganizationIdAndVentureId($organizationId, $ventureId)),
            Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME
        );

        $keywordRecord = [
            Models\MakProgrammaticKeyword::COL_FK_ORGANIZATION => $organizationId,
            Models\MakProgrammaticKeyword::COL_FK_VENTURE => $ventureId,
            Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_CREATED_BY => Models\Admin::EPSILO_SYSTEM,
            Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_CREATED_AT => Library\Common::getCurrentTimestamp(),
        ];

        $allKeywordInsert = array_diff($allKeyword, $allKeywordDb);
        $allKeywordInsert = array_unique($allKeywordInsert);
        $arrayChunkKeyword = array_chunk($allKeywordInsert, 200);
        foreach ($arrayChunkKeyword as $chunkKeyword) {
            $keywordRecordArray = [];
            foreach ($chunkKeyword as $keyword) {
                $keywordRecord[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME] = $keyword;

                $keywordRecordArray[] = $keywordRecord;
            }

            Models\MakProgrammaticKeyword::batchInsert($keywordRecordArray);
        }
        sleep(1);

        $dataKeyword = Library\Formater::stdClassToArray(Models\MakProgrammaticKeyword::searchByOrganizationIdAndVentureId($organizationId, $ventureId));
        if ( ! $dataKeyword) {
            throw new Exception('Keyword empty');
        }

        $assocNameId = [];
        foreach ($dataKeyword as $keyword) {
            $_id = $keyword[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID];
            $_name = Library\Common::strToHex($keyword[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME]);

            $assocNameId[$_name] = $_id;
        }

        $arrayIdentifySuccess = [];
        foreach ($keywordProductMongo as $keywordProduct) {
            $itemIdentify = $keywordProduct['productIdentify'];
            $itemData = $keywordProduct['data'];
            $arrayKeywords = $itemData['keywords'];

            $productShopChannelId = $assocIdentifyProductShopChannelId[$itemIdentify] ?? null;
            if ( ! $productShopChannelId) continue;

            foreach ($arrayKeywords as $keyword) {
                $keywordName = $keyword['keyword'];
                $matchType = $keyword['matchType'];
                $price = $keyword['price'];
                $status = $keyword['status'];
                $keywordId = $assocNameId[Library\Common::strToHex($keywordName)];

                $keywordProductDb = Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::getByKeywordIdAndProductId($keywordId, $productShopChannelId));

                $keywordProductRecord = [
                    Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_BIDDING_PRICE => $price,
                    Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_MATCH_TYPE => $matchType,
                    Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS => $status
                ];
                if ($keywordProductDb) {
                    $keywordProductRecord[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_UPDATED_BY] = Models\Admin::EPSILO_SYSTEM;
                    $keywordProductRecord[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_UPDATED_AT] = Library\Common::getCurrentTimestamp();
                } else {
                    $keywordProductRecord[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_BY] = Models\Admin::EPSILO_SYSTEM;
                    $keywordProductRecord[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_AT] = Library\Common::getCurrentTimestamp();
                }

                $keywordProductFilter = [
                    Models\MakProgrammaticKeywordProduct::COL_FK_MAK_PROGRAMMATIC_KEYWORD => $keywordId,
                    Models\MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL => $productShopChannelId,
                ];

                Models\MakProgrammaticKeywordProduct::import($keywordProductFilter, $keywordProductRecord);

                $arrayIdentifySuccess[] = $itemIdentify;
            }
        }

        if ($arrayIdentifySuccess) {
            $arrayIdentifySuccess = array_values(array_unique($arrayIdentifySuccess));
            Models\Mongo\KeywordProduct::updateImported($shopId, $channelId, $arrayIdentifySuccess);
        }

        Models\Mongo\MakSetup::updateSetupDone($shopId, $channelId, Models\Mongo\MakSetup::SETUP_PULL_KEYWORD);

    }
}