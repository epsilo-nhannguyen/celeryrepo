<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 15:09
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use App\Jobs;
use Illuminate\Console\Command;
use Exception;


class RawCrawlerAds extends Command
{
    /**
     * @uses:
     * php artisan raw-crawler-ads --shop=shop-id --channel=channel-id --dateFrom=date-from --dateTo=date-to --paddingDate=padding-date --onlyPaidAds=only-paid-ads
     * example: php artisan raw-crawler-ads --shop=62 --channel=3 --dateFrom=2019-09-01 --dateTo=2019-09-30 --paddingDate=0 --onlyPaidAds=0
     */

    protected $signature = 'raw-crawler-ads {--shop=} {--channel=} {--dateFrom=} {--dateTo=} {--paddingDate=0} {--onlyPaidAds=0}';

    protected $description = 'Raw paid ads data from Shopee to mongo database';

    public function handle()
    {
        $today = date('Y-m-d');

        $shopId = $this->option('shop');
        $channelId = $this->option('channel');
        $dateFrom = $this->option('dateFrom') ?? $today;
        $dateTo = $this->option('dateTo') ?? $today;
        $paddingDate = $this->option('paddingDate');
        $onlyPaidAds = $this->option('onlyPaidAds');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        if ($channelInfo[Models\Channel::COL_CHANNEL_CODE] != Models\Channel::SHOPEE_CODE) return;

        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];
        Library\Common::setTimezoneByVentureId($ventureId);

        if ($paddingDate) {
            $dateTo = date('Y-m-d', strtotime("{$dateTo} -{$paddingDate} days"));
        }

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];

        $stateArray = [
            Models\ProductAds::STATE_PAUSED, Models\ProductAds::STATE_ONGOING, Models\ProductAds::STATE_SCHEDULED, Models\ProductAds::STATE_CLOSED, Models\ProductAds::STATE_ENDED
        ];
        $productAdsData = Library\Formater::stdClassToArray(Models\ProductAds::searchByShopChannelId($shopChannelId, $stateArray));

        $productAdsIdArray = array_column($productAdsData, Models\ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID);
        $productAdsIdChunk = array_chunk($productAdsIdArray, 10);
        foreach ($productAdsIdChunk as $chunk) {
            $message = json_encode([
                'shopId' => $shopId,
                'channelId' => $channelId,
                'arrayAds' => $chunk,
                'ventureId' => $ventureId,
                'shopChannelId' => $shopChannelId,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
            ]);

            dispatch((new Jobs\RawCrawlerAds($message))->onQueue(Library\QueueName::getByClass(Jobs\RawCrawlerAds::class)));
        }

        if ($onlyPaidAds) return;

        $productShopChannelIdArray = array_column(
            array_filter($productAdsData, function ($item) {
                return $item[Models\ProductAds::COL_PRODUCT_ADS_STATE] == Models\ProductAds::STATE_ONGOING;
            }),
            Models\ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL
        );

        $assocKeywordItemId = [];
        $keywordProductArray = Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByProductIdArray($productShopChannelIdArray));
        foreach ($keywordProductArray as $keywordProduct) {
            $keywordName = $keywordProduct[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME];

            if ( ! isset($assocKeywordItemId[$keywordName])) {
                $assocKeywordItemId[$keywordName] = [];
            }
            $assocKeywordItemId[$keywordName][] = $keywordProduct[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID];
        }

        $credential = json_decode($shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_API_CREDENTIAL], true);
        $shopeeShopId = $credential['shop_id'] ?? 0;

        $keywordItemIdChunk = array_chunk($assocKeywordItemId, 50, true);
        foreach ($keywordItemIdChunk as $chunk) {
            $message = json_encode([
                'shopId' => $shopId,
                'channelId' => $channelId,
                'shopeeShopId' => $shopeeShopId,
                'dataKeyword' => $chunk,
                'totalPage' => env('RANKING_KEYWORD_TOTAL_PAGE'),
            ]);

            dispatch((new Jobs\RawRankingKeyword($message))->onQueue(Library\QueueName::getByClass(Jobs\RawRankingKeyword::class)));
        }

    }

}