<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 12:09
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use Illuminate\Console\Command;
use Exception;


class ImportProduct extends Command
{
    /**
     * @uses php artisan import-product --shop=shop-id --channel=channel-id
     * example: php artisan import-product --shop=62 --channel=3
     */
    protected $signature = 'import-product {--shop=} {--channel=}';

    protected $description = 'Import product from mongo to mysql database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        $channelCode = $channelInfo[Models\Channel::COL_CHANNEL_CODE];
        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];

        $shopInfo = Library\Formater::stdClassToArray(Models\ShopMaster::getById($shopId));
        $organizationId = $shopInfo[Models\ShopMaster::COL_FK_ORGANIZATION];

        Library\Common::setTimezoneByVentureId($ventureId);

        $arrayIdentify = $assocIdentifyId = [];
        $productMongo = Models\Mongo\Product::searchByShopIdChannelId($shopId, $channelId);
        foreach ($productMongo as $product) {
            $productData = $product['data'];
            $channelCategory = $productData['channelCategory'];

            if ( ! isset($assocIdentifyId[$channelCategory])) {
                $channelCategoryInfo = Models\ChannelCategory::getByChannelIdAndIdentify($organizationId, $channelId, $channelCategory);
                if ($channelCategoryInfo) {
                    $assocIdentifyId[$channelCategory] = $channelCategoryInfo[Models\ChannelCategory::COL_CHANNEL_CATEGORY_ID];
                }
            }

            $channelCategoryId = $assocIdentifyId[$channelCategory] ?? null;
            $channelIdentify = trim($productData['channelIdentify']);

            $rawDataToProcess = json_decode($productData['rawData'], true);
            $shopSku = $rawDataToProcess['skus'][0]['ShopSku'] ?? null;

            Models\ProductShopChannel::import(
                $channelIdentify, $shopChannelId, $productData['name'], $productData['allocationStock'], $productData['fbxStock'],
                $productData['rrp'], $productData['sellingPrice'], $channelIdentify, $channelCategoryId, $productData['updatedAt'],
                Models\Admin::EPSILO_SYSTEM, $productData['createdAt'], Models\Admin::EPSILO_SYSTEM, $productData['rawData'], $shopSku
            );

            if ($channelCode == Models\Channel::SHOPEE_CODE) {
                sleep(1);

                $productShopChannelInfo = Library\Formater::stdClassToArray(Models\ProductShopChannel::getByShopChannelIdAndSellerSku($shopChannelId, $channelIdentify));

                Models\ProductShopee::import($productShopChannelInfo[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID], $rawDataToProcess['item']['status']);
            }

            $arrayIdentify[] = $channelIdentify;
        }
        if ($arrayIdentify) {
            Models\Mongo\Product::updateImported($shopId, $channelId, $arrayIdentify);
        }

        Models\Mongo\MakSetup::updateSetupDone($shopId, $channelId, Models\Mongo\MakSetup::SETUP_PULL_PRODUCT);

    }
}