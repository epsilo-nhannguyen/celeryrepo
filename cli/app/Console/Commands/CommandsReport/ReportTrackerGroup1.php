<?php


namespace App\Console\Commands\CommandsReport;


use App\Services\ShopeeKeywordWalletBalance;
use Illuminate\Console\Command;
use App\Library;
use App\Models;
use App\Repository\RepositoryStat\MetricsTrackerReport;


class ReportTrackerGroup1 extends Command
{
    /**
     * @uses: php artisan report-tracker-group1 --dateFrom=date-from --dateTo=date-to --type=type --isShowMessage=is-show-message
     * example: php artisan report-tracker-group1 --dateFrom=2019-09-01 --dateTo=2019-09-30 --type=daily --isShowMessage=0
     */

    protected $signature = 'report-tracker-group1 {--dateFrom=} {--dateTo=} {--type=} {--isShowMessage=0}';

    protected $description = 'Report Tracker Group1 from Marketing to Stat';

    /**
     * @var array
     */
    private $assocDatePerformance = [];
    private $assocDateHourBalance = [];
    private $assocDailyTotalPerformance = [];
    private $assocWeeklyTotalPerformance = [];
    private $assocMonthlyTotalPerformance = [];

    /**
     * @var int
     */
    private $numberCrawOneDay = 24;

    /**
     * @var string
     */
    private $dateFromCommon = '';
    private $dateToCommon = '';

    public function handle()
    {
        ini_set('memory_limit','2048M');

        $isShowMessage = $this->option('isShowMessage');
        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');
        $type = $this->option('type');

        Library\Common::setIsShowMessage($isShowMessage);

        Library\Common::showMessage('Start '.Library\Common::getCurrentDatetime().PHP_EOL);

        list($assocDailyDate, $assocWeeklyDate, $assocMonthlyDate) = $this->_buildAssocDate($dateFrom, $dateTo);

        if ($type == 'daily') {
            $assocWeeklyDate = $assocMonthlyDate = [];
        } elseif ($type == 'weekly') {
            $assocDailyDate = $assocMonthlyDate = [];
        } elseif ($type == 'monthly') {
            $assocDailyDate = $assocWeeklyDate = [];
        }

        $dateArray = Library\Common::dayOfBetween($this->dateFromCommon, $this->dateToCommon);

        $channelCode = Models\Channel::SHOPEE_CODE;
        $shopChannelData = Models\ShopChannel::searchShopData(false, false, $channelCode);
        foreach ($shopChannelData as $shopChannel) {
            Library\Common::setTimezone($shopChannel->venture_timezone);

            $ventureId = $shopChannel->venture_id;
            $shopChannelId = $shopChannel->shop_channel_id;
            $shopId = $shopChannel->shop_master_id;
            $channelId = $shopChannel->channel_id;
            $shopChannelName = $shopChannel->shop_channel_name;
            $shopChannelIsActive = $shopChannel->shop_channel_is_active;
            $shopChannelCreatedAt = $shopChannel->shop_channel_created_at;
            $shopChannelDateCreated = Library\Common::convertTimestampToDate($shopChannelCreatedAt);
            $ventureName = $shopChannel->venture_name;
            $currencyCode = $shopChannel->venture_exchange;

            $this->assocDatePerformance = [];
            $this->assocDateHourBalance = [];

            $serviceWalletBalance = new ShopeeKeywordWalletBalance($shopChannelId);
            foreach ($dateArray as $date) {
                $this->assocDateHourBalance[$date] = $serviceWalletBalance->getListByDate($date)->toArray();
            }

            $performanceData = Models\MakPerformance::searchByShopChannelId(
                $shopChannelId,
                Library\Common::convertTimestampToDate(strtotime("{$this->dateFromCommon} -7 days")),
                $this->dateToCommon
            );
            foreach ($performanceData as $performanceItem) {
                $performanceDate = $performanceItem->mak_performance_date;
                $performanceItem->cost_usd = Library\CurrencyStat::convertCurrency($performanceItem->mak_performance_expense, $performanceDate, $currencyCode);
                $performanceItem->gmv_usd = Library\CurrencyStat::convertCurrency($performanceItem->mak_performance_gmv, $performanceDate, $currencyCode);
                $this->assocDatePerformance[$performanceDate][] = $performanceItem;
            }

            $this->_calculatorPerformance($assocDailyDate, 'daily', $shopId);
            $this->_calculatorPerformance($assocWeeklyDate, 'weekly', $shopId);
            $this->_calculatorPerformance($assocMonthlyDate, 'monthly', $shopId);

            $message = PHP_EOL.'Done save Group 1 Shop channel id: '.$shopChannelId.' - Shop channel name: '.$shopChannelName.' - Country: '.$ventureName;
            Library\Common::showMessage($message);
        }

        Library\Common::showMessage('End '.Library\Common::getCurrentDatetime().PHP_EOL);

    }

    /**
     * build Assoc Date
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    private function _buildAssocDate($dateFrom, $dateTo)
    {
        $assocDailyDate = $assocWeeklyDate = $assocMonthlyDate = $yearMonthArray = [];

        if ( ! $dateFrom || ! $dateTo) {
            $today = date('Y-m-d');
            $yesterday = Library\Common::convertTimestampToDate(strtotime("{$today} -1 days"));
            $assocDailyDate[$yesterday] = [$yesterday];

            $this->dateFromCommon = $yesterday;
            $this->dateToCommon = $yesterday;

            $timestampYesterday = strtotime($yesterday);
            $year = date('Y', $timestampYesterday);
            $month = date('m', $timestampYesterday);

            $dayNumber = date('d'); # 01 to 31
            $dayText = date('D'); # Mon through Sun
            if ($dayText == 'Mon') {
                $dateInWeekArray = [$yesterday];
                for ($i = 1; $i < 7; $i ++) {
                    array_unshift($dateInWeekArray, date('Y-m-d', strtotime($yesterday.'- '.$i.' days')));
                }

                foreach ($dateInWeekArray as $dateInWeek) {
                    $timestampDateWeek = strtotime($dateInWeek);

                    $_weekly = date('W', $timestampDateWeek);
                    $_month = date('m', $timestampDateWeek);
                    $_year = date('Y', $timestampDateWeek);

                    if ($_weekly == '01' && $_month == '12') {
                        $_weekly = '53';
                    }
                    $yearWeek = $_year.'-w'.$_weekly;
                    $assocWeeklyDate[$yearWeek][] = $dateInWeek;
                }
                $this->dateFromCommon = current($dateInWeekArray);
            }

            if ($dayNumber == '01') {
                $assocMonthlyDate[$year.'-'.$month] = Library\Common::dayOfBetween(
                    date('Y-m-01', $timestampYesterday), date('Y-m-t', $timestampYesterday)
                );

                $this->dateFromCommon = date('Y-m-01', $timestampYesterday);
                $this->dateToCommon = date('Y-m-t', $timestampYesterday);
            }
        } else {
            $dateArray = Library\Common::dayOfBetween($dateFrom, $dateTo);
            $thisDay = date('Y-m-d');
            $thisYearMonth = date('Y-m');

            $thisYear = date('Y');
            $thisMonth = date('m');
            $thisWeekly = date('W');

            if ($thisWeekly == '01' && $thisMonth == '12') {
                $thisWeekly = '53';
            }

            $thisYearWeek = $thisYear.'-w'.$thisWeekly;

            foreach ($dateArray as $date) {
                $timestampDate = strtotime($date);

                $assocDailyDate[$date][] = $date;

                $year = date('Y', $timestampDate);
                $month = date('m', $timestampDate);
                $weekly = date('W', $timestampDate);

                if ($weekly == '01' && $month == '12') {
                    $weekly = '53';
                }

                $yearWeek = $year.'-w'.$weekly;
                $assocWeeklyDate[$yearWeek][] = $date;

                $yearMonth = date('Y-m', $timestampDate);

                if ( ! in_array($yearMonth, $yearMonthArray)) {
                    $yearMonthArray[] = $yearMonth;
                    $assocMonthlyDate[$yearMonth] = Library\Common::dayOfBetween(date('Y-m-01', $timestampDate), date('Y-m-t', $timestampDate));
                }
            }
            $this->dateFromCommon = $dateFrom;
            $this->dateToCommon = $dateTo;
        }

        return [$assocDailyDate, $assocWeeklyDate, $assocMonthlyDate];
    }

    /**
     * _calculator Performance
     * @param array $assocDate
     * @param string $typeReport
     * @param int $shopId
     */
    private function _calculatorPerformance($assocDate, $typeReport, $shopId)
    {
        foreach ($assocDate as $keyText => $dateArray) {
            $keywordProductIdArray = $productIdArray = [];
            $totalView = $totalAvgWeighted = 0;
            $assocKeywordProductId_haveCostNoSale = [];
            $keywordProductId_haveSaleArray = $productId_haveSaleArray = $keywordId_haveSaleArray = [];
            $atr = $gmvLossFromOutOfBudget = 0;

            foreach ($dateArray as $date) {
                foreach ($this->assocDatePerformance[$date] ?? [] as $performance) {
                    $keywordProductId = $performance->mak_programmatic_keyword_product_id;
                    $productId = $performance->product_shop_channel_id;
                    $keywordId = $performance->fk_mak_programmatic_keyword;
                    $cost = $performance->mak_performance_expense;
                    $costUsd = $performance->cost_usd;
                    $gmv = $performance->mak_performance_gmv;
                    $gmvUsd = $performance->gmv_usd;
                    $itemSold = $performance->mak_performance_product_sold;
                    $view = $performance->mak_performance_view;
                    $avgWeighted = $performance->avg_weighted;

                    if ( ! in_array($keywordProductId, $keywordProductIdArray)) {
                        $keywordProductIdArray[] = $keywordProductId;
                    }
                    if ( ! in_array($productId, $productIdArray)) {
                        $productIdArray[] = $productId;
                    }
                    $totalView += $view;
                    $totalAvgWeighted += $avgWeighted;

                    if ($costUsd > 0 && $itemSold == 0) {
                        $assocKeywordProductId_haveCostNoSale[$keywordProductId] = $costUsd;
                    }

                    if ($itemSold > 0) {
                        if ( ! in_array($keywordProductId, $keywordProductId_haveSaleArray)) {
                            $keywordProductId_haveSaleArray[] = $keywordProductId;
                        }
                        if ( ! in_array($productId, $productId_haveSaleArray)) {
                            $productId_haveSaleArray[] = $productId;
                        }
                        if ( ! in_array($keywordId, $keywordId_haveSaleArray)) {
                            $keywordId_haveSaleArray[] = $keywordId;
                        }
                    }
                }

                if ($typeReport == 'daily') {
                    $numberOutOfBudget = count(array_filter($this->assocDateHourBalance[$date] ?? [], function ($item) {
                        return $item == 0;
                    }));

                    $percentOutOfBudget = $numberOutOfBudget / $this->numberCrawOneDay;
                    $atr = (1 - $percentOutOfBudget) * 100;

                    # get date array to L7D
                    $dateInL7DArray = Library\Common::dayOfBetween(
                        Library\Common::convertTimestampToDate(strtotime("{$date} -7 days")),
                        Library\Common::convertTimestampToDate(strtotime("{$date} -1 days"))
                    );

                    $assocProductId_totalGmvUsd = [];
                    foreach ($dateInL7DArray as $dateInL7D) {
                        foreach ($this->assocDatePerformance[$dateInL7D] ?? [] as $performance) {
                            $productId = $performance->product_shop_channel_id;
                            $gmvUsd = $performance->gmv_usd;

                            if (isset($assocProductId_totalGmvUsd[$productId])) {
                                $assocProductId_totalGmvUsd[$productId] += $gmvUsd;
                            } else {
                                $assocProductId_totalGmvUsd[$productId] = $gmvUsd;
                            }
                        }
                    }

                    $assocProductId_gmvLossFromOutOfBudget = array_map(function ($item) use ($percentOutOfBudget) {
                        return $item / 7 * $percentOutOfBudget;
                    }, $assocProductId_totalGmvUsd);

                    $gmvLossFromOutOfBudget = array_sum($assocProductId_gmvLossFromOutOfBudget);
                }
            }

            $totalKeywordProduct = count($keywordProductIdArray);
            $totalProduct = count($productIdArray);
            $totalKeywordProduct_haveCostNoSale = count($assocKeywordProductId_haveCostNoSale);
            $totalWastedCostUsd = array_sum($assocKeywordProductId_haveCostNoSale);
            $averagePositionShop = $totalView > 0 ? $totalAvgWeighted / $totalView : 0;

            $totalKeywordProduct_haveSale = count($keywordProductId_haveSaleArray);
            $totalProduct_haveSale = count($productId_haveSaleArray);
            $totalKeyword_haveSale = count($keywordId_haveSaleArray);

            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_NUMBER_SKU_RUN_ADS,
                $totalProduct,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_NUMBER_SKU_KEYWORD_RUN_ADS,
                $totalKeywordProduct,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_AVERAGE_POSITION_SHOP,
                $averagePositionShop,
                $keyText,
                $shopId
            );

            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_NUMBER_SELLING_SKU_KEYWORD,
                $totalKeywordProduct_haveSale,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_NUMBER_SELLING_SKU,
                $totalProduct_haveSale,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_NUMBER_SELLING_KEYWORD,
                $totalKeyword_haveSale,
                $keyText,
                $shopId
            );

            if ($typeReport == 'daily') {
                MetricsTrackerReport::save(
                    $typeReport.MetricsTrackerReport::METRIC_NUMBER_SKU_KEYWORD_HAVE_COST_NO_SALE,
                    $totalKeywordProduct_haveCostNoSale,
                    $keyText,
                    $shopId
                );
                MetricsTrackerReport::save(
                    $typeReport.MetricsTrackerReport::METRIC_TOTAL_WASTED_COST_USD,
                    $totalWastedCostUsd,
                    $keyText,
                    $shopId
                );

                MetricsTrackerReport::save(
                    $typeReport.MetricsTrackerReport::METRIC_ATR,
                    $atr,
                    $keyText,
                    $shopId
                );
                MetricsTrackerReport::save(
                    $typeReport.MetricsTrackerReport::METRIC_GMV_LOSS_FROM_OOB,
                    $gmvLossFromOutOfBudget,
                    $keyText,
                    $shopId
                );
            }

        }
    }

//    /**
//     * _calculator Performance Total Shop
//     * @param array $assocKeyDatePerformance
//     * @param string $typeReport
//     */
//    private function _calculatorPerformanceTotalShop($assocKeyDatePerformance, $typeReport)
//    {
//        foreach ($assocKeyDatePerformance as $keyText => $performanceData) {
//            $totalGmvUsd = array_sum(array_column($performanceData, 0));
//            $totalItemSold = array_sum(array_column($performanceData, 1));
//            $totalSaleOrder = array_sum(array_column($performanceData, 2));
//
//            MetricsTrackerReport::save(
//                $typeReport.MetricsTrackerReport::METRIC_TOTAL_GMV_ADS_ALL_SHOP_USD,
//                $totalGmvUsd,
//                $keyText
//            );
//        }
//    }

}
