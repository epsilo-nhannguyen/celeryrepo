<?php


namespace App\Console\Commands\CommandsReport;


use Illuminate\Console\Command;
use App\Library;
use App\Models;
use App\Repository\RepositoryStat\MetricsBalance;


class PullBalanceElasticSearch extends Command
{
    /**
     * @uses: php artisan pull-balance-elastic-search --isShowMessage=is-show-message
     * example: php artisan pull-balance-elastic-search --isShowMessage=0
     */

    protected $signature = 'pull-balance-elastic-search {--type=} {--isShowMessage=0}';

    protected $description = 'Pull balance from Elastic Search';

    /**
     * @var array
     */
    private $assocDateBalanceData = [];

    /**
     * @var string
     */
    private $dateFromCommon = '';
    private $dateToCommon = '';

    public function handle()
    {
        $isShowMessage = $this->option('isShowMessage');
        $type = $this->option('type');

        Library\Common::setIsShowMessage($isShowMessage);

        Library\Common::showMessage('Start '.Library\Common::getCurrentDatetime().PHP_EOL);

        $elasticSearch = new Library\ElasticSearch($isShowMessage);

        $index = 'balance-*';
        $from = 0;
        $size = 500;
        $channelCode = Models\Channel::SHOPEE_CODE;
        $shopChannelData = Models\ShopChannel::searchShopData(false, false, $channelCode);
        foreach ($shopChannelData as $shopChannel) {
            Library\Common::setTimezone($shopChannel->venture_timezone);
            $shopChannelId = $shopChannel->shop_channel_id;
            $shopId = $shopChannel->shop_master_id;
            $shopChannelName = $shopChannel->shop_channel_name;
            $ventureId = $shopChannel->venture_id;
            $ventureName = $shopChannel->venture_name;
            $currencyCode = $shopChannel->venture_exchange;

            $this->assocDateBalanceData = [];

            $balanceData = $elasticSearch->search($index, $this->_buildParam($shopChannelId, $from, $size));
            $this->_processBalanceData($balanceData);

            $totalPage = $balanceData['hits']['total'] ?? 0;
            for ($i = 1; $i < ceil($totalPage/$size); $i ++) {
                $page = $size * $i + 1;
                $balanceData = $elasticSearch->search($index, $this->_buildParam($shopChannelId, $page, $size));
                $this->_processBalanceData($balanceData);
            }

            ksort($this->assocDateBalanceData);

            $dateHaveBalance = array_keys($this->assocDateBalanceData);

            $dateFrom = current($dateHaveBalance);
            $dateTo = end($dateHaveBalance);
            list($assocDailyDate, $assocWeeklyDate, $assocMonthlyDate) = $this->_buildAssocDate($dateFrom, $dateTo);

            $this->_calculatorBalance($assocDailyDate, 'daily', $shopId, $currencyCode);
            $this->_calculatorBalance($assocWeeklyDate, 'weekly', $shopId, $currencyCode);
            $this->_calculatorBalance($assocMonthlyDate, 'monthly', $shopId, $currencyCode);

            $message = PHP_EOL.'Done save Shop channel id: '.$shopChannelId.' - Shop channel name: '.$shopChannelName.' - Country: '.$ventureName.'.';
            if ( ! $this->assocDateBalanceData) {
                $message.= ' BUT shop is no data';
            }
            Library\Common::showMessage($message);
        }

        Library\Common::showMessage(PHP_EOL.PHP_EOL.PHP_EOL.'Done save all shop'.PHP_EOL.PHP_EOL);
    }

    /**
     * process Balance Data
     * @param array $balanceData
     */
    private function _processBalanceData($balanceData)
    {
        $sourceData = array_column($balanceData['hits']['hits'] ?? [], '_source');
        foreach ($sourceData as $source) {
            $date = Library\Common::convertTimestampToDate($source['Timestamp']);
            $this->assocDateBalanceData[$date][] = $source['wallet'];
        }
    }

    /**
     * build Assoc Date
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    private function _buildAssocDate($dateFrom, $dateTo)
    {
        $assocDailyDate = $assocWeeklyDate = $assocMonthlyDate = $yearMonthArray = [];

        if ( ! $dateFrom || ! $dateTo) {
            $today = date('Y-m-d');
            $yesterday = Library\Common::convertTimestampToDate(strtotime("{$today} -1 days"));
            $assocDailyDate[$yesterday] = [$yesterday];

            $this->dateFromCommon = $yesterday;
            $this->dateToCommon = $yesterday;

            $timestampYesterday = strtotime($yesterday);
            $year = date('Y', $timestampYesterday);
            $month = date('m', $timestampYesterday);

            $dayNumber = date('d'); # 01 to 31
            $dayText = date('D'); # Mon through Sun
            if ($dayText == 'Mon') {
                $dateInWeekArray = [$yesterday];
                for ($i = 1; $i < 7; $i ++) {
                    array_unshift($dateInWeekArray, date('Y-m-d', strtotime($yesterday.'- '.$i.' days')));
                }

                foreach ($dateInWeekArray as $dateInWeek) {
                    $timestampDateWeek = strtotime($dateInWeek);

                    $_weekly = date('W', $timestampDateWeek);
                    $_month = date('m', $timestampDateWeek);
                    $_year = date('Y', $timestampDateWeek);

                    if ($_weekly == '01' && $_month == '12') {
                        $_weekly = '53';
                    }
                    $yearWeek = $_year.'-w'.$_weekly;
                    $assocWeeklyDate[$yearWeek][] = $dateInWeek;
                }
                $this->dateFromCommon = current($dateInWeekArray);
            }

            if ($dayNumber == '01') {
                $assocMonthlyDate[$year.'-'.$month] = Library\Common::dayOfBetween(
                    date('Y-m-01', $timestampYesterday), date('Y-m-t', $timestampYesterday)
                );

                $this->dateFromCommon = date('Y-m-01', $timestampYesterday);
                $this->dateToCommon = date('Y-m-t', $timestampYesterday);
            }
        } else {
            $dateArray = Library\Common::dayOfBetween($dateFrom, $dateTo);
            $thisDay = date('Y-m-d');
            $thisYearMonth = date('Y-m');

            $thisYear = date('Y');
            $thisMonth = date('m');
            $thisWeekly = date('W');

            if ($thisWeekly == '01' && $thisMonth == '12') {
                $thisWeekly = '53';
            }

            $thisYearWeek = $thisYear.'-w'.$thisWeekly;

            foreach ($dateArray as $date) {
                $timestampDate = strtotime($date);

                if ($date != $thisDay) {
                    $assocDailyDate[$date][] = $date;
                }

                $year = date('Y', $timestampDate);
                $month = date('m', $timestampDate);
                $weekly = date('W', $timestampDate);

                if ($weekly == '01' && $month == '12') {
                    $weekly = '53';
                }

                $yearWeek = $year.'-w'.$weekly;
                if ($yearWeek != $thisYearWeek) {
                    $assocWeeklyDate[$yearWeek][] = $date;
                }

                $yearMonth = date('Y-m', $timestampDate);

                if ($yearMonth != $thisYearMonth &&  ! in_array($yearMonth, $yearMonthArray)) {
                    $yearMonthArray[] = $yearMonth;
                    $assocMonthlyDate[$yearMonth] = Library\Common::dayOfBetween(date('Y-m-01', $timestampDate), date('Y-m-t', $timestampDate));
                }
            }
            $this->dateFromCommon = $dateFrom;
            $this->dateToCommon = $dateTo;
        }

        return [$assocDailyDate, $assocWeeklyDate, $assocMonthlyDate];
    }

    /**
     * calculator Balance
     * @param array $assocDate
     * @param string $typeReport
     * @param int $shopId
     * @param string $currencyCode
     */
    private function _calculatorBalance($assocDate, $typeReport, $shopId, $currencyCode)
    {
        foreach ($assocDate as $keyText => $dateThisKeyArray) {
            $assocLxDDate = $this->_buildLxDData($dateThisKeyArray);
            $assocLxDDate['thisKeyText'] = $dateThisKeyArray;
            $balanceLxDDataLocal = $balanceLxDDataUsd = [];
            foreach ($assocLxDDate as $LxD => $dateLxDArray) {
                $balanceDataLocal = $balanceDataUsd = [];
                foreach ($dateLxDArray as $date) {
                    $balanceLocalArray = $this->assocDateBalanceData[$date] ?? [];
                    $balanceDataLocal = array_merge($balanceDataLocal, $balanceLocalArray);

                    $balanceUsdArray = array_map(function ($item) use ($date, $currencyCode) {
                        return Library\CurrencyStat::convertCurrency($item, $date, $currencyCode);
                    }, $balanceLocalArray);

                    $balanceDataUsd = array_merge($balanceDataUsd, $balanceUsdArray);
                }
                $balanceLxDDataLocal[$LxD] = $balanceDataLocal;
                $balanceLxDDataUsd[$LxD] = $balanceDataUsd;
            }

            $balanceCalculator = $balanceUsdCalculator = [];
            foreach ($balanceLxDDataLocal as $_LxD => $_balanceData) {
                $min = $max = $average = 0;
                $minUsd = $maxUsd = $averageUsd = 0;

                $countBalance = count($_balanceData);
                if ($countBalance) {
                    $balanceDataUnique = array_values(array_unique($_balanceData));
                    $min = min($balanceDataUnique);
                    $max = max($balanceDataUnique);
                    $average = array_sum($_balanceData) / $countBalance;
                }

                $_balanceDataUsd = $balanceLxDDataUsd[$_LxD] ?? [];
                $countBalanceUsd = count($_balanceDataUsd);
                if ($countBalanceUsd) {
                    $balanceDataUniqueUsd = array_values(array_unique($_balanceDataUsd));
                    $minUsd = min($balanceDataUniqueUsd);
                    $maxUsd = max($balanceDataUniqueUsd);
                    $averageUsd = array_sum($_balanceDataUsd) / $countBalanceUsd;
                }

                $balanceCalculator[$_LxD.'Min'] = $min;
                $balanceCalculator[$_LxD.'Max'] = $max;
                $balanceCalculator[$_LxD.'Average'] = $average;

                $balanceUsdCalculator[$_LxD.'Min'] = $minUsd;
                $balanceUsdCalculator[$_LxD.'Max'] = $maxUsd;
                $balanceUsdCalculator[$_LxD.'Average'] = $averageUsd;
            }

            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_AVERAGE_USD,
                $balanceUsdCalculator['thisKeyTextAverage'],
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_AVERAGE_LOCAL,
                $balanceCalculator['thisKeyTextAverage'],
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_AVERAGE_L7D,
                $balanceCalculator['L7DAverage'] > 0 ? $balanceCalculator['thisKeyTextAverage'] / $balanceCalculator['L7DAverage'] * 100 : 0,
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_AVERAGE_L14D,
                $balanceCalculator['L14DAverage'] > 0 ? $balanceCalculator['thisKeyTextAverage'] / $balanceCalculator['L14DAverage'] * 100 : 0,
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_AVERAGE_L30D,
                $balanceCalculator['L30DAverage'] > 0 ? $balanceCalculator['thisKeyTextAverage'] / $balanceCalculator['L30DAverage'] * 100 : 0,
                $keyText,
                $shopId
            );

            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MIN_USD,
                $balanceUsdCalculator['thisKeyTextMin'],
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MIN_LOCAL,
                $balanceCalculator['thisKeyTextMin'],
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MIN_L7D,
                $balanceCalculator['L7DMin'] > 0 ? $balanceCalculator['thisKeyTextMin'] / $balanceCalculator['L7DMin'] * 100 : 0,
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MIN_L14D,
                $balanceCalculator['L14DMin'] > 0 ? $balanceCalculator['thisKeyTextMin'] / $balanceCalculator['L14DMin'] * 100 : 0,
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MIN_L30D,
                $balanceCalculator['L30DMin'] > 0 ? $balanceCalculator['thisKeyTextMin'] / $balanceCalculator['L30DMin'] * 100 : 0,
                $keyText,
                $shopId
            );

            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MAX_USD,
                $balanceUsdCalculator['thisKeyTextMax'],
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MAX_LOCAL,
                $balanceCalculator['thisKeyTextMax'],
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MAX_L7D,
                $balanceCalculator['L7DMax'] > 0 ? $balanceCalculator['thisKeyTextMax'] / $balanceCalculator['L7DMax'] * 100 : 0,
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MAX_L14D,
                $balanceCalculator['L14DMax'] > 0 ? $balanceCalculator['thisKeyTextMax'] / $balanceCalculator['L14DMax'] * 100 : 0,
                $keyText,
                $shopId
            );
            MetricsBalance::save(
                $typeReport.MetricsBalance::METRIC_MAX_L30D,
                $balanceCalculator['L30DMax'] > 0 ? $balanceCalculator['thisKeyTextMax'] / $balanceCalculator['L30DMax'] * 100 : 0,
                $keyText,
                $shopId
            );
        }
    }

    /**
     * _build LxD Data
     * @param array $dateArray
     * @return array
     */
    private function _buildLxDData($dateArray)
    {
        $assocLxDDate = [];

        $dateFirst = current($dateArray);
        $LxDArray = [7, 14, 30];
        foreach ($LxDArray as $LxD) {
            $dateFrom = date('Y-m-d', strtotime($dateFirst.'- '.$LxD.' days'));
            $dateTo = date('Y-m-d', strtotime($dateFirst.'- 1 days'));

            $assocLxDDate['L'.$LxD.'D'] = Library\Common::dayOfBetween($dateFrom, $dateTo);
        }

        return $assocLxDDate;
    }

    /**
     * build Param
     * @param int $shopChannelId
     * @param int $from
     * @param int $size
     * @return array
     */
    private function _buildParam($shopChannelId, $from, $size)
    {
        return [
            '_source' => [
                'includes' => ['shopChannelId', 'wallet', 'Timestamp']
            ],
            'from' => $from,
            'size' => $size,
            'query' => [
                'match' => [
                    'shopChannelId' => $shopChannelId
                ]
            ]
        ];
    }

}