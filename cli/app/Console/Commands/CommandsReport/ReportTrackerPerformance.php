<?php


namespace App\Console\Commands\CommandsReport;


use Illuminate\Console\Command;
use App\Library;
use App\Models;
use App\Repository\RepositoryStat\MetricsTrackerReport;


class ReportTrackerPerformance extends Command
{
    /**
     * @uses: php artisan report-tracker-performance --dateFrom=date-from --dateTo=date-to --type=type --isShowMessage=is-show-message
     * example: php artisan report-tracker-performance --dateFrom=2019-09-01 --dateTo=2019-09-30 --type=daily --isShowMessage=0
     */

    protected $signature = 'report-tracker-performance {--dateFrom=} {--dateTo=} {--type=} {--isShowMessage=0}';

    protected $description = 'Report Tracker Performance from Marketing to Stat';

    /**
     * @var array
     */
    private $assocDatePerformance = [];
    private $assocDailyTotalPerformance = [];
    private $assocWeeklyTotalPerformance = [];
    private $assocMonthlyTotalPerformance = [];

    /**
     * @var string
     */
    private $dateFromCommon = '';
    private $dateToCommon = '';

    public function handle()
    {
        ini_set('memory_limit','2048M');

        $isShowMessage = $this->option('isShowMessage');
        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');
        $type = $this->option('type');

        Library\Common::setIsShowMessage($isShowMessage);

        Library\Common::showMessage('Start '.Library\Common::getCurrentDatetime().PHP_EOL);

        list($assocDailyDate, $assocWeeklyDate, $assocMonthlyDate) = $this->_buildAssocDate($dateFrom, $dateTo);

        if ($type == 'daily') {
            $assocWeeklyDate = $assocMonthlyDate = [];
        } elseif ($type == 'weekly') {
            $assocDailyDate = $assocMonthlyDate = [];
        } elseif ($type == 'monthly') {
            $assocDailyDate = $assocWeeklyDate = [];
        }

        $channelCode = Models\Channel::SHOPEE_CODE;
        $shopChannelData = Models\ShopChannel::searchShopData(false, false, $channelCode);
        foreach ($shopChannelData as $shopChannel) {
            Library\Common::setTimezone($shopChannel->venture_timezone);

            $ventureId = $shopChannel->venture_id;
            $shopChannelId = $shopChannel->shop_channel_id;
            $shopId = $shopChannel->shop_master_id;
            $channelId = $shopChannel->channel_id;
            $shopChannelName = $shopChannel->shop_channel_name;
            $shopChannelIsActive = $shopChannel->shop_channel_is_active;
            $shopChannelCreatedAt = $shopChannel->shop_channel_created_at;
            $shopChannelDateCreated = Library\Common::convertTimestampToDate($shopChannelCreatedAt);
            $ventureName = $shopChannel->venture_name;
            $currencyCode = $shopChannel->venture_exchange;

            $this->assocDatePerformance = [];

            $performanceData = Models\MakPerformance::sumPerformanceByShopChannelId($shopChannelId, $this->dateFromCommon, $this->dateToCommon);
            foreach ($performanceData as $performanceItem) {
                $performanceDate = $performanceItem->mak_performance_date;
                $performanceItem->total_gmv_usd = Library\CurrencyStat::convertCurrency($performanceItem->total_gmv, $performanceDate, $currencyCode);
                $performanceItem->total_cost_usd = Library\CurrencyStat::convertCurrency($performanceItem->total_cost, $performanceDate, $currencyCode);
                $this->assocDatePerformance[$performanceDate] = $performanceItem;
            }

            $this->_calculatorPerformance($assocDailyDate, 'daily', $shopId);
            $this->_calculatorPerformance($assocWeeklyDate, 'weekly', $shopId);
            $this->_calculatorPerformance($assocMonthlyDate, 'monthly', $shopId);

            $message = PHP_EOL.'Done save Paid Ads Shop channel id: '.$shopChannelId.' - Shop channel name: '.$shopChannelName.' - Country: '.$ventureName;
            Library\Common::showMessage($message);
        }

        $this->_calculatorPerformanceTotalShop($this->assocDailyTotalPerformance, 'daily');
        $this->_calculatorPerformanceTotalShop($this->assocWeeklyTotalPerformance, 'weekly');
        $this->_calculatorPerformanceTotalShop($this->assocMonthlyTotalPerformance, 'monthly');

        Library\Common::showMessage(PHP_EOL.PHP_EOL.PHP_EOL.'Done save Paid Ads all shop'.PHP_EOL);

        Library\Common::showMessage('End '.Library\Common::getCurrentDatetime().PHP_EOL);

    }

    /**
     * build Assoc Date
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    private function _buildAssocDate($dateFrom, $dateTo)
    {
        $assocDailyDate = $assocWeeklyDate = $assocMonthlyDate = $yearMonthArray = [];

        if ( ! $dateFrom || ! $dateTo) {
            $today = date('Y-m-d');
            $yesterday = Library\Common::convertTimestampToDate(strtotime("{$today} -1 days"));
            $assocDailyDate[$yesterday] = [$yesterday];

            $this->dateFromCommon = $yesterday;
            $this->dateToCommon = $yesterday;

            $timestampYesterday = strtotime($yesterday);
            $year = date('Y', $timestampYesterday);
            $month = date('m', $timestampYesterday);

            $dayNumber = date('d'); # 01 to 31
            $dayText = date('D'); # Mon through Sun
            if ($dayText == 'Mon') {
                $dateInWeekArray = [$yesterday];
                for ($i = 1; $i < 7; $i ++) {
                    array_unshift($dateInWeekArray, date('Y-m-d', strtotime($yesterday.'- '.$i.' days')));
                }

                foreach ($dateInWeekArray as $dateInWeek) {
                    $timestampDateWeek = strtotime($dateInWeek);

                    $_weekly = date('W', $timestampDateWeek);
                    $_month = date('m', $timestampDateWeek);
                    $_year = date('Y', $timestampDateWeek);

                    if ($_weekly == '01' && $_month == '12') {
                        $_weekly = '53';
                    }
                    $yearWeek = $_year.'-w'.$_weekly;
                    $assocWeeklyDate[$yearWeek][] = $dateInWeek;
                }
                $this->dateFromCommon = current($dateInWeekArray);
            }

            if ($dayNumber == '01') {
                $assocMonthlyDate[$year.'-'.$month] = Library\Common::dayOfBetween(
                    date('Y-m-01', $timestampYesterday), date('Y-m-t', $timestampYesterday)
                );

                $this->dateFromCommon = date('Y-m-01', $timestampYesterday);
                $this->dateToCommon = date('Y-m-t', $timestampYesterday);
            }
        } else {
            $dateArray = Library\Common::dayOfBetween($dateFrom, $dateTo);
            $thisDay = date('Y-m-d');
            $thisYearMonth = date('Y-m');

            $thisYear = date('Y');
            $thisMonth = date('m');
            $thisWeekly = date('W');

            if ($thisWeekly == '01' && $thisMonth == '12') {
                $thisWeekly = '53';
            }

            $thisYearWeek = $thisYear.'-w'.$thisWeekly;

            foreach ($dateArray as $date) {
                $timestampDate = strtotime($date);

                $assocDailyDate[$date][] = $date;

                $year = date('Y', $timestampDate);
                $month = date('m', $timestampDate);
                $weekly = date('W', $timestampDate);

                if ($weekly == '01' && $month == '12') {
                    $weekly = '53';
                }

                $yearWeek = $year.'-w'.$weekly;
                $assocWeeklyDate[$yearWeek][] = $date;

                $yearMonth = date('Y-m', $timestampDate);

                if ( ! in_array($yearMonth, $yearMonthArray)) {
                    $yearMonthArray[] = $yearMonth;
                    $assocMonthlyDate[$yearMonth] = Library\Common::dayOfBetween(date('Y-m-01', $timestampDate), date('Y-m-t', $timestampDate));
                }
            }
            $this->dateFromCommon = $dateFrom;
            $this->dateToCommon = $dateTo;
        }

        return [$assocDailyDate, $assocWeeklyDate, $assocMonthlyDate];
    }

    /**
     * _calculator Performance
     * @param array $assocDate
     * @param string $typeReport
     * @param int $shopId
     */
    private function _calculatorPerformance($assocDate, $typeReport, $shopId)
    {
        foreach ($assocDate as $keyText => $dateArray) {
             $totalGmvUsd = $totalItemSold = $totalSaleOrder = 0;
             $totalGmv = $totalCost = $totalCostUsd = $totalView = $totalClick = 0;
            foreach ($dateArray as $date) {
                $performance = $this->assocDatePerformance[$date] ?? new \stdClass();

                $totalGmvUsd += $performance->total_gmv_usd ?? 0;
                $totalItemSold += $performance->total_item_sold ?? 0;
                $totalSaleOrder += $performance->total_sale_order ?? 0;
                $totalGmv += $performance->total_gmv ?? 0;
                $totalCost += $performance->total_cost ?? 0;
                $totalCostUsd += $performance->total_cost_usd ?? 0;
                $totalView += $performance->total_view ?? 0;
                $totalClick += $performance->total_click ?? 0;
            }

            $cpc = $totalClick > 0 ? $totalCost / $totalClick : 0;
            $ctr = $totalView > 0 ? $totalClick / $totalView * 100 : 0;
            $cr = $totalClick > 0 ? $totalItemSold / $totalClick * 100 : 0;
            $cir = $totalGmv > 0 ? $totalCost / $totalGmv * 100 : 0;

            if ($typeReport == 'daily') {
                $this->assocDailyTotalPerformance[$keyText][] = [$totalGmvUsd, $totalItemSold, $totalSaleOrder];
            } elseif ($typeReport == 'weekly') {
                $this->assocWeeklyTotalPerformance[$keyText][] = [$totalGmvUsd, $totalItemSold, $totalSaleOrder];
            } else {
                $this->assocMonthlyTotalPerformance[$keyText][] = [$totalGmvUsd, $totalItemSold, $totalSaleOrder];
            }

            # Start Stat Report New 2020-Feb
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_IMPRESSION_ADS,
                $totalView,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_CLICK_ADS,
                $totalClick,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_CPC_ADS,
                $cpc,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_PERCENT_CTR_ADS,
                $ctr,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_PERCENT_CR_ADS,
                $cr,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_PERCENT_CIR_ADS,
                $cir,
                $keyText,
                $shopId
            );
            # End Stat Report New 2020-Feb

            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_COST_ADS_LOCAL,
                $totalCost,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_COST_ADS_USD,
                $totalCostUsd,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_GMV_ADS_USD,
                $totalGmvUsd,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_ITEM_SOLD_ADS,
                $totalItemSold,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_ORDER_ADS,
                $totalSaleOrder,
                $keyText,
                $shopId
            );
        }
    }

    /**
     * _calculator Performance Total Shop
     * @param array $assocKeyDatePerformance
     * @param string $typeReport
     */
    private function _calculatorPerformanceTotalShop($assocKeyDatePerformance, $typeReport)
    {
        foreach ($assocKeyDatePerformance as $keyText => $performanceData) {
            $totalGmvUsd = array_sum(array_column($performanceData, 0));
            $totalItemSold = array_sum(array_column($performanceData, 1));
            $totalSaleOrder = array_sum(array_column($performanceData, 2));

            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_GMV_ADS_ALL_SHOP_USD,
                $totalGmvUsd,
                $keyText
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_ITEM_SOLD_ADS_ALL_SHOP,
                $totalItemSold,
                $keyText
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_ORDER_ADS_ALL_SHOP,
                $totalSaleOrder,
                $keyText
            );
        }
    }

}
