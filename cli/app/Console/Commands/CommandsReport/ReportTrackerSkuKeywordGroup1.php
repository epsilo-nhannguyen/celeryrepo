<?php


namespace App\Console\Commands\CommandsReport;


use Illuminate\Console\Command;
use App\Library;
use App\Models;
use App\Repository\RepositoryStat\MetricsTrackerReportSkuKeyword;


class ReportTrackerSkuKeywordGroup1 extends Command
{
    /**
     * @uses: php artisan report-tracker-sku-keyword-group1 --dateFrom=date-from --dateTo=date-to --type=type --isShowMessage=is-show-message
     * example: php artisan report-tracker-sku-keyword-group1 --dateFrom=2019-09-01 --dateTo=2019-09-30 --type=daily --isShowMessage=0
     */

    protected $signature = 'report-tracker-sku-keyword-group1 {--dateFrom=} {--dateTo=} {--type=} {--isShowMessage=0}';

    protected $description = 'Report Tracker Sku Keyword Group1 from Marketing to Stat';

    /**
     * @var array
     */
    private $assocDatePerformance = [];
    private $assocDailyTotalPerformance = [];
    private $assocWeeklyTotalPerformance = [];
    private $assocMonthlyTotalPerformance = [];

    private $assocKeywordProductId_data = [];

    /**
     * @var string
     */
    private $dateFromCommon = '';
    private $dateToCommon = '';

    public function handle()
    {
        ini_set('memory_limit','2048M');

        $isShowMessage = $this->option('isShowMessage');
        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');
        $type = $this->option('type');

        Library\Common::setIsShowMessage($isShowMessage);

        Library\Common::showMessage('Start '.Library\Common::getCurrentDatetime().PHP_EOL);

        list($assocDailyDate, $assocWeeklyDate, $assocMonthlyDate) = $this->_buildAssocDate($dateFrom, $dateTo);

        if ($type == 'daily') {
            $assocWeeklyDate = $assocMonthlyDate = [];
        } elseif ($type == 'weekly') {
            $assocDailyDate = $assocMonthlyDate = [];
        } elseif ($type == 'monthly') {
            $assocDailyDate = $assocWeeklyDate = [];
        }

        $channelCode = Models\Channel::SHOPEE_CODE;
        $shopChannelData = Models\ShopChannel::searchShopData(false, false, $channelCode);
        foreach ($shopChannelData as $shopChannel) {
            Library\Common::setTimezone($shopChannel->venture_timezone);

            $ventureId = $shopChannel->venture_id;
            $shopChannelId = $shopChannel->shop_channel_id;
            $shopId = $shopChannel->shop_master_id;
            $channelId = $shopChannel->channel_id;
            $shopChannelName = $shopChannel->shop_channel_name;
            $shopChannelIsActive = $shopChannel->shop_channel_is_active;
            $shopChannelCreatedAt = $shopChannel->shop_channel_created_at;
            $shopChannelDateCreated = Library\Common::convertTimestampToDate($shopChannelCreatedAt);
            $ventureName = $shopChannel->venture_name;
            $currencyCode = $shopChannel->venture_exchange;

            $this->assocDatePerformance = [];
            $this->assocKeywordProductId_data = [];

            $performanceData = Models\MakPerformance::searchForSkuKeywordByShopChannelId($shopChannelId, $this->dateFromCommon, $this->dateToCommon);
            foreach ($performanceData as $performanceItem) {
                $keywordProductId = $performanceItem->mak_programmatic_keyword_product_id;
                $productId = $performanceItem->product_shop_channel_id;
                $itemId = $performanceItem->product_shop_channel_item_id;
                $keywordId = $performanceItem->mak_programmatic_keyword_id;
                $keywordName = $performanceItem->mak_programmatic_keyword_name;
                if ( ! isset($this->assocKeywordProductId_data[$keywordProductId])) {
                    $this->assocKeywordProductId_data[$keywordProductId] = [$productId, $itemId, $keywordId, $keywordName];
                }

                $performanceDate = $performanceItem->mak_performance_date;
                $performanceItem->cost_usd = Library\CurrencyStat::convertCurrency($performanceItem->mak_performance_expense, $performanceDate, $currencyCode);
                $performanceItem->gmv_usd = Library\CurrencyStat::convertCurrency($performanceItem->mak_performance_gmv, $performanceDate, $currencyCode);
                $this->assocDatePerformance[$performanceDate][] = $performanceItem;
            }

            $this->_calculatorPerformance($assocDailyDate, 'daily', $shopId);
            $this->_calculatorPerformance($assocWeeklyDate, 'weekly', $shopId);
            $this->_calculatorPerformance($assocMonthlyDate, 'monthly', $shopId);

            $message = PHP_EOL.'Done save Sku Keyword Group 1 Shop channel id: '.$shopChannelId.' - Shop channel name: '.$shopChannelName.' - Country: '.$ventureName;
            Library\Common::showMessage($message);
        }

        Library\Common::showMessage('End '.Library\Common::getCurrentDatetime().PHP_EOL);

    }

    /**
     * build Assoc Date
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    private function _buildAssocDate($dateFrom, $dateTo)
    {
        $assocDailyDate = $assocWeeklyDate = $assocMonthlyDate = $yearMonthArray = [];

        if ( ! $dateFrom || ! $dateTo) {
            $today = date('Y-m-d');
            $yesterday = Library\Common::convertTimestampToDate(strtotime("{$today} -1 days"));
            $assocDailyDate[$yesterday] = [$yesterday];

            $this->dateFromCommon = $yesterday;
            $this->dateToCommon = $yesterday;

            $timestampYesterday = strtotime($yesterday);
            $year = date('Y', $timestampYesterday);
            $month = date('m', $timestampYesterday);

            $dayNumber = date('d'); # 01 to 31
            $dayText = date('D'); # Mon through Sun
            if ($dayText == 'Mon') {
                $dateInWeekArray = [$yesterday];
                for ($i = 1; $i < 7; $i ++) {
                    array_unshift($dateInWeekArray, date('Y-m-d', strtotime($yesterday.'- '.$i.' days')));
                }

                foreach ($dateInWeekArray as $dateInWeek) {
                    $timestampDateWeek = strtotime($dateInWeek);

                    $_weekly = date('W', $timestampDateWeek);
                    $_month = date('m', $timestampDateWeek);
                    $_year = date('Y', $timestampDateWeek);

                    if ($_weekly == '01' && $_month == '12') {
                        $_weekly = '53';
                    }
                    $yearWeek = $_year.'-w'.$_weekly;
                    $assocWeeklyDate[$yearWeek][] = $dateInWeek;
                }
                $this->dateFromCommon = current($dateInWeekArray);
            }

            if ($dayNumber == '01') {
                $assocMonthlyDate[$year.'-'.$month] = Library\Common::dayOfBetween(
                    date('Y-m-01', $timestampYesterday), date('Y-m-t', $timestampYesterday)
                );

                $this->dateFromCommon = date('Y-m-01', $timestampYesterday);
                $this->dateToCommon = date('Y-m-t', $timestampYesterday);
            }
        } else {
            $dateArray = Library\Common::dayOfBetween($dateFrom, $dateTo);
            $thisDay = date('Y-m-d');
            $thisYearMonth = date('Y-m');

            $thisYear = date('Y');
            $thisMonth = date('m');
            $thisWeekly = date('W');

            if ($thisWeekly == '01' && $thisMonth == '12') {
                $thisWeekly = '53';
            }

            $thisYearWeek = $thisYear.'-w'.$thisWeekly;

            foreach ($dateArray as $date) {
                $timestampDate = strtotime($date);

                $assocDailyDate[$date][] = $date;

                $year = date('Y', $timestampDate);
                $month = date('m', $timestampDate);
                $weekly = date('W', $timestampDate);

                if ($weekly == '01' && $month == '12') {
                    $weekly = '53';
                }

                $yearWeek = $year.'-w'.$weekly;
                $assocWeeklyDate[$yearWeek][] = $date;

                $yearMonth = date('Y-m', $timestampDate);

                if ( ! in_array($yearMonth, $yearMonthArray)) {
                    $yearMonthArray[] = $yearMonth;
                    $assocMonthlyDate[$yearMonth] = Library\Common::dayOfBetween(date('Y-m-01', $timestampDate), date('Y-m-t', $timestampDate));
                }
            }
            $this->dateFromCommon = $dateFrom;
            $this->dateToCommon = $dateTo;
        }

        return [$assocDailyDate, $assocWeeklyDate, $assocMonthlyDate];
    }

    /**
     * _calculator Performance
     * @param array $assocDate
     * @param string $typeReport
     * @param int $shopId
     */
    private function _calculatorPerformance($assocDate, $typeReport, $shopId)
    {
        foreach ($assocDate as $keyText => $dateArray) {
            $assocKeywordProductId_totalView = $assocKeywordProductId_totalClick = $assocKeywordProductId_totalItemSold = [];
            $assocKeywordProductId_totalCostUsd = $assocKeywordProductId_totalGmvUsd = [];
            $assocKeywordProductId_avgPosition = [];

            foreach ($dateArray as $date) {
                foreach ($this->assocDatePerformance[$date] ?? [] as $performance) {
                    $keywordProductId = $performance->mak_programmatic_keyword_product_id;

                    $view = $performance->mak_performance_view;
                    $click = $performance->mak_performance_clicks;
                    $costUsd = $performance->cost_usd;
                    $gmvUsd = $performance->gmv_usd;
                    $itemSold = $performance->mak_performance_product_sold;
                    $avgPosition = $performance->mak_performance_avg_position;

                    # view
                    if (isset($assocKeywordProductId_totalView[$keywordProductId])) {
                        $assocKeywordProductId_totalView[$keywordProductId] += $view;
                    } else {
                        $assocKeywordProductId_totalView[$keywordProductId] = $view;
                    }

                    # click
                    if (isset($assocKeywordProductId_totalClick[$keywordProductId])) {
                        $assocKeywordProductId_totalClick[$keywordProductId] += $click;
                    } else {
                        $assocKeywordProductId_totalClick[$keywordProductId] = $click;
                    }

                    # item sold
                    if (isset($assocKeywordProductId_totalItemSold[$keywordProductId])) {
                        $assocKeywordProductId_totalItemSold[$keywordProductId] += $itemSold;
                    } else {
                        $assocKeywordProductId_totalItemSold[$keywordProductId] = $itemSold;
                    }

                    # cost
                    if (isset($assocKeywordProductId_totalCostUsd[$keywordProductId])) {
                        $assocKeywordProductId_totalCostUsd[$keywordProductId] += $costUsd;
                    } else {
                        $assocKeywordProductId_totalCostUsd[$keywordProductId] = $costUsd;
                    }

                    # gmv
                    if (isset($assocKeywordProductId_totalGmvUsd[$keywordProductId])) {
                        $assocKeywordProductId_totalGmvUsd[$keywordProductId] += $gmvUsd;
                    } else {
                        $assocKeywordProductId_totalGmvUsd[$keywordProductId] = $gmvUsd;
                    }

                    # avg position
                    if ( ! isset($assocKeywordProductId_avgPosition[$keywordProductId])) {
                        $assocKeywordProductId_avgPosition[$keywordProductId] = $avgPosition;
                    }
                }
            }

            foreach ($assocKeywordProductId_totalView as $keywordProductId => $totalValue) {
                list($productId, $itemId, $keywordId, $keywordName) = $this->assocKeywordProductId_data[$keywordProductId];
                MetricsTrackerReportSkuKeyword::save(
                    $typeReport.MetricsTrackerReportSkuKeyword::METRIC_TOTAL_IMPRESSION_ADS,
                    $totalValue, $keyText, $keywordProductId, $productId, $itemId, $keywordId, $keywordName, $shopId
                );
            }

            foreach ($assocKeywordProductId_totalClick as $keywordProductId => $totalValue) {
                list($productId, $itemId, $keywordId, $keywordName) = $this->assocKeywordProductId_data[$keywordProductId];
                MetricsTrackerReportSkuKeyword::save(
                    $typeReport.MetricsTrackerReportSkuKeyword::METRIC_TOTAL_CLICK_ADS,
                    $totalValue, $keyText, $keywordProductId, $productId, $itemId, $keywordId, $keywordName, $shopId
                );
            }

            foreach ($assocKeywordProductId_totalItemSold as $keywordProductId => $totalValue) {
                list($productId, $itemId, $keywordId, $keywordName) = $this->assocKeywordProductId_data[$keywordProductId];
                MetricsTrackerReportSkuKeyword::save(
                    $typeReport.MetricsTrackerReportSkuKeyword::METRIC_TOTAL_ITEM_SOLD_ADS,
                    $totalValue, $keyText, $keywordProductId, $productId, $itemId, $keywordId, $keywordName, $shopId
                );
            }

            foreach ($assocKeywordProductId_totalCostUsd as $keywordProductId => $totalValue) {
                list($productId, $itemId, $keywordId, $keywordName) = $this->assocKeywordProductId_data[$keywordProductId];
                MetricsTrackerReportSkuKeyword::save(
                    $typeReport.MetricsTrackerReportSkuKeyword::METRIC_TOTAL_COST_ADS_USD,
                    $totalValue, $keyText, $keywordProductId, $productId, $itemId, $keywordId, $keywordName, $shopId
                );
            }

            foreach ($assocKeywordProductId_totalGmvUsd as $keywordProductId => $totalValue) {
                list($productId, $itemId, $keywordId, $keywordName) = $this->assocKeywordProductId_data[$keywordProductId];
                MetricsTrackerReportSkuKeyword::save(
                    $typeReport.MetricsTrackerReportSkuKeyword::METRIC_TOTAL_GMV_ADS_USD,
                    $totalValue, $keyText, $keywordProductId, $productId, $itemId, $keywordId, $keywordName, $shopId
                );
            }

            if ($typeReport == 'daily') {
                foreach ($assocKeywordProductId_avgPosition as $keywordProductId => $totalValue) {
                    list($productId, $itemId, $keywordId, $keywordName) = $this->assocKeywordProductId_data[$keywordProductId];
                    MetricsTrackerReportSkuKeyword::save(
                        $typeReport.MetricsTrackerReportSkuKeyword::METRIC_AVERAGE_POSITION,
                        $totalValue, $keyText, $keywordProductId, $productId, $itemId, $keywordId, $keywordName, $shopId
                    );
                }
            }

        }
    }
}
