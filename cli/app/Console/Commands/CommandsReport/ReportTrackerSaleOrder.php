<?php


namespace App\Console\Commands\CommandsReport;


use Illuminate\Console\Command;
use App\Library;
use App\Models;
use App\Repository\RepositoryStat\MetricsTrackerReport;


class ReportTrackerSaleOrder extends Command
{

    /**
     * @uses: php artisan report-tracker-sale-order --dateFrom=date-from --dateTo=date-to --type=type --isShowMessage=is-show-message
     * example: php artisan report-tracker-sale-order --dateFrom=2019-09-01 --dateTo=2019-09-30 --type=daily --isShowMessage=0
     */

    protected $signature = 'report-tracker-sale-order {--dateFrom=} {--dateTo=} {--type=} {--isShowMessage=0}';

    protected $description = 'Report Tracker Sale Order from Marketing to Stat';

    /**
     * @var array
     */
    private $assocDateSaleOrder = [];
    private $assocDailyTotalSaleOrder = [];
    private $assocWeeklyTotalSaleOrder = [];
    private $assocMonthlyTotalSaleOrder = [];

    /**
     * @var string
     */
    private $dateFromCommon = '';
    private $dateToCommon = '';

    public function handle()
    {
        ini_set('memory_limit','2048M');

        $isShowMessage = $this->option('isShowMessage');
        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');
        $type = $this->option('type');

        Library\Common::setIsShowMessage($isShowMessage);

        Library\Common::showMessage('Start '.Library\Common::getCurrentDatetime().PHP_EOL);

        list($assocDailyDate, $assocWeeklyDate, $assocMonthlyDate) = $this->_buildAssocDate($dateFrom, $dateTo);

        if ($type == 'daily') {
            $assocWeeklyDate = $assocMonthlyDate = [];
        } elseif ($type == 'weekly') {
            $assocDailyDate = $assocMonthlyDate = [];
        } elseif ($type == 'monthly') {
            $assocDailyDate = $assocWeeklyDate = [];
        }

        $channelCode = Models\Channel::SHOPEE_CODE;
        $shopChannelData = Models\ShopChannel::searchShopData(false, false, $channelCode);
        foreach ($shopChannelData as $shopChannel) {
            Library\Common::setTimezone($shopChannel->venture_timezone);

            $ventureId = $shopChannel->venture_id;
            $shopChannelId = $shopChannel->shop_channel_id;
            $shopId = $shopChannel->shop_master_id;
            $channelId = $shopChannel->channel_id;
            $shopChannelName = $shopChannel->shop_channel_name;
            $shopChannelCreatedAt = $shopChannel->shop_channel_created_at;
            $shopChannelDateCreated = Library\Common::convertTimestampToDate($shopChannelCreatedAt);
            $ventureName = $shopChannel->venture_name;
            $currencyCode = $shopChannel->venture_exchange;

            $this->assocDateSaleOrder = [];

            $saleOrderData = Models\Mongo\SaleOrder::searchByShopIdChannelId(
                $shopId, $channelId, strtotime($this->dateFromCommon), strtotime($this->dateToCommon.' 23:59:59')
            );
            foreach ($saleOrderData as $saleOrderItem) {
                $saleOrderItem = Library\Formater::stdClassToArray($saleOrderItem);

                $saleOrderCreatedAt = Library\Common::convertTimestampToDate($saleOrderItem['data']['createdAt']);
                $totalGmvUsd = Library\CurrencyStat::convertCurrency($saleOrderItem['data']['totalPrice'], $saleOrderCreatedAt, $currencyCode);
                $saleOrderItemArray = current($saleOrderItem)['arrayItem'];
                $this->assocDateSaleOrder[$saleOrderCreatedAt][] = [$totalGmvUsd, $saleOrderItemArray];
            }

            $this->_calculatorSaleOrder($assocDailyDate, 'daily', $shopId);
            $this->_calculatorSaleOrder($assocWeeklyDate, 'weekly', $shopId);
            $this->_calculatorSaleOrder($assocMonthlyDate, 'monthly', $shopId);

            $message = PHP_EOL.'Done save Sale Order Shop channel id: '.$shopChannelId.' - Shop channel name: '.$shopChannelName.' - Country: '.$ventureName;
            Library\Common::showMessage($message);
        }

        $this->_calculatorSaleOrderTotalShop($this->assocDailyTotalSaleOrder, 'daily');
        $this->_calculatorSaleOrderTotalShop($this->assocWeeklyTotalSaleOrder, 'weekly');
        $this->_calculatorSaleOrderTotalShop($this->assocMonthlyTotalSaleOrder, 'monthly');

        Library\Common::showMessage(PHP_EOL.PHP_EOL.PHP_EOL.'Done save Sale Order all shop'.PHP_EOL);

        Library\Common::showMessage('End '.Library\Common::getCurrentDatetime().PHP_EOL);

    }

    /**
     * build Assoc Date
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    private function _buildAssocDate($dateFrom, $dateTo)
    {
        $assocDailyDate = $assocWeeklyDate = $assocMonthlyDate = $yearMonthArray = [];

        if ( ! $dateFrom || ! $dateTo) {
            $today = date('Y-m-d');
            $yesterday = Library\Common::convertTimestampToDate(strtotime("{$today} -1 days"));
            $assocDailyDate[$yesterday] = [$yesterday];

            $this->dateFromCommon = $yesterday;
            $this->dateToCommon = $yesterday;

            $timestampYesterday = strtotime($yesterday);
            $year = date('Y', $timestampYesterday);
            $month = date('m', $timestampYesterday);

            $dayNumber = date('d'); # 01 to 31
            $dayText = date('D'); # Mon through Sun
            if ($dayText == 'Mon') {
                $dateInWeekArray = [$yesterday];
                for ($i = 1; $i < 7; $i ++) {
                    array_unshift($dateInWeekArray, date('Y-m-d', strtotime($yesterday.'- '.$i.' days')));
                }

                foreach ($dateInWeekArray as $dateInWeek) {
                    $timestampDateWeek = strtotime($dateInWeek);

                    $_weekly = date('W', $timestampDateWeek);
                    $_month = date('m', $timestampDateWeek);
                    $_year = date('Y', $timestampDateWeek);

                    if ($_weekly == '01' && $_month == '12') {
                        $_weekly = '53';
                    }
                    $yearWeek = $_year.'-w'.$_weekly;
                    $assocWeeklyDate[$yearWeek][] = $dateInWeek;
                }
                $this->dateFromCommon = current($dateInWeekArray);
            }

            if ($dayNumber == '01') {
                $assocMonthlyDate[$year.'-'.$month] = Library\Common::dayOfBetween(
                    date('Y-m-01', $timestampYesterday), date('Y-m-t', $timestampYesterday)
                );

                $this->dateFromCommon = date('Y-m-01', $timestampYesterday);
                $this->dateToCommon = date('Y-m-t', $timestampYesterday);
            }
        } else {
            $dateArray = Library\Common::dayOfBetween($dateFrom, $dateTo);
            $thisDay = date('Y-m-d');
            $thisYearMonth = date('Y-m');

            $thisYear = date('Y');
            $thisMonth = date('m');
            $thisWeekly = date('W');

            if ($thisWeekly == '01' && $thisMonth == '12') {
                $thisWeekly = '53';
            }

            $thisYearWeek = $thisYear.'-w'.$thisWeekly;

            foreach ($dateArray as $date) {
                $timestampDate = strtotime($date);

                $assocDailyDate[$date][] = $date;

                $year = date('Y', $timestampDate);
                $month = date('m', $timestampDate);
                $weekly = date('W', $timestampDate);

                if ($weekly == '01' && $month == '12') {
                    $weekly = '53';
                }

                $yearWeek = $year.'-w'.$weekly;
                $assocWeeklyDate[$yearWeek][] = $date;

                $yearMonth = date('Y-m', $timestampDate);

                if ( ! in_array($yearMonth, $yearMonthArray)) {
                    $yearMonthArray[] = $yearMonth;
                    $assocMonthlyDate[$yearMonth] = Library\Common::dayOfBetween(date('Y-m-01', $timestampDate), date('Y-m-t', $timestampDate));
                }
            }
            $this->dateFromCommon = $dateFrom;
            $this->dateToCommon = $dateTo;
        }

        return [$assocDailyDate, $assocWeeklyDate, $assocMonthlyDate];
    }

    /**
     * _calculator Sale Order
     * @param array $assocDate
     * @param string $typeReport
     * @param int $shopId
     */
    private function _calculatorSaleOrder($assocDate, $typeReport, $shopId)
    {
        foreach ($assocDate as $keyText => $dateArray) {
            $totalGmvUsd = 0;
            $totalItemSold = 0;
            $totalSaleOrder = 0;
            foreach ($dateArray as $date) {
                $saleOrderArray = $this->assocDateSaleOrder[$date] ?? [];
                $totalSaleOrder += count($saleOrderArray);
                $totalGmvUsd += array_sum(array_column($saleOrderArray, 0));

                $saleOrderItemArray = array_column($saleOrderArray, 1);
                foreach ($saleOrderItemArray as $saleOrderItem) {
                    $totalItemSold += count($saleOrderItem);
                }
            }

            if ($typeReport == 'daily') {
                $this->assocDailyTotalSaleOrder[$keyText][] = [$totalGmvUsd, $totalItemSold, $totalSaleOrder];
            } elseif ($typeReport == 'weekly') {
                $this->assocWeeklyTotalSaleOrder[$keyText][] = [$totalGmvUsd, $totalItemSold, $totalSaleOrder];
            } else {
                $this->assocMonthlyTotalSaleOrder[$keyText][] = [$totalGmvUsd, $totalItemSold, $totalSaleOrder];
            }

            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_GMV_SO_USD,
                $totalGmvUsd,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_ITEM_SOLD_SO,
                $totalItemSold,
                $keyText,
                $shopId
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_ORDER_SO,
                $totalSaleOrder,
                $keyText,
                $shopId
            );
        }
    }

    /**
     * _calculator Sale Order Total Shop
     * @param array $assocKeyDateSaleOrder
     * @param string $typeReport
     */
    private function _calculatorSaleOrderTotalShop($assocKeyDateSaleOrder, $typeReport)
    {
        foreach ($assocKeyDateSaleOrder as $keyText => $saleOrderData) {
            $totalGmvUsd = array_sum(array_column($saleOrderData, 0));
            $totalItemSold = array_sum(array_column($saleOrderData, 1));
            $totalSaleOrder = array_sum(array_column($saleOrderData, 2));

            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_GMV_SO_ALL_SHOP_USD,
                $totalGmvUsd,
                $keyText
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_ITEM_SOLD_SO_ALL_SHOP,
                $totalItemSold,
                $keyText
            );
            MetricsTrackerReport::save(
                $typeReport.MetricsTrackerReport::METRIC_TOTAL_ORDER_SO_ALL_SHOP,
                $totalSaleOrder,
                $keyText
            );
        }
    }

}
