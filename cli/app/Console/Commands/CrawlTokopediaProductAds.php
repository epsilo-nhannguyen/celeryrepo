<?php

namespace App\Console\Commands;

use App\Library\QueueName;
use App\Models;
use Illuminate\Console\Command;
use App\Jobs;
use App\Library;

class CrawlTokopediaProductAds extends Command
{
    /**
     * @uses:
     * php artisan crawler-tokopedia-product-ads --shop=shop-channel-id
     * example: php artisan crawler-tokopedia-product-ads --shop=590
     */

    protected $signature = 'crawler-tokopedia-product-ads {--shop=}';
    protected $description = 'Crawl product ads from Tokopedia to MySQL database (with out shop channel id will crawl all tokopedia shop)';

    public function handle()
    {
        // Read input params
        $shopId = $this->option('shop');

        // Check input and push queue
        if (!$shopId) {
            $allTokopediaShop = Models\ShopChannel::getAllShopChannelIsTokopedia();
            foreach ($allTokopediaShop as $tokopediaShop) {
                $crawlCredential = $this->checkLoginTokopediaService($tokopediaShop->shop_channel_api_credential);
                if (!$crawlCredential) {
                    Library\LogError::getInstance()->slack('Job CrawlTokopediaProductAds - Tokopedia - Shop Channel Id: ' . $tokopediaShop->shop_channel_id . ' - Login failure');
                    continue;
                }
                $jobCrawlTokopediaProductAds = new Jobs\CrawlTokopediaProductAds($tokopediaShop->shop_channel_id, $crawlCredential);
                dispatch($jobCrawlTokopediaProductAds->onQueue(QueueName::getByClass(Jobs\CrawlTokopediaProductAds::class)));
            }
        } else {
            $getShopChannelInfo = Models\ShopChannel::getById($shopId);
            $crawlCredential = $this->checkLoginTokopediaService($getShopChannelInfo->shop_channel_api_credential);
            if (!$crawlCredential) {
                Library\LogError::getInstance()->slack('Job CrawlTokopediaProductAds - Tokopedia - Crawl group ads - Shop Channel Id: ' . $shopId . ' - Login failure');
                return null;
            }
            $jobCrawlTokopediaProductAds = new Jobs\CrawlTokopediaProductAds($shopId, $crawlCredential);
            dispatch($jobCrawlTokopediaProductAds->onQueue(QueueName::getByClass(Jobs\CrawlTokopediaProductAds::class)));
        }
    }

    /**
     * @param $shopChannelCredential
     * @return mixed
     */
    private function checkLoginTokopediaService($shopChannelCredential)
    {
        // Verify credential
        $shopChannelCredential = json_decode($shopChannelCredential, true);
        if (!isset($shopChannelCredential['shopId'])) {
            return null;
        }
        if (!isset($shopChannelCredential['tkpdUserId'])) {
            return null;
        }
        if (!isset($shopChannelCredential['cookieString'])) {
            return null;
        }
        return $shopChannelCredential;
    }
}