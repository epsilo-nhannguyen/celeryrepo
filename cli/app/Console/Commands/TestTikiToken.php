<?php

namespace App\Console\Commands;

use App\Library\Crawler\Channel\TikiAuthentication;
use Illuminate\Console\Command;

date_default_timezone_set('Asia/Ho_Chi_Minh');

class TestTikiToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test-tiki-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Tiki token';

    public function handle()
    {
        $tkAuth = new TikiAuthentication('biorevietnam@aad.asia', 'szdsXi7U', 1);
        $data = $tkAuth->checkSellerCenterToken();
    }
}