<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 28/09/2019
 * Time: 17:47
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use Illuminate\Console\Command;
use Exception;


class ShopChannelBalance extends Command
{
    /**
     * @uses php artisan shop-channel-balance --shop=shop-id --channel=channel-id
     * example: php artisan shop-channel-balance --shop=62 --channel=3
     */
    protected $signature = 'shop-channel-balance {--shop=} {--channel=}';

    protected $description = 'Raw balance from Shopee to mysql database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        Library\Common::setTimezoneByChannelId($channelId);

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];
        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        if ( ! $isLogin) $isLogin = $crawler->login();
        if ( ! $isLogin) $isLogin = $crawler->login();
        if ( ! $isLogin) $isLogin = $crawler->login();

        $balance = $crawler->getBalance();
        if ($balance === null) return;

        Models\ShopChannelBalance::import($shopChannelId, $balance);

    }
}