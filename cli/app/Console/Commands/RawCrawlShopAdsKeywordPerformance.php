<?php

namespace App\Console\Commands;

use App\Services\ProgrammaticShopeeShopAds;
use Illuminate\Console\Command;
use App\Library;
use App\Models;
use Exception;

class RawCrawlShopAdsKeywordPerformance extends Command
{
    /**
     * @uses:
     * php artisan raw-crawler-shop-ads-keyword-performance --shop=shop-id --channel=channel-id --dateFrom=date-from --dateTo=date-to
     * example: php artisan raw-crawler-shop-ads-keyword-performance --shop=21 --channel=3 --dateFrom=2019-12-18 --dateTo=2019-12-19
     */

    protected $signature = 'raw-crawler-shop-ads-keyword-performance {--shop=} {--channel=} {--dateFrom=} {--dateTo=}';
    protected $description = 'Raw shop ads keyword performance from Shopee to MySQL database';

    public function handle()
    {
        $isHaveTime = false;
        $hour = null;
        $today = date('Y-m-d');
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');
        $dateFrom = $this->option('dateFrom') ?? $today;
        $tempFrom = explode(" ", $dateFrom);
        $dateTo = $this->option('dateTo');
        if (count($tempFrom) > 1)
            $isHaveTime = true;
        if ($isHaveTime)
            $hour = date('H', $dateFrom);

        if (!$shopId || !$channelId)
        {
            $getAllShopChannelRunShopAds = Models\ShopChannel::getAllShopActive_channelShopee_andOrganization();
            foreach ($getAllShopChannelRunShopAds as $shopRunShopAds) {
                $shopId = $shopRunShopAds->fk_shop_master;
                $channelId = $shopRunShopAds->fk_channel;
                try {
                    $this->runWithOption($shopId, $channelId, $dateFrom, $dateTo, $hour);
                } catch (Exception $e) {
                    Library\LogError::logErrorToTextFile($e);
                    continue;
                }
            }
        }
        else {
            $this->runWithOption($shopId, $channelId, $dateFrom, $dateTo, $hour);
        }

    }

    private function runWithOption($shopId, $channelId, $dateFrom, $dateTo, $hour)
    {
        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if (!$shopChannelInfo) {
            return;
        }

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));

        if ($channelInfo[Models\Channel::COL_CHANNEL_CODE] != Models\Channel::SHOPEE_CODE)
        {
            return;
        }

        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];
        Library\Common::setTimezoneByVentureId($ventureId);
        $dateTo = $dateTo ?? date('Y-m-d', strtotime($dateFrom . ' +1 days'));
        $dateFrom = strtotime($dateFrom);
        $dateTo = strtotime($dateTo);
        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];

        $shopAdsInfo = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAds::getByShopChannelId($shopChannelId));
        if (!$shopAdsInfo) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                "Error" => 'Shop ads info not found',
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $shopAdsKeywordId = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAdsKeyword::getByShopAdsId($shopAdsInfo[Models\ModelBusiness\ShopAds::COL_ID]));
        $listAdsKeyword = [];
        foreach ($shopAdsKeywordId as $keyword) {
            $listAdsKeyword[$keyword[Models\ModelBusiness\ShopAdsKeyword::COL_KEYWORD_NAME]] = $keyword['id'];
        }

        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $crawler->login();
        if (!$isLogin) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                "Error" => 'Login failure!!!',
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }

        $crawlData = $crawler->crawlShopAdsKeywordPerformance($shopAdsInfo[Models\ModelBusiness\ShopAds::COL_ADSID], $dateFrom, $dateTo);
        if (!$crawlData)
            $crawlData = $crawler->crawlShopAdsKeywordPerformance($shopAdsInfo[Models\ModelBusiness\ShopAds::COL_ADSID], $dateFrom, $dateTo);
        if (!$crawlData)
            $crawlData = $crawler->crawlShopAdsKeywordPerformance($shopAdsInfo[Models\ModelBusiness\ShopAds::COL_ADSID], $dateFrom, $dateTo);

        if (!$crawlData || $crawlData['err_code'] != 0) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                "Error" => 'Channel response with null or error',
                "Data" => $crawlData
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }
        if (!isset($crawlData['data'])) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                "Error" => 'Channel change params response',
                "Data" => $crawlData
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }

        foreach ($crawlData['data'] as $keywordPerformance) {
            $adsKeywordId = $listAdsKeyword[$keywordPerformance['keyword']] ?? null;
            // Import or update shop ads keyword performance intraday
            if ($hour) {
                Library\LogError::logErrorToTextFile('Intraday not provide');
                return;
//                if (!$adsKeywordId) {
//                    // Import shop ads keyword performance intraday
//                    $recordInsert = Models\ModelBusiness\ShopAdsDataPerformanceIntraday::buildDataInsert(
//                        $shopAdsInfo[Models\ModelBusiness\ShopAds::COL_ID] ?? null,
//                        $adsKeywordId,
//                        $keywordPerformance['keyword'],
//                        $dateFrom,
//                        $hour,
//                        $keywordPerformance['order_gmv'],
//                        $keywordPerformance['cost'],
//                        $keywordPerformance['order'],
//                        $keywordPerformance['shop_item_click'],
//                        $keywordPerformance['order_amount'],
//                        $keywordPerformance['impression'],
//                        $keywordPerformance['click'],
//                        $keywordPerformance
//                    );
//                    Models\ModelBusiness\ShopAdsDataPerformanceIntraday::insertData($recordInsert);
//                } else {
//                    $keywordPerformanceIntradayByDateAndHour = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAdsDataPerformanceIntraday::searchByShopAdsKeywordIdAndDateHour($adsKeywordId, $dateFrom, $hour));
//                    if ($keywordPerformanceIntradayByDateAndHour) {
//                        $recordUpdate = Models\ModelBusiness\ShopAdsDataPerformanceIntraday::buildDataUpdate(
//                            $keywordPerformance['order_gmv'],
//                            $keywordPerformance['cost'],
//                            $keywordPerformance['order'],
//                            $keywordPerformance['shop_item_click'],
//                            $keywordPerformance['order_amount'],
//                            $keywordPerformance['impression'],
//                            $keywordPerformance['click'],
//                            $keywordPerformance
//                        );
//                        Models\ModelBusiness\ShopAdsDataPerformanceIntraday::updateData($keywordPerformanceIntradayByDateAndHour[Models\ModelBusiness\ShopAdsKeywordPerformance::COL_ID], $recordUpdate);
//                    } else {
//                        // Import shop ads keyword performance
//                        $recordInsert = Models\ModelBusiness\ShopAdsDataPerformanceIntraday::buildDataInsert(
//                            $shopAdsInfo[Models\ModelBusiness\ShopAds::COL_ID] ?? null,
//                            $adsKeywordId,
//                            $keywordPerformance['keyword'],
//                            $dateFrom,
//                            $hour,
//                            $keywordPerformance['order_gmv'],
//                            $keywordPerformance['cost'],
//                            $keywordPerformance['order'],
//                            $keywordPerformance['shop_item_click'],
//                            $keywordPerformance['order_amount'],
//                            $keywordPerformance['impression'],
//                            $keywordPerformance['click'],
//                            $keywordPerformance
//                        );
//                        Models\ModelBusiness\ShopAdsDataPerformanceIntraday::insertData($recordInsert);
//                    }
//                }
            } else {
                // Shop ads keyword performance
                $keywordPerformanceByDate = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAdsKeywordPerformance::searchByShopAdsKeywordNameAndDate($keywordPerformance['keyword'], $dateFrom, $shopAdsInfo[Models\ModelBusiness\ShopAds::COL_ID]));
                if ($keywordPerformanceByDate) {
                    $recordUpdate = Models\ModelBusiness\ShopAdsKeywordPerformance::buildDataUpdate(
                        $adsKeywordId ?? null,
                        $keywordPerformance['order_gmv'],
                        $keywordPerformance['cost'],
                        $keywordPerformance['order'],
                        $keywordPerformance['shop_item_click'],
                        $keywordPerformance['order_amount'],
                        $keywordPerformance['impression'],
                        $keywordPerformance['click'],
                        $keywordPerformance
                    );
                    $isUpdateSAKPSuccess = Models\ModelBusiness\ShopAdsKeywordPerformance::updateData($keywordPerformanceByDate[Models\ModelBusiness\ShopAdsKeywordPerformance::COL_ID], $recordUpdate);
                    if (!$isUpdateSAKPSuccess) {
                        $dataLog = [
                            "ShopChannelId" => $shopChannelId,
                            "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                            "Error" => 'Update Shop ads keyword performance failure!!!',
                            "Data" => $recordUpdate
                        ];
                        Library\LogError::logErrorToTextFile($dataLog);
//                        throw new Exception('Update Shop ads keyword performance failure!!!');
                    }
                } else {
                    // Import shop ads keyword performance
                    $recordInsert = Models\ModelBusiness\ShopAdsKeywordPerformance::buildDataInsert(
                        $shopAdsInfo[Models\ModelBusiness\ShopAds::COL_ID] ?? null,
                        $adsKeywordId ?? null,
                        $keywordPerformance['keyword'],
                        $dateFrom,
                        $keywordPerformance['order_gmv'],
                        $keywordPerformance['cost'],
                        $keywordPerformance['order'],
                        $keywordPerformance['shop_item_click'],
                        $keywordPerformance['order_amount'],
                        $keywordPerformance['impression'],
                        $keywordPerformance['click'],
                        $keywordPerformance,
                        bin2hex($keywordPerformance['keyword'])
                    );
                    $isInsertSAKPSuccess = Models\ModelBusiness\ShopAdsKeywordPerformance::insertData($recordInsert);
                    if(!$isInsertSAKPSuccess){
                        $dataLog = [
                            "ShopChannelId" => $shopChannelId,
                            "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                            "Error" => 'Insert Shop ads keyword performance failure!!!',
                            "Data" => $recordInsert
                        ];
                        Library\LogError::logErrorToTextFile($dataLog);
//                        throw new Exception('Insert Shop ads keyword performance failure!!!');
                    }
                }
                # programmatic shop keyword
                ProgrammaticShopeeShopAds::turnStatusShopAdsKeyword($shopChannelId);
                ProgrammaticShopeeShopAds::changeBiddingPrice($shopChannelId);
            }
        }

        // Import or update shop ads performance
        if (!$hour) {
            $hasShopPerformance = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopDataPerformance::getByShopChannelIdAndDate($shopChannelId, $dateFrom));
            if (!$hasShopPerformance) {
                $getSumData = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAdsKeywordPerformance::sumPerformanceByDate($shopChannelId, $dateFrom));
                $buildDataInsert = null;
                if (!$getSumData) {
                    $buildDataInsert = Models\ModelBusiness\ShopDataPerformance::buildDataInsert(
                        $shopChannelId,
                        $dateFrom,
                        0,
                        0,
                        0,
                        0,
                        0
                    );
                } else {
                    $buildDataInsert = Models\ModelBusiness\ShopDataPerformance::buildDataInsert(
                        $shopChannelId,
                        $dateFrom,
                        $getSumData['total_view'],
                        $getSumData['total_click'],
                        $getSumData['total_sold'],
                        $getSumData['total_gmv'],
                        $getSumData['total_expense']
                    );
                }
                $isInsertSDPSuccess = Models\ModelBusiness\ShopDataPerformance::insertData($buildDataInsert);
                if (!$isInsertSDPSuccess) {
                    $dataLog = [
                        "ShopChannelId" => $shopChannelId,
                        "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                        "Error" => 'Insert shop data performance failure!!!',
                        "Data" => $buildDataInsert
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
//                    throw new Exception('Insert shop data performance failure!!!');
                }
            } else {
                $getSumData = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAdsKeywordPerformance::sumPerformanceByDate($shopChannelId, $dateFrom));
                if (!$getSumData) {
                    $dataLog = [
                        "ShopChannelId" => $shopChannelId,
                        "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                        "Error" => 'Bug - Have performance in date but no data'
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
//                    throw new Exception('Bug - Have performance in date but no data');
                } else {
                    $buildDataUpdate = Models\ModelBusiness\ShopDataPerformance::buildDataUpdate(
                        $getSumData['total_view'],
                        $getSumData['total_click'],
                        $getSumData['total_sold'],
                        $getSumData['total_gmv'],
                        $getSumData['total_expense']
                    );
                    $isUpdateSDPSuccess = Models\ModelBusiness\ShopDataPerformance::updateData($hasShopPerformance[Models\ModelBusiness\ShopDataPerformance::COL_ID], $buildDataUpdate);
                    if (!$isUpdateSDPSuccess) {
                        $dataLog = [
                            "ShopChannelId" => $shopChannelId,
                            "TimeFrom" => Library\Formater::timeStampToDate($dateFrom),
                            "Error" => 'Update shop data performance failure!!!',
                            "Data" => $buildDataUpdate
                        ];
                        Library\LogError::logErrorToTextFile($dataLog);
//                        throw new Exception('Update shop data performance failure!!!');
                    }
                }
            }
            # programmatic shop
            ProgrammaticShopeeShopAds::turnStatusShop($shopChannelId);
        }
    }
}