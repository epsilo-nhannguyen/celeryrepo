<?php

namespace App\Console\Commands;

use App\Library;
use App\Models;
use App\Jobs;
use App\Models\Mongo\Manager;
use Illuminate\Console\Command;
use Exception;

class RawKeywordSuggestShopee extends Command
{
    /**
     * @uses php artisan raw-keyword-suggest-shopee --shop= shopId --channel= channelId
     * example: php artisan raw-keyword-suggest-shopee --shop 16 --channel 3
     */
    protected $signature = 'raw-keyword-suggest-shopee {--shop=} {--channel=}';

    protected $description = 'Raw keyword product from Shopee to mysql database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];

        $shopMasterInfo = Library\Formater::stdClassToArray(Models\ShopMaster::getById($shopId));
        $organizationId = $shopMasterInfo[Models\ShopMaster::COL_FK_ORGANIZATION];

        Library\Common::setTimezoneByVentureId($ventureId);

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];

        $message = json_encode([
//            'shopId' => $shopId,
//            'channelId' => $channelId,
            'shopChannelId' => $shopChannelId,
            'ventureId' => $ventureId,
            'organizationId' => $organizationId,
        ]);

        dispatch((new Jobs\RawKeywordSuggestShopee($message))->onQueue(Library\QueueName::getByClass(Jobs\RawKeywordSuggestShopee::class)));
    }
}