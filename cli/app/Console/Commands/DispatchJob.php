<?php

namespace App\Console\Commands;

use App\Jobs\Job;
use Exception;
use Illuminate\Console\Command;

class DispatchJob extends Command
{
    protected $description = 'Dispatch job';
    protected $signature = 'job:dispatch {job} {params?*}';

    public function handle()
    {
        /** @var Job $class */
        $class = $this->argument('job');
        $defaultNamespace = '\\App\\Jobs\\';
        if (!class_exists($class)) {
            $oldClass = $class;
            $class =  $defaultNamespace . strval($class);
            if (!class_exists($class)) {
                throw new Exception("$oldClass and $class classes do not exist.");
            }
        }
        $parameters = $this->argument('params') ?? [];
        $job = new $class(...$parameters);
        $this->dispatch($job);
    }

    /**
     * @param $job Job
     */
    protected function dispatch($job)
    {
        $job->handle();
    }
}