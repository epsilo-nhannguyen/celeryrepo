<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Celery;

class CeleryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test-celery';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Celery';

    public function handle()
    {
//        $celery = new Celery('localhost', 'guest', 'guest', '/');
//        $celery->PostTask('tasks.processJobInQueue', [5, 15]);
        for ($i = 0; $i < 5; $i++) {
            $c = new Celery('localhost', 'guest', 'guest', '/');
            $c->PostTask('tasks.processJobInQueue', [$i, $i*$i]);
        }
        echo 'Post tasks done!' . PHP_EOL;
    }
}