<?php


namespace App\Console\Commands\Dashboard;

use App\Jobs\Dashboard\Shop\ImportMetricDaily;
use App\Library\QueueName;
use App\Library\UniversalCurrency;
use App\Models;
use App\Services\Dashboard\Shop\Database\Database;
use App\Services\ShopeeKeywordWalletBalance;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CalculateReservedMetric extends Command
{
    protected $signature = 'dashboard:calculate_reserved_metric {--date=} {--arrayShopId=} {--hour=}';

    protected $description = 'Calculate reserved metric {--date=} {--arrayShopId=}';

    public function handle()
    {
        $arrayShopId = $this->option('arrayShopId');
        $hour = $this->option('hour');
        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $allShopChannel = Models\ShopChannel::getAllActive();

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }
        $collectionShopChannelId = $allShopChannel->pluck('shop_channel_id');

        #prepare data
        //$assocVentureIdTimeZone = Models\Venture::getAll()->pluck('venture_timezone', 'venture_id')->all();

        $date = $this->option('date');

        $allCurrencyCode = Models\Currency::getAll()->pluck('currency_code', 'currency_id');
        $assocShopChannelId_channelCode = $allShopChannel->pluck('channel_code', 'shop_channel_id');
        $allVenture = Models\Venture::getAll()->pluck('venture_timezone', 'venture_id');
        foreach ($allShopChannel as $shopChannel) {
            date_default_timezone_set($allVenture[$shopChannel->fk_venture]);

            if (strpos($date, ',') !== false) {
                list($dateFrom, $dateTo) = explode(',', $date);
            } else {
                if (strlen($date)) {
                    $dateFrom = $dateTo = $date;
                } else {
                    $dateFrom = $dateTo = date('Y-m-d');
                }
            }

            $startDate = new Carbon($dateFrom);
            $endDate = new Carbon($dateTo);
            $listDate = [];
            while ($startDate->lte($endDate)) {
                $listDate[] = $startDate->toDateString();

                $startDate->addDay();
            }
            foreach ($listDate as $_date) {
                if ($hour != -1) {
                    $arrayHour = [date('H')];
                } else {
                    if ($_date == date('Y-m-d')) {
                        $collectionHour = collect(range(0, date('H')));
                    } else {
                        $collectionHour = collect(range(0, 23));
                    }
                    $arrayHour = $collectionHour->map(function ($item) {
                        return str_pad($item,2 , '0', STR_PAD_LEFT);
                    });
                }
                $serviceWalletBalance = new ShopeeKeywordWalletBalance($shopChannel->shop_channel_id);
                $walletData = $serviceWalletBalance->getListByDate($_date)->toArray();
                foreach ($arrayHour as $_hour) {
                    $walletValue = $walletData[$_hour] ?? null;
                    $from = sprintf('%s %s:00:00', $_date, $_hour);
                    $to = sprintf('%s %s:59:59', $_date, $_hour);
                    $saleOrderData = Models\Mongo\SaleOrder::getDataSaleOrderDateFromTo(
                        $shopChannel->fk_shop_master, $shopChannel->fk_channel,
                        strtotime($from), strtotime($to)
                    );

                    $saleOrderData = (array) current((array) $saleOrderData);

                    $gmvChannel = floatval($saleOrderData['totalPrice'] ?? 0);
                    $itemSoldChannel = intval($saleOrderData['totalItem'] ?? 0);
                    $assocDataGmv = UniversalCurrency::buildCurrencyData(
                        $gmvChannel,
                        date('Y-m', strtotime($from)),
                        $shopChannel->fk_venture
                    );
                    $assocDataWallet = [];
                    if ($walletValue !== null) {
                        $assocDataWallet = UniversalCurrency::buildCurrencyData(
                            $walletValue,
                            date('Y-m', strtotime($from)),
                            $shopChannel->fk_venture
                        );
                    }

                    foreach ($allCurrencyCode as $currencyId => $currencyCode) {
                        $dataPushed = [];

                        $dataPushed[$_date][$_hour] = [
                            'total_gmv' => $assocDataGmv[$currencyId] ?? 0,
                            'wallet_balance' => $assocDataWallet[$currencyId] ?? null,
                            'total_item_sold' => $itemSoldChannel,
                        ];

                        $job = new ImportMetricDaily($shopChannel->shop_channel_id, null, $assocShopChannelId_channelCode[$shopChannel->shop_channel_id], $dataPushed, $currencyCode);
                        dispatch($job->onQueue(QueueName::CALCULATE_DASHBOARD_SHOP_INTRADAY_1));
                    }
                }
            }
        }

    }
}