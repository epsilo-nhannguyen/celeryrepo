<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:42
 */

namespace App\Console\Commands\Dashboard\ShopAds;


use Illuminate\Console\Command;
use App\Library;
use App\Models;
use App\Services;
use Exception;


class ShopAdsFetchChartKeywordMetric extends Command
{
    /**
     * @uses: php artisan shop-ads-fetch-chart-keyword-metric --shop=shop-id --channel=channel-id --dateFrom=date-from --dateTo=date-to --paddingDate=padding-date
     * example: php artisan shop-ads-fetch-chart-keyword-metric --shop=62 --channel=3 --dateFrom=2019-09-01 --dateTo=2019-09-30 --paddingDate=60
     */

    protected $signature = 'shop-ads-fetch-chart-keyword-metric {--shop=} {--channel=} {--dateFrom=} {--dateTo=} {--paddingDate=}';

    protected $description = 'ShopAds: Fetch data to chart keyword metric';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');
        $paddingDate = $this->option('paddingDate');

        $isRunAgain = $dateFrom || $dateTo || $paddingDate;

        if ($paddingDate) {
            $yesterday = Library\Common::convertTimestampToDate(strtotime("{$currentDate} -1 days"));
            $dateArray = [$yesterday, $currentDate];
        } else {
            $dateArray = Library\Common::dayOfBetween($dateFrom, $dateTo, '+1 day');
        }

        $allShopChannel = Models\ShopChannel::getAllShopActive_channelShopee();

        foreach ($allShopChannel as $shopChannel) {
            if ($shopId && $shopChannel->fk_shop_master != $shopId) {
                continue;
            }
            if ($channelId && $shopChannel->fk_channel != $channelId) {
                continue;
            }
            $ventureId = $shopChannel->{Models\Channel::COL_FK_VENTURE};
            Library\Common::setTimezoneByVentureId($ventureId);

            $shopChannelId = $shopChannel->{Models\ShopChannel::COL_SHOP_CHANNEL_ID};

            # begin chart keyword campaign metric
            $campaignMetric = new Services\ShopAdsChartKeywordCampaignMetric();
            $campaignMetric->setShopChannelId($shopChannelId)
                ->setShopId($shopId)
                ->setChannelId($channelId)
                ->setVentureId($ventureId)
                ->setDateArray($dateArray)
                ->setIsRunAgain($isRunAgain);
            $campaignMetric->execute();
            # end chart keyword campaign metric
        }
    }
}