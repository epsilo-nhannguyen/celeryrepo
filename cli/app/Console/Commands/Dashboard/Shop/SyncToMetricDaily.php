<?php


namespace App\Console\Commands\Dashboard\Shop;


use App\Jobs\Dashboard\Shop\FetchDataMetricDaily;
use App\Library\QueueName;
use App\Models;
use App\Services\Dashboard\Shop\Database\Database;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class SyncToMetricDaily extends Command
{
    protected $signature = 'dashboard:shop:sync_to_metric_daily {--arrayShopId=} {--date=}';

    protected $description = 'Dashboard - Shop : Sync to metric daily {--arrayShopId=} {--date=}';

    public function handle()
    {
        $arrayShopId = $this->option('arrayShopId');

        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $allShopChannel = Models\ShopChannel::getAllActive();

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }

        $collectionShopChannelId = $allShopChannel->pluck('shop_channel_id');

        #prepare data
        //$assocVentureIdTimeZone = Models\Venture::getAll()->pluck('venture_timezone', 'venture_id')->all();

        $date = $this->option('date');

        if (strpos($date, ',') !== false) {
            list($dateFrom, $dateTo) = explode(',', $date);
        } else {
            if (strlen($date)) {
                $dateFrom = $dateTo = $date;
            } else {
                $dateFrom = $dateTo = date('Y-m-d');
            }
        }

        $startDate = new Carbon($dateFrom);
        $endDate = new Carbon($dateTo);
        $listDate = [];
        while ($startDate->lte($endDate)) {
            $listDate[] = $startDate->toDateString();

            $startDate->addDay();
        }

        $collectionShopChannelIdChunked = $collectionShopChannelId->chunk(10);

        foreach ($collectionShopChannelIdChunked as $chunk) {
            /** @var Collection $chunk */
            foreach ($listDate as $dateInLoop) {
                foreach (Database::getAllConnection() as $connection) {
                    $job = new FetchDataMetricDaily($chunk->all(), $dateInLoop, $connection);
                    dispatch($job->onQueue(QueueName::CALCULATE_DASHBOARD_SHOP_DAILY));
                }
            }
        }
    }
}