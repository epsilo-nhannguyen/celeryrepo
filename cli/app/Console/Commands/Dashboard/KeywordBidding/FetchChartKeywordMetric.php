<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:42
 */

namespace App\Console\Commands\Dashboard\KeywordBidding;


use Illuminate\Console\Command;
use App\Library;
use App\Models;
use App\Services;
use Exception;


class FetchChartKeywordMetric extends Command
{
    /**
     * @uses: php artisan fetch-chart-keyword-metric --shop=shop-id --channel=channel-id --dateFrom=date-from --dateTo=date-to --paddingDate=padding-date
     * example: php artisan fetch-chart-keyword-metric --shop=62 --channel=3 --dateFrom=2019-09-01 --dateTo=2019-09-30 --paddingDate=60
     */

    protected $signature = 'fetch-chart-keyword-metric {--shop=} {--channel=} {--dateFrom=} {--dateTo=} {--paddingDate=} {--forceIntraday=} {--dateBeginForecast=}';

    protected $description = 'Fetch data to chart keyword metric';

    /**
     * @var bool
     */
    private $isRunAgain = false;

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId || ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Models\ShopChannel::getByShopIdChannelId($shopId, $channelId);

        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $channelInfo = Models\Channel::getById($channelId);
        if ($channelInfo->{Models\Channel::COL_CHANNEL_CODE} != Models\Channel::SHOPEE_CODE) return;

        $ventureId = $channelInfo->{Models\Channel::COL_FK_VENTURE};
        Library\Common::setTimezoneByVentureId($ventureId);

        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');
        $paddingDate = $this->option('paddingDate');
        $forceIntraday = $this->option('forceIntraday');
        $dateBeginForecast = $this->option('dateBeginForecast');

        $this->isRunAgain = $dateFrom || $dateTo || $paddingDate;

        $now = Library\Common::getCurrentTimestamp();

        $currentDate = Library\Common::convertTimestampToDate($now);
        $currentTime = Library\Common::convertTimestampToTime($now);
        $cycleRawPaidAds = env('PAID_ADS_NUMBER_MINUTE_CYCLE_RAW', 10);
        $hourCutoff = env('PAID_ADS_HOUR_CUTOFF', 3); # from 0 to 23 hour
        $hourCutoff = str_pad($hourCutoff, 2, 0, STR_PAD_LEFT);

        $isTimeCutOff = $currentTime >= $hourCutoff.':00:00'
            && $currentTime <= $hourCutoff.':'.str_pad($cycleRawPaidAds - 1, 2, 0, STR_PAD_LEFT).':59';

        $dateFrom = $dateFrom ?? $currentDate;
        $dateTo = $dateTo ?? $currentDate;

        if ($paddingDate) {
            $dateFrom = Library\Common::convertTimestampToDate(strtotime("{$dateFrom} -{$paddingDate} days"));
        }

        if ( ! ($isTimeCutOff && ! $this->isRunAgain)) {
            $dateArray = Library\Common::dayOfBetween($dateFrom, $dateTo, '+1 day');
        } else {
            $yesterday = Library\Common::convertTimestampToDate(strtotime("{$currentDate} -1 days"));
            $dateArray = [$yesterday, $currentDate];
        }

        $shopChannelId = $shopChannelInfo->{Models\ShopChannel::COL_SHOP_CHANNEL_ID};
        $shopChannelCreatedAt = $shopChannelInfo->{Models\ShopChannel::COL_SHOP_CHANNEL_CREATED_AT};

        # begin chart keyword campaign metric
        $campaignMetric = new Services\ChartKeywordCampaignMetric();
        $campaignMetric->setShopChannelId($shopChannelId)->setShopId($shopId)->setChannelId($channelId)
            ->setVentureId($ventureId)->setDateArray($dateArray)->setIsRunAgain($this->isRunAgain);
        $campaignMetric->execute();
        # end chart keyword campaign metric

        # begin chart keyword campaign metric intraday

        #using for get data begin 2020
        if (!$dateBeginForecast) {
            $dateBeginForecast = '2020-01-01';
        }
        if ($forceIntraday) {
            $this->isRunAgain = 1;
        }
        $campaignMetricIntraday = new Services\ChartKeywordCampaignMetricIntraday();
        $campaignMetricIntraday->setShopChannelId($shopChannelId)->setShopId($shopId)->setChannelId($channelId)
            ->setVentureId($ventureId)->setDateArray($dateArray)->setIsRunAgain($this->isRunAgain)->setShopChannelCreatedAt($shopChannelCreatedAt);
        $campaignMetricIntraday->setForceIntraday($forceIntraday);
        $campaignMetricIntraday->setDateBeginForecast($dateBeginForecast);
        $campaignMetricIntraday->execute();
        # end chart keyword campaign metric intraday

        # begin chart keyword sku metric
        /*$skuMetric = new Services\ChartKeywordSkuMetric();
        $skuMetric->setShopChannelId($shopChannelId)->setShopId($shopId)->setChannelId($channelId)->setDateArray($dateArray);
        $skuMetric->execute();*/
        # end chart keyword sku metric
    }

}