<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:42
 */

namespace App\Console\Commands\Dashboard\KeywordBidding;


use Illuminate\Console\Command;
use App\Library;
use App\Models;
use Exception;


class FetchChartKeywordAction extends Command
{
    /**
     * @uses: php artisan fetch-chart-keyword-action --shop=shop-id --channel=channel-id --dateFrom=date-from --dateTo=date-to
     * example: php artisan fetch-chart-keyword-action --shop=62 --channel=3 --dateFrom=2019-09-01 --dateTo=2019-09-30
     */

    protected $signature = 'fetch-chart-keyword-action {--shop=} {--channel=} {--dateFrom=} {--dateTo=}';

    protected $description = 'Fetch data to chart keyword action';

    private $actionIdCampaign = [
        Models\MakProgrammaticAction::ADD_NEW_CAMPAIGN,
        Models\MakProgrammaticAction::PAUSE_CAMPAIGN,
        Models\MakProgrammaticAction::RESUME_CAMPAIGN,
    ];

    private $actionIdSku = [
        Models\MakProgrammaticAction::PAUSE_SKU,
        Models\MakProgrammaticAction::RESUME_SKU,
        Models\MakProgrammaticAction::ADD_NEW_SKU,
    ];

    private $actionIdKeyword = [
        Models\MakProgrammaticAction::DEACTIVATE_KEYWORD,
        Models\MakProgrammaticAction::ACTIVATE_KEYWORD,
        Models\MakProgrammaticAction::ADD_NEW_KEYWORD,
    ];

    private $assocActionIdName = [
        Models\MakProgrammaticAction::ADD_NEW_CAMPAIGN => 'Add new Campaign',
        Models\MakProgrammaticAction::PAUSE_CAMPAIGN => 'Pause Campaign',
        Models\MakProgrammaticAction::RESUME_CAMPAIGN => 'Resume Campaign',
        Models\MakProgrammaticAction::PAUSE_SKU => 'Pause SKU',
        Models\MakProgrammaticAction::RESUME_SKU => 'Resume SKU',
        Models\MakProgrammaticAction::ADD_NEW_SKU => 'Add new SKU',
        Models\MakProgrammaticAction::DEACTIVATE_KEYWORD => 'Delete Keyword',
        Models\MakProgrammaticAction::ACTIVATE_KEYWORD => 'Restore Keyword',
        Models\MakProgrammaticAction::ADD_NEW_KEYWORD => 'Add new keyword',
    ];

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');
        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');

        if ( ! $shopId || ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Models\ShopChannel::getByShopIdChannelId($shopId, $channelId);

        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $channelInfo = Models\Channel::getById($channelId);
        if ($channelInfo->{Models\Channel::COL_CHANNEL_CODE} != Models\Channel::SHOPEE_CODE) return;

        $ventureId = $channelInfo->{Models\Channel::COL_FK_VENTURE};
        Library\Common::setTimezoneByVentureId($ventureId);

        $currentDate = Library\Common::convertTimestampToDate(Library\Common::getCurrentTimestamp());

        $shopChannelId = $shopChannelInfo->{Models\ShopChannel::COL_SHOP_CHANNEL_ID};

        $keyCampaignActionInDb = $keyCampaignActionIntradayInDb = [];

        $isRunAgain = $dateFrom && $dateTo;

        if ( ! $isRunAgain) {
            echo PHP_EOL, 'begin get max ' . microtime(), PHP_EOL;
            $maxCreatedAtCampaign = Models\ModelChart\ChartKeywordCampaignAction::getMaxCreatedAtByActionIdArray($shopChannelId, $this->actionIdCampaign);
            $maxCreatedAtSku = Models\ModelChart\ChartKeywordCampaignAction::getMaxCreatedAtByActionIdArray($shopChannelId, $this->actionIdSku);
            $maxCreatedAtKeyword = Models\ModelChart\ChartKeywordCampaignAction::getMaxCreatedAtByActionIdArray($shopChannelId, $this->actionIdKeyword);
            echo 'end get max ' . microtime(), PHP_EOL;
            $dateFromCampaign = $dateFromSku = $dateFromKeyword = $dateFromCommon = Library\Common::dateFrom($currentDate);

            if ($maxCreatedAtCampaign) {
                $dateFromCampaign = $maxCreatedAtCampaign + 1;
            }
            if ($maxCreatedAtSku) {
                $dateFromSku = $maxCreatedAtSku + 1;
            }
            if ($maxCreatedAtKeyword) {
                $dateFromKeyword = $maxCreatedAtKeyword + 1;
            }

            $dateToCommon = Library\Common::dateTo($currentDate);
        } else {
            $dateFromCampaign = $dateFromSku = $dateFromKeyword = $dateFromCommon = Library\Common::dateFrom($dateFrom);
            $dateToCommon = Library\Common::dateTo($dateTo);
            echo PHP_EOL, 'begin get ChartKeywordCampaignAction::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;
            $chartCampaignActionData = Models\ModelChart\ChartKeywordCampaignAction::searchShopChannelIdByDateFromTo($shopChannelId, $dateFromCommon, $dateToCommon);
            foreach ($chartCampaignActionData as $chartCampaignAction) {
                $keyCampaignAction = sprintf(
                    'campaignId%s_actionId%s_createdAt%s',
                    $chartCampaignAction->{Models\ModelChart\ChartKeywordCampaignAction::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID},
                    $chartCampaignAction->{Models\ModelChart\ChartKeywordCampaignAction::COL_MAK_PROGRAMMATIC_ACTION_ID},
                    $chartCampaignAction->{Models\ModelChart\ChartKeywordCampaignAction::COL_CREATED_AT}
                );

                $keyCampaignActionInDb[] = $keyCampaignAction;
            }
            echo 'end get ChartKeywordCampaignAction::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;

            echo PHP_EOL, 'begin get ChartKeywordCampaignAction::searchShopChannelIdByDateFromTo Intraday' . microtime(), PHP_EOL;
            $chartCampaignActionIntradayData = Models\ModelChart\ChartKeywordCampaignActionIntraday::searchShopChannelIdByDateFromTo($shopChannelId, $dateFromCommon, $dateToCommon);
            foreach ($chartCampaignActionIntradayData as $chartCampaignActionIntraday) {
                $keyCampaignActionIntraday = sprintf(
                    'campaignId%s_actionId%s_createdAt%s',
                    $chartCampaignActionIntraday->{Models\ModelChart\ChartKeywordCampaignActionIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID},
                    $chartCampaignActionIntraday->{Models\ModelChart\ChartKeywordCampaignActionIntraday::COL_MAK_PROGRAMMATIC_ACTION_ID},
                    $chartCampaignActionIntraday->{Models\ModelChart\ChartKeywordCampaignActionIntraday::COL_CREATED_AT}
                );

                $keyCampaignActionIntradayInDb[] = $keyCampaignActionIntraday;
            }
            echo 'end get ChartKeywordCampaignAction::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;
        }

        $assocCampaignIdName = array_column(
            Library\Formater::stdClassToArray(Models\MakProgrammaticCampaign::searchByShopChannelId($shopChannelId)),
            Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_NAME,
            Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID
        );

        $campaignActionInsertArray = $campaignActionIntradayInsertArray = [];

        echo PHP_EOL, 'begin get MakProgrammaticLogCampaign::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;
        $actionCampaignData = Models\MakProgrammaticLogCampaign::searchShopChannelIdByDateFromTo($shopChannelId, $dateFromCampaign, $dateToCommon);
        foreach ($actionCampaignData as $actionCampaign) {
            $campaignId = $actionCampaign->{Models\MakProgrammaticLogCampaign::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN};
            $actionId = $actionCampaign->{Models\MakProgrammaticLogCampaign::COL_FK_MAK_PROGRAMMATIC_ACTION};
            $createdAt = $actionCampaign->{Models\MakProgrammaticLogCampaign::COL_MAK_PROGRAMMATIC_LOG_CAMPAIGN_CREATED_AT};
            $keyActionCampaign = sprintf(
                'campaignId%s_actionId%s_createdAt%s',
                $campaignId, $actionId, $createdAt
            );

            if ( ! in_array($keyActionCampaign, $keyCampaignActionInDb)) {
                $campaignActionInsertArray[] = Models\ModelChart\ChartKeywordCampaignAction::buildDataInsert(
                    $shopChannelId, $campaignId, $actionId, Library\Common::convertTimestampToDate($createdAt),
                    $assocCampaignIdName[$campaignId], $this->assocActionIdName[$actionId], $createdAt
                );
            }
            if ( ! in_array($keyActionCampaign, $keyCampaignActionIntradayInDb)) {
                $campaignActionIntradayInsertArray[] = Models\ModelChart\ChartKeywordCampaignActionIntraday::buildDataInsert(
                    $shopChannelId, $campaignId, $actionId, Library\Common::convertTimestampToDate($createdAt),
                    Library\Common::convertTimestampToHour($createdAt), $assocCampaignIdName[$campaignId], $this->assocActionIdName[$actionId], $createdAt
                );
            }
        }
        echo 'end get MakProgrammaticLogCampaign::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;

        echo PHP_EOL, 'begin get MakProgrammaticLogSku::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;
        $actionSkuData = Models\MakProgrammaticLogSku::searchShopChannelIdByDateFromTo($shopChannelId, $dateFromSku, $dateToCommon);
        foreach ($actionSkuData as $actionSku) {
            $campaignId = $actionSku->{Models\MakProgrammaticLogSku::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN};
            $actionId = $actionSku->{Models\MakProgrammaticLogSku::COL_FK_MAK_PROGRAMMATIC_ACTION};
            $createdAt = $actionSku->{Models\MakProgrammaticLogSku::COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_AT};
            $keyActionSku = sprintf(
                'campaignId%s_actionId%s_createdAt%s',
                $campaignId, $actionId, $createdAt
            );

            if ( ! in_array($keyActionSku, $keyCampaignActionInDb)) {
                $campaignActionInsertArray[] = Models\ModelChart\ChartKeywordCampaignAction::buildDataInsert(
                    $shopChannelId, $campaignId, $actionId, Library\Common::convertTimestampToDate($createdAt),
                    $assocCampaignIdName[$campaignId], $this->assocActionIdName[$actionId], $createdAt
                );
            }
            if ( ! in_array($keyActionSku, $keyCampaignActionIntradayInDb)) {
                $campaignActionIntradayInsertArray[] = Models\ModelChart\ChartKeywordCampaignActionIntraday::buildDataInsert(
                    $shopChannelId, $campaignId, $actionId, Library\Common::convertTimestampToDate($createdAt),
                    Library\Common::convertTimestampToHour($createdAt), $assocCampaignIdName[$campaignId], $this->assocActionIdName[$actionId], $createdAt
                );
            }
        }
        echo 'end get MakProgrammaticLogSku::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;

        echo PHP_EOL, 'begin get MakProgrammaticLogKeyword::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;
        $actionKeywordData = Models\MakProgrammaticLogKeyword::searchShopChannelIdByDateFromTo($shopChannelId, $dateFromKeyword, $dateToCommon, $this->actionIdKeyword);
        foreach ($actionKeywordData as $actionKeyword) {
            $campaignId = $actionKeyword->{Models\MakProgrammaticLogKeyword::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN};
            $actionId = $actionKeyword->{Models\MakProgrammaticLogKeyword::COL_FK_MAK_PROGRAMMATIC_ACTION};
            $createdAt = $actionKeyword->{Models\MakProgrammaticLogKeyword::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_AT};
            $keyActionKeyword = sprintf(
                'campaignId%s_actionId%s_createdAt%s',
                $campaignId, $actionId, $createdAt
            );

            if ( ! in_array($keyActionKeyword, $keyCampaignActionInDb)) {
                $campaignActionInsertArray[] = Models\ModelChart\ChartKeywordCampaignAction::buildDataInsert(
                    $shopChannelId, $campaignId, $actionId, Library\Common::convertTimestampToDate($createdAt),
                    $assocCampaignIdName[$campaignId], $this->assocActionIdName[$actionId], $createdAt
                );
            }
            if ( ! in_array($keyActionKeyword, $keyCampaignActionIntradayInDb)) {
                $campaignActionIntradayInsertArray[] = Models\ModelChart\ChartKeywordCampaignActionIntraday::buildDataInsert(
                    $shopChannelId, $campaignId, $actionId, Library\Common::convertTimestampToDate($createdAt),
                    Library\Common::convertTimestampToHour($createdAt), $assocCampaignIdName[$campaignId], $this->assocActionIdName[$actionId], $createdAt
                );
            }
        }
        echo 'end get MakProgrammaticLogKeyword::searchShopChannelIdByDateFromTo ' . microtime(), PHP_EOL;

        echo PHP_EOL, 'begin insert action ' . microtime(), PHP_EOL;
        # begin insert campaign action
        $campaignActionInsertChunk = array_chunk($campaignActionInsertArray, 200);
        foreach ($campaignActionInsertChunk as $chunk) {
            Models\ModelChart\ChartKeywordCampaignAction::batchInsert($chunk);
        }
        echo 'end insert action ' . microtime(), PHP_EOL;
        # end insert campaign action

        echo PHP_EOL, 'begin insert action intraday ' . microtime(), PHP_EOL;
        # begin insert campaign action intraday
        $campaignActionIntradayInsertChunk = array_chunk($campaignActionIntradayInsertArray, 200);
        foreach ($campaignActionIntradayInsertChunk as $chunk) {
            Models\ModelChart\ChartKeywordCampaignActionIntraday::batchInsert($chunk);
        }
        echo 'end insert action intraday ' . microtime(), PHP_EOL;
        # end insert campaign action intraday

    }
}