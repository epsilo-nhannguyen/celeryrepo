<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 09:46
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use Illuminate\Console\Command;
use Exception;


class MappingProductChannelCategory extends Command
{

    /**
     * @uses php artisan mapping-product-channel-category --shop=shop-id --channel=channel-id
     * example: php artisan mapping-product-channel-category --shop=62 --channel=3
     */
    protected $signature = 'mapping-product-channel-category {--shop=} {--channel=}';

    protected $description = 'Mapping channel category to product shop channel';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        Library\Common::setTimezoneByChannelId($channelId);

        $shopInfo = Library\Formater::stdClassToArray(Models\ShopMaster::getById($shopId));
        $organizationId = $shopInfo[Models\ShopMaster::COL_FK_ORGANIZATION];

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];

        $itemIdArray = $assocItemIdProductShopChannelId = [];
        $productShopChannelData = Library\Formater::stdClassToArray(Models\ProductShopChannel::searchByChannelCategoryIsNull($shopChannelId, 1000));
        foreach ($productShopChannelData as $productShopChannel) {
            $itemId = $productShopChannel[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID];
            $productShopChannelId = $productShopChannel[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID];

            $itemIdArray[] = $itemId;
            $assocItemIdProductShopChannelId[$itemId] = $productShopChannelId;
        }

        $assocItemIdCategory = [];
        $productMongo = Models\Mongo\Product::searchByIdentifyArray($shopId, $channelId, $itemIdArray);
        foreach ($productMongo as $product) {
            $channelIdentify = $product['data']['channelIdentify'];
            $channelCategory = $product['data']['channelCategory'];

            $assocItemIdCategory[$channelIdentify] = $channelCategory;
        }

        $assocCategoryChannelId = [];
        foreach ($assocItemIdProductShopChannelId as $itemId => $productShopChannelId) {
            $categoryChannelIdentify = $assocItemIdCategory[$itemId] ?? null;

            if ( ! $categoryChannelIdentify) continue;

            if ( ! isset($assocCategoryChannelId[$categoryChannelIdentify])) {
                $channelCategoryInfo = Models\ChannelCategory::getByChannelIdAndIdentify($organizationId, $channelId, $categoryChannelIdentify);
                if ($channelCategoryInfo) {
                    $assocCategoryChannelId[$categoryChannelIdentify] = $channelCategoryInfo[Models\ChannelCategory::COL_CHANNEL_CATEGORY_ID];
                }
            }

            $channelCategoryId = $assocCategoryChannelId[$categoryChannelIdentify] ?? null;

            if ( ! $channelCategoryId) continue;

            Models\ProductShopChannel::updateCategoryById($productShopChannelId, $channelCategoryId, Models\Admin::EPSILO_SYSTEM);
        }

        Models\Mongo\MakSetup::updateSetupDone($shopId, $channelId, Models\Mongo\MakSetup::SETUP_PULL_CATEGORY);

    }
}