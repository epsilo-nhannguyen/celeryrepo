<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 28/09/2019
 * Time: 13:55
 */

namespace App\Console\Commands;

use App\Library;
use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Exception;


class CampaignTimeline extends Command
{

    /**
     * @uses php artisan campaign-timeline --shop=shop-id --channel=channel-id
     * example: php artisan campaign-timeline --shop=62 --channel=3
     */
    protected $signature = 'campaign-timeline {--shop=} {--channel=}';

    protected $description = 'Check timeline campaign for pause product ads and inactive campaign';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        Library\Common::setTimezoneByChannelId($channelId);

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];
        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $today = date('Y-m-d');
        $campaignData = Library\Formater::stdClassToArray(Models\MakProgrammaticCampaign::searchAvailableTimelineToUpdate($shopChannelId, $today));
        foreach ($campaignData as $campaign) {
            $campaignId = $campaign[Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID];
            $isActivated = $campaign[Models\MakProgrammaticCampaign::COL_FK_CONFIG_ACTIVE] == Models\ConfigActive::ACTIVE;
            $isAvailable = boolval($campaign['isAvailable']);

            $isNotAllowUpdate = ($isAvailable && $isActivated) || ( ! $isAvailable && ! $isActivated);
            if ($isNotAllowUpdate) continue;

            $campaignService = new Services\ProgrammaticCampaign($campaignId);
            $dataProductAds = $campaignService->dataProductAds;

            $dataProductAdsValid = [];
            $campaignStatus = $productStateChannel = -1;
            $productStateSystem = '';

            if ( ! $isAvailable && $isActivated) {
                $dataProductAdsValid = array_filter($dataProductAds, function ($item) {
                    return $item[Models\ProductAds::COL_PRODUCT_ADS_STATE] == Models\ProductAds::STATE_ONGOING;
                });

                $campaignStatus = Models\ConfigActive::INACTIVE;
                $productStateChannel = Library\Crawler\Channel\Shopee::SKU_PAUSE;
                $productStateSystem = Models\ProductAds::STATE_PAUSED;
            }

            if ($isAvailable && ! $isActivated) {
                $dataProductAdsValid = array_filter($dataProductAds, function ($item) {
                    return $item[Models\ProductAds::COL_PRODUCT_ADS_STATE] == Models\ProductAds::STATE_PAUSED
                        && $item[Models\ProductAds::COL_PRODUCT_ADS_STATE_UPDATED_BY] == Models\Admin::EPSILO_SYSTEM;
                });

                $campaignStatus = Models\ConfigActive::ACTIVE;
                $productStateChannel = Library\Crawler\Channel\Shopee::SKU_RESUME;
                $productStateSystem = Models\ProductAds::STATE_ONGOING;
            }
            $adminId = Models\Admin::EPSILO_SYSTEM;
            Models\MakProgrammaticCampaign::updateStatusById($campaignId, $campaignStatus, $adminId);

            $productAdsIdArray = [];
            foreach ($dataProductAdsValid as $productAds) {
                $productAdsId = $productAds[Models\ProductAds::COL_PRODUCT_ADS_ID];
                $adsId = $productAds[Models\ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID];

                if ( ! $isLogin) $isLogin = $crawler->login();
                if ( ! $isLogin) $isLogin = $crawler->login();
                if ( ! $isLogin) $isLogin = $crawler->login();

                $isUpdated = $crawler->turnSkuWithStatus($adsId, $productStateChannel);
                if ( ! $isUpdated) continue;

                $productAdsIdArray[] = $productAdsId;
            }

            if ( ! $productAdsIdArray) continue;
            Models\ProductAds::updateStateByIdArray($productAdsIdArray, $productStateSystem, $adminId);

        }
    }
}