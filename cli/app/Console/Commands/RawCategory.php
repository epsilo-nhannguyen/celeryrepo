<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 11:12
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use App\Jobs;
use Illuminate\Console\Command;
use Exception;


class RawCategory extends Command
{
    /**
     * @uses php artisan raw-category --shop=shop-id --channel=channel-id
     * example: php artisan raw-category --shop=62 --channel=3
     */
    protected $signature = 'raw-category {--shop=} {--channel=}';

    protected $description = 'Raw category from Shopee to mongo database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        Library\Common::setTimezoneByChannelId($channelId);

        $message = json_encode([
            'shopId' => $shopId,
            'channelId' => $channelId,
        ]);

        dispatch((new Jobs\RawCategory($message))->onQueue(Library\QueueName::getByClass(Jobs\RawCategory::class)));

    }

}