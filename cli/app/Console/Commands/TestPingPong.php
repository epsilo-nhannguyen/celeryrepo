<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 21/09/2019
 * Time: 16:32
 */

namespace App\Console\Commands;


use App\Jobs\ExampleJob;
use App\Library\Common;
use App\Library\Crawler\Channel\Shopee;
use App\Library\MLSWShopee;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Queue;

class TestPingPong extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test-ping-pong';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Redis-cache vs Redis Queue';

    public function handle()
    {
        $MLKWShopee = new MLSWShopee(62,3,149);
        $a = $MLKWShopee->modifyBiddingPrice(892699, array('toner' => [666,123], 'ahc'=>[555,123]));
//        $a = $MLKWShopee->modifyBiddingPrice(892699,['toner' => 555]);

        echo $a.PHP_EOL;

        die;
        $this->info('Begin Test Caching');
        Cache::set('cache_test_ping_pong', '1', 1);
        if (Cache::get('cache_test_ping_pong') == 1) {
            $this->info('Caching ok!');
        } else {
            $this->warn('Caching not work!');
        }

        sleep(1);

        $this->info('Begin Test Queue');
        $res = Queue::pushOn('test_ping_pong', new ExampleJob(), []);
        if ($res) {
            $this->info('Queue ok!');
        } else {
            $this->error('Queue not work!');
        }

        $this->info('Begin test mysql');


    }
}