<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:42
 */

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Library;
use App\Models;
use Exception;


class SnapshotProductLog extends Command
{
    /**
     * @uses: php artisan snapshot-product-log --shop=shop-id --channel=channel-id
     * example: php artisan snapshot-product-log --shop=62 --channel=3
     */

    protected $signature = 'snapshot-product-log {--shop=} {--channel=}';

    protected $description = 'Snapshot product log to Mongo database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');
        if ( ! $shopId || ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Models\ShopChannel::getByShopIdChannelId($shopId, $channelId);
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        Library\Common::setTimezoneByChannelId($channelId);

        $now = Library\Common::getCurrentTimestamp();
        $currentDate = Library\Common::convertTimestampToDate($now);
        $currentHour = date('H', $now);

        $shopChannelId = $shopChannelInfo->{Models\ShopChannel::COL_SHOP_CHANNEL_ID};

        $productLogInsertArray = [];

        $productIdInLog = array_column(
            Models\Mongo\ProductLog::searchByShopIdChannelId($shopId, $channelId, $currentDate, $currentDate, $currentHour),
            'productShopChannelId'
        );

        $productShopChannelData = Models\ProductShopChannel::searchByShopChannelIdAndState($shopChannelId, Models\ProductAds::STATE_ONGOING);
        foreach ($productShopChannelData as $productShopChannel) {
            $productShopChannelId = $productShopChannel->{Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID};

            if (in_array($productShopChannelId, $productIdInLog)) continue;

            $postsub = $productShopChannel->{Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_PRICE_POSTSUB_CHANNEL};
            $rrp = $productShopChannel->{Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_PRICE_RRP_CHANNEL};
            $sellableStock = $productShopChannel->{Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK};

            $presentDiscount = $rrp > 0 ? (1 - $postsub / $rrp) * 100 : 0;

            $productLogInsertArray[] = [
                'shopId' => intval($shopId),
                'channelId' => intval($channelId),
                'productShopChannelId' => intval($productShopChannelId),
                'date' => $currentDate,
                'hour' => intval($currentHour),
                'presentDiscount' => floatval($presentDiscount),
                'sellableStock' => intval($sellableStock),
                'createAt' => $now,
                'createAtString' => Library\Common::convertTimestampToDatetime($now)
            ];
        }

        if ( ! $productLogInsertArray) return;

        Models\Mongo\ProductLog::batchInsert($productLogInsertArray);
    }
}