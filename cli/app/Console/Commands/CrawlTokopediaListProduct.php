<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Library\QueueName;
use App\Models;
use App\Library;
use App\Jobs;

class CrawlTokopediaListProduct extends Command
{
    /**
     * @uses:
     * php artisan crawler-tokopedia-list-product --shop=shop-channel-id
     * example: php artisan crawler-tokopedia-list-product --shop=590
     */

    protected $signature = 'crawler-tokopedia-list-product {--shop=}';
    protected $description = 'Crawl list product from Tokopedia to MySQL database (with out shop channel id will crawl all tokopedia shop)';

    public function handle()
    {
        // Read input params
        $shopId = $this->option('shop');

        // Check input and push queue
        if (!$shopId) {
            $allTokopediaShop = Models\ShopChannel::getAllShopChannelIsTokopedia();
            foreach ($allTokopediaShop as $tokopediaShop) {
                $crawlCredential = $this->checkLoginTokopediaService($tokopediaShop->shop_channel_api_credential);
                if (!$crawlCredential) {
                    Library\LogError::getInstance()->slack('Job' . __CLASS__ . ' - Tokopedia - Shop Channel Id: ' . $tokopediaShop->shop_channel_id . ' - Login failure');
                    continue;
                }
                $jobCrawlTokopediaProduct = new Jobs\CrawlTokopediaListProduct($tokopediaShop->shop_channel_id, $crawlCredential);
                dispatch($jobCrawlTokopediaProduct->onQueue(QueueName::getByClass(Jobs\CrawlTokopediaListProduct::class)));
            }
        } else {
            $getShopChannelInfo = Models\ShopChannel::getById($shopId);
            $crawlCredential = $this->checkLoginTokopediaService($getShopChannelInfo->shop_channel_api_credential);
            if (!$crawlCredential) {
                Library\LogError::getInstance()->slack('Job' . __CLASS__ . ' - Tokopedia - Shop Channel Id: ' . $shopId . ' - Login failure');
                return null;
            }
            $jobCrawlTokopediaProduct = new Jobs\CrawlTokopediaListProduct($shopId, $crawlCredential);
            dispatch($jobCrawlTokopediaProduct->onQueue(QueueName::getByClass(Jobs\CrawlTokopediaListProduct::class)));
        }
    }

    /**
     * @param $shopChannelCredential
     * @return mixed
     */
    private function checkLoginTokopediaService($shopChannelCredential)
    {
        // Verify credential
        $shopChannelCredential = json_decode($shopChannelCredential, true);
        if (!isset($shopChannelCredential['shopId'])) {
            return null;
        }
        if (!isset($shopChannelCredential['tkpdUserId'])) {
            return null;
        }
        if (!isset($shopChannelCredential['cookieString'])) {
            return null;
        }
        return $shopChannelCredential;
    }
}