<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 15:10
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Exception;


class ImportCrawlerAds extends Command
{
    /**
     * @uses: php artisan import-crawler-ads --shop=shop-id --channel=channel-id
     * example: php artisan import-crawler-ads --shop=62 --channel=3
     */

    protected $signature = 'import-crawler-ads {--shop=} {--channel=}';

    protected $description = 'Import paid ads data from mongo to mysql database';

    /**
     * @var array
     */
    private $_cacheCampaign = [];

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if (!$shopId || !$channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if (!$shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        if ($channelInfo[Models\Channel::COL_CHANNEL_CODE] != Models\Channel::SHOPEE_CODE) return;

        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];
        Library\Common::setTimezoneByVentureId($ventureId);

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];
        
        $productStateArray = [
            Models\ProductAds::STATE_PAUSED, Models\ProductAds::STATE_ONGOING, Models\ProductAds::STATE_SCHEDULED, Models\ProductAds::STATE_CLOSED, Models\ProductAds::STATE_ENDED
        ];
        $assocAdsIdProductShopChannelId = array_column(
            Library\Formater::stdClassToArray(Models\ProductAds::searchByShopChannelId($shopChannelId, $productStateArray)),
            Models\ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL,
            Models\ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID
        );
        
        $assocKeywordIdName = $assocKeywordProductToId = $assocItemIdToProductShopChannelId = [];

        $keywordProductArray = Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByShopChannelId($shopChannelId));
        foreach ($keywordProductArray as $keywordProduct) {
            $keywordName = $keywordProduct[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME];
            $keywordName = Library\Common::strToHex($keywordName);
            
            $keywordId = $keywordProduct[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID];
            $productShopChannelId = $keywordProduct[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID];
            $itemId = $keywordProduct[Models\ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID];
            $keywordProductId = $keywordProduct[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID];
            
            if ( ! isset($assocKeywordIdName[$keywordName])) {
                $assocKeywordIdName[$keywordName] = [];
            }

            $assocKeywordIdName[$keywordName] = $keywordId;
            $assocItemIdToProductShopChannelId[$itemId] = $productShopChannelId;
            $assocKeywordProductToId[$keywordId.'-'.$productShopChannelId] = $keywordProductId;
        }

        $performanceInsertArray = $performanceUpdateCase = $trendInsertArray = [];
        $idImportedArray = $assocPerformanceDateDb = [];

        $crawlerAdsMongo = Models\Mongo\CrawlerAds::searchForImport($shopId, $channelId);
        foreach ($crawlerAdsMongo as $bson) {
            /** @var MongoDB\BSON\ObjectId $mongoObjectId */
            $mongoObjectId = $bson->_id;
            $document = Library\Formater::stdClassToArray($bson);
            $idImportedArray[] = $mongoObjectId;
            $timestamp = $document['dateHour'];

            $date = date('Y-m-d', $timestamp);
            $datetime = date('Y-m-d H:i:s', $timestamp);

            if ( ! isset($assocPerformanceDateDb[$date])) {
                $assocPerformanceDateDb[$date] = Library\Formater::stdClassToArray(Models\MakPerformance::searchByShopChannelIdAndDate(
                    $shopChannelId, Library\Formater::date($date)
                ));
            }

            $performanceDateDb = $assocPerformanceDateDb[$date];
            $keywordProductInDbArray = array_column($performanceDateDb, Models\MakPerformance::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT);

            $dataReport = json_decode($document['dataReport'], true);
            $adsId = $document['adsId'];

            $productShopChannelId = $assocAdsIdProductShopChannelId[$adsId] ?? null;
            if ( ! $productShopChannelId) continue;

            $keyCache = $productShopChannelId . '_' . $date;
            if ( ! isset($this->_cacheCampaign[$keyCache])) {
                $productShopChannelCampaign = Library\Formater::stdClassToArray(Models\ProductShopChannelCampaign::getByProductShopChannelAndDate($productShopChannelId, Library\Formater::date($date)));
                if ($productShopChannelCampaign) {
                    $campaignId = $productShopChannelCampaign[Models\ProductShopChannelCampaign::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN];
                } else {
                    $campaignDefault = Library\Formater::stdClassToArray(Models\MakProgrammaticCampaign::getByShopChannelId($shopChannelId));
                    $campaignId = $campaignDefault[Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID];
                }

                $this->_cacheCampaign[$keyCache] = $campaignId;
            }

            foreach ($dataReport['reports'] ?? [] as $item) {
                $keyword = Library\Common::strToHex($item['keyword']);
                $matchType = $item['match_type'] == 1 ? Models\MakProgrammaticKeywordProduct::MATCH_TYPE_EXPANSION : Models\MakProgrammaticKeywordProduct::MATCH_TYPE_DEFINED;

                $biddingPrice = 0;
                if ($ventureId == Models\Venture::VIETNAM) {
                    $biddingPrice = $matchType == Models\MakProgrammaticKeywordProduct::MATCH_TYPE_EXPANSION ? 480 : 400;
                } elseif ($ventureId == Models\Venture::PHILIPPINES) {
                    $biddingPrice = $matchType == Models\MakProgrammaticKeywordProduct::MATCH_TYPE_EXPANSION ? 0.48 : 0.4;
                } elseif ($ventureId == Models\Venture::MALAYSIA) {
                    $biddingPrice = $matchType == Models\MakProgrammaticKeywordProduct::MATCH_TYPE_EXPANSION ? 0.12 : 0.1;
                } elseif ($ventureId == Models\Venture::SINGAPORE) {
                    $biddingPrice = $matchType == Models\MakProgrammaticKeywordProduct::MATCH_TYPE_EXPANSION ? 0.05 : 0.04;
                } elseif ($ventureId == Models\Venture::INDONESIA) {
                    $biddingPrice = $matchType == Models\MakProgrammaticKeywordProduct::MATCH_TYPE_EXPANSION ? 180 : 150;
                }

                $keywordId = $assocKeywordIdName[$keyword] ?? null;
                if ( ! $keywordId) continue;

                if ( ! isset($assocKeywordProductToId[$keywordId.'-'.$productShopChannelId])) {
                    $keywordProductId = Models\MakProgrammaticKeywordProduct::insertGetId(
                        $keywordId,
                        $productShopChannelId,
                        $biddingPrice,
                        $matchType,
                        Models\MakProgrammaticKeywordProduct::STATUS_DELETED,
                        Models\Admin::EPSILO_SYSTEM
                    );
                    sleep(1);

                    $assocKeywordProductToId[$keywordId.'-'.$productShopChannelId] = $keywordProductId;
                }

                $keywordProductId = $assocKeywordProductToId[$keywordId.'-'.$productShopChannelId] ?? null;
                if ( ! $keywordProductId) continue;

                $search = $item['query'];
                $view = $item['views'];
                $click = $item['clicks'];
                $rateClick = ($item['ctr']) * 100;
                $numberOfOrder = $item['orders'];
                $productSold = $item['items'];
                $gmv = $item['gmv'];
                $expenses = floatval($item['expenses']);
                $averagePosition = $item['position'];

                $trendInsertArray[] = Models\MakTrend::buildDataInsert(
                    $keywordProductId, Library\Formater::datetime($datetime), $gmv, $view, $rateClick, $numberOfOrder, $search,
                    $productSold, $averagePosition, $expenses, $click, $this->_cacheCampaign[$keyCache] ?? null
                );

                if ( ! in_array($keywordProductId, $keywordProductInDbArray)) {
                    $performanceInsertArray[] = Models\MakPerformance::buildDataInsert(
                        $keywordProductId, Library\Formater::date($date), $gmv, $view, $rateClick, $numberOfOrder, $search,
                        $productSold, $averagePosition, $expenses, $click, $this->_cacheCampaign[$keyCache] ?? null
                    );

                    $keywordProductInDbArray[] = $keywordProductId;
                    
                    $assocPerformanceDateDb[$date][] = [
                        Models\MakPerformance::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT => $keywordProductId,
                        Models\MakPerformance::COL_MAK_PERFORMANCE_DATE => $date,
                        Models\MakPerformance::COL_MAK_PERFORMANCE_ID => null
                    ];
                } else {
                    $performanceId = null;
                    foreach ($performanceDateDb as $performance) {
                        if (
                            $performance[Models\MakPerformance::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT] == $keywordProductId
                            && $performance[Models\MakPerformance::COL_MAK_PERFORMANCE_DATE] == $date
                        ) {
                            $performanceId = $performance[Models\MakPerformance::COL_MAK_PERFORMANCE_ID];
                            break;
                        }
                    }

                    if ( ! $performanceId) continue;

                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_GMV][$performanceId] = $gmv;
                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_VIEW][$performanceId] = $view;
                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_CLICK_RATE][$performanceId] = $rateClick;
                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_ORDERS][$performanceId] = $numberOfOrder;
                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_QUERY][$performanceId] = $search;
                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD][$performanceId] = $productSold;
                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_AVG_POSITION][$performanceId] = $averagePosition;
                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_EXPENSE][$performanceId] = $expenses;
                    $performanceUpdateCase[Models\MakPerformance::COL_MAK_PERFORMANCE_CLICKS][$performanceId] = $click;
                }
            }
        }

        #begin insert trend
        $trendInsertChunk = array_chunk($trendInsertArray, 200);
        foreach ($trendInsertChunk as $chunk) {
            Models\MakTrend::batchInsert($chunk);
        }
        #end insert trend

        #begin insert performance
        $performanceInsertChunk = array_chunk($performanceInsertArray, 200);
        foreach ($performanceInsertChunk as $chunk) {
            Models\MakPerformance::batchInsert($chunk);
        }
        #end insert performance

        #begin update performance
        foreach ($performanceUpdateCase as $columnUpdated => $assocIdValue) {
            $assocIdValueChunk = array_chunk($assocIdValue, 200, true);
            foreach ($assocIdValueChunk as $chunk) {
                Models\MakPerformance::setTableUpdated(Models\MakPerformance::TABLE_NAME);
                Models\MakPerformance::setColumnCondition(Models\MakPerformance::COL_MAK_PERFORMANCE_ID);
                Models\MakPerformance::setColumnUpdated($columnUpdated);
                foreach ($chunk as $id => $value) {
                    Models\MakPerformance::addCase($id, $value);
                }

                $sql = Models\MakPerformance::assemble();
                Models\MakPerformance::updateCase($sql);
            }
        }

        #end update performance

        if ($idImportedArray) {
            Models\Mongo\CrawlerAds::updateImported($shopId, $channelId, $idImportedArray);
        }

        #Import Ranking
        $idImportedArray = [];
        $rankingData = Models\Mongo\MakRankingKeyword::searchForImport($shopId, $channelId);

        $dataCrawRankingInsert = $assocKeywordProductIdCurrentRank = [];
        foreach ($rankingData as $bson) {
            /** @var MongoDB\BSON\ObjectId $mongoObjectId */
            $mongoObjectId = $bson->_id;
            $idImportedArray[] = $mongoObjectId;
            $document = Library\Formater::stdClassToArray($bson);

            $keywordName = $document['keyword'];
            $keywordId = $assocKeywordIdName[Library\Common::strToHex($keywordName)] ?? null;
            if ( ! $keywordId) continue;

            $collectedAt = $document['updatedAt'];
            $data = $document['data'];
            foreach ($data as $info) {
                $crawItemId = $info['itemid'];
                $crawRank = $info['rank'];

                $productShopChannelId = $assocItemIdToProductShopChannelId[$crawItemId] ?? null;
                if ( ! isset($productShopChannelId)) continue;

                $dataCrawRankingInsert[] = Models\MakCrawRanking::buildDataInsert($keywordId, $productShopChannelId, $crawRank, $collectedAt, $channelId);

                $keywordProductId = $assocKeywordProductToId[$keywordId.'-'.$productShopChannelId] ?? null;
                if ( ! $keywordProductId) continue;

                $assocKeywordProductIdCurrentRank[Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CURRENT_RANK][$keywordProductId] = $crawRank;
            }
        }

        #begin insert ranking
        $dataCrawRankingInsertChunk = array_chunk($dataCrawRankingInsert, 200);
        foreach ($dataCrawRankingInsertChunk as $chunk) {
            Models\MakCrawRanking::batchInsert($chunk);
        }
        #end insert ranking

        #begin update ranking
        foreach ($assocKeywordProductIdCurrentRank as $columnUpdated => $assocIdValue) {
            $assocIdValueChunk = array_chunk($assocIdValue, 200, true);
            foreach ($assocIdValueChunk as $chunk) {
                Models\MakProgrammaticKeywordProduct::setTableUpdated(Models\MakProgrammaticKeywordProduct::TABLE_NAME);
                Models\MakProgrammaticKeywordProduct::setColumnCondition(Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID);
                Models\MakProgrammaticKeywordProduct::setColumnUpdated($columnUpdated);
                foreach ($chunk as $id => $value) {
                    Models\MakProgrammaticKeywordProduct::addCase($id, $value);
                }

                $sql = Models\MakProgrammaticKeywordProduct::assemble();
                $isSuccess = Models\MakProgrammaticKeywordProduct::updateCase($sql);

                if ($isSuccess) {
                    Models\MakProgrammaticKeywordProduct::updateCurrentRankByIdArray(array_keys($chunk));
                }
            }
        }
        #end update ranking

        if ($idImportedArray) {
            Models\Mongo\MakRankingKeyword::updateImported($idImportedArray);
        }

        #Import Ranking
        Services\ProgrammaticShopeeKeywordBidding::turnStatusSku($shopChannelId);
        Services\ProgrammaticShopeeKeywordBidding::turnStatusSkuKeyword($shopChannelId);
        Services\ProgrammaticShopeeKeywordBidding::changeBiddingPrice($shopChannelId);

    }
}