<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 13:23
 */

namespace App\Console\Commands;


use App\Services\ChartKeywordCampaignMetricIntraday;
use App\Services\Dashboard\Forecast\Strategy\Contribution;
use App\Services\Dashboard\MetricManager;
use App\Services\Dashboard\Shop\ConvertingIntradayToDaily;
use App\Services\Dashboard\Shop\Database\Database;
use App\Services\Dashboard\Shop\Forecast;
use App\Services\Dashboard\Shop\PrimaryData;
use App\Services\Dashboard\Forecast\ForecastManager;
use App\Services\ShopeeKeywordWalletBalance;
use App\Services\TestCurl;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models;
use App\Library;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Index extends Command
{

    protected $signature = 'index';

    protected $description = 'Do some thing';

    public function handle()
    {
//        throw new \Exception('AAA');
        echo "Hello Celery" . PHP_EOL;
        return;

        $columnArray = [
            'currency_id',
            'fk_venture',
            'currency_code',
            'currency_created_at',
            'currency_created_by',

        ];


        echo "const TABLE_NAME = '".current($columnArray)."';".PHP_EOL.PHP_EOL;

        foreach ($columnArray as $index => $column) {
            if ($index == 0) continue;

            echo "const COL_".strtoupper($column)." = '".$column."';".PHP_EOL;
        }

    }

    /**
     * build Param
     * @param int $shopChannelId
     * @param int $from
     * @param int $size
     * @return array
     */
    private function _buildParam($shopChannelId, $from, $size)
    {
        return [
            '_source' => [
                'includes' => ['shopChannelId', 'wallet', 'Timestamp']
            ],
            'from' => $from,
            'size' => $size,
            'query' => [
                'match' => [
                    'shopChannelId' => $shopChannelId
                ]
            ]
        ];
    }
}