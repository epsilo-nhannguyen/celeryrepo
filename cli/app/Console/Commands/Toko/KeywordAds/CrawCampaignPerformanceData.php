<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 11/02/2020
 * Time: 10:20
 */

namespace App\Console\Commands\Toko\KeywordAds;

use App\Jobs;
use App\Library;
use App\Models;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Queue;


class CrawCampaignPerformanceData extends Command
{
    protected $signature = 'toko:keyword-ads:craw-campaign-performance-data {--arrayShopId=} {--dateFrom} {--dateTo}';

    protected $description = 'Craw campaign performance info from channel --arrayShopId={separator by comma} {--dateFrom} {--dateTo}';

    public function handle()
    {
        $arrayShopId = $this->option('arrayShopId');
        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');

        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $channelCode = Models\Channel::TOKOPEDIA_CODE;
        $allShopChannel = Models\ModelBusiness\ShopChannel::searchShopActive_byChannelCode($channelCode);

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }

        foreach ($allShopChannel as $shopChannel) {
            $shopChannelId = $shopChannel->shop_channel_id;

            Library\Common::setTimezone($shopChannel->venture_timezone);

            if ($dateFrom && ! $dateTo) {
                $dateTo = date('y-m-d');
            } elseif ( !$dateFrom && $dateTo) {
                $dateFrom = $dateTo;
            } elseif ( ! $dateFrom && ! $dateTo) {
                $dateFrom = date('Y-m-d');
                $dateTo = date('Y-m-d');
            }

            $tokopedia = new Library\Crawler\Channel\Tokopedia($shopChannelId);
            $responseDTO = $tokopedia->login();

            if ( ! $responseDTO->getIsSuccess()) $responseDTO = $tokopedia->login();;
            if ( ! $responseDTO->getIsSuccess()) $responseDTO = $tokopedia->login();;
            if ( ! $responseDTO->getIsSuccess()) $responseDTO = $tokopedia->login();;
            if ( ! $responseDTO->getIsSuccess()) continue;

            // can sleep any minutes
            // push queue craw campaign performance

            $job = new Jobs\CrawlTokopediaGroupAdsPerformance($shopChannelId, $responseDTO->getData(), $dateFrom, $dateTo);
            dispatch($job->onQueue(Library\QueueName::TOKO_KEYWORD_ADS_CRAW_CAMPAIGN_PERFORMANCE));

            // push queue craw sku
            $job = new Jobs\CrawlTokopediaProductAdsPerformance($shopChannelId, $responseDTO->getData(), $dateFrom, $dateTo);
            dispatch($job->onQueue(Library\QueueName::TOKO_KEYWORD_ADS_CRAW_SKU_PERFORMANCE));

            // push queue craw keyword
            $job = new Jobs\CrawlTokopediaKeywordAdsPerformance($shopChannelId, $responseDTO->getData(), $dateFrom, $dateTo);
            dispatch($job->onQueue(Library\QueueName::TOKO_KEYWORD_ADS_CRAW_KEYWORD_PERFORMANCE));

        }
    }
}