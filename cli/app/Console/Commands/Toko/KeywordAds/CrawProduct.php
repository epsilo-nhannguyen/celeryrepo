<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 11/02/2020
 * Time: 10:20
 */

namespace App\Console\Commands\Toko\KeywordAds;

use App\Jobs;
use App\Library;
use App\Models;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Queue;


class CrawProduct extends Command
{
    protected $signature = 'toko:keyword-ads:craw-product {--arrayShopId=}';

    protected $description = 'Craw product info from channel --arrayShopId={separator by comma}';

    public function handle()
    {
        $arrayShopId = $this->option('arrayShopId');

        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $channelCode = Models\Channel::TOKOPEDIA_CODE;
        $allShopChannel = Models\ModelBusiness\ShopChannel::searchShopActive_byChannelCode($channelCode);

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }

        foreach ($allShopChannel as $shopChannel) {
            $shopChannelId = $shopChannel->shop_channel_id;

            $tokopedia = new Library\Crawler\Channel\Tokopedia($shopChannelId);
            $responseDTO = $tokopedia->login();

            if ( ! $responseDTO->getIsSuccess()) $responseDTO = $tokopedia->login();
            if ( ! $responseDTO->getIsSuccess()) $responseDTO = $tokopedia->login();
            if ( ! $responseDTO->getIsSuccess()) $responseDTO = $tokopedia->login();
            if ( ! $responseDTO->getIsSuccess()) continue;

            // push queue
            $job = new Jobs\CrawlTokopediaListProduct($shopChannelId, $responseDTO->getData());
            dispatch($job->onQueue(Library\QueueName::TOKO_KEYWORD_ADS_CRAW_PRODUCT));
        }
    }
}