<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 28/09/2019
 * Time: 13:55
 */

namespace App\Console\Commands\Toko\KeywordAds;

use App\Jobs;
use App\Library;
use App\Models;
use App\Repository;
use App\Services;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Queue;


class CampaignTimeline extends Command
{

    protected $signature = 'toko:keyword-ads:campaign-timeline {--arrayShopId=}';

    protected $description = 'Check timeline campaign --arrayShopId={separator by comma}';

    public function handle()
    {
        $arrayShopId = $this->option('arrayShopId');

        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $channelCode = Models\Channel::TOKOPEDIA_CODE;
        $allShopChannel = Models\ModelBusiness\ShopChannel::searchShopActive_byChannelCode($channelCode);

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }

        foreach ($allShopChannel as $shopChannel) {
            $shopChannelId = $shopChannel->shop_channel_id;
            $timezone = $shopChannel->venture_timezone;
            Library\Common::setTimezone($timezone);

            $today = date('Y-m-d');

            $campaignData = Models\ModelBusiness\TokoAdsCampaign::searchAvailableTimelineToUpdate($shopChannelId, $today);
            $campaignAvailable = $campaignData->filter(function ($item) {
                $isActivated = $item->config_active_id == Models\ConfigActive::ACTIVE;
                $updatedBy = $item->updated_by;
                $isUpdatedBySystem =  ! $updatedBy || $updatedBy == Models\Admin::EPSILO_SYSTEM;
                $isAvailable = boolval($item->is_available);

                return ( ! $isAvailable && $isActivated) || ($isAvailable && ! $isActivated && $isUpdatedBySystem);
            });

            $campaignIdArray = $campaignAvailable->pluck('id')->toArray();
            $assocCampaignId_skuData = Models\ModelBusiness\TokoAdsSku::searchByCampaignIdArray($campaignIdArray)->groupBy('campaign_id');
            $assocCampaignId_keywordData = Models\ModelBusiness\TokoAdsKeyword::searchByCampaignIdArray($campaignIdArray)->groupBy('campaign_id');
            $assocItemId_productId = Models\ModelBusiness\ProductShopChannel::searchByShopChannelId($shopChannelId)
                ->pluck('product_shop_channel_id', 'product_shop_channel_item_id');

            foreach ($campaignAvailable as $campaign) {
                $campaignId = $campaign->id;
                $channelCampaignId = $campaign->channel_campaign_id;
                $dailyBudget = $campaign->daily_budget;
                $isAuto = $campaign->is_auto;
                $isActivated = $campaign->config_active_id == Models\ConfigActive::ACTIVE;
                $isAvailable = boolval($campaign->is_available);

                /**
                 * @var Collection $skuData
                 */
                $skuData = $assocCampaignId_skuData->get($campaignId, collect([]));
                $productIdInSkuArray = $skuData->pluck('product_id')->toArray();
                /**
                 * @var Collection $assocProductId_channelProductId
                 */
                $assocProductId_channelProductId = $skuData->pluck('channel_product_id', 'product_id');
                /**
                 * @var Collection $keywordData
                 */
                $keywordData = $assocCampaignId_keywordData->get($campaignId, collect([]));

                $campaignStatus = $keywordStatus =  -1;
                $skuStatus = '';
                $campaignStatusChannel = $skuStatusChannel = $keywordStatusChannel = null;

                // push queue
                if ($channelCampaignId || $isAuto) {
                    $skuValidData = $keywordValidData = collect([]);

                    if ( ! $isAvailable && $isActivated) {
                        $skuValidData = $skuData->filter(function ($item) {
                            return $item->status == Models\ModelBusiness\TokoAdsSku::STATUS_ONGOING;
                        });
                        $keywordValidData = $keywordData->filter(function ($item) {
                            return $item->config_active_id == Models\ConfigActive::ACTIVE;
                        });

                        $campaignStatus = Models\ConfigActive::INACTIVE;
                        $skuStatus = Models\ModelBusiness\TokoAdsSku::STATUS_PAUSED;
                        $keywordStatus = Models\ConfigActive::INACTIVE;

                        $campaignStatusChannel = false;
                        $skuStatusChannel = false;
                        $keywordStatusChannel = false;
                    }

                    if ($isAvailable && ! $isActivated) {
                        $skuValidData = $skuData->filter(function ($item) {
                            $updatedBy = $item->updated_by;
                            $isUpdatedBySystem =  ! $updatedBy || $updatedBy == Models\Admin::EPSILO_SYSTEM;

                            return $item->status == Models\ModelBusiness\TokoAdsSku::STATUS_PAUSED && $isUpdatedBySystem;
                        });
                        $keywordValidData = $keywordData->filter(function ($item) {
                            $updatedBy = $item->updated_by;
                            $isUpdatedBySystem =  ! $updatedBy || $updatedBy == Models\Admin::EPSILO_SYSTEM;

                            return $item->config_active_id == Models\ConfigActive::INACTIVE && $isUpdatedBySystem;
                        });

                        $campaignStatus = Models\ConfigActive::ACTIVE;
                        $skuStatus = Models\ModelBusiness\TokoAdsSku::STATUS_ONGOING;
                        $keywordStatus = Models\ConfigActive::ACTIVE;

                        $campaignStatusChannel = true;
                        $skuStatusChannel = true;
                        $keywordStatusChannel = true;
                    }

                    if ($channelCampaignId) {
                        $groupAdsDTO = new Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO();
                        $groupAdsDTO->setGroupId(strval($channelCampaignId));
                        $groupAdsDTO->setToggleAdsGroup($campaignStatusChannel);

                        $object = json_encode([
                            'status' => $campaignStatus,
                            'isInTerminal' => 1,
                        ]);
                        $job = new Jobs\Channels\Tokopedia\ModifyGroupAdsStatus($shopChannelId, $groupAdsDTO, null, $campaignId, null, $object);
                        dispatch($job->onQueue('tokopedia_simulation'));

                        foreach ($skuValidData as $sku) {
                            $productAdsDTO = new Repository\RepositoryBusiness\Tokopedia\DTO\ProductAdsDTO();
                            $productAdsDTO->setAdsId(strval($sku->channel_product_id));
                            $productAdsDTO->setToggleSKU($skuStatusChannel);

                            $object = json_encode([
                                'status' => $skuStatus,
                                'isInTerminal' => 1,
                            ]);
                            $job = new Jobs\Channels\Tokopedia\ActiveDeactivateSkuInGroupAds($shopChannelId, $productAdsDTO, null, $sku->id, null, $object);
                            dispatch($job->onQueue('tokopedia_simulation'));
                        }

                        foreach ($keywordValidData as $keyword) {
                            $keywordAdsDTO = new Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO();
                            $keywordAdsDTO->setKeywordId(strval($keyword->channel_keyword_id));
                            $keywordAdsDTO->setToggleKeyword($keywordStatusChannel);

                            $object = json_encode([
                                'status' => $keywordStatus,
                                'isInTerminal' => 1,
                            ]);
                            $job = new Jobs\Channels\Tokopedia\ActiveDeactivateKeywordInGroupAds($shopChannelId, $keywordAdsDTO, null, $keyword->id, null, $object);
                            dispatch($job->onQueue('tokopedia_simulation'));
                        }
                    } else {
                        $groupAdsDTO = new Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO();
                        $groupAdsDTO->setDailyBudgetCost(intval($dailyBudget));

                        $object = json_encode([
                            'status' => $campaignStatus,
                            'isInTerminal' => 1,
                        ]);
                        if ($campaignStatus == Models\ConfigActive::ACTIVE) {
                            $job = new Jobs\Channels\Tokopedia\ActiveAutomationAds($shopChannelId, $groupAdsDTO, null, $campaignId, null, $object);
                            dispatch($job->onQueue('tokopedia_simulation'));
                        } elseif ($campaignStatus == Models\ConfigActive::INACTIVE) {
                            $job = new Jobs\Channels\Tokopedia\InactiveAutomationAds($shopChannelId, $groupAdsDTO, null, $campaignId, null, $object);
                            dispatch($job->onQueue('tokopedia_simulation'));
                        }
                    }
                } else {
                    $campaignStatus = Models\ConfigActive::ACTIVE;
                    $skuStatus = Models\ModelBusiness\TokoAdsSku::STATUS_ONGOING;
                    $keywordStatus = Models\ConfigActive::ACTIVE;
                    $groupAdsDTO = new Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO();
                    $groupAdsDTO->setName($campaign->name);
                    $groupAdsDTO->setDailyBudgetCost(intval($campaign->daily_budget));
                    $groupAdsDTO->setCostPerClick(intval($campaign->max_cost));

                    $assocItemId_productIdValidData = $assocItemId_productId->filter(function ($productId, $itemId) use ($productIdInSkuArray) {
                        return in_array($productId, $productIdInSkuArray);
                    });

                    $arrayProductAdsDTO = [];
                    foreach ($assocItemId_productIdValidData as $itemId => $productId) {
                        $channelProductId = $assocProductId_channelProductId->get($productId, 0);
                        $productAdsDTO = new Repository\RepositoryBusiness\Tokopedia\DTO\ProductAdsDTO();
                        $productAdsDTO->setProductId(strval($itemId));
                        $productAdsDTO->setAdsId(strval($channelProductId));

                        $arrayProductAdsDTO[] = $productAdsDTO;
                    }

                    $arrayKeywordAdsDTO = [];
                    foreach ($keywordData as $keyword) {
                        $keywordAdsDTO = new Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO();
                        $keywordAdsDTO->setKeywordName($keyword->name);
                        $keywordAdsDTO->setBidPrice(intval($keyword->max_cost));
                        $keywordAdsDTO->setMatchType($keyword->match_type == 'broad_match');

                        $arrayKeywordAdsDTO[] = $keywordAdsDTO;
                    }

                    $object = json_encode([
                        'isInTerminal' => 1,
                        'campaignStatus' => $campaignStatus,
                        'skuStatus' => $skuStatus,
                        'keywordStatus' => $keywordStatus,
                        'assocItemId_productId' => $assocItemId_productIdValidData->toArray(),
                        'keywordData' => $keywordData->toArray()
                    ]);

                    $job = new Jobs\Channels\Tokopedia\CreateGroupAds($shopChannelId, $groupAdsDTO, $arrayProductAdsDTO, null, $campaignId, null, $object, $arrayKeywordAdsDTO);
                    dispatch($job->onQueue('tokopedia_simulation'));
                }
            }
        }
    }
}
