<?php

namespace App\Console\Commands;

use App\Models;
use App\Library;
use Illuminate\Console\Command;
use Exception;

class RawCrawlShopAdsKeyword extends Command
{

    /**
     * @uses:
     * php artisan raw-crawler-shop-ads-keyword --organization=organization --shop=shop-id --channel=channel-id
     * example: php artisan raw-crawler-shop-ads-keyword --organization=25 --shop=21 --channel=3
     */

    const IS_SHOP_ADS = 3;

    protected $signature = 'raw-crawler-shop-ads-keyword {--organization=} {--shop=} {--channel=}';
    protected $description = 'Raw shop ads keyword from Shopee to MySQL database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');
        $organizationId = $this->option('organization');
        if (!$shopId || !$channelId || !$organizationId)
        {
            $getAllShopChannelRunShopAds = Models\ShopChannel::getAllShopActive_channelShopee_andOrganization();
            foreach ($getAllShopChannelRunShopAds as $shopRunShopAds) {
                $shopId = $shopRunShopAds->fk_shop_master;
                $channelId = $shopRunShopAds->fk_channel;
                $organizationId = $shopRunShopAds->fk_organization;
                try {
                    $this->runWithOption($shopId, $channelId, $organizationId);
                } catch (Exception $e) {
                    continue;
                }
            }
        }
        else {
            $this->runWithOption($shopId, $channelId, $organizationId);
        }
    }

    /**
     * @param $shopMasterId
     * @param $channelId
     * @param $organizationId
     * @throws Exception
     */
    private function runWithOption($shopMasterId, $channelId, $organizationId)
    {
        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopMasterId, $channelId));
        if (!$shopChannelInfo) {
            return;
        }

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));

        if ($channelInfo[Models\Channel::COL_CHANNEL_CODE] != Models\Channel::SHOPEE_CODE) {
            return;
        }

        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];
        Library\Common::setTimezoneByVentureId($ventureId);
        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $crawler->login();
        if (!$isLogin) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "Error" => 'Login failure!!!',
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }

        $crawlData = $crawler->crawlShopAdsKeyword();
        if (!$crawlData)
            $crawlData = $crawler->crawlShopAdsKeyword();
        if (!$crawlData)
            $crawlData = $crawler->crawlShopAdsKeyword();

        $rawCrawlData = $crawlData;
        $listAdsKeyword = [];
        $shopAdsInfo = [];

        if (!$crawlData || $crawlData['err_code'] != 0) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "Error" => 'Crawl with null value or error',
                "Data" => $crawlData,
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }
        if (!isset($crawlData['data'])) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "Error" => 'Channel change params response',
                "Data" => $crawlData,
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }
        if (!isset($crawlData['data']['campaign_ads_list'])) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "Error" => 'Channel change params response',
                "Data" => $crawlData,
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }

        $campaignAdsList = $crawlData['data']['campaign_ads_list'];

        foreach ($campaignAdsList as $campaign) {
            if (!isset($campaign['advertisements'])) {
                $dataLog = [
                    "ShopChannelId" => $shopChannelId,
                    "Error" => 'Channel change params response',
                    "Data" => $crawlData,
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                return;
            }
            if (!isset($campaign['campaign'])) {
                $dataLog = [
                    "ShopChannelId" => $shopChannelId,
                    "Error" => 'Channel change params response',
                    "Data" => $crawlData,
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                return;
            }

            // Get list ads key word & ads id & campaign id - On Advertisements
            foreach ($campaign['advertisements'] as $advertisement) {
                if ($advertisement['placement'] == self::IS_SHOP_ADS) {
                    if (!isset($advertisement['extinfo']) || !isset($advertisement['extinfo']['keywords'])) {
                        $dataLog = [
                            "ShopChannelId" => $shopChannelId,
                            "Error" => 'Channel change params response',
                            "Data" => $crawlData,
                        ];
                        Library\LogError::logErrorToTextFile($dataLog);
                        return;
                    }
                    $listAdsKeyword = $advertisement['extinfo']['keywords'];
                    unset($advertisement['extinfo']);
                    $shopAdsInfo['advertisement'] = $advertisement;
                }
            }

            // Get shop ads campaign info
            $shopAdsInfo = array_merge($shopAdsInfo, $campaign['campaign']);
        }

        // Insert or update shop ads info

        /**
         * @var $shopAdsModel Models\ModelBusiness\ShopAds
         */
        $shopAdsModel = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAds::getByShopChannelId($shopChannelId));

        if (!$shopAdsModel) {
            // Prepare data insert
            $recordDataInsert = Models\ModelBusiness\ShopAds::buildDataInsert(
                $shopChannelId,
                $shopAdsInfo['status'],
                $shopAdsInfo['advertisement']['adsid'],
                $shopAdsInfo['start_time'],
                $shopAdsInfo['end_time'],
                $shopAdsInfo['daily_quota'],
                $shopAdsInfo['total_quota'],
                $rawCrawlData
            );
            $isInsertSASuccess = Models\ModelBusiness\ShopAds::insertData($recordDataInsert);
            if (!$isInsertSASuccess) {
                $dataLog = [
                    "ShopChannelId" => $shopChannelId,
                    "Error" => 'Insert shop ads failure!!!',
                    "Data" => $recordDataInsert
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                return;
            }
        } else {
            // Prepare data update
            $recordDataUpdate = Models\ModelBusiness\ShopAds::buildDataUpdate(
                $shopAdsInfo['status'],
                $shopAdsInfo['advertisement']['adsid'],
                $shopAdsInfo['start_time'],
                $shopAdsInfo['end_time'],
                $shopAdsInfo['daily_quota'],
                $shopAdsInfo['total_quota'],
                $rawCrawlData
            );
            $isUpdateSASuccess = Models\ModelBusiness\ShopAds::updateById($shopAdsModel[Models\ModelBusiness\ShopAds::COL_ID], $recordDataUpdate);
            if (!$isUpdateSASuccess) {
                $dataLog = [
                    "ShopChannelId" => $shopChannelId,
                    "Error" => 'Update shop ads failure!!!',
                    "Data" => $recordDataUpdate
                ];
                Library\LogError::logErrorToTextFile($dataLog);
                return;
            }
        }

        // Insert or update shop ads keyword

        $shopAds = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAds::getByShopChannelId($shopChannelId));
        if (!$shopAds) {
            $dataLog = [
                "ShopChannelId" => $shopChannelId,
                "Error" => 'Shop ads info not found'
            ];
            Library\LogError::logErrorToTextFile($dataLog);
            return;
        }

        $listKeywordExist = Library\Formater::stdClassToArray(Models\MakProgrammaticKeyword::searchByOrganizationIdAndVentureId($organizationId, $ventureId));
        $listKeywordName = [];

        foreach ($listKeywordExist as $keyword) {
            $listKeywordName[$keyword[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME]] = $keyword[Models\MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID];
        }

        foreach ($listAdsKeyword as $adsKeyword) {
            $checkKeywordName = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAdsKeyword::getByKeywordName($shopAds[Models\ModelBusiness\ShopAds::COL_ID], $adsKeyword['keyword']));
            if (!$checkKeywordName) {
                $recordInsert = Models\ModelBusiness\ShopAdsKeyword::buildDataInsert(
                    $shopAds[Models\ModelBusiness\ShopAds::COL_ID],
                    $listKeywordName[$adsKeyword['keyword']] ?? null,
                    $adsKeyword['keyword'],
                    $adsKeyword['status'],
                    $adsKeyword['match_type'],
                    $adsKeyword['price'],
                    $adsKeyword,
                    bin2hex($adsKeyword['keyword'])
                );
                $isInsetAdsKeywordSuccess = Models\ModelBusiness\ShopAdsKeyword::insertData($recordInsert);
                if (!$isInsetAdsKeywordSuccess) {
                    $dataLog = [
                        "ShopChannelId" => $shopChannelId,
                        "Error" => 'Insert shop ads keyword failure!!',
                        "Data" => $recordInsert
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
                    return;
                }
            } else {
                $recordUpdate = Models\ModelBusiness\ShopAdsKeyword::buildDataUpdate(
                    $listKeywordName[$adsKeyword['keyword']] ?? null,
                    $adsKeyword['status'],
                    $adsKeyword['match_type'],
                    $adsKeyword['price'],
                    $adsKeyword
                );
                $isUpdateAdsKeywordSuccess = Models\ModelBusiness\ShopAdsKeyword::updateById($checkKeywordName[Models\ModelBusiness\ShopAdsKeyword::COL_ID], $recordUpdate);
                if (!$isUpdateAdsKeywordSuccess) {
                    $dataLog = [
                        "ShopChannelId" => $shopChannelId,
                        "Error" => 'Update shop ads keyword failure!!',
                        "Data" => $recordUpdate
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
                    return;
                }
            }
        }
    }

}