<?php

namespace App\Console\Commands;

use App\Library\QueueName;
use App\Library;
use App\Models;
use Illuminate\Console\Command;
use App\Jobs;

class CrawlTokopediaProductAdsPerformance extends Command
{
    /**
     * @uses:
     * php artisan crawler-tokopedia-product-ads-performance --shop=shop-channel-id --dateFrom=date-from --dateTo=date-to
     * example: php artisan crawler-tokopedia-product-ads-performance --shop=590 --dateFrom=2020-02-05 --dateTo=2020-02-06
     */

    protected $signature = 'crawler-tokopedia-product-ads-performance {--shop=} {--dateFrom=} {--dateTo=}';
    protected $description = 'Crawl product ads performance from Tokopedia to MySQL database (with out shop channel id will crawl all tokopedia shop)';

    public function handle()
    {
        // Read input params
        $shopId = $this->option('shop');
        $dateFrom = $this->option('dateFrom') ?? date('Y-m-d');
        $dateTo = $this->option('dateTo') ?? date('Y-m-d', strtotime($dateFrom . '+1 days'));

        // Check input and push queue
        if (!$shopId) {
            $allTokopediaShop = Models\ShopChannel::getAllShopChannelIsTokopedia();
            foreach ($allTokopediaShop as $tokopediaShop) {
                $crawlCredential = $this->checkLoginTokopediaService($tokopediaShop->shop_channel_api_credential);
                if (!$crawlCredential) {
                    Library\LogError::getInstance()->slack('Job CrawlTokopediaProductAdsPerformance - Tokopedia - Shop Channel Id: ' . $tokopediaShop->shop_channel_id . ' - Login failure');
                    continue;
                }
                $jobCrawlTokopediaProductAdsPerformance = new Jobs\CrawlTokopediaProductAdsPerformance($tokopediaShop->shop_channel_id, $crawlCredential, $dateFrom, $dateTo);
                dispatch($jobCrawlTokopediaProductAdsPerformance->onQueue(QueueName::getByClass(Jobs\CrawlTokopediaProductAdsPerformance::class)));
            }
        } else {
            $getShopChannelInfo = Models\ShopChannel::getById($shopId);
            $crawlCredential = $this->checkLoginTokopediaService($getShopChannelInfo->shop_channel_api_credential);
            if (!$crawlCredential) {
                Library\LogError::getInstance()->slack('Job CrawlTokopediaProductAdsPerformance - Tokopedia - Crawl Product ads - Shop Channel Id: ' . $shopId . ' - Login failure');
                return null;
            }
            $jobCrawlTokopediaProductAdsPerformance = new Jobs\CrawlTokopediaProductAdsPerformance($shopId, $crawlCredential, $dateFrom, $dateTo);
            dispatch($jobCrawlTokopediaProductAdsPerformance->onQueue(QueueName::getByClass(Jobs\CrawlTokopediaProductAdsPerformance::class)));
        }
    }

    /**
     * @param $shopChannelCredential
     * @return mixed
     */
    private function checkLoginTokopediaService($shopChannelCredential)
    {
        // Verify credential
        $shopChannelCredential = json_decode($shopChannelCredential, true);
        if (!isset($shopChannelCredential['shopId'])) {
            return null;
        }
        if (!isset($shopChannelCredential['tkpdUserId'])) {
            return null;
        }
        if (!isset($shopChannelCredential['cookieString'])) {
            return null;
        }
        return $shopChannelCredential;
    }
}