<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 16:14
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Exception;


class CampaignBudget extends Command
{

    /**
     * @uses php artisan campaign-budget --shop=shop-id --channel=channel-id
     * example: php artisan campaign-budget --shop=62 --channel=3
     */
    protected $signature = 'campaign-budget {--shop=} {--channel=}';

    protected $description = 'Check budget campaign for pause product ads and inactive campaign';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        Library\Common::setTimezoneByChannelId($channelId);

        $shopChannelId = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID];
        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        $today = date('Y-m-d');
        $campaignData = Library\Formater::stdClassToArray(Models\MakProgrammaticCampaign::searchByShopChannelId($shopChannelId, Models\ConfigActive::ACTIVE));
        foreach ($campaignData as $campaign) {
            $campaignId = $campaign[Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID];
            $campaignBudget = $campaign[Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_BUDGET];
            $dateFrom = $campaign[Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_FROM];
            $dateTo = $campaign[Models\MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_TO] ?? $today;

            if ($campaignBudget === null) continue;

            $isNotExpired = $dateFrom && $dateTo && $dateFrom <= $today && $dateTo >= $today;
            if ( ! $isNotExpired) continue;

            $adminId = Models\Admin::EPSILO_SYSTEM;
            Models\MakProgrammaticCampaign::updateStatusById($campaignId, Models\ConfigActive::INACTIVE, $adminId);

            $campaignService = new Services\ProgrammaticCampaign($campaignId);
            $totalCostSpend = $campaignService->calculateCostSpend($dateFrom, $today);
            if ($totalCostSpend <= floatval($campaignBudget)) continue;

            $productAdsIdArray = [];
            $dataProductAds = $campaignService->dataProductAds;
            foreach ($dataProductAds as $productAds) {
                $productAdsId = $productAds[Models\ProductAds::COL_PRODUCT_ADS_ID];
                $adsId = $productAds[Models\ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID];
                $state = $productAds[Models\ProductAds::COL_PRODUCT_ADS_STATE];

                if ( ! ($state === 'ongoing' && $adsId)) continue;

                if ( ! $isLogin) $isLogin = $crawler->login();
                if ( ! $isLogin) $isLogin = $crawler->login();
                if ( ! $isLogin) $isLogin = $crawler->login();

                $isUpdated = $crawler->turnSkuWithStatus($adsId, Library\Crawler\Channel\Shopee::SKU_PAUSE);
                if ( ! $isUpdated) continue;

                $productAdsIdArray[] = $productAdsId;
            }

            if ( ! $productAdsIdArray) continue;
            Models\ProductAds::updateStateByIdArray($productAdsIdArray, Models\ProductAds::STATE_PAUSED, $adminId);

        }
    }
}