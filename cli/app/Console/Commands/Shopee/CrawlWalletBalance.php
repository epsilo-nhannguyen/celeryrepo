<?php


namespace App\Console\Commands\Shopee;


use App\Library\Crawler\Channel\Shopee;
use App\Models\ShopChannel;
use Illuminate\Console\Command;

class CrawlWalletBalance extends Command
{
    protected $signature = 'shopee:crawl_wallet_balance {--shopId=}';
    protected $description = 'crawl Snapshot wallet balance {--shopId=}';

    public function handle()
    {

        $shopChannelId = intval($this->option('shopId'));

        if ($shopChannelId) {
            $arrayShopChannelId = [$shopChannelId];
        } else {
            $arrayShopChannelId = ShopChannel::getAllShopActive_channelShopee()->pluck('shop_channel_id')->all();
        }

        foreach ($arrayShopChannelId as $_shopChannelId) {
            $shopee = new Shopee($_shopChannelId);
            $isSuccess = $shopee->login();
            if (!$isSuccess) {
                $isSuccess = $shopee->login();
            }
            if (!$isSuccess) {
                $isSuccess = $shopee->login();
            }

            if (!$isSuccess) {
                continue;
            }

            $jsonWallet = $shopee->getWallet();

            $dataWallet = json_decode($jsonWallet, true);
        }
    }
}