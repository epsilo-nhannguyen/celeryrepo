<?php


namespace App\Console\Commands\Shopee;


use App\Models\ShopChannel;
use App\Models\ShopeeWalletBalance;
use App\Models\Venture;
use App\Services\ShopeeKeywordWalletBalance;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Console\Command;

class FillShopeeWalletBalance extends Command
{
    protected $signature = 'shopee:fill_shopee_wallet_balance {dateFrom} {dateTo}';

    protected $description = 'Fill wallet from elastic_search to mysql {dateFrom} {dateTo}';

    public function handle()
    {
        $allShopChannel = ShopChannel::getAllShopActive_channelShopee();
        $assocVenture = Venture::getAll()->pluck('venture_timezone', 'venture_id');
        $dateFrom = $this->argument('dateFrom');
        $dateTo = $this->argument('dateTo');

        foreach ($allShopChannel as $shopChannel) {
            date_default_timezone_set($assocVenture[$shopChannel->fk_venture]);
            $shopChannelId = $shopChannel->shop_channel_id;

            $walletService = new ShopeeKeywordWalletBalance($shopChannelId);
            $datePeriod = CarbonPeriod::create()->setStartDate($dateFrom)->setEndDate($dateTo);
            foreach ($datePeriod as $_date) {
                /** @var Carbon $_date */
                $walletData = $walletService->getData(
                    strtotime($_date->format('Y-m-d 00:00:00')),
                    strtotime($_date->format('Y-m-d 23:59:59'))
                );
                $walletRows = $walletData->map(function ($item) {
                    return [
                        'shop_channel_id' => $item['shopChannelId'],
                        'value' => $item['wallet'],
                        'collected_at' => $item['Timestamp'],
                    ];
                });

                if (!$walletRows->count()) {
                    continue;
                }

                $result = ShopeeWalletBalance::insertMulti($walletRows->toArray());
                if (!$result) {
                    $this->line(sprintf(
                        'shop %d and date %s insert fail',
                        $shopChannelId, $_date->format('Y-m-d')
                    ));
                }
            }
        }
    }
}