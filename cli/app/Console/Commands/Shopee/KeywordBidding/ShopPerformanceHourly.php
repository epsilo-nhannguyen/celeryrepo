<?php


namespace App\Console\Commands\Shopee\KeywordBidding;

use App\Jobs\Shopee\KeywordBidding\CrawlShopPerformanceByHour;
use App\Models;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Library\Crawler;
use App\Library\QueueName;

class ShopPerformanceHourly extends Command
{
    protected $signature = 'shopee:keyword_bidding:shop_performance_hourly {--arrayShopId=} {--date=}';

    protected $description = 'Crawl Shop performance hourly {--arrayShopId=} {--date=}';

    public function handle()
    {
        $arrayShopId = $this->option('arrayShopId');

        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $allShopChannel = Models\ShopChannel::getAllShopActive_channelShopee();

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }

        $arrayShopChannelId = $allShopChannel->pluck('shop_channel_id')->all();

        #prepare data
        $assocVentureIdTimeZone = Models\Venture::getAll()->pluck('venture_timezone', 'venture_id')->all();

        $date = $this->option('date');

        if (strpos($date, ',') !== false) {
            list($dateFrom, $dateTo) = explode(',', $date);
        } else {

            if (strlen($date)) {
                $dateFrom = $dateTo = $date;
            } else {
                $dateFrom = $dateTo = date('Y-m-d');
            }
        }

        $startDate = new Carbon($dateFrom);
        $endDate = new Carbon($dateTo);
        $listDate = [];
        while ($startDate->lte($endDate)) {
            $listDate[] = $startDate->toDateString();

            $startDate->addDay();
        }

        foreach ($allShopChannel as $shopChannel) {
            $shopChannelId = $shopChannel->shop_channel_id;
            $shopee = new Crawler\Channel\ShopeeKeywordBidding($shopChannelId);
            $isSuccess = $shopee->login();
            if (!$isSuccess) {
                $isSuccess = $shopee->login();
            }
            if (!$isSuccess) {
                $isSuccess = $shopee->login();
            }
            if (!$isSuccess) {
                continue;
            }

            $ventureId = $shopChannel->fk_venture;
            $timezone = $assocVentureIdTimeZone[$ventureId];
            foreach ($listDate as $dateInLoop) {
                $job = new CrawlShopPerformanceByHour($shopee, $dateInLoop, $timezone);
                dispatch($job->onQueue(QueueName::SHOPEE_KEYWORD_BIDDING_CRAWL_PERFORMANCE_HOUR_BY_SHOP));
            }
        }
    }
}