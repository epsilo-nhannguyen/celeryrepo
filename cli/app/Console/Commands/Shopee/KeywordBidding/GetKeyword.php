<?php


namespace App\Console\Commands\Shopee\KeywordBidding;

use App\Models;
use Illuminate\Console\Command;

class GetKeyword extends Command
{
    protected $signature = 'shopee:keyword_bidding:get_keyword {--arrayShopId=}';

    protected $description = 'Crawl keyword product ads {--arrayShopId=}';

    public function handle()
    {
        $arrayShopId = $this->option('arrayShopId');

        $arrayShopId = explode(',', $arrayShopId);
        $arrayShopId = array_filter($arrayShopId);

        $allShopChannel = Models\ShopChannel::getAllShopActive_channelShopee();

        if ($arrayShopId) {
            $allShopChannel = $allShopChannel->filter(function ($item) use ($arrayShopId) {
                return in_array($item->fk_shop_master, $arrayShopId);
            });
        }

        $arrayShopChannelId = $allShopChannel->pluck('shop_channel_id')->all();

        #prepare data
        $assocVentureIdTimeZone = Models\Venture::getAll()->pluck('venture_timezone', 'venture_id')->all();

        foreach ($allShopChannel as $shopChannel) {
            print_r($shopChannel);die;
        }
    }
}