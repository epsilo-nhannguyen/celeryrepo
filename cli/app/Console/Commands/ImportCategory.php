<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 11:22
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use Exception;
use Illuminate\Console\Command;


class ImportCategory extends Command
{

    /**
     * @uses php artisan import-category --shop=shop-id --channel=channel-id
     * example: php artisan import-category --shop=62 --channel=3
     */
    protected $signature = 'import-category {--shop=} {--channel=}';

    protected $description = 'Import category from mongo to mysql database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        $channelCode = $channelInfo[Models\Channel::COL_CHANNEL_CODE];
        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];

        $ventureInfo = Library\Formater::stdClassToArray(Models\Venture::getById($ventureId));
        $ventureCode = $ventureInfo[Models\Venture::COL_VENTURE_CODE];

        $shopInfo = Library\Formater::stdClassToArray(Models\ShopMaster::getById($shopId));
        $organizationId = $shopInfo[Models\ShopMaster::COL_FK_ORGANIZATION];

        Library\Common::setTimezoneByVentureId($ventureId);

        $categoryData = Models\Mongo\Category::searchByShopIdChannelId($shopId, $channelId);

        $arrayIdentify = [];
        foreach ($categoryData as $categoryObj) {
            $categoryObj = Library\Formater::stdClassToArray($categoryObj);
            $category = $categoryObj['data'];
            $identify = trim($categoryObj['identify']);

            Models\ChannelCategory::import(
                $organizationId,
                $channelCode.'-'.$ventureCode.'-'.$identify,
                $category['level_1'],
                $category['level_2'],
                $category['level_3'],
                $category['level_4'],
                $category['level_5'],
                $channelId,
                $identify
            );

            $arrayIdentify[] = $identify;
        }

        if ( ! $arrayIdentify) return;

        Models\Mongo\Category::updateImported($shopId, $channelId, $arrayIdentify);

    }
}