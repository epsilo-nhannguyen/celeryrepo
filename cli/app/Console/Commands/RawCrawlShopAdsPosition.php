<?php

namespace App\Console\Commands;

use App\Models;
use App\Library;
use Illuminate\Console\Command;

class RawCrawlShopAdsPosition extends Command
{
    /**
     * @uses:
     * php artisan raw-crawler-shop-ads-position
     * example: php artisan raw-crawler-shop-ads-position
     */

    protected $signature = 'raw-crawler-shop-ads-position';
    protected $description = 'Raw shop ads position for keyword from Shopee to MySQL database';

    public function handle()
    {
        $allKeyword = Library\Formater::stdClassToArray(Models\ModelBusiness\ShopAdsKeyword::selectAllKeywordWithVentureAndAdsId());
        foreach ($allKeyword as $keyword) {
            $isAdsPosition = Library\Crawler\Channel\Frontend\Shopee::isShopAdsPosition($keyword[Models\Channel::COL_FK_VENTURE], $keyword[Models\ModelBusiness\ShopAdsKeyword::COL_KEYWORD_NAME]);
            if (!$isAdsPosition)
                $isAdsPosition = Library\Crawler\Channel\Frontend\Shopee::isShopAdsPosition($keyword[Models\Channel::COL_FK_VENTURE], $keyword[Models\ModelBusiness\ShopAdsKeyword::COL_KEYWORD_NAME]);
            if (!$isAdsPosition)
                $isAdsPosition = Library\Crawler\Channel\Frontend\Shopee::isShopAdsPosition($keyword[Models\Channel::COL_FK_VENTURE], $keyword[Models\ModelBusiness\ShopAdsKeyword::COL_KEYWORD_NAME]);
            if (!$isAdsPosition) continue;
                //throw new Exception('Can not connect channel');

            if (!isset($isAdsPosition['data']['users'])) {
                #print_r($isAdsPosition);
                #throw new Exception('Channel modify params response');
                continue;
            }
                #continue;

            $data = $isAdsPosition['data']['users'] ?? [];
            $data = current($data);

            $adsId = $data['adsid'] ?? 0;

            if ( ! count($data)) {
                $isUpdateSuccess = Models\ModelBusiness\ShopAdsKeyword::updateIsShopAdsPosition($keyword[Models\ModelBusiness\ShopAdsKeyword::COL_ID], 0);
                if (!$isUpdateSuccess) {
                    $dataLog = [
                        "Error" => 'Update is shop ads position failure!!!',
                        "isTop" => false,
                        "Data" => $keyword
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
                }
                continue;
            }
            if ($adsId == $keyword[Models\ModelBusiness\ShopAds::COL_ADSID]) {
                $isUpdateSuccess = Models\ModelBusiness\ShopAdsKeyword::updateIsShopAdsPosition($keyword[Models\ModelBusiness\ShopAdsKeyword::COL_ID], 1);
                if (!$isUpdateSuccess) {
                    $dataLog = [
                        "Error" => 'Update is shop ads position failure!!!',
                        "isTop" => true,
                        "Data" => $keyword
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
                }
            } else {
                $isUpdateSuccess = Models\ModelBusiness\ShopAdsKeyword::updateIsShopAdsPosition($keyword[Models\ModelBusiness\ShopAdsKeyword::COL_ID], 0);
                if (!$isUpdateSuccess) {
                    $dataLog = [
                        "Error" => 'Update is shop ads position failure!!!',
                        "isTop" => false,
                        "Data" => $keyword
                    ];
                    Library\LogError::logErrorToTextFile($dataLog);
                }
            }
        }
    }
}