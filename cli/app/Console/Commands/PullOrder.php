<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 08/10/2019
 * Time: 11:06
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use App\Jobs;
use Illuminate\Console\Command;
use Exception;


class PullOrder extends Command
{

    /**
     * @uses:
     * php artisan pull-order --shop=shop-id --channel=channel-id --dateFrom=date-from --dateTo=date-to --subAccount=sub-account
     * example: php artisan pull-order --shop=62 --channel=3 --dateFrom=2019-09-01 --dateTo=2019-09-30 --subAccount=0
     */
    protected $signature = 'pull-order {--shop=} {--channel=} {--dateFrom=0} {--dateTo=0} {--subAccount=0}';

    protected $description = 'Pull order to mongo database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');
        $dateFrom = $this->option('dateFrom');
        $dateTo = $this->option('dateTo');
        $isSubAccount = $this->option('subAccount');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        Library\Common::setTimezoneByChannelId($channelId);

        $shopChannelMongo = Models\Mongo\ShopChannel::getByShopIdChannelId($shopId, $channelId);
        if (empty($shopChannelMongo['last_time_raw'])) {
            $maxUpdateAt = Models\Mongo\SaleOrder::getMaxUpdatedAt($shopId, $channelId);
        } else {
            $maxUpdateAt = $shopChannelMongo['last_time_raw'];
        }

        if ( ! $maxUpdateAt) {
            $maxUpdateAt = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_CREATED_AT];
        }

        $lastTimePullOrders = intval($maxUpdateAt);

        if ($dateFrom) $lastTimePullOrders = strtotime($dateFrom);

        if (!$dateTo) {
            $dateTo = strtotime('now');
        } else {
            $dateTo = strtotime($dateTo);
        }

        $beforeLastTimePullOrders1Min = strtotime('-1 minutes', $lastTimePullOrders);
        $message = json_encode([
            'shopId' => $shopId,
            'channelId' => $channelId,
            'updateAtFrom' => $beforeLastTimePullOrders1Min,
            'updateAtTo' => $dateTo,
            'isSubAccount' => $isSubAccount,
        ]);

        dispatch((new Jobs\PullOrder($message))->onQueue(Library\QueueName::getByClass(Jobs\PullOrder::class)));

    }

}