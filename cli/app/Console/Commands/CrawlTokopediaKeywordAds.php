<?php

namespace App\Console\Commands;

use App\Library\QueueName;
use App\Jobs;
use App\Models;
use Illuminate\Console\Command;
use App\Library;

class CrawlTokopediaKeywordAds extends Command
{
    /**
     * @uses:
     * php artisan crawler-tokopedia-keyword-ads --shop=shop-channel-id
     * example: php artisan crawler-tokopedia-keyword-ads --shop=590
     */

    protected $signature = 'crawler-tokopedia-keyword-ads {--shop=}';
    protected $description = 'Crawl keyword ads from Tokopedia to MySQL database';

    public function handle()
    {
        // Read input params
        $shopId = $this->option('shop');

        // Check input and push queue
        if (!$shopId) {
            $allTokopediaShop = Models\ShopChannel::getAllShopChannelIsTokopedia();
            foreach ($allTokopediaShop as $tokopediaShop) {
                $crawlCredential = $this->checkLoginTokopediaService($tokopediaShop->shop_channel_api_credential);
                if (!$crawlCredential) {
                    Library\LogError::getInstance()->slack('Job CrawlTokopediaKeywordAds - Tokopedia - Shop Channel Id: ' . $tokopediaShop->shop_channel_id . ' - Login failure');
                    continue;
                }
                $jobCrawlTokopediaKeywordAds = new Jobs\CrawlTokopediaKeywordAds($tokopediaShop->shop_channel_id, $crawlCredential);
                dispatch($jobCrawlTokopediaKeywordAds->onQueue(QueueName::getByClass(Jobs\CrawlTokopediaKeywordAds::class)));
            }
        } else {
            $getShopChannelInfo = Models\ShopChannel::getById($shopId);
            $crawlCredential = $this->checkLoginTokopediaService($getShopChannelInfo->shop_channel_api_credential);
            if (!$crawlCredential) {
                Library\LogError::getInstance()->slack('Job CrawlTokopediaKeywordAds - Tokopedia - Crawl group ads - Shop Channel Id: ' . $shopId . ' - Login failure');
                return null;
            }
            $jobCrawlTokopediaKeywordAds = new Jobs\CrawlTokopediaKeywordAds($shopId, $crawlCredential);
            dispatch($jobCrawlTokopediaKeywordAds->onQueue(QueueName::getByClass(Jobs\CrawlTokopediaKeywordAds::class)));
        }
    }

    /**
     * @param $shopChannelCredential
     * @return mixed
     */
    private function checkLoginTokopediaService($shopChannelCredential)
    {
        // Verify credential
        $shopChannelCredential = json_decode($shopChannelCredential, true);
        if (!isset($shopChannelCredential['shopId'])) {
            return null;
        }
        if (!isset($shopChannelCredential['tkpdUserId'])) {
            return null;
        }
        if (!isset($shopChannelCredential['cookieString'])) {
            return null;
        }
        return $shopChannelCredential;
    }
}