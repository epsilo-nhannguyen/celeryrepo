<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 08/10/2019
 * Time: 11:28
 */

namespace App\Console\Commands;


use App\Library;
use App\Models;
use App\Jobs;
use Illuminate\Console\Command;
use Exception;


class PullProduct extends Command
{

    /**
     * @uses: php artisan pull-product --shop=shop-id --channel=channel-id --subAccount=sub-account
     * example: php artisan pull-product --shop=62 --channel=3 --subAccount=0
     */
    protected $signature = 'pull-product {--shop=} {--channel=} {--subAccount=0}';

    protected $description = 'Pull product to mongo database';

    public function handle()
    {
        $shopId = $this->option('shop');
        $channelId = $this->option('channel');
        $isSubAccount = $this->option('subAccount');

        if ( ! $shopId ||  ! $channelId) {
            throw new Exception('Please input shop and channel');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));
        if ( ! $shopChannelInfo) {
            throw new Exception('Shop channel info not found');
        }

        Library\Common::setTimezoneByChannelId($channelId);

        $shopChannelMongo = Models\Mongo\ShopChannel::getByShopIdChannelId($shopId, $channelId);
        if (empty($shopChannelMongo['last_time_pull_product'])) {
            $maxUpdateAt = Models\Mongo\Product::getMaxUpdatedAt($shopId, $channelId);
        } else {
            $maxUpdateAt = $shopChannelMongo['last_time_pull_product'];
        }

        $beforeLastTimePull1Min = null;
        if ($maxUpdateAt) {
            $beforeLastTimePull1Min = strtotime('-1 minutes', $maxUpdateAt);
        }

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        $channelCode = $channelInfo[Models\Channel::COL_CHANNEL_CODE];
        if ($channelCode == Models\Channel::SHOPEE_CODE) {
            if ( ! $isSubAccount) {
                $apiShopee = new Library\Extend\Channel\Shopee($shopId, $channelId);
                $listItem = $apiShopee->catchupListingProduct($beforeLastTimePull1Min, strtotime('now'));
                $listItemId = array_column((array) $listItem, 'item_id');
                $arrayChunkItemId = array_chunk($listItemId, 25);

                foreach ($arrayChunkItemId as $chunkItemId) {
                    $message = json_encode([
                        'shopId' => $shopId,
                        'channelId' => $channelId,
                        'arrayItemId' => $chunkItemId,
                    ]);

                    dispatch((new Jobs\PullProduct($message))->onQueue(Library\QueueName::getByClass(Jobs\PullProduct::class)));
                }
            } else {
                $message = json_encode([
                    'shopId' => $shopId,
                    'channelId' => $channelId,
                    'isSubAccount' => $isSubAccount,
                ]);

                dispatch((new Jobs\PullProduct($message))->onQueue(Library\QueueName::getByClass(Jobs\PullProduct::class)));
            }
        } else {
            $message = json_encode([
                'shopId' => $shopId,
                'channelId' => $channelId,
                'updateAtFrom' => $beforeLastTimePull1Min,
                'updateAtTo' => strtotime('now'),
            ]);

            dispatch((new Jobs\PullProduct($message))->onQueue(Library\QueueName::getByClass(Jobs\PullProduct::class)));
        }
    }

}