<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 02/10/2019
 * Time: 20:30
 */

namespace App\Helpers;

use Pusher\Pusher;

class PusherHelper
{

    /**
     * Pusher constructor.
     */
    private $pusher;

    public function __construct()
    {
        $options = config('pusher.options');
        $this->pusher = new Pusher(
            env('PUSHER_APP_KEY', '7012d7d52be60c8ce01e'),
            env('PUSHER_APP_SECRET', '28249b7045ffdd2e9c77'),
            env('PUSHER_APP_ID', '943698'),
            $options
        );
    }

    /**
     * @param $channelName
     * @param $eventName
     * @param $data
     * @throws \Pusher\PusherException
     */
    public function triggerAction($channelName, $eventName, $data)
    {
        $this->pusher->trigger($channelName, $eventName, $data);
    }
}