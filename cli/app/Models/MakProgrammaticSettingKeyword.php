<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:58
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakProgrammaticSettingKeyword extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_setting_keyword';

    const COL_MAK_PROGRAMMATIC_SETTING_KEYWORD_ID = 'mak_programmatic_setting_keyword_id';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT = 'fk_mak_programmatic_keyword_product';
    const COL_FK_MAK_PROGRAMMATIC_RULE = 'fk_mak_programmatic_rule';
    const COL_MAK_PROGRAMMATIC_SETTING_KEYWORD_CREATED_AT = 'mak_programmatic_setting_keyword_created_at';
    const COL_MAK_PROGRAMMATIC_SETTING_KEYWORD_CREATED_BY = 'mak_programmatic_setting_keyword_created_by';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_SETTING_KEYWORD_ID;
    public $timestamps = false;

    /**
     * @var int
     */
    const IS_DELETED = 1;

    /**
     * @var int
     */
    const IS_EXIST = 0;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * search By Shop Channel Id
     * @param int $shopChannelId
     * @param array $actionIdArray
     * @return mixed
     */
    public static function searchByShopChannelId($shopChannelId, $actionIdArray)
    {
        $today = date('Y-m-d');

        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticRule::TABLE_NAME,
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_RULE
            )
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_VALIDITY_FROM, '<=', $today)
            ->where(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_VALIDITY_TO, '>=', $today)
            ->where(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_FK_CONFIG_ACTIVE, ConfigActive::ACTIVE)
            ->whereIn(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_FK_MAK_PROGRAMMATIC_ACTION, $actionIdArray)
            ->select(
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID,
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_EXTEND,
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_FK_MAK_PROGRAMMATIC_ACTION
            )
            ->orderBy(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID)
            ->distinct()
            ->get()
        ;
    }

    /**
     * search By Rule Id
     * @param int $ruleId
     * @return mixed
     */
    public static function searchByRuleId($ruleId)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticRule::TABLE_NAME,
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_RULE
            )
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                MakProgrammaticKeyword::TABLE_NAME,
                MakProgrammaticKeyword::TABLE_NAME.'.'.MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_MAK_PROGRAMMATIC_KEYWORD
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->join(
                ProductAds::TABLE_NAME,
                ProductAds::TABLE_NAME.'.'.ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL,
                '=',
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->where(self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_RULE, $ruleId)
            ->select(
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_BIDDING_PRICE,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_AT,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CURRENT_RANK,
                MakProgrammaticKeyword::TABLE_NAME.'.'.MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID,
                MakProgrammaticKeyword::TABLE_NAME.'.'.MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_SELLER_SKU,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK,
                ProductAds::TABLE_NAME.'.'.ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID
            )
            ->get()
            ;
    }

}