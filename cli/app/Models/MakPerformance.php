<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 18:04
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakPerformance extends Library\ModelBusiness
{
    const TABLE_NAME = 'mak_performance';

    const COL_MAK_PERFORMANCE_ID = 'mak_performance_id';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT = 'fk_mak_programmatic_keyword_product';
    const COL_MAK_PERFORMANCE_DATE = 'mak_performance_date';
    const COL_MAK_PERFORMANCE_GMV = 'mak_performance_gmv';
    const COL_MAK_PERFORMANCE_VIEW = 'mak_performance_view';
    const COL_MAK_PERFORMANCE_CLICK_RATE = 'mak_performance_click_rate';
    const COL_MAK_PERFORMANCE_ORDERS = 'mak_performance_orders';
    const COL_MAK_PERFORMANCE_QUERY = 'mak_performance_query';
    const COL_MAK_PERFORMANCE_PRODUCT_SOLD = 'mak_performance_product_sold';
    const COL_MAK_PERFORMANCE_AVG_POSITION = 'mak_performance_avg_position';
    const COL_MAK_PERFORMANCE_EXPENSE = 'mak_performance_expense';
    const COL_MAK_PERFORMANCE_CLICKS = 'mak_performance_clicks';
    const COL_MAK_PERFORMANCE_CREATED_AT = 'mak_performance_created_at';
    const COL_MAK_PERFORMANCE_UPDATED_AT = 'mak_performance_updated_at';
    const COL_FK_MAK_PROGRAMMATIC_CAMPAIGN = 'fk_mak_programmatic_campaign';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PERFORMANCE_ID;
    public $timestamps = false;

    use Master;

    /**
     * build Data Insert
     * @param $keywordProductId
     * @param $date
     * @param $gmv
     * @param $view
     * @param $clickRate
     * @param $orders
     * @param $search
     * @param $productSold
     * @param $avgPosition
     * @param $expense
     * @param $clicks
     * @param $campaignId
     * @return array
     */
    public static function buildDataInsert(
        $keywordProductId, $date, $gmv, $view, $clickRate, $orders, $search, $productSold, $avgPosition, $expense, $clicks, $campaignId
    ) {
        return [
            self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT => $keywordProductId,
            self::COL_MAK_PERFORMANCE_DATE => $date,
            self::COL_MAK_PERFORMANCE_GMV => $gmv,
            self::COL_MAK_PERFORMANCE_VIEW => $view,
            self::COL_MAK_PERFORMANCE_CLICK_RATE => $clickRate,
            self::COL_MAK_PERFORMANCE_ORDERS => $orders,
            self::COL_MAK_PERFORMANCE_QUERY => $search,
            self::COL_MAK_PERFORMANCE_PRODUCT_SOLD => $productSold,
            self::COL_MAK_PERFORMANCE_AVG_POSITION => $avgPosition,
            self::COL_MAK_PERFORMANCE_EXPENSE => $expense,
            self::COL_MAK_PERFORMANCE_CLICKS => $clicks,
            self::COL_MAK_PERFORMANCE_CREATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_MAK_PERFORMANCE_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN => $campaignId
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * update Case
     * @param string $sql
     * @return mixed
     */
    public static function updateCase($sql)
    {
        return DB::statement($sql);
    }

    /**
     * search By Shop Channel Id And Date
     * @param int $shopChannelId
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndDate($shopChannelId, $date)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(MakPerformance::TABLE_NAME.'.'.MakPerformance::COL_MAK_PERFORMANCE_DATE, $date)
            ->select(
                self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_ID,
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT,
                self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_DATE
            )
            ->get()
        ;
    }

    /**
     * sum Column By Date Array
     * @param string $column
     * @param array $keywordProductIdArray
     * @param array $dateArray
     * @return mixed
     */
    public static function sumColumnByDateArray($column, $keywordProductIdArray, $dateArray)
    {
        if ( ! $keywordProductIdArray) return 0;

        return DB::table(self::TABLE_NAME)
            ->whereIn(self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT, $keywordProductIdArray)
            ->whereIn(self::COL_MAK_PERFORMANCE_DATE, $dateArray)
            ->sum($column)
        ;
    }

    /**
     * sum Column By Date From To
     * @param string $column
     * @param int|array $keywordProductId
     * @param string $dateFrom
     * @param string $dateTo
     * @return mixed
     */
    public static function sumColumnByDateFromTo($column, $keywordProductId, $dateFrom, $dateTo)
    {
        if ( ! $keywordProductId) return 0;

        return DB::table(self::TABLE_NAME)
            ->when(is_array($keywordProductId), function ($query) use ($keywordProductId) {
                return $query->whereIn(self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT, $keywordProductId);
            }, function ($query) use ($keywordProductId) {
                return $query->where(self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT, $keywordProductId);
            })
            ->whereBetween(self::COL_MAK_PERFORMANCE_DATE, [$dateFrom, $dateTo])
            ->sum($column)
        ;
    }

    /**
     * sum Performance By Date
     * @param int $shopChannelId
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public static function sumPerformanceByDate($shopChannelId, $date)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_DATE, $date)
            ->select(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_VIEW.') as '.self::COL_MAK_PERFORMANCE_VIEW),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_CLICKS.') as '.self::COL_MAK_PERFORMANCE_CLICKS),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_CLICK_RATE.') as '.self::COL_MAK_PERFORMANCE_CLICK_RATE),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_PRODUCT_SOLD.') as '.self::COL_MAK_PERFORMANCE_PRODUCT_SOLD),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_GMV.') as '.self::COL_MAK_PERFORMANCE_GMV),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_EXPENSE.') as '.self::COL_MAK_PERFORMANCE_EXPENSE)
            )
            ->groupBy(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN
            )
            ->get()
        ;
    }

    /**
     * search By Shop Channel Id And View Greater 0
     * @param int $shopChannelId
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndViewGreater0($shopChannelId, $date)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_VIEW, '>', 0)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_DATE, $date)
            ->select(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->get()
        ;
    }

    /**
     * search By Shop Channel Id
     * @param int $shopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelId($shopChannelId, $dateFrom, $dateTo)
    {
        $model = DB::connection('master_business')->table('mak_performance')
            ->join(
                'mak_programmatic_keyword_product',
                'mak_programmatic_keyword_product.mak_programmatic_keyword_product_id',
                '=',
                'mak_performance.fk_mak_programmatic_keyword_product'
            )
            ->join(
                'product_shop_channel',
                'product_shop_channel.product_shop_channel_id',
                '=',
                'mak_programmatic_keyword_product.fk_product_shop_channel'
            )
            ->where('product_shop_channel.fk_shop_channel', $shopChannelId)
            ->whereBetween('mak_performance.mak_performance_date', [$dateFrom, $dateTo])
            ->select([
                'mak_performance.mak_performance_date',
                'mak_programmatic_keyword_product_id',
                'fk_mak_programmatic_keyword',
                'product_shop_channel_id',
                'mak_performance.mak_performance_expense',
                'mak_performance.mak_performance_gmv',
                'mak_performance.mak_performance_product_sold',
                'mak_performance.mak_performance_view',
                'mak_performance.mak_performance_avg_position',
                DB::raw('mak_performance_view * mak_performance_avg_position as avg_weighted'),
            ])
        ;

        return $model->get();
    }

    /**
     * search For Sku Keyword By Shop Channel Id
     * @param int $shopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function searchForSkuKeywordByShopChannelId($shopChannelId, $dateFrom, $dateTo)
    {
        $model = DB::connection('master_business')->table('mak_performance')
            ->join(
                'mak_programmatic_keyword_product',
                'mak_programmatic_keyword_product.mak_programmatic_keyword_product_id',
                '=',
                'mak_performance.fk_mak_programmatic_keyword_product'
            )
            ->join(
                'product_shop_channel',
                'product_shop_channel.product_shop_channel_id',
                '=',
                'mak_programmatic_keyword_product.fk_product_shop_channel'
            )
            ->join(
                'mak_programmatic_keyword',
                'mak_programmatic_keyword.mak_programmatic_keyword_id',
                '=',
                'mak_programmatic_keyword_product.fk_mak_programmatic_keyword'
            )
            ->where('product_shop_channel.fk_shop_channel', $shopChannelId)
            ->whereBetween('mak_performance.mak_performance_date', [$dateFrom, $dateTo])
            ->select([
                'mak_performance.mak_performance_date',
                'mak_programmatic_keyword_product_id',
                'mak_programmatic_keyword_id',
                'mak_programmatic_keyword_name',
                'product_shop_channel_id',
                'product_shop_channel_item_id',
                'mak_performance.mak_performance_view',
                'mak_performance.mak_performance_clicks',
                'mak_performance.mak_performance_expense',
                'mak_performance.mak_performance_avg_position',
                'mak_performance.mak_performance_gmv',
                'mak_performance.mak_performance_product_sold',
                'mak_performance.mak_performance_orders',
            ])
        ;

        return $model->get();
    }

    /**
     * sum Performance By Shop Channel Id
     * @param int $shopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function sumPerformanceByShopChannelId($shopChannelId, $dateFrom, $dateTo)
    {
        $model = DB::connection('master_business')->table('mak_performance')
            ->join(
                'mak_programmatic_keyword_product',
                'mak_programmatic_keyword_product.mak_programmatic_keyword_product_id',
                '=',
                'mak_performance.fk_mak_programmatic_keyword_product'
            )
            ->join(
                'product_shop_channel',
                'product_shop_channel.product_shop_channel_id',
                '=',
                'mak_programmatic_keyword_product.fk_product_shop_channel'
            )
            ->where('product_shop_channel.fk_shop_channel', $shopChannelId)
            ->whereBetween('mak_performance.mak_performance_date', [$dateFrom, $dateTo])
            ->select([
                'mak_performance.mak_performance_date',
                DB::raw('sum(mak_performance.mak_performance_gmv) as total_gmv'),
                DB::raw('sum(mak_performance.mak_performance_product_sold) as total_item_sold'),
                DB::raw('sum(mak_performance.mak_performance_orders) as total_sale_order'),
                DB::raw('sum(mak_performance.mak_performance_view) as total_view'),
                DB::raw('sum(mak_performance.mak_performance_clicks) as total_click'),
                DB::raw('sum(mak_performance.mak_performance_expense) as total_cost'),
                DB::raw('sum(mak_performance.mak_performance_avg_position) as total_avg_position'),
            ])
            ->groupBy('mak_performance.mak_performance_date')
        ;
        return $model->get();
    }

}