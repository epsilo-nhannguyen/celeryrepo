<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 13:37
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;


class Venture extends ModelBusiness
{

    const TABLE_NAME = 'venture';

    const COL_VENTURE_ID = 'venture_id';
    const COL_VENTURE_NAME = 'venture_name';
    const COL_VENTURE_CODE = 'venture_code';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_VENTURE_CREATED_BY = 'venture_created_by';
    const COL_VENTURE_CREATED_AT = 'venture_created_at';
    const COL_VENTURE_UPDATED_BY = 'venture_updated_by';
    const COL_VENTURE_UPDATED_AT = 'venture_updated_at';
    const COL_VENTURE_EXCHANGE = 'venture_exchange';
    const COL_VENTURE_FORMAT_RIGHT = 'venture_format_right';
    const COL_VENTURE_TIMEZONE_TIME_PADDING = 'venture_timezone_time_padding';
    const COL_VENTURE_TIMEZONE = 'venture_timezone';
    const COL_VENTURE_COUNTRY = 'venture_country';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_VENTURE_ID;
    public $timestamps = false;

    /**
     * @const int
     */
    const VIETNAM = 1;

    /**
     * @const int
     */
    const PHILIPPINES = 2;

    /**
     * @const int
     */
    const MALAYSIA = 3;

    /**
     * @const int
     */
    const SINGAPORE = 4;

    /**
     * @const int
     */
    const INDONESIA = 5;

    /**
     * @const int
     */
    const THAILAND = 6;

    /**
     * @var string
     */
    const VIETNAM_CODE = 'VN';
    const PHILIPPINES_CODE = 'PH';
    const MALAYSIA_CODE = 'MY';
    const SINGAPORE_CODE = 'SG';
    const INDONESIA_CODE = 'ID';
    const THAILAND_CODE = 'TH';

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * @param string $code
     * @return mixed
     */
    public static function getByCode($code)
    {
        return self::where(self::COL_VENTURE_CODE, $code)->first();
    }

}