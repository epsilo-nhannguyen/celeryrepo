<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 16:23
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class ConfigActive extends Model
{
    const TABLE_NAME = 'config_active';

    const COL_CONFIG_ACTIVE_ID = 'config_active_id';
    const COL_CONFIG_ACTIVE_NAME = 'config_active_name';
    const COL_CONFIG_ACTIVE_NOTE = 'config_active_note';
    const COL_CONFIG_ACTIVE_CREATED_AT = 'config_active_created_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_CONFIG_ACTIVE_ID;
    public $timestamps = false;

    /**
     * @var int
     */
    const INACTIVE = 0;

    /**
     * @var int
     */
    const ACTIVE = 1;

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

}