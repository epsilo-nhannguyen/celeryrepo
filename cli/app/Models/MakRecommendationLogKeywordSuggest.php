<?php
/**
 * Created by PhpStorm.
 * User: An Nguyen
 * Date: 30/12/2019
 * Time: 09:33
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakRecommendationLogKeywordSuggest extends Model
{
    const TABLE_NAME = 'mak_recommendation_log_keyword_suggest';

    const COL_MAK_RECOMMENDATION_LOG_KEYWORD_SUGGEST_ID = 'mak_recommendation_log_keyword_suggest_id';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';
    const COL_FK_PRODUCT_SHOP_CHANNEL = 'fk_product_shop_channel';
    const COL_MAK_RECOMMENDATION_LOG_KEYWORD_SUGGEST_JSON = 'mak_recommendation_log_keyword_suggest_json';
    const COL_MAK_RECOMMENDATION_LOG_KEYWORD_SUGGEST_CREATED_AT = 'mak_recommendation_log_keyword_suggest_created_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_RECOMMENDATION_LOG_KEYWORD_SUGGEST_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }


    /**
     * insertGetId
     * @param array $recordArray
     * @return integer
     */
    public static function insertGetId($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insertGetId($recordArray);
    }



}