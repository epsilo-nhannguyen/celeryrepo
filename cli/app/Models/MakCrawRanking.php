<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 01/10/2019
 * Time: 20:00
 */

namespace App\Models;


use App\Library\Model\Sql\Manager;
use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models;


class MakCrawRanking extends ModelBusiness
{
    const TABLE_NAME = 'mak_craw_ranking';

    const COL_MAK_CRAW_RANKING_ID = 'mak_craw_ranking_id';
    const COL_FK_CHANNEL = 'fk_channel';
    const COL_MAK_CRAW_RANKING_SHOP_NAME = 'mak_craw_ranking_shop_name';
    const COL_MAK_CRAW_RANKING_CATEGORY_NAME = 'mak_craw_ranking_category_name';
    const COL_MAK_CRAW_RANKING_IS_SPONSOR = 'mak_craw_ranking_is_sponsor';
    const COL_MAK_CRAW_RANKING_RANK = 'mak_craw_ranking_rank';
    const COL_MAK_CRAW_RANKING_SKU = 'mak_craw_ranking_sku';
    const COL_MAK_CRAW_RANKING_NAME = 'mak_craw_ranking_name';
    const COL_MAK_CRAW_RANKING_RRP = 'mak_craw_ranking_rrp';
    const COL_MAK_CRAW_RANKING_SELLING_PRICE = 'mak_craw_ranking_selling_price';
    const COL_MAK_CRAW_RANKING_COLLECT_AT = 'mak_craw_ranking_collect_at';
    const COL_MAK_CRAW_RANKING_USERAGENT = 'mak_craw_ranking_useragent';
    const COL_MAK_CRAW_RANKING_IS_OFFICIAL = 'mak_craw_ranking_is_official';
    const COL_MAK_CRAW_RANKING_BRAND_NAME = 'mak_craw_ranking_brand_name';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD = 'fk_mak_programmatic_keyword';
    const COL_FK_PRODUCT_SHOP_CHANNEL = 'fk_product_shop_channel';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_CRAW_RANKING_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * build Data Insert
     * @param int $keywordId
     * @param int $productShopChannelId
     * @param int $rank
     * @param int $collectedAt
     * @param int $channelId
     * @return array
     */
    public static function buildDataInsert($keywordId, $productShopChannelId, $rank, $collectedAt, $channelId)
    {
        return [
            self::COL_FK_MAK_PROGRAMMATIC_KEYWORD => $keywordId,
            self::COL_FK_PRODUCT_SHOP_CHANNEL => $productShopChannelId,
            self::COL_MAK_CRAW_RANKING_RANK => $rank,
            self::COL_MAK_CRAW_RANKING_COLLECT_AT => $collectedAt,
            self::COL_FK_CHANNEL => $channelId
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * @project: RECOMMENDATION
     * @param int $time
     * @param string $keyword
     * @param string $itemId
     * @param int $shopId
     * @return array
     */
    public static function searchSkuRankRecommendationData($time, $keyword, $itemId, $shopId, $DBConnect = null)
    {
        if (!$DBConnect) {
            $DBConnect = env('DB_CONNECTION', Manager::SQL_CHART_CONNECT);
        }

        $shopId = intval($shopId);
        $time = intval($time);
        $keyword = trim($keyword);
        $itemId = trim($itemId);

        $query = DB::connection($DBConnect)->query()
            ->select([
                    DB::raw('product_shop_channel_item_id sku'),
                    DB::raw('shop_channel_id scid'),
                    DB::raw('mak_programmatic_keyword_name keyword'),
                    DB::raw('mak_craw_ranking_collect_at timestamp'),
                    DB::raw('mak_craw_ranking_rank position'),
                    ShopChannel::COL_SHOP_CHANNEL_ID
                ]
            )
            ->from(self::TABLE_NAME)
            ->join(ProductShopChannel::TABLE_NAME, MakCrawRanking::COL_FK_PRODUCT_SHOP_CHANNEL, ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID)
            ->join(ShopChannel::TABLE_NAME, ProductShopChannel::COL_FK_SHOP_CHANNEL, ShopChannel::COL_SHOP_CHANNEL_ID)
            ->join(MakProgrammaticKeyword::TABLE_NAME, MakCrawRanking::COL_FK_MAK_PROGRAMMATIC_KEYWORD, MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID)
            ->where(MakCrawRanking::COL_MAK_CRAW_RANKING_COLLECT_AT, '>=', $time)
            ->where(MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME, '=', $keyword)
            ->where(ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID, '=', $itemId)
            ->where(ShopChannel::COL_FK_SHOP_MASTER, '=', $shopId)
            ->get();
        return $query->toArray();
    }

    public static function getRank($keyword, $sku, $DBConnect = null)
    {
        if (!$DBConnect) {
            $DBConnect = env('DB_CONNECTION', Manager::SQL_CHART_CONNECT);
        }
        $keyword = trim($keyword);
        $now = intval(strtotime('now'));
        $last = intval(strtotime('-1 Day'));
        $query = DB::connection($DBConnect)->table(self::TABLE_NAME)
            ->join(MakProgrammaticKeyword::TABLE_NAME, MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID, self::COL_FK_MAK_PROGRAMMATIC_KEYWORD)
            ->join(ProductShopChannel::TABLE_NAME, ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID, self::COL_FK_PRODUCT_SHOP_CHANNEL)
            ->where(DB::raw(MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME . ' = "' . $keyword . '"'))
            ->where(DB::raw(ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_SELLER_SKU . ' =  "' . $sku . '"'))
            ->where(DB::raw(self::COL_MAK_CRAW_RANKING_COLLECT_AT . ' >= ' . $last))
            ->where(DB::raw(self::COL_MAK_CRAW_RANKING_COLLECT_AT . ' <= ' . $now))
            ->first();
        return $query;
    }
}