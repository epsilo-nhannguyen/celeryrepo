<?php

namespace App\Models;

use App\Library\Model\Sql\Manager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MakRecommendationSkSelect extends Model
{
    const TABLE_NAME = 'mak_recommendation_sk_select';

    const COL_MAK_RECOMMENDATION_SK_SELECT_ID = 'mak_recommendation_sk_select_id';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';
    const COL_MAK_RECOMMENDATION_SK_SELECT_JSON = 'mak_recommendation_sk_select_json';
    const COL_MAK_RECOMMENDATION_SK_SELECT_CREATED_AT = 'mak_recommendation_sk_select_created_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_RECOMMENDATION_SK_SELECT_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray,$DBConnect = null)
    {
        if(!$DBConnect){
            $DBConnect = env('DB_CONNECTION',Manager::SQL_CHART_CONNECT);
        }
        return DB::connection($DBConnect)->table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * @param int|array $shopChannelId
     * @return array
     */
    public static function getByShopChannel($shopChannelId)
    {
        return self::where(self::COL_FK_SHOP_CHANNEL,$shopChannelId)
            ->orderBy(self::COL_MAK_RECOMMENDATION_SK_SELECT_CREATED_AT,'DESC')
            ->first();
    }
}