<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MakRecommendationLogBiddingPrice extends Model
{
    const TABLE_NAME = 'mak_recommendation_log_bidding_price';
    const COL_MAK_RECOMMENDATION_LOG_BIDDING_PRICE_ID = 'mak_recommendation_log_bidding_price_id';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';
    const COL_FK_ADS_ID = 'fk_ads_id';
    const COL_MAK_RECOMMENDATION_LOG_BIDDING_PRICE_KEYWORD_JSON = 'mak_recommendation_log_bidding_price_keyword_json';
    const COL_MAK_RECOMMENDATION_LOG_BIDDING_PRICE_CREATED_AT = 'mak_recommendation_log_bidding_price_created_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_RECOMMENDATION_LOG_BIDDING_PRICE_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }


}