<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:03
 */

namespace App\Models\ModelChart;


use App\Library;
use App\Models\Master;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class ChartKeywordCampaignMetric extends Model
{
    const TABLE_NAME = 'chart_keyword_campaign_metric';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_CURRENCY_ID = 'currency_id';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_ID = 'mak_programmatic_campaign_id';
    const COL_DATE = 'date';
    const COL_UPDATED_AT = 'updated_at';
    const COL_IMPRESSION = 'impression';
    const COL_CLICK = 'click';
    const COL_ITEM_SOLD = 'item_sold';
    const COL_GMV = 'gmv';
    const COL_COST = 'cost';
    const COL_CTR = 'ctr';
    const COL_CR = 'cr';
    const COL_CPC = 'cpc';
    const COL_CPI = 'cpi';
    const COL_CIR = 'cir';
    const COL_TOTAL_SKU = 'total_sku';
    const COL_TOTAL_KEYWORD = 'total_keyword';
    const COL_TOTAL_GMV = 'total_gmv';
    const COL_TOTAL_ITEM_SOLD = 'total_item_sold';
    const COL_PERCENT_GMV_DIVISION_TOTAL = 'percent_gmv_division_total';
    const COL_PERCENT_ITEM_SOLD_DIVISION_TOTAL = 'percent_item_sold_division_total';
    const COL_WALLET_BALANCE = 'wallet_balance';


    use Master;

    /**
     * build Data Insert
     * @param $shopChannelId
     * @param $campaignId
     * @param $date
     * @param $impression
     * @param $click
     * @param $itemSold
     * @param $gmv
     * @param $cost
     * @param $ctr
     * @param $cr
     * @param $cpc
     * @param $cpi
     * @param $cir
     * @param $totalSku
     * @param $totalKeyword
     * @param $totalGmv
     * @param $totalItemSold
     * @param $percentGmvGmvChannel
     * @param $percentItemSoldItemSoldChannel
     * @param $currencyId
     * @param $walletData
     * @return array
     */
    public static function buildDataInsert(
        $shopChannelId, $campaignId, $date, $impression, $click, $itemSold, $gmv, $cost, $ctr, $cr, $cpc, $cpi, $cir,
        $totalSku, $totalKeyword, $totalGmv, $totalItemSold, $percentGmvGmvChannel, $percentItemSoldItemSoldChannel, $currencyId,
        $walletData
    )
    {
        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID => $campaignId,
            self::COL_CURRENCY_ID => $currencyId,
            self::COL_DATE => $date,
            self::COL_IMPRESSION => $impression,
            self::COL_CLICK => $click,
            self::COL_ITEM_SOLD => $itemSold,
            self::COL_GMV => $gmv,
            self::COL_COST => $cost,
            self::COL_CTR => $ctr,
            self::COL_CR => $cr,
            self::COL_CPC => $cpc,
            self::COL_CPI => $cpi,
            self::COL_CIR => $cir,
            self::COL_TOTAL_SKU => $totalSku,
            self::COL_TOTAL_KEYWORD => $totalKeyword,
            self::COL_TOTAL_GMV => $totalGmv,
            self::COL_TOTAL_ITEM_SOLD => $totalItemSold,
            self::COL_PERCENT_GMV_DIVISION_TOTAL => $percentGmvGmvChannel,
            self::COL_PERCENT_ITEM_SOLD_DIVISION_TOTAL => $percentItemSoldItemSoldChannel,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_WALLET_BALANCE => $walletData,
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * update Case
     * @param string $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::connection('master_chart')->statement($sql);
    }

    /**
     * snapshot Update Time
     * @param array $idArray
     * @return int
     */
    public static function snapshotUpdateTime($idArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
            ->whereIn(self::COL_ID, $idArray)
            ->update([
                self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ])
        ;
    }

    /**
     * search By Shop Channel Id And Date
     * @param int $shopChannelId
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndDate($shopChannelId, $date)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
        # return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::COL_DATE, $date)
            ->select(
                self::COL_ID,
                self::COL_CURRENCY_ID,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID
            )
            ->get()
        ;
    }

    /**
     * @param $shopChannelId
     * @param $campaignId
     * @param $date
     * @param $currency
     * @return mixed
     */
    public static function findByKey($shopChannelId, $campaignId, $date, $currency)
    {
        $select = DB::connection('master_chart')->table('chart_keyword_campaign_metric')
            ->where('shop_channel_id', $shopChannelId)
            ->where('mak_programmatic_campaign_id', $campaignId)
            ->where('date', $date)
            ->where('currency_id', $currency)
        ;

        return $select->get()->first();
    }

    /**
     * @param $data
     */
    public static function saveData($data)
    {
        $dataInDb = static::findByKey($data['shop_channel_id'], $data['mak_programmatic_campaign_id'], $data['date'], $data['currency_id']);
        if ($dataInDb) {
            DB::connection('master_chart')->table('chart_keyword_campaign_metric')
                ->where('id', $dataInDb['id'])
                ->update($dataInDb);
        } else {
            DB::connection('master_chart')->table('chart_keyword_campaign_metric')->insert($data);
        }
    }

    /**
     * @param $arrayShopChannelId
     * @param $date
     * @param $connection
     * @return Collection
     */
    public static function getByArrayShop($arrayShopChannelId, $date, $connection)
    {
        $select = DB::connection($connection)->table('chart_keyword_campaign_metric')
            ->whereIn('shop_channel_id', $arrayShopChannelId)
            ->where('date', $date)
            ->select([
                'shop_channel_id',
                'mak_programmatic_campaign_id',
            ])
        ;

        return $select->get();
    }

    /**
     * @param Collection $collection
     * @param $connection
     */
    public static function saveCollection(Collection $collection, $connection)
    {
        foreach ($collection as $item) {

            $dataDb = static::findOne(
                $item['shop_channel_id'],
                $item['mak_programmatic_campaign_id'],
                $item['date'],
                $item['currency_id'],
                $connection
            );
            $item['updated_at'] = DB::raw('unix_timestamp()');
            if ($dataDb) {
                DB::connection($connection)->table('chart_keyword_campaign_metric')->where('id', $dataDb->id)->update($item);
            } else {
                DB::connection($connection)->table('chart_keyword_campaign_metric')->insert($item);
            }
        }
    }

    /**
     * @param $shopChannelId
     * @param $campaignId
     * @param $date
     * @param $currencyId
     * @param $connection
     * @return mixed
     */
    public static function findOne($shopChannelId, $campaignId, $date, $currencyId, $connection)
    {
        $select = DB::connection($connection)->table('chart_keyword_campaign_metric')
            ->where('shop_channel_id', $shopChannelId)
            ->where('mak_programmatic_campaign_id', $campaignId)
            ->where('date', $date)
            ->where('currency_id', $currencyId)
        ;

        return $select->get()->first();
    }
}