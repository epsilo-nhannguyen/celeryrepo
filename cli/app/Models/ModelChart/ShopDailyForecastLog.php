<?php


namespace App\Models\ModelChart;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopDailyForecastLog extends Model
{
    /**
     * @param int $shopChannelId
     * @param string $date
     * @param int $hour
     * @param json|string $snapshotCurrent
     * @param json|string $snapshotContribution
     * @param json|string $snapshotResult
     * @param string $connection
     * @return bool
     */
    public static function insert($shopChannelId, $date, $hour, $snapshotCurrent, $snapshotContribution, $snapshotResult, $connection = 'master_business')
    {
        return DB::connection($connection)->table('shop_daily_forecast_log')->insert([
            'shop_channel_id' => $shopChannelId,
            'date' => $date,
            'hour' => $hour,
            'snapshot_current' => $snapshotCurrent,
            'snapshot_contribution' => $snapshotContribution,
            'snapshot_result' => $snapshotResult,
        ]);
    }
}