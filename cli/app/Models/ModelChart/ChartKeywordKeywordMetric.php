<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:06
 */

namespace App\Models\ModelChart;


use App\Models\Master;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Library;


class ChartKeywordKeywordMetric extends Model
{
    const TABLE_NAME = 'chart_keyword_keyword_metric';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_DATE = 'date';
    const COL_UPDATED_AT = 'updated_at';
    const COL_TOTAL_KEYWORD = 'total_keyword';
    const COL_TOTAL_KEYWORD_APPLIED = 'total_keyword_applied';
    const COL_PERCENT_KEYWORD_SALE_KEYWORD = 'percent_keyword_sale_keyword';
    const COL_PERCENT_KEYWORD_SALE_KEYWORD_SALE = 'percent_keyword_sale_keyword_sale';
    const COL_PERCENT_KEYWORD_TOP_POSITION = 'percent_keyword_top_position';
    const COL_PERCENT_KEYWORD_CTR_LARGER_AVG = 'percent_keyword_ctr_larger_avg';
    const COL_IS_HAVE_ITEM_SOLD_L30D = 'is_have_item_sold_l30d';
    const COL_IS_HAVE_IMPRESSION_L30D = 'is_have_impression_l30d';
    const COL_TYPE = 'type';

    /**
     * @var int
     */
    const IS_HAVE_ITEM_SOLD_L30D = 1;
    const IS_NO_ITEM_SOLD_L30D = 0;
    const IS_HAVE_IMPRESSION_L30D = 1;
    const IS_NO_IMPRESSION_L30D = 0;

    use Master;

    /**
     * build Data Insert
     * @param $shopChannelId
     * @param $date
     * @param $totalKeyword
     * @param $totalKeywordApplied
     * @param $percentKeywordSaleKeyword
     * @param $percentKeywordSaleKeywordSale
     * @param $percentKeywordTopPosition
     * @param $percentKeywordCtrLargerAvg
     * @param $isHaveItemSoldL30d
     * @param $isHaveImpressionL30d
     * @return array
     */
    public static function buildDataInsert(
        $shopChannelId, $date, $totalKeyword, $totalKeywordApplied, $percentKeywordSaleKeyword,
        $percentKeywordSaleKeywordSale, $percentKeywordTopPosition, $percentKeywordCtrLargerAvg, $isHaveItemSoldL30d, $isHaveImpressionL30d
    )
    {
        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_DATE => $date,
            self::COL_TOTAL_KEYWORD => $totalKeyword,
            self::COL_TOTAL_KEYWORD_APPLIED => $totalKeywordApplied,
            self::COL_PERCENT_KEYWORD_SALE_KEYWORD => $percentKeywordSaleKeyword,
            self::COL_PERCENT_KEYWORD_SALE_KEYWORD_SALE => $percentKeywordSaleKeywordSale,
            self::COL_PERCENT_KEYWORD_TOP_POSITION => $percentKeywordTopPosition,
            self::COL_PERCENT_KEYWORD_CTR_LARGER_AVG => $percentKeywordCtrLargerAvg,
            self::COL_IS_HAVE_ITEM_SOLD_L30D => $isHaveItemSoldL30d,
            self::COL_IS_HAVE_IMPRESSION_L30D => $isHaveImpressionL30d,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
        ];
    }
    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * update Case
     * @param string $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::connection('master_chart')->statement($sql);
    }

    /**
     * snapshot Update Time
     * @param array $idArray
     * @return int
     */
    public static function snapshotUpdateTime($idArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
            ->whereIn(self::COL_ID, $idArray)
            ->update([
                self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ])
            ;
    }

    /**
     * search By Shop Channel Id And Date
     * @param int $shopChannelId
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndDate($shopChannelId, $date)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::COL_DATE, $date)
            ->select(
                self::COL_ID,
                self::COL_DATE,
                self::COL_IS_HAVE_ITEM_SOLD_L30D,
                self::COL_IS_HAVE_IMPRESSION_L30D
            )
            ->get()
        ;
    }

}