<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:04
 */

namespace App\Models\ModelChart;


use App\Models\Master;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Library;


class ChartKeywordCampaignMetricIntraday extends Model
{
    const TABLE_NAME = 'chart_keyword_campaign_metric_intraday';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_CURRENCY_ID = 'currency_id';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_ID = 'mak_programmatic_campaign_id';
    const COL_DATE = 'date';
    const COL_HOUR = 'hour';
    const COL_UPDATED_AT = 'updated_at';
    const COL_IMPRESSION = 'impression';
    const COL_CLICK = 'click';
    const COL_ITEM_SOLD = 'item_sold';
    const COL_GMV = 'gmv';
    const COL_COST = 'cost';
    const COL_CTR = 'ctr';
    const COL_CR = 'cr';
    const COL_CPC = 'cpc';
    const COL_CPI = 'cpi';
    const COL_CIR = 'cir';
    const COL_TOTAL_SKU = 'total_sku';
    const COL_TOTAL_KEYWORD = 'total_keyword';
    const COL_TOTAL_GMV = 'total_gmv';
    const COL_TOTAL_ITEM_SOLD = 'total_item_sold';
    const COL_PERCENT_GMV_TOTAL = 'percent_gmv_total';
    const COL_PERCENT_ITEM_SOLD_TOTAL = 'percent_item_sold_total';
    const COL_IS_FORECAST = 'is_forecast';
    const COL_WALLET_BALANCE = 'wallet_balance';

    /**
     * @var int
     */
    const IS_FORECAST = 1;
    const IS_LIVE = 0;

    use Master;

    /**
     * build Data Insert
     * @param $shopChannelId
     * @param $campaignId
     * @param $date
     * @param $hour
     * @param $impression
     * @param $click
     * @param $itemSold
     * @param $gmv
     * @param $cost
     * @param $ctr
     * @param $cr
     * @param $cpc
     * @param $cpi
     * @param $cir
     * @param $totalSku
     * @param $totalKeyword
     * @param $totalGmv
     * @param $totalItemSold
     * @param $percentGmvGmvChannel
     * @param $percentItemSoldItemSoldChannel
     * @param $currencyId
     * @param $isForecast
     * @return array
     */
    public static function buildDataInsert(
        $shopChannelId, $campaignId, $date, $hour, $impression, $click, $itemSold, $gmv, $cost, $ctr, $cr, $cpc, $cpi, $cir,
        $totalSku, $totalKeyword, $totalGmv, $totalItemSold, $percentGmvGmvChannel, $percentItemSoldItemSoldChannel, $currencyId, $walletValue = null, $isForecast = self::IS_LIVE
    )
    {
        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID => $campaignId,
            self::COL_CURRENCY_ID => $currencyId,
            self::COL_IS_FORECAST => $isForecast,
            self::COL_DATE => $date,
            self::COL_HOUR => $hour,
            self::COL_IMPRESSION => $impression,
            self::COL_CLICK => $click,
            self::COL_ITEM_SOLD => $itemSold,
            self::COL_GMV => $gmv,
            self::COL_COST => $cost,
            self::COL_CTR => $ctr,
            self::COL_CR => $cr,
            self::COL_CPC => $cpc,
            self::COL_CPI => $cpi,
            self::COL_CIR => $cir,
            self::COL_TOTAL_SKU => $totalSku,
            self::COL_TOTAL_KEYWORD => $totalKeyword,
            self::COL_TOTAL_GMV => $totalGmv,
            self::COL_TOTAL_ITEM_SOLD => $totalItemSold,
            self::COL_PERCENT_GMV_TOTAL => $percentGmvGmvChannel,
            self::COL_PERCENT_ITEM_SOLD_TOTAL => $percentItemSoldItemSoldChannel,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_WALLET_BALANCE => $walletValue,
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * update Case
     * @param string $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::connection('master_chart')->statement($sql);
    }

    /**
     * snapshot Update Time
     * @param array $idArray
     * @return int
     */
    public static function snapshotUpdateTime($idArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
            ->whereIn(self::COL_ID, $idArray)
            ->update([
                self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ])
        ;
    }

    /**
     * search By Shop Channel Id And Date Hour
     * @param int $shopChannelId
     * @param string $date
     * @param int $hour
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndDateHour($shopChannelId, $date, $hour = null)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
        # return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::COL_DATE, $date)
            ->when($hour !== null, function ($query) use ($hour) {
                return $query->where(self::COL_HOUR, $hour);
            })
            ->get()
        ;
    }

    /**
     * search By Shop Channel Id And Date Hour
     * @param int $shopChannelId
     * @param string $date
     * @param int $hour
     * @param int $currency
     * @return \Illuminate\Support\Collection
     */
    public static function getAllLessThanDate($shopChannelId, $date, $hour = null, $currency = 1)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
            # return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::COL_DATE, '<=', $date)
            ->where(self::COL_CURRENCY_ID, '<=', $currency)
            ->orderBy('date')
            ->orderBy('hour')
            ->when($hour !== null, function ($query) use ($hour) {
                return $query->where(self::COL_HOUR, $hour);
            })
            ->get()
            ;
    }

    /**
     * @param $shopChannelId
     * @param $operatorDate
     * @param $dateTo
     * @param $currencyId
     * @param $operatorDateBegin
     * @param $dateBegin
     * @return \Illuminate\Support\Collection
     */
    public static function getGroupByDate($shopChannelId, $operatorDate, $dateTo, $currencyId, $operatorDateBegin = null, $dateBegin = null)
    {
        $select = DB::connection('master_chart')
            ->table(ChartKeywordCampaignMetricIntraday::TABLE_NAME)
            ->where(ChartKeywordCampaignMetricIntraday::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(ChartKeywordCampaignMetricIntraday::COL_DATE, $operatorDate, $dateTo)
            ->where(ChartKeywordCampaignMetricIntraday::COL_CURRENCY_ID, $currencyId)
            ->groupBy(
                ChartKeywordCampaignMetricIntraday::COL_SHOP_CHANNEL_ID,
                ChartKeywordCampaignMetricIntraday::COL_DATE,
                ChartKeywordCampaignMetricIntraday::COL_CURRENCY_ID,
                ChartKeywordCampaignMetricIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID
            )
            ->select([
                ChartKeywordCampaignMetricIntraday::COL_SHOP_CHANNEL_ID,
                ChartKeywordCampaignMetricIntraday::COL_DATE,
                ChartKeywordCampaignMetricIntraday::COL_CURRENCY_ID,
                ChartKeywordCampaignMetricIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                DB::raw(sprintf('sum(%s) as total_impression', ChartKeywordCampaignMetricIntraday::COL_IMPRESSION)),
                DB::raw(sprintf('sum(%s) as total_click', ChartKeywordCampaignMetricIntraday::COL_CLICK)),
                DB::raw(sprintf('sum(%s) as total_cost', ChartKeywordCampaignMetricIntraday::COL_COST)),
                DB::raw(sprintf('sum(%s) as total_gmv', ChartKeywordCampaignMetricIntraday::COL_GMV)),
                DB::raw(sprintf('sum(%s) as total_item_sold', ChartKeywordCampaignMetricIntraday::COL_ITEM_SOLD)),
                DB::raw(sprintf('sum(%s) as total_item_sold_channel', ChartKeywordCampaignMetricIntraday::COL_TOTAL_ITEM_SOLD)),
                DB::raw(sprintf('sum(%s) as total_gmv_channel', ChartKeywordCampaignMetricIntraday::COL_TOTAL_GMV)),
                DB::raw(sprintf('sum(%s) as total_sku', ChartKeywordCampaignMetricIntraday::COL_TOTAL_SKU)),
                DB::raw(sprintf('sum(%s) as total_keyword', ChartKeywordCampaignMetricIntraday::COL_TOTAL_KEYWORD)),
            ])
        ;

        if ($operatorDateBegin && $dateBegin) {
            $select->where(ChartKeywordCampaignMetricIntraday::COL_DATE, $operatorDateBegin, $dateBegin);
        }

        return $select->get();
    }

    /**
     * sum Performance By Shop Channel Id
     * @param int $shopChannelId
     * @param string $date
     * @param int $hour
     * @return \Illuminate\Support\Collection
     */
    public static function sumPerformanceByShopChannelId($shopChannelId, $date, $hour)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::COL_DATE, '<', $date)
            ->where(self::COL_HOUR, '>', $hour)
            ->select(
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                self::COL_CURRENCY_ID,
                self::COL_HOUR,
                DB::raw('sum('.self::COL_IMPRESSION.') as '.self::COL_IMPRESSION),
                DB::raw('sum('.self::COL_CLICK.') as '.self::COL_CLICK),
                DB::raw('sum('.self::COL_ITEM_SOLD.') as '.self::COL_ITEM_SOLD),
                DB::raw('sum('.self::COL_GMV.') as '.self::COL_GMV),
                DB::raw('sum('.self::COL_COST.') as '.self::COL_COST),
                DB::raw('sum('.self::COL_TOTAL_SKU.') as '.self::COL_TOTAL_SKU),
                DB::raw('sum('.self::COL_TOTAL_KEYWORD.') as '.self::COL_TOTAL_KEYWORD),
                DB::raw('sum('.self::COL_TOTAL_GMV.') as '.self::COL_TOTAL_GMV),
                DB::raw('sum('.self::COL_TOTAL_ITEM_SOLD.') as '.self::COL_TOTAL_ITEM_SOLD)
            )
            ->groupBy(
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                self::COL_CURRENCY_ID,
                self::COL_HOUR
            )
            ->get()
        ;
    }

    public static function saveCollection(Collection $collection, $connection)
    {
        foreach ($collection as $item) {
            $dataDb = static::findOne(
                $item['shop_channel_id'],
                $item['mak_programmatic_campaign_id'],
                $item['date'],
                $item['hour'],
                $item['currency_id'],
                $connection
            );
            $item['updated_at'] = DB::raw('unix_timestamp()');
            if ($dataDb) {
                DB::connection($connection)->table('chart_keyword_campaign_metric_intraday')->where('id', $dataDb->id)->update($item);
            } else {
                DB::connection($connection)->table('chart_keyword_campaign_metric_intraday')->insert($item);
            }
        }
    }

    /**
     * @param $shopChannelId
     * @param $campaignId
     * @param $date
     * @param $hour
     * @param $currencyId
     * @param $connection
     * @return mixed
     */
    public static function findOne($shopChannelId, $campaignId, $date, $hour, $currencyId, $connection)
    {
        $select = DB::connection($connection)->table('chart_keyword_campaign_metric_intraday')
            ->where('shop_channel_id', $shopChannelId)
            ->where('mak_programmatic_campaign_id', $campaignId)
            ->where('date', $date)
            ->where('hour', $hour)
            ->where('currency_id', $currencyId)
        ;

        return $select->get()->first();
    }

    public static function getByFilter($shopChannelId, $campaignId, $dateFrom, $dateTo, $connection, $currencyId)
    {
        $select = DB::connection($connection)->table('chart_keyword_campaign_metric_intraday')
            ->where('shop_channel_id', $shopChannelId)
            ->where('mak_programmatic_campaign_id', $campaignId)
            ->whereBetween('date', [$dateFrom, $dateTo])
            ->where('currency_id', $currencyId)
            ->select([
                'date',
                'hour',
                'impression',
                'click',
                'item_sold',
                'gmv',
                'cost',
                'total_sku',
                'total_keyword',
                'total_gmv',
                'total_item_sold',
                'currency_id',
                'wallet_balance',
            ])
        ;

        return $select->get();
    }

    /**
     * @param $arrayShopChannelId
     * @param $date
     * @param $connection
     * @return Collection
     */
    public static function getByDate($arrayShopChannelId, $date, $connection)
    {
        $dateFrom = $date.' 00:00:00';
        $dateTo = $date.' 23:59:59';
        $select = DB::connection($connection)->table('chart_keyword_campaign_metric_intraday')
            ->whereIn('shop_channel_id', $arrayShopChannelId)
            ->whereBetween('updated_at', [strtotime($dateFrom), strtotime($dateTo)])
            ->select(['*'])
        ;

        return $select->get();
    }
}