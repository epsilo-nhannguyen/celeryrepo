<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:04
 */

namespace App\Models\ModelChart;


use App\Application\Functions\Arrays;
use App\Application\Functions\Formatter;
use App\Models\Master;
use App\Models\ShopChannel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Library;


class ChartKeywordCampaignContribution extends Model
{
    const TABLE_NAME = 'chart_keyword_campaign_contribution';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_ID = 'mak_programmatic_campaign_id';
    const COL_DATE = 'date';
    const COL_TOTAL_DATE = 'total_date';
    const COL_HOUR = 'hour';
    const COL_UPDATED_AT = 'updated_at';
    const COL_IMPRESSION = 'impression';
    const COL_CLICK = 'click';
    const COL_ITEM_SOLD = 'item_sold';
    const COL_GMV = 'gmv';
    const COL_COST = 'cost';
    const COL_TOTAL_SKU = 'total_sku';
    const COL_TOTAL_KEYWORD = 'total_keyword';
    const COL_TOTAL_GMV = 'total_gmv';
    const COL_TOTAL_ITEM_SOLD = 'total_item_sold';

    use Master;

    /**
     * build Data Insert
     * @param $shopChannelId
     * @param $campaignId
     * @param $date
     * @param $totalDate
     * @param $hour
     * @param $impression
     * @param $click
     * @param $itemSold
     * @param $gmv
     * @param $cost
     * @param $totalSku
     * @param $totalKeyword
     * @param $totalGmv
     * @param $totalItemSold
     * @return array
     */
    public static function buildDataInsert(
        $shopChannelId, $campaignId, $date, $totalDate, $hour, $impression, $click, $itemSold, $gmv, $cost, $totalSku, $totalKeyword, $totalGmv, $totalItemSold
    )
    {
        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID => $campaignId,
            self::COL_DATE => $date,
            self::COL_TOTAL_DATE => $totalDate,
            self::COL_HOUR => $hour,
            self::COL_IMPRESSION => $impression,
            self::COL_CLICK => $click,
            self::COL_ITEM_SOLD => $itemSold,
            self::COL_GMV => $gmv,
            self::COL_COST => $cost,
            self::COL_TOTAL_SKU => $totalSku,
            self::COL_TOTAL_KEYWORD => $totalKeyword,
            self::COL_TOTAL_GMV => $totalGmv,
            self::COL_TOTAL_ITEM_SOLD => $totalItemSold,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * update Case
     * @param string $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::connection('master_chart')->statement($sql);
    }

    /**
     * snapshot Update Time
     * @param array $idArray
     * @return int
     */
    public static function snapshotUpdateTime($idArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
            ->whereIn(self::COL_ID, $idArray)
            ->update([
                self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ])
        ;
    }

    public static function filterShopDate($shopChannelId, $date = null)
    {
        $select = DB::connection('master_chart')->table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)

            ->orderBy('date', 'desc')
            ->limit(24);

        if ($date) {
            $select->where('date', $date);
        }

        return $select->get();
    }

    /**
     * search By Shop Channel Id And Date
     * @param int $shopChannelId
     * @param string $date
     * @param string $dateBegin
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndDate($shopChannelId, $date, $dateBegin = '')
    {
        $data = self::filterShopDate($shopChannelId, $date);

        if (!$data->count()) {
            $dataInsert = [];
            //do get date max
            $dataSubtractOneDay = self::filterShopDate($shopChannelId, null);

            if ($dataSubtractOneDay->count()) {
                $dataInsert = self::_processSubtractOneDay($shopChannelId, $date, $dataSubtractOneDay);
            } else {
                ini_set('memory_limit', -1);

                if (!$dateBegin) {
                    $dataGroupDate = ChartKeywordCampaignMetricIntraday::getGroupByDate(
                        $shopChannelId, '<', $date, 1
                    );
                } else {
                    $dataGroupDate = ChartKeywordCampaignMetricIntraday::getGroupByDate(
                        $shopChannelId, '<', $date, 1, '>', $dateBegin
                    );
                }

                $assocTotalByDate = [];
                foreach ($dataGroupDate as $item) {
                    $_campaignId = $item->{ChartKeywordCampaignMetricIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID};
                    $_date = $item->{ChartKeywordCampaignMetricIntraday::COL_DATE};
                    $assocTotalByDate[$_campaignId][$_date] = $item;
                }

                $dataIntraday = ChartKeywordCampaignMetricIntraday::getAllLessThanDate($shopChannelId, $date);
                $avgPercent = [];
                $listDate = [];
                $dictionary = [];
                foreach ($dataIntraday as $intraday) {
                    $intraday_campaignId = $intraday->{ChartKeywordCampaignMetricIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID};
                    $intraday_hour = $intraday->{ChartKeywordCampaignMetricIntraday::COL_HOUR};
                    $intraday_date = $intraday->{ChartKeywordCampaignMetricIntraday::COL_DATE};
                    $dictionary[$intraday_campaignId][$intraday_date][$intraday_hour] = $intraday;
                }

                foreach ($dataIntraday as $intraday) {
                    $intraday_campaignId = $intraday->{ChartKeywordCampaignMetricIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID};
                    $intraday_hour = $intraday->{ChartKeywordCampaignMetricIntraday::COL_HOUR};
                    $intraday_date = $intraday->{ChartKeywordCampaignMetricIntraday::COL_DATE};

                    if (!in_array($intraday_date, $listDate)) {
                        $listDate[] = $intraday_date;
                    }
                    $listMetric = [
                        self::COL_IMPRESSION => 'total_impression',
                        self::COL_CLICK => 'total_click',
                        self::COL_ITEM_SOLD => 'total_item_sold',
                        self::COL_GMV => 'total_gmv',
                        self::COL_COST => 'total_cost',
                        self::COL_TOTAL_SKU => 'total_sku',
                        self::COL_TOTAL_KEYWORD => 'total_keyword',
                        self::COL_TOTAL_GMV => 'total_gmv_channel',
                        self::COL_TOTAL_ITEM_SOLD => 'total_item_sold_channel',
                    ];
                    foreach ($listMetric as $metric => $totalMetric) {
                        if (!isset($avgPercent[$intraday_campaignId][$intraday_hour][$metric])) {
                            $avgPercent[$intraday_campaignId][$intraday_hour][$metric] = [
                                'sum' => 0,
                                'count' => 0,
                            ];
                        }
                        $totalValue = $assocTotalByDate[$intraday_campaignId][$intraday_date]->{$totalMetric} ?? 0;

                        if (isset($dictionary[$intraday_campaignId][$intraday_date][$intraday_hour - 1])) {
                            if (isset($dictionary[$intraday_campaignId][$intraday_date][$intraday_hour])) {
                                $value = $dictionary[$intraday_campaignId][$intraday_date][$intraday_hour]->{$metric}
                                    - $dictionary[$intraday_campaignId][$intraday_date][$intraday_hour - 1]->{$metric};
                            } else {
                                $value = 0;
                            }
                        } else {
                            $value = $dictionary[$intraday_campaignId][$intraday_date][$intraday_hour]->{$metric} ?? 0;
                        }

                        $avgPercent[$intraday_campaignId][$intraday_hour][$metric]['sum'] += ($totalValue > 0 ? $value/$totalValue * 100 : 0);
                        $avgPercent[$intraday_campaignId][$intraday_hour][$metric]['count'] += 1;
                    }
                }

                foreach ($avgPercent as $campaignId => $hourData) {

                    foreach ($hourData as $hour => $metricValue) {
                        foreach ($metricValue as $metric => $dataAvg) {
                            $metricValue[$metric] = $dataAvg['count'] > 0 ? $dataAvg['sum'] / $dataAvg['count'] : 0;
                        }
                        $dataInsert[] = self::buildDataInsert(
                            $shopChannelId,
                            $campaignId,
                            $date,
                            count($listDate) + 1,
                            $hour,
                            $metricValue[self::COL_IMPRESSION],
                            $metricValue[self::COL_CLICK],
                            $metricValue[self::COL_ITEM_SOLD],
                            $metricValue[self::COL_GMV],
                            $metricValue[self::COL_COST],
                            $metricValue[self::COL_TOTAL_SKU],
                            $metricValue[self::COL_TOTAL_KEYWORD],
                            $metricValue[self::COL_TOTAL_GMV],
                            $metricValue[self::COL_TOTAL_ITEM_SOLD]
                        );
                    }
                }
            }

            $dataInsertChunk = array_chunk($dataInsert, 200);
            foreach ($dataInsertChunk as $chunk) {
                self::batchInsert($chunk);
            }

            $result = [];
            foreach ($dataInsert as $item) {
                $result[] = Arrays::transformToStdClass($item);
            }
            $data = collect($result);
        }

        return $data;
    }

    private static function _processSubtractOneDay($shopChannelId, $date, $dataSubtractOneDay)
    {
        $dataInsert = [];
        $assocPerHour = [];
        $dataIntraday = ChartKeywordCampaignMetricIntraday::searchByShopChannelIdAndDateHour($shopChannelId, $date);
        foreach ($dataIntraday as $intraday) {
            $intraday_campaignId = $intraday->{ChartKeywordCampaignMetricIntraday::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID};
            $intraday_hour = $intraday->{ChartKeywordCampaignMetricIntraday::COL_HOUR};
            $assocPerHour[$intraday_campaignId][$intraday_hour] = $intraday;
        }
        foreach ($dataSubtractOneDay as $item) {
            $totalDate = $item->{self::COL_TOTAL_DATE};
            $intradayData = $assocPerHour[$item->{self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID}][$item->{self::COL_HOUR}] ?? Arrays::transformToStdClass([
                    ChartKeywordCampaignMetricIntraday::COL_IMPRESSION => 0,
                    ChartKeywordCampaignMetricIntraday::COL_CLICK => 0,
                    ChartKeywordCampaignMetricIntraday::COL_ITEM_SOLD => 0,
                    ChartKeywordCampaignMetricIntraday::COL_GMV => 0,
                    ChartKeywordCampaignMetricIntraday::COL_COST => 0,
                    ChartKeywordCampaignMetricIntraday::COL_TOTAL_SKU => 0,
                    ChartKeywordCampaignMetricIntraday::COL_TOTAL_KEYWORD => 0,
                    ChartKeywordCampaignMetricIntraday::COL_TOTAL_GMV => 0,
                    ChartKeywordCampaignMetricIntraday::COL_TOTAL_ITEM_SOLD => 0,
                ]);

            $dataInsert[] = self::buildDataInsert(
                $shopChannelId,
                $item->{self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID},
                $date,
                $totalDate + 1,
                $item->{self::COL_HOUR},
                ($item->{self::COL_IMPRESSION} * $totalDate + $intradayData->{self::COL_IMPRESSION}) / ($totalDate + 1),
                ($item->{self::COL_CLICK} * $totalDate + $intradayData->{self::COL_CLICK}) / ($totalDate + 1),
                ($item->{self::COL_ITEM_SOLD} * $totalDate + $intradayData->{self::COL_ITEM_SOLD}) / ($totalDate + 1),
                ($item->{self::COL_GMV} * $totalDate + $intradayData->{self::COL_GMV}) / ($totalDate + 1),
                ($item->{self::COL_COST} * $totalDate + $intradayData->{self::COL_COST}) / ($totalDate + 1),
                ($item->{self::COL_TOTAL_SKU} * $totalDate + $intradayData->{self::COL_TOTAL_SKU}) / ($totalDate + 1),
                ($item->{self::COL_TOTAL_KEYWORD} * $totalDate + $intradayData->{self::COL_TOTAL_KEYWORD}) / ($totalDate + 1),
                ($item->{self::COL_TOTAL_GMV} * $totalDate + $intradayData->{self::COL_TOTAL_GMV}) / ($totalDate + 1),
                ($item->{self::COL_TOTAL_ITEM_SOLD} * $totalDate + $intradayData->{self::COL_TOTAL_ITEM_SOLD}) / ($totalDate + 1)
            );
        }

        return $dataInsert;
    }
}