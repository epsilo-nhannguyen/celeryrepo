<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 14:52
 */

namespace App\Models\ModelChart;


use App\Models\Master;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ChartKeywordCampaignAction extends Model
{
    const TABLE_NAME = 'chart_keyword_campaign_action';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_ID = 'mak_programmatic_campaign_id';
    const COL_MAK_PROGRAMMATIC_ACTION_ID = 'mak_programmatic_action_id';
    const COL_DATE = 'date';
    const COL_CREATED_AT = 'created_at';
    const COL_CAMPAIGN_NAME = 'campaign_name';
    const COL_ACTION_NAME = 'action_name';

    use Master;

    /**
     * build Data Insert
     * @param int $shopChannelId
     * @param int $campaignId
     * @param int $actionId
     * @param string $date
     * @param string $campaignName
     * @param string $actionName
     * @param int $createdAt
     * @return array
     */
    public static function buildDataInsert($shopChannelId, $campaignId, $actionId, $date, $campaignName, $actionName, $createdAt)
    {
        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID => $campaignId,
            self::COL_MAK_PROGRAMMATIC_ACTION_ID => $actionId,
            self::COL_DATE => $date,
            self::COL_CAMPAIGN_NAME => $campaignName,
            self::COL_ACTION_NAME => $actionName,
            self::COL_CREATED_AT => $createdAt
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * update Case
     * @param string $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::connection('master_chart')->statement($sql);
    }

    /**
     * get Max Created At By Action Id Array
     * @param int $shopChannelId
     * @param array $actionIdArray
     * @return mixed
     */
    public static function getMaxCreatedAtByActionIdArray($shopChannelId, $actionIdArray)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
        # return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->whereIn(self::COL_MAK_PROGRAMMATIC_ACTION_ID, $actionIdArray)
            ->max(self::COL_CREATED_AT)
        ;
    }

    /**
     * search Shop Channel Id By Date From To
     * @param int $shopChannelId
     * @param int $dateFrom
     * @param int $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function searchShopChannelIdByDateFromTo($shopChannelId, $dateFrom, $dateTo)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
        # return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->whereIn(self::COL_CREATED_AT, [$dateFrom, $dateTo])
            ->select(
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                self::COL_MAK_PROGRAMMATIC_ACTION_ID,
                self::COL_CREATED_AT
            )
            ->get()
        ;
    }
}