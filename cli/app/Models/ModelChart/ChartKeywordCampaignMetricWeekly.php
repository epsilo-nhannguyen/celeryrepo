<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 25/12/2019
 * Time: 23:32
 */

namespace App\Models\ModelChart;


use App\Library;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ChartKeywordCampaignMetricWeekly extends Model
{
    use Master;

    const TABLE_NAME = 'chart_keyword_campaign_metric_weekly';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_CURRENCY_ID = 'currency_id';
    const COL_WEEK = 'week';
    const COL_UPDATED_AT = 'updated_at';
    const COL_IMPRESSION = 'impression';
    const COL_CLICK = 'click';
    const COL_ITEM_SOLD = 'item_sold';
    const COL_GMV = 'gmv';
    const COL_COST = 'cost';
    const COL_CTR = 'ctr';
    const COL_CR = 'cr';
    const COL_CPC = 'cpc';
    const COL_CPI = 'cpi';
    const COL_CIR = 'cir';
    const COL_TOTAL_SKU = 'total_sku';
    const COL_TOTAL_KEYWORD = 'total_keyword';
    const COL_TOTAL_GMV = 'total_gmv';
    const COL_TOTAL_ITEM_SOLD = 'total_item_sold';
    const COL_PERCENT_GMV_DIVISION_TOTAL = 'percent_gmv_division_total';
    const COL_PERCENT_ITEM_SOLD_DIVISION_TOTAL = 'percent_item_sold_division_total';

    /**
     * build Data Insert
     * @param $shopChannelId
     * @param $week
     * @param $impression
     * @param $click
     * @param $itemSold
     * @param $gmv
     * @param $cost
     * @param $ctr
     * @param $cr
     * @param $cpc
     * @param $cpi
     * @param $cir
     * @param $totalSku
     * @param $totalKeyword
     * @param $totalGmv
     * @param $totalItemSold
     * @param $percentGmvGmvChannel
     * @param $percentItemSoldItemSoldChannel
     * @param $currencyId
     * @return array
     */
    public static function buildDataInsert(
        $shopChannelId, $week, $impression, $click, $itemSold, $gmv, $cost, $ctr, $cr, $cpc, $cpi, $cir,
        $totalSku, $totalKeyword, $totalGmv, $totalItemSold, $percentGmvGmvChannel, $percentItemSoldItemSoldChannel, $currencyId
    )
    {
        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_CURRENCY_ID => $currencyId,
            self::COL_WEEK => $week,
            self::COL_IMPRESSION => $impression,
            self::COL_CLICK => $click,
            self::COL_ITEM_SOLD => $itemSold,
            self::COL_GMV => $gmv,
            self::COL_COST => $cost,
            self::COL_CTR => $ctr,
            self::COL_CR => $cr,
            self::COL_CPC => $cpc,
            self::COL_CPI => $cpi,
            self::COL_CIR => $cir,
            self::COL_TOTAL_SKU => $totalSku,
            self::COL_TOTAL_KEYWORD => $totalKeyword,
            self::COL_TOTAL_GMV => $totalGmv,
            self::COL_TOTAL_ITEM_SOLD => $totalItemSold,
            self::COL_PERCENT_GMV_DIVISION_TOTAL => $percentGmvGmvChannel,
            self::COL_PERCENT_ITEM_SOLD_DIVISION_TOTAL => $percentItemSoldItemSoldChannel,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * update Case
     * @param string $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::statement($sql);
    }

    /**
     * snapshot Update Time
     * @param array $idArray
     * @return int
     */
    public static function snapshotUpdateTime($idArray)
    {
        return DB::table(self::TABLE_NAME)
            ->whereIn(self::COL_ID, $idArray)
            ->update([
                self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ])
            ;
    }

    /**
     * search By Shop Channel Id And Date
     * @param int $shopChannelId
     * @param int $week
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndDate($shopChannelId, $week)
    {
        return DB::connection('master_chart')->table(self::TABLE_NAME)
            # return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::COL_WEEK, $week)
            ->select(
                self::COL_ID,
                self::COL_CURRENCY_ID
            )
            ->get()
            ;
    }
}