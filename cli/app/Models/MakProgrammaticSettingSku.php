<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:55
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakProgrammaticSettingSku extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_setting_sku';

    const COL_MAK_PROGRAMMATIC_SETTING_SKU_ID = 'mak_programmatic_setting_sku_id';
    const COL_FK_PRODUCT_SHOP_CHANNEL = 'fk_product_shop_channel';
    const COL_FK_MAK_PROGRAMMATIC_RULE = 'fk_mak_programmatic_rule';
    const COL_MAK_PROGRAMMATIC_SETTING_SKU_CREATED_AT = 'mak_programmatic_setting_sku_created_at';
    const COL_MAK_PROGRAMMATIC_SETTING_SKU_CREATED_BY = 'mak_programmatic_setting_sku_created_by';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_SETTING_SKU_ID;
    public $timestamps = false;

    /**
     * @var int
     */
    const IS_DELETED = 1;

    /**
     * @var int
     */
    const IS_EXIST = 0;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * search By Shop Channel Id
     * @param int $shopChannelId
     * @param array $actionIdArray
     * @return mixed
     */
    public static function searchByShopChannelId($shopChannelId, $actionIdArray)
    {
        $today = date('Y-m-d');

        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticRule::TABLE_NAME,
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_RULE
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_VALIDITY_FROM, '<=', $today)
            ->where(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_VALIDITY_TO, '>=', $today)
            ->where(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_FK_CONFIG_ACTIVE, ConfigActive::ACTIVE)
            ->whereIn(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_FK_MAK_PROGRAMMATIC_ACTION, $actionIdArray)
            ->select(
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID,
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_EXTEND,
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_FK_MAK_PROGRAMMATIC_ACTION
            )
            ->orderBy(MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID)
            ->distinct()
            ->get()
        ;
    }

    /**
     * search By Rule Id
     * @param int $ruleId
     * @return mixed
     */
    public static function searchByRuleId($ruleId)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticRule::TABLE_NAME,
                MakProgrammaticRule::TABLE_NAME.'.'.MakProgrammaticRule::COL_MAK_PROGRAMMATIC_RULE_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_RULE
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->join(
                ProductAds::TABLE_NAME,
                ProductAds::TABLE_NAME.'.'.ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL,
                '=',
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->where(self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_RULE, $ruleId)
            ->select(
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_SELLER_SKU,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_PRICE_RRP_CHANNEL,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_PRICE_POSTSUB_CHANNEL,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK,
                ProductAds::TABLE_NAME.'.'.ProductAds::COL_PRODUCT_ADS_ID,
                ProductAds::TABLE_NAME.'.'.ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID,
                ProductAds::TABLE_NAME.'.'.ProductAds::COL_PRODUCT_ADS_STATE,
                ProductAds::TABLE_NAME.'.'.ProductAds::COL_PRODUCT_ADS_CREATED_AT
            )
            ->get()
        ;
    }

}