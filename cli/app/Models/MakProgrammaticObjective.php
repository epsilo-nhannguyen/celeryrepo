<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:52
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;


class MakProgrammaticObjective extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_objective';

    const COL_MAK_PROGRAMMATIC_OBJECTIVE_ID = 'mak_programmatic_objective_id';
    const COL_MAK_PROGRAMMATIC_OBJECTIVE_NAME = 'mak_programmatic_objective_name';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_OBJECTIVE_ID;
    public $timestamps = false;

    /**
     * @var int
     */
    const KEYWORD_SKU = 1;

    /**
     * @var int
     */
    const SKU = 2;

    /**
     * @var int
     */
    const CAMPAIGN = 3;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

}