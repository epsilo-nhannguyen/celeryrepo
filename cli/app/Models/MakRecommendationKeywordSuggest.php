<?php
/**
 * Created by PhpStorm.
 * User: An Nguyen
 * Date: 30/12/2019
 * Time: 09:33
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakRecommendationKeywordSuggest extends Model
{
    const TABLE_NAME = 'mak_recommendation_keyword_suggest';

    const COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_ID = 'mak_recommendation_keyword_suggest_id';
    const COL_FK_PROGRAMMATIC_KEYWORD_PRODUCT = 'fk_mak_programmatic_keyword_product';
    const COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_QUALITY = 'mak_recommendation_keyword_suggest_quality';
    const COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_SEARCH = 'mak_recommendation_keyword_suggest_search';
    const COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_PRICE = 'mak_recommendation_keyword_suggest_price';
    const COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_CREATED_AT = 'mak_recommendation_keyword_suggest_created_at';
    const COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_UPDATE_AT = 'mak_recommendation_keyword_suggest_update_at';
    const COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_CREATED_BY = 'mak_recommendation_keyword_suggest_created_by';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }


    /**
     * insertGetId
     * @param array $recordArray
     * @return integer
     */
    public static function insertGetId($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insertGetId($recordArray);
    }



}