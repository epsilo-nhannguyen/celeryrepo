<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 24/10/2019
 * Time: 15:07
 */

namespace App\Models\ModelChartShopAds;


use App\Models\Master;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Library;


class ChartKeywordSkuMetric extends Model
{
    const TABLE_NAME = 'chart_keyword_sku_metric';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_DATE = 'date';
    const COL_UPDATED_AT = 'updated_at';
    const COL_TOTAL_SKU = 'total_sku';
    const COL_AVG_KEYWORD_SKU = 'avg_keyword_sku';
    const COL_PERCENT_SKU_SALE_TOTAL_SKU = 'percent_sku_sale_total_sku'; # total Sku Have Sale
    const COL_PERCENT_SKU_KEYWORD_SALE_TOTAL_SKU = 'percent_sku_keyword_sale_total_sku';
    const COL_AVG_PERCENT_DISCOUNT = 'avg_percent_discount'; # total present discount
    const COL_PERCENT_SKU_SELLABLE_STOCK = 'percent_sku_sellable_stock'; # total sku have sellable stock
    const COL_PERCENT_SKU_SALE_SKU_SALE_IN_L30D = 'percent_sku_sale_sku_sale_in_l30d';
    const COL_PERCENT_KEYWORD_SALE_KEYWORD_SALE = 'percent_keyword_sale_keyword_sale';
    const COL_IS_HAVE_ITEM_SOLD_L14D = 'is_have_item_sold_l14d';
    const COL_TOTAL_SKU_KEYWORD = 'total_sku_keyword';
    const COL_TOTAL_SKU_KEYWORD_SALE = 'total_sku_keyword_sale';
    const COL_TOTAL_SKU_IMPRESSION_L30 = 'total_sku_impression_l30';
    const COL_TOTAL_SKU_SALE_L30 = 'total_sku_sale_l30d';
    const COL_TOTAL_SKU_KEYWORD_SALE_L30 = 'total_sku_keyword_sale_l30d';
    const COL_TYPE = 'type';

    use Master;

    /**
     * build Data Insert
     * @param $shopChannelId
     * @param $date
     * @param $totalSku
     * @param $totalSkuKeyword
     * @param $totalSkuHaveSale
     * @param $totalSkuKeywordHaveSale
     * @param $totalPresentDiscount
     * @param $totalSkuHaveSellableStock
     * @param $totalSkuHaveImpressionL30d
     * @param $totalSkuHaveSaleL30d
     * @param $totalSkuKeywordHaveSaleL30d
     * @param $type
     * @return array
     */
    public static function buildDataInsert(
        $shopChannelId, $date, $totalSku, $totalSkuKeyword, $totalSkuHaveSale, $totalSkuKeywordHaveSale, $totalPresentDiscount,
        $totalSkuHaveSellableStock, $totalSkuHaveImpressionL30d, $totalSkuHaveSaleL30d, $totalSkuKeywordHaveSaleL30d, $type
    )
    {
        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_DATE => $date,
            self::COL_TOTAL_SKU => $totalSku,
            self::COL_PERCENT_SKU_SALE_TOTAL_SKU => $totalSkuHaveSale,
            self::COL_TOTAL_SKU_KEYWORD => $totalSkuKeyword,
            self::COL_TOTAL_SKU_KEYWORD_SALE => $totalSkuKeywordHaveSale,
            self::COL_AVG_PERCENT_DISCOUNT => $totalPresentDiscount,
            self::COL_PERCENT_SKU_SELLABLE_STOCK => $totalSkuHaveSellableStock,
            self::COL_TOTAL_SKU_IMPRESSION_L30 => $totalSkuHaveImpressionL30d,
            self::COL_TOTAL_SKU_SALE_L30 => $totalSkuHaveSaleL30d,
            self::COL_TOTAL_SKU_KEYWORD_SALE_L30 => $totalSkuKeywordHaveSaleL30d,
            self::COL_TYPE => $type,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_chart_shop_ads')->table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * update Case
     * @param string $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::connection('master_chart_shop_ads')->statement($sql);
    }

    /**
     * snapshot Update Time
     * @param array $idArray
     * @return int
     */
    public static function snapshotUpdateTime($idArray)
    {
        return DB::connection('master_chart_shop_ads')->table(self::TABLE_NAME)
            ->whereIn(self::COL_ID, $idArray)
            ->update([
                self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ])
        ;
    }

    /**
     * search By Shop Channel Id And Date
     * @param int $shopChannelId
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndDate($shopChannelId, $date)
    {
        return DB::connection('master_chart_shop_ads')->table(self::TABLE_NAME)
        # return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::COL_DATE, $date)
            ->select(
                self::COL_ID,
                self::COL_TYPE
            )
            ->get()
        ;
    }

}