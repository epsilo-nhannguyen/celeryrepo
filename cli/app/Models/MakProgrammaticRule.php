<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:56
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;


class MakProgrammaticRule extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_rule';

    const COL_MAK_PROGRAMMATIC_RULE_ID = 'mak_programmatic_rule_id';
    const COL_FK_MAK_PROGRAMMATIC_ACTION = 'fk_mak_programmatic_action';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_MAK_PROGRAMMATIC_RULE_NAME = 'mak_programmatic_rule_name';
    const COL_MAK_PROGRAMMATIC_RULE_VALIDITY_FROM = 'mak_programmatic_rule_validity_from';
    const COL_MAK_PROGRAMMATIC_RULE_VALIDITY_TO = 'mak_programmatic_rule_validity_to';
    const COL_MAK_PROGRAMMATIC_RULE_EXTEND = 'mak_programmatic_rule_extend';
    const COL_MAK_PROGRAMMATIC_RULE_CREATED_AT = 'mak_programmatic_rule_created_at';
    const COL_MAK_PROGRAMMATIC_RULE_CREATED_BY = 'mak_programmatic_rule_created_by';
    const COL_MAK_PROGRAMMATIC_RULE_UPDATED_AT = 'mak_programmatic_rule_updated_at';
    const COL_MAK_PROGRAMMATIC_RULE_UPDATED_BY = 'mak_programmatic_rule_updated_by';
    const COL_FK_ORGANIZATION = 'fk_organization';
    const COL_FK_VENTURE = 'fk_venture';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_RULE_ID;
    public $timestamps = false;

    /**
     * @var string
     */
    const FOLLOWING_CONDITION_AND = 'and';

    /**
     * @var string
     */
    const FOLLOWING_CONDITION_OR = 'or';

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

}