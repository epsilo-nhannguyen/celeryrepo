<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 01/11/2019
 * Time: 11:47
 */

namespace App\Models\Bi;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Bi3EpsmKbTractionProductShopChannelSkuLevel extends Model
{
    const TABLE_NAME = 'bi3_epsm_kb_traction_product_shop_channel_sku_level';

    const COL_PRODUCT_SHOP_CHANNEL_ID = 'product_shop_channel_id';
    const COL_DATE = 'date';
    const COL_PERCENT_DISCOUNT = 'percent_discount';
    const COL_SELLABLE_STOCK = 'sellable_stock';

    const CONNECTION_NAME = 'bi';

    /**
     * search By Date From To
     * @param string $dateFrom
     * @param string $dateTo
     * @return mixed
     */
    public static function searchByDateFromTo($dateFrom = '', $dateTo = '')
    {
        return DB::connection(self::CONNECTION_NAME)->table(self::TABLE_NAME)
            ->when($dateFrom && $dateTo, function ($query) use ($dateFrom, $dateTo) {
                return $query->whereIn(self::COL_DATE, [$dateFrom, $dateTo]);
            })
            ->select(
                self::COL_PRODUCT_SHOP_CHANNEL_ID,
                self::COL_DATE,
                self::COL_PERCENT_DISCOUNT,
                self::COL_SELLABLE_STOCK
            )
            ->get()
         ;
    }
}