<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 17/11/2019
 * Time: 18:59
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class UniversalCurrency extends ModelBusiness
{
    const TABLE_NAME = 'universal_currency';

    const COL_UNIVERSAL_CURRENCY_ID = 'universal_currency_id';
    const COL_UNIVERSAL_CURRENCY_MONTH = 'universal_currency_month';
    const COL_FK_VENTURE = 'fk_venture';
    const COL_UNIVERSAL_CURRENCY_VALUE = 'universal_currency_value';
    const COL_UNIVERSAL_CURRENCY_CREATED_AT = 'universal_currency_created_at';
    const COL_UNIVERSAL_CURRENCY_CREATED_BY = 'universal_currency_created_by';
    const COL_UNIVERSAL_CURRENCY_CODE = 'universal_currency_code';
    const COL_FK_CURRENCY = 'fk_currency';

    /**
     * getAll
     * @return \Illuminate\Support\Collection
     */
    public static function getAll()
    {
        return DB::table(self::TABLE_NAME)
            ->select(
                self::COL_UNIVERSAL_CURRENCY_MONTH,
                self::COL_UNIVERSAL_CURRENCY_VALUE,
                self::COL_FK_CURRENCY
            )
            ->get()
        ;
    }

}