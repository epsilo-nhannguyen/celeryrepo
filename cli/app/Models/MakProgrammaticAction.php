<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:55
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;


class MakProgrammaticAction extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_action';

    const COL_MAK_PROGRAMMATIC_ACTION_ID = 'mak_programmatic_action_id';
    const COL_MAK_PROGRAMMATIC_ACTION_NAME = 'mak_programmatic_action_name';
    const COL_FK_MAK_PROGRAMMATIC_OBJECTIVE = 'fk_mak_programmatic_objective';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_ACTION_ID;
    public $timestamps = false;

    /**
     * @var int
     */
    const DEACTIVATE_KEYWORD = 1;

    /**
     * @var int
     */
    const ACTIVATE_KEYWORD = 2;

    /**
     * @var int
     */
    const INCREASE_BIDDING_PRICE = 3;

    /**
     * @var int
     */
    const DECREASE_BIDDING_PRICE = 7;

    /**
     * @var int
     */
    const ADD_NEW_KEYWORD = 4;

    /**
     * @var int
     */
    const PAUSE_SKU = 5;

    /**
     * @var int
     */
    const RESUME_SKU = 6;

    /**
     * @var int
     */
    const ADD_NEW_SKU = 8;

    /**
     * @var int
     */
    const ADD_NEW_CAMPAIGN = 9;

    /**
     * @var int
     */
    const PAUSE_CAMPAIGN = 10;

    /**
     * @var int
     */
    const RESUME_CAMPAIGN = 11;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

}