<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:51
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakProgrammaticMetric extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_metric';

    const COL_MAK_PROGRAMMATIC_METRIC_ID = 'mak_programmatic_metric_id';
    const COL_MAK_PROGRAMMATIC_METRIC_NAME = 'mak_programmatic_metric_name';
    const COL_MAK_PROGRAMMATIC_METRIC_UNIT = 'mak_programmatic_metric_unit';
    const COL_FK_MAK_PROGRAMMATIC_OBJECTIVE = 'fk_mak_programmatic_objective';
    const COL_MAK_PROGRAMMATIC_METRIC_VALUE = 'mak_programmatic_metric_value';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_METRIC_ID;
    public $timestamps = false;

    /**
     * @var int
     */
    const SKU_ITEM_SOLD_IN_LAST_3_DAYS = 1;

    /**
     * @var int
     */
    const SKU_ITEM_SOLD_IN_LAST_7_DAYS = 2;

    /**
     * @var int
     */
    const SKU_ITEM_SOLD_IN_LAST_14_DAYS =3 ;

    /**
     * @var int
     */
    const SKU_GMV_IN_LAST_3_DAYS = 4;

    /**
     * @var int
     */
    const SKU_GMV_IN_LAST_7_DAYS = 5;

    /**
     * @var int
     */
    const SKU_GMV_IN_LAST_14_DAYS = 6;

    /**
     * @var int
     */
    const SKU_COST_IN_LAST_3_DAYS = 7;

    /**
     * @var int
     */
    const SKU_COST_IN_LAST_7_DAYS = 8;

    /**
     * @var int
     */
    const SKU_COST_IN_LAST_14_DAYS = 9;

    /**
     * @var int
     */
    const SKU_STOCK_COVERAGE_IN_LAST_3_DAYS = 10;

    /**
     * @var int
     */
    const SKU_STOCK_COVERAGE_IN_LAST_7_DAYS = 11;

    /**
     * @var int
     */
    const SKU_STOCK_COVERAGE_IN_LAST_14_DAYS = 12;

    /**
     * @var int
     */
    const SKU_IMPRESSION_IN_LAST_3_DAYS = 13;

    /**
     * @var int
     */
    const SKU_IMPRESSION_IN_LAST_7_DAYS = 14;

    /**
     * @var int
     */
    const SKU_IMPRESSION_IN_LAST_14_DAYS = 15;

    /**
     * @var int
     */
    const SKU_CLICK_IN_LAST_3_DAYS = 16;

    /**
     * @var int
     */
    const SKU_CLICK_IN_LAST_7_DAYS = 17;

    /**
     * @var int
     */
    const SKU_CLICK_IN_LAST_14_DAYS = 18;

    /**
     * @var int
     */
    const SKU_CPI_IN_LAST_3_DAYS = 19;

    /**
     * @var int
     */
    const SKU_CPI_IN_LAST_7_DAYS = 20;

    /**
     * @var int
     */
    const SKU_CPI_IN_LAST_14_DAYS = 21;

    /**
     * @var int
     */
    const SKU_ACTIVATED_DURATION = 22;

    /**
     * @var int
     */
    const SKU_ACTIVATED_KEYWORD = 23;

    /**
     * @var int
     */
    const SKU_CTR_IN_LAST_3_DAYS = 47;

    /**
     * @var int
     */
    const SKU_CTR_IN_LAST_7_DAYS = 48;

    /**
     * @var int
     */
    const SKU_CTR_IN_LAST_14_DAYS = 49;

    /**
     * @var int
     */
    const SKU_CR_IN_LAST_3_DAYS = 50;

    /**
     * @var int
     */
    const SKU_CR_IN_LAST_7_DAYS = 51;

    /**
     * @var int
     */
    const SKU_CR_IN_LAST_14_DAYS = 52;

    /**
     * @var int
     */
    const SKU_PERCENTAGE_DISCOUNT = 53;

    /**
     * @var int
     */
    const SKU_SELLABLE_STOCK = 54;

    /**
     * @var int
     */
    const KEYWORD_ITEM_SOLD_IN_LAST_3_DAYS = 24;

    /**
     * @var int
     */
    const KEYWORD_ITEM_SOLD_IN_LAST_7_DAYS = 25;

    /**
     * @var int
     */
    const KEYWORD_ITEM_SOLD_IN_LAST_14_DAYS = 26;

    /**
     * @var int
     */
    const KEYWORD_GMV_IN_LAST_3_DAYS = 27;

    /**
     * @var int
     */
    const KEYWORD_GMV_IN_LAST_7_DAYS = 28;

    /**
     * @var int
     */
    const KEYWORD_GMV_IN_LAST_14_DAYS = 29;

    /**
     * @var int
     */
    const KEYWORD_COST_IN_LAST_3_DAYS = 30;

    /**
     * @var int
     */
    const KEYWORD_COST_IN_LAST_7_DAYS = 31;

    /**
     * @var int
     */
    const KEYWORD_COST_IN_LAST_14_DAYS = 32;

    /**
     * @var int
     */
    const KEYWORD_STOCK_COVERAGE_IN_LAST_3_DAYS = 33;

    /**
     * @var int
     */
    const KEYWORD_STOCK_COVERAGE_IN_LAST_7_DAYS = 34;

    /**
     * @var int
     */
    const KEYWORD_STOCK_COVERAGE_IN_LAST_14_DAYS = 35;

    /**
     * @var int
     */
    const KEYWORD_IMPRESSION_IN_LAST_3_DAYS = 36;

    /**
     * @var int
     */
    const KEYWORD_IMPRESSION_IN_LAST_7_DAYS = 37;

    /**
     * @var int
     */
    const KEYWORD_IMPRESSION_IN_LAST_14_DAYS = 38;

    /**
     * @var int
     */
    const KEYWORD_CLICK_IN_LAST_3_DAYS = 39;

    /**
     * @var int
     */
    const KEYWORD_CLICK_IN_LAST_7_DAYS = 40;

    /**
     * @var int
     */
    const KEYWORD_CLICK_IN_LAST_14_DAYS = 41;

    /**
     * @var int
     */
    const KEYWORD_CPI_IN_LAST_3_DAYS = 42;

    /**
     * @var int
     */
    const KEYWORD_CPI_IN_LAST_7_DAYS = 43;

    /**
     * @var int
     */
    const KEYWORD_CPI_IN_LAST_14_DAYS = 44;

    /**
     * @var int
     */
    const KEYWORD_SKU_KEYWORD_RANK = 45;


    /**
     * @var int
     */
    const KEYWORD_ACTIVATED_DURATION = 46;

    /**
     * @var int
     */
    const KEYWORD_CTR_IN_LAST_3_DAYS = 55;

    /**
     * @var int
     */
    const KEYWORD_CTR_IN_LAST_7_DAYS = 56;

    /**
     * @var int
     */
    const KEYWORD_CTR_IN_LAST_14_DAYS = 57;

    /**
     * @var int
     */
    const KEYWORD_CR_IN_LAST_3_DAYS = 58;

    /**
     * @var int
     */
    const KEYWORD_CR_IN_LAST_7_DAYS = 59;

    /**
     * @var int
     */
    const KEYWORD_CR_IN_LAST_14_DAYS = 60;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * search By Objective Id
     * @param $objectiveId
     * @return \Illuminate\Support\Collection
     */
    public static function searchByObjectiveId($objectiveId)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_FK_MAK_PROGRAMMATIC_OBJECTIVE, $objectiveId)
            ->get()
        ;
    }

}