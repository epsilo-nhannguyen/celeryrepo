<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 17:27
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ProductAds extends Library\ModelBusiness
{
    const TABLE_NAME = 'product_ads';

    const COL_PRODUCT_ADS_ID = 'product_ads_id';
    const COL_FK_PRODUCT_SHOP_CHANNEL = 'fk_product_shop_channel';
    const COL_PRODUCT_ADS_TIME_START = 'product_ads_time_start';
    const COL_PRODUCT_ADS_TIME_END = 'product_ads_time_end';
    const COL_PRODUCT_ADS_STATE = 'product_ads_state';
    const COL_PRODUCT_ADS_STATE_PAST = 'product_ads_state_past';
    const COL_PRODUCT_ADS_MODIFY_AT = 'product_ads_modify_at';
    const COL_PRODUCT_ADS_QUOTA_TOTAL = 'product_ads_quota_total';
    const COL_PRODUCT_ADS_QUOTA_DAILY = 'product_ads_quota_daily';
    const COL_PRODUCT_ADS_CHANNEL_ADS_ID = 'product_ads_channel_ads_id';
    const COL_PRODUCT_ADS_MODIFY_BY = 'product_ads_modify_by';
    const COL_PRODUCT_ADS_CREATED_AT = 'product_ads_created_at';
    const COL_PRODUCT_ADS_CREATED_BY = 'product_ads_created_by';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';
    const COL_PRODUCT_ADS_STATE_UPDATED_AT = 'product_ads_state_updated_at';
    const COL_PRODUCT_ADS_STATE_UPDATED_BY = 'product_ads_state_updated_by';
    const COL_FK_MAK_PROGRAMMATIC_CAMPAIGN = 'fk_mak_programmatic_campaign';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_PRODUCT_ADS_ID;
    public $timestamps = false;

    /**
     * @var string
     */
    const STATE_ENDED = 'ended';

    /**
     * @var string
     */
    const STATE_PAUSED = 'paused';

    /**
     * @var string
     */
    const STATE_ONGOING = 'ongoing';

    /**
     * @var string
     */
    const STATE_CLOSED = 'closed';

    /**
     * @var string
     */
    const STATE_SCHEDULED = 'scheduled';

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * import data
     * @param array $filter
     * @param array $data
     * @return mixed
     */
    public static function import($filter, $data)
    {
        return DB::table(self::TABLE_NAME)
            ->updateOrInsert(
                $filter,
                $data
            )
        ;
    }

    /**
     * update State By Id Array
     * @param array $idArray
     * @param string $state
     * @param int $adminId
     * @return mixed
     */
    public static function updateStateByIdArray($idArray, $state, $adminId)
    {
        return DB::table(self::TABLE_NAME)
            ->whereIn(self::COL_PRODUCT_ADS_ID, $idArray)
            ->update([
                self::COL_PRODUCT_ADS_STATE => $state,
                self::COL_PRODUCT_ADS_STATE_PAST => $state,
                self::COL_PRODUCT_ADS_STATE_UPDATED_BY => $adminId,
                self::COL_PRODUCT_ADS_STATE_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            ])
        ;
    }

    /**
     * get By Product Shop Channel Id And Ads Id
     * @param int $productShopChannelId
     * @param string $adsId
     * @param int $shopChannelId
     * @return mixed
     */
    public static function getByProductShopChannelIdAndAdsId($productShopChannelId, $adsId, $shopChannelId)
    {
        return self::where(self::COL_FK_PRODUCT_SHOP_CHANNEL, $productShopChannelId)
            ->where(self::COL_PRODUCT_ADS_CHANNEL_ADS_ID, $adsId)
            ->where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->select(self::COL_PRODUCT_ADS_ID)
            ->first()
        ;
    }

    /**
     * search By Product Id Array
     * @param array $productIdArray
     * @return array
     */
    public static function searchByProductIdArray($productIdArray)
    {
        if ( ! $productIdArray) return [];

        return DB::table(self::TABLE_NAME)
            ->whereIn(self::COL_FK_PRODUCT_SHOP_CHANNEL, $productIdArray)
            ->select(
                self::COL_PRODUCT_ADS_ID,
                self::COL_PRODUCT_ADS_STATE,
                self::COL_PRODUCT_ADS_STATE_UPDATED_BY,
                self::COL_PRODUCT_ADS_CHANNEL_ADS_ID,
                self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->get()
        ;
    }

    /**
     * search By Shop Channel Id
     * @param int $shopChannelId
     * @param array $stateArray
     * @return mixed
     */
    public static function searchByShopChannelId($shopChannelId, $stateArray = [])
    {
        return self::where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->when($stateArray, function ($query) use ($stateArray) {
                return $query->whereIn(self::COL_PRODUCT_ADS_STATE, $stateArray);
            })
            ->select(
                self::COL_FK_PRODUCT_SHOP_CHANNEL,
                self::COL_PRODUCT_ADS_CHANNEL_ADS_ID,
                self::COL_PRODUCT_ADS_STATE
            )
            ->get()
        ;
    }

    /**
     * search By Campaign Id And State
     * @param int $campaignId
     * @param string $state
     * @return \Illuminate\Support\Collection
     */
    public static function searchByCampaignIdAndState($campaignId, $state)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->join(
                ProductShopChannelCampaign::TABLE_NAME,
                ProductShopChannelCampaign::TABLE_NAME.'.'.ProductShopChannelCampaign::COL_FK_PRODUCT_SHOP_CHANNEL,
                '=',
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->where(ProductShopChannelCampaign::TABLE_NAME.'.'.ProductShopChannelCampaign::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN, $campaignId)
            ->where(self::TABLE_NAME.'.'.self::COL_PRODUCT_ADS_STATE, $state)
            ->select(
                self::TABLE_NAME.'.'.self::COL_PRODUCT_ADS_ID,
                self::TABLE_NAME.'.'.self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->distinct()
            ->get()
        ;
    }

}