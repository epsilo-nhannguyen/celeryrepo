<?php


namespace App\Models\ModelStat;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Currency extends Model
{

    /**
     * @return mixed
     */
    public static function getAll()
    {
        $model = DB::connection('master_stat')->table('daily_1_1_511_0035')
            ->orderBy('time')
        ;
        return $model->get();
    }

}