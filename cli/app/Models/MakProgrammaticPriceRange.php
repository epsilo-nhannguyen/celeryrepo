<?php
/**
 * This class is generated automatically by schema_update. !!! Do not touch or modify
 * Last modified : 2019-10-01 09:10:56
 * Class DbTable_Mak_Programmatic_Keyword_Product
 */

namespace App\Models;

use App\Library\Model\Sql\Manager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MakProgrammaticPriceRange extends Model
{
    const TABLE_NAME = 'mak_recommendation_price_range';

    const COL_MAK_PROGRAMMATIC_PRICE_RANGE_ID = 'mak_recommendation_price_range_id';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT = 'fk_mak_programmatic_keyword_product';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';
    const COL_MAK_PROGRAMMATIC_PRICE_RANGE_JSON = 'mak_recommendation_price_range_json';
    const COL_MAK_PROGRAMMATIC_PRICE_RANGE_BIDDING = 'mak_recommendation_price_range_bidding';
    const COL_MAK_PROGRAMMATIC_PRICE_RANGE_DATE = 'mak_recommendation_price_range_date';
    const COL_MAK_PROGRAMMATIC_PRICE_RANGE_CREATED_AT = 'mak_recommendation_price_range_created_at';
    const COL_MAK_PROGRAMMATIC_PRICE_RANGE_CREATED_BY = 'mak_recommendation_price_range_updated_at';


    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_PRICE_RANGE_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray,$DBConnect = null)
    {
        if(!$DBConnect){
            $DBConnect = env('DB_CONNECTION',Manager::SQL_CHART_CONNECT);
        }
        return DB::connection($DBConnect)->table(self::TABLE_NAME)->insert($recordArray);
    }

}