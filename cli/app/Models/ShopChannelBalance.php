<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 28/09/2019
 * Time: 17:55
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ShopChannelBalance extends Library\ModelBusiness
{
    const TABLE_NAME = 'shop_channel_balance';

    const COL_SHOP_CHANNEL_BALANCE_ID = 'shop_channel_balance_id';
    const COL_SHOP_CHANNEL_BALANCE_VALUE = 'shop_channel_balance_value';
    const COL_SHOP_CHANNEL_BALANCE_UPDATED_AT = 'shop_channel_balance_updated_at';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_SHOP_CHANNEL_BALANCE_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * import data
     * @param int $shopChannelId
     * @param float $balance
     * @return mixed
     */
    public static function import($shopChannelId, $balance)
    {
        return DB::table(self::TABLE_NAME)
            ->updateOrInsert(
                [
                    self::COL_FK_SHOP_CHANNEL => $shopChannelId,
                ],
                [
                    self::COL_SHOP_CHANNEL_BALANCE_VALUE => $balance,
                    self::COL_SHOP_CHANNEL_BALANCE_UPDATED_AT => Library\Common::getCurrentTimestamp(),
                ]
            )
        ;
    }
}