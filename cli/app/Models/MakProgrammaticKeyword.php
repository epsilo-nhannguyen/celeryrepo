<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 09:33
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakProgrammaticKeyword extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_keyword';

    const COL_MAK_PROGRAMMATIC_KEYWORD_ID = 'mak_programmatic_keyword_id';
    const COL_MAK_PROGRAMMATIC_KEYWORD_NAME = 'mak_programmatic_keyword_name';
    const COL_FK_ORGANIZATION = 'fk_organization';
    const COL_FK_VENTURE = 'fk_venture';
    const COL_MAK_PROGRAMMATIC_KEYWORD_CREATED_AT = 'mak_programmatic_keyword_created_at';
    const COL_MAK_PROGRAMMATIC_KEYWORD_CREATED_BY = 'mak_programmatic_keyword_created_by';
    const COL_MAK_PROGRAMMATIC_KEYWORD_UPDATED_AT = 'mak_programmatic_keyword_updated_at';
    const COL_MAK_PROGRAMMATIC_KEYWORD_UPDATED_BY = 'mak_programmatic_keyword_updated_by';
    const COL_FK_CHANNEL_CATEGORY = 'fk_channel_category';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_KEYWORD_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }


    /**
     * insertGetId
     * @param array $recordArray
     * @return integer
     */
    public static function insertGetId($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insertGetId($recordArray);
    }

    /**search By Organization Id And Venture Id
     * @param int $organizationId
     * @param int $ventureId
     * @return mixed
     */
    public static function searchByOrganizationIdAndVentureId($organizationId, $ventureId)
    {
        return self::where(self::COL_FK_ORGANIZATION, $organizationId)
            ->where(self::COL_FK_VENTURE, $ventureId)
            ->select(
                self::COL_MAK_PROGRAMMATIC_KEYWORD_ID,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_NAME
            )
            ->get()
        ;
    }
}