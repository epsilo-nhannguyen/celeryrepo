<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 18:29
 */

namespace App\Models\Mongo;


use App\Library;


class MakRankingKeyword
{
    /**
     * batch Insert
     * @param array $documentArray
     * @return \MongoDB\Driver\WriteResult
     * @throws \Exception
     */
    public static function batchInsert($documentArray)
    {
        $documentArray = array_values($documentArray);
        return Manager::getInstance()->batchInsert(
            'mak_ranking_keyword',
            $documentArray
        );
    }

    /**
     * @param int $channelId
     * @param int $shopId
     * @param string $keywordName
     * @param array $assocItemIdRank
     * @return array
     */
    public static function buildDocument($channelId, $shopId, $keywordName, $assocItemIdRank)
    {
        $channelId = intval($channelId);
        $shopId = intval($shopId);
        $keywordName = trim($keywordName);

        return [
            'channelId' => $channelId,
            'shopId' => $shopId,
            'isImported' => 0,
            'updatedAt' => Library\Common::getCurrentTimestamp(),
            'data' => $assocItemIdRank,
            'keyword' => $keywordName,
        ];
    }

    /**
     * search For Import
     * @param int $shopId
     * @param int $channelId
     * @return \MongoDB\Driver\Cursor
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchForImport($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        return Manager::getInstance()->select(
            'mak_ranking_keyword',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'isImported' => 0
            ],
            [],
            [
                'limit' => 1000
            ]
        );
    }

    /**
     * update Imported
     * @param $objectIdArray
     * @return \MongoDB\Driver\WriteResult
     */
    public static function updateImported($objectIdArray)
    {
        $objectIdArray = (array) $objectIdArray;

        return Manager::getInstance()->update(
            'mak_ranking_keyword',
            [
                '_id' => [
                    '$in' => $objectIdArray
                ]
            ],
            [
                'isImported' => 1
            ],
            [
                'multi' => true,
                'upsert' => false
            ]
        );
    }
}