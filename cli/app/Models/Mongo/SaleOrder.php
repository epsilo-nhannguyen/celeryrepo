<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 08/10/2019
 * Time: 11:14
 */

namespace App\Models\Mongo;


use App\Library;


class SaleOrder
{

    /**
     * @param int $shopId
     * @param int $channelId
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function getMaxUpdatedAt($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        $result = Manager::getInstance()->max(
            'sale_order',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
            ],
            'updateAt'
        );

        return $result;
    }

    /**
     * get Data Sale Order Date From To
     * @param int $shopId
     * @param int $channelId
     * @param int $dateFrom
     * @param int $dateTo
     * @return mixed|null
     */
    public static function getDataSaleOrderDateFromTo($shopId, $channelId, $dateFrom, $dateTo)
    {
        try {
            $match = [
                'shopId' => intval($shopId),
                'channelId' => intval($channelId),
                'data.createdAt' => [
                    '$gte' => intval($dateFrom),
                    '$lte' => intval($dateTo)
                ],
            ];

            return Manager::getInstance()->aggregate(
                'sale_order',
                [
                    ['$match' => $match],
                    ['$group' => [
                        '_id' => null,
                        'totalItem' => ['$sum' => ['$size' => '$data.arrayItem']],
                        'totalPrice' => ['$sum' => ['$sum' => '$data.totalPrice']]
                    ]]
                ]
            );
        } catch (\MongoDB\Driver\Exception\Exception $e) {
            return null;
        }
    }

    /**
     * search By Shop Id Channel Id
     * @param int $shopId
     * @param int $channelId
     * @param int|null $dateFrom
     * @param int|null $dateTo
     * @return array
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchByShopIdChannelId($shopId, $channelId, $dateFrom = null, $dateTo = null)
    {
        $filter = [
            'shopId' => intval($shopId),
            'channelId' => intval($channelId)
        ];

        if ($dateFrom && $dateTo) {
            $filter['data.createdAt'] = [
                '$gte' => intval($dateFrom),
                '$lte' => intval($dateTo)
            ];
        }

        $column = [
            '_id' => 0,
            'data.totalPrice' => 1,
            'data.createdAt' => 1,
            'data.arrayItem.itemId' => 1,
            'data.arrayItem.itemPrice' => 1,
        ];

        $result = Manager::getInstance()->select(
            'sale_order',
            $filter,
            $column
        )->toArray();

        return $result;
    }

}