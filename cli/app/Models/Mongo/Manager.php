<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 18:02
 */

namespace App\Models\Mongo;


use App\Library;
use Exception;


class Manager extends Library\Singleton
{
    /**
     * @var \MongoDB\Driver\Manager
     */
    protected $manager;

    protected $database;

    public function __construct()
    {
        if (env('MONGO_USERNAME') && env('MONGO_PASSWORD')){
            $this->manager = new \MongoDB\Driver\Manager(
                'mongodb://'.env('MONGO_USERNAME').':'.env('MONGO_PASSWORD').'@'.
                env('MONGO_HOST').':'.env('MONGO_PORT').'/'.env('MONGO_DATABASE').'?authenticationDatabase='.env('MONGO_DATABASE')
            );
        } else {
            $this->manager = new \MongoDB\Driver\Manager(
                'mongodb://'.env('MONGO_HOST').':'.env('MONGO_PORT'));
        }
        $this->database = env('MONGO_DATABASE')??'epsilo';
    }

    /**
     * @param $collectionName
     * @param $document
     * @return \MongoDB\Driver\WriteResult
     */
    public function insert($collectionName, $document)
    {
        $bulk = new \MongoDB\Driver\BulkWrite;
        $bulk->insert($document);
        $result = $this->manager->executeBulkWrite($this->database.'.'.$collectionName, $bulk);
        return $result;
    }

    /**
     * @param $collectionName
     * @param $filter
     * @param $data
     * @param $option ['multi' => false, 'upsert' => false]
     * @param $method
     * @return \MongoDB\Driver\WriteResult
     */
    public function update($collectionName, $filter, $data, $option, $method = '$set')
    {
        $bulk = new \MongoDB\Driver\BulkWrite;
        $bulk->update(
            $filter,
            [$method => $data],
            $option
        );
        $result = $this->manager->executeBulkWrite($this->database.'.'.$collectionName, $bulk);
        return $result;
    }

    /**
     * @param $collectionName
     * @param $filter
     * @param $column
     * @param $options
     * @return \MongoDB\Driver\Cursor
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function select($collectionName, $filter, $column = [], $options = [])
    {
        if ($column) {
            $options['projection'] = $column;
        }
        $query = new \MongoDB\Driver\Query($filter, $options);
        $rows = $this->manager->executeQuery($this->database . '.' . $collectionName, $query); // $mongo contains the connection object to MongoDB
        return $rows;
    }

    public function count($collectionName, $filter, $option)
    {
        $command = new \MongoDB\Driver\Command(['count' => $collectionName, 'query' => $filter]);
        $obj = $this->manager->executeCommand($this->database, $command); // $mongo contains the connection object to MongoDB
        $obj = current($obj->toArray());
        return $obj->n;
    }

    /**
     * @param $collectionName
     * @param $filter
     * @param $columnMax
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function max($collectionName, $filter, $columnMax)
    {
        $query = ['$group' => ['_id' => null, 'max' => ['$max' => '$'.$columnMax]]];

        if ($filter) {
            $match = ['$match' => $filter];
            $pipeline = [$match, $query];
        } else {
            $pipeline = [$query];
        }

        $command = new \MongoDB\Driver\Command([
            'aggregate' => $collectionName,
            'pipeline' => $pipeline,
            'cursor' => ['batchSize' => 1]
        ]);
        $rows = $this->manager->executeCommand($this->database, $command)->toArray();
        $rows = (array) ($rows[0] ?? []);

        return $rows['max'] ?? 0;
    }

    /**
     * @return \MongoDB\Driver\Manager
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Insert multi rows
     * @param string $collectionName
     * @param array $documentArray
     * @return \MongoDB\Driver\WriteResult
     * @throws Exception
     */
    public function batchInsert($collectionName, $documentArray)
    {
        $bulk = new \MongoDB\Driver\BulkWrite;
        foreach ($documentArray as $key => $document) {
            if (!is_int($key)) {
                throw new Exception('Please format correctly!');
            }
        }

        foreach ($documentArray as $document){
            $bulk->insert($document);
        }

        $result = $this->manager->executeBulkWrite($this->database . '.' . $collectionName, $bulk);
        return $result;
    }

    /**
     * @param $collectionName
     * @param $pipeline
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function aggregate($collectionName, $pipeline)
    {
        $command = new \MongoDB\Driver\Command([
            'aggregate' => $collectionName,
            'pipeline' => $pipeline,
            'cursor' => ['batchSize' => 1]
        ]);
        $rows = $this->manager->executeCommand($this->database, $command)->toArray();
        return $rows;
    }
}