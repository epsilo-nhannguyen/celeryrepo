<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 18:17
 */

namespace App\Models\Mongo;


use App\Library;


class Category
{

    /**
     * search By Shop Id Channel Id
     * @param int $shopId
     * @param int $channelId
     * @return \MongoDB\Driver\Cursor
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchByShopIdChannelId($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        return Manager::getInstance()->select(
            'category',
            [
                'isImported' => 0,
                'shopId' => $shopId,
                'channelId' => $channelId
            ],
            [
                'data' => 1,
                'identify' => 1
            ],
            [
                'limit' => 500
            ]
        );
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @param Library\DataStructure\ChannelCategory $category
     * @return \MongoDB\Driver\WriteResult
     */
    public static function save($shopId, $channelId, Library\DataStructure\ChannelCategory $category)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        return Manager::getInstance()->update(
            'category',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'identify' => $category->getChannelIdentify()
            ],
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'identify' => $category->getChannelIdentify(),
                'isImported' => 0,
                'updatedAt' => Library\Common::getCurrentTimestamp(),
                'data' => [
                    'level_1' => $category->getCategoryByLevel(1),
                    'level_2' => $category->getCategoryByLevel(2),
                    'level_3' => $category->getCategoryByLevel(3),
                    'level_4' => $category->getCategoryByLevel(4),
                    'level_5' => $category->getCategoryByLevel(5),
                ]
            ],
            [
                'multi' => false,
                'upsert' => true
            ]
        );
    }

    /**
     * update Imported
     * @param int $shopId
     * @param int $channelId
     * @param array $arrayIdentify
     * @return \MongoDB\Driver\WriteResult
     */
    public static function updateImported($shopId, $channelId, $arrayIdentify)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $arrayIdentify = array_map('trim', (array) $arrayIdentify);

        return Manager::getInstance()->update(
            'category',
            [
                'identify' => [
                    '$in' => $arrayIdentify
                ],
                'shopId' => $shopId,
                'channelId' => $channelId,
            ],
            [
                'isImported' => 1
            ],
            [
                'multi' => true
            ]
        );
    }
}