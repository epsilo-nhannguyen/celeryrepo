<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 18:08
 */

namespace App\Models\Mongo;


class CrawlerAds
{
    /**
     * batch Insert
     * @param array $documentArray
     * @return \MongoDB\Driver\WriteResult
     * @throws \Exception
     */
    public static function batchInsert($documentArray)
    {
        $documentArray = array_values($documentArray);
        return Manager::getInstance()->batchInsert(
            'crawler_ads',
            $documentArray
        );
    }

    /**
     * update Imported
     * @param int $shopId
     * @param int $channelId
     * @param array $idImportedArray
     * @return \MongoDB\Driver\WriteResult
     */
    public static function updateImported($shopId, $channelId, $idImportedArray)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $idImportedArray = (array) $idImportedArray;

        return Manager::getInstance()->update(
            'crawler_ads',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                '_id' => [
                    '$in' => $idImportedArray
                ],
            ],
            [
                'isImported' => 1,
            ],
            [
                'multi' => true,
                'upsert' => false
            ]
        );
    }

    /**
     * search For Import
     * @param int $shopId
     * @param int $channelId
     * @return array
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchForImport($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        return Manager::getInstance()->select(
            'crawler_ads',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'isImported' => 0
            ],
            [],
            [
                'limit' => 1000
            ]
        )->toArray();
    }

}