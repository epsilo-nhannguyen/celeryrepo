<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 28/09/2019
 * Time: 18:32
 */

namespace App\Models\Mongo;


use App\Library;
use App\Models;


class KeywordProduct
{

    /**
     * @param array $documentArray
     * @return \MongoDB\Driver\WriteResult
     * @throws \Exception
     */
    public static function batchInsert($documentArray)
    {
        $documentArray = array_values($documentArray);
        return Manager::getInstance()->batchInsert(
            'keyword_product',
            $documentArray
        );
    }

    /**
     * @param $mongoObjectId
     * @param $shopId
     * @param $channelId
     * @param $productIdentify
     * @param $totalQuota
     * @param $dailyQuota
     * @param $startTime
     * @param $endTime
     * @param $state
     * @param $adsIdentify
     * @param $keywords
     * @param $dataRaw
     * @return \MongoDB\Driver\WriteResult
     */
    public static function update(
        $mongoObjectId,
        $shopId,
        $channelId,
        $productIdentify,
        $totalQuota,
        $dailyQuota,
        $startTime,
        $endTime,
        $state,
        $adsIdentify,
        $keywords,
        $dataRaw
    ) {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $productIdentify = trim($productIdentify);
        $totalQuota = floatval($totalQuota);
        $dailyQuota = floatval($dailyQuota);
        $startTime = trim($startTime);
        $endTime = trim($endTime);
        $state = trim($state);
        $adsIdentify = trim($adsIdentify);

        return Manager::getInstance()->update(
            'keyword_product',
            [
                '_id' => $mongoObjectId,
            ],
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'productIdentify' => $productIdentify,
                'isImported' => 0,
                'isGetPaidReports' => 0,
                'updatedAt' => Library\Common::getCurrentTimestamp(),
                'data' => [
                    'totalQuota' => $totalQuota,
                    'dailyQuota' => $dailyQuota,
                    'startTime' => $startTime,
                    'endTime' => $endTime,
                    'state' => $state,
                    'adsId' => $adsIdentify,
                    'keywords' => $keywords
                ],
                'raw' => $dataRaw
            ],
            [
                'multi' => false,
                'upsert' => false
            ]
        );
    }

    /**
     * search By Product Identify Array
     * @param int $shopId
     * @param int $channelId
     * @param array $productIdentifyArray
     * @return \MongoDB\Driver\Cursor
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchByProductIdentifyArray($shopId, $channelId, $productIdentifyArray)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $productIdentifyArray = array_map('trim', (array) $productIdentifyArray);

        return Manager::getInstance()->select(
            'keyword_product',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'productIdentify' => [
                    '$in' => $productIdentifyArray
                ]
            ],
            [
                'productIdentify' => 1
            ]
        );
    }

    /**
     * search For Import
     * @param int $shopId
     * @param int $channelId
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchForImport($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        $result = Manager::getInstance()->select(
            'keyword_product',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'isImported' => 0,
                'data.state' => [
                    '$in' => [
                        Models\ProductAds::STATE_ENDED,
                        Models\ProductAds::STATE_ONGOING,
                        Models\ProductAds::STATE_PAUSED,
                        Models\ProductAds::STATE_CLOSED,
                        Models\ProductAds::STATE_SCHEDULED,
                    ]
                ]
            ],
            [
                'data' => 1,
                'productIdentify' => 1
            ],
            [
                'limit' => 500
            ]
        )->toArray();

        return Library\Formater::stdClassToArray($result);
    }

    /**
     * update Imported
     * @param int $shopId
     * @param int $channelId
     * @param array $arrayProductIdentify
     * @return \MongoDB\Driver\WriteResult
     */
    public static function updateImported($shopId, $channelId, $arrayProductIdentify)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $arrayProductIdentify = array_map('trim', (array) $arrayProductIdentify);

        return Manager::getInstance()->update(
            'keyword_product',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'productIdentify' => ['$in' => $arrayProductIdentify],
            ],
            [
                'isImported' => 1
            ],
            [
                'multi' => true,
                'upsert' => false
            ]
        );
    }
}