<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 10:31
 */

namespace App\Models\Mongo;


use App\Library;


class Product
{

    /**
     * @param array $documentArray
     * @return \MongoDB\Driver\WriteResult
     * @throws \Exception
     */
    public static function batchInsert($documentArray)
    {
        return Manager::getInstance()->batchInsert(
            'product',
            $documentArray
        );
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function getMaxUpdatedAt($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        $result = Manager::getInstance()->max(
            'product',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
            ],
            'updatedAt'
        );

        return $result;
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @param Library\DataStructure\ChannelProduct $product
     * @param bool $upsert
     * @return \MongoDB\Driver\WriteResult
     */
    public static function save($shopId, $channelId, Library\DataStructure\ChannelProduct $product, $upsert = false)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $upsert = boolval($upsert);

        return Manager::getInstance()->update(
            'product',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'identify' => $product->getChannelIdentify()
            ],
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'identify' => $product->getChannelIdentify(),
                'isImported' => 0,
                'updatedAt' => Library\Common::getCurrentTimestamp(),
                'data' => $product->extractArray()
            ],
            [
                'multi' => false,
                'upsert' => $upsert
            ]
        );
    }

    /**
     * search By Shop Id Channel Id
     * @param int $shopId
     * @param int $channelId
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchByShopIdChannelId($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        $result = Manager::getInstance()->select(
            'product',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'isImported' => 0,
            ],
            [
                'data' => 1,
            ],
            [
                'limit' => 400
            ]
        )->toArray();

        return Library\Formater::stdClassToArray($result);
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @param array $identifyArray
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchByIdentifyArray($shopId, $channelId, $identifyArray)
    {
        if ( ! $identifyArray) return [];

        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $identifyArray = array_map('trim', (array) $identifyArray);

        $result = Manager::getInstance()->select(
            'product',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'identify' => [
                    '$in' => $identifyArray
                ]
            ],
            [
                'data' => 1
            ]
        )->toArray();

        return Library\Formater::stdClassToArray($result);
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @param array $arrayIdentify
     * @return \MongoDB\Driver\WriteResult
     */
    public static function updateImported($shopId, $channelId, $arrayIdentify)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $arrayIdentify = array_map('trim', (array) $arrayIdentify);

        return Manager::getInstance()->update(
            'product',
            [
                'identify' => ['$in' => $arrayIdentify],
                'shopId' => $shopId,
                'channelId' => $channelId,
            ],
            [
                'isImported' => 1
            ],
            [
                'multi' => true
            ]
        );
    }
}