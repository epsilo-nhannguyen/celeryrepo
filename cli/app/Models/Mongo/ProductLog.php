<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 10:31
 */

namespace App\Models\Mongo;


use App\Library;


class ProductLog
{

    /**
     * batch Insert
     * @param array $documentArray
     * @return \MongoDB\Driver\WriteResult
     * @throws \Exception
     */
    public static function batchInsert($documentArray)
    {
        return Manager::getInstance()->batchInsert(
            'product_log',
            $documentArray
        );
    }

    /**
     * search By Shop Id Channel Id
     * @param int $shopId
     * @param int $channelId
     * @param string $dateFrom
     * @param string $dateTo
     * @param int $hour
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function searchByShopIdChannelId($shopId, $channelId, $dateFrom, $dateTo, $hour = null)
    {
        $filter = [
            'shopId' => intval($shopId),
            'channelId' => intval($channelId),
            'date' => [
                '$gte' => trim($dateFrom),
                '$lte' => trim($dateTo)
            ],
        ];
        if ($hour !== null) {
            $filter['hour'] = intval($hour);
        }
        $result = Manager::getInstance()->select(
            'product_log',
            $filter,
            [
                '_id' => 0,
                'productShopChannelId' => 1,
                'presentDiscount' => 1,
                'sellableStock' => 1,
                'date' => 1,
                'hour' => 1,
            ]
        )->toArray();

        return Library\Formater::stdClassToArray($result);
    }

}