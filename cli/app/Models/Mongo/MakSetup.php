<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 11:14
 */

namespace App\Models\Mongo;


class MakSetup
{

    /**
     * @var string
     */
    const SETUP_PULL_ORDER = 'isPullOrder';

    /**
     * @var string
     */
    const SETUP_PULL_PRODUCT = 'isPullProduct';

    /**
     * @var string
     */
    const SETUP_PULL_KEYWORD = 'isPullKeyword';

    /**
     * @var string
     */
    const SETUP_PULL_CATEGORY = 'isPullCategory';

    /**
     * @param int $shopId
     * @param int $channelId
     * @param string $setupName
     * @return bool|\MongoDB\Driver\WriteResult
     */
    public static function updateSetupDone($shopId, $channelId, $setupName)
    {
        $setupArray = [
            self::SETUP_PULL_ORDER,
            self::SETUP_PULL_PRODUCT,
            self::SETUP_PULL_KEYWORD,
            self::SETUP_PULL_CATEGORY,
        ];

        $setupName = trim($setupName);
        if ( ! in_array($setupName, $setupArray)) {
            return false;
        }

        $shopId = intval($shopId);
        $channelId = intval($channelId);

        return Manager::getInstance()->update(
            'mak_setup',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                $setupName => 0,
            ],
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                $setupName => 1,
            ],
            [
                'multi' => false,
            ]
        );
    }
}