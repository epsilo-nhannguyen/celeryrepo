<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 08/10/2019
 * Time: 11:12
 */

namespace App\Models\Mongo;


use App\Library;


class ShopChannel
{

    /**
     * get By Shop Id Channel Id
     * @param int $shopId
     * @param int $channelId
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function getByShopIdChannelId($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        $result = current(Manager::getInstance()->select(
            'shop_channel',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
            ]
        )->toArray());

        return Library\Formater::stdClassToArray($result);
    }

    /**
     * insert Last Time Pull Product
     * @param int $shopId
     * @param int $channelId
     * @param int $lastTimePullProduct
     * @return \MongoDB\Driver\WriteResult
     */
    public static function insertLastTimePullProduct($shopId, $channelId, $lastTimePullProduct)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $lastTimePullProduct = intval($lastTimePullProduct);

        return Manager::getInstance()->insert(
            'shop_channel',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'last_time_pull_product' => $lastTimePullProduct
            ]
        );
    }

    /**
     * update Last Time Pull Product
     * @param int $shopId
     * @param int $channelId
     * @param int $lastTimePullProduct
     * @return \MongoDB\Driver\WriteResult
     */
    public static function updateLastTimePullProduct($shopId, $channelId, $lastTimePullProduct)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $lastTimePullProduct = intval($lastTimePullProduct);

        return Manager::getInstance()->update(
            'shop_channel',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'last_time_pull_product' => [
                    '$lte' => $lastTimePullProduct
                ]
            ],
            [
                'last_time_pull_product' => $lastTimePullProduct
            ],
            []
        );
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @param int $lastTimePullOrder
     * @return \MongoDB\Driver\WriteResult
     */
    public static function insertLastTimePullOrder($shopId, $channelId, $lastTimePullOrder)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $lastTimePullOrder = intval($lastTimePullOrder);

        return Manager::getInstance()->insert(
            'shop_channel',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'last_time_raw' => $lastTimePullOrder
            ]
        );
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @param int $lastTimePullOrder
     * @return \MongoDB\Driver\WriteResult
     */
    public static function updateLastTimePullOrder($shopId, $channelId, $lastTimePullOrder)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);
        $lastTimePullOrder = intval($lastTimePullOrder);

        return Manager::getInstance()->update(
            'shop_channel',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
                'last_time_raw' => [
                    '$lte' => $lastTimePullOrder
                ]
            ],
            [
                'last_time_raw' => $lastTimePullOrder
            ],
            []
        );
    }

}