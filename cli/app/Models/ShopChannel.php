<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 14:28
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Library;


class ShopChannel extends Library\ModelBusiness
{
    const TABLE_NAME = 'shop_channel';

    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_SHOP_CHANNEL_NAME = 'shop_channel_name';
    const COL_FK_SHOP_MASTER = 'fk_shop_master';
    const COL_FK_CHANNEL = 'fk_channel';
    const COL_SHOP_CHANNEL_API_CREDENTIAL = 'shop_channel_api_credential';
    const COL_SHOP_CHANNEL_API_CREDENTIAL_UPDATED_AT = 'shop_channel_api_credential_updated_at';
    const COL_SHOP_CHANNEL_LINKED = 'shop_channel_linked';
    const COL_SHOP_CHANNEL_INIT = 'shop_channel_init';
    const COL_SHOP_CHANNEL_LOGISTICS_ADDRESS = 'shop_channel_logistics_address';
    const COL_SHOP_CHANNEL_LAST_TIME_PULL_PRODUCT = 'shop_channel_last_time_pull_product';
    const COL_SHOP_CHANNEL_LAST_TIME_PULL_ORDER = 'shop_channel_last_time_pull_order';
    const COL_SHOP_CHANNEL_CREATED_AT = 'shop_channel_created_at';
    const COL_SHOP_CHANNEL_UPDATED_AT = 'shop_channel_updated_at';
    const COL_SHOP_CHANNEL_CREATED_BY = 'shop_channel_created_by';
    const COL_SHOP_CHANNEL_UPDATED_BY = 'shop_channel_updated_by';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_SHOP_CHANNEL_QUOTA_PULL_ORDER = 'shop_channel_quota_pull_order';
    const COL_SHOP_CHANNEL_LAST_TIME_PULL_SELLABLE = 'shop_channel_last_time_pull_sellable';
    const COL_SHOP_CHANNEL_LAST_TIME_SYNC_STOCK = 'shop_channel_last_time_sync_stock';
    const COL_SHOP_CHANNEL_LAST_TIME_CALCULATE_STOCK = 'shop_channel_last_time_calculate_stock';
    const COL_SHOP_CHANNEL_LAST_TIME_CUTOFF_STOCK = 'shop_channel_last_time_cutoff_stock';
    const COL_SHOP_CHANNEL_FIRST_TIME_CRAWLER_PAID_ADS = 'shop_channel_first_time_crawler_paid_ads';
    const COL_SHOP_CHANNEL_FIRST_TIME_CRAWLER_TARGETING = 'shop_channel_first_time_crawler_targeting';
    const COL_SHOP_CHANNEL_IS_CONNECTION_API_VALID = 'shop_channel_is_connection_api_valid';
    const COL_SHOP_CHANNEL_IS_CONNECTION_CREDENTIAL_VALID = 'shop_channel_is_connection_credential_valid';
    const COL_SHOP_CHANNEL_CHECK_API_VALID_UPDATED_AT = 'shop_channel_check_api_valid_updated_at';
    const COL_SHOP_CHANNEL_CHECK_CREDENTIAL_VALID_UPDATED_AT = 'shop_channel_check_credential_valid_updated_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_SHOP_CHANNEL_ID;
    public $timestamps = false;

    private static $_cache = [];

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
//        return self::find($id);
        return self::join('channel', 'channel.channel_id','=','shop_channel.fk_channel')
            ->where(self::COL_SHOP_CHANNEL_ID, $id)
            ->first()
            ;
    }

    public static function updateCredential($id, $credential)
    {
        $id = intval($id);
        $credential = trim($credential);

        DB::table('shop_channel')->where('shop_channel_id', $id)->update([
            'shop_channel_api_credential' => $credential,
            'shop_channel_api_credential_updated_at' => strtotime('now')
        ]);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getAllActive()
    {
        $select = DB::connection('master_business')->table('shop_channel')
            ->join('channel', 'channel_id', '=', 'fk_channel')
            ->where('shop_channel.fk_config_active', 1)
            ->select([DB::raw('shop_channel.*'), 'fk_venture', 'channel_code'])
        ;

        return $select->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getAll()
    {
        $select = DB::connection('master_business')->table('shop_channel')
            ->join('shop_ads', 'shop_ads.shop_channel_id', '=', 'shop_channel.shop_channel_id')
            ->where('fk_config_active', 1)
        ;

        return $select->get();
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @return mixed
     */
    public static function getByShopIdChannelId($shopId, $channelId)
    {
        return self::where(self::COL_FK_SHOP_MASTER, $shopId)
            ->where(self::COL_FK_CHANNEL, $channelId)
            ->first()
        ;
    }

    /**
     * update By Shop Id Channel Id
     * @param int $shopId
     * @param int $channelId
     * @param string $name
     * @param int $adminId
     * @return mixed
     */
    public static function updateByShopIdChannelId($shopId, $channelId, $name, $adminId) {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_FK_SHOP_MASTER, $shopId)
            ->where(self::COL_FK_CHANNEL, $channelId)
            ->update([
                self::COL_SHOP_CHANNEL_NAME => $name,
                self::COL_SHOP_CHANNEL_UPDATED_BY => $adminId,
                self::COL_SHOP_CHANNEL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            ])
        ;
    }

    /**
     * =====================================
     * || function de Cin Nhan lam report ||
     * =====================================
     * search Shop Data
     * @param bool $isHaveOrganizationDefault
     * @param bool $isShopActive
     * @param string $channelCode
     * @return mixed
     */
    public static function searchShopData($isHaveOrganizationDefault = true, $isShopActive = false, $channelCode = '')
    {
        $model = DB::connection('master_business')->table('shop_channel')
            ->join('shop_master', 'shop_master.shop_master_id','=','shop_channel.fk_shop_master')
            ->join('channel', 'channel.channel_id','=','shop_channel.fk_channel')
            ->join('venture', 'venture.venture_id','=','channel.fk_venture')
            ->when( ! $isHaveOrganizationDefault, function ($query) {
                return $query->where('shop_master.fk_organization', '<>', 1);
            })
            ->when($isShopActive, function ($query) {
                return $query->where('shop_channel.fk_config_active', ConfigActive::ACTIVE)
                             ->where('shop_master.fk_config_active', ConfigActive::ACTIVE);
            })
            ->when($channelCode, function ($query) use ($channelCode) {
                return $query->where('channel.channel_code', $channelCode);
            })
            #->whereIn('shop_channel.shop_channel_id', [16,220])
            ->select(
                'shop_channel.shop_channel_id',
                'shop_channel.shop_channel_name',
                'shop_channel.shop_channel_created_at',
                DB::raw('if(shop_channel.fk_config_active and shop_master.fk_config_active, 1, 0) as shop_channel_is_active'),
                'shop_master.shop_master_id',
                'shop_master.shop_master_code',
                'channel.channel_id',
                'channel.channel_code',
                'venture.venture_id',
                'venture.venture_exchange',
                'venture.venture_timezone',
                'venture.venture_name'
            )
            ->orderBy('shop_channel.shop_channel_id')
        ;

        return $model->get();
    }

    /**
     * @return mixed
     */
    public static function getAllShopChannelIsTokopedia() {
        $select = DB::connection('master_business')->table('shop_channel')
            ->join('channel', 'shop_channel.fk_channel', '=', 'channel.channel_id')
            ->where('channel.channel_code', Channel::TOKOPEDIA_CODE)
            ->where('shop_channel.fk_config_active', 1)
            ->where('shop_channel.shop_channel_api_credential', 'not like', '{}')
        ;
        return $select->get();
    }
    /**
     * Get all active
     * @return \Illuminate\Support\Collection
     */
    public static function getAllShopActive_channelShopee()
    {
        $select = DB::table('shop_channel')
            ->join('channel', 'fk_channel', '=', 'channel_id')
            ->where('shop_channel.fk_config_active', 1)
            ->where('channel_code', 'SHOPEE')
            ->select([
                'fk_shop_master',
                'fk_channel',
                'shop_channel_id',
                'fk_venture',
            ])
        ;
        return $select->get();
    }

    /**
     * @param int $id
     * @return \stdClass|null
     */
    public static function find($id)
    {
        if ( ! isset(self::$_cache[$id])) {
            $select = DB::connection('master_business')->table('shop_channel')
                ->join('channel', 'shop_channel.fk_channel', '=', 'channel_id')
                ->join('venture', 'channel.fk_venture', '=', 'venture_id')
                ->where('shop_channel_id', $id)
                ->where('shop_channel.fk_config_active', 1)
                ->select([
                    'shop_channel.shop_channel_id',
                    'shop_channel.fk_shop_master',
                    'shop_channel.fk_channel',
                    'channel.channel_code',
                    'venture_timezone',
                    'venture_id',
                    'shop_channel_api_credential',
                ])
            ;

            $data = $select->get();
            if ($data->count()) {
                $result = $data->first();
            } else {
                $result = null;
            }

            self::$_cache[$id] = $result;
        }

        return self::$_cache[$id];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getAllShopActive_channelShopee_andOrganization()
    {
        $select = DB::table('shop_channel')
            ->join('channel', 'fk_channel', '=', 'channel_id')
            ->join('shop_master', 'fk_shop_master', '=', 'shop_master_id')
            ->where('shop_channel.fk_config_active', 1)
            ->where('shop_channel.shop_channel_api_credential', 'not like', '{}')
            ->where('channel_code', 'SHOPEE')
            ->select([
                'fk_shop_master',
                'fk_channel',
                'fk_organization'
            ])
        ;
        return $select->get();
    }
}