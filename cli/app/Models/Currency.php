<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 17/11/2019
 * Time: 18:59
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Currency extends ModelBusiness
{
    const TABLE_NAME = 'currency';

    const COL_CURRENCY_ID = 'currency_id';
    const COL_FK_VENTURE = 'fk_venture';
    const COL_CURRENCY_CODE = 'currency_code';
    const COL_CURRENCY_CREATED_AT = 'currency_created_at';
    const COL_CURRENCY_CREATED_BY = 'currency_created_by';

    /**
     * @const int
     */
    const VND = 1;

    /**
     * @const int
     */
    const PHP = 2;

    /**
     * @const int
     */
    const MYR = 3;

    /**
     * @const int
     */
    const SGD = 4;

    /**
     * @const int
     */
    const IDR = 5;

    /**
     * @const int
     */
    const USD = 6;

    public static function getAll()
    {
        return DB::table(self::TABLE_NAME)
            ->select(
                self::COL_CURRENCY_ID,
                self::COL_CURRENCY_CODE,
                self::COL_FK_VENTURE
            )
            ->get()
        ;
    }

}