<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 14:24
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Channel extends ModelBusiness
{

    const TABLE_NAME = 'channel';

    const COL_CHANNEL_ID = 'channel_id';
    const COL_CHANNEL_NAME = 'channel_name';
    const COL_CHANNEL_CODE = 'channel_code';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_CHANNEL_CREATED_AT = 'channel_created_at';
    const COL_CHANNEL_UPDATED_AT = 'channel_updated_at';
    const COL_CHANNEL_UPDATED_BY = 'channel_updated_by';
    const COL_FK_VENTURE = 'fk_venture';
    const COL_CHANNEL_CONFIGURATION_RRP = 'channel_configuration_rrp';
    const COL_CHANNEL_CONFIGURATION_SELLING_PRICE = 'channel_configuration_selling_price';
    const COL_CHANNEL_IS_OFFLINE = 'channel_is_offline';
    const COL_CHANNEL_REQUEST_PACKING = 'channel_request_packing';
    const COL_CHANNEL_CONFIG = 'channel_config';
    const COL_CHANNEL_DATE_TO_SHIP = 'channel_date_to_ship';
    const COL_CHANNEL_BUSINESS_DAY_FROM = 'channel_business_day_from';
    const COL_CHANNEL_BUSINESS_DAY_TO = 'channel_business_day_to';
    const COL_CHANNEL_URL_HANDLE_CALLBACK = 'channel_url_handle_callback';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_CHANNEL_ID;
    public $timestamps = false;

    /**
     * @var string
     */
    const LAZADA_CODE = 'LAZADA';

    /**
     * @var string
     */
    const TIKI_CODE = 'TIKI';

    /**
     * @var string
     */
    const SHOPEE_CODE = 'SHOPEE';

    /**
     * @var string
     */
    const SENDO_CODE = 'SENDO';

    /**
     * @var string
     */
    const TOKOPEDIA_CODE = 'TOKOPEDIA';

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * @return Collection
     */
    public static function getChannelTiKi()
    {
        $query = DB::connection('master_business')->table('channel')
            ->where('fk_config_active', 1)
            ->where('channel_code', self::TIKI_CODE);
        return $query->first();
    }
}