<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 09:53
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ProductShopChannel extends Library\ModelBusiness
{

    const TABLE_NAME = 'product_shop_channel';

    const COL_PRODUCT_SHOP_CHANNEL_ID = 'product_shop_channel_id';
    const COL_PRODUCT_SHOP_CHANNEL_GROUP_CODE = 'product_shop_channel_group_code';
    const COL_PRODUCT_SHOP_CHANNEL_SELLER_SKU = 'product_shop_channel_seller_sku';
    const COL_PRODUCT_SHOP_CHANNEL_SHOP_SKU = 'product_shop_channel_shop_sku';
    const COL_PRODUCT_SHOP_CHANNEL_URL = 'product_shop_channel_url';
    const COL_PRODUCT_SHOP_CHANNEL_NAME = 'product_shop_channel_name';
    const COL_PRODUCT_SHOP_CHANNEL_DESCRIPTION = 'product_shop_channel_description';
    const COL_PRODUCT_SHOP_CHANNEL_QUANTITY = 'product_shop_channel_quantity';
    const COL_PRODUCT_SHOP_CHANNEL_DATA = 'product_shop_channel_data';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';
    const COL_FK_PRODUCT = 'fk_product';
    const COL_FK_PRICE_ADJUSTMENT = 'fk_price_adjustment';
    const COL_PRODUCT_SHOP_CHANNEL_CREATED_AT = 'product_shop_channel_created_at';
    const COL_PRODUCT_SHOP_CHANNEL_UPDATED_AT = 'product_shop_channel_updated_at';
    const COL_PRODUCT_SHOP_CHANNEL_CREATED_BY = 'product_shop_channel_created_by';
    const COL_PRODUCT_SHOP_CHANNEL_UPDATED_BY = 'product_shop_channel_updated_by';
    const COL_FK_PROMOTION_BRAND_ADJUSTMENT = 'fk_promotion_brand_adjustment';
    const COL_FK_PROMOTION_CHANNEL_ADJUSTMENT = 'fk_promotion_channel_adjustment';
    const COL_FK_PROMOTION_SELLER_ADJUSTMENT = 'fk_promotion_seller_adjustment';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_PRODUCT_SHOP_CHANNEL_RRP_QUEUE_STATUS = 'product_shop_channel_rrp_queue_status';
    const COL_PRODUCT_SHOP_CHANNEL_POSTSUB_QUEUE_STATUS = 'product_shop_channel_postsub_queue_status';
    const COL_PRODUCT_SHOP_CHANNEL_STOCK = 'product_shop_channel_stock';
    const COL_PRODUCT_SHOP_CHANNEL_STOCK_QUEUE_STATUS = 'product_shop_channel_stock_queue_status';
    const COL_FK_CHANNEL_CATEGORY = 'fk_channel_category';
    const COL_PRODUCT_SHOP_CHANNEL_CURRENT_ONSITE_PRICE_RRP = 'product_shop_channel_current_onsite_price_rrp';
    const COL_PRODUCT_SHOP_CHANNEL_CURRENT_ONSITE_PRICE_POSTSUB = 'product_shop_channel_current_onsite_price_postsub';
    const COL_PRODUCT_SHOP_CHANNEL_STOCK_FBX = 'product_shop_channel_stock_fbx';
    const COL_PRODUCT_SHOP_CHANNEL_STOCK_FBX_COLLECTED_AT = 'product_shop_channel_stock_fbx_collected_at';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_RESPONSE_SYNC_RRP = 'product_shop_channel_last_response_sync_rrp';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_RESPONSE_SYNC_RRP_RESPONSE_AT = 'product_shop_channel_last_response_sync_rrp_response_at';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_RESPONSE_SYNC_POSTSUB = 'product_shop_channel_last_response_sync_postsub';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_RESPONSE_SYNC_POSTSUB_RESPONSE_AT = 'product_shop_channel_last_response_sync_postsub_response_at';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_RESPONSE_SYNC_STOCK = 'product_shop_channel_last_response_sync_stock';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_RESPONSE_SYNC_STOCK_RESPONSE_AT = 'product_shop_channel_last_response_sync_stock_response_at';
    const COL_PRODUCT_SHOP_CHANNEL_ALLOW_SYNC_STOCK = 'product_shop_channel_allow_sync_stock';
    const COL_PRODUCT_SHOP_CHANNEL_ALLOW_SYNC_STOCK_AT = 'product_shop_channel_allow_sync_stock_at';
    const COL_PRODUCT_SHOP_CHANNEL_ALLOW_SYNC_STOCK_BY = 'product_shop_channel_allow_sync_stock_by';
    const COL_PRODUCT_SHOP_CHANNEL_CHANNEL_RRP = 'product_shop_channel_channel_rrp';
    const COL_PRODUCT_SHOP_CHANNEL_CHANNEL_SELLING_PRICE = 'product_shop_channel_channel_selling_price';
    const COL_PRODUCT_SHOP_CHANNEL_CHANNEL_ALLOCATION_STOCK = 'product_shop_channel_channel_allocation_stock';
    const COL_PRODUCT_SHOP_CHANNEL_CHANNEL_RESERVATION = 'product_shop_channel_channel_reservation';
    const COL_PRODUCT_SHOP_CHANNEL_ALLOW_SYNC_PRICE = 'product_shop_channel_allow_sync_price';
    const COL_PRODUCT_SHOP_CHANNEL_ALLOW_SYNC_PRICE_AT = 'product_shop_channel_allow_sync_price_at';
    const COL_PRODUCT_SHOP_CHANNEL_ALLOW_SYNC_PRICE_BY = 'product_shop_channel_allow_sync_price_by';
    const COL_FK_BRAND_MASTER = 'fk_brand_master';
    const COL_PRODUCT_SHOP_CHANNEL_IS_VARIATION = 'product_shop_channel_is_variation';
    const COL_PRODUCT_SHOP_CHANNEL_IS_SYNC_TO_CHANNEL_TODAY = 'product_shop_channel_is_sync_to_channel_today';
    const COL_PRODUCT_SHOP_CHANNEL_PRICE_POSTSUB_CHANNEL = 'product_shop_channel_price_postsub_channel';
    const COL_PRODUCT_SHOP_CHANNEL_PRICE_COLLECTED_AT = 'product_shop_channel_price_collected_at';
    const COL_PRODUCT_SHOP_CHANNEL_PRICE_RRP_CHANNEL = 'product_shop_channel_price_rrp_channel';
    const COL_PRODUCT_SHOP_CHANNEL_PRICE_LAST_SYNC_PRICE_SYSTEM_AT = 'product_shop_channel_price_last_sync_price_system_at';
    const COL_PRODUCT_SHOP_CHANNEL_PRICE_LAST_SYNC_PRICE_AT = 'product_shop_channel_price_last_sync_price_at';
    const COL_PRODUCT_SHOP_CHANNEL_PRICE_LAST_SYNC_PRICE_BY = 'product_shop_channel_price_last_sync_price_by';
    const COL_PRODUCT_SHOP_CHANNEL_STOCK_TOTAL_SELLABLE = 'product_shop_channel_stock_total_sellable';
    const COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK = 'product_shop_channel_stock_allocation_stock';
    const COL_PRODUCT_SHOP_CHANNEL_STOCK_COLLECTED_AT = 'product_shop_channel_stock_collected_at';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_SYNC_STOCK_SYSTEM_AT = 'product_shop_channel_last_sync_stock_system_at';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_SYNC_STOCK_AT = 'product_shop_channel_last_sync_stock_at';
    const COL_PRODUCT_SHOP_CHANNEL_LAST_SYNC_STOCK_BY = 'product_shop_channel_last_sync_stock_by';
    const COL_PRODUCT_SHOP_CHANNEL_ITEM_ID = 'product_shop_channel_item_id';
    const COL_PRODUCT_SHOP_CHANNEL_PULLED_AT = 'product_shop_channel_pulled_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_PRODUCT_SHOP_CHANNEL_ID;
    public $timestamps = false;

    /**
     * import data
     * @param $sku
     * @param $shopChannelId
     * @param $name
     * @param $allocationStock
     * @param $fbxStock
     * @param $rrp
     * @param $sellingPrice
     * @param $channelIdentify
     * @param $category
     * @param $updatedAt
     * @param $updatedBy
     * @param $createdAt
     * @param $createdBy
     * @param $rawData
     * @param $shopSku
     * @return mixed
     */
    public static function import($sku, $shopChannelId, $name, $allocationStock, $fbxStock, $rrp, $sellingPrice,
                                  $channelIdentify, $category, $updatedAt, $updatedBy, $createdAt, $createdBy, $rawData, $shopSku)
    {

        return DB::table(self::TABLE_NAME)
            ->updateOrInsert(
                [
                    self::COL_FK_SHOP_CHANNEL => $shopChannelId,
                    self::COL_PRODUCT_SHOP_CHANNEL_SELLER_SKU => $sku,
                ],
                [
                    self::COL_PRODUCT_SHOP_CHANNEL_NAME => $name,
                    self::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK => $allocationStock,
                    self::COL_PRODUCT_SHOP_CHANNEL_STOCK_FBX => $fbxStock,
                    self::COL_PRODUCT_SHOP_CHANNEL_STOCK_FBX_COLLECTED_AT => Library\Common::getCurrentTimestamp(),
                    self::COL_PRODUCT_SHOP_CHANNEL_PRICE_RRP_CHANNEL => $rrp,
                    self::COL_PRODUCT_SHOP_CHANNEL_PRICE_POSTSUB_CHANNEL => $sellingPrice,
                    self::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID => $channelIdentify,
                    self::COL_FK_CHANNEL_CATEGORY => $category,
                    self::COL_PRODUCT_SHOP_CHANNEL_CREATED_AT => $createdAt,
                    self::COL_PRODUCT_SHOP_CHANNEL_CREATED_BY => $createdBy,
                    self::COL_PRODUCT_SHOP_CHANNEL_UPDATED_AT => $updatedAt,
                    self::COL_PRODUCT_SHOP_CHANNEL_UPDATED_BY => $updatedBy,
                    self::COL_PRODUCT_SHOP_CHANNEL_PULLED_AT => Library\Common::getCurrentTimestamp(),
                    self::COL_PRODUCT_SHOP_CHANNEL_DATA => $rawData,
                    self::COL_PRODUCT_SHOP_CHANNEL_SHOP_SKU => $shopSku,
                ]
            )
        ;
    }

    /**
     * update Category By Id
     * @param int $id
     * @param int $category
     * @param int $adminId
     * @return mixed
     */
    public static function updateCategoryById($id, $category, $adminId) {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_PRODUCT_SHOP_CHANNEL_ID, $id)
            ->update([
                self::COL_FK_CHANNEL_CATEGORY => $category,
                self::COL_PRODUCT_SHOP_CHANNEL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
                self::COL_PRODUCT_SHOP_CHANNEL_UPDATED_BY => $adminId,
            ])
        ;
    }

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * search By Shop Channel Id
     * @param int $shopChannelId
     * @return mixed
     */
    public static function searchByShopChannelId($shopChannelId)
    {
        return self::where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->select(
                self::COL_PRODUCT_SHOP_CHANNEL_ID,
                self::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID
            )
            ->get()
        ;
    }

    /**
     * search By Channel Category Is Null
     * @param int $shopChannelId
     * @param int $limit
     * @return mixed
     */
    public static function searchByChannelCategoryIsNull($shopChannelId, $limit)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->whereNull(self::COL_FK_CHANNEL_CATEGORY)
            ->select(
                self::COL_PRODUCT_SHOP_CHANNEL_ID,
                self::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID
            )
            ->orderBy(self::COL_PRODUCT_SHOP_CHANNEL_ID)
            ->limit($limit)
            ->get()
        ;
    }

    /**
     * search By Shop Channel Id And State
     * @param $shopChannelId
     * @param $state
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndState($shopChannelId, $state)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                ProductAds::TABLE_NAME,
                ProductAds::TABLE_NAME.'.'.ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL,
                '=',
                self::TABLE_NAME.'.'.self::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->where(self::TABLE_NAME.'.'.self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(ProductAds::TABLE_NAME.'.'.ProductAds::COL_PRODUCT_ADS_STATE, $state)
            ->select(
                self::TABLE_NAME.'.'.self::COL_PRODUCT_SHOP_CHANNEL_ID,
                self::TABLE_NAME.'.'.self::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK,
                self::TABLE_NAME.'.'.self::COL_PRODUCT_SHOP_CHANNEL_PRICE_POSTSUB_CHANNEL,
                self::TABLE_NAME.'.'.self::COL_PRODUCT_SHOP_CHANNEL_PRICE_RRP_CHANNEL
            )
            ->distinct()
            ->get()
        ;
    }

    /**
     * get By Shop Channel Id And Seller Sku
     * @param $shopChannelId
     * @param $sellerSku
     * @return mixed
     */
    public static function getByShopChannelIdAndSellerSku($shopChannelId, $sellerSku)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(self::COL_PRODUCT_SHOP_CHANNEL_SELLER_SKU, $sellerSku)
            ->select(self::COL_PRODUCT_SHOP_CHANNEL_ID)
            ->first()
        ;
    }

    /**
     * sum Performance By Shop Channel Id And Date From To
     * @param int $shopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function sumPerformanceByShopChannelIdAndDateFromTo($shopChannelId, $dateFrom, $dateTo)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL,
                '=',
                self::TABLE_NAME.'.'.self::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->join(
                MakPerformance::TABLE_NAME,
                MakPerformance::TABLE_NAME.'.'.MakPerformance::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            )
            ->where(self::TABLE_NAME.'.'.self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->whereBetween(MakPerformance::TABLE_NAME.'.'.MakPerformance::COL_MAK_PERFORMANCE_DATE, [$dateFrom, $dateTo])
            ->where(MakPerformance::TABLE_NAME.'.'.MakPerformance::COL_MAK_PERFORMANCE_VIEW, '>', 0)
            ->select(
                self::TABLE_NAME.'.'.self::COL_PRODUCT_SHOP_CHANNEL_ID,
                DB::raw('sum('.MakPerformance::TABLE_NAME.'.'.MakPerformance::COL_MAK_PERFORMANCE_VIEW.') as '.MakPerformance::COL_MAK_PERFORMANCE_VIEW),
                DB::raw('sum('.MakPerformance::TABLE_NAME.'.'.MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD.') as '.MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD)
            )
            ->groupBy(
                self::TABLE_NAME.'.'.self::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->get()
        ;
    }

    /**
     * count Run Ads By Campaign Id
     * @param int $campaignId
     * @param int $timeStart
     * @param int $timeEnd
     * @return int
     */
    public static function countRunAdsByCampaignId($campaignId, $timeStart, $timeEnd)
    {
        $sql = "
            select count(distinct fk_product_shop_channel)
            from (select mak_programmatic_log_sku.mak_programmatic_log_sku_id,
            mak_programmatic_log_sku.fk_product_shop_channel,
            mak_programmatic_log_sku.mak_programmatic_log_sku_created_at,
            base.to
            from mak_programmatic_log_sku
            left join (select mak_programmatic_log_sku.mak_programmatic_log_sku_id,
            mak_programmatic_log_sku.fk_product_shop_channel,
            mak_programmatic_log_sku.mak_programmatic_log_sku_created_at `from`,
            min(child.mak_programmatic_log_sku_created_at) `to`
            from mak_programmatic_log_sku
            join mak_programmatic_log_sku child USE INDEX ()
            on child.fk_product_shop_channel = mak_programmatic_log_sku.fk_product_shop_channel
            and mak_programmatic_log_sku.fk_mak_programmatic_campaign =
            child.fk_mak_programmatic_campaign
            and mak_programmatic_log_sku.mak_programmatic_log_sku_created_at <=
            child.mak_programmatic_log_sku_created_at
            where mak_programmatic_log_sku.fk_mak_programmatic_campaign = {$campaignId}
            and child.fk_mak_programmatic_action = 5
            and mak_programmatic_log_sku.fk_mak_programmatic_action in (6, 8)
            and mak_programmatic_log_sku.mak_programmatic_log_sku_created_at <= {$timeEnd}
            group by mak_programmatic_log_sku.mak_programmatic_log_sku_id order by null ) base
            on base.mak_programmatic_log_sku_id = mak_programmatic_log_sku.mak_programmatic_log_sku_id
            where mak_programmatic_log_sku.fk_mak_programmatic_campaign = {$campaignId}
            and mak_programmatic_log_sku.fk_mak_programmatic_action in (6, 8)
            and mak_programmatic_log_sku.mak_programmatic_log_sku_created_at <= {$timeEnd}
            having `to` >= {$timeStart}
            or `to` is null) bb;
        ";

        return DB::select($sql);
    }

    /**
     * @param array $arrayShopChannelId
     * @return \Illuminate\Support\Collection
     */
    public static function getWithProductAds($arrayShopChannelId)
    {
        $arrayShopChannelId = array_map('intval', $arrayShopChannelId);

        $select = DB::connection('master_business')->table('product_shop_channel')
            ->join('product_ads', 'fk_product_shop_channel', '=', 'product_shop_channel_id')
            ->whereIn('product_shop_channel.fk_shop_channel', $arrayShopChannelId)
            ->select([
                'product_shop_channel_seller_sku',
                'product_shop_channel_id',
                'product_shop_channel.fk_shop_channel',
            ])
        ;

        return $select->get();
    }
}