<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 07/10/2019
 * Time: 14:09
 */

namespace App\Models;


trait Master
{
    /**
     * @var string
     */
    protected $tableUpdated = '';

    /**
     * @var string
     */
    protected $columnUpdated = '';

    /**
     * @var string
     */
    protected $columnCondition = '';

    /**
     * @var array
     */
    protected $caseValue = [];

    /**
     * @return string
     */
    public function getTableUpdated()
    {
        return $this->tableUpdated;
    }

    /**
     * @param string $tableUpdated
     */
    public function setTableUpdated($tableUpdated)
    {
        $this->tableUpdated = $tableUpdated;
    }

    /**
     * @return string
     */
    public function getColumnUpdated()
    {
        return $this->columnUpdated;
    }

    /**
     * @param string $columnUpdated
     */
    public function setColumnUpdated($columnUpdated)
    {
        $this->columnUpdated = $columnUpdated;
    }

    /**
     * @return string
     */
    public function getColumnCondition()
    {
        return $this->columnCondition;
    }

    /**
     * @param string $columnCondition
     */
    public function setColumnCondition($columnCondition)
    {
        $this->columnCondition = $columnCondition;
    }

    /**
     * @param mixed $case
     * @param mixed $value
     */
    public function addCase($case, $value)
    {
        $this->caseValue[$case] = $value;
    }

    /**
     * assemble
     * @return string
     */
    public function assemble()
    {
        $whenArray = [];
        foreach ($this->caseValue as $case => $value) {
            if ($value === null) {
                $value = 'null';
            }
            $whenArray[] = sprintf(
                'when %s then %s',
                is_string($case) ? '"'.$case.'"' : $case,
                is_string($value) ? '"'.$value.'"' : $value
            );
        }

        $sql = sprintf(
            'update %s set %s = case %s %s end where %s in (%s)',
            $this->getTableUpdated(),
            $this->getColumnUpdated(),
            $this->getColumnCondition(),
            implode(' ', $whenArray),
            $this->getColumnCondition(),
            implode(', ', array_keys($this->caseValue))
        );

        return $sql;
    }

}