<?php


namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;

class ShopeeWalletBalance extends ModelBusiness
{
    /**
     * @param $rows
     * @return bool
     */
    public static function insertMulti($rows)
    {
        return DB::table('shopee_wallet_balance')->insert($rows);
    }

    public static function saveWallet($shopChannelId, $value)
    {
        $time = strtotime('now');
        $message = '';
        try {
            DB::beginTransaction();

            DB::table('shopee_wallet_balance')->insert([
                'shop_channel_id' => $shopChannelId,
                'value' => $value,
                'collected_at' => $time
            ]);
            $data = DB::table('shop_channel_balance')
                ->where('fk_shop_channel', $shopChannelId)
                ->get()
                ->first();
            if (!$data) {
                DB::table('shop_channel_balance')->insert([
                    'shop_channel_balance_value' => $value,
                    'fk_shop_channel' => $shopChannelId,
                ]);
            } else {
                $shopChannelBalanceId = $data->shop_channel_balance_id;
                DB::table('shop_channel_balance')->where('shop_channel_balance_id', $shopChannelBalanceId)
                    ->update([
                        'shop_channel_balance_value' => $value,
                        'fk_shop_channel' => $shopChannelId
                    ]);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            $message = $exception->getMessage();
        }

        return $message;
    }
}