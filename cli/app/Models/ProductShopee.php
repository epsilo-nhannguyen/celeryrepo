<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 13:57
 */

namespace App\Models;


use App\Library\Model\Sql\Manager;
use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ProductShopee extends ModelBusiness
{
    const TABLE_NAME = 'product_shopee';

    const COL_PRODUCT_SHOPEE_ID = 'product_shopee_id';
    const COL_FK_PRODUCT_SHOP_CHANNEL = 'fk_product_shop_channel';
    const COL_PRODUCT_SHOPEE_STATUS = 'product_shopee_status';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_PRODUCT_SHOPEE_ID;
    public $timestamps = false;

    /**
     * string
     */
    const STATUS_BANNED = 'BANNED';

    /**
     * string
     */
    const STATUS_UNLIST = 'UNLIST';

    /**
     * string
     */
    const STATUS_NORMAL = 'NORMAL';

    /**
     * string
     */
    const STATUS_DELETED = 'DELETED';

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * import data
     * @param int $productShopChannelId
     * @param string $status
     * @return mixed
     */
    public static function import($productShopChannelId, $status)
    {
        return DB::table(self::TABLE_NAME)
            ->updateOrInsert(
                [
                    self::COL_FK_PRODUCT_SHOP_CHANNEL => $productShopChannelId,
                ],
                [
                    self::COL_PRODUCT_SHOPEE_STATUS => $status,
                ]
            )
        ;
    }

    public static function getAllProductByShopchannelId($shopChannelId, $DBConnect = null)
    {
        if(!$DBConnect){
            $DBConnect = env('DB_CONNECTION',Manager::SQL_CHART_CONNECT);
        }
        return DB::connection($DBConnect)
            ->table(self::TABLE_NAME)
            ->join(ProductShopChannel::TABLE_NAME, ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,self::COL_FK_PRODUCT_SHOP_CHANNEL)
            ->where(self::COL_PRODUCT_SHOPEE_STATUS,self::STATUS_BANNED)
            ->where(ProductShopChannel::COL_FK_SHOP_CHANNEL,$shopChannelId)
            ->get();
    }

}