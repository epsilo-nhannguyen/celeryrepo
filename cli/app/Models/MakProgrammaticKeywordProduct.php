<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 18:02
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakProgrammaticKeywordProduct extends Library\ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_keyword_product';

    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID = 'mak_programmatic_keyword_product_id';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD = 'fk_mak_programmatic_keyword';
    const COL_FK_PRODUCT_SHOP_CHANNEL = 'fk_product_shop_channel';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD_TYPE = 'fk_mak_programmatic_keyword_type';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_AT = 'mak_programmatic_keyword_product_created_at';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_BY = 'mak_programmatic_keyword_product_created_by';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_UPDATED_AT = 'mak_programmatic_keyword_product_updated_at';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_UPDATED_BY = 'mak_programmatic_keyword_product_updated_by';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_BIDDING_PRICE = 'mak_programmatic_keyword_product_bidding_price';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_MATCH_TYPE = 'mak_programmatic_keyword_product_match_type';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS = 'mak_programmatic_keyword_product_status';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CURRENT_RANK = 'mak_programmatic_keyword_product_current_rank';
    const COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CURRENT_RANK_UPDATED_AT = 'mak_programmatic_keyword_product_current_rank_updated_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID;
    public $timestamps = false;

    /**
     * @var string
     */
    const MATCH_TYPE_EXPANSION = 'expansion';

    /**
     * @var string
     */
    const MATCH_TYPE_DEFINED = 'defined';

    /**
     * @var string
     */
    const STATUS_RESTORE = 'restore';

    /**
     * @var string
     */
    const STATUS_DELETED = 'deleted';

    use Master;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * import data
     * @param array $filter
     * @param array $data
     * @return mixed
     */
    public static function import($filter, $data)
    {
        return DB::table(self::TABLE_NAME)
            ->updateOrInsert(
                $filter,
                $data
            );
    }

    /**
     * insert Get Id
     * @param int $keywordId
     * @param int $productShopChannelId
     * @param float $biddingPrice
     * @param string $matchType
     * @param string $status
     * @param int $adminId
     * @return mixed
     */
    public static function insertGetId($keywordId, $productShopChannelId, $biddingPrice, $matchType, $status, $adminId)
    {
        return DB::table(self::TABLE_NAME)->insertGetId(
            [
                self::COL_FK_MAK_PROGRAMMATIC_KEYWORD => $keywordId,
                self::COL_FK_PRODUCT_SHOP_CHANNEL => $productShopChannelId,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_BIDDING_PRICE => $biddingPrice,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_MATCH_TYPE => $matchType,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS => $status,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_BY => $adminId,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CREATED_AT => Library\Common::getCurrentTimestamp()
            ]
        );
    }

    /**
     * update Status By Id Array
     * @param array $idArray
     * @param string $status
     * @param int $adminId
     * @return mixed
     */
    public static function updateStatusByIdArray($idArray, $status, $adminId)
    {
        return DB::table(self::TABLE_NAME)
            ->whereIn(self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID, $idArray)
            ->update([
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS => $status,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_UPDATED_BY => $adminId,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ]);
    }

    /**
     * update Bidding Price By Id
     * @param int $keywordProductId
     * @param float $biddingPrice
     * @param int $adminId
     * @return mixed
     */
    public static function updateBiddingPriceById($keywordProductId, $biddingPrice, $adminId)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID, $keywordProductId)
            ->update([
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_BIDDING_PRICE => $biddingPrice,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_UPDATED_BY => $adminId,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ]);
    }

    /**
     * update Case
     * @param string $sql
     * @return mixed
     */
    public static function updateCase($sql)
    {
        return DB::statement($sql);
    }

    /**
     * update Current Rank By Id Array
     * @param array $idArray
     * @return mixed
     */
    public static function updateCurrentRankByIdArray($idArray)
    {
        return DB::table(self::TABLE_NAME)
            ->whereIn(self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID, $idArray)
            ->update([
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_CURRENT_RANK_UPDATED_AT => Library\Common::getCurrentTimestamp()
            ]);
    }

    /**
     * get By Keyword Id And Product Id
     * @param int $keywordId
     * @param int $productShopChannelId
     * @return mixed
     */
    public static function getByKeywordIdAndProductId($keywordId, $productShopChannelId)
    {
        return self::where(self::COL_FK_MAK_PROGRAMMATIC_KEYWORD, $keywordId)
            ->where(self::COL_FK_PRODUCT_SHOP_CHANNEL, $productShopChannelId)
            ->select(self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID)
            ->first();
    }

    /**
     * search By Product Shop Channel Id
     * @param int $productShopChannelId
     * @param string $status
     * @return mixed
     */
    public static function searchByProductShopChannelId($productShopChannelId, $status = '')
    {
        return self::where(self::COL_FK_PRODUCT_SHOP_CHANNEL, $productShopChannelId)
            ->when($status, function ($query) use ($status) {
                return $query->where(self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS, $status);
            })
            ->select(
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS
            )
            ->get();
    }

    /**
     * search By Product Id Array
     * @param $productIdArray
     * @return array
     */
    public static function searchByProductIdArray($productIdArray)
    {
        if (!$productIdArray) return [];

        return DB::table(self::TABLE_NAME)
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->join(
                MakProgrammaticKeyword::TABLE_NAME,
                MakProgrammaticKeyword::TABLE_NAME . '.' . MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_MAK_PROGRAMMATIC_KEYWORD
            )
            ->whereIn(self::TABLE_NAME . '.' . self::COL_FK_PRODUCT_SHOP_CHANNEL, $productIdArray)
            ->select(
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID,
                MakProgrammaticKeyword::TABLE_NAME . '.' . MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME
            )
            ->get();
    }

    /**
     * search By Shop Channel Id
     * @param int $shopChannelId
     * @return mixed
     */
    public static function searchByShopChannelId($shopChannelId)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->join(
                MakProgrammaticKeyword::TABLE_NAME,
                MakProgrammaticKeyword::TABLE_NAME . '.' . MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_MAK_PROGRAMMATIC_KEYWORD
            )
            ->where(ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->select(
                self::TABLE_NAME . '.' . self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID,
                MakProgrammaticKeyword::TABLE_NAME . '.' . MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID,
                MakProgrammaticKeyword::TABLE_NAME . '.' . MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME
            )
            ->get();
    }

    /**
     * search Performance By Shop Channel Id And Date
     * @param int $shopChannelId
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public static function searchPerformanceByShopChannelIdAndDate($shopChannelId, $date)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->join(
                ProductAds::TABLE_NAME,
                ProductAds::TABLE_NAME . '.' . ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL,
                '=',
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->join(
                MakPerformance::TABLE_NAME,
                MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT,
                '=',
                self::TABLE_NAME . '.' . self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            )
            ->where(ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_DATE, $date)
            ->where(MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_VIEW, '>', 0)
            ->select(
                self::TABLE_NAME . '.' . self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                self::TABLE_NAME . '.' . self::COL_FK_MAK_PROGRAMMATIC_KEYWORD,
                self::TABLE_NAME . '.' . self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS,

                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_PRICE_POSTSUB_CHANNEL,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_PRICE_RRP_CHANNEL,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_STOCK_ALLOCATION_STOCK,

                ProductAds::TABLE_NAME . '.' . ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL,
                ProductAds::TABLE_NAME . '.' . ProductAds::COL_PRODUCT_ADS_STATE,

                MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD,
                MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_AVG_POSITION,
                MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_VIEW,
                MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_CLICKS,
                MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_CLICK_RATE
            )
            ->distinct()
            ->get();
    }

    /**
     * sum Performance By Shop Channel Id And Date From To
     * @param int $shopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function sumPerformanceByShopChannelIdAndDateFromTo($shopChannelId, $dateFrom, $dateTo)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->join(
                MakPerformance::TABLE_NAME,
                MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT,
                '=',
                self::TABLE_NAME . '.' . self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            )
            ->where(ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->whereBetween(MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_DATE, [$dateFrom, $dateTo])
            ->where(MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_VIEW, '>', 0)
            ->select(
                self::TABLE_NAME . '.' . self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                self::TABLE_NAME . '.' . self::COL_FK_PRODUCT_SHOP_CHANNEL,
                DB::raw('sum(' . MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_VIEW . ') as ' . MakPerformance::COL_MAK_PERFORMANCE_VIEW),
                DB::raw('sum(' . MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_CLICKS . ') as ' . MakPerformance::COL_MAK_PERFORMANCE_CLICKS),
                DB::raw('sum(' . MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD . ') as ' . MakPerformance::COL_MAK_PERFORMANCE_PRODUCT_SOLD),
                DB::raw('sum(' . MakPerformance::TABLE_NAME . '.' . MakPerformance::COL_MAK_PERFORMANCE_CLICK_RATE . ') as ' . MakPerformance::COL_MAK_PERFORMANCE_CLICK_RATE)
            )
            ->groupBy(
                self::TABLE_NAME . '.' . self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            )
            ->get();
    }

    /**
     * search By Campaign Id And State
     * @param int $campaignId
     * @param string $status
     * @return \Illuminate\Support\Collection
     */
    public static function searchByCampaignIdAndState($campaignId, $status)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->join(
                ProductShopChannelCampaign::TABLE_NAME,
                ProductShopChannelCampaign::TABLE_NAME . '.' . ProductShopChannelCampaign::COL_FK_PRODUCT_SHOP_CHANNEL,
                '=',
                ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->where(ProductShopChannelCampaign::TABLE_NAME . '.' . ProductShopChannelCampaign::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN, $campaignId)
            ->where(self::TABLE_NAME . '.' . self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS, $status)
            ->select(
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS
            )
            ->get();
    }

    /**
     * count Run Ads By Campaign Id
     * @param int $campaignId
     * @param int $timeStart
     * @param int $timeEnd
     * @return int
     */
    public static function countRunAdsByCampaignId($campaignId, $timeStart, $timeEnd)
    {
        $sql = "
            select count(distinct fk_mak_programmatic_keyword_product)
            from (select mak_programmatic_log_keyword.mak_programmatic_log_keyword_id,
            mak_programmatic_log_keyword.fk_mak_programmatic_keyword_product,
            mak_programmatic_log_keyword.mak_programmatic_log_keyword_created_at,
            base.to
            from mak_programmatic_log_keyword
            left join (select mak_programmatic_log_keyword.mak_programmatic_log_keyword_id,
            mak_programmatic_log_keyword.fk_mak_programmatic_keyword_product,
            mak_programmatic_log_keyword.mak_programmatic_log_keyword_created_at `from`,
            min(child.mak_programmatic_log_keyword_created_at) `to`
            from mak_programmatic_log_keyword
            join mak_programmatic_log_keyword child USE INDEX ()
            on child.fk_mak_programmatic_keyword_product = mak_programmatic_log_keyword.fk_mak_programmatic_keyword_product
            and mak_programmatic_log_keyword.fk_mak_programmatic_campaign =
            child.fk_mak_programmatic_campaign
            and mak_programmatic_log_keyword.mak_programmatic_log_keyword_created_at <=
            child.mak_programmatic_log_keyword_created_at
            where mak_programmatic_log_keyword.fk_mak_programmatic_campaign = {$campaignId}
            and child.fk_mak_programmatic_action = 1
            and mak_programmatic_log_keyword.fk_mak_programmatic_action in (4, 2)
            and mak_programmatic_log_keyword.mak_programmatic_log_keyword_created_at <= {$timeEnd}
            group by mak_programmatic_log_keyword.mak_programmatic_log_keyword_id order by null ) base
            on base.mak_programmatic_log_keyword_id = mak_programmatic_log_keyword.mak_programmatic_log_keyword_id
            where mak_programmatic_log_keyword.fk_mak_programmatic_campaign = {$campaignId}
            and mak_programmatic_log_keyword.fk_mak_programmatic_action in (4, 2)
            and mak_programmatic_log_keyword.mak_programmatic_log_keyword_created_at <= {$timeEnd}
            having `to` >= {$timeStart}
            or `to` is null) bb;
        ";

        return DB::select($sql);
    }

    /**
     * count Run Ads By Campaign Id
     * @param int $keywordId
     * @param int $productId
     * @return int
     */
    public static function getIdByKP($keywordId, $productId)
    {
        return self::where(self::COL_FK_PRODUCT_SHOP_CHANNEL, $productId)
            ->where(self::COL_FK_MAK_PROGRAMMATIC_KEYWORD, $keywordId)
            ->select(
                self::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            )
            ->first();
    }

    public static function getDataSK($shopChannelId, $DBConnect = null)
    {
        if(!$DBConnect){
            $DBConnect = env('DB_CONNECTION',Library\Model\Sql\Manager::SQL_CHART_CONNECT);
        }
        $shopChannelId = intval($shopChannelId);
        $dataSkuId = ProductShopChannelCampaign::getProductByShopchannel($shopChannelId, $DBConnect);
        $dataSkuInCampaign = [];
        foreach ($dataSkuId as $product) {
            array_push($dataSkuInCampaign, $product->{ProductShopChannelCampaign::COL_FK_PRODUCT_SHOP_CHANNEL});
        }
        // get product Banned
        $dataProductBanned = ProductShopee::getAllProductByShopchannelId($shopChannelId, $DBConnect);
        $dataBanned = [];
        foreach ($dataProductBanned as $banned) {
            array_push($dataBanned, $banned->{ProductShopee::COL_FK_PRODUCT_SHOP_CHANNEL});
        }
        $dataNotIn = array_merge($dataBanned, $dataSkuInCampaign);
        $stateNotin = [
            'ongoing',
            'scheduled'
        ];
        return DB::connection($DBConnect)
            ->table(self::TABLE_NAME)
            ->leftjoin(MakProgrammaticKeyword::TABLE_NAME, MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID, '=', self::COL_FK_MAK_PROGRAMMATIC_KEYWORD)
            ->leftjoin(ProductShopChannel::TABLE_NAME, ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID, '=', self::TABLE_NAME . '.' . self::COL_FK_PRODUCT_SHOP_CHANNEL)
            ->leftjoin(ProductAds::TABLE_NAME, ProductAds::TABLE_NAME . '.' . ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL, '=', ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID)
            ->whereNotIn(ProductAds::COL_PRODUCT_ADS_STATE, $stateNotin)
            ->whereNotIn(ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID, $dataNotIn)
            ->where(MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_STATUS, 'deleted')
            ->where(ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(ProductShopChannel::TABLE_NAME . '.' . ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ALLOW_SYNC_STOCK, '>', 0)
            ->get();

    }

    /**
     * @param array $arrayProductId
     * @return \Illuminate\Support\Collection
     */
    public static function getByArrayProductId($arrayProductId)
    {
        $select = DB::connection('master_business')->table('mak_programmatic_keyword_product')
            ->join('product_shop_channel', 'fk_product_shop_channel', '=', 'product_shop_channel_id')
            ->join('mak_programmatic_keyword', 'fk_mak_programmatic_keyword', '=', 'mak_programmatic_keyword_id')
            ->whereIn('fk_product_shop_channel', $arrayProductId)
            ->select([
                'mak_programmatic_keyword_product_id',
                'mak_programmatic_keyword_name',
                'product_shop_channel_seller_sku'
            ])
        ;

        return $select->get();
    }
}