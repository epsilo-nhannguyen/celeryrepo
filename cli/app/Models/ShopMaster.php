<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 14:28
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Library;


class ShopMaster extends Library\ModelBusiness
{
    const TABLE_NAME = 'shop_master';

    const COL_SHOP_MASTER_ID = 'shop_master_id';
    const COL_SHOP_MASTER_NAME = 'shop_master_name';
    const COL_SHOP_MASTER_STATUS = 'shop_master_status';
    const COL_SHOP_MASTER_CREATED_AT = 'shop_master_created_at';
    const COL_SHOP_MASTER_UPDATED_AT = 'shop_master_updated_at';
    const COL_SHOP_MASTER_UPDATED_BY = 'shop_master_updated_by';
    const COL_FK_ORGANIZATION = 'fk_organization';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_SHOP_MASTER_CREATED_BY = 'shop_master_created_by';
    const COL_SHOP_MASTER_IN_QUOTA = 'shop_master_in_quota';
    const COL_SHOP_MASTER_CODE = 'shop_master_code';
    const COL_FK_VENTURE = 'fk_venture';
    const COL_SHOP_MASTER_REFERENCE_WAREHOUSE = 'shop_master_reference_warehouse';
    const COL_SHOP_MASTER_ALLOW_SYNC_STOCK = 'shop_master_allow_sync_stock';
    const COL_SHOP_MASTER_ALLOW_SYNC_STOCK_AT = 'shop_master_allow_sync_stock_at';
    const COL_SHOP_MASTER_ALLOW_SYNC_STOCK_BY = 'shop_master_allow_sync_stock_by';
    const COL_SHOP_MASTER_ALLOW_SYNC_PRICE = 'shop_master_allow_sync_price';
    const COL_SHOP_MASTER_ALLOW_SYNC_PRICE_AT = 'shop_master_allow_sync_price_at';
    const COL_SHOP_MASTER_ALLOW_SYNC_PRICE_BY = 'shop_master_allow_sync_price_by';
    const COL_SHOP_MASTER_RESPONSIBLE = 'shop_master_responsible';
    const COL_SHOP_MASTER_REPRESENTATIVE = 'shop_master_representative';
    const COL_SHOP_MASTER_CONFIG = 'shop_master_config';
    const COL_FK_ADMIN_PRODUCT = 'fk_admin_product';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_SHOP_MASTER_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return ShopMaster[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * update By Id
     * @param int $id
     * @param string $name
     * @param string $code
     * @param int $adminId
     * @return mixed
     */
    public static function updateById($id, $name, $code, $adminId) {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_MASTER_ID, $id)
            ->update([
                self::COL_SHOP_MASTER_NAME => $name,
                self::COL_SHOP_MASTER_CODE => $code,
                self::COL_SHOP_MASTER_UPDATED_BY => $adminId,
                self::COL_SHOP_MASTER_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            ])
        ;
    }

    /**
     * change Name
     * @param string $name
     * @param string $code
     * @param int $adminId
     * @param int $shopId
     * @param int $channelId
     * @return mixed
     */
    public static function changeName($name, $code, $adminId, $shopId, $channelId = null)
    {
        $isSuccess = self::updateById($shopId, $name, $code, $adminId);

        if ($channelId) {
            $isSuccess = ShopChannel::updateByShopIdChannelId($shopId, $channelId, $name, $adminId);
        }

        return $isSuccess;
    }
}