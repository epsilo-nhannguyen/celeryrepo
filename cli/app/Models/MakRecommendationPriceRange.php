<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MakRecommendationPriceRange extends Model
{
    const PERCENT_90 = 'percent_90';
    const PERCENT_75 = 'percent_75';
    const PERCENT_60 = 'percent_60';

    const TABLE_NAME = 'mak_recommendation_price_range';

    const COL_MAK_RECOMMENDATION_PRICE_RANGE_ID = 'mak_recommendation_price_range_id';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT = 'fk_mak_programmatic_keyword_product';
    const COL_MAK_RECOMMENDATION_PRICE_RANGE_JSON = 'mak_recommendation_price_range_json';
    const COL_MAK_RECOMMENDATION_PRICE_RANGE_BIDDING = 'mak_recommendation_price_range_bidding';
    const COL_MAK_RECOMMENDATION_PRICE_RANGE_DATE = 'mak_recommendation_price_range_date';
    const COL_MAK_RECOMMENDATION_PRICE_RANGE_CREATED_AT = 'mak_recommendation_price_range_created_at';
    const COL_MAK_RECOMMENDATION_PRICE_RANGE_UPDATE_AT = 'mak_recommendation_price_range_updated_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_RECOMMENDATION_PRICE_RANGE_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }


    public static function getByKeywordProduct($shopChannelId,$kwpId)
    {
        return self::where(self::COL_FK_SHOP_CHANNEL,$shopChannelId)
            ->where(self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT,$kwpId)
            ->orderBy(self::COL_MAK_RECOMMENDATION_PRICE_RANGE_CREATED_AT,'DESC')
            ->first();
    }
}