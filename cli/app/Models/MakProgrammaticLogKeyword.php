<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:58
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakProgrammaticLogKeyword extends Library\ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_log_keyword';

    const COL_MAK_PROGRAMMATIC_LOG_KEYWORD_ID = 'mak_programmatic_log_keyword_id';
    const COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CHECKPOINT = 'mak_programmatic_log_keyword_checkpoint';
    const COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_AT = 'mak_programmatic_log_keyword_created_at';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT = 'fk_mak_programmatic_keyword_product';
    const COL_FK_MAK_PROGRAMMATIC_ACTION = 'fk_mak_programmatic_action';
    const COL_FK_MAK_PROGRAMMATIC_RULE = 'fk_mak_programmatic_rule';
    const COL_FK_MAK_PROGRAMMATIC_CAMPAIGN = 'fk_mak_programmatic_campaign';
    const COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_BY = 'mak_programmatic_log_keyword_created_by';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * build Data Insert
     * @param int $keywordProductId
     * @param int $actionId
     * @param int $ruleId
     * @param string $checkpoint
     * @param int $campaignId
     * @param int $adminId
     * @return array
     */
    public static function buildDataInsert($keywordProductId, $actionId, $ruleId, $checkpoint, $campaignId, $adminId)
    {
        return [
            self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT => $keywordProductId,
            self::COL_FK_MAK_PROGRAMMATIC_ACTION => $actionId,
            self::COL_FK_MAK_PROGRAMMATIC_RULE => $ruleId,
            self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN => $campaignId,
            self::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CHECKPOINT => $checkpoint,
            self::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_BY => $adminId,
            self::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_AT => Library\Common::getCurrentTimestamp()
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }


    /**
     * search Campaign Id By Date From To
     * @param int $campaignId
     * @param int $dateFrom
     * @param int $dateTo
     * @param array $actionIdArray
     * @return \Illuminate\Support\Collection
     */
    public static function searchCampaignIdByDateFromTo($campaignId, $dateFrom, $dateTo, $actionIdArray)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN, $campaignId)
            ->whereBetween(self::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_AT, [$dateFrom, $dateTo])
            ->whereIn(self::COL_FK_MAK_PROGRAMMATIC_ACTION, $actionIdArray)
            ->select(
                self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT,
                self::COL_FK_MAK_PROGRAMMATIC_ACTION,
                self::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_AT
            )
            ->get()
        ;
    }

    /**
     * search Shop Channel Id By Date From To
     * @param int $shopChannelId
     * @param int $dateFrom
     * @param int $dateTo
     * @param array $actionIdArray
     * @return \Illuminate\Support\Collection
     */
    public static function searchShopChannelIdByDateFromTo($shopChannelId, $dateFrom, $dateTo, $actionIdArray = [])
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticCampaign::TABLE_NAME,
                MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN
            )
            ->where(MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->whereBetween(self::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_AT, [$dateFrom, $dateTo])
            ->when($actionIdArray, function ($query) use ($actionIdArray) {
                return $query->whereIn(self::COL_FK_MAK_PROGRAMMATIC_ACTION, $actionIdArray);
            })
            ->select(
                self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                self::COL_FK_MAK_PROGRAMMATIC_ACTION,
                self::COL_MAK_PROGRAMMATIC_LOG_KEYWORD_CREATED_AT
            )
            ->get()
        ;
    }
}