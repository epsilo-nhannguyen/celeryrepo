<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:56
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;


class MakProgrammaticOperator extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_operator';

    const COL_MAK_PROGRAMMATIC_OPERATOR_ID = 'mak_programmatic_operator_id';
    const COL_MAK_PROGRAMMATIC_OPERATOR_NAME = 'mak_programmatic_operator_name';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_OPERATOR_ID;
    public $timestamps = false;

    /**
     * @var int
     * is =
     */
    const EQUALS = 1;

    /**
     * @var int
     * is >
     */
    const GREATER_THAN = 2;

    /**
     * @var int
     * is <
     */
    const LESS_THAN = 3;

    /**
     * @var int
     * is >=
     */
    const GREATER_THAN_OR_EQUAL_TO = 4;

    /**
     * @var int
     * is <=
     */
    const LESS_THAN_OR_EQUAL_TO = 5;

    /**
     * @var int
     * is !=
     */
    const NOR_EQUAL_TO = 6;

    /**
     * @var int
     * is is not
     */
    const IS_NOT = 7;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

}