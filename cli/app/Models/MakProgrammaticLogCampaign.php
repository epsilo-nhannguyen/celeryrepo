<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 29/10/2019
 * Time: 13:09
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakProgrammaticLogCampaign extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_log_campaign';

    const COL_MAK_PROGRAMMATIC_LOG_CAMPAIGN_ID = 'mak_programmatic_log_campaign_id';
    const COL_FK_MAK_PROGRAMMATIC_CAMPAIGN = 'fk_mak_programmatic_campaign';
    const COL_FK_MAK_PROGRAMMATIC_ACTION = 'fk_mak_programmatic_action';
    const COL_FK_MAK_PROGRAMMATIC_RULE = 'fk_mak_programmatic_rule';
    const COL_MAK_PROGRAMMATIC_LOG_CAMPAIGN_CHECKPOINT = 'mak_programmatic_log_campaign_checkpoint';
    const COL_MAK_PROGRAMMATIC_LOG_CAMPAIGN_CREATED_AT = 'mak_programmatic_log_campaign_created_at';
    const COL_MAK_PROGRAMMATIC_LOG_CAMPAIGN_CREATED_BY = 'mak_programmatic_log_campaign_created_by';


    /**
     * search Shop Channel Id By Date From To
     * @param int $shopChannelId
     * @param int $dateFrom
     * @param int $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function searchShopChannelIdByDateFromTo($shopChannelId, $dateFrom, $dateTo)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticCampaign::TABLE_NAME,
                MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN
            )
            ->where(MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->whereBetween(self::COL_MAK_PROGRAMMATIC_LOG_CAMPAIGN_CREATED_AT, [$dateFrom, $dateTo])
            ->select(
                self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                self::COL_FK_MAK_PROGRAMMATIC_ACTION,
                self::COL_MAK_PROGRAMMATIC_LOG_CAMPAIGN_CREATED_AT
            )
            ->get()
        ;
    }

}