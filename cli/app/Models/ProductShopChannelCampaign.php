<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 17:27
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ProductShopChannelCampaign extends Library\ModelBusiness
{
    const TABLE_NAME = 'product_shop_channel_campaign';

    const COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_ID = 'product_shop_channel_campaign_id';
    const COL_FK_PRODUCT_SHOP_CHANNEL = 'fk_product_shop_channel';
    const COL_FK_MAK_PROGRAMMATIC_CAMPAIGN = 'fk_mak_programmatic_campaign';
    const COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_CREATED_AT = 'product_shop_channel_campaign_created_at';
    const COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_CREATED_BY = 'product_shop_channel_campaign_created_by';
    const COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_UPDATED_AT = 'product_shop_channel_campaign_updated_at';
    const COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_UPDATED_BY = 'product_shop_channel_campaign_updated_by';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * build Data Insert
     * @param int $productShopChannelId
     * @param int $campaignId
     * @param int $adminId
     * @return array
     */
    public static function buildDataInsert($productShopChannelId, $campaignId, $adminId)
    {
        return [
            self::COL_FK_PRODUCT_SHOP_CHANNEL => $productShopChannelId,
            self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN => $campaignId,
            self::COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_CREATED_BY => $adminId,
            self::COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_CREATED_AT => Library\Common::getCurrentTimestamp(),
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * search By Campaign Id
     * @param $campaignId
     * @return mixed
     */
    public static function searchByCampaignId($campaignId)
    {
        return self::where(self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN, $campaignId)
            ->select(self::COL_FK_PRODUCT_SHOP_CHANNEL)
            ->get()
        ;
    }

    /**
     * search By Shop Channel Id
     * @param int $shopChannelId
     * @param bool $includeKeywordProduct
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelId($shopChannelId, $includeKeywordProduct = false)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticCampaign::TABLE_NAME,
                MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->when($includeKeywordProduct, function ($query) {
                return $query->join(
                    MakProgrammaticKeywordProduct::TABLE_NAME,
                    MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL,
                    '=',
                    ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID
                )
                ->select(MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID);
            })
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_FK_CONFIG_ACTIVE, ConfigActive::ACTIVE)
            ->addSelect(
                MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID
            )
            ->get()
        ;
    }

    /**
     * get By Product Shop Channel And Date
     * @param int $productShopChannelId
     * @param string $date
     * @return mixed
     */
    public static function getByProductShopChannelAndDate($productShopChannelId, $date)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticCampaign::TABLE_NAME,
                MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN
            )
            ->where(self::TABLE_NAME.'.'.self::COL_FK_PRODUCT_SHOP_CHANNEL, $productShopChannelId)
            ->where(function ($query) use ($date) {
                $query->whereNull(MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_FROM)
                    ->orWhere(MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_FROM, '<=', $date);
            })
            ->where(function ($query) use ($date) {
                $query->whereNull(MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_TO)
                    ->orWhere(MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_TO, '>=', $date);
            })
            ->select(self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN)
            ->first()
        ;
    }

    public static function getProductByShopchannel($shopChannelId, $DBConnect = null)
    {
        if(!$DBConnect){
            $DBConnect = env('DB_CONNECTION',Library\Model\Sql\Manager::SQL_CHART_CONNECT);
        }
        return  DB::connection($DBConnect)
            ->table(self::TABLE_NAME)
            ->join(MakProgrammaticCampaign::TABLE_NAME, MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID, self::COL_PRODUCT_SHOP_CHANNEL_CAMPAIGN_ID)
            ->where(MakProgrammaticCampaign::COL_FK_CONFIG_ACTIVE, 1)
            ->where(MakProgrammaticCampaign::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->select(self::COL_FK_PRODUCT_SHOP_CHANNEL)
            ->get();
    }

}