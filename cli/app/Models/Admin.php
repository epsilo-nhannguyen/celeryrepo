<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 08:37
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    const TABLE_NAME = 'admin';

    const COL_ADMIN_ID = 'admin_id';
    const COL_ADMIN_EMAIL = 'admin_email';
    const COL_ADMIN_PASSWORD = 'admin_password';
    const COL_ADMIN_FULLNAME = 'admin_fullname';
    const COL_ADMIN_PHONE = 'admin_phone';
    const COL_ADMIN_CREATED_AT = 'admin_created_at';
    const COL_ADMIN_LAST_LOGIN = 'admin_last_login';
    const COL_ADMIN_LAST_LOGIN_IP = 'admin_last_login_ip';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_FK_ADMIN_ROLE = 'fk_admin_role';
    const COL_FK_ORGANIZATION = 'fk_organization';
    const COL_ADMIN_TOKEN = 'admin_token';
    const COL_ADMIN_LOGIN_AS = 'admin_login_as';
    const COL_FK_ADMIN_MODULE = 'fk_admin_module';
    const COL_FK_ADMIN_TYPE = 'fk_admin_type';
    const COL_ADMIN_CREATED_BY = 'admin_created_by';
    const COL_ADMIN_UPDATED_AT = 'admin_updated_at';
    const COL_ADMIN_UPDATED_BY = 'admin_updated_by';
    const COL_ADMIN_LAST_VENTURE_WORKING = 'admin_last_venture_working';
    const COL_ADMIN_LAST_WAREHOUSE_WORKING = 'admin_last_warehouse_working';
    const COL_ADMIN_HISTORY_LAST_LOGIN = 'admin_history_last_login';
    const COL_ADMIN_URL_PATH = 'admin_url_path';
    const COL_FK_COUNTRY = 'fk_country';
    const COL_ADMIN_LAST_MODIFY_PASSWORD = 'admin_last_modify_password';
    const COL_FK_TABLEAU_CLIENT = 'fk_tableau_client';
    const COL_ADMIN_SHOW_CURRENCY_FOR_VENTURE = 'admin_show_currency_for_venture';
    const COL_IS_MEMBER = 'is_member';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_ADMIN_ID;
    public $timestamps = false;

    /**
     * @const int
     */
    const EPSILO_SYSTEM = 300;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

}