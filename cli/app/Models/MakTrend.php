<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 18:05
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakTrend extends Library\ModelBusiness
{
    const TABLE_NAME = 'mak_trend';

    const COL_MAK_TREND_ID = 'mak_trend_id';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT = 'fk_mak_programmatic_keyword_product';
    const COL_MAK_TREND_DATETIME = 'mak_trend_datetime';
    const COL_MAK_TREND_GMV = 'mak_trend_gmv';
    const COL_MAK_TREND_VIEW = 'mak_trend_view';
    const COL_MAK_TREND_CLICK_RATE = 'mak_trend_click_rate';
    const COL_MAK_TREND_ORDERS = 'mak_trend_orders';
    const COL_MAK_TREND_QUERY = 'mak_trend_query';
    const COL_MAK_TREND_PRODUCT_SOLD = 'mak_trend_product_sold';
    const COL_MAK_TREND_AVG_POSITION = 'mak_trend_avg_position';
    const COL_MAK_TREND_EXPENSE = 'mak_trend_expense';
    const COL_MAK_TREND_CLICKS = 'mak_trend_clicks';
    const COL_MAK_TREND_CREATED_AT = 'mak_trend_created_at';
    const COL_MAK_TREND_UPDATED_AT = 'mak_trend_updated_at';
    const COL_FK_MAK_PROGRAMMATIC_CAMPAIGN = 'fk_mak_programmatic_campaign';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_TREND_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**build Data Insert
     * @param $keywordProductId
     * @param $datetime
     * @param $gmv
     * @param $view
     * @param $clickRate
     * @param $orders
     * @param $search
     * @param $productSold
     * @param $avgPosition
     * @param $expense
     * @param $clicks
     * @param $campaignId
     * @return array
     */
    public static function buildDataInsert(
        $keywordProductId, $datetime, $gmv, $view, $clickRate, $orders, $search, $productSold, $avgPosition, $expense, $clicks, $campaignId
    ) {
        return [
            self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT => $keywordProductId,
            self::COL_MAK_TREND_DATETIME => $datetime,
            self::COL_MAK_TREND_GMV => $gmv,
            self::COL_MAK_TREND_VIEW => $view,
            self::COL_MAK_TREND_CLICK_RATE => $clickRate,
            self::COL_MAK_TREND_ORDERS => $orders,
            self::COL_MAK_TREND_QUERY => $search,
            self::COL_MAK_TREND_PRODUCT_SOLD => $productSold,
            self::COL_MAK_TREND_AVG_POSITION => $avgPosition,
            self::COL_MAK_TREND_EXPENSE => $expense,
            self::COL_MAK_TREND_CLICKS => $clicks,
            self::COL_MAK_TREND_CREATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_MAK_TREND_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN => $campaignId
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * get Max Datetime By Campaign Id Array
     * @param array $campaignIdArray
     * @param string $dateFrom
     * @param string $dateTo
     * @return mixed
     */
    public static function getMaxDatetimeByCampaignIdArray($campaignIdArray, $dateFrom, $dateTo)
    {
        if ( ! $campaignIdArray) return '';

        return DB::table(self::TABLE_NAME)
            ->whereIn(self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN, $campaignIdArray)
            ->whereBetween(self::COL_MAK_TREND_DATETIME, [$dateFrom, $dateTo])
            ->max(self::COL_MAK_TREND_DATETIME)
        ;
    }

    /**
     * sum Performance By Date From To
     * @param $shopChannelId
     * @param $dateFrom
     * @param $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function sumPerformanceByDateFromTo($shopChannelId, $dateFrom, $dateTo)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->whereBetween(self::TABLE_NAME.'.'.self::COL_MAK_TREND_DATETIME, [$dateFrom, $dateTo])
            ->select(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_VIEW.') as '.self::COL_MAK_TREND_VIEW),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_CLICKS.') as '.self::COL_MAK_TREND_CLICKS),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_CLICK_RATE.') as '.self::COL_MAK_TREND_CLICK_RATE),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_PRODUCT_SOLD.') as '.self::COL_MAK_TREND_PRODUCT_SOLD),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_GMV.') as '.self::COL_MAK_TREND_GMV),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_EXPENSE.') as '.self::COL_MAK_TREND_EXPENSE)
            )
            ->groupBy(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN
            )
            ->get()
        ;
    }

    /**
     * search By Shop Channel Id And Date From To
     * @param int $shopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdAndDateFromTo($shopChannelId, $dateFrom, $dateTo)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_TREND_VIEW, '>', 0)
            ->whereBetween(self::TABLE_NAME.'.'.self::COL_MAK_TREND_DATETIME, [$dateFrom, $dateTo])
            ->whereNotNull(self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN)
            ->select(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->get()
        ;
    }

    /**
     * search By Campaign Id
     * @param int $campaignId
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function searchByCampaignId($campaignId, $dateFrom, $dateTo)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN, $campaignId)
            ->whereBetween(self::COL_MAK_TREND_DATETIME, [$dateFrom, $dateTo])
            ->select(
                DB::raw('max('.self::COL_MAK_TREND_ID.') as '.self::COL_MAK_TREND_ID)
            )
            ->groupBy(
                self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->get()
        ;
    }

    /**
     * sumBy Id Array
     * @param array $idArray
     * @return \Illuminate\Support\Collection
     */
    public static function sumByIdArray($idArray)
    {
        return DB::table(self::TABLE_NAME)
            ->whereIn(self::COL_MAK_TREND_ID, $idArray)
            ->select(
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_VIEW.') as '.self::COL_MAK_TREND_VIEW),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_CLICKS.') as '.self::COL_MAK_TREND_CLICKS),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_CLICK_RATE.') as '.self::COL_MAK_TREND_CLICK_RATE),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_PRODUCT_SOLD.') as '.self::COL_MAK_TREND_PRODUCT_SOLD),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_GMV.') as '.self::COL_MAK_TREND_GMV),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_TREND_EXPENSE.') as '.self::COL_MAK_TREND_EXPENSE)
            )
            ->get()
            ->first()
        ;
    }

    /**
     * @param int $campaignId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public static function sumPerformanceByCampaignId($campaignId, $dateFrom, $dateTo)
    {
        $sql = "
            select
            ifnull(sum(a.mak_trend_view), 0) as mak_trend_view,
            ifnull(sum(a.mak_trend_clicks), 0) as mak_trend_clicks,
            ifnull(sum(a.mak_trend_click_rate), 0) as mak_trend_click_rate,
            ifnull(sum(a.mak_trend_product_sold), 0) as mak_trend_product_sold,
            ifnull(sum(a.mak_trend_gmv), 0) as mak_trend_gmv,
            ifnull(sum(a.mak_trend_expense), 0) as mak_trend_expense,
            a.fk_mak_programmatic_campaign
            from (
            select mak_trend.*
            from mak_trend
            where mak_trend_datetime between '{$dateFrom}' and '{$dateTo}'
            and fk_mak_programmatic_campaign={$campaignId}
            ) as a
            left join (
            select mak_trend.*
            from mak_trend
            where mak_trend_datetime between '{$dateFrom}' and '{$dateTo}'
            and fk_mak_programmatic_campaign={$campaignId}
            ) as b on a.mak_trend_id < b.mak_trend_id
            and a.fk_mak_programmatic_keyword_product=b.fk_mak_programmatic_keyword_product
            where b.mak_trend_id is null;
        ";

        return DB::select($sql);
    }

}