<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 07/10/2019
 * Time: 13:59
 */

namespace App\Models\CrawlerLog;

class ProductRankingShopee extends \App\Library\Model\Mongo\CrawlerLog\ProductRankingShopee
{
    public static function topDataQuery($keyword, $startTime, $stopTime)
    {
        $match = [
            'isSponsorPosition' => ['$gt' => 0],
            'keywordSearch' => $keyword,
            'timeRaw' => [
                '$gte' => intval($startTime),
                '$lte' => intval($stopTime)
            ],
        ];
        $aggregation = [
            ['$match' => $match],
            ['$group' => [
                '_id' => [
                    'sku' => '$itemid',
                    'keyword' => '$keywordSearch',
                ],
                'avg' => ['$avg' => '$isSponsorPosition']
            ]],
            ['$sort' => ['avg' => 1]],
            ['$limit' => 10]
        ];
        return self::aggregate($aggregation);
    }

    public static function topSkuPosition($keyword, $itemidArray, $startTime, $stopTime)
    {
        $matchFirst = [
            'timeRaw' => ['$gte' => intval($startTime), '$lte' => intval($stopTime)],
        ];
        $matchSecond = [
            'isSponsorPosition' => ['$ne' => null],
            'keywordSearch' => $keyword,
            'itemid' => ['$in' => array_map('intval', $itemidArray)],
        ];
        $projection = [
            'timeRaw' => 1,
            'isSponsorPosition' => 1,
            'itemid' => 1
        ];
        $aggregation = [
            ['$match' => $matchFirst],
            ['$match' => $matchSecond],
            ['$project' => $projection],
        ];
        return self::aggregate($aggregation);
    }
}