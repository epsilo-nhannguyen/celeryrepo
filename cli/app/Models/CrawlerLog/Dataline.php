<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 07/10/2019
 * Time: 13:59
 */
namespace App\Models\CrawlerLog;

class Dataline extends \App\Library\Model\Mongo\CrawlerLog\Dataline
{
    public static function searchData($fromTime, $shopChannelId, $keyword, $itemId)
    {
        $fromTime = intval($fromTime);
        $shopChannelId = intval($shopChannelId);
        $keyword = trim($keyword);
        $itemId = trim($itemId);


        $filter = [
            'timestamp' => ['$gte' => $fromTime],
            'shop_channel_id' => $shopChannelId,
            'keyword' => $keyword,
            'sku' => $itemId,
        ];
        $options = [];
        return static::select($filter, [], $options)->toArray();
    }
}