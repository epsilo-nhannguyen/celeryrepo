<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 18:04
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakPerformanceHour extends Library\ModelBusiness
{
    const TABLE_NAME = 'mak_performance_hour';

    const COL_MAK_PERFORMANCE_HOUR_ID = 'mak_performance_hour_id';
    const COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT = 'fk_mak_programmatic_keyword_product';
    const COL_MAK_PERFORMANCE_HOUR_DATE = 'mak_performance_hour_date';
    const COL_MAK_PERFORMANCE_HOUR_HOUR = 'mak_performance_hour_hour';
    const COL_MAK_PERFORMANCE_HOUR_GMV = 'mak_performance_hour_gmv';
    const COL_MAK_PERFORMANCE_HOUR_VIEW = 'mak_performance_hour_view';
    const COL_MAK_PERFORMANCE_HOUR_CLICK_RATE = 'mak_performance_hour_click_rate';
    const COL_MAK_PERFORMANCE_HOUR_ORDERS = 'mak_performance_hour_orders';
    const COL_MAK_PERFORMANCE_HOUR_QUERY = 'mak_performance_hour_query';
    const COL_MAK_PERFORMANCE_HOUR_PRODUCT_SOLD = 'mak_performance_hour_product_sold';
    const COL_MAK_PERFORMANCE_HOUR_AVG_POSITION = 'mak_performance_hour_avg_position';
    const COL_MAK_PERFORMANCE_HOUR_EXPENSE = 'mak_performance_hour_expense';
    const COL_MAK_PERFORMANCE_HOUR_CLICKS = 'mak_performance_hour_clicks';
    const COL_MAK_PERFORMANCE_HOUR_CREATED_AT = 'mak_performance_hour_created_at';
    const COL_MAK_PERFORMANCE_HOUR_UPDATED_AT = 'mak_performance_hour_updated_at';
    const COL_FK_MAK_PROGRAMMATIC_CAMPAIGN = 'fk_mak_programmatic_campaign';

    use Master;

    /**
     * sum Performance By Date Hour
     * @param int $shopChannelId
     * @param string $date
     * @param int $hour
     * @return \Illuminate\Support\Collection
     */
    public static function sumPerformanceByDateHour($shopChannelId, $date, $hour)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_DATE, $date)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_HOUR, $hour)
            ->select(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_VIEW.') as total_view'),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_CLICKS.') as total_click'),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_PRODUCT_SOLD.') as total_item_sold'),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_GMV.') as total_gmv'),
                DB::raw('sum('.self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_EXPENSE.') as total_expense')
            )
            ->groupBy(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN
            )
            ->get()
        ;
    }

    /**
     * sum Performance By Date Hour
     * @param int $shopChannelId
     * @param string $date
     * @param int $hour
     * @return \Illuminate\Support\Collection
     */
    public static function performanceByDateHour($shopChannelId, $date, $hour)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_DATE, $date)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_HOUR, $hour)
            ->select(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                DB::raw(sprintf('sum(%s) as total_view', self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_VIEW)),
                DB::raw(sprintf('sum(%s) as total_click', self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_CLICKS)),
                DB::raw(sprintf('sum(%s) as total_item_sold', self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_PRODUCT_SOLD)),
                DB::raw(sprintf('sum(%s) as total_gmv', self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_GMV)),
                DB::raw(sprintf('sum(%s) as total_cost', self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_EXPENSE))
            )
            ->groupBy([
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                'mak_performance_hour_date',
                'mak_performance_hour_hour'
            ])
            ->get()
            ;
    }

    /**
     * search By View Greater 0
     * @param int $shopChannelId
     * @param string $date
     * @param int $hour
     * @return \Illuminate\Support\Collection
     */
    public static function searchByViewGreater0($shopChannelId, $date, $hour)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticKeywordProduct::TABLE_NAME,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )
            ->join(
                ProductShopChannel::TABLE_NAME,
                ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID,
                '=',
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->where(ProductShopChannel::TABLE_NAME.'.'.ProductShopChannel::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_DATE, $date)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_HOUR, $hour)
            ->where(self::TABLE_NAME.'.'.self::COL_MAK_PERFORMANCE_HOUR_VIEW, '>', 0)
            ->select(
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID,
                MakProgrammaticKeywordProduct::TABLE_NAME.'.'.MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            )
            ->get()
        ;
    }

    /**
     * @param $id
     * @param $keywordProductId
     * @param $date
     * @param $hour
     * @param $gmv
     * @param $view
     * @param $clickRate
     * @param $orders
     * @param $query
     * @param $itemSold
     * @param $avgPosition
     * @param $expense
     * @param $clicks
     * @param $campaignId
     * @param $locationInAds
     * @return array
     */
    public static function buildRow(
        $keywordProductId,
        $date,
        $hour,
        $gmv,
        $view,
        $clickRate,
        $orders,
        $query,
        $itemSold,
        $avgPosition,
        $expense,
        $clicks,
        $campaignId,
        $locationInAds
    ) {
        return [
            'fk_mak_programmatic_keyword_product' => $keywordProductId,
            'mak_performance_hour_date' => $date,
            'mak_performance_hour_hour' => $hour,
            'mak_performance_hour_gmv' => $gmv,
            'mak_performance_hour_view' => $view,
            'mak_performance_hour_click_rate' => $clickRate,
            'mak_performance_hour_orders' => $orders,
            'mak_performance_hour_query' => $query,
            'mak_performance_hour_product_sold' => $itemSold,
            'mak_performance_hour_avg_position' => $avgPosition,
            'mak_performance_hour_expense' => $expense,
            'mak_performance_hour_clicks' => $clicks,
            'mak_performance_hour_created_at' => strtotime('now'),
            'mak_performance_hour_updated_at' => strtotime('now'),
            'fk_mak_programmatic_campaign' => $campaignId,
            'location_in_ads' => $locationInAds,
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('mak_performance_hour')->insert($recordArray);
    }

    /**
     * @param $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::connection('master_business')->statement($sql);
    }

    /**
     * @param $arrayKeywordProductId
     * @param $date
     * @param $hour
     * @return \Illuminate\Support\Collection
     */
    public static function getByArraySkuKeyword($arrayKeywordProductId, $date, $hour)
    {
        $select = DB::connection('master_business')->table('mak_performance_hour')
            ->whereIn('fk_mak_programmatic_keyword_product', $arrayKeywordProductId)
            ->where('mak_performance_hour_date', $date)
            ->where('mak_performance_hour_hour', $hour)
            ->select([
                'mak_performance_hour_id',
                'fk_mak_programmatic_keyword_product',
            ])
        ;

        return $select->get();
    }

}