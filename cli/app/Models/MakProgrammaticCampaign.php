<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 16:26
 */

namespace App\Models;


use App\Library;
use App\Services;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MakProgrammaticCampaign extends Library\ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_campaign';

    const COL_MAK_PROGRAMMATIC_CAMPAIGN_ID = 'mak_programmatic_campaign_id';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_NAME = 'mak_programmatic_campaign_name';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_BUDGET = 'mak_programmatic_campaign_budget';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_FROM = 'mak_programmatic_campaign_from';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_TO = 'mak_programmatic_campaign_to';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_CREATED_AT = 'mak_programmatic_campaign_created_at';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_CREATED_BY = 'mak_programmatic_campaign_created_by';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_UPDATED_AT = 'mak_programmatic_campaign_updated_at';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_UPDATED_BY = 'mak_programmatic_campaign_updated_by';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';
    const COL_MAK_PROGRAMMATIC_CAMPAIGN_CODE = 'mak_programmatic_campaign_code';
    const COL_FK_ORGANIZATION = 'fk_organization';
    const COL_TYPE_CAMPAIGN = 'type_campaign';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID;
    public $timestamps = false;

    /**
     * @var string
     */
    const CAMPAIGN_NAME_DEFAULT = 'Campaign Default';
    const SMART_CAMPAIGN = 'smart_campaign';


    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * insert Get Id
     * @param int $organizationId
     * @param int $shopChannelId
     * @param string $name
     * @param float $budget
     * @param string $from
     * @param string $to
     * @param int $adminId
     * @return mixed
     */
    public static function insertGetId($organizationId, $shopChannelId, $name, $budget, $from, $to, $adminId)
    {
        return DB::table(self::TABLE_NAME)->insertGetId(
            [
                self::COL_FK_ORGANIZATION => $organizationId,
                self::COL_FK_SHOP_CHANNEL => $shopChannelId,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_NAME => $name,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_BUDGET => $budget,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_FROM => $from,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_TO => $to,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_CREATED_BY => $adminId,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_CREATED_AT => Library\Common::getCurrentTimestamp(),
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_CODE => Services\ProgrammaticCampaign::generateCampaignCode($organizationId),
            ]
        );
    }

    /**
     * get Max Campaign Code
     * @param int $organizationId
     * @return mixed
     */
    public static function getMaxCampaignCode($organizationId)
    {
        return self::where(self::COL_FK_ORGANIZATION, $organizationId)
            ->max(self::COL_MAK_PROGRAMMATIC_CAMPAIGN_CODE)
        ;
    }

    /**
     * update Status By Id
     * @param int $id
     * @param int $status
     * @param int $adminId
     * @return mixed
     */
    public static function updateStatusById($id, $status, $adminId)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID, $id)
            ->update([
                self::COL_FK_CONFIG_ACTIVE => $status,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_UPDATED_BY => $adminId,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            ])
        ;
    }

    /**
     * search By Shop Channel Id
     * @param int $shopChannelId
     * @param int $status
     * @return mixed
     */
    public static function searchByShopChannelId($shopChannelId, $status = -1)
    {
        return self::where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->when($status > -1, function ($query) use ($status) {
                return $query->where(self::COL_FK_CONFIG_ACTIVE, $status);
            })
            ->select(
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_NAME,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_BUDGET,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_FROM,
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_TO
            )
            ->get()
        ;
    }

    /**
     * get By Shop Channel Id
     * @param int $shopChannelId
     * @return mixed
     */
    public static function getByShopChannelId($shopChannelId)
    {
        return self::where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->select(self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID)
            ->orderBy(self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID)
            ->first()
        ;
    }

    /**
     * search Available Timeline To Update
     * @param int $shopChannelId
     * @param string $date
     * @return mixed
     */
    public static function searchAvailableTimelineToUpdate($shopChannelId, $date)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->whereNotNull(self::COL_MAK_PROGRAMMATIC_CAMPAIGN_FROM)
            ->select(
                self::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                self::COL_FK_CONFIG_ACTIVE,
                DB::raw(
                    sprintf(
                        "(%s <= '%s') and if (%s is not null, '%s' <= %s, 1) as `isAvailable`",
                        self::COL_MAK_PROGRAMMATIC_CAMPAIGN_FROM,
                        $date,
                        self::COL_MAK_PROGRAMMATIC_CAMPAIGN_TO,
                        $date,
                        self::COL_MAK_PROGRAMMATIC_CAMPAIGN_TO
                    )
                )
            )
            ->get()
        ;
    }

    public static function getSmartCampaign($shopChannelId)
    {
        return self::where(self::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->where(self::COL_FK_CONFIG_ACTIVE, 1)
            ->where(self::COL_TYPE_CAMPAIGN, self::SMART_CAMPAIGN)
            ->orderBy(self::COL_MAK_PROGRAMMATIC_CAMPAIGN_CREATED_AT, 'DESC')
            ->first();
    }

}