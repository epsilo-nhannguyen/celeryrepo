<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;


class ShopAdsAction extends ModelBusiness
{

    /**
     * @var int
     */
    const DELETE_SHOP_ADS_KEYWORD = 1;

    /**
     * @var int
     */
    const RESTORE_SHOP_ADS_KEYWORD = 2;

    /**
     * @var int
     */
    const INCREASE_BIDDING_PRICE = 3;

    /**
     * @var int
     */
    const DECREASE_BIDDING_PRICE = 4;

    /**
     * @var int
     */
    const ADD_SHOP_ADS = 5;

    /**
     * @var int
     */
    const PAUSE_SHOP_ADS = 6;

    /**
     * @var int
     */
    const RESUME_SHOP_ADS = 7;

    /**
     * @var int
     */
    const ADD_SHOP_ADS_KEYWORD = 8;

}