<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;
use App\Library;


class TokoAdsKeywordPerformance extends ModelBusiness
{
    protected $table = 'toko_ads_keyword_performance';
    public static function isKeywordPerformanceFromChannelExists($channelCampaignId, $channelCreateDate)
    {
        $keyword = TokoAdsKeywordPerformance::where('channel_campaign_id', $channelCampaignId)
            ->where('channel_create_date', $channelCreateDate)->first();
        return $keyword ? true : false;
    }

    public static function updateIfExistKeywordPerformanceFromChannel($channelCampaignId, $channelCreateDate, $data)
    {
        $keywords = TokoAdsKeywordPerformance::where('channel_campaign_id', $channelCampaignId)
            ->where('channel_create_date', $channelCreateDate);

        return $keywords->update($data);
    }

    public static function addNewIfNonExistsKeywordPerformanceFromChannel($data)
    {
        return TokoAdsKeywordPerformance::insert($data);
    }


}
