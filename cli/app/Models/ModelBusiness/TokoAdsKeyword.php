<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Library;


class TokoAdsKeyword extends ModelBusiness
{
    protected $table = 'toko_ads_keyword';

    /**
     * search By Channel Keyword Id Array
     * @param array $channelKeywordIdArray
     * @return Collection
     */
    public static function searchByChannelKeywordIdArray($channelKeywordIdArray)
    {
        $query = DB::connection('master_business')->table('toko_ads_keyword')
            ->whereIn('channel_keyword_id', $channelKeywordIdArray)
            ->select([
                'id',
                'campaign_id',
                'channel_keyword_id',
            ])
        ;

        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('toko_ads_keyword')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('toko_ads_keyword')
            ->where('id', $id)
            ->update($record)
            ;
    }

    /**
     * search By Campaign Id
     * @param int $campaignId
     * @param array $keywordArray
     * @return Collection
     */
    public static function searchByCampaignId($campaignId, $keywordArray = [])
    {
        $query = DB::connection('master_business')->table('toko_ads_keyword')
            ->where('campaign_id', $campaignId)
            ->when($keywordArray, function ($select) use ($keywordArray) {
                return $select->whereIn('name', $keywordArray);
            })
            ->select([
                'id',
                'name',
                'campaign_id',
                'channel_keyword_id',
                'config_active_id',
                'max_cost',
                'match_type',
                'updated_by',
            ])
        ;

        return $query->get();
    }

    /**
     * search By Campaign Id Array
     * @param array $campaignIdArray
     * @return Collection
     */
    public static function searchByCampaignIdArray($campaignIdArray)
    {
        if ( ! $campaignIdArray) return collect([]);

        $query = DB::connection('master_business')->table('toko_ads_keyword')
            ->whereIn('campaign_id', $campaignIdArray)
            ->select([
                'id',
                'name',
                'campaign_id',
                'channel_keyword_id',
                'config_active_id',
                'max_cost',
                'match_type',
                'updated_by',
            ])
        ;
        return $query->get();
    }

    public static function getCampaignByCampaignId($id)
    {
        return DB::connection('master_business')->table('toko_ads_campaign as tac')
            ->join('toko_ads_keyword as tak', 'tak.campaign_id', '=', 'tac.id')
            ->where('tak.id', $id)->select(['tac.*']);
    }

    public static function getById($id)
    {
        return TokoAdsKeyword::where('id', $id)->first();
    }

    public static function getByIds($ids)
    {
        return TokoAdsKeyword::whereIn('id', $ids);
    }

    /**
     * @param $keywordId
     * @param $value
     */
    public static function queryUpdateStatus($keywordId, $value)
    {
        $keyword = TokoAdsKeyword::where('id', $keywordId)->first();
        $keyword->config_active_id = $value;
        return $keyword->save();
    }

    public static function queryUpdateMatchType($ids, $value)
    {
        try {
            $keywords = TokoAdsKeyword::whereIn('id', $ids);
            $keywords->update(['match_type' => $value]);
            return true;
        } catch (Exception $exception){
            $logError = new Library\LogError();
            $logError->slack('toko_ads: error from update match type keyword'. json_encode($exception));
        }
    }

    public static function queryUpdateMaxCost($ids, $value)
    {
        try {
            $keywords = TokoAdsKeyword::whereIn('id', $ids);
            $keywords->update(['max_cost' => $value]);
            return true;
        } catch (Exception $exception){
            $logError = new Library\LogError();
            $logError->slack('toko_ads: error from update max cost keyword'. json_encode($exception));
        }
    }

    public static function getKeywordIdByChannel($channelCampaignId, $channelKeywordId)
    {
        return DB::connection('master_business')->table('toko_ads_keyword as tak')
            ->join('toko_ads_campaign as tac', 'tac.id', '=', 'tak.campaign_id')
            ->where('tak.channel_keyword_id', $channelKeywordId)
            ->where('tac.channel_campaign_id', $channelCampaignId)
            ->select(['tak.id'])->pluck('id')->first();
    }

}
