<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use App\Library;
use Exception;
use Illuminate\Support\Facades\DB;

class ShopAdsDataPerformanceIntraday extends ModelBusiness
{
    const TABLE_NAME = 'shop_ads_data_performance_intraday';

    const COL_ID = 'id';
    const COL_SHOP_ADS_ID = 'shop_ads_id';
    const COL_SHOP_ADS_KEYWORD_ID = 'shop_ads_keyword_id';
    const COL_KEYWORD_NAME = 'keyword_name';
    const COL_CREATE_DATE = 'create_date';
    const COL_HOUR = 'hour';
    const COL_GMV = 'gmv';
    const COL_EXPENSE = 'expense';
    const COL_SOLD = 'sold';
    const COL_SHOP_ITEM_CLICK = 'shop_item_click';
    const COL_ORDER_AMOUNT = 'order_amount';
    const COL_VIEW = 'view';
    const COL_CLICK = 'click';
    const COL_CREATED_AT = 'created_at';
    const COL_UPDATED_AT = 'updated_at';
    const COL_RAW_DATA = 'data';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_ID;
    public $timestamps = false;

    /**
     * @param $shopAdsId
     * @param $keywordId
     * @param $keywordName
     * @param $createDate
     * @param $hour
     * @param $gmv
     * @param $expense
     * @param $sold
     * @param $shopItemClick
     * @param $orderAmount
     * @param $view
     * @param $click
     * @param $fullJson
     * @return array
     */
    public static function buildDataInsert(
        $shopAdsId, $keywordId, $keywordName, $createDate, $hour, $gmv, $expense,
        $sold, $shopItemClick, $orderAmount, $view, $click, $fullJson)
    {
        return [
            self::COL_SHOP_ADS_ID => $shopAdsId,
            self::COL_SHOP_ADS_KEYWORD_ID => $keywordId,
            self::COL_KEYWORD_NAME => $keywordName,
            self::COL_CREATE_DATE => Library\Formater::timeStampToDate($createDate),
            self::COL_HOUR => intval($hour),
            self::COL_GMV => $gmv,
            self::COL_EXPENSE => $expense,
            self::COL_SOLD => $sold,
            self::COL_SHOP_ITEM_CLICK => $shopItemClick,
            self::COL_ORDER_AMOUNT => $orderAmount,
            self::COL_VIEW => $view,
            self::COL_CLICK => $click,
            self::COL_CREATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_RAW_DATA => json_encode($fullJson)
        ];
    }

    /**
     * @param $gmv
     * @param $expense
     * @param $sold
     * @param $shopItemClick
     * @param $orderAmount
     * @param $view
     * @param $click
     * @param $fullJson
     * @return array
     */
    public static function buildDataUpdate(
        $gmv, $expense, $sold, $shopItemClick,
        $orderAmount, $view, $click, $fullJson)
    {
        return [
            self::COL_GMV => $gmv,
            self::COL_EXPENSE => $expense,
            self::COL_SOLD => $sold,
            self::COL_SHOP_ITEM_CLICK => $shopItemClick,
            self::COL_ORDER_AMOUNT => $orderAmount,
            self::COL_VIEW => $view,
            self::COL_CLICK => $click,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_RAW_DATA => json_encode($fullJson)
        ];
    }

    /**
     * Batch Insert
     * @param array $recordInsert
     * @return mixed
     * @throws Exception
     */
    public static function insertData($recordInsert)
    {
        try {
            return DB::table(self::TABLE_NAME)->insert($recordInsert);
        } catch (Exception $e) {
            Library\LogError::logErrorToTextFile($e);
            throw $e;
        }
    }

    /**
     * @param $recordId
     * @param $recordUpdateData
     * @return mixed
     * @throws Exception
     */
    public static function updateData($recordId, $recordUpdateData)
    {
        try {
            return self::where(self::COL_ID, $recordId)
                ->update($recordUpdateData);
        } catch (Exception $e) {
            Library\LogError::logErrorToTextFile($e);
            throw $e;
        }
    }

    public static function searchByShopAdsKeywordIdAndDateHour($shopAdsKeywordId, $date, $hour)
    {
        $hour = intval($hour);
        $date = Library\Formater::timeStampToDate($date);
        return DB::table(self::TABLE_NAME)
            ->join(
                ShopAdsKeyword::TABLE_NAME,
                ShopAdsKeyword::TABLE_NAME . '.' . ShopAdsKeyword::COL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_SHOP_ADS_KEYWORD_ID
            )
            ->where(self::TABLE_NAME . '.' . self::COL_SHOP_ADS_KEYWORD_ID, $shopAdsKeywordId)
            ->where(self::TABLE_NAME . '.' . self::COL_CREATE_DATE, $date)
            ->where(self::TABLE_NAME . '.' . self::COL_HOUR, $hour)
            ->select(
                self::TABLE_NAME . '.' . self::COL_ID,
                self::TABLE_NAME . '.' . self::COL_SHOP_ADS_KEYWORD_ID,
                self::TABLE_NAME . '.' . self::COL_CREATE_DATE,
                self::TABLE_NAME . '.' . self::COL_HOUR
            )
            ->offset(0)
            ->limit(1)
            ->first();
    }
}