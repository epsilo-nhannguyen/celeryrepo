<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;
use App\Library;


class TokoAdsCampaignPerformance extends ModelBusiness
{
    protected $table = 'toko_ads_campaign_performance';
    const TABLE_NAME = 'toko_ads_campaign_performance';
    const COL_ID = 'id';
    const COL_CAMPAIGN_ID = 'campaign_id';
    const COL_CHANNEL_CAMPAIGN_ID = 'channel_campaign_id';
    const COL_IMPRESSION = 'impression';
    const COL_CLICK = 'click';
    const COL_ITEM_SOLD = 'item_sold';
    const COL_COST = 'cost';
    const COL_GMV = 'gmv';
    const COL_CTR = 'ctr';
    const COL_ALL_SOLD = 'all_sold';
    const COL_AVERAGE = 'average';
    const COL_RANK = 'rank';
    const COL_CHANNEL_CREATE_DATE = 'channel_create_date';
    const COL_IS_AUTO = 'is_auto';
    const COL_CREATED_AT = 'created_at';
    const COL_UPDATED_AT = 'updated_at';



    public static function isCampaignPerformanceFromChannelExists($channelCampaignId, $channelCreateDate)
    {
        $campaign = TokoAdsCampaignPerformance::where('channel_campaign_id', $channelCampaignId)
            ->where('channel_create_date', $channelCreateDate)->first();
        return $campaign ? true : false;
    }

    public static function updateIfExistCampaignPerformanceFromChannel($channelCampaignId, $channelCreateDate, $data)
    {
        $campaigns = TokoAdsCampaignPerformance::where('channel_campaign_id', $channelCampaignId)
            ->where('channel_create_date', $channelCreateDate);

        return $campaigns->update($data);
    }

    public static function addNewIfNonExistsCampaignPerformanceFromChannel($data)
    {
        return TokoAdsCampaignPerformance::insert($data);
    }


}
