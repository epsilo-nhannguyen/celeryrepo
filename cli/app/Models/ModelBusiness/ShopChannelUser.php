<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopChannelUser extends ModelBusiness
{

    #region TABLE shop_channel_user
    /**
     * @var string
     */
    const TABLE_NAME = 'shop_channel_user';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_USER_ID = 'shop_channel_user_id';

    /**
     * @var string
     */
    const COL_FK_USER = 'fk_user';

    /**
     * @var string
     */
    const COL_FK_SHOP_CHANNEL = 'fk_shop_channel';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_USER_CREATED_AT = 'shop_channel_user_created_at';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_USER_CREATED_BY = 'shop_channel_user_created_by';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_USER_UPDATED_AT = 'shop_channel_user_updated_at';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_USER_UPDATED_BY = 'shop_channel_user_updated_by';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = self::COL_SHOP_CHANNEL_USER_ID;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COL_FK_USER,
        self::COL_FK_SHOP_CHANNEL,
        self::COL_SHOP_CHANNEL_USER_CREATED_AT,
        self::COL_SHOP_CHANNEL_USER_CREATED_BY,
        self::COL_SHOP_CHANNEL_USER_UPDATED_AT,
        self::COL_SHOP_CHANNEL_USER_UPDATED_BY,
    ];
    #endregion

    /**
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public static function getByUser($userId)
    {
        $userId = intval($userId);
        $sql = self::queryGetByUser($userId);
        return $sql->get();
    }

    /**
     * @param $userId
     * @return \Illuminate\Database\Query\Builder
     */
    public static function queryGetByUser($userId)
    {
        $select = DB::connection('master_business')->table(self::TABLE_NAME)
            ->join(
                ShopChannel::TABLE_NAME,
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_SHOP_CHANNEL
            )
            ->join(
                Channel::TABLE_NAME,
                Channel::TABLE_NAME . '.' . Channel::COL_CHANNEL_ID,
                '=',
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_FK_CHANNEL
            )
            ->join(
                Venture::TABLE_NAME,
                Venture::TABLE_NAME . '.' . Venture::COL_VENTURE_ID,
                '=',
                Channel::TABLE_NAME . '.' . Channel::COL_FK_VENTURE
            )
            ->where(self::COL_FK_USER, '=', $userId);
        $select->select(
            self::COL_FK_SHOP_CHANNEL,
            ShopChannel::COL_FK_SHOP_MASTER,
            ShopChannel::COL_FK_CHANNEL,
            ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_FK_CONFIG_ACTIVE,
            Venture::COL_VENTURE_TIMEZONE,
            Venture::COL_VENTURE_TIMEZONE_TIME_PADDING,
            Venture::COL_VENTURE_COUNTRY,
            Venture::COL_VENTURE_ID
        );
        return $select;
    }

    /**
     * @param $userId
     * @param $channelName
     * @param $shopChannelId
     * @return array|\Illuminate\Support\Collection
     */
    public static function getAllByUserId($userId, $channelName, $shopChannelId)
    {
        $userId = intval($userId);
        $channelName = trim($channelName);
        $shopChannelId = intval($shopChannelId);
        if (!$userId) {
            return [];
        }

        $select = self::queryGetAllByUserId($userId, $channelName, $shopChannelId);
        return $select->get();
    }

    /**
     * @param $userId
     * @param $channelName
     * @param $shopChannelId
     * @return \Illuminate\Database\Query\Builder
     */
    public static function queryGetAllByUserId($userId, $channelName, $shopChannelId)
    {
        $select = DB::table(self::TABLE_NAME)
            ->join(
                ShopChannel::TABLE_NAME,
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_SHOP_CHANNEL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_SHOP_CHANNEL
            )
            ->join(
                Channel::TABLE_NAME,
                Channel::TABLE_NAME . '.' . Channel::COL_CHANNEL_ID,
                '=',
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_FK_CHANNEL
            )
            ->where(
                self::COL_FK_USER, '=', $userId
            );
        if (!empty($channelName)) {
            $select->where(Channel::COL_CHANNEL_NAME, '=', $channelName);
        }

        if ($shopChannelId) {
            $select->where(ShopChannel::COL_SHOP_CHANNEL_ID, '=', $shopChannelId);
        }

        $select->select(
            self::COL_FK_SHOP_CHANNEL,
            ShopChannel::COL_FK_SHOP_MASTER,
            ShopChannel::COL_SHOP_CHANNEL_CREATED_BY,
            Channel::COL_CHANNEL_ID,
            Channel::COL_CHANNEL_NAME,
            Channel::COL_CHANNEL_CODE
        );
        return $select;
    }

    /**
     * @param string $email
     * @return int
     */
    public static function countByEmail($email)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                Admin::TABLE_NAME,
                Admin::TABLE_NAME . '.' . Admin::COL_ADMIN_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_USER
            )
            ->where(Admin::COL_ADMIN_EMAIL, '=', $email)
            ->count();
    }

    /**
     * @param array $inputs
     * @return bool
     */
    public static function insert($inputs)
    {
        $model = new self();
        foreach ($inputs as $key => $val) {
            $model->$key = $val;
        }
        return $model->save();
    }
}