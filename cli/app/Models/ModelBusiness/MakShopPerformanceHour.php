<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use App\Models\Master;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MakShopPerformanceHour extends ModelBusiness
{
    use Master;

    public $timestamps = false;

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('mak_performance_hour')->insert($recordArray);
    }

    /**
     * @param $sql
     * @return bool
     */
    public static function updateCase($sql)
    {
        return DB::connection('master_business')->statement($sql);
    }

    /**
     * @param $shopChannelId
     * @param $date
     * @return \Illuminate\Support\Collection
     */
    public static function findByShopAndDate($shopChannelId, $date)
    {
        $select = DB::connection('master_business')->table('mak_shop_performance_hour')
            ->where('shop_channel_id', $shopChannelId)
            ->where('date', $date)
            ->select([
                'id',
                'hour'
            ])
        ;

        return $select->get();
    }

    /**
     * @param Collection $arrayRows
     */
    public static function saveMulti(Collection $arrayRows)
    {
        $groupedDate = $arrayRows->groupBy('date')->all();

        foreach ($groupedDate as $date => $collectionPerDate) {
            /** @var Collection $collectionPerDate */
            $groupedShop = $collectionPerDate->groupBy('shop_channel_id')->all();

            foreach ($groupedShop as $shopId => $collectionPerShop) {
                /** @var Collection $collectionPerShop */

                $listInDb = static::findByShopAndDate($shopId, $date)->pluck('id', 'hour');

                foreach ($collectionPerShop as $dataHour) {
                    $hour = intval($dataHour['hour']);
                    $id = $listInDb[$hour] ?? 0;
                    $dataHour['hour'] = $hour;
                    if ($id > 0) {
                        DB::connection('master_business')->table('mak_shop_performance_hour')->where('id', $id)->update($dataHour);
                    } else {
                        DB::connection('master_business')->table('mak_shop_performance_hour')->insert($dataHour);
                    }
                }
            }
        }
    }

    public static function performanceByDateHour($shopChannelId, $campaignMetricIntradayDate, $campaignMetricIntradayHour)
    {
        $select = DB::connection('master_business')->table('mak_shop_performance_hour')
            ->where('shop_channel_id', $shopChannelId)
            ->where('date', $campaignMetricIntradayDate)
            ->where('hour', $campaignMetricIntradayHour)
            ->select([
                DB::raw('impression as total_view'),
                DB::raw('click as total_click'),
                DB::raw('order_amount as total_item_sold'),
                DB::raw('order_gmv as total_gmv'),
                DB::raw('cost as total_cost')
            ])
        ;

        return $select->get();
    }

    public static function getByDate($shopChannelId, $date)
    {
        $select = DB::connection('master_business')->table('mak_shop_performance_hour')
            ->where('shop_channel_id', $shopChannelId)
            ->where('date', $date)
            ->select([
                'impression',
                'click',
                'order_amount',
                'order_gmv',
                'cost',
                'hour'
            ])
        ;

        return $select->get();
    }

    /**
     * @param $shopChannelId
     * @return Collection
     */
    public static function getContribution($shopChannelId)
    {
        $shopChannelId = intval($shopChannelId);

        $subSelect = DB::connection('master_business')->table('mak_shop_performance_hour')
            ->where('shop_channel_id', $shopChannelId)
            ->select([
                'date',
                DB::raw('sum(click) as total_click'),
                DB::raw('sum(order_gmv) as total_gmv'),
                DB::raw('sum(order_amount) as total_item_sold'),
                DB::raw('sum(impression) as total_impression'),
                DB::raw('sum(cost) as total_cost'),
            ])
            ->groupBy('shop_channel_id', 'date')
        ;

        $select = DB::connection('master_business')->table('mak_shop_performance_hour')
            ->where('shop_channel_id', $shopChannelId)
            ->join(DB::raw("({$subSelect->toSql()}) as grouped_date"), function (JoinClause $join) use ($subSelect) {
                $join->on('grouped_date.date', '=', 'mak_shop_performance_hour.date')
                ->addBinding($subSelect->getBindings());
            })
            ->select([
                'hour',
                DB::raw('avg(click / total_click) * 100 as avg_click'),
                DB::raw('avg(order_amount / total_item_sold) * 100 as avg_item_sold'),
                DB::raw('avg(order_gmv / total_gmv) * 100 as avg_gmv'),
                DB::raw('avg(cost / total_cost) * 100 as avg_cost'),
                DB::raw('avg(impression / total_impression) * 100 as avg_impression'),
            ])
            ->groupBy('hour')
        ;

        return $select->get();
    }

}