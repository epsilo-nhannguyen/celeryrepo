<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;


class ShopAdsOperator extends ModelBusiness
{
    /**
     * @var int
     * is =
     */
    const EQUALS = 1;

    /**
     * @var int
     * is >
     */
    const GREATER_THAN = 2;

    /**
     * @var int
     * is <
     */
    const LESS_THAN = 3;

    /**
     * @var int
     * is >=
     */
    const GREATER_THAN_OR_EQUAL_TO = 4;

    /**
     * @var int
     * is <=
     */
    const LESS_THAN_OR_EQUAL_TO = 5;

}