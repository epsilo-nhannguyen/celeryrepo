<?php

namespace App\Models\ModelBusiness;

use App\Library;
use Illuminate\Support\Facades\DB;
use Exception;

class ShopAds extends Library\ModelBusiness
{
    const TABLE_NAME = 'shop_ads';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_STATUS = 'status';
    const COL_AMOUNT_USED = 'amount_used';
    const COL_ADSID = 'adsid';
    const COL_IS_DAILY_BUDGET = 'is_daily_budget';
    const COL_IS_TOTAL_BUDGET = 'is_total_budget';
    const COL_IS_NO_LIMIT_BUDGET = 'is_no_limit_budget';
    const COL_BUDGET_AMOUNT = 'budget_amount';
    const COL_TIME_LINE_FROM = 'time_line_from';
    const COL_TIME_LINE_TO = 'time_line_to';
    const COL_UPDATED_BY = 'updated_by';
    const COL_CREATED_BY = 'created_by';
    const COL_CREATED_AT = 'created_at';
    const COL_UPDATED_AT = 'updated_at';
    const COL_RAW_DATA = 'data';

    // Shop Ads Status
    const ON_GOING = 'on-going';
    const PAUSE = 'pause';
    const ENDED = 'ended';
    const SCHEDULE = 'schedule';

    // Epsilo System
    const EPSILO_SYSTEM = 300;

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_ID;
    public $timestamps = false;

    /**
     * @param $shopChannelId
     * @param $shopAdsStatus
     * @param $adsId
     * @param $startTime
     * @param $endTime
     * @param $dailyQuota
     * @param $totalQuota
     * @param $fullJson
     * @return array
     */
    public static function buildDataInsert($shopChannelId, $shopAdsStatus, $adsId, $startTime, $endTime, $dailyQuota, $totalQuota, $fullJson)
    {
        $budgetAmount = 0;
        $isDaily = false;
        $isTotal = false;
        $isUnLimit = false;
        $status = self::ON_GOING;

        // Logic Budget
        if ($dailyQuota > 0) {
            $budgetAmount = $dailyQuota;
            $isDaily = true;
        } elseif ($totalQuota > 0) {
            $budgetAmount = $totalQuota;
            $isTotal = true;
        } else {
            $isUnLimit = true;
        }

        // Logic ShopAds status
        if($shopAdsStatus != 1) {
            $status = self::PAUSE;
        }
        if ($startTime > strtotime('now')) {
            $status = self::SCHEDULE;
        }
        if ($endTime != 0 && $endTime < strtotime('now')) {
            $status = self::ENDED;
        }

        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_STATUS => $status,
            self::COL_ADSID => $adsId,
            self::COL_IS_DAILY_BUDGET => $isDaily,
            self::COL_IS_TOTAL_BUDGET => $isTotal,
            self::COL_IS_NO_LIMIT_BUDGET => $isUnLimit,
            self::COL_BUDGET_AMOUNT => $budgetAmount,
            self::COL_TIME_LINE_FROM => $startTime,
            self::COL_TIME_LINE_TO => $endTime,
            self::COL_AMOUNT_USED => 0,
            self::COL_UPDATED_BY => self::EPSILO_SYSTEM,
            self::COL_CREATED_BY => self::EPSILO_SYSTEM,
            self::COL_CREATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_RAW_DATA => json_encode($fullJson)
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getAllShopActive()
    {
        $select = DB::connection('master_business')->table('shop_ads')
            ->join('shop_channel', 'shop_ads.shop_channel_id', '=', 'shop_channel.shop_channel_id')
            ->where('shop_channel.fk_config_active', 1)
        ;

        return $select->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getAllShopActiveWithOrganizationIdAndVentureId() {
        $select = DB::connection('master_business')->table('shop_ads')
            ->join('shop_channel', 'shop_ads.shop_channel_id', '=', 'shop_channel.shop_channel_id')
            ->join('shop_master', 'shop_master.shop_master_id', '=', 'shop_channel.fk_shop_master')
            ->where('shop_master.fk_config_active', 1)
        ;

        return $select->select(
                'shop_ads.id',
                'shop_channel.shop_channel_id',
                'shop_master.shop_master_id',
                'shop_master.fk_venture',
                'shop_master.fk_organization'
            )
            ->get();
    }

    /**
     * @param $shopAdsStatus
     * @param $adsId
     * @param $startTime
     * @param $endTime
     * @param $dailyQuota
     * @param $totalQuota
     * @param $fullJson
     * @return array
     */
    public static function buildDataUpdate($shopAdsStatus, $adsId, $startTime, $endTime, $dailyQuota, $totalQuota, $fullJson)
    {
        $budgetAmount = 0;
        $isDaily = false;
        $isTotal = false;
        $isUnLimit = false;
        $status = self::ON_GOING;

        // Logic Budget
        if ($dailyQuota > 0) {
            $budgetAmount = $dailyQuota;
            $isDaily = true;
        } elseif ($totalQuota > 0) {
            $budgetAmount = $totalQuota;
            $isTotal = true;
        } else {
            $isUnLimit = true;
        }

        // Logic ShopAds status
        if($shopAdsStatus != 1) {
            $status = self::PAUSE;
        }
        if ($startTime > strtotime('now')) {
            $status = self::SCHEDULE;
        }
        if ($endTime != 0 && $endTime < strtotime('now')) {
            $status = self::ENDED;
        }

        return [
            self::COL_STATUS => $status,
            self::COL_ADSID => $adsId,
            self::COL_IS_DAILY_BUDGET => $isDaily,
            self::COL_IS_TOTAL_BUDGET => $isTotal,
            self::COL_IS_NO_LIMIT_BUDGET => $isUnLimit,
            self::COL_BUDGET_AMOUNT => $budgetAmount,
            self::COL_TIME_LINE_FROM => $startTime,
            self::COL_TIME_LINE_TO => $endTime,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_RAW_DATA => json_encode($fullJson)
        ];
    }

    /**
     * Batch Insert
     * @param array $recordInsert
     * @return mixed
     * @throws Exception
     */
    public static function insertData($recordInsert)
    {
        try {
            return DB::table(self::TABLE_NAME)->insert($recordInsert);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $shopChannelId
     * @return mixed
     */
    public static function getByShopChannelId($shopChannelId)
    {
        return self::where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->select()
            ->offset(0)
            ->limit(1)
            ->first();
    }

    /**
     * @param $shopAdsId
     * @param $recordUpdate
     * @return mixed
     * @throws Exception
     */
    public static function updateById($shopAdsId, $recordUpdate)
    {
        try {
            return self::where(self::COL_ID, $shopAdsId)
                ->update($recordUpdate);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Update Case
     * @param string $sql
     * @return mixed
     */
    public static function updateCase($sql)
    {
        return DB::statement($sql);
    }

    /**
     * update Status By Id
     * @param $id
     * @param $status
     * @param $data
     * @param $adminId
     * @return int
     */
    public static function updateStatusById($id, $status, $data, $adminId)
    {
        return DB::connection('master_business')->table('shop_ads')
            ->where('id', $id)
            ->update([
                'status' => $status,
                'data' => $data,
                'updated_by' => $adminId,
                'updated_at' => Library\Common::getCurrentTimestamp()
            ])
        ;
    }
}