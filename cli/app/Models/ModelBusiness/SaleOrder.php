<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use App\Repository\RepositoryBusiness\DTO\DTOSaleOrder;
use App\Repository\RepositoryBusiness\DTO\DTOSaleOrderItem;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use function foo\func;

class SaleOrder extends ModelBusiness
{
    /**
     * @param $shopChannelId
     * @param $arrayOrderNumber
     * @return Collection
     */
    public static function findByArrayOrderNumber($shopChannelId, $arrayOrderNumber)
    {
        return DB::connection('master_business')->table('sale_order')
            ->where('fk_shop_channel', $shopChannelId)
            ->whereIn('sale_order_number', $arrayOrderNumber)
            ->select([
                'sale_order_number',
                'sale_order_id'
            ])
            ->get()
        ;
    }

    /**
     * @param Collection $collection
     * @return array
     */
    public static function saveMulti(Collection $collection)
    {
        $assocOrderNumberId = collect([]);
        $collection = $collection->filter(function ($item) {
            return $item instanceof DTOSaleOrder;
        });

        $groupedCollection = $collection->groupBy(function (DTOSaleOrder $item) {
            return $item->getShopChannelId();
        });

        foreach ($groupedCollection as $shopChannelId => $collectionOrder) {

            /** @var Collection $collectionOrder */
            $arrayOrderNumber = $collectionOrder->map(function (DTOSaleOrder $item) {
                return $item->getOrderNumber();
            });

            $orderNumberInDb = static::findByArrayOrderNumber($shopChannelId, $arrayOrderNumber)->pluck('sale_order_number');

            $collectionOrder = $collectionOrder->filter(function (DTOSaleOrder $item) use ($orderNumberInDb) {
                return ! $orderNumberInDb->contains($item->getOrderNumber());
            });

            $collectionInserted_order = collect([]);
            foreach ($collectionOrder as $order) {
                /** @var DTOSaleOrder $order */
                $collectionInserted_order->push([
                    'fk_shop_channel' => $order->getShopChannelId(),
                    'sale_order_number' => $order->getOrderNumber(),
                    'sale_order_voucher_code' => $order->getVoucherCode(),
                    'sale_order_voucher_amount' => $order->getVoucherAmount(),
                    'sale_order_status' => '',
                    'sale_order_address_city' => $order->getCustomerAddressCity(),
                    'sale_order_phone' => $order->getCustomerPhone(),
                    'sale_order_data' => $order->getRawData(),
                    'sale_order_system_created_at' => strtotime('now'),
                    'sale_order_order_created_at' => $order->getCreatedAt(),
                ]);
            }

            if ( ! $collectionInserted_order->count()) {
                continue;
            }

            DB::connection('master_business')->table('sale_order')->insert($collectionInserted_order->toArray());

            sleep(1);

            $orderNumberInserted = $collectionInserted_order->pluck('sale_order_number')->all();
            $assocOrderNumberId = static::findByArrayOrderNumber($shopChannelId, $orderNumberInserted)->pluck('sale_order_id', 'sale_order_number');

            $collectionInserted_orderItem = collect([]);

            foreach ($collectionOrder as $order) {
                /** @var DTOSaleOrder $order */
                foreach ($order->getArrayItem() as $orderItem) {
                    /** @var DTOSaleOrderItem $orderItem */
                    $collectionInserted_orderItem->push([
                        'sale_order_item_seller_sku' => $orderItem->getSku(),
                        'sale_order_item_name' => $orderItem->getName(),
                        'sale_order_item_item_price' => $orderItem->getPrice(),
                        'sale_order_item_item_created_at' => $order->getCreatedAt(),
                        'fk_sale_order' => $assocOrderNumberId[$order->getOrderNumber()] ?? null,
                        'sale_order_item_data' => $orderItem->getRawData(),
                        'sale_order_item_system_created_at' => strtotime('now'),
                    ]);
                }
            }

            if ( ! $collectionInserted_orderItem->count()) {
                continue;
            }

            DB::connection('master_business')->table('sale_order_item')->insert($collectionInserted_orderItem->toArray());
        }

        return $assocOrderNumberId->all();
    }
}