<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;


class ShopAdsSettingShop extends ModelBusiness
{

    /**
     * search By Shop Ads Id
     * @param int $shopAdsId
     * @param array $actionIdArray
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopAdsId($shopAdsId, $actionIdArray)
    {
        $today = date('Y-m-d');

        $model = DB::connection('master_business')->table('shop_ads_setting_shop')
            ->join('shop_ads_rule', 'shop_ads_rule.id','=','shop_ads_setting_shop.shop_ads_rule_id')
            ->where('shop_ads_setting_shop.shop_ads_id', $shopAdsId)
            ->whereIn('shop_ads_rule.shop_ads_action_id', $actionIdArray)
            ->where('shop_ads_rule.validity_from', '<=', $today)
            ->where('shop_ads_rule.validity_to', '>=', $today)
            ->select(
                'shop_ads_rule.id',
                'shop_ads_rule.extend',
                'shop_ads_rule.shop_ads_action_id'
            )
            ->orderBy('shop_ads_rule.id')
            ->distinct()
        ;
        return $model->get();
    }

}