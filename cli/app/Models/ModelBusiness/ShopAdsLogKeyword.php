<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;
use App\Library;


class ShopAdsLogKeyword extends ModelBusiness
{

    /**
     * build Data Insert
     * @param $shopAdsKeywordId
     * @param $actionId
     * @param $createdBy
     * @param $ruleId
     * @param $checkpoint
     * @return array
     */
    public static function buildDataInsert($shopAdsKeywordId, $actionId, $createdBy, $ruleId = null, $checkpoint = null)
    {
        return [
            'shop_ads_keyword_id' => $shopAdsKeywordId,
            'shop_ads_action_id' => $actionId,
            'created_at' => Library\Common::getCurrentTimestamp(),
            'created_by' => $createdBy,
            'shop_ads_rule_id' => $ruleId,
            'checkpoint' => $checkpoint
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('shop_ads_log_keyword')->insert($recordArray);
    }

}