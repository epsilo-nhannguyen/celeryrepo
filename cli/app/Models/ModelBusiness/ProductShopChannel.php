<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Library;


class ProductShopChannel extends ModelBusiness
{

    /**
     * searchByShopChannelId
     * @param int $shopChannelId
     * @param array $itemIdArray
     * @return Collection
     */
    public static function searchByShopChannelId($shopChannelId, $itemIdArray = null)
    {
        $query = DB::connection('master_business')->table('product_shop_channel')
            ->where('fk_shop_channel', $shopChannelId)
            ->when($itemIdArray, function ($select) use ($itemIdArray) {
                return $select->whereIn('product_shop_channel_item_id', $itemIdArray);
            })
            ->select([
                'product_shop_channel_id',
                'product_shop_channel_item_id',
                'product_shop_channel_shop_sku'
            ])
        ;

        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('product_shop_channel')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('product_shop_channel')
            ->where('product_shop_channel_id', $id)
            ->update($record)
            ;
        ;
    }

}