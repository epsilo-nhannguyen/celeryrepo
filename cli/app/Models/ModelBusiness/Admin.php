<?php

namespace App\Models\ModelBusiness;

use App\Application\Constant\Cache;
use App\Application\Constant\CacheService;
use App\Application\ModelMaster;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Library\ModelBusiness;

class Admin extends ModelBusiness
{
    const TABLE_NAME = 'admin';

    const COL_ADMIN_ID = 'admin_id';
    const COL_ADMIN_EMAIL = 'admin_email';
    const COL_ADMIN_PASSWORD = 'admin_password';
    const COL_ADMIN_FULLNAME = 'admin_fullname';
    const COL_ADMIN_PHONE = 'admin_phone';
    const COL_ADMIN_CREATED_AT = 'admin_created_at';
    const COL_ADMIN_LAST_LOGIN = 'admin_last_login';
    const COL_ADMIN_LAST_LOGIN_IP = 'admin_last_login_ip';
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';
    const COL_FK_ADMIN_ROLE = 'fk_admin_role';
    const COL_FK_ORGANIZATION = 'fk_organization';
    const COL_ADMIN_TOKEN = 'admin_token';
    const COL_ADMIN_LOGIN_AS = 'admin_login_as';
    const COL_FK_ADMIN_MODULE = 'fk_admin_module';
    const COL_FK_ADMIN_TYPE = 'fk_admin_type';
    const COL_ADMIN_CREATED_BY = 'admin_created_by';
    const COL_ADMIN_UPDATED_AT = 'admin_updated_at';
    const COL_ADMIN_UPDATED_BY = 'admin_updated_by';
    const COL_ADMIN_LAST_VENTURE_WORKING = 'admin_last_venture_working';
    const COL_ADMIN_LAST_WAREHOUSE_WORKING = 'admin_last_warehouse_working';
    const COL_ADMIN_HISTORY_LAST_LOGIN = 'admin_history_last_login';
    const COL_ADMIN_URL_PATH = 'admin_url_path';
    const COL_FK_COUNTRY = 'fk_country';
    const COL_ADMIN_LAST_MODIFY_PASSWORD = 'admin_last_modify_password';
    const COL_FK_TABLEAU_CLIENT = 'fk_tableau_client';
    const COL_ADMIN_SHOW_CURRENCY_FOR_VENTURE = 'admin_show_currency_for_venture';
    const COL_IS_MEMBER = 'is_member';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = self::COL_ADMIN_ID;


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COL_ADMIN_EMAIL,
        self::COL_ADMIN_FULLNAME,
        self::COL_ADMIN_PHONE,
        self::COL_ADMIN_CREATED_AT,
        self::COL_ADMIN_LAST_LOGIN,
        self::COL_ADMIN_LAST_LOGIN_IP,
        self::COL_FK_CONFIG_ACTIVE,
        self::COL_FK_ADMIN_ROLE,
        self::COL_FK_ORGANIZATION,
        self::COL_ADMIN_TOKEN,
        self::COL_ADMIN_LOGIN_AS,
        self::COL_FK_ADMIN_MODULE,
        self::COL_FK_ADMIN_TYPE,
        self::COL_ADMIN_CREATED_BY,
        self::COL_ADMIN_UPDATED_AT,
        self::COL_ADMIN_UPDATED_BY,
        self::COL_ADMIN_LAST_VENTURE_WORKING,
        self::COL_ADMIN_LAST_WAREHOUSE_WORKING,
        self::COL_ADMIN_HISTORY_LAST_LOGIN,
        self::COL_ADMIN_URL_PATH,
        self::COL_FK_COUNTRY,
        self::COL_ADMIN_LAST_MODIFY_PASSWORD,
        self::COL_FK_TABLEAU_CLIENT,
        self::COL_ADMIN_SHOW_CURRENCY_FOR_VENTURE,
        self::COL_IS_MEMBER
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        self::COL_ADMIN_PASSWORD
    ];

    /**
     * @param $email
     * @return mixed
     */
    public static function isEmailValid($email)
    {
        $model = DB::table(Admin::TABLE_NAME)
            ->join(Organization::TABLE_NAME, Admin::TABLE_NAME . '.' . Admin::COL_FK_ORGANIZATION,
                '=', Organization::TABLE_NAME . '.' . Organization::COL_ORGANIZATION_ID)
            ->select(
                [
                    Admin::TABLE_NAME . '.*',
                    Organization::TABLE_NAME . '.' . Organization::COL_ORGANIZATION_DOMAIN,
                    Organization::TABLE_NAME . '.' . Organization::COL_ORGANIZATION_NAME
                ]
            )
            ->where(
                sprintf(
                    '%s.%s',
                    Admin::TABLE_NAME,
                    Admin::COL_FK_CONFIG_ACTIVE
                ), 1
            )
            ->where(
                sprintf(
                    '%s.%s',
                    Organization::TABLE_NAME,
                    Organization::COL_FK_CONFIG_ACTIVE
                ), 1
            )
            ->where(
                sprintf(
                    '%s.%s',
                    Admin::TABLE_NAME,
                    Admin::COL_ADMIN_EMAIL
                ), $email
            )
            ->get()
            ->first();

        return $model;
    }

    /**
     * @param string $email
     * @param string $ipAddress
     * @return int
     */
    public static function updateIpLastLogin($email, $ipAddress)
    {
        $now = time();

        return DB::table(Admin::TABLE_NAME)
            ->where(Admin::COL_ADMIN_EMAIL, $email)
            ->update([
                Admin::COL_ADMIN_LAST_LOGIN => $now,
                Admin::COL_ADMIN_LAST_LOGIN_IP => $ipAddress
            ]);
    }

    /**
     * @param string $email
     * @return mixed
     */
    public static function getByEmail($email)
    {
        return self::where(self::COL_ADMIN_EMAIL, $email)->first();
    }

    /**
     * @param int|array $id
     * @param array $params
     * @return bool
     */
    public static function updateData($id, $params)
    {
        $model = self::find($id);
        foreach ($params as $key => $val) {
            $model->$key = $val;
        }
        return $model->save();
    }

    /**
     * @param array $inputs
     * @return bool
     */
    public static function insert($inputs)
    {
        $model = new self();
        foreach ($inputs as $key => $val) {
            $model->$key = $val;
        }
        return $model->save();
    }
}
