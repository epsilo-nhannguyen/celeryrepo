<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Library;


class TokoAdsCampaign extends ModelBusiness
{
    protected $table = 'toko_ads_campaign';
    const TABLE_NAME = 'toko_ads_campaign';
    const COL_ID = 'id';
    const COL_CHANNEL_CAMPAIGN_ID = 'channel_campaign_id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_CONFIG_ACTIVE_ID = 'config_active_id';
    const COL_CODE = 'code';
    const COL_NAME = 'name';
    const COL_DAILY_BUDGET = 'daily_budget';
    const COL_MAX_COST = 'max_cost';
    const COL_FROM = 'from';
    const COL_TO = 'to';
    const COL_CREATED_AT = 'created_at';
    const COL_CREATED_BY = 'created_by';
    const COL_UPDATED_AT = 'updated_at';
    const COL_UPDATED_BY = 'updated_by';

    /**
     * @var string
     */
    const CAMPAIGN_AUTO_ADS = 'Auto Ads';

    /**
     * @var int
     */
    const IS_AUTO = 1;
    const IS_NORMAL = 0;

    /**
     * searchByShopChannelId
     * @param int $shopChannelId
     * @param array $channelCampaignIdArray
     * @return Collection
     */
    public static function searchByShopChannelId($shopChannelId, $channelCampaignIdArray = null)
    {
        $query = DB::connection('master_business')->table('toko_ads_campaign')
            ->where('shop_channel_id', $shopChannelId)
            ->when($channelCampaignIdArray, function ($select) use ($channelCampaignIdArray) {
                return $select->whereIn('channel_campaign_id', $channelCampaignIdArray);
            })
            ->select([
                'id',
                'channel_campaign_id',
                'max_cost',
            ])
        ;

        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('toko_ads_campaign')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('toko_ads_campaign')
            ->where('id', $id)
            ->update($record)
            ;
    }

    /**
     * search Available Timeline To Update
     * @param int $shopChannelId
     * @param string $date
     * @return Collection
     */
    public static function searchAvailableTimelineToUpdate($shopChannelId, $date)
    {
        $query = DB::connection('master_business')->table('toko_ads_campaign')
            ->where('shop_channel_id', $shopChannelId)
            ->whereNotNull('from')
            ->select(
                'id',
                'channel_campaign_id',
                'is_auto',
                'config_active_id',
                'name',
                'daily_budget',
                'max_cost',
                'updated_by',
                DB::raw(
                    sprintf(
                        "(%s <= '%s') and if (%s is not null, '%s' <= %s, 1) as `is_available`",
                        'toko_ads_campaign.from',
                        $date,
                        'toko_ads_campaign.to',
                        $date,
                        'toko_ads_campaign.to'
                    )
                )
            )
        ;

        return $query->get();
    }

    /**
     * @param int|null $channelCampaignId
     * @param int $shopChannelId
     * @param int $configActiveId
     * @param string $code
     * @param string $name
     * @param float $dailyBudget
     * @param float $maxCost
     * @param string $from
     * @param string $to
     * @param int $isAuto
     * @param int $createdBy
     * @return int
     */
    public static function insertGetId($channelCampaignId, $shopChannelId, $configActiveId, $code, $name, $dailyBudget, $maxCost, $from, $to, $isAuto, $createdBy)
    {
        return DB::connection('master_business')->table('toko_ads_campaign')->insertGetId([
            'channel_campaign_id' => $channelCampaignId,
            'shop_channel_id' => $shopChannelId,
            'config_active_id' => $configActiveId,
            'code' => $code,
            'name' => $name,
            'daily_budget' => $dailyBudget,
            'max_cost' => $maxCost,
            'from' => $from,
            'to' => $to,
            'is_auto' => $isAuto,
            'created_by' => $createdBy,
            'created_at' => Library\Common::getCurrentTimestamp()
        ]);
    }

    /**
     * @param $campaignId
     * @param $value
     */
    public static function queryUpdateStatus($campaignId, $value)
    {
        $campaign = TokoAdsCampaign::where('id', $campaignId)->first();
        $campaign->config_active_id = $value;
        return $campaign->save();
    }

    public static function queryUpdateInfo($campaignId, $dailyBudget, $maxCost, $campaignName)
    {
        $campaign = TokoAdsCampaign::where('id', $campaignId)->first();
        $campaign->daily_budget = $dailyBudget;
        $campaign->max_cost = $maxCost;
        $campaign->name = $campaignName;
        return $campaign->save();
    }

    public static function queryUpdateTimeLineById($id, $from, $to)
    {
        $campaign = TokoAdsCampaign::where('id', $id)->first();
        $campaign->from = $from;
        $campaign->to = $to;
        return $campaign->save();
    }



    /**
     * Generate by in organization
     * @param int $organizationId
     * @return string
     */
    public static function generateByInOrganization($organizationId)
    {
        $milliseconds = round(microtime(true) * 1000);
        Library\Common::convert10SystemTo36System($milliseconds, $data);
        Library\Common::convert10SystemTo36System($organizationId, $organizationId36);

        return 'CAMP'.strtoupper($data).str_pad($organizationId36, 6, 0, STR_PAD_LEFT);
    }

    /**
     * get By Id
     * @param int $id
     * @return mixed
     */
    public static function getById($id)
    {
        $query = DB::connection('master_business')->table('toko_ads_campaign')
            ->where('toko_ads_campaign.id', $id)
        ;

        return $query->get()->first();
    }

    /**
     * @param $from
     * @param $to
     * @param $status
     * @param $typeSort
     * @param $columnName
     * @param $shopChannelId
     * @param $campaignName
     * @param $allShopChannelAllow
     * @return mixed
     */
    public static function getCampaignManagement($from, $to, $status, $typeSort, $columnName , $shopChannelId, $campaignName, $allShopChannelAllow)
    {
        $query = DB::table(ShopMaster::TABLE_NAME.' as sm')
            ->join(ShopChannel::TABLE_NAME.' as sc', 'sc.fk_shop_master','=','sm.shop_master_id')
            ->join(self::TABLE_NAME.' as tac', 'tac.shop_channel_id', '=', 'sc.shop_channel_id')
            ->join(Channel::TABLE_NAME.' as c', 'c.channel_id', '=', 'sc.fk_channel')
            ->join(Venture::TABLE_NAME.' as v', 'v.venture_id','=','sm.fk_venture')
            ->leftJoin(TokoAdsCampaignPerformance::TABLE_NAME.' as tacp', 'tacp.campaign_id', '=', 'tac.id');

        if($campaignName){
            $query = $query->where('tac.name', 'like', '%'.$campaignName.'%');
        }

        if($shopChannelId && $shopChannelId > 0){
            $query = $query->where('tac.'.self::COL_SHOP_CHANNEL_ID, $shopChannelId);
        }

        if($status != -1){
            $query = $query->where('tac.'.self::COL_CONFIG_ACTIVE_ID, $status);
        }

        if($typeSort && $columnName){
            $query = $query->orderBy($columnName, $typeSort);
        }

        if($from && $to){
            $colImpression = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.impression, 0)) as impression";
            $colClick = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.click, 0)) as click";
            $colItemSold = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.item_sold, 0)) as sold";
            $colCost = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.cost, 0)) as cost";
            $colGmv = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.gmv, 0)) as gmv";
            $colCTR = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.ctr, 0)) as ctr";
            $colAllSold = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.all_sold, 0)) as all_sold";
            $colAverage = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.average, 0)) as average";
            $colRank = "sum(if(`tacp`.`channel_create_date` between '{$from}' and '{$to}', tacp.rank, 0)) as rank";
        } else {
            $colImpression = "sum(tacp.impression) as impression";
            $colClick = "sum(tacp.click) as click";
            $colItemSold = "sum(tacp.item_sold) as sold";
            $colCost = "sum(tacp.cost) as cost";
            $colGmv = "sum(tacp.gmv) as gmv";
            $colCTR = "sum(tacp.ctr) as ctr";
            $colAllSold = "sum(tacp.all_sold) as all_sold";
            $colAverage = "sum(tacp.average) as average";
            $colRank = "sum(tacp.rank) as rank";
        }

        $query = $query->where('v.venture_code', 'ID')
            ->where('c.channel_code', 'TOKOPEDIA');
//                       ->whereIn('sc.shop_channel_id', $allShopChannelAllow);
        $query = $query->groupBy(['tac.id']);
        $queryTotalSku = "(select count(1) from toko_ads_sku as tas where tas.campaign_id=tac.id) as total_sku";
        $queryTotalKeyword = "(select count(1) from toko_ads_keyword as tak where tak.campaign_id=tac.id) as total_keyword";

        $query = $query->select([
            'tac.id',
            'tac.code',
            'tac.daily_budget',
            'tac.from',
            'tac.to',
            'tac.config_active_id',
            'sc.shop_channel_name',
            'sc.shop_channel_id',
            'sc.fk_channel',
            'tac.max_cost',
            DB::raw('concat(tac.code,'. '"-"'.', tac.name) as campaign_name'),
            DB::raw($queryTotalSku),
            DB::raw($queryTotalKeyword),
            DB::raw($colImpression),
            DB::raw($colClick),
            DB::raw($colItemSold),
            DB::raw($colCost),
            DB::raw($colGmv),
            DB::raw($colCTR),
            DB::raw($colAllSold),
            DB::raw($colAverage),
            DB::raw($colRank),
        ]);

        return $query;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getCampaignById($id)
    {
        return TokoAdsCampaign::where('id', $id)->first();
    }

    /**
     * search By Shop Channel Id Array
     * @param array $shopChannelIdArray
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdArray($shopChannelIdArray)
    {
        $query = DB::connection('master_business')->table('toko_ads_campaign')
            ->join('shop_channel', 'shop_channel.shop_channel_id','=','toko_ads_campaign.shop_channel_id')
            ->whereIn('toko_ads_campaign.shop_channel_id', $shopChannelIdArray)
            ->select([
                'toko_ads_campaign.id',
                'toko_ads_campaign.code',
                'toko_ads_campaign.name',
                'toko_ads_campaign.config_active_id',
                'shop_channel.shop_channel_id',
                'shop_channel.shop_channel_name',
            ])
        ;

        return $query->get();
    }

    /**
     * get Campaign Info
     * @param $shopChannelId
     * @param $search
     * @param $column
     * @return mixed
     */
    public static function getCampaignInfo($shopChannelId, $search, $column)
    {
        $query = DB::connection('master_business')->table('toko_ads_campaign')
            ->join('shop_channel', 'shop_channel.shop_channel_id','=','toko_ads_campaign.shop_channel_id')
            ->join('channel', 'channel.channel_id','=','shop_channel.fk_channel')
            ->where('toko_ads_campaign.shop_channel_id', $shopChannelId)
            ->where('toko_ads_campaign.is_auto', self::IS_NORMAL)
            ->when($search, function ($select) use ($search, $column) {
                $select->when($column, function ($select2) use ($search, $column) {
                    return $select2->where($column, $search);
                }, function ($select2) use ($search, $column) {
                    return $select2->where(function ($select3) use ($search) {
                        $select3->orWhere('toko_ads_campaign.code', $search)
                            ->orWhere('toko_ads_campaign.name', $search);
                    });
                });
            })
            ->select([
                'toko_ads_campaign.id',
                'toko_ads_campaign.shop_channel_id',
                'toko_ads_campaign.code',
                'toko_ads_campaign.name',
                'toko_ads_campaign.daily_budget',
                'toko_ads_campaign.from',
                'toko_ads_campaign.to',
                'channel.fk_venture',
            ])
        ;

        return $query->get()->first();
    }

    public static function getCampaignIdByChannelCampaignId($channelCampaignId)
    {
        return DB::connection('master_business')->table('toko_ads_campaign')
            ->where('channel_campaign_id', $channelCampaignId)->pluck('id')->first();
    }

    public static function getIsAutoByChannel($channelCampaignId)
    {
        return DB::connection('master_business')->table('toko_ads_campaign')
            ->where('channel_campaign_id', $channelCampaignId)->select(['is_auto'])->pluck('is_auto')->first();
    }

}
