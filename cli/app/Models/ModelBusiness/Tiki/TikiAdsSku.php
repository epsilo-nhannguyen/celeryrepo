<?php

namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TikiAdsSku extends ModelBusiness
{
    protected $table = 'tiki_ads_sku';


    /**
     * @param int $shopChannelId
     * @return Collection
     */
    public static function getAllSkuByShopChannelId($shopChannelId)
    {
        $query = DB::connection('master_business')->table('tiki_ads_sku')
            ->where('shop_channel_id', $shopChannelId);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_sku')->insert($recordArray);
    }
}
