<?php

namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TikiAdsGroupSkuRelationship extends ModelBusiness
{
    protected $table = 'tiki_ads_group_ads_sku';

    /**
     * @param int[] $arrayDbSKUAdsId
     * @return Collection
     */
    public static function getAllGroupAdsSKUAdsByDbSKUId($arrayDbSKUAdsId)
    {
        $query = DB::connection('master_business')->table('tiki_ads_group_ads_sku')
            ->whereIn('sku_id', $arrayDbSKUAdsId)
            ->select([
                'id',
                'channel_sku_creative_id',
                'status'
            ]);
        return $query->get();
    }

    /**
     * @param $arrayDbSKUAdsId
     * @return Collection
     */
    public static function getAllGroupAdsSKUAdsForPerformanceByDbSKUId($arrayDbSKUAdsId) {
        $query = DB::connection('master_business')->table('tiki_ads_group_ads_sku')
            ->join('tiki_ads_group_ads', 'tiki_ads_group_ads.id', '=' , 'tiki_ads_group_ads_sku.group_ads_id')
            ->join('tiki_ads_sku', 'tiki_ads_sku.id', '=' , 'tiki_ads_group_ads_sku.sku_id')
            ->whereIn('sku_id', $arrayDbSKUAdsId)
            ->select([
                'tiki_ads_group_ads_sku.id',
                'tiki_ads_group_ads.channel_group_ads_id',
                'tiki_ads_sku.channel_sku_id'
            ]);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads_sku')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads_sku')
            ->where('id', $id)
            ->update($record);
    }
}