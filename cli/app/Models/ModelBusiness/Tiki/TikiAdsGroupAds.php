<?php

namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TikiAdsGroupAds extends ModelBusiness
{
    protected $table = 'tiki_ads_group_ads';

    /**
     * @param $shopChannelId
     * @return Collection
     */
    public static function getAllGroupAdsByShopChannelId($shopChannelId)
    {
        $query = DB::connection('master_business')->table('tiki_ads_group_ads')
            ->where('shop_channel_id', $shopChannelId)
            ->select([
                'id',
                'campaign_id',
                'channel_campaign_id',
                'channel_group_ads_id',
                'status',
                'name',
                'price'
            ]);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads')
            ->where('id', $id)
            ->update($record);
    }
}
