<?php
namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;

class TikiAdsGroupAdsPerformance extends ModelBusiness
{
    protected $table = 'tiki_ads_group_ads_performance';

    /**
     * @param array $arrayChannelGroupAdsId
     * @param date|string $date
     * @return Collection
     */
    public static function getAllPerformanceByChannelGroupAdsIdAndDate($arrayChannelGroupAdsId, $date)
    {
        $query = DB::connection('master_business')->table('tiki_ads_group_ads_performance')
            ->whereIn('channel_group_ads_id', $arrayChannelGroupAdsId)
            ->where('channel_create_date', $date);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads_performance')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads_performance')
            ->where('id', $id)
            ->update($record);
    }
}