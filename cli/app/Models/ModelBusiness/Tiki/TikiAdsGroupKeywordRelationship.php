<?php
namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TikiAdsGroupKeywordRelationship extends ModelBusiness
{
    protected $table = 'tiki_ads_group_ads_keyword';

    /**
     * @param array $arrayKeywordId (Epsilo Tiki keyword ads id)
     * @return Collection
     */
    public static function getAllGroupAdsKeywordAdsByDbKeywordId($arrayKeywordId)
    {
        $query = DB::connection('master_business')->table('tiki_ads_group_ads_keyword')
            ->join('tiki_ads_group_ads', 'tiki_ads_group_ads.id', '=' , 'tiki_ads_group_ads_keyword.group_ads_id')
            ->join('tiki_ads_keyword', 'tiki_ads_keyword.id', '=' , 'tiki_ads_group_ads_keyword.keyword_id')
            ->whereIn('keyword_id', $arrayKeywordId)
            ->select([
                'tiki_ads_group_ads_keyword.id',
                'tiki_ads_group_ads.channel_group_ads_id',
                'tiki_ads_keyword.channel_keyword_id',
                'tiki_ads_group_ads_keyword.status',
                'tiki_ads_group_ads_keyword.bidding_price',
                'tiki_ads_group_ads_keyword.match_type'
            ]);
        return $query->get();
    }

    /**
     * @param $arrayDbGroupAdsId
     * @return Collection
     */
    public static function getAllGroupAdsKeywordAdsByDbGroupAdsId($arrayDbGroupAdsId)
    {
        $query = DB::connection('master_business')->table('tiki_ads_group_ads_keyword')
            ->join('tiki_ads_group_ads', 'tiki_ads_group_ads.id', '=' , 'tiki_ads_group_ads_keyword.group_ads_id')
            ->join('tiki_ads_keyword', 'tiki_ads_keyword.id', '=' , 'tiki_ads_group_ads_keyword.keyword_id')
            ->whereIn('group_ads_id', $arrayDbGroupAdsId)
            ->select([
                'tiki_ads_group_ads_keyword.id',
                'tiki_ads_group_ads.channel_group_ads_id',
                'tiki_ads_keyword.channel_keyword_id'
            ]);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads_keyword')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads_keyword')
            ->where('id', $id)
            ->update($record);
    }
}