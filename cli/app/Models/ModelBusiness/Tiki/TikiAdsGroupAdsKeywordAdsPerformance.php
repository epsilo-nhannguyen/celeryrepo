<?php
namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TikiAdsGroupAdsKeywordAdsPerformance extends ModelBusiness
{
    protected $table = 'tiki_ads_group_ads_keyword_performance';

    /**
     * @param $arrayChannelGroupAds
     * @param date|string $date
     * @return Collection
     */
    public static function getAllPerformanceByChannelGroupAdsAndDate($arrayChannelGroupAds, $date)
    {
        $query = DB::connection('master_business')->table('tiki_ads_group_ads_keyword_performance')
            ->whereIn('channel_group_ads_id', $arrayChannelGroupAds)
            ->where('channel_create_date', $date);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads_keyword_performance')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('tiki_ads_group_ads_keyword_performance')
            ->where('id', $id)
            ->update($record);
    }
}