<?php

namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;
use App\Models;
use App\Library;

class TikiAdsKeyword extends ModelBusiness
{
    protected $table = 'tiki_ads_keyword';

    /**
     * @param int $shopChannelId
     * @return Collection
     */
    public static function getAllKeywordByShopChannelId($shopChannelId)
    {
        $query = DB::connection('master_business')->table('tiki_ads_keyword')
            ->where('shop_channel_id', $shopChannelId);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_keyword')->insert($recordArray);
    }
}
