<?php

namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TikiAdsCampaign extends ModelBusiness
{
    protected $table = 'tiki_ads_campaign';

    /**
     * get By Id
     * @param int $id
     * @return mixed
     */
    public static function getById($id)
    {
        $query = DB::connection('master_business')->table('tiki_ads_campaign')
            ->where('tiki_ads_campaign.id', $id);

        return $query->get()->first();
    }

    /**
     * search By Shop Channel Id Array
     * @param array $shopChannelIdArray
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopChannelIdArray($shopChannelIdArray)
    {
        $query = DB::connection('master_business')->table('tiki_ads_campaign')
            ->whereIn('shop_channel_id', $shopChannelIdArray)
            ->select([
                'id',
                'name',
                'status',
                'shop_channel_id',
                'status',
            ]);

        return $query->get();
    }

    /**
     * @param $shopChannelId
     * @return Collection
     */
    public static function getAllCampaignByShopChannelId($shopChannelId)
    {
        $query = DB::connection('master_business')->table('tiki_ads_campaign')
            ->where('shop_channel_id', $shopChannelId)
            ->select([
                'id',
                'channel_campaign_id',
                'name',
                'status',
                'daily_budget',
                'from',
                'to'
            ]);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_campaign')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('tiki_ads_campaign')
            ->where('id', $id)
            ->update($record);
    }
}
