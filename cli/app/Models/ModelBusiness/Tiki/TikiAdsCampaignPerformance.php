<?php

namespace App\Models\ModelBusiness\Tiki;

use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TikiAdsCampaignPerformance extends ModelBusiness
{
    protected $table = 'tiki_ads_campaign_performance';

    /**
     * @param array $arrayChannelCampaignId
     * @param date|string $date
     * @return Collection
     */
    public static function getAllPerformanceByChannelCampaignIdAndDate($arrayChannelCampaignId, $date)
    {
        $query = DB::connection('master_business')->table('tiki_ads_campaign_performance')
            ->whereIn('channel_campaign_id', $arrayChannelCampaignId)
            ->where('channel_create_date', $date);
        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('tiki_ads_campaign_performance')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('tiki_ads_campaign_performance')
            ->where('id', $id)
            ->update($record);
    }
}
