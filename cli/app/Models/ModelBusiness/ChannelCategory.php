<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 14:28
 */


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Library;


class ChannelCategory extends ModelBusiness
{
    /**
     * search By Organization Id And Channel Id
     * @param int $organizationId
     * @param int $channelId
     * @param mixed $codeArray
     * @return Collection
     */
    public static function searchByOrganizationIdAndChannelId($organizationId, $channelId, $codeArray = null)
    {
        $query = DB::connection('master_business')->table('channel_category')
            ->where('fk_organization', $organizationId)
            ->where('fk_channel', $channelId)
            ->when($codeArray, function ($select) use ($codeArray) {
                return $select->whereIn('channel_category_code', $codeArray);
            })
            ->select([
                'channel_category_id',
                'channel_category_code',
            ])
        ;

        return $query->get();
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return bool
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('channel_category')->insert($recordArray);
    }


}