<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;


class ShopAdsObjective extends ModelBusiness
{

    /**
     * @var int
     */
    const SHOP_KEYWORD = 1;

    /**
     * @var int
     */
    const SHOP = 2;

}