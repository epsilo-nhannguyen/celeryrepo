<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;
use App\Library;


class TokoAdsSkuPerformance extends ModelBusiness
{
    protected $table = 'toko_ads_sku_performance';
    public static function isSkuPerformanceFromChannelExists($channelCampaignId, $channelCreateDate)
    {
        $sku = DB::connection('master_business')->table('toko_ads_sku_performance')
            ->where('channel_campaign_id', $channelCampaignId)
            ->where('channel_create_date', $channelCreateDate)
            ->first();
        return $sku ? true : false;
    }

    public static function updateIfExistSkuPerformanceFromChannel($channelCampaignId, $channelCreateDate, $data)
    {
        $skus = TokoAdsSkuPerformance::where('channel_campaign_id', $channelCampaignId)
            ->where('channel_create_date', $channelCreateDate);

        return $skus->update($data);
    }

    public static function addNewIfNonExistsSkuPerformanceFromChannel($data)
    {
        return DB::connection('master_business')->table('toko_ads_sku_performance')->insert($data);
    }
}
