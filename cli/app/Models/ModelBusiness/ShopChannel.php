<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 14:28
 */


namespace App\Models\ModelBusiness;


use App\Application\Constant\ConfigActive as Constant_ConfigActive;
use App\Library;
use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ShopChannel extends ModelBusiness
{
    #region TABLE shop_channel
    /**
     * @var string
     */
    const TABLE_NAME = 'shop_channel';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_NAME = 'shop_channel_name';

    /**
     * @var string
     */
    const COL_FK_SHOP_MASTER = 'fk_shop_master';

    /**
     * @var string
     */
    const COL_FK_CHANNEL = 'fk_channel';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_API_CREDENTIAL = 'shop_channel_api_credential';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_API_CREDENTIAL_UPDATED_AT = 'shop_channel_api_credential_updated_at';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_LINKED = 'shop_channel_linked';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_INIT = 'shop_channel_init';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_LAST_TIME_PULL_PRODUCT = 'shop_channel_last_time_pull_product';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_LAST_TIME_PULL_ORDER = 'shop_channel_last_time_pull_order';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_CREATED_AT = 'shop_channel_created_at';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_UPDATED_AT = 'shop_channel_updated_at';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_CREATED_BY = 'shop_channel_created_by';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_UPDATED_BY = 'shop_channel_updated_by';

    /**
     * @var string
     */
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_IS_API_VALID = 'shop_channel_is_api_valid';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_IS_CREDENTIAL_VALID = 'shop_channel_is_credential_valid';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_IS_API_VALID_UPDATED_AT = 'shop_channel_is_api_valid_updated_at';

    /**
     * @var string
     */
    const COL_SHOP_CHANNEL_IS_CREDENTIAL_VALID_UPDATED_AT = 'shop_channel_is_credential_valid_updated_at';
    #endregion

    /**
     * get By Id
     * @param int $id
     * @return mixed
     */
    public static function getById($id)
    {
        $query = DB::connection('master_business')->table('shop_channel')
            ->where('shop_channel_id', $id)
        ;

        return $query->get()->first();
    }

    /**
     * search Shop Active By Channel Code
     * @param $channelCode
     * @return Collection
     */
    public static function searchShopActive_byChannelCode($channelCode)
    {
        $query = DB::connection('master_business')->table('shop_channel')
            ->join('channel', 'fk_channel', '=', 'channel_id')
            ->join('venture', 'fk_venture', '=', 'venture_id')
            ->where('shop_channel.fk_config_active', 1)
            ->where('shop_channel.shop_channel_linked', 1)
            ->where('channel_code', $channelCode)
            ->select([
                'fk_shop_master',
                'fk_channel',
                'shop_channel_id',
                'shop_channel_api_credential',
                'venture_timezone',
            ])
        ;

        return $query->get();
    }

    /**
     * @param string $channelCode
     * @param int $ventureId
     * @return \Illuminate\Database\Query\Builder
     */
    public static function getByChannelAndVenture($channelCode, $ventureId)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                Channel::TABLE_NAME,
                Channel::TABLE_NAME . '.' . Channel::COL_CHANNEL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_FK_CHANNEL
            )
            ->join(
                Venture::TABLE_NAME,
                Venture::TABLE_NAME . '.' . Venture::COL_VENTURE_ID,
                '=',
                Channel::TABLE_NAME . '.' . Channel::COL_FK_VENTURE
            )
            ->join(
                ShopChannelUser::TABLE_NAME,
                ShopChannelUser::TABLE_NAME . '.' . ShopChannelUser::COL_FK_SHOP_CHANNEL,
                '=',
                self::TABLE_NAME . '.' . self::COL_SHOP_CHANNEL_ID
            )
            ->join(
                Admin::TABLE_NAME,
                Admin::TABLE_NAME . '.' . Admin::COL_ADMIN_ID,
                '=',
                ShopChannelUser::TABLE_NAME . '.' . ShopChannelUser::COL_FK_USER
            )
            ->where(
                Channel::TABLE_NAME . '.' . Channel::COL_CHANNEL_CODE,
                '=',
                $channelCode
            )
            ->where(
                Venture::TABLE_NAME . '.' . Venture::COL_VENTURE_ID,
                '=',
                $ventureId
            )
            ->get();
//            ->where(
//                self::TABLE_NAME . '.' . self::COL_SHOP_CHANNEL_API_CREDENTIAL,
//                'like',
//                '%' . $username . '%'
//            )
//            ->first();
    }


    /**
     * @param int $shopChannelId
     * @param int $channelId
     * @return string
     */
    public static function updateShopChannelLinked($shopChannelId, $channelId)
    {
        $shopChannelId = intval($shopChannelId);
        $channelId = intval($channelId);


        return DB::connection('master_business')->table(self::TABLE_NAME)
            ->where(self::COL_FK_CHANNEL, $channelId)
            ->where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->update([
                self::COL_FK_CONFIG_ACTIVE => Constant_ConfigActive::ACTIVE,
                self::COL_SHOP_CHANNEL_LINKED => 1
            ]);
    }

    /**
     * @param $shopChannelId
     * @return mixed
     */
    public static function getShopChannelAndMasterInfoByShopChannelId($shopChannelId)
    {
        $query = DB::connection('master_business')->table('shop_channel')
            ->join('shop_master', 'fk_shop_master', '=', 'shop_master_id')
            ->join('venture', 'fk_venture', '=', 'venture_id')
            ->where('shop_channel_id', $shopChannelId)
            ->where('shop_channel.fk_config_active', 1)
            ->select([
                'fk_shop_master',
                'fk_channel',
                'shop_channel_id',
                'shop_channel_api_credential',
                'fk_venture',
                'venture_timezone',
            ]);
        return $query->first();
    }

    /**
     * @param $channelId
     * @return Collection
     */
    public static function getAllShopChannelLinkedByChannelId($channelId)
    {
        $query = DB::connection('master_business')->table('shop_channel')
            ->join('shop_master', 'fk_shop_master', '=', 'shop_master_id')
            ->join('venture', 'fk_venture', '=', 'venture_id')
            ->where('shop_channel.fk_config_active', 1)
            ->where('fk_channel', $channelId)
            ->where('shop_channel_linked', 1)
            ->select([
                'fk_shop_master',
                'shop_channel_id',
                'shop_channel_api_credential',
                'fk_venture',
                'venture_timezone',
            ]);
        return $query->get();
    }
}