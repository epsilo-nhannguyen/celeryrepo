<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 14:28
 */


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Library;


class ShopMaster extends ModelBusiness
{
    #region TABLE shop_master
    /**
     * @var string
     */
    const TABLE_NAME = 'shop_master';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_ID = 'shop_master_id';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_NAME = 'shop_master_name';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_STATUS = 'shop_master_status';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_CREATED_AT = 'shop_master_created_at';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_UPDATED_AT = 'shop_master_updated_at';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_UPDATED_BY = 'shop_master_updated_by';

    /**
     * @var string
     */
    const COL_FK_ORGANIZATION = 'fk_organization';

    /**
     * @var string
     */
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_CREATED_BY = 'shop_master_created_by';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_IN_QUOTA = 'shop_master_in_quota';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_CODE = 'shop_master_code';

    /**
     * @var string
     */
    const COL_FK_VENTURE = 'fk_venture';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_REFERENCE_WAREHOUSE = 'shop_master_reference_warehouse';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_ALLOW_SYNC_STOCK = 'shop_master_allow_sync_stock';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_ALLOW_SYNC_STOCK_AT = 'shop_master_allow_sync_stock_at';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_ALLOW_SYNC_STOCK_BY = 'shop_master_allow_sync_stock_by';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_ALLOW_SYNC_PRICE = 'shop_master_allow_sync_price';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_ALLOW_SYNC_PRICE_AT = 'shop_master_allow_sync_price_at';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_ALLOW_SYNC_PRICE_BY = 'shop_master_allow_sync_price_by';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_RESPONSIBLE = 'shop_master_responsible';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_REPRESENTATIVE = 'shop_master_representative';

    /**
     * @var string
     */
    const COL_SHOP_MASTER_CONFIG = 'shop_master_config';

    /**
     * @var string
     */
    const COL_FK_ADMIN_PRODUCT = 'fk_admin_product';
    #endregion

    /**
     * get By Id
     * @param int $id
     * @return mixed
     */
    public static function getById($id)
    {
        $query = DB::connection('master_business')->table('shop_master')
            ->where('shop_master_id', $id)
        ;

        return $query->get()->first();
    }

    /**
     * @param $shopMasterInfo
     * @param $shopChannelInfo
     * @param $shopChannelUserInfo
     * @return string
     */
    public static function insertNewStore($shopMasterInfo, $shopChannelInfo, $shopChannelUserInfo)
    {
        $message = '';
        DB::beginTransaction();
        try {
            $shopMasterId = DB::table(self::TABLE_NAME)->insertGetId(
                $shopMasterInfo
            );

            $shopChannelInfo[ShopChannel::COL_FK_SHOP_MASTER] = $shopMasterId;
            $shopChannelId = DB::table(ShopChannel::TABLE_NAME)->insertGetId(
                $shopChannelInfo
            );

            $shopChannelUserInfo[ShopChannelUser::COL_FK_SHOP_CHANNEL] = $shopChannelId;
            DB::table(ShopChannelUser::TABLE_NAME)->insert(
                $shopChannelUserInfo
            );

            DB::commit();
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            DB::rollback();
        }

        return $message;
    }
}