<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;


class ShopAdsSettingKeyword extends ModelBusiness
{
    /**
     * search By Shop Ads Id
     * @param int $shopAdsId
     * @param array $actionIdArray
     * @return \Illuminate\Support\Collection
     */
    public static function searchByShopAdsId($shopAdsId, $actionIdArray)
    {
        $today = date('Y-m-d');
        $model = DB::connection('master_business')->table('shop_ads_setting_keyword')
            ->join('shop_ads_rule', 'shop_ads_rule.id','=','shop_ads_setting_keyword.shop_ads_rule_id')
            ->join('shop_ads_keywords', 'shop_ads_keywords.id','=','shop_ads_setting_keyword.shop_ads_keyword_id')
            ->where('shop_ads_keywords.shop_ads_id', $shopAdsId)
            ->whereIn('shop_ads_rule.shop_ads_action_id', $actionIdArray)
            ->where('shop_ads_rule.validity_from', '<=', $today)
            ->where('shop_ads_rule.validity_to', '>=', $today)
            ->select(
                'shop_ads_rule.id',
                'shop_ads_rule.extend',
                'shop_ads_rule.shop_ads_action_id'
            )
            ->orderBy('shop_ads_rule.id')
            ->distinct()
        ;
        return $model->get();
    }

    /**
     * search By Rule Id
     * @param int $ruleId
     * @return \Illuminate\Support\Collection
     */
    public static function searchByRuleId($ruleId)
    {
        $model = DB::connection('master_business')->table('shop_ads_setting_keyword')
            ->join('shop_ads_keywords', 'shop_ads_keywords.id','=','shop_ads_setting_keyword.shop_ads_keyword_id')
            ->where('shop_ads_setting_keyword.shop_ads_rule_id', $ruleId)
            ->select(
                DB::raw('shop_ads_keywords.keyword_name as mak_programmatic_keyword_name'),
                'shop_ads_keywords.id',
                'shop_ads_keywords.bidding_price',
                'shop_ads_keywords.is_shop_ads_position'
            )
        ;

        return $model->get();
    }
}