<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 14:28
 */


namespace App\Models\ModelBusiness;


use App\Library;
use App\Library\ModelBusiness;
use App\Library\Cache;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class Venture extends ModelBusiness
{
    /**
     * @var int
     */
    const VIETNAM = 1;
    const PHILIPPINES = 2;
    const MALAYSIA = 3;
    const SINGAPORE = 4;
    const INDONESIA = 5;
    const THAILAND = 6;

    /**
     * @var string
     */
    const VIETNAM_CODE = 'VN';
    const PHILIPPINES_CODE = 'PH';
    const MALAYSIA_CODE = 'MY';
    const SINGAPORE_CODE = 'SG';
    const INDONESIA_CODE = 'ID';
    const THAILAND_CODE = 'TH';

    #region TABLE venture
    /**
     * @var string
     */
    const TABLE_NAME = 'venture';

    /**
     * @var string
     */
    const COL_VENTURE_ID = 'venture_id';

    /**
     * @var string
     */
    const COL_VENTURE_NAME = 'venture_name';

    /**
     * @var string
     */
    const COL_VENTURE_CODE = 'venture_code';

    /**
     * @var string
     */
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';

    /**
     * @var string
     */
    const COL_VENTURE_CREATED_BY = 'venture_created_by';

    /**
     * @var string
     */
    const COL_VENTURE_CREATED_AT = 'venture_created_at';

    /**
     * @var string
     */
    const COL_VENTURE_UPDATED_BY = 'venture_updated_by';

    /**
     * @var string
     */
    const COL_VENTURE_UPDATED_AT = 'venture_updated_at';

    /**
     * @var string
     */
    const COL_VENTURE_EXCHANGE = 'venture_exchange';

    /**
     * @var string
     */
    const COL_VENTURE_FORMAT_RIGHT = 'venture_format_right';

    /**
     * @var string
     */
    const COL_VENTURE_TIMEZONE_TIME_PADDING = 'venture_timezone_time_padding';

    /**
     * @var string
     */
    const COL_VENTURE_TIMEZONE = 'venture_timezone';

    /**
     * @var string
     */
    const COL_VENTURE_COUNTRY = 'venture_country';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = self::COL_VENTURE_ID;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COL_VENTURE_NAME,
        self::COL_VENTURE_CODE,
        self::COL_FK_CONFIG_ACTIVE,
        self::COL_VENTURE_CREATED_BY,
        self::COL_VENTURE_CREATED_AT,
        self::COL_VENTURE_UPDATED_BY,
        self::COL_VENTURE_UPDATED_AT,
        self::COL_VENTURE_EXCHANGE,
        self::COL_VENTURE_FORMAT_RIGHT,
        self::COL_VENTURE_TIMEZONE_TIME_PADDING,
        self::COL_VENTURE_TIMEZONE,
        self::COL_VENTURE_COUNTRY
    ];
    #endregion

    /**
     * @return mixed
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * get By Id
     * @param int $id
     * @return mixed
     */
    public static function getById($id)
    {
        $query = DB::connection('master_business')->table('venture')
            ->where('venture_id', $id)
        ;

        return $query->get()->first();
    }

    /**
     * @param $id
     * @return array|mixed|null
     * @throws \App\Application\Exceptions\PassportException
     */
    public static function getByIdFromCache($id)
    {
        $key = Cache::getInstance()->getAllVenture();
        $arrVentures = json_decode(Cache::getInstance()->get($key), true);
        if (!$arrVentures) {
            $arrVentures = self::getAll();
            Cache::getInstance()->save($key, json_encode($arrVentures));
        }
        foreach ($arrVentures as $venture) {
            if ($venture[self::COL_VENTURE_ID] == $id) {
                return $venture;
            }
        }
        return null;
    }
}