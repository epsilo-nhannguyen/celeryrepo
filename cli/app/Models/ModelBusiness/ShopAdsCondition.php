<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;


class ShopAdsCondition extends ModelBusiness
{
    // shop info in database

    //shop_ads_condition
    //id
    //value
    //shop_ads_rule_id
    //shop_ads_metric_id
    //shop_ads_operator_id
    //created_at
    //created_by

    /**
     * search By Rule Id
     * @param int $ruleId
     * @return \Illuminate\Support\Collection
     */
    public static function searchByRuleId($ruleId)
    {
        $model = DB::connection('master_business')->table('shop_ads_condition')
            ->where('shop_ads_rule_id', $ruleId)
            ->select(
                'shop_ads_metric_id',
                'shop_ads_operator_id',
                'value'
            )
        ;

        return $model->get();
    }
}