<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 14:28
 */


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Library;


class Channel extends ModelBusiness
{

    /**
     * @var string
     */
    const LAZADA_CODE = 'LAZADA';

    /**
     * @var string
     */
    const TIKI_CODE = 'TIKI';

    /**
     * @var string
     */
    const SHOPEE_CODE = 'SHOPEE';

    /**
     * @var string
     */
    const SENDO_CODE = 'SENDO';

    /**
     * @var string
     */
    const TOKOPEDIA_CODE = 'TOKOPEDIA';

    #region TABLE Channel
    /**
     * @var string
     */
    const TABLE_NAME = 'channel';

    /**
     * @var string
     */
    const COL_CHANNEL_ID = 'channel_id';

    /**
     * @var string
     */
    const COL_CHANNEL_NAME = 'channel_name';

    /**
     * @var string
     */
    const COL_CHANNEL_CODE = 'channel_code';

    /**
     * @var string
     */
    const COL_FK_CONFIG_ACTIVE = 'fk_config_active';

    /**
     * @var string
     */
    const COL_CHANNEL_CREATED_AT = 'channel_created_at';

    /**
     * @var string
     */
    const COL_CHANNEL_UPDATED_BY = 'channel_updated_by';

    /**
     * @var string
     */
    const COL_FK_VENTURE = 'fk_venture';

    /**
     * @var string
     */
    const COL_CHANNEL_CONFIGURATION_RRP = 'channel_configuration_rrp';

    /**
     * @var string
     */
    const COL_CHANNEL_CONFIGURATION_SELLING_PRICE = 'channel_configuration_selling_price';

    /**
     * @var string
     */
    const COL_CHANNEL_IS_OFFLINE = 'channel_is_offline';

    /**
     * @var string
     */
    const COL_CHANNEL_REQUEST_PACKING = 'channel_request_packing';

    /**
     * @var string
     */
    const COL_CHANNEL_CONFIG = 'channel_config';

    /**
     * @var string
     */
    const COL_CHANNEL_DATE_TO_SHIP = 'channel_date_to_ship';

    /**
     * @var string
     */
    const COL_CHANNEL_BUSINESS_DAY_FROM = 'channel_business_day_from';

    /**
     * @var string
     */
    const COL_CHANNEL_BUSINESS_DAY_TO = 'channel_business_day_to';

    /**
     * @var string
     */
    const COL_CHANNEL_URL_HANDLE_CALLBACK = 'channel_url_handle_callback';
    #endregion

    /**
     * get By Id
     * @param int $id
     * @return mixed
     */
    public static function getById($id)
    {
        $query = DB::connection('master_business')->table('channel')
            ->where('channel_id', $id)
        ;

        return $query->get()->first();
    }

    /**
     * @param string $channelCode
     * @param int $ventureId
     * @return \Illuminate\Database\Query\Builder
     */
    public static function getByChannelCodeAndVentureId($channelCode, $ventureId)
    {
        return DB::table(self::TABLE_NAME)
            ->where(
                self::TABLE_NAME . '.' . self::COL_CHANNEL_CODE,
                '=',
                $channelCode
            )
            ->where(
                self::TABLE_NAME . '.' . self::COL_FK_VENTURE,
                '=',
                $ventureId
            )
            ->first();
    }
}