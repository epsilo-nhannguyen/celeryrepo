<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;
use App\Library;


class ShopAdsLogShop extends ModelBusiness
{

    /**
     * build Data Insert
     * @param $shopAdsId
     * @param $actionId
     * @param $createdBy
     * @param $ruleId
     * @param $checkpoint
     * @return array
     */
    public static function buildDataInsert($shopAdsId, $actionId, $createdBy, $ruleId = null, $checkpoint = null)
    {
        return [
            'shop_ads_id' => $shopAdsId,
            'shop_ads_action_id' => $actionId,
            'created_at' => Library\Common::getCurrentTimestamp(),
            'created_by' => $createdBy,
            'shop_ads_rule_id' => $ruleId,
            'checkpoint' => $checkpoint
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('shop_ads_log_shop')->insert($recordArray);
    }

}