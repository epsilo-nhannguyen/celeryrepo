<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Library;


class TokoAdsSku extends ModelBusiness
{
    protected $table = 'toko_ads_sku';
    /**
     * @var string
     */
    const STATUS_ONGOING = 'ongoing';
    const STATUS_PAUSED = 'paused';
    const STATUS_ENDED = 'ended';
    const STATUS_SCHEDULED = 'scheduled';
    const STATUS_CLOSED = 'closed';
    const STATUS_NOT_DELIVERED = 'not_delivered';

    /**
     * search By Shop Channel Id
     * @param $shopChannelId
     * @return Collection
     */
    public static function searchByShopChannelId($shopChannelId)
    {
        $query = DB::connection('master_business')->table('toko_ads_sku')
            ->where('shop_channel_id', $shopChannelId)
            ->select([
                'id',
                'product_id',
                'channel_product_id',
            ])
        ;

        return $query->get();
    }

    /**
     * build Data Insert
     * @param int $shopChannelId
     * @param int|null $channelProductId
     * @param int $campaignId
     * @param int $productId
     * @param string $status
     * @param float $maxCost
     * @param int $from
     * @param int $to
     * @param int $createdBy
     * @return array
     */
    public static function buildDataInsert($shopChannelId, $channelProductId, $campaignId, $productId, $status, $maxCost, $from, $to, $createdBy)
    {
        return [
            'shop_channel_id' => $shopChannelId,
            'channel_product_id' => $channelProductId,
            'campaign_id' => $campaignId,
            'product_id' => $productId,
            'status' => $status,
            'max_cost' => $maxCost,
            'from' => $from,
            'to' => $to,
            'created_by' => $createdBy,
            'created_at' => Library\Common::getCurrentTimestamp(),
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::connection('master_business')->table('toko_ads_sku')->insert($recordArray);
    }

    /**
     * updated By Id
     * @param int $id
     * @param array $record
     * @return int
     */
    public static function updatedById($id, $record)
    {
        return DB::connection('master_business')->table('toko_ads_sku')
            ->where('id', $id)
            ->update($record)
            ;
    }

    /**
     * updated By Product Id
     * @param int $productId
     * @param array $record
     * @return int
     */
    public static function updatedByProductId($productId, $record)
    {
        return DB::connection('master_business')->table('toko_ads_sku')
            ->where('product_id', $productId)
            ->update($record)
            ;
    }

    /**
     * search By Campaign Id Array
     * @param array $campaignIdArray
     * @return Collection
     */
    public static function searchByCampaignIdArray($campaignIdArray)
    {
        if ( ! $campaignIdArray) return collect([]);

        $query = DB::connection('master_business')->table('toko_ads_sku')
            ->whereIn('campaign_id', $campaignIdArray)
            ->select([
                'id',
                'campaign_id',
                'product_id',
                'channel_product_id',
                'status',
                'updated_by',
            ])
        ;

        return $query->get();
    }

    public static function getSkuManagement($from, $to, $status, $shopId, $search, $allShopChannelAllow, $campaignId)
    {
        $query = DB::connection('master_business')->table('shop_master as sm')
            ->join('shop_channel as sc', 'sc.fk_shop_master','=','sm.shop_master_id')
            ->join('channel as c', 'c.channel_id','=','sc.fk_channel')
            ->join('venture as v','v.venture_id','=','sm.fk_venture')
            ->join('toko_ads_campaign as tac', 'tac.shop_channel_id','=','sc.shop_channel_id')
            ->join('toko_ads_sku as tas', 'tas.campaign_id','=','tac.id')
            ->join('product_shop_channel as psc', 'psc.product_shop_channel_id', '=','tas.product_id')
            ->leftJoin('toko_ads_sku_performance as tasp', 'tasp.product_id','=','tas.product_id');

        if($from && $to){
            $colImpression = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.impression, 0)) as impression";
            $colClick = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.click, 0)) as click";
            $colItemSold = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.item_sold, 0)) as sold";
            $colCost = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.cost, 0)) as cost";
            $colGmv = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.gmv, 0)) as gmv";
            $colCTR = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.ctr, 0)) as ctr";
            $colAllSold = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.all_sold, 0)) as all_sold";
            $colAverage = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.average, 0)) as average";
            $colRank = "sum(if(`tasp`.`channel_create_date` between '{$from}' and '{$to}', tasp.rank, 0)) as rank";
        } else {
            $colImpression = "sum(tasp.impression) as impression";
            $colClick = "sum(tasp.click) as click";
            $colItemSold = "sum(tasp.item_sold) as sold";
            $colCost = "sum(tasp.cost) as cost";
            $colGmv = "sum(tasp.gmv) as gmv";
            $colCTR = "sum(tasp.ctr) as ctr";
            $colAllSold = "sum(tasp.all_sold) as all_sold";
            $colAverage = "sum(tasp.average) as average";
            $colRank = "sum(tasp.rank) as rank";
        }

        if($status != -1){
            $query = $query->where('tas.status', $status);
        }

        if($shopId > 0){
            $query = $query->where('tac.shop_channel_id', $shopId);
        }

        if($search){
            $query = $query->where('psc.product_shop_channel_name', 'like', '%'.$search.'%')
                ->orWhere('tac.name', 'like', '%'.$search.'%')
                ->orWhere('tac.code', 'like', '%'.$search.'%');
        }
        if($campaignId){
            $query = $query->where('tas.campaign_id', $campaignId);
        }

        $query = $query->where('c.channel_code', 'TOKOPEDIA')
            ->where('v.venture_code', 'ID');
        $query = $query->groupBy(['tas.id']);
//            ->whereIn('sc.shop_channel_id', $allShopChannelAllow);
        $query = $query->select([
            'tas.id',
            'tas.status',
            'tas.product_id',
            'tas.channel_product_id',
            'sc.shop_channel_name as shop_channel_name',
            'psc.product_shop_channel_name as product_name',
            'tac.name as campaign_name',
            'tac.code as code',
            DB::raw("ifnull(psc.product_shop_channel_stock_allocation_stock, 0) as current_stock"),
            DB::raw($colImpression),
            DB::raw($colClick),
            DB::raw($colItemSold),
            DB::raw($colCost),
            DB::raw($colGmv),
            DB::raw($colRank),
            DB::raw($colCTR),
            DB::raw($colAllSold),
            DB::raw($colAverage),
            'tas.max_cost as max_cost',
            'tas.from as time_line_from',
            'tas.to as time_line_to'
        ]);
        return $query;
    }

    public static function updateTimeLine($ids, $from, $to)
    {
        return TokoAdsSku::whereIn('id', $ids)->update(['from' => $from, 'to' => $to]);
    }

    public static function updateStatus($id, $value)
    {
        $sku = TokoAdsSku::where('id', $id)->first();
        $sku->status = $value;
        return $sku->save();
    }

    public static function getById($id)
    {
        return TokoAdsSku::where('id', $id)->first();
    }

    public static function getCampaignById($id)
    {
        return DB::connection('master_business')->table('toko_ads_campaign as tac')
            ->join('toko_ads_sku as tas', 'tas.campaign_id', '=', 'tac.id')
            ->where('tas.id', $id)->select(['tac.*']);
    }

    public static function getProductShopChannel($id)
    {
        return DB::connection('master_business')->table('toko_ads_sku as tas')
            ->join('product_shop_channel as psc', 'psc.product_shop_channel_id', '=',
                'tas.product_id')
            ->where('tas.id', $id)->select(['psc.*']);
    }

    public static function getProductIdByChannel($channelCampaignId, $channelProductId)
    {
        return DB::connection('master_business')->table('toko_ads_sku as tas')
            ->join('toko_ads_campaign as tac', 'tac.id', '=', 'tas.campaign_id')
            ->where('tac.channel_campaign_id', $channelCampaignId)
            ->where('tas.channel_product_id', $channelProductId)->select(['tas.product_id'])->pluck('product_id')->first();
    }

}
