<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use App\Library;
use Illuminate\Support\Facades\DB;
use Exception;

class ShopDataPerformance extends ModelBusiness
{
    const TABLE_NAME = 'shop_data_performance';

    const COL_ID = 'id';
    const COL_SHOP_CHANNEL_ID = 'shop_channel_id';
    const COL_CREATE_DATE = 'create_date';
    const COL_GMV = 'gmv';
    const COL_COST = 'cost';
    const COL_VIEW = 'view';
    const COL_SOLD = 'sold';
    const COL_CLICK = 'click';
    const COL_CREATED_AT = 'created_at';
    const COL_UPDATED_AT = 'updated_at';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_ID;
    public $timestamps = false;

    /**
     * @param $shopChannelId
     * @param $createDate
     * @return mixed
     */
    public static function getByShopChannelIdAndDate($shopChannelId, $createDate)
    {
        $createDate = Library\Formater::timeStampToDate($createDate);
        return self::where(self::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::COL_CREATE_DATE, $createDate)
            ->select(self::COL_ID)
            ->offset(0)
            ->limit(1)
            ->first();
    }

    public static function buildDataInsert($shopChannelId, $date, $totalView, $totalClick, $totalSold, $totalGmv, $totalCost)
    {
        $date = Library\Formater::timeStampToDate($date);
        return [
            self::COL_SHOP_CHANNEL_ID => $shopChannelId,
            self::COL_CREATE_DATE => $date,
            self::COL_VIEW => $totalView,
            self::COL_CLICK => $totalClick,
            self::COL_SOLD => $totalSold,
            self::COL_GMV => $totalGmv,
            self::COL_COST => $totalCost,
            self::COL_CREATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
        ];
    }

    public static function buildDataUpdate($totalView, $totalClick, $totalSold, $totalGmv, $totalCost)
    {
        return [
            self::COL_VIEW => $totalView,
            self::COL_CLICK => $totalClick,
            self::COL_SOLD => $totalSold,
            self::COL_GMV => $totalGmv,
            self::COL_COST => $totalCost,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp()
        ];
    }

    /**
     * @param $recordInsert
     * @return mixed
     * @throws Exception
     */
    public static function insertData($recordInsert)
    {
        try {
            return DB::table(self::TABLE_NAME)->insert($recordInsert);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $recordId
     * @param $updateRecord
     * @return mixed
     * @throws Exception
     */
    public static function updateData($recordId, $updateRecord)
    {
        try {
            return self::where(self::COL_ID, $recordId)
                ->update($updateRecord);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $shopChannelId
     * @param $date
     * @return mixed
     */
    public static function getPerformance($shopChannelId, $date)
    {
        return DB::table(self::TABLE_NAME)
            ->join('shop_ads', 'shop_ads.shop_channel_id', '=', 'shop_data_performance.shop_channel_id')
            ->where('shop_data_performance.shop_channel_id', $shopChannelId)
            ->where('shop_data_performance.create_date', $date)
            ->select()
            ->get();
    }

    /**
     * sum Column By Date From To
     * @param string $column
     * @param int $shopChannelId
     * @param string $dateFrom
     * @param string $dateTo
     * @return mixed
     */
    public static function sumColumnByDateFromTo($column, $shopChannelId, $dateFrom, $dateTo)
    {
        $model = DB::connection('master_business')->table('shop_data_performance')
            ->where('shop_channel_id', $shopChannelId)
            ->whereBetween('create_date', [$dateFrom, $dateTo])
        ;
        return $model->sum($column);
    }
}