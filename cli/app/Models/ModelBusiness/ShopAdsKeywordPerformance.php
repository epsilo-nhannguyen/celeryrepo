<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use App\Library;
use App\Models\ShopChannel;
use Illuminate\Support\Facades\DB;
use Exception;

class ShopAdsKeywordPerformance extends ModelBusiness
{
    const TABLE_NAME = 'shop_ads_data_performance';

    const COL_ID = 'id';
    const COL_SHOP_ADS_ID = 'shop_ads_id';
    const COL_SHOP_ADS_KEYWORD_ID = 'shop_ads_keyword_id';
    const COL_KEYWORD_NAME = 'keyword_name';
    const COL_CREATE_DATE = 'create_date';
    const COL_GMV = 'gmv';
    const COL_EXPENSE = 'expense';
    const COL_SOLD = 'sold';
    const COL_SHOP_ITEM_CLICK = 'shop_item_click';
    const COL_ORDER_AMOUNT = 'order_amount';
    const COL_VIEW = 'view';
    const COL_CLICK = 'click';
    const COL_CREATED_AT = 'created_at';
    const COL_UPDATED_AT = 'updated_at';
    const COL_RAW_DATA = 'data';
    const COL_HEX_KEYWORD_NAME = 'hex_keyword_name';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_ID;
    public $timestamps = false;

    /**
     * @param $shopAdsId
     * @param $keywordId
     * @param $keywordName
     * @param $createDate
     * @param $gmv
     * @param $expense
     * @param $sold
     * @param $shopItemClick
     * @param $orderAmount
     * @param $view
     * @param $click
     * @param $fullJson
     * @param $hexKeywordName
     * @return array
     */
    public static function buildDataInsert(
        $shopAdsId, $keywordId, $keywordName, $createDate, $gmv, $expense,
        $sold, $shopItemClick, $orderAmount, $view, $click, $fullJson, $hexKeywordName = null)
    {
        return [
            self::COL_SHOP_ADS_ID => $shopAdsId,
            self::COL_SHOP_ADS_KEYWORD_ID => $keywordId,
            self::COL_KEYWORD_NAME => $keywordName,
            self::COL_CREATE_DATE => Library\Formater::timeStampToDate($createDate),
            self::COL_GMV => $gmv,
            self::COL_EXPENSE => $expense,
            self::COL_SOLD => $sold,
            self::COL_SHOP_ITEM_CLICK => $shopItemClick,
            self::COL_ORDER_AMOUNT => $orderAmount,
            self::COL_VIEW => $view,
            self::COL_CLICK => $click,
            self::COL_CREATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_RAW_DATA => json_encode($fullJson),
            self::COL_HEX_KEYWORD_NAME => $hexKeywordName
        ];
    }

    /**
     * @param $keywordId
     * @param $gmv
     * @param $expense
     * @param $sold
     * @param $shopItemClick
     * @param $orderAmount
     * @param $view
     * @param $click
     * @param $fullJson
     * @return array
     */
    public static function buildDataUpdate(
        $keywordId, $gmv, $expense, $sold, $shopItemClick,
        $orderAmount, $view, $click, $fullJson)
    {
        return [
            self::COL_SHOP_ADS_KEYWORD_ID => $keywordId,
            self::COL_GMV => $gmv,
            self::COL_EXPENSE => $expense,
            self::COL_SOLD => $sold,
            self::COL_SHOP_ITEM_CLICK => $shopItemClick,
            self::COL_ORDER_AMOUNT => $orderAmount,
            self::COL_VIEW => $view,
            self::COL_CLICK => $click,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_RAW_DATA => json_encode($fullJson)
        ];
    }

    /**
     * Batch Insert
     * @param array $recordInsert
     * @return mixed
     * @throws Exception
     */
    public static function insertData($recordInsert)
    {
        try {
            return DB::table(self::TABLE_NAME)->insert($recordInsert);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $recordId
     * @param $recordUpdateData
     * @return mixed
     * @throws Exception
     */
    public static function updateData($recordId, $recordUpdateData)
    {
        try {
            return self::where(self::COL_ID, $recordId)
                ->update($recordUpdateData);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $keywordName
     * @param int $date
     * @param null $shopAdsId
     * @return mixed
     */
    public static function searchByShopAdsKeywordNameAndDate($keywordName, $date, $shopAdsId = null)
    {
        $date = Library\Formater::timeStampToDate($date);
        $select = DB::table(self::TABLE_NAME)
            ->where(self::COL_CREATE_DATE, $date)
            ->whereRaw('HEX(' . self::COL_KEYWORD_NAME . ') = HEX(?)', [$keywordName]);

        if ($shopAdsId) {
            $select->where(self::COL_SHOP_ADS_ID, $shopAdsId);
        }
        $select->select(self::COL_ID)
            ->offset(0)
            ->limit(1);
        return $select->first();
    }

    /**
     * @param $shopAdsId
     * @param $date
     * @return mixed
     */
    public static function searchByShopAdsIdAndDate($shopAdsId, $date)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                ShopAds::TABLE_NAME,
                ShopAds::TABLE_NAME . '.' . ShopAds::COL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_SHOP_ADS_ID
            )
            ->join(
                ShopAdsKeyword::TABLE_NAME,
                ShopAdsKeyword::TABLE_NAME . '.' . ShopAdsKeyword::COL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_SHOP_ADS_KEYWORD_ID
            )
            ->where(ShopAds::TABLE_NAME . '.' . ShopAds::COL_ID, $shopAdsId)
            ->where(self::TABLE_NAME . '.' . self::COL_CREATE_DATE, $date)
            ->select(
                self::TABLE_NAME . '.' . self::COL_ID,
                self::TABLE_NAME . '.' . self::COL_SHOP_ADS_KEYWORD_ID,
                self::TABLE_NAME . '.' . self::COL_CREATE_DATE
            )
            ->get();
    }

    public static function sumPerformanceByDate($shopChannelId, $date)
    {
        $date = Library\Formater::timeStampToDate($date);
        return DB::table(self::TABLE_NAME)
            ->join(
                ShopAds::TABLE_NAME,
                ShopAds::TABLE_NAME . '.' . ShopAds::COL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_SHOP_ADS_ID
            )
            ->join(
                ShopChannel::TABLE_NAME,
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_SHOP_CHANNEL_ID,
                '=',
                ShopAds::TABLE_NAME . '.' . ShopAds::COL_SHOP_CHANNEL_ID
            )
            ->where(ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_SHOP_CHANNEL_ID, $shopChannelId)
            ->where(self::TABLE_NAME . '.' . self::COL_CREATE_DATE, $date)
            ->select(
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_SHOP_CHANNEL_ID,
                DB::raw('sum(' . self::TABLE_NAME . '.' . self::COL_VIEW . ') as total_view'),
                DB::raw('sum(' . self::TABLE_NAME . '.' . self::COL_CLICK . ') as total_click'),
                DB::raw('sum(' . self::TABLE_NAME . '.' . self::COL_ORDER_AMOUNT . ') as total_sold'),
                DB::raw('sum(' . self::TABLE_NAME . '.' . self::COL_GMV . ') as total_gmv'),
                DB::raw('sum(' . self::TABLE_NAME . '.' . self::COL_EXPENSE . ') as total_expense')
            )
            ->groupBy(
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_SHOP_CHANNEL_ID
            )
            ->offset(0)
            ->limit(1)
            ->first();
    }

    /**
     * sum Column By Date From To
     * @param $column
     * @param $shopAdsKeywordId
     * @param $dateFrom
     * @param $dateTo
     * @return mixed
     */
    public static function sumColumnByDateFromTo($column, $shopAdsKeywordId, $dateFrom, $dateTo)
    {
        $model = DB::connection('master_business')->table('shop_ads_data_performance')
            ->where('shop_ads_keyword_id', $shopAdsKeywordId)
            ->whereBetween('create_date', [$dateFrom, $dateTo])
        ;
        return $model->sum($column);
    }

    /**
     * @param int $limit
     * @return \Illuminate\Support\Collection
     */
    public static function getByKeywordIsNull($limit = 1000)
    {
        $select = DB::connection('master_business')->table('shop_ads_data_performance')
            ->join('shop_ads', 'shop_ads.id', '=', 'shop_ads_data_performance.shop_ads_id')
            ->join('shop_channel', 'shop_ads.shop_channel_id', '=', 'shop_channel.shop_channel_id')
            ->whereNull('shop_ads_keyword_id')
            ->where('shop_channel.fk_config_active', 1)
            ->select([
                'shop_ads_data_performance.id',
                'shop_ads_id',
                'keyword_name',
            ])
            ->limit($limit)
        ;

        return $select->get();
    }

    /**
     * @param $arrayRecordId
     * @param $shopAdsKeywordId
     * @return bool
     */
    public static function updateShopAdsKeywordIdByKeywordName($arrayRecordId, $shopAdsKeywordId) {
        try {
            return DB::connection('master_business')->table('shop_ads_data_performance')
                ->whereIn(self::COL_ID, array_values($arrayRecordId))
                ->update([
                    self::COL_SHOP_ADS_KEYWORD_ID => $shopAdsKeywordId
                ]);
        } catch (Exception $e) {
            return false;
        }
    }
}