<?php

namespace App\Models\ModelBusiness;

use App\Library\ModelBusiness;
use App\Library;
use App\Models\Channel;
use App\Models\ShopChannel;
use Illuminate\Support\Facades\DB;
use Exception;

class ShopAdsKeyword extends ModelBusiness
{
    const TABLE_NAME = 'shop_ads_keywords';

    const COL_ID = 'id';
    const COL_SHOP_ADS_ID = 'shop_ads_id';
    const COL_KEYWORD_NAME = 'keyword_name';
    const COL_MAK_PROGRAMMATIC_KEYWORD_ID = 'mak_programmatic_keyword_id';
    const COL_IS_ACTIVE = 'is_active';
    const COL_MATCH_TYPE = 'match_type';
    const COL_BIDDING_PRICE = 'bidding_price';
    const COL_UPDATED_BY = 'updated_by';
    const COL_CREATED_BY = 'created_by';
    const COL_CREATED_AT = 'created_at';
    const COL_UPDATED_AT = 'updated_at';
    const COL_RAW_DATA = 'data';
    const COL_IS_SHOP_ADS_POSITION = 'is_shop_ads_position';
    const COL_LAST_TIME_UPDATED_IS_SHOP_ADS_POSITION = 'last_time_updated_is_shop_ads_position';
    const COL_HEX_KEYWORD_NAME = 'hex_keyword_name';

    // Match type
    const BROAD_MATCH = 'broad_match';
    const EXACT_MATCH = 'exact_match';

    // Epsilo System
    const EPSILO_SYSTEM = 300;

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_ID;
    public $timestamps = false;

    /**
     * @param $shopAdsId
     * @param $keywordId
     * @param $keywordName
     * @param $status
     * @param $matchType
     * @param $biddingPrice
     * @param $fullJson
     * @param $hexKeywordName
     * @return array
     */
    public static function buildDataInsert($shopAdsId, $keywordId, $keywordName, $status, $matchType, $biddingPrice, $fullJson, $hexKeywordName = null)
    {
        return [
            self::COL_SHOP_ADS_ID => $shopAdsId,
            self::COL_MAK_PROGRAMMATIC_KEYWORD_ID => $keywordId,
            self::COL_KEYWORD_NAME => $keywordName,
            self::COL_IS_ACTIVE => $status,
            self::COL_MATCH_TYPE => $matchType == 1 ? self::BROAD_MATCH : self::EXACT_MATCH,
            self::COL_BIDDING_PRICE => $biddingPrice,
            self::COL_CREATED_BY => self::EPSILO_SYSTEM,
            self::COL_UPDATED_BY => self::EPSILO_SYSTEM,
            self::COL_CREATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_RAW_DATA => json_encode($fullJson),
            self::COL_HEX_KEYWORD_NAME => $hexKeywordName
        ];
    }

    /**
     * @param $keywordId
     * @param $status
     * @param $matchType
     * @param $biddingPrice
     * @param $fullJson
     * @return array
     */
    public static function buildDataUpdate($keywordId, $status, $matchType, $biddingPrice, $fullJson)
    {
        return [
            self::COL_MAK_PROGRAMMATIC_KEYWORD_ID => $keywordId,
            self::COL_IS_ACTIVE => $status,
            self::COL_MATCH_TYPE => $matchType == 1 ? self::BROAD_MATCH : self::EXACT_MATCH,
            self::COL_BIDDING_PRICE => $biddingPrice,
            self::COL_UPDATED_AT => Library\Common::getCurrentTimestamp(),
            self::COL_RAW_DATA => json_encode($fullJson)
        ];
    }

    /**
     * Batch Insert
     * @param array $recordInsert
     * @return mixed
     * @throws Exception
     */
    public static function insertData($recordInsert)
    {
        try {
            return DB::table(self::TABLE_NAME)->insert($recordInsert);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $shopAdsId
     * @return mixed
     */
    public static function getByShopAdsId($shopAdsId)
    {
        return self::where(self::COL_SHOP_ADS_ID, $shopAdsId)
            ->get();
    }

    /**
     * @param $shopAdsId
     * @return mixed
     */
    public static function getAllByShopAdsId($shopAdsId)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_ADS_ID, $shopAdsId)
            ->get();
    }

    /**
     * @param $shopAdsId
     * @param $keywordName
     * @return mixed
     */
    public static function getByKeywordName($shopAdsId, $keywordName)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_SHOP_ADS_ID, $shopAdsId)
            ->whereRaw('HEX(' . self::COL_KEYWORD_NAME . ') = HEX(?)', [$keywordName])
            ->offset(0)
            ->limit(1)
            ->first();
    }


    /**
     * @return mixed
     */
    public static function selectAllKeywordWithVentureAndAdsId()
    {
        return DB::table(self::TABLE_NAME)
            ->join(ShopAds::TABLE_NAME,
                ShopAds::TABLE_NAME . '.' . ShopAds::COL_ID,
                '=',
                self::TABLE_NAME . '.' . self::COL_SHOP_ADS_ID)
            ->join(ShopChannel::TABLE_NAME,
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_SHOP_CHANNEL_ID,
                '=',
                ShopAds::TABLE_NAME . '.' . ShopAds::COL_SHOP_CHANNEL_ID)
            ->join(Channel::TABLE_NAME,
                Channel::TABLE_NAME . '.' . Channel::COL_CHANNEL_ID,
                '=',
                ShopChannel::TABLE_NAME . '.' . ShopChannel::COL_FK_CHANNEL)
            ->select(self::TABLE_NAME . '.' . self::COL_ID, self::COL_KEYWORD_NAME, Channel::COL_FK_VENTURE, ShopAds::COL_ADSID)
            ->get();
    }

    /**
     * @param $recordId
     * @param $isPosition
     * @return mixed
     * @throws Exception
     */
    public static function updateIsShopAdsPosition($recordId, $isPosition)
    {
        try {
            return self::where(self::COL_ID, $recordId)
                ->update([
                    self::COL_IS_SHOP_ADS_POSITION => $isPosition,
                    self::COL_LAST_TIME_UPDATED_IS_SHOP_ADS_POSITION => strtotime('now')
                ]);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $shopAdsKeywordId
     * @param $recordUpdate
     * @return mixed
     * @throws Exception
     */
    public static function updateById($shopAdsKeywordId, $recordUpdate)
    {
        try {
            return self::where(self::COL_ID, $shopAdsKeywordId)
                ->update($recordUpdate);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Update Case
     * @param string $sql
     * @return mixed
     */
    public static function updateCase($sql)
    {
        return DB::statement($sql);
    }

    /**
     * update Status By Id Array
     * @param $idArray
     * @param $status
     * @param $data
     * @param $adminId
     * @return int
     */
    public static function updateStatusByIdArray($idArray, $status, $data, $adminId)
    {
        return DB::connection('master_business')->table('shop_ads_keywords')
            ->whereIn('id', $idArray)
            ->update([
                'is_active' => $status,
                'data' => $data,
                'updated_by' => $adminId,
                'updated_at' => Library\Common::getCurrentTimestamp()
            ])
        ;
    }

    /**
     * update Bidding Price By Id
     * @param int $id
     * @param float $biddingPrice
     * @param mixed $data
     * @param int $adminId
     * @return int
     */
    public static function updateBiddingPriceById($id, $biddingPrice, $data, $adminId)
    {
        return DB::connection('master_business')->table('shop_ads_keywords')
            ->where('id', $id)
            ->update([
                'bidding_price' => $biddingPrice,
                'data' => $data,
                'updated_by' => $adminId,
                'updated_at' => Library\Common::getCurrentTimestamp()
            ])
        ;
    }

    /**
     * @param $recordInsert
     * @return bool
     */
    public static function insertGetId($recordInsert) {
        try {
            return DB::connection('master_business')
                ->table(self::TABLE_NAME)
                ->insertGetId($recordInsert);
        } catch (Exception $e) {
            return null;
        }
    }

}