<?php


namespace App\Models\ModelBusiness;


use App\Library\ModelBusiness;
use Illuminate\Support\Facades\DB;


class ShopAdsMetric extends ModelBusiness
{

    /**
     * @var int
     */
    const KEYWORD_IMPRESSION_LAST_3_DAYS = 1;

    /**
     * @var int
     */
    const KEYWORD_IMPRESSION_LAST_7_DAYS = 2;

    /**
     * @var int
     */
    const KEYWORD_IMPRESSION_LAST_14_DAYS = 3;

    /**
     * @var int
     */
    const KEYWORD_IMPRESSION_LAST_30_DAYS = 4;

    /**
     * @var int
     */
    const KEYWORD_CLICK_LAST_3_DAYS = 5;

    /**
     * @var int
     */
    const KEYWORD_CLICK_LAST_7_DAYS = 6;

    /**
     * @var int
     */
    const KEYWORD_CLICK_LAST_14_DAYS = 7;

    /**
     * @var int
     */
    const KEYWORD_CLICK_LAST_30_DAYS = 8;

    /**
     * @var int
     */
    const KEYWORD_ITEM_SOLD_LAST_3_DAYS = 9;

    /**
     * @var int
     */
    const KEYWORD_ITEM_SOLD_LAST_7_DAYS = 10;

    /**
     * @var int
     */
    const KEYWORD_ITEM_SOLD_LAST_14_DAYS = 11;

    /**
     * @var int
     */
    const KEYWORD_ITEM_SOLD_LAST_30_DAYS = 12;

    /**
     * @var int
     */
    const KEYWORD_GMV_LAST_3_DAYS = 13;

    /**
     * @var int
     */
    const KEYWORD_GMV_LAST_7_DAYS = 14;

    /**
     * @var int
     */
    const KEYWORD_GMV_LAST_14_DAYS = 15;

    /**
     * @var int
     */
    const KEYWORD_GMV_LAST_30_DAYS = 16;

    /**
     * @var int
     */
    const KEYWORD_COST_SPENT_LAST_3_DAYS = 17;

    /**
     * @var int
     */
    const KEYWORD_COST_SPENT_LAST_7_DAYS = 18;

    /**
     * @var int
     */
    const KEYWORD_COST_SPENT_LAST_14_DAYS = 19;

    /**
     * @var int
     */
    const KEYWORD_COST_SPENT_LAST_30_DAYS = 20;

    /**
     * @var int
     */
    const KEYWORD_TOP_1 = 41;

    /**
     * @var int
     */
    const SHOP_IMPRESSION_LAST_3_DAYS = 21;

    /**
     * @var int
     */
    const SHOP_IMPRESSION_LAST_7_DAYS = 22;

    /**
     * @var int
     */
    const SHOP_IMPRESSION_LAST_14_DAYS = 23;

    /**
     * @var int
     */
    const SHOP_IMPRESSION_LAST_30_DAYS = 24;

    /**
     * @var int
     */
    const SHOP_CLICK_LAST_3_DAYS = 25;

    /**
     * @var int
     */
    const SHOP_CLICK_LAST_7_DAYS = 26;

    /**
     * @var int
     */
    const SHOP_CLICK_LAST_14_DAYS = 27;

    /**
     * @var int
     */
    const SHOP_CLICK_LAST_30_DAYS = 28;

    /**
     * @var int
     */
    const SHOP_ITEM_SOLD_LAST_3_DAYS = 29;

    /**
     * @var int
     */
    const SHOP_ITEM_SOLD_LAST_7_DAYS = 30;

    /**
     * @var int
     */
    const SHOP_ITEM_SOLD_LAST_14_DAYS = 31;

    /**
     * @var int
     */
    const SHOP_ITEM_SOLD_LAST_30_DAYS = 32;

    /**
     * @var int
     */
    const SHOP_GMV_LAST_3_DAYS = 33;

    /**
     * @var int
     */
    const SHOP_GMV_LAST_7_DAYS = 34;

    /**
     * @var int
     */
    const SHOP_GMV_LAST_14_DAYS = 35;

    /**
     * @var int
     */
    const SHOP_GMV_LAST_30_DAYS = 36;

    /**
     * @var int
     */
    const SHOP_COST_SPENT_LAST_3_DAYS = 37;

    /**
     * @var int
     */
    const SHOP_COST_SPENT_LAST_7_DAYS = 38;

    /**
     * @var int
     */
    const SHOP_COST_SPENT_LAST_14_DAYS = 39;

    /**
     * @var int
     */
    const SHOP_COST_SPENT_LAST_30_DAYS = 40;

    /**
     * @var string
     */
    const NAME_IMPRESSION = 'Impression';

    /**
     * @var string
     */
    const NAME_CLCIK = 'Click';

    /**
     * @var string
     */
    const NAME_ITEM_SOLD = 'Item Sold';

    /**
     * @var string
     */
    const NAME_GMV = 'GMV';

    /**
     * @var string
     */
    const NAME_COST_SPENT = 'Cost Spent';

    /**
     * @var string
     */
    const NAME_TOP_1 = 'Top 1';

    # data table mysql
    // shop_ads_metric
    // id
    // name
    // shop_ads_objective_id
    // value

    /**
     * search By Objective Id
     * @param int $objectiveId
     * @return \Illuminate\Support\Collection
     */
    public static function searchByObjectiveId($objectiveId)
    {
        $model = DB::connection('master_business')->table('shop_ads_metric')
            ->where('shop_ads_objective_id', $objectiveId)
        ;

        return $model->get();
    }
}