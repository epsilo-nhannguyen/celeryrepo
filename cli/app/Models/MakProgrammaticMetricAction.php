<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 13:16
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;


class MakProgrammaticMetricAction extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_metric_action';

    const COL_MAK_PROGRAMMATIC_METRIC_ACTION_ID = 'mak_programmatic_metric_action_id';
    const COL_FK_MAK_PROGRAMMATIC_METRIC = 'fk_mak_programmatic_metric';
    const COL_FK_MAK_PROGRAMMATIC_ACTION = 'fk_mak_programmatic_action';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_METRIC_ACTION_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

}