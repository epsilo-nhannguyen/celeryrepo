<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:56
 */

namespace App\Models;


use App\Library\ModelBusiness;
use Illuminate\Database\Eloquent\Model;


class MakProgrammaticCondition extends ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_condition';

    const COL_MAK_PROGRAMMATIC_CONDITION_ID = 'mak_programmatic_condition_id';
    const COL_FK_MAK_PROGRAMMATIC_RULE = 'fk_mak_programmatic_rule';
    const COL_FK_MAK_PROGRAMMATIC_METRIC = 'fk_mak_programmatic_metric';
    const COL_FK_MAK_PROGRAMMATIC_OPERATOR = 'fk_mak_programmatic_operator';
    const COL_MAK_PROGRAMMATIC_CONDITION_VALUE = 'mak_programmatic_condition_value';
    const COL_MAK_PROGRAMMATIC_CONDITION_CREATED_AT = 'mak_programmatic_condition_created_at';
    const COL_MAK_PROGRAMMATIC_CONDITION_CREATED_BY = 'mak_programmatic_condition_created_by';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_CONDITION_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * search By Rule Id
     * @param int $ruleId
     * @return mixed
     */
    public static function searchByRuleId($ruleId)
    {
        return self::where(self::COL_FK_MAK_PROGRAMMATIC_RULE, $ruleId)
            ->select(
                self::COL_FK_MAK_PROGRAMMATIC_METRIC,
                self::COL_FK_MAK_PROGRAMMATIC_OPERATOR,
                self::COL_MAK_PROGRAMMATIC_CONDITION_VALUE
            )
            ->get()
        ;
    }
}