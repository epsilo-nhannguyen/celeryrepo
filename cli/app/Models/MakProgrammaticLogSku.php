<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 02/10/2019
 * Time: 12:57
 */

namespace App\Models;


use App\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MakProgrammaticLogSku extends Library\ModelBusiness
{
    const TABLE_NAME = 'mak_programmatic_log_sku';

    const COL_MAK_PROGRAMMATIC_LOG_SKU_ID = 'mak_programmatic_log_sku_id';
    const COL_MAK_PROGRAMMATIC_LOG_SKU_CHECKPOINT = 'mak_programmatic_log_sku_checkpoint';
    const COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_AT = 'mak_programmatic_log_sku_created_at';
    const COL_FK_PRODUCT_SHOP_CHANNEL = 'fk_product_shop_channel';
    const COL_FK_MAK_PROGRAMMATIC_ACTION = 'fk_mak_programmatic_action';
    const COL_FK_MAK_PROGRAMMATIC_RULE = 'fk_mak_programmatic_rule';
    const COL_FK_MAK_PROGRAMMATIC_CAMPAIGN = 'fk_mak_programmatic_campaign';
    const COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_BY = 'mak_programmatic_log_sku_created_by';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COL_MAK_PROGRAMMATIC_LOG_SKU_ID;
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return Venture[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    /**
     * build Data Insert
     * @param int $productShopChannelId
     * @param int $actionId
     * @param int $ruleId
     * @param string $checkpoint
     * @param int $campaignId
     * @param int $adminId
     * @return array
     */
    public static function buildDataInsert($productShopChannelId, $actionId, $ruleId, $checkpoint, $campaignId, $adminId)
    {
        return [
            self::COL_FK_PRODUCT_SHOP_CHANNEL => $productShopChannelId,
            self::COL_FK_MAK_PROGRAMMATIC_ACTION => $actionId,
            self::COL_FK_MAK_PROGRAMMATIC_RULE => $ruleId,
            self::COL_MAK_PROGRAMMATIC_LOG_SKU_CHECKPOINT => $checkpoint,
            self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN => $campaignId,
            self::COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_BY => $adminId,
            self::COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_AT => Library\Common::getCurrentTimestamp()
        ];
    }

    /**
     * batch Insert
     * @param array $recordArray
     * @return mixed
     */
    public static function batchInsert($recordArray)
    {
        return DB::table(self::TABLE_NAME)->insert($recordArray);
    }

    /**
     * search Campaign Id By Date From To
     * @param int $campaignId
     * @param int $dateFrom
     * @param int $dateTo
     * @param array $actionIdArray
     * @return \Illuminate\Support\Collection
     */
    public static function searchCampaignIdByDateFromTo($campaignId, $dateFrom, $dateTo, $actionIdArray)
    {
        return DB::table(self::TABLE_NAME)
            ->where(self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN, $campaignId)
            ->whereBetween(self::COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_AT, [$dateFrom, $dateTo])
            ->whereIn(self::COL_FK_MAK_PROGRAMMATIC_ACTION, $actionIdArray)
            ->select(
                self::COL_FK_PRODUCT_SHOP_CHANNEL,
                self::COL_FK_MAK_PROGRAMMATIC_ACTION,
                self::COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_AT
            )
            ->get()
        ;
    }

    /**
     * search Shop Channel Id By Date From To
     * @param int $shopChannelId
     * @param int $dateFrom
     * @param int $dateTo
     * @return \Illuminate\Support\Collection
     */
    public static function searchShopChannelIdByDateFromTo($shopChannelId, $dateFrom, $dateTo)
    {
        return DB::table(self::TABLE_NAME)
            ->join(
                MakProgrammaticCampaign::TABLE_NAME,
                MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_MAK_PROGRAMMATIC_CAMPAIGN_ID,
                '=',
                self::TABLE_NAME.'.'.self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN
            )
            ->where(MakProgrammaticCampaign::TABLE_NAME.'.'.MakProgrammaticCampaign::COL_FK_SHOP_CHANNEL, $shopChannelId)
            ->whereBetween(self::COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_AT, [$dateFrom, $dateTo])
            ->select(
                self::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                self::COL_FK_MAK_PROGRAMMATIC_ACTION,
                self::COL_MAK_PROGRAMMATIC_LOG_SKU_CREATED_AT
            )
            ->get()
        ;
    }
}