<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 18:02
 */

namespace App\Library;


class Singleton
{
    /**
     * @var static[]
     */
    private static $_instance;

    /**
     * @return static
     */
    final public static function &getInstance()
    {
        $calledClass = get_called_class();
        if( !isset(self::$_instance[$calledClass]) || is_null(self::$_instance[$calledClass]) ) {
            self::$_instance[$calledClass] = new $calledClass();
        }
        return self::$_instance[$calledClass];
    }

    final protected function __clone(){}
}