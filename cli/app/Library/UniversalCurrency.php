<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 17/11/2019
 * Time: 19:13
 */

namespace App\Library;


use App\Models;


class UniversalCurrency
{
    /**
     * @var int
     */
    const CURRENCY_USD = 1;

    /**
     * @var array
     */
    static $assocDataCurrency = [];

    /**
     * @var array
     */
    static $assocVentureIdCurrencyId = [];

    /**
     * build Currency
     */
    private static function buildCurrencyArray()
    {
        if ( ! self::$assocDataCurrency) {
            $data = Models\UniversalCurrency::getAll();
            foreach ($data as $item) {
                $currencyId = $item->{Models\UniversalCurrency::COL_FK_CURRENCY};
                $yearMonth = $item->{Models\UniversalCurrency::COL_UNIVERSAL_CURRENCY_MONTH};
                if ( ! isset(self::$assocDataCurrency[$currencyId])) {
                    self::$assocDataCurrency[$currencyId] = [];
                }
                self::$assocDataCurrency[$currencyId][$yearMonth] = $item->{Models\UniversalCurrency::COL_UNIVERSAL_CURRENCY_VALUE};
            }
        }
    }

    /**
     * get Currency Value
     * @param int $currencyId
     * @param string $yearMonth
     * @return float
     */
    private static function getCurrencyValue($currencyId, $yearMonth)
    {
        self::buildCurrencyArray();
        
        $currencyValue = self::$assocDataCurrency[$currencyId][$yearMonth] ?? null;
        if ( ! $currencyValue) $currencyValue = end(self::$assocDataCurrency[$currencyId]);

        return $currencyValue;
    }

    /**
     * get Currency Array
     */
    private static function getCurrencyArray()
    {
        if ( ! self::$assocVentureIdCurrencyId) {
            self::$assocVentureIdCurrencyId = array_column(
                array_filter(Formater::stdClassToArray(Models\Currency::getAll()), function ($item) {
                    return $item[Models\Currency::COL_FK_VENTURE];
                }),
                Models\Currency::COL_CURRENCY_ID, Models\Currency::COL_FK_VENTURE
            );
        }
    }

    /**
     * build Currency Data
     * @param float $price
     * @param string $yearMonth
     * @param int $ventureId
     * @return array
     */
    public static function buildCurrencyData($price, $yearMonth, $ventureId)
    {
        self::getCurrencyArray();
        $currencyIdVenture = self::$assocVentureIdCurrencyId[$ventureId];
        $priceUSD = $price / self::getCurrencyValue($currencyIdVenture, $yearMonth);

        $assocCurrencyIdPrice = [$currencyIdVenture => $price, Models\Currency::USD => $priceUSD];
        foreach (self::$assocVentureIdCurrencyId as $currencyId) {
            if ($currencyId == $currencyIdVenture) continue;

            $assocCurrencyIdPrice[$currencyId] = $priceUSD * self::getCurrencyValue($currencyId, $yearMonth);
        }

        return $assocCurrencyIdPrice;
    }

    /**
     * convert Currency
     * ex: convert Vietnam to Malaysia
     * ($ventureId = Venture Id Vietnam, $currencyId = Currency Id Malaysia)
     * @param float $price
     * @param string $yearMonth
     * @param int $ventureId
     * @param int $currencyId
     * @return float
     */
    public static function convertCurrency($price, $yearMonth, $ventureId, $currencyId = Models\Currency::USD)
    {
        $currencyIdOfVenture = self::getCurrencyIdByVentureId($ventureId);

        if ($currencyIdOfVenture != $currencyId) {
            $currencyFirst = self::getCurrency($yearMonth, $currencyIdOfVenture);
            $currencySecond = self::getCurrency($yearMonth, $currencyId);

            $price =  ($price / $currencyFirst) * $currencySecond;
        }

        return $price;
    }

    /**
     * get Currency Id By Venture Id
     * @param $ventureId
     * @return int
     */
    public static function getCurrencyIdByVentureId($ventureId)
    {
        if (!self::$assocVentureIdCurrencyId) {

            self::$assocVentureIdCurrencyId = array_column(
                array_filter(Formater::stdClassToArray(Models\Currency::getAll()), function ($item) {
                    return $item[Models\Currency::COL_FK_VENTURE];
                }),
                Models\Currency::COL_CURRENCY_ID, Models\Currency::COL_FK_VENTURE
            );
        }

        return intval(self::$assocVentureIdCurrencyId[$ventureId]);
    }

    /**
     * get Currency
     * @param string $yearMonth
     * @param int $currencyId
     * @return float
     */
    private static function getCurrency($yearMonth, $currencyId)
    {
        $currencyValue = self::CURRENCY_USD;

        if ($currencyId != Models\Currency::USD) {
            self::buildCurrencyArray();

            $currencyValue = self::$assocDataCurrency[$currencyId][$yearMonth] ?? null;
            if (!$currencyValue) $currencyValue = end(self::$assocDataCurrency[$currencyId]);
        }

        return floatval($currencyValue);
    }
}