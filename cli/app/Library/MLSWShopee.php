<?php

namespace App\Library;

use App\Library\Model\Mongo\SaleOrder;
use App\Library\Model\Sql\Manager;
use App\Models\Channel;
use App\Models\MakCrawRanking;
use App\Models\MakProgrammaticKeyword;
use App\Models\MakProgrammaticKeywordProduct;
use App\Models\MakProgrammaticPriceRange;
use App\Models\MakRecommendationBiddingPrice;
use App\Models\MakRecommendationLogBiddingPrice;
use App\Models\MakRecommendationSkSelect;
use App\Models\ProductAds;
use App\Models\ProductShopChannel;
use App\Models\ShopChannel;
use App\Services\Recommendation;
use App\Library\Common;
use App\Library\Crawler\Channel\Shopee;
use Exception;

class MLSWShopee
{
    private $shopId;
    private $channelId;
    private $shopChannelId;
    private $machine = 'machine';
    public $bidding = 'bidding';
    private $bidding_auto = true;

    public function __construct($shopId, $channelId, $shopChannelId)
    {
        $this->shopId = $shopId;
        $this->channelId = $channelId;
        $this->shopChannelId = $shopChannelId;
    }

    private function getSkuKeyword()
    {
        //get data Sku-KW
        $data = MakProgrammaticKeywordProduct::getDataSK($this->shopChannelId,Manager::SQL_MASTER_CONNECT)->toArray();
        $cohortFrom = date('Y-m-d 00:00:00', strtotime('- 29 days'));
        $cohortTo = date('Y-m-d H:i:s');

        $dataSaleOrderL30DMongo = SaleOrder::countItemSoldPerItemIdByArrayShopId($this->shopId, strtotime($cohortFrom), strtotime($cohortTo));
        $dataItemSoldPerItemIdL30D = [];
        foreach ($dataSaleOrderL30DMongo as $saleOrderL30DMongo) {
            $dataItemSoldPerItemIdL30D[$saleOrderL30DMongo->{'_id'}] = $saleOrderL30DMongo->{'totalItemSold'};
        }

        $dataSaleOrderMongo = SaleOrder::countItemSoldPerItemIdByArrayShopId($this->shopId);
        $dataItemSoldPerItemId = [];
        foreach ($dataSaleOrderMongo as $saleOrderMongo) {
            $dataItemSoldPerItemId[$saleOrderMongo->{'_id'}] = $saleOrderMongo->{'totalItemSold'};
        }

        $result = $dataSkuKeyword = [];
        $BDLimit = json_decode(env('BIDDING_PRICE_LIMIT', "[[0,2],[500000,1],[1000000,0.2]]"),true);
        $rank1 = $BDLimit[0][0];
        $rate1 = $BDLimit[0][1];
        $rank2 = $BDLimit[1][0];
        $rate2 = $BDLimit[1][1];
        $rank3 = $BDLimit[2][0];
        $rate3 = $BDLimit[2][1];

        foreach ($data as $item) {
            $keyword = $item->{MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME};
            $itemId = $item->{ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID};
            $skuName = $item->{ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_NAME};
            $productId = $item->{ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID};
            $price = $item->{ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_CHANNEL_SELLING_PRICE};
            $keywordProductId = $item->{MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID};
            $sellerSKU = $item->{ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_SELLER_SKU};
            $adsId= $item->{ProductAds::COL_PRODUCT_ADS_CHANNEL_ADS_ID};
            $ranking = MakCrawRanking::getRank($keyword, $sellerSKU);
            $itemSoldL30D = $dataItemSoldPerItemIdL30D[$itemId] ?? 0;
            $itemSold = $dataItemSoldPerItemId[$itemId] ?? 0;
            if ($price >= $rank1 && $price <= $rank2){
                $priceLimit = $price * $rate1/100;
            }else if ($price > $rank2 && $price <= $rank3){
                $priceLimit = $price * $rate2/100;
            }else if ($price > $rank3){
                $priceLimit = $price * $rate3/100;
            }
            if ($itemSoldL30D > 0 && ((!empty($ranking) && $ranking->{MakCrawRanking::COL_MAK_CRAW_RANKING_RANK} > 10) || empty($ranking))) {
                $result[$keyword][] = explode(',', $itemId . ',' . $itemSold . ',' . $keywordProductId . ',' . $productId . ',' . $sellerSKU . ',' . $priceLimit . ',' .$adsId . ',' . $skuName);
            }
        }

        foreach ($result as $keyword => $item) {
            if (count($item) >= 10) {
                $dataSkuKeyword[$keyword] = $item;
            }
        }

        $dataResult = [];

        $typeSort = 'desc';

        foreach ($dataSkuKeyword as $keyword => $skuKeyword) {
            $totalItemSold = $averageItemSold = 0;
            foreach ($skuKeyword as $item) {
                $totalItemSold += $item[1];
            }

            $averageItemSold = $totalItemSold / count($skuKeyword);
            self::_dataSort($skuKeyword, 1, $typeSort == 'desc' ? 1 : 0);
            $dataResult[$keyword] = $skuKeyword;
            $dataResult[$keyword]['averageItemSold'] = $averageItemSold;
            $dataResult[$keyword]['keyword'] = $keyword;
        }

        self::_dataSort($dataResult, 'averageItemSold', $typeSort == 'desc' ? 1 : 0);

        $limit = 0;
        $result = [];
        foreach ($dataResult as $data) {
            $keyword = $data['keyword'];
            if ($limit < 5) {
                foreach ($data as $key => $item) {
                    if (is_int($key)) {
                        $itemId = $item[0];
                        $itemSold = $item[1];
                        $keywordProductId = $item[2];
                        $productId = $item[3];
                        $sku = $item[4];
                        $priceLimit = $item[5];
                        $adsId= $item[6];
                        $skuName = $item[7];
                        $dataItem['itemId'] = $itemId;
                        $dataItem['itemSold'] = $itemSold;
                        $dataItem['kwpId'] = $keywordProductId;
                        $dataItem['skuName'] = $skuName;
                        $dataItem['sku'] = $sku;
                        $dataItem['productId'] = $productId;
                        $dataItem['priceLimit'] = $priceLimit;
                        $dataItem['adsId'] = $adsId;
                        $result[$limit][] = $dataItem;
                    }
                }
                $result[$limit]['keyword'] = $keyword;
            }
            $limit++;
        }
        if (!empty($result)) {
            MakRecommendationSkSelect::batchInsert([
                MakRecommendationSkSelect::COL_FK_SHOP_CHANNEL => $this->shopChannelId,
                MakRecommendationSkSelect::COL_MAK_RECOMMENDATION_SK_SELECT_JSON => json_encode($result),
                MakRecommendationSkSelect::COL_MAK_RECOMMENDATION_SK_SELECT_CREATED_AT => strtotime('now'),
            ],Manager::SQL_MASTER_CONNECT);
        }

        return $result;
    }

    /**
     * @param array $array
     * @param string $field
     * @param int $type : 0 - ASC / 1 - DESC
     * @return void
     */
    private function _dataSort(array &$array, $field, $type = 0)
    {
        $field = trim($field);
        usort($array, function ($a, $b) use ($field, $type) {
            if ($a[$field] == $b[$field]) {
                return 0;
            }
            if ($type == 0) {
                return ($a[$field] > $b[$field]) ? 1 : -1;
            } else {
                return ($a[$field] < $b[$field]) ? 1 : -1;
            }
        });
    }

    public function runSavePriceRange()
    {
//        $dataSK = json_decode('[{"0": {"kwpId": "43325", "itemId": "1617870178", "skuName": "Nước Hoa Hồng Dưỡng Ẩm AHC Premium Hydra B5 Toner (120ml)", "itemSold": "258"}, "1": {"kwpId": "56789", "itemId": "2769563654", "skuName": "Bộ Trail Kit Sản Phẩm Dưỡng Ẩm AHC Premium Hydra B5 (5 Món)", "itemSold": "124"}, "2": {"kwpId": "40926", "itemId": "1658836961", "skuName": "Mặt Nạ Cấp Nước AHC Premium Hydra (27ml) x 1", "itemSold": "72"}, "3": {"kwpId": "41092", "itemId": "2115030669", "skuName": "Mặt Nạ Trắng Da Cellulose AHC Vital C Complex Cellulose (27ml)", "itemSold": "68"}, "4": {"kwpId": "42937", "itemId": "1617870356", "skuName": "Kem Chống Nắng Dạng Thanh AHC Natural Perfection SPF 50PA ++++ (14g)", "itemSold": "55"}, "5": {"kwpId": "42816", "itemId": "1617870265", "skuName": "[DEAL ĐỘC QUYỀN] Sữa Dưỡng Ẩm AHC Premium Hydra B5 Lotion (120ml)", "itemSold": "51"}, "6": {"kwpId": "42403", "itemId": "1617870382", "skuName": "Kem Chống Nắng Dưỡng Ẩm AHC Natural Perfection (50ml)", "itemSold": "26"}, "7": {"kwpId": "41171", "itemId": "2115030626", "skuName": "Mặt Nạ Tái Tạo Da AHC Phyto Complex Cellulose (27ml)", "itemSold": "26"}, "8": {"kwpId": "41141", "itemId": "1617870284", "skuName": "Kem Dưỡng Da Cấp Ẩm Và Chống Lão Hoá AHC Premium Hydra B5 (50ml)", "itemSold": "13"}, "9": {"kwpId": "43619", "itemId": "1617870258", "skuName": "Xịt Khoáng Dưỡng Ẩm AHC Premium Hydra B5 Soothing Mist (60ml)", "itemSold": "5"}, "keyword": "ahc"}]',true);
        $dataSK = $this->getSkuKeyword();
        if (!empty($dataSK)) {
            try {
                foreach ($dataSK as $SK) {
                    $kw = $SK['keyword'];
                    foreach ($SK as $i => $item) {
                        if ($i === 'keyword') {
                            continue;
                        }
                        $itemId = $item['itemId'];
                        $kwpId = $item['kwpId'];
                        $arrPrice = $this->getPriceRange($kw, $this->shopId, $itemId, $this->machine);
                        if (!empty($arrPrice)) {
                            MakProgrammaticPriceRange::batchInsert([
                                MakProgrammaticPriceRange::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT => $kwpId,
                                MakProgrammaticPriceRange::COL_FK_SHOP_CHANNEL => $this->shopChannelId,
                                MakProgrammaticPriceRange::COL_MAK_PROGRAMMATIC_PRICE_RANGE_JSON => json_encode($arrPrice),
                                MakProgrammaticPriceRange::COL_MAK_PROGRAMMATIC_PRICE_RANGE_DATE => date('Y-m-d'),
                                MakProgrammaticPriceRange::COL_MAK_PROGRAMMATIC_PRICE_RANGE_CREATED_BY => 2,
                                MakProgrammaticPriceRange::COL_MAK_PROGRAMMATIC_PRICE_RANGE_CREATED_AT => strtotime('now'),
                            ],Manager::SQL_MASTER_CONNECT);
                        }
                    }
                }
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    private function changePosition($lastDataTime, $follow)
    {
        $pickRang[] = $lastDataTime;
        $range = [];
        foreach ($lastDataTime as $j => $item) {
            if ($lastDataTime[2] == $follow) {

            } else {
                if ($j == 2) {
                    $item = $follow;
                } else if ($j > 2) {
                    if ((21 > $item) && ($item >= $follow)) {
                        $item = $item + 1;
                    }
                }
            }
            array_push($range, $item);
        }
        return $range;
    }

    public function getPriceRange($keyword, $shopId, $itemId, $type)
    {
        $temp = [];
        $sr = new Recommendation($keyword, $shopId, $itemId);
        $data = $sr->run();
        if (!empty($data)) {
            $path = storage_path() . '/csv/data-' . $shopId . '-' . $type . '.csv';
            $fp = fopen($path, 'w');
            foreach ($data as $fields) {
                fputcsv($fp, $fields);
            }
            fclose($fp);
            $lastDataTime = end($data);
            $dataPickRange = [];
            foreach ($lastDataTime as $i => $item) {
                if ($item == '' || $item == null) {
                    $item = 21;
                }
                if ($i > 1) {
                    array_push($dataPickRange, $item);
                }
            }
            $follow1 = $this->changePosition($dataPickRange, 1);
            $follow10 = $this->changePosition($dataPickRange, 10);
            $pickRange[] = $dataPickRange;
            $pickRange[] = $follow1;
            $pickRange[] = $follow10;
            $path = storage_path() . '/csv/pickRange-' . $shopId . '-' . $type . '.csv';
            $fo = fopen($path, 'w');
            foreach ($pickRange as $item) {
                fputcsv($fo, $item);
            }
            fclose($fo);
            $namePY = '/shopeeKeyword-' . $shopId . '-' . $type . '.py';
            $pathNewPY = storage_path() . $namePY;
            if (!file_exists($pathNewPY)) {
                $pathPY = storage_path() . '/shopeeKeyword.py';
                $current = file_get_contents($pathPY);
                $current = str_replace('%nameData%', 'storage/csv/data-' . $shopId . '-' . $type . '.csv', $current);
                $current = str_replace('%namePick%', 'storage/csv/pickRange-' . $shopId . '-' . $type . '.csv', $current);
                $fp = fopen($pathNewPY, 'w');
                file_put_contents($pathNewPY, $current);
            }
            $priceArr = shell_exec('python3 storage' . $namePY);
            $resStr = str_replace('[', '', $priceArr);
            $resStr = str_replace(']', '', $resStr);
            $temp = explode(' ', $resStr);
        }
        return $temp;
    }

    /**
     * @param $adsId
     * @param $keywordData
     * @example {"ahc": [555, 123], "toner": [555, 123]}
     * @return bool
     */
    public function modifyBiddingPrice($adsId, $keywordData)
    {
        $shopChannelInfo = ShopChannel::getById($this->shopChannelId);
        $ventureId = $shopChannelInfo->{Channel::COL_FK_VENTURE};
        Common::setTimezoneByVentureId($ventureId);
        $crawler = new Shopee($this->shopChannelId);

        $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();

        $ch = curl_init();
        $randomString = Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $crawler->shopeeSellerDomain() . "/api/v2/pas/campaign/?campaignid={$adsId}&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $crawler->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $crawler->cookiePath);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $resultCampaignAds = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $keywordArray = $resultCampaignAds['data']['advertisements'][0]['extinfo']['keywords'] ?? [];
        if (!$keywordArray) return false;
        $inputKeywordArray = array_keys($keywordData);
        foreach ($keywordArray as &$keywordInfo) {
            if (!in_array($keywordInfo['keyword'], $inputKeywordArray)) continue;
            $keywordInfo['price'] = $keywordData[$keywordInfo['keyword']][0];
        }
        $resultCampaignAds['data']['advertisements'][0]['extinfo']['keywords'] = $keywordArray;
        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;
        $payload = [
            'ads_audit_event' => null,
            'campaign_ads_list' => [$resultCampaignAds['data']]
        ];
        $ch = curl_init();
        $randomString = Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $crawler->shopeeSellerDomain() . "/api/v2/pas/campaign/?campaignid={$adsId}&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $crawler->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $crawler->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $crawler->shopeeSellerDomain());
        curl_setopt($ch, CURLOPT_COOKIEJAR, $crawler->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $crawler->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        curl_close($ch);
//        if (php_sapi_name() == 'cli')
////            echo "\n" . _FUNCTION_ . "\n$result\n";
//            echo 'DONE'.PHP_EOL;
        $result = json_decode($result, true);

        $success = false;

        if(isset($result['err_msg']) && $result['err_msg'] == 'success') {
            MakRecommendationLogBiddingPrice::insert(
                [
                    MakRecommendationLogBiddingPrice::COL_FK_SHOP_CHANNEL => $this->shopChannelId,
                    MakRecommendationLogBiddingPrice::COL_FK_ADS_ID => $adsId,
                    MakRecommendationLogBiddingPrice::COL_MAK_RECOMMENDATION_LOG_BIDDING_PRICE_KEYWORD_JSON => json_encode($keywordData),
                    MakRecommendationLogBiddingPrice::COL_MAK_RECOMMENDATION_LOG_BIDDING_PRICE_CREATED_AT => strtotime('now'),
                ]
            );

            MakRecommendationBiddingPrice::updateOrCreate(
                [   MakRecommendationBiddingPrice::COL_FK_SHOP_CHANNEL => $this->shopChannelId,
                    MakRecommendationBiddingPrice::COL_FK_ADS_ID => $adsId
                ],
                [
                    MakRecommendationBiddingPrice::COL_MAK_RECOMMENDATION_BIDDING_PRICE_KEYWORD_JSON => json_encode($keywordData),
                    MakRecommendationBiddingPrice::COL_MAK_RECOMMENDATION_BIDDING_PRICE_CREATED_AT => strtotime('now'),
                ]
            );

            $success = true;
        }

        return $success;
    }
}