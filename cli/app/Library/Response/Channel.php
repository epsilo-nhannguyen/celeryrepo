<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:36
 */

namespace App\Library\Response;


use Exception;
use App\Library;


class Channel
{
    /**
     * @var bool
     */
    protected $isSuccess = true;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var array
     */
    protected $orders;

    /**
     * @param bool $isSuccess
     */
    public function setIsSuccess($isSuccess)
    {
        $this->isSuccess = $isSuccess;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param array $arrayOrder
     * @throws Exception
     */
    public function setOrders($arrayOrder)
    {
        foreach ($arrayOrder as $order) {
            if (!$order instanceof Library\DataStructure\ChannelOrder) {
                throw new Exception('$arrayOrder item must be '.Library\DataStructure\ChannelOrder::class);
            }
        }

        $this->orders = $arrayOrder;
    }
}