<?php


namespace App\Library\Algorithm\ShuntingYard;


class Number extends TerminalExpression
{
    public function operate(Stack $stack)
    {
        return $this->value;
    }

    public function getPrecedence()
    {
        return null;
    }
}