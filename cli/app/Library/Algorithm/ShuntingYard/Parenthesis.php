<?php


namespace App\Library\Algorithm\ShuntingYard;


class Parenthesis extends TerminalExpression
{
    protected $precedence = 7;

    public function operate(Stack $stack)
    {
        // TODO: Implement operate() method.
    }

    /**
     * @return int
     */
    public function getPrecedence(): int
    {
        return $this->precedence;
    }

    public function isNoOp()
    {
        return true;
    }

    public function isParenthesis()
    {
        return true;
    }

    public function isOpen()
    {
        return $this->value == '(';
    }
}