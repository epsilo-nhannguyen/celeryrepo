<?php


namespace App\Library\Algorithm\ShuntingYard;


class Power extends Operator
{
    protected $precedence = 6;

    public function operate(Stack $stack)
    {
        $left = $stack->pop()->operate($stack);
        $right = $stack->pop()->operate($stack);
        return pow($right , $left);
    }
}