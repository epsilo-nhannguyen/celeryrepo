<?php


namespace App\Library\Algorithm\ShuntingYard;


class Subtraction extends Operator
{
    protected $precedence = 4;

    public function operate(Stack $stack)
    {
        $left = $stack->pop()->operate($stack);
        $right = $stack->pop()->operate($stack);

        return $right - $left;
    }
}