<?php


namespace App\Library\Algorithm\ShuntingYard;


class Stack
{
    protected $data = [];

    public function push($element)
    {
        $this->data[] = $element;
    }

    /**
     * @return Operator
     */
    public function poke()
    {
        return end($this->data);
    }

    /**
     * @return Operator
     */
    public function pop()
    {
        return array_pop($this->data);
    }
}