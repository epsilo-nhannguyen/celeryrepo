<?php


namespace App\Library\Algorithm\ShuntingYard;


class Division extends Operator
{
    protected $precedence = 5;

    public function operate(Stack $stack)
    {
        $left = $stack->pop()->operate($stack);
        $right = $stack->pop()->operate($stack);
        return $left != 0 ? $right / $left : null;
    }
}