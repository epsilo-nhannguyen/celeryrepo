<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 11:22
 */


namespace App\Library;


class QueueName
{

    /**
     * get By Class
     * @param string $class
     * @return string
     */
    public static function getByClass($class)
    {
        $configQueue = require dirname(__DIR__, 2).'/config/queue.php';
        $queueName = $configQueue['handlerQueue'][$class];

        return $queueName;
    }

    /**
     * @var string
     */
    const TEST_PING_PONG = 'test_ping_pong';
    const DO_SEND_MAIL = 'do_send_mail';
    const RAW_CATEGORY = 'raw_category';
    const INIT_SHOP_DATA = 'init_shop_data';
    const RAW_KEYWORD_PRODUCT = 'raw_keyword_product';
    const RAW_CRAWLER_ADS = 'raw_crawler_ads';
    const RAW_RANKING_KEYWORD = 'raw_ranking_keyword';
    const PULL_PRODUCT = 'pull_product';
    const PULL_ORDER = 'pull_order';
    const RAW_KEYWORD_SUGGEST_SHOPEE = 'raw_keyword_suggest_shopee';
    const RAW_KEYWORD_MACHINE_SHOPEE = 'raw_keyword_machine_shopee';
    const RAW_KEYWORD_BIDDING_SHOPEE = 'raw_keyword_bidding_shopee';

    const RAW_CRAWL_SHOP_ADS_KEYWORD = 'raw_crawl_shop_ads_keyword';
    const RAW_CRAWL_SHOP_ADS_KEYWORD_PERFORMANCE = 'raw_crawl_shop_ads_keyword_performance';

    /**
     * @var string
     */
    const SHOPEE_KEYWORD_BIDDING_CRAWL_PERFORMANCE_HOUR = 'shopee_keyword_bidding_crawl_performance_hour';
    const SHOPEE_KEYWORD_BIDDING_CRAWL_PERFORMANCE_HOUR_BY_SHOP = 'shopee_keyword_bidding_crawl_performance_hour_by_shop';
    const CALCULATE_DASHBOARD_SHOP_INTRADAY = 'calculate_dashboard_shop_intraday';
    const CALCULATE_DASHBOARD_SHOP_DAILY = 'calculate_dashboard_shop_daily';
    const SHOPEE_SHOPADS_PUSH_DATA_DASHBOARD = 'shopee_shopads_push_data_dashboard';

    #region Tokopedia
    const CRAWL_TOKOPEDIA_LIST_PRODUCT = 'crawl_tokopedia_list_product';
    const CRAWL_TOKOPEDIA_GROUP_ADS = 'crawl_tokopedia_group_ads';
    const CRAWL_TOKOPEDIA_PRODUCT_ADS = 'crawl_tokopedia_product_ads';
    const CRAWL_TOKOPEDIA_KEYWORD_ADS = 'crawl_tokopedia_keyword_ads';
    const CRAWL_TOKOPEDIA_GROUP_ADS_PERFORMANCE = 'crawl_tokopedia_group_ads_performance';
    const CRAWL_TOKOPEDIA_PRODUCT_ADS_PERFORMANCE = 'crawl_tokopedia_product_ads_performance';
    const CRAWL_TOKOPEDIA_KEYWORD_ADS_PERFORMANCE = 'crawl_tokopedia_keyword_ads_performance';

    const TOKO_KEYWORD_ADS_CRAW_PRODUCT = 'toko_keyword_ads_craw_product';
    const TOKO_KEYWORD_ADS_IMPORT_PRODUCT = 'toko_keyword_ads_import_product';
    const TOKO_KEYWORD_ADS_CRAW_CAMPAIGN = 'toko_keyword_ads_craw_campaign';
    const TOKO_KEYWORD_ADS_IMPORT_CAMPAIGN = 'toko_keyword_ads_import_campaign';
    const TOKO_KEYWORD_ADS_CRAW_SKU = 'toko_keyword_ads_craw_sku';
    const TOKO_KEYWORD_ADS_IMPORT_SKU = 'toko_keyword_ads_import_sku';
    const TOKO_KEYWORD_ADS_CRAW_KEYWORD = 'toko_keyword_ads_craw_keyword';
    const TOKO_KEYWORD_ADS_IMPORT_KEYWORD = 'toko_keyword_ads_import_keyword';

    const TOKO_KEYWORD_ADS_CRAW_KEYWORD_PERFORMANCE = 'toko_keyword_ads_craw_keyword_performance';
    const TOKO_KEYWORD_ADS_IMPORT_KEYWORD_PERFORMANCE = 'toko_keyword_ads_import_keyword_sku_performance';

    const TOKO_KEYWORD_ADS_CRAW_CAMPAIGN_PERFORMANCE = 'toko_keyword_ads_craw_campaign_performance';
    const TOKO_KEYWORD_ADS_IMPORT_CAMPAIGN_PERFORMANCE = 'toko_keyword_ads_import_campaign_performance';

    const TOKO_KEYWORD_ADS_CRAW_SKU_PERFORMANCE = 'toko_keyword_ads_craw_sku_performance';
    const TOKO_KEYWORD_ADS_IMPORT_SKU_PERFORMANCE = 'toko_keyword_ads_import_sku_performance';

    #endregion

    const CALCULATE_DASHBOARD_SHOP_INTRADAY_1 = 'calculate_dashboard_shop_intraday_1';
    const CALCULATE_DASHBOARD_SHOP_INTRADAY_2 = 'calculate_dashboard_shop_intraday_2';
    const CALCULATE_DASHBOARD_SHOP_INTRADAY_3 = 'calculate_dashboard_shop_intraday_3';
}