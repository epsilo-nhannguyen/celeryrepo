<?php

namespace App\Library;

use Carbon\Carbon;

class LogError
{
    const SLACK_URL = 'https://hooks.slack.com/services/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';
    const SLACK_ROOM = 'epsilo-m';
    private static $_instance;
    private $ch;

    /**
     * @return LogError
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * LogError constructor.
     */
    public function __construct()
    {
        $this->ch = curl_init(self::SLACK_URL);
    }

    /**
     * LogError destruct.
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }

    /**
     * @param $error // Exception or message want to log
     */
    public static function logErrorToTextFile($error)
    {
        $dateTime = Carbon::now("Asia/Ho_Chi_Minh");
        $pathFile = SYS_PATH . "/storage/app/log";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/log/")) mkdir(SYS_PATH . "/storage/app/log/");
        $message = 'Error at: ' . $dateTime . PHP_EOL;
        $message .= 'Content: ' . json_encode($error) . PHP_EOL;
        $message .= '##########################' . PHP_EOL;
        file_put_contents($pathFile . '/log_' . $dateTime->format('Y-m-d') . '.txt', $message, FILE_APPEND);
    }

    /**
     * @param $message
     * @param string $room
     * @return bool|string
     */
    public function slack($message, $room = self::SLACK_ROOM)
    {
        $room = ($room) ? $room : self::SLACK_ROOM;
        $data = "payload=" . json_encode(array(
                "channel" => "#{$room}",
                "text" => $message,
                "icon_emoji" => ':ghost:'
            ));

        // You can get your webhook endpoint from your Slack settings
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($this->ch);

        if($result != 'ok') {
            $data = 'TypeError: Push Slack Failure!' . PHP_EOL . 'Content: ' . $message;
            self::logErrorToTextFile($data);
        }

        return $result;
    }
}