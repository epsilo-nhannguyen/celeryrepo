<?php

namespace App\Library\Model\Sql;



class Manager
{
    const SQL_MASTER_CONNECT = 'master_business';
    const SQL_STAT_CONNECT = 'master_stat';
    const SQL_CHART_CONNECT = 'master_chart';
    const SQL_CHART_SHOP_ADS_CONNECT = 'master_chart_shop_ads';
    const SQL_BI_CONNECT = 'bi';

}