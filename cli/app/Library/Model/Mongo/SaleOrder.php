<?php

namespace App\Library\Model\Mongo;

use App\Library\Application\Functions\Format;
use App\Library\Application\Functions\Times;
use App\Library\Model\Mongo\Manager;

class SaleOrder
{

    /**
     * @param int $shopId
     * @param int $channelId
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function getMaxUpdatedAt($shopId, $channelId)
    {
        $shopId = intval($shopId);
        $channelId = intval($channelId);

        $result = Manager::getInstance()->max(
            'sale_order',
            [
                'shopId' => $shopId,
                'channelId' => $channelId,
            ],
            'updateAt'
        );

        return $result;
    }

    public static function getMinCreatedAt() {
        $result = Manager::getInstance()->min('sale_order', 'data.createdAt');
    }

    /**
     * get Data Sale Order Date From To
     * @param array $shopIds
     * @param int $dateFrom
     * @param int $dateTo
     * @return mixed|null
     */
    public static function getDataSaleOrderDateFromTo($shopIds, $dateFrom, $dateTo)
    {
        try {
            $match = [
                'shopId' => ['$in' => array_map('intval', (array)$shopIds)],
                'data.createdAt' => [
                    '$gte' => intval($dateFrom),
                    '$lte' => intval($dateTo)
                ],
            ];

            return Manager::getInstance()->aggregate(
                'sale_order',
                [
                    ['$match' => $match],
                    ['$group' => [
                        '_id' => null,
                        'totalItem' => ['$sum' => ['$size' => '$data.arrayItem']],
                        'totalPrice' => ['$sum' => ['$sum' => '$data.arrayItem.itemPrice']],
                        'totalOrder' => ['$sum' => 1],
                    ]]
                ]
            );
        } catch (\MongoDB\Driver\Exception\Exception $e) {
            return null;
        }
    }

    /*db.getCollection('sale_order').aggregate([
        {$match:
            {'data.createdAt': {$gte: 1570461155, $lte: 1570463755} }},
    {$unwind: '$data.arrayItem'},
    {$group:{_id: '$data.arrayItem.itemId' }},
    {$group:{_id: null, total: {$sum:1} }},
    ]);*/

    /**
     * get Data Sale Order Date From To
     * @param array $shopIds
     * @param int $dateFrom
     * @param int $dateTo
     * @return mixed|null
     */
    public static function countUniqueItemIdDateFromTo($shopIds, $dateFrom, $dateTo)
    {
        try {
            $match = [
                'shopId' => ['$in' => array_map('intval', (array)$shopIds)],
                'data.createdAt' => [
                    '$gte' => intval($dateFrom),
                    '$lte' => intval($dateTo)
                ],
            ];

            return Manager::getInstance()->aggregate(
                'sale_order',
                [
                    ['$match' => $match],
                    ['$unwind' => '$data.arrayItem'],
                    ['$group' => [
                        '_id' => '$data.arrayItem.itemId',
                    ]],
                    ['$group' => [
                        '_id' => null,
                        'totalSKu' => ['$sum' => 1],
                    ]]
                ]
            );
        } catch (\MongoDB\Driver\Exception\Exception $e) {
            return null;
        }
    }

    /**
     * get Data Sale Order Date From To
     * @param array $shopIds
     * @param int $dateFrom
     * @param int $dateTo
     * @return mixed|null
     */
    public static function countItemSoldPerItemIdByArrayShopId($shopIds, $dateFrom=null, $dateTo=null)
    {
        try {
            $match = [
                'shopId' => ['$in' => array_map('intval', (array)$shopIds)]
            ];

            if ($dateFrom && $dateTo) {
                $match['data.createdAt'] = [
                    '$gte' => intval($dateFrom),
                    '$lte' => intval($dateTo)
                ];
            }

            return Manager::getInstance()->aggregate(
                'sale_order',
                [
                    ['$match' => $match],
                    ['$unwind' => '$data.arrayItem'],
                    ['$group' => [
                        '_id' => '$data.arrayItem.itemId',
                        'totalItemSold' => ['$sum' => 1],
                    ]],
                ]
            );
        } catch (\MongoDB\Driver\Exception\Exception $e) {
            return null;
        }
    }
}