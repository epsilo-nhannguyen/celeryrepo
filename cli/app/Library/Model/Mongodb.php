<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 03/10/2019
 * Time: 17:46
 */

namespace App\Library\Model;

class Mongodb
{
    private static $connections = [];
    private static $configs = [];

    /**
     * @throws \Exception
     */
    public static function purge() {
        self::$configs = config('database.mongodb');
    }

    /**
     * @param string $connectionName
     * @return mixed
     * @throws \Exception
     */
    public static function getConfig($connectionName) {
        if (!self::$configs) {
            self::$configs = config('database.mongodb');
        }
        if (!isset(self::$configs[$connectionName])) {
            throw new \Exception("Undefined mongo connection $connectionName");
        }
        return self::$configs[$connectionName];
    }

    /**
     * @param $connectionName
     * @return static
     * @throws \Exception
     */
    private static function initConnection($connectionName) {
        self::$connections[$connectionName] = new static($connectionName);
        return self::$connections[$connectionName];
    }

    /**
     * @param null $connectionName
     * @return static
     * @throws \Exception
     */
    public static function connection($connectionName = null) {
        $connectionName = $connectionName ?? env('MONGO_CONNECTION', 'kernel');
        return self::$connections[$connectionName] ?? self::initConnection($connectionName);
    }

    /**
     * Mongodb constructor.
     * @param $connectionName
     * @throws \Exception
     */
    private function __construct($connectionName) {
        $config = self::getConfig($connectionName);
        $host = $config['host'];
        $port = $config['port'];
        $database = $config['database'];
        $username = $config['username'];
        $password = $config['password'];
        $uri = 'mongodb://'.$host.':'.$port;
        $uriOptions = [];
        $username ? $uriOptions['username'] = $username : null;
        $password ? $uriOptions['password'] = $password : null;
        $database ? $uriOptions['authSource'] = $database : null;
        $this->database = $database;
        $this->manager = new \MongoDB\Driver\Manager($uri, $uriOptions);
    }
    
    protected $manager;
    protected $database;

    /**
     * @param $collectionName
     * @param $document
     * @return \MongoDB\Driver\WriteResult
     */
    public function insert($collectionName, $document)
    {
        $bulk = new \MongoDB\Driver\BulkWrite;
        $bulk->insert($document);
        $result = $this->manager->executeBulkWrite($this->database.'.'.$collectionName, $bulk);
        return $result;
    }

    /**
     * @param $collectionName
     * @param $filter
     * @param $data
     * @param $option ['multi' => false, 'upsert' => false]
     * @param $method
     * @return \MongoDB\Driver\WriteResult
     */
    public function update($collectionName, $filter, $data, $option = [], $method = '$set')
    {
        $bulk = new \MongoDB\Driver\BulkWrite;
        $bulk->update(
            $filter,
            [$method => $data],
            $option
        );
        $result = $this->manager->executeBulkWrite($this->database.'.'.$collectionName, $bulk);
        return $result;
    }

    /**
     * @param $collectionName
     * @param $filter
     * @param $column
     * @param $options
     * @return \MongoDB\Driver\Cursor
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function select($collectionName, $filter, $column = [], $options = [])
    {
        if ($column) {
            $options['projection'] = $column;
        }
        $query = new \MongoDB\Driver\Query($filter, $options);
        $rows = $this->manager->executeQuery($this->database . '.' . $collectionName, $query); // $mongo contains the connection object to MongoDB
        return $rows;
    }

    /**
     * @param $collectionName
     * @param $filter
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function count($collectionName, $filter)
    {
        $command = new \MongoDB\Driver\Command(['count' => $collectionName, 'query' => $filter]);
        $obj = $this->manager->executeCommand($this->database, $command); // $mongo contains the connection object to MongoDB
        $obj = current($obj->toArray());
        return $obj->n;
    }

    /**
     * @param $collectionName
     * @param $filter
     * @param $columnMax
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function max($collectionName, $filter, $columnMax)
    {
        $query = ['$group' => ['_id' => null, 'max' => ['$max' => '$'.$columnMax]]];
        if ($filter) {
            $match = ['$match' => $filter];
            $pipeline = [$match, $query];
        } else {
            $pipeline = [$query];
        }
        $command = new \MongoDB\Driver\Command([
            'aggregate' => $collectionName,
            'pipeline' => $pipeline,
            'cursor' => ['batchSize' => 1]
        ]);
        $rows = $this->manager->executeCommand($this->database, $command)->toArray();
        $rows = (array) ($rows[0] ?? []);
        return $rows['max'] ?? 0;
    }

    /**
     * @return \MongoDB\Driver\Manager
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Insert multi rows
     * @param string $collectionName
     * @param array $documentArray
     * @return \MongoDB\Driver\WriteResult
     * @throws \Exception
     */
    public function batchInsert($collectionName, $documentArray)
    {
        $bulk = new \MongoDB\Driver\BulkWrite;
        foreach ($documentArray as $key => $document) {
            if (!is_int($key)) {
                throw new \Exception(static::class.' Please format correctly!');
            }
        }
        foreach ($documentArray as $document){
            $bulk->insert($document);
        }
        $result = $this->manager->executeBulkWrite($this->database . '.' . $collectionName, $bulk);
        return $result;
    }

    /**
     * @param $collectionName
     * @param $pipeline
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function aggregate($collectionName, $pipeline)
    {
        $command = new \MongoDB\Driver\Command([
            'aggregate' => $collectionName,
            'pipeline' => $pipeline,
            'cursor' => ['batchSize' => 10000]
        ]);
        $rows = $this->manager->executeCommand($this->database, $command)->toArray();
        return $rows;
    }
}