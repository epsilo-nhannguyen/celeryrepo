<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 11:19
 */

namespace App\Library;


use App\Application\Functions\Formatter;
use App\Models\MakRecommendationPriceRange;

class Formater
{

    public static function stdClassToArray($stdClass)
    {
        return json_decode(json_encode($stdClass), true);
    }

    /**
     * format date yyyy-mm-dd
     * @param string $date
     * @return false|string
     */
    public static function date($date)
    {
        return date('Y-m-d', strtotime(trim($date)));
    }

    /**
     * date Slash To Hyphen (dd/mm/yyyy to yyyy-mm-dd)
     * @param string $date
     * @return string
     */
    public static function dateSlashToHyphen($date)
    {
        $dateArray = explode('/', $date);
        return implode('-', [$dateArray[2], $dateArray[1], $dateArray[0]]);
    }

    /**
     * format datetime yyyy-mm-dd H:i:s
     * @param string $datetime
     * @return false|string
     */
    public static function datetime($datetime)
    {
        return date('Y-m-d H:i:s', strtotime(trim($datetime)));
    }

    /**
     * format date yyyy-mm-dd
     * @param $timestamp
     * @return false|string
     */
    public static function timeStampToDate($timestamp)
    {
        return date('Y-m-d', $timestamp);
    }

    public static function convertTotalRangePricePerPercent($array)
    {
        $percent90 = 0;
        $percent75 = 0;
        $percent60 = 0;
        $totalItem = count($array);

        foreach ($array as $item) {
            $array = json_decode($item, true);
            $first = array_pop($array);
            $last = $array[0];
            if ($first > $last) {
                $min = $last;
                $max = $first;
            } else {
                $min = $first;
                $max = $last;
            }
            $percent90 += $max;
            $percent75 += ($min + $max) / 2;
            $percent60 += $min;
        }

        $arrayResult = [
            MakRecommendationPriceRange::PERCENT_90 => $totalItem > 0 ? Formatter::number($percent90 / $totalItem, 0) : 0,
            MakRecommendationPriceRange::PERCENT_75 => $totalItem > 0 ? Formatter::number($percent75 / $totalItem, 0) : 0,
            MakRecommendationPriceRange::PERCENT_60 => $totalItem > 0 ? Formatter::number($percent60 / $totalItem, 0) : 0,
        ];

        return $arrayResult;
    }

}