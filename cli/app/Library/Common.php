<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 11:20
 */

namespace App\Library;


use App\Models;


class Common
{

    /**
     * @var int
     */
    public static $isShowMessage = 0;

    /**
     * @param int $isShowMessage
     */
    public static function setIsShowMessage($isShowMessage)
    {
        self::$isShowMessage = $isShowMessage;
    }

    /**
     * show Message
     * @param mixed $message
     */
    public static function showMessage($message = '')
    {
        if (self::$isShowMessage) {
            if (is_array($message) || is_object($message)) {
                print_r($message);
            } else {
                echo $message;
            }
        }
    }

    public static function randomString($length = 8)
    {
        $characters = '123456789abcdefghjkmnpqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function strToHex($string)
    {
        $hex = '';
        for ($i=0; $i<strlen($string); $i++){
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0'.$hexCode, -2);
        }
        return strToUpper($hex);
    }

    public static function hexToStr($hex)
    {
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }

    public static function convert10SystemTo36System($number, &$result)
    {
        $mod = $number % 36;
        $newNumber = ($number - $mod) / 36;
        if ($mod > 9) {
            $mod = chr($mod - 10 + 65); // convert to A->Z
        }
        $result = $result.$mod;
        if ($newNumber > 0) {
            return self::convert10SystemTo36System($newNumber, $result);
        } else {
            return $result;
        }
    }

    /**
     * @return false|int
     */
    public static function getCurrentTimestamp()
    {
        return strtotime('now');
    }

    /**
     * get Current Datetime
     * @return false|string
     */
    public static function getCurrentDatetime()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * convert Datetime To Timestamp
     * @param string $value
     * @return false|int
     * example: $value = 2019-09-01 00:00:00 or 2019-09-01
     */
    public static function convertDatetimeToTimestamp($value)
    {
        $value = str_replace('/', '-', $value);
        return strtotime(trim($value));
    }

    /**
     * convert Timestamp To Datetime
     * @param int $value
     * @return false|string
     */
    public static function convertTimestampToDatetime($value)
    {
        return date('Y-m-d H:i:s', intval($value));
    }

    /**
     * convert Timestamp To Time
     * @param int $value
     * @return false|string
     */
    public static function convertTimestampToTime($value)
    {
        return date('H:i:s', intval($value));
    }

    /**
     * convert Timestamp To Date
     * @param int $value
     * @return false|string
     */
    public static function convertTimestampToDate($value)
    {
        return date('Y-m-d', intval($value));
    }

    /**
     * convert Timestamp To Hour
     * @param int $value
     * @return false|string
     */
    public static function convertTimestampToHour($value)
    {
        return date('H', intval($value));
    }

    /**
     * get Date From Datetime
     * @param string $value
     * @return false|string
     */
    public static function getDateFromDatetime($value)
    {
        return date('Y-m-d', strtotime(trim($value)));
    }

    /**
     * get Hour From Datetime
     * @param string $value
     * @return false|string
     */
    public static function getHourFromDatetime($value)
    {
        return date('H', strtotime(trim($value)));
    }

    /**
     * date From
     * @param string $value
     * @return false|int
     */
    public static function dateFrom($value)
    {
        $value = str_replace('/', '-', $value);
        $value = sprintf('%s 00:00:00', date('Y-m-d', strtotime(trim($value))));
        return strtotime($value);
    }

    /**
     * date To
     * @param string $value
     * @return false|int
     */
    public static function dateTo($value)
    {
        $value = str_replace('/', '-', $value);
        $value = sprintf('%s 23:59:59', date('Y-m-d', strtotime(trim($value))));
        return strtotime($value);
    }

    /**
     * Retrieve days of between date_from & date_to
     * @param string $date_from
     * @param string $date_to
     * @param string $step
     * @return array
     */
    public static function dayOfBetween($date_from, $date_to, $step = '+1 day')
    {
        $date_from = strtotime($date_from);
        $date_to = strtotime($date_to);
        $result = [];
        while ($date_from <= $date_to) {
            array_push($result, date('Y-m-d', $date_from));
            $date_from = strtotime($step, $date_from);
        }
        return $result;
    }

    /**
     * set Timezone By Venture Id
     * @param int $ventureId
     */
    public static function setTimezoneByVentureId($ventureId)
    {
        $timezone = 'Asia/Ho_Chi_Minh';
        $ventureData = Models\Venture::getAll();

        foreach ($ventureData as $ventureInfo) {
            if ($ventureId != $ventureInfo->{Models\Venture::COL_VENTURE_ID}) continue;

            $timezone = $ventureInfo->{Models\Venture::COL_VENTURE_TIMEZONE}; break;
        }

        date_default_timezone_set($timezone);
    }

    /**
     * set Timezone By Channel Id
     * @param int $channelId
     */
    public static function setTimezoneByChannelId($channelId)
    {
        $channelInfo = Models\Channel::getById($channelId);

        self::setTimezoneByVentureId($channelInfo->{Models\Channel::COL_FK_VENTURE});
    }

    /**
     * set Timezone
     * @param string $timezone
     */
    public static function setTimezone($timezone)
    {
        date_default_timezone_set($timezone);
    }

    /**
     * ranking Assoc Array
     * @param array $array
     * @return array
     */
    public static function rankingAssocArray(array $array) {
        $arrayUnique = array_unique($array);
        $arrayDuplicate = array_diff_key($array, $arrayUnique);

        $arrayProcess = $arrayUnique;
        arsort($arrayProcess);
        # Initival values
        $rank = 0;
        $hiddenRank = 0;
        $hold = null;
        foreach ($arrayProcess as $key => $value) {
            # Always increases hidden rank
            $hiddenRank ++;
            # If current value is lower than previous:
            # set new hold, and set rank to hidden rank.
            if ( is_null($hold) || $value < $hold ) {
                $rank = $hiddenRank;
                $hold = $value;
            }

            $array[$key] = $rank;
            foreach ($arrayDuplicate as $_key => $_value) {
                if ($_value != $value) continue;

                $array[$_key] = $rank;
                unset($arrayDuplicate[$_key]);
            }
        }

        return $array;
    }

}