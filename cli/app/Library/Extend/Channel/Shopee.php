<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 27/09/2019
 * Time: 15:50
 */

namespace App\Library\Extend\Channel;


use App\Library;
use App\Models;
use Exception;


class Shopee extends Library\Factory\Channel\Shopee
{
    /**
     * Shopee constructor.
     * @param $shopId
     * @param $channelId
     * @throws Exception
     */
    public function __construct($shopId = null, $channelId = null)
    {
        if (!$shopId || !$channelId) return;
        parent::__construct();
        $this->setShopId($shopId);
        $this->setChannelId($channelId);

        $shopInfo = Library\Formater::stdClassToArray(Models\ShopMaster::getById($shopId));
        if (!$shopInfo) {
            throw new Exception('shop Info not found');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($this->getShopId(), $this->getChannelId()));
        if (!$shopChannelInfo) {
            throw new Exception('Shop and channel be not linked');
        }


        $credential = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_API_CREDENTIAL];
        $this->setCredential($credential);
        $this->setShopId($this->getShopId());
        $this->setChannelId($this->getChannelId());
    }

    /**
     * @param int $updateFrom
     * @param int $updateTo
     * @return array
     */
    public function catchupListingProduct($updateFrom, $updateTo)
    {
        $numTry = 0;
        $result = [];
        do {
            $offset = intval($numTry * 100);
            $params = [
                'pagination_offset' => $offset,
                'pagination_entries_per_page' => 100
            ];
            if ($updateFrom) {
                $params['update_time_from'] = $updateFrom;
            }
            if ($updateTo) {
                $params['update_time_to'] = $updateTo;
            }
            $response = $this->execute('/items/get', 'POST', $params);
            $itemList = $response['items'];
            if (is_array($itemList)) {
                foreach ($itemList as $item) {
                    $result[] = $item;
                }
            }
            $numTry++;
            if (!isset($response['more'])) {
                $response['more'] = false;
            }
        } while ($response['more']);
        return $result;
    }

    /**
     * Get item detail
     * @param int $itemId
     * @return array
     */
    public function getItemDetail($itemId)
    {
        $itemId = intval($itemId);
        $params = [
            'item_id' => $itemId
        ];
        $response = $this->execute('/item/get', 'POST', $params);
        return $response;
    }

    public function getProductData()
    {
        $hashShopSkuToItem = [];
        $numTry = 0;
        do {
            $response = $this->_getItemList($numTry * 100);
            $itemList = $response['items'];
            if (is_array($itemList)) {
                foreach ($itemList as $item) {
                    $itemDetailResponse = $this->_getItemDetail($item['item_id']);
                    $itemDetail = $itemDetailResponse['item'];
                    $hashShopSkuToItem[trim(strtoupper($item['item_sku']))][] = $itemDetail;
                }
            }
            $numTry++;
        } while ($response['more']);
        return $hashShopSkuToItem;
    }

    public function getProductData2()
    {
        $hashShopSkuToItem = [];
        $numTry = 0;
        do {
            $response = $this->_getItemList($numTry * 100);
            $itemList = $response['items'];
            if (is_array($itemList)) {
                foreach ($itemList as $item) {
                    $itemDetailResponse = $this->_getItemDetail($item['item_id']);
                    $itemDetail = $itemDetailResponse['item'];
                    $hashShopSkuToItem[] = $itemDetail;
                }
            }
            $numTry++;
        } while ($response['more']);
        return $hashShopSkuToItem;
    }

    public function getStoreInfo()
    {
        return $this->execute('/shop/get','POST', []);
    }

    /**
     * @param $arrayItemId
     * @return array
     */
    public function boost($arrayItemId)
    {
        $arrayItemId = (array) $arrayItemId;
        $arrayItemId = array_map('intval', $arrayItemId);
        $response = $this->execute('/items/boost', 'POST', ['item_id' => $arrayItemId]);
        return $response['batch_result']['successes'] ?? [];
    }
}