<?php

namespace App\Library\Selenium\Channel;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Cookie;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

class Tokopedia
{
    private $username;
    private $password;
    private $ventureId;

    /**
     * Tokopedia constructor.
     * @param $username
     * @param $password
     * @param $ventureId
     */
    public function __construct($username, $password, $ventureId)
    {
        $this->username = $username;
        $this->password = $password;
        $this->ventureId = $ventureId;
    }

    public function getBrowserCookies()
    {
        $host = env('SELENIUM_HOST', 'http://localhost:4444/wd/hub');
        $capabilities = DesiredCapabilities::firefox();
        $capabilities->setCapability(
            'moz:firefoxOptions',
            ['args' => ['-headless']]
        );

        $driver = RemoteWebDriver::create($host, $capabilities, 5000);

//        $capabilities = DesiredCapabilities::chrome();
//        $options = new ChromeOptions();
//
//        $options->addArguments(array(
////            '--incognito',
////            '--user-data-dir=selenium',
////            '--headless',
////            '--user-agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"'
//        ));
//
//        $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
//        $driver = RemoteWebDriver::create($host, $capabilities, 5000);

        $driver->get('https://accounts.tokopedia.com/login');
        $cookies = $driver->manage()->getCookies();
        $strCookies = null;
        $arrCookies = [];
        foreach ($cookies as $cookie) {
            $strCookie = $cookie->getName() . '=' . $cookie->getValue();
            array_push($arrCookies, $strCookie);
        }
        $strCookies = implode(';', $arrCookies);

        $strCookies = str_replace('lang=id', 'lang=en', $strCookies);

        $file = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");
        file_put_contents($file, $strCookies);

        $driver->quit();
    }

    public function loginByCookie()
    {
        $host = env('SELENIUM_HOST', 'http://localhost:4444/wd/hub');
        $capabilities = DesiredCapabilities::firefox();
        $capabilities->setCapability(
            'moz:firefoxOptions',
            ['args' => ['-headless']]
        );

        $driver = RemoteWebDriver::create($host, $capabilities, 5000);

        $driver->get('https://accounts.tokopedia.com/login');
        $driver->manage()->deleteAllCookies();
        // Check already has cookies or not
        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists($cookiesPath)) {
            throw new \Exception('Missing cookies to simulation login by Selenium');
        }
        $cookies = file_get_contents($cookiesPath);
        parse_str(strtr($cookies, array('&' => '%26', '+' => '%2B', ';' => '&')), $cookies);
        foreach ($cookies as $key => $value) {
            $ck = new Cookie($key, $value);
            $driver->manage()->addCookie($ck);
        }
        $driver->get('https://accounts.tokopedia.com/login');

        $driver->quit();
    }
}