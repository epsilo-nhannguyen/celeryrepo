<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 10/01/2020
 * Time: 19:13
 */

namespace App\Library;


use App\Models;


class CurrencyStat
{

    /**
     * @var string
     */
    const CURRENCY_CODE_USD = 'USD';

    /**
     * @var array
     */
    static $assocDataCurrency = [];

    /**
     * build Currency
     */
    private static function buildCurrencyArray()
    {
        if ( ! self::$assocDataCurrency) {
            $currencyData = Models\ModelStat\Currency::getAll();
            foreach ($currencyData as $currency) {
                $value = $currency->value;
                $date = $currency->time;
                $currencyCode = $currency->currency;
                self::$assocDataCurrency[$currencyCode][$date] = $value;
            }
        }
    }

    /**
     * get Currency Value
     * @param string $currencyCode
     * @param string $date
     * @return float
     */
    private static function getCurrencyValue($currencyCode, $date)
    {
        self::buildCurrencyArray();

        $currencyValue = self::$assocDataCurrency[$currencyCode][$date] ?? null;
        if ( ! $currencyValue) $currencyValue = end(self::$assocDataCurrency[$currencyCode]);

        return floatval($currencyValue);
    }

    /**
     * convert Currency
     * ex: convert Vietnam to Malaysia
     * ($currencyCodeFirst = VND, $currencyCodeSecond = MYR)
     * @param float $price
     * @param string $date
     * @param string $currencyCodeFirst
     * @param string $currencyCodeSecond
     * @return float
     */
    public static function convertCurrency($price, $date, $currencyCodeFirst, $currencyCodeSecond = self::CURRENCY_CODE_USD)
    {
        if ($currencyCodeFirst != $currencyCodeSecond) {
            $currencyFirst = self::getCurrencyValue($currencyCodeFirst, $date);
            $currencySecond = self::getCurrencyValue($currencyCodeSecond, $date);

            $price =  ($price / $currencyFirst) * $currencySecond;
        }

        return $price;
    }

}