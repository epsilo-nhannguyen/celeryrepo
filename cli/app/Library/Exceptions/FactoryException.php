<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:52
 */

namespace App\Library\Exceptions;


class FactoryException extends BaseException
{
    const MESSAGE_FORMAT = 'Identifier %s does not exist';
}