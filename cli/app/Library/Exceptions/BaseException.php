<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:51
 */

namespace App\Library\Exceptions;


class BaseException extends \Exception
{
    const MESSAGE_FORMAT = '';

    /**
     * @var array
     */
    protected $messageIngredient;

    /**
     * BaseException constructor.
     *
     * @param string|array   $messageIngredient
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($messageIngredient, $code = 0, \Throwable $previous = null)
    {
        if (is_scalar($messageIngredient)) {
            $messageIngredient = [$messageIngredient];
        }
        $this->messageIngredient = $messageIngredient;

        return parent::__construct($this->buildMessage(), $code, $previous);
    }

    /**
     * @return string
     */
    protected function buildMessage()
    {
        if(empty($this->messageIngredient)) {
            return self::MESSAGE_FORMAT;
        }

        return vsprintf(self::MESSAGE_FORMAT, $this->messageIngredient);
    }
}