<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:32
 */

namespace App\Library\DataStructure;


use Exception;
use App\Library;


class ChannelOrder
{
    /**
     * @var string
     */
    protected $orderNumber;

    /**
     * @var string
     */
    protected $orderId;

    /**
     * @var float
     */
    protected $voucherAmount;

    /**
     * @var string
     */
    protected $voucherCode;

    /**
     * @var float
     */
    protected $totalPrice;

    /**
     * @var int
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $addressCity;

    /**
     * @var string
     */
    protected $addressPhone;

    /**
     * @var string
     */
    protected $fullJson;

    /**
     * @var int
     */
    protected $channelStatusId;

    /**
     * @var array
     */
    protected $arrayItem;

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param string $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return float
     */
    public function getVoucherAmount()
    {
        return $this->voucherAmount;
    }

    /**
     * @param float $voucherAmount
     */
    public function setVoucherAmount($voucherAmount)
    {
        $this->voucherAmount = $voucherAmount;
    }

    /**
     * @return string
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }

    /**
     * @param string $voucherCode
     */
    public function setVoucherCode($voucherCode)
    {
        $this->voucherCode = $voucherCode;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * @param string $addressCity
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;
    }

    /**
     * @return string
     */
    public function getAddressPhone()
    {
        return $this->addressPhone;
    }

    /**
     * @param string $addressPhone
     */
    public function setAddressPhone($addressPhone)
    {
        $this->addressPhone = $addressPhone;
    }

    /**
     * @return string
     */
    public function getFullJson()
    {
        return $this->fullJson;
    }

    /**
     * @param string $fullJson
     */
    public function setFullJson($fullJson)
    {
        $this->fullJson = $fullJson;
    }

    /**
     * @return int
     */
    public function getChannelStatusId()
    {
        return $this->channelStatusId;
    }

    /**
     * @param int $channelStatusId
     */
    public function setChannelStatusId($channelStatusId)
    {
        $this->channelStatusId = $channelStatusId;
    }

    /**
     * @return array
     */
    public function getArrayItem()
    {
        return $this->arrayItem;
    }

    /**
     * @param $arrayItem
     * @throws Exception
     */
    public function setArrayItem($arrayItem)
    {
        foreach ($arrayItem as $item) {
            if (!$item instanceof Library\DataStructure\ChannelOrder\Item) {
                throw new Exception('$arrayItem must be array of '.Library\DataStructure\ChannelOrder\Item::class);
            }
        }
        $this->arrayItem = $arrayItem;
    }
}