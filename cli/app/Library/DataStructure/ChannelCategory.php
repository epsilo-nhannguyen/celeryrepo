<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:32
 */

namespace App\Library\DataStructure;


class ChannelCategory
{
    protected $categories = [];

    protected $channelIdentify = null;

    public function __construct($channelIdentify)
    {
        $this->channelIdentify = trim($channelIdentify);
    }

    /**
     * @return string
     */
    public function getChannelIdentify()
    {
        return strval($this->channelIdentify);
    }

    /**
     * @param $level
     * @param $value
     */
    public function setCategoryByLevel($level, $value)
    {
        $this->categories[$level] = $value;
    }

    /**
     * @param $level
     * @return mixed|string
     */
    public function getCategoryByLevel($level)
    {
        return $this->categories[$level] ?? '';
    }

    public function adjustCategory()
    {
        ksort($this->categories);
        $data = array_values(array_filter($this->categories));
        for ($i = 1; $i <= 5; $i++) {
            $this->setCategoryByLevel($i, $data[$i - 1] ?? '');
        }
    }
}