<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:33
 */

namespace App\Library\DataStructure;


class ChannelProduct
{
    /**
     * @var string
     */
    protected $sku;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $allocationStock;

    /**
     * @var int|null
     */
    protected $fbxStock;

    /**
     * @var float
     */
    protected $rrp;

    /**
     * @var float
     */
    protected $sellingPrice;

    /**
     * @var int
     */
    protected $active;

    /**
     * @var string
     */
    protected $channelIdentify;

    /**
     * @var string
     */
    protected $channelCategory;

    /**
     * @var int
     */
    protected $updatedAt;

    /**
     * @var int
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $rawData = '';

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = trim($sku);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getAllocationStock()
    {
        return $this->allocationStock;
    }

    /**
     * @param int $allocationStock
     */
    public function setAllocationStock($allocationStock)
    {
        $this->allocationStock = $allocationStock;
    }

    /**
     * @return int|null
     */
    public function getFbxStock()
    {
        return $this->fbxStock;
    }

    /**
     * @param int|null $fbxStock
     */
    public function setFbxStock($fbxStock)
    {
        $this->fbxStock = $fbxStock;
    }

    /**
     * @return float
     */
    public function getRrp()
    {
        return $this->rrp;
    }

    /**
     * @param float $rrp
     */
    public function setRrp($rrp)
    {
        $this->rrp = $rrp;
    }

    /**
     * @return float
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    /**
     * @param float $sellingPrice
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getChannelIdentify()
    {
        return $this->channelIdentify;
    }

    /**
     * @param string $channelIdentify
     */
    public function setChannelIdentify($channelIdentify)
    {
        $this->channelIdentify = trim($channelIdentify);
    }

    /**
     * @return string
     */
    public function getChannelCategory()
    {
        return trim($this->channelCategory);
    }

    /**
     * @param string $channelCategory
     */
    public function setChannelCategory($channelCategory)
    {
        $this->channelCategory = trim($channelCategory);
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param string $rawData
     */
    public function setRawData($rawData)
    {
        $this->rawData = $rawData;
    }

    /**
     * @return array
     */
    public function extractArray()
    {
        return [
            'sku' => $this->getSku(),
            'name' => $this->getName(),
            'allocationStock' => $this->getAllocationStock(),
            'fbxStock' => $this->getFbxStock(),
            'rrp' => $this->getRrp(),
            'sellingPrice' => $this->getSellingPrice(),
            'active' => $this->getActive(),
            'channelIdentify' => $this->getChannelIdentify(),
            'channelCategory' => $this->getChannelCategory(),
            'updatedAt' => $this->getUpdatedAt(),
            'createdAt' => $this->getCreatedAt(),
            'rawData' => $this->getRawData(),
        ];
    }
}