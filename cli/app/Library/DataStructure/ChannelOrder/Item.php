<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:31
 */

namespace App\Library\DataStructure\ChannelOrder;


class Item
{
    /**
     * @var string
     */
    protected $sku;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $channelSku;

    /**
     * @var float
     */
    protected $itemPrice;

    /**
     * @var string
     */
    protected $voucherCode;

    /**
     * @var float
     */
    protected $voucherAmount;

    /**
     * @var string
     */
    protected $itemId;

    /**
     * @var int
     */
    protected $createdAt;

    /**
     * @var int
     */
    protected $updatedAt;

    /**
     * @var int
     */
    protected $statusId;

    /**
     * @var string
     */
    protected $dateString;

    /**
     * @var string
     */
    protected $shipmentProvider;

    /**
     * @var string
     */
    protected $fullJson;

    /**
     * @var string
     */
    protected $internalId;

    /**
     * @var string
     */
    protected $shippingType;

    /**
     * @var int
     */
    protected $channelDeliveredAt;

    /**
     * @return int
     */
    public function getChannelDeliveredAt()
    {
        return $this->channelDeliveredAt;
    }

    /**
     * @param int $channelDeliveredAt
     */
    public function setChannelDeliveredAt($channelDeliveredAt)
    {
        $this->channelDeliveredAt = $channelDeliveredAt;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getChannelSku()
    {
        return $this->channelSku;
    }

    /**
     * @param string $channelSku
     */
    public function setChannelSku($channelSku)
    {
        $this->channelSku = $channelSku;
    }

    /**
     * @return float
     */
    public function getItemPrice()
    {
        return $this->itemPrice;
    }

    /**
     * @param float $itemPrice
     */
    public function setItemPrice($itemPrice)
    {
        $this->itemPrice = $itemPrice;
    }

    /**
     * @return string
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }

    /**
     * @param string $voucherCode
     */
    public function setVoucherCode($voucherCode)
    {
        $this->voucherCode = $voucherCode;
    }

    /**
     * @return float
     */
    public function getVoucherAmount()
    {
        return $this->voucherAmount;
    }

    /**
     * @param float $voucherAmount
     */
    public function setVoucherAmount($voucherAmount)
    {
        $this->voucherAmount = $voucherAmount;
    }

    /**
     * @return string
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @param string $itemId
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param int $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @return string
     */
    public function getDateString()
    {
        return $this->dateString;
    }

    /**
     * @param string $dateString
     */
    public function setDateString($dateString)
    {
        $this->dateString = $dateString;
    }

    /**
     * @return string
     */
    public function getShipmentProvider()
    {
        return $this->shipmentProvider;
    }

    /**
     * @param string $shipmentProvider
     */
    public function setShipmentProvider($shipmentProvider)
    {
        $this->shipmentProvider = $shipmentProvider;
    }

    /**
     * @return string
     */
    public function getFullJson()
    {
        return $this->fullJson;
    }

    /**
     * @param string $fullJson
     */
    public function setFullJson($fullJson)
    {
        $this->fullJson = $fullJson;
    }

    /**
     * @return string
     */
    public function getInternalId()
    {
        return $this->internalId;
    }

    /**
     * @param string $internalId
     */
    public function setInternalId($internalId)
    {
        $this->internalId = $internalId;
    }

    /**
     * @return string
     */
    public function getShippingType()
    {
        return $this->shippingType;
    }

    /**
     * @param string $shippingType
     */
    public function setShippingType($shippingType)
    {
        $this->shippingType = $shippingType;
    }
}