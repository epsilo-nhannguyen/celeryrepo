<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:41
 */

namespace App\Library;


class Factory
{

    /**
     * @var string
     */
    protected static $classFactory;

    /**
     * @param string $classFactory
     */
    public static function setClassFactory($classFactory)
    {
        self::$classFactory = $classFactory;
    }

    /**
     * @param $classIdentifier
     * @param $config
     * @return mixed
     * @throws \App\Library\Exceptions\FactoryException
     */
    public static function build($classIdentifier, $config)
    {
        $className = $classIdentifier;
        $pathClassName = self::_getClassNameExists($className);
        if (self::_checkFileExists($className) && $pathClassName) {
            return new $pathClassName($config);
        }

        throw new \App\Library\Exceptions\FactoryException($classIdentifier);
    }

    /**
     * @param string $className
     * @return bool
     */
    private static function _checkFileExists($className)
    {
        $fileName = $className.'.php';
        $path = lcfirst(str_replace('\\', DIRECTORY_SEPARATOR, self::$classFactory));
        return file_exists($path.DIRECTORY_SEPARATOR.$fileName);
    }

    /**
     * @param string $className
     * @return string
     */
    private static function _getClassNameExists($className)
    {
        $pathClassName = self::$classFactory.'\\'.$className;
        return class_exists($pathClassName) ? $pathClassName : '';
    }

}