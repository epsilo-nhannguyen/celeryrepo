<?php

namespace App\Library\Application\Notification;


use Illuminate\Mail\Mailable;

class Email extends Mailable
{

    /**
     * @var string
     */
    public $subject;

    /**
     * @var string
     */
    public $pathTemplate;

    /**
     * @var array
     */
    public $assocKeyData;

    public function __construct($subject, $pathTemplate, $assocKeyData)
    {
        $this->subject = $subject;
        $this->pathTemplate = $pathTemplate;
        $this->assocKeyData = $assocKeyData;
    }

    /**
     * @return $this
     */
    public function build()
    {
        return $this->view($this->pathTemplate)
            ->subject($this->subject)
            ->with($this->assocKeyData)
            ;
    }

}