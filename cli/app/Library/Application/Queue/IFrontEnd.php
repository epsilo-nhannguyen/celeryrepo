<?php


namespace App\Library\Application\Queue;


interface IFrontEnd
{
    /**
     * @var string
     */
    const QUEUE_NAME_TEST_PING_PONG = 'test_ping_pong';
    const QUEUE_NAME_DO_SEND_MAIL = 'do_send_mail';
    const QUEUE_NAME_PULL_PRODUCT = 'pull_product';
    const QUEUE_NAME_PULL_ORDER = 'pull_order';

}