<?php

namespace App\Library;

use Illuminate\Mail\Mailable;

class Email extends Mailable
{
    /**
     * @var string
     */
    public $subject;
    public $pathTemplate;

    /**
     * @var array
     */
    public $assocKeyData;
    public $pathAttachmentFileArray;

    public function __construct($subject, $pathTemplate, $assocKeyData, $pathAttachmentFileArray)
    {
        $this->subject = $subject;
        $this->pathTemplate = $pathTemplate;
        $this->assocKeyData = $assocKeyData;
        $this->pathAttachmentFileArray = $pathAttachmentFileArray;
    }

    /**
     * @return $this
     */
    public function build()
    {
        $message = $this->view($this->pathTemplate)
            ->subject($this->subject)
            ->with($this->assocKeyData)
        ;
        foreach ($this->pathAttachmentFileArray as $pathAttachmentFile) {
            $message->attach($pathAttachmentFile); // attach each file
        }

        return $message;
    }
}
