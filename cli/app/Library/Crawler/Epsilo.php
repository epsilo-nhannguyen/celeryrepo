<?php
namespace App\library\Crawler;

class Epsilo
{
    protected $ch;

    protected $domain;

    protected $cookiePath;

    public function __construct()
    {
        $this->ch = curl_init();
    }

    public function __destruct()
    {
        curl_close($this->ch);
        $this->domain = 'api-marketing.buffsell.lc';
        $this->cookiePath = storage_path('/app/cookies/epsilo');
    }

    public function login($username, $password)
    {
        $username = urlencode($username);
        $password = urlencode($password);

        curl_setopt($this->ch, CURLOPT_URL, $this->domain.'/api/user/login');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, "email={$username}&password={$password}}&remember=false");
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->cookiePath);

        $headers = array();
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Accept-Encoding: gzip, deflate';
        $headers[] = 'Accept-Language: en-US,en;q=0.9';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        return curl_exec($this->ch);
    }
}