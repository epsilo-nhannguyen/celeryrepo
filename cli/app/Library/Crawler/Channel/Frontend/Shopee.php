<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 18:23
 */

namespace App\Library\Crawler\Channel\Frontend;


use App\Models;


class Shopee
{
    /**
     * get Url Frontend
     * @param int $ventureId
     * @return string
     */
    public static function getUrlFrontend($ventureId)
    {
        $url = '';
        switch ($ventureId) {
            case Models\Venture::VIETNAM:
                $url = 'https://shopee.vn';
                break;

            case Models\Venture::PHILIPPINES:
                $url = 'https://shopee.ph';
                break;

            case Models\Venture::MALAYSIA:
                $url = 'https://shopee.com.my';
                break;

            case Models\Venture::SINGAPORE:
                $url = 'https://shopee.sg';
                break;

            case Models\Venture::INDONESIA:
                $url = 'https://shopee.co.id';
                break;
            case Models\Venture::THAILAND:
                $url = 'https://shopee.co.th';
                break;
        }
        return $url;
    }

    /**
     * @param int $venture
     * @param string $keyword
     * @param int $totalPage
     * @param array $arrayItemIdNeed
     * @param int $shopId
     * @return array
     */
    public static function findProductRank($venture, $keyword, $totalPage, $arrayItemIdNeed, $shopId)
    {
        $returnData = [];

        $limit = 50;
        $rank = 1;
        for ($i = 0; $i < $totalPage; $i++) {
            $newPage = $i * $limit;
            $ch = curl_init();
            $url = self::getUrlFrontend($venture) . '/api/v2/search_items/?by=relevancy&keyword=' . urlencode($keyword) . '&limit=' . $limit . '&newest=' . $newPage . '&order=desc&page_type=search';
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
            $headers = array();
            $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0';
            $headers[] = 'Accept: */*';
            $headers[] = 'Accept-Language: en-US,en;q=0.5';
            $headers[] = 'Referer: https://shopee.vn/search?keyword=' . urlencode($keyword);
            $headers[] = 'X-Requested-With: XMLHttpRequest';
            $headers[] = 'X-API-SOURCE: pc';
            $headers[] = 'Connection: keep-alive';
            $headers[] = 'Te: Trailers';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($ch);
            curl_close($ch);
            $itemInfo = json_decode($result, true);
            foreach ($itemInfo['items'] ?? [] as $item) {
                if (!$item['ads_keyword']) continue;

                $crawlItemId = trim($item['itemid']);
                $crawShopId = $item['shopid'];
                if ($shopId == $crawShopId && in_array($crawlItemId, $arrayItemIdNeed)) {
                    $returnData[] = ['itemid' => $crawlItemId, 'rank' => $rank];
                }
                $rank++;
            }
        }

        return $returnData;
    }

    public static function searchRank($venture, $keyword, $total)
    {
        $returnData = [];
        if ($venture == Application_Constant_Db_Venture::VIETNAM) {
            $limit = 50;
            $page = $total / $limit;
            for ($i = 0; $i < $page; $i++) {
                $newPage = $i * $limit;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,
                    'https://shopee.vn/api/v2/search_items/?by=relevancy&keyword=' . urlencode($keyword) . '&limit=50&newest=' . $newPage . '&order=desc&page_type=search');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
                $headers = array();
                $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
                $headers[] = 'Accept: */*';
                $headers[] = 'Accept-Language: en-US,en;q=0.5';
                $headers[] = 'Referer: https://shopee.vn/search?keyword=' . urlencode($keyword);
                $headers[] = 'X-Requested-With: XMLHttpRequest';
                $headers[] = 'X-Api-Source: pc';
                $headers[] = 'Connection: keep-alive';
                $headers[] = 'Te: Trailers';
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $result = curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                curl_close($ch);
                $itemInfo = json_decode($result, true);
                $stt = 1;
                foreach ($itemInfo['items'] as $item) {

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://shopee.vn/api/v2/item/get?itemid=' . $item['itemid'] . '&shopid=' . $item['shopid'] . '');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

                    $headers = array();
                    $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
                    $headers[] = 'Accept: */*';
                    $headers[] = 'Accept-Language: en-US,en;q=0.5';
                    $headers[] = 'Referer: https://shopee.vn/Son-m%C3%B4i-MAC-Relentlessly-Red-Retro-Matte-lipstick-i.' . $item['shopid'] . '.' . $item['itemid'] . '';
                    $headers[] = 'X-Requested-With: XMLHttpRequest';
                    $headers[] = 'X-Api-Source: pc';
                    $headers[] = 'Connection: keep-alive';
                    $headers[] = 'Te: Trailers';
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    $data = curl_exec($ch);
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                    $totalItem = json_decode($data, true);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://shopee.vn/api/v2/shop/get?is_brief=1&shopid=' . $item['shopid'] . '');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

                    $headers = array();
                    $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
                    $headers[] = 'Accept: */*';
                    $headers[] = 'Accept-Language: en-US,en;q=0.5';
                    $headers[] = 'Referer: https://shopee.vn/Son-m%C3%B4i-MAC-Relentlessly-Red-Retro-Matte-lipstick-i.' . $item['shopid'] . '.' . $item['itemid'] . '';
                    $headers[] = 'X-Requested-With: XMLHttpRequest';
                    $headers[] = 'X-Api-Source: pc';
                    $headers[] = 'Connection: keep-alive';
                    $headers[] = 'Te: Trailers';
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    $resultShop = curl_exec($ch);
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                    $shopInfo = json_decode($resultShop, true);
                    if ($item['adsid']) {
                        $item['adsid'] = 1;
                    } else {
                        $item['adsid'] = 0;
                    }
                    if ($totalItem['item']['is_official_shop'] == true) {
                        $totalItem['item']['is_official_shop'] = 1;
                    } else {
                        $totalItem['item']['is_official_shop'] = 0;
                    }

                    $returnData[] = [
                        'shop_id' => $shopInfo['data']['shopid'],
                        'shop_name' => $shopInfo['data']['name'],
                        'category' => $totalItem['item']['categories'][0]['display_name'] . '>' . $totalItem['item']['categories'][1]['display_name'] . '>' . $totalItem['item']['categories'][2]['display_name'],
                        'adsid' => $item['adsid'],
                        'rank' => $stt++,
                        'itemid' => $totalItem['item']['itemid'],
                        'product_name' => $totalItem['item']['name'],
                        'price_before_discount' => $totalItem['item']['price_before_discount'] / 100000,
                        'price_max' => $totalItem['item']['price_max'] / 100000,
                        'collected' => date('Y-m-d H:i:s'),
                        'useragent' => 'web',
                        'is_official' => $totalItem['item']['is_official_shop']
                    ];
                }
            }
        }

        return $returnData;
    }

    public function crawlerShopeeAction($limit, $newest, $organization, $venture)
    {
        if ($venture == Application_Constant_Db_Venture::VIETNAM) {
            $triSosSettingKeyWordInfo = Crawler_Model_CrawlerTriSosSettingKeyWord::getInstance()->getByOrganizationAndVenture($organization, $venture);
            $page = $newest / $limit;
            for ($i = 0; $i <= $page; $i++) {
                $newPage = $i * $limit;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,
                    'https://shopee.vn/api/v2/search_items/?by=relevancy&keyword=' . urlencode($triSosSettingKeyWordInfo[DbTable_Tri_Sos_Setting_Keyword::COL_TRI_SOS_SETTING_KEYWORD_VALUE]) . '&limit=50&newest=' . $newPage . '&order=desc&page_type=search');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
                $headers = array();
                $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
                $headers[] = 'Accept: */*';
                $headers[] = 'Accept-Language: en-US,en;q=0.5';
                $headers[] = 'Referer: https://shopee.vn/search?keyword=' . urlencode($triSosSettingKeyWordInfo[DbTable_Tri_Sos_Setting_Keyword::COL_TRI_SOS_SETTING_KEYWORD_VALUE]);
                $headers[] = 'X-Requested-With: XMLHttpRequest';
                $headers[] = 'X-Api-Source: pc';
                $headers[] = 'Connection: keep-alive';
                $headers[] = 'Te: Trailers';
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $result = curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                curl_close($ch);
                $itemInfo = json_decode($result, true);
                $stt = 1;
                foreach ($itemInfo['items'] as $item) {

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://shopee.vn/api/v2/item/get?itemid=' . $item['itemid'] . '&shopid=' . $item['shopid'] . '');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

                    $headers = array();
                    $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
                    $headers[] = 'Accept: */*';
                    $headers[] = 'Accept-Language: en-US,en;q=0.5';
                    $headers[] = 'Referer: https://shopee.vn/Son-m%C3%B4i-MAC-Relentlessly-Red-Retro-Matte-lipstick-i.' . $item['shopid'] . '.' . $item['itemid'] . '';
                    $headers[] = 'X-Requested-With: XMLHttpRequest';
                    $headers[] = 'X-Api-Source: pc';
                    $headers[] = 'Connection: keep-alive';
                    $headers[] = 'Te: Trailers';
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    $data = curl_exec($ch);
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                    $totalItem = json_decode($data, true);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://shopee.vn/api/v2/shop/get?is_brief=1&shopid=' . $item['shopid'] . '');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

                    $headers = array();
                    $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
                    $headers[] = 'Accept: */*';
                    $headers[] = 'Accept-Language: en-US,en;q=0.5';
                    $headers[] = 'Referer: https://shopee.vn/Son-m%C3%B4i-MAC-Relentlessly-Red-Retro-Matte-lipstick-i.' . $item['shopid'] . '.' . $item['itemid'] . '';
                    $headers[] = 'X-Requested-With: XMLHttpRequest';
                    $headers[] = 'X-Api-Source: pc';
                    $headers[] = 'Connection: keep-alive';
                    $headers[] = 'Te: Trailers';
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    $resultShop = curl_exec($ch);
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                    $shopInfo = json_decode($resultShop, true);
                    if ($item['adsid']) {
                        $item['adsid'] = 1;
                    } else {
                        $item['adsid'] = 0;
                    }
                    if ($totalItem['item']['is_official_shop'] == true) {
                        $totalItem['item']['is_official_shop'] = 1;
                    } else {
                        $totalItem['item']['is_official_shop'] = 0;
                    }
                    Crawler_Model_CrawlerTriSosData::getInstance()->insert(
                        Application_Constant_Db_Channel::SHOPEE,
                        $shopInfo['data']['name'],
                        $totalItem['item']['categories'][0]['display_name'] . '>' . $totalItem['item']['categories'][1]['display_name'] . '>' . $totalItem['item']['categories'][2]['display_name'],
                        $item['adsid'],
                        $stt++,
                        $totalItem['item']['itemid'],
                        $totalItem['item']['name'],
                        $totalItem['item']['price_before_discount'],
                        $totalItem['item']['price_max'],
                        date('Y-m-d'),
                        'web',
                        $totalItem['item']['is_official_shop'],
                        $triSosSettingKeyWordInfo[DbTable_Tri_Sos_Setting_Keyword::COL_TRI_SOS_SETTING_KEYWORD_ID]
                    );
                }
            }
        }
    }

    public function crawlerByShopeeAction($limit, $newest, $url, $settingShopId)
    {
        $newUrl = explode('https://shopee.vn/', $url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://shopee.vn/api/v2/shop/get?username=' . $newUrl[1] . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
        $headers[] = 'Accept: */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Referer: ' . $url . '';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Api-Source: pc';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $shopInfo = json_decode($result, true);
        $page = $newest / $limit;
        for ($i = 0; $i <= $page; $i++) {
            $newPage = $i * $limit;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://shopee.vn/api/v2/search_items/?by=pop&limit=30&match_id=' . $shopInfo['data']['shopid'] . '&newest=' . $newPage . '&order=desc&page_type=shop');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
            $headers[] = 'Accept: */*';
            $headers[] = 'Accept-Language: en-US,en;q=0.5';
            $headers[] = 'Referer: ' . $url . '';
            $headers[] = 'X-Requested-With: XMLHttpRequest';
            $headers[] = 'X-Api-Source: pc';
            $headers[] = 'Connection: keep-alive';
            $headers[] = 'Te: Trailers';

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $resultItem = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $shopItem = json_decode($resultItem, true);
            foreach ($shopItem['items'] as $item) {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://shopee.vn/api/v2/item/get?itemid=' . $item['itemid'] . '&shopid=' . $item['shopid'] . '');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

                $headers = array();
                $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0';
                $headers[] = 'Accept: */*';
                $headers[] = 'Accept-Language: en-US,en;q=0.5';
                $headers[] = 'Referer: https://shopee.vn/' . urlencode($item['name']) . '-i.' . $item['shopid'] . '.' . $item['itemid'] . '';
                $headers[] = 'X-Requested-With: XMLHttpRequest';
                $headers[] = 'X-Api-Source: pc';
                $headers[] = 'Connection: keep-alive';
                $headers[] = 'Te: Trailers';
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $resultPrice = curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                curl_close($ch);
                $itemPrice = json_decode($resultPrice, true);
                Crawler_Model_TriPreData::getInstance()->insert(
                    $item['itemid'],
                    $item['name'],
                    $item['item_rating']['rating_star'],
                    $item['item_rating']['rating_count'][0],
                    $settingShopId,
                    $itemPrice['item']['price_before_discount'],
                    $itemPrice['item']['price_min'],
                    date('Y-m-d')
                );
            }
        }
    }

    /**
     * @param $ventureId
     * @param $keyword
     * @return bool|false|string
     */
    public static function isShopAdsPosition($ventureId, $keyword)
    {
        $ch = curl_init();
        $url = self::getUrlFrontend($ventureId) . "/api/v2/search_users/?keyword=" . urlencode($keyword) . "&limit=1&with_search_cover=true";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }
}
