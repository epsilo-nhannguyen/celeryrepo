<?php

namespace App\Library\Crawler\Channel;

use App\Library;
use App\Models;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;

class Tiki
{
    const CURL_TIME_OUT = 60;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var resource
     */
    private $ch;

    /**
     * @var string
     */
    private $tokenAds;

    /**
     * @var $string
     */
    private $tokenSC;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $accountId;

    /**
     * @var string;
     */
    private $sellerId;

    /**
     * @var int
     */
    private $venture;

    /**
     * @var int
     */
    private $size;

    /**
     * @var TikiAuthentication
     */
    private $tikiAuth;

    /**
     * get Size
     * @return int
     */
    public function getSize()
    {
        return $this->size ?? env('TIKI_AD_PERFORMANCE_SIZE', 200);
    }

    /**
     * Tiki constructor.
     * @param $shopChannelId int
     */
    public function __construct($shopChannelId)
    {
        $this->shopChannelId = $shopChannelId;
        $shopInfo = Models\ModelBusiness\ShopChannel::getShopChannelAndMasterInfoByShopChannelId($shopChannelId);
        if (!$shopInfo) {
            Library\LogError::getInstance()->slack('Tiki - Shop do not exist in epsilo - ShopChannelId: ' . $shopChannelId, config('slack.tiki'));
        }
        $shopCredential = json_decode($shopInfo->shop_channel_api_credential, true);
        if (!isset($shopCredential['username']) || !isset($shopCredential['password'])) {
            Library\LogError::getInstance()->slack('Tiki - Shop do not have credential - ShopChannelId: ' . $shopChannelId, config('slack.tiki'));
        }
        $this->ch = curl_init();
        $this->tikiAuth = new TikiAuthentication($shopCredential['username'], $shopCredential['password'], $shopInfo->fk_venture);
    }

    /**
     * Tiki destructor.
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }

    /**
     * @return DTOResponse
     */
    public function login()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        $checkTokenIfHad = $this->tikiAuth->checkSellerCenterToken();
        if (!$checkTokenIfHad->getIsSuccess()) {
            $loginResponse = $this->tikiAuth->getToken();
            if (!$loginResponse->getIsSuccess()) {
                return $loginResponse;
            }
            $credentialData = $this->tikiAuth->getCredentials();
            if (!$credentialData->getIsSuccess()) {
                return $credentialData;
            }
        } else {
            $credentialData = $this->tikiAuth->getCredentials();
            if (!$credentialData->getIsSuccess()) {
                return $credentialData;
            }
        }
        $responseDTO->setData($credentialData->getData());
        return $responseDTO;
    }

    /**
     * @param $function
     * @param $response
     * @param $request
     * @param $url
     * @return DTOResponse
     */
    private function verifyResponseAdsData($function, $response, $request, $url)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $data = [
            "function" => $function,
            "response" => $response,
            "request" => $request,
            "url" => $url
        ];
        $data = json_encode($data);
        if (!$response) {
            Library\LogError::getInstance()->slack('Tiki ' . $function . 'Response with null value ' . 'Params: ' . $data);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Response with null value');
            return $responseDTO;
        }
        if (!isset($response['code']) || !isset($response['message']) || !isset($response['data'])) {
            Library\LogError::getInstance()->slack('Tiki ' . $function . 'Channel change params ' . 'Params: ' . $data);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel change params');
            return $responseDTO;
        }
        if ($response['code'] != 200 || $response['message'] != 'Success') {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Error');
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * @param $credentialData
     */
    public function setCredential($credentialData)
    {
        $this->tokenAds = $credentialData['shopAdsToken'];
        $this->userId = $credentialData['userId'];
        $this->accountId = $credentialData['accountId'];
        $this->sellerId = $credentialData['sellerId'];
        $this->venture = $credentialData['ventureId'];
        $this->tokenSC = $credentialData['sellerCenterToken'];
    }

    /**
     * raw Keyword Suggest
     * --------------------------------------------------
     * response include:
     * - keyword data
     * - info keyword have: cate, master, master_relevant
     * @param int $productId
     * @return DTOResponse
     */
    public function rawKeywordSuggest($productId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        $url = $this->tikiAntsDomain() . "/v3/api/keyword/index?type=get-keyword-suggest&product_ids=$productId&_token=$this->tokenAds&_user_id=$this->userId&_account_id=$this->accountId";

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = [];
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->tikiAntsDomain());
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            return $responseDTO;
        }

        $result = json_decode($response, true);

        $isSuccess = $this->verifyResponseAdsData(__FUNCTION__, $result, null, $url);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * get Data Keyword Suggest
     * @param int $productId
     * @return DTOResponse
     */
    public function getDataKeywordSuggest($productId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $result = $this->rawKeywordSuggest($productId);
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $keywordData = $result->getData()['data']['keywords']['rows'] ?? [];
        $assocKeywordClick = array_column($keywordData, 'click', 'keyword');

        $responseDTO->setData($assocKeywordClick);
        return $responseDTO;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param $page
     * @param $size
     * @return DTOResponse
     */
    private function _rawDataCampaign($dateFrom, $dateTo, $page, $size)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $dateFrom = str_replace('-', '/', $dateFrom);
        $dateTo = str_replace('-', '/', $dateTo);
        $url = $this->tikiAntsDomain() . '/v3/api/lineItem/performance?limit=' . $size . '&page=' . $page . '&sort=&az=&format=grid&from_date=' . $dateFrom . '&to_date=' . $dateTo . '&filter=[]&type=196&_token=' . $this->tokenAds . '&_user_id=' . $this->userId . '&_account_id=' . $this->accountId . '&_lang=vi';

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, self::CURL_TIME_OUT);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: tiki.ants.vn';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Sec-Fetch-Dest: empty';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            return $responseDTO;
        }

        $result = json_decode($response, true);

        $isSuccess = $this->verifyResponseAdsData(__FUNCTION__, $result, null, $url);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * raw Data Ad Group
     * @param string $dateFrom
     * @param string $dateTo
     * @param int $page
     * @param int $size
     * @return DTOResponse
     */
    private function _rawDataAdGroup($dateFrom, $dateTo, $page, $size)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $dateFrom = str_replace('-', '/', $dateFrom);
        $dateTo = str_replace('-', '/', $dateTo);
        $url = $this->tikiAntsDomain() . '/v3/api/campaign/performance?limit=' . $size . '&page=' . $page . '&sort=&az=&format=grid&from_date=' . $dateFrom . '&to_date=' . $dateTo . '&filter=[{}]&type=197&_token=' . $this->tokenAds . '&_user_id=' . $this->userId . '&_account_id=' . $this->accountId;

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = [];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            return $responseDTO;
        }

        $result = json_decode($response, true);

        $isSuccess = $this->verifyResponseAdsData(__FUNCTION__, $result, null, $url);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * raw Data Product
     * @param string $dateFrom
     * @param string $dateTo
     * @param int $page
     * @param int $size
     * @return DTOResponse
     */
    private function _rawDataProduct($dateFrom, $dateTo, $page, $size)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $dateFrom = str_replace('-', '/', $dateFrom);
        $dateTo = str_replace('-', '/', $dateTo);
        $url = $this->tikiAntsDomain() . '/v3/api/creative/performance?limit=' . $size . '&page=' . $page . '&sort=&az=&format=grid&from_date=' . $dateFrom . '&to_date=' . $dateTo . '&filter=[{}]&type=198&_token=' . $this->tokenAds . '&_user_id=' . $this->userId . '&_account_id=' . $this->accountId;

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = [];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            return $responseDTO;
        }

        $result = json_decode($response, true);

        $isSuccess = $this->verifyResponseAdsData(__FUNCTION__, $result, null, $url);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * raw Data Keyword
     * @param string $dateFrom
     * @param string $dateTo
     * @param int $page
     * @param int $size
     * @param int $campaignId
     * @return DTOResponse
     */
    private function _rawDataKeyword($dateFrom, $dateTo, $page, $size)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $dateFrom = str_replace('-', '/', $dateFrom);
        $dateTo = str_replace('-', '/', $dateTo);
        $url = $this->tikiAntsDomain() . '/v3/api/keyword/performance?limit=' . $size . '&page=' . $page . '&sort=&az=&format=grid&from_date=' . $dateFrom . '&to_date=' . $dateTo . '&filter=[]&type=199&_token=' . $this->tokenAds . '&_user_id=' . $this->userId . '&_account_id=' . $this->accountId;

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = [];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            return $responseDTO;
        }

        $result = json_decode($response, true);

        $isSuccess = $this->verifyResponseAdsData(__FUNCTION__, $result, null, $url);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * @param $offset int $Offset
     * @param $size int Size
     * @return DTOResponse
     */
    private function _rawProductInfo($offset, $size)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, 'https://sellercenter.tiki.vn/api/tiki_api?path=catalog/products&offset=' . $offset . '&limit=' . $size . '&include=attributes,inventory,images,reasons,master_sku,categories&master_id=gt|0&entity_type=seller_simple&counted_by_inventory=1&seller_ids=' . $this->sellerId . '&order_by=created_at|desc');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: sellercenter.tiki.vn';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36';
        $headers[] = 'Authorization: Bearer ' . $this->tokenSC;
        $headers[] = 'Accept: */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (!$result) {
            Library\LogError::getInstance()->slack('Tiki ' . __FUNCTION__ . ' raw null data');
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Response with null value');
            return $responseDTO;
        }
        if (!isset($result['data']) || !isset($result['paging']) || !isset($result['paging']['total'])) {
            Library\LogError::getInstance()->slack('Tiki ' . __FUNCTION__ . ' channel change params');
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel change params');
            return $responseDTO;
        }

        if (curl_errno($this->ch)) {
            Library\LogError::getInstance()->slack('Tiki ' . __FUNCTION__ . ' - ' . curl_errno($this->ch));
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL failure');
            return $responseDTO;
        }

        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * raw Data Campaign
     * @param $dateFrom - Format d-m-Y
     * @param $dateTo - Format d-m-Y
     * @return DTOResponse
     */
    public function rawDataCampaign($dateFrom, $dateTo)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        $responseDTO->setData([]);
        $data = [];

        $result = $this->_rawDataCampaign($dateFrom, $dateTo, 1, $this->getSize());
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }
        $data = array_merge($data, $result->getData()['data']['rows'] ?? []);

        $totalRecord = $result->getData()['data']['total_records'] ?? 0;
        for ($page = 2; $page <= ceil($totalRecord / $this->getSize()); $page++) {
            $result = $this->_rawDataCampaign($dateFrom, $dateTo, $page, $this->getSize());
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }
            $data = array_merge($data, $result->getData()['data']['rows'] ?? []);
        }

        $responseDTO->setData($data);
        return $responseDTO;
    }

    /**
     * raw Data Ad Group
     * @param string $dateFrom
     * @param string $dateTo
     * @return DTOResponse
     */
    public function rawDataAdGroup($dateFrom, $dateTo)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        $responseDTO->setData([]);
        $data = [];

        $result = $this->_rawDataAdGroup($dateFrom, $dateTo, 1, $this->getSize());
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }
        $data = array_merge($data, $result->getData()['data']['rows'] ?? []);


        $totalRecord = $result->getData()['data']['total_records'] ?? 0;
        for ($page = 2; $page <= ceil($totalRecord / $this->getSize()); $page++) {
            $result = $this->_rawDataAdGroup($dateFrom, $dateTo, $page, $this->getSize());
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }
            $data = array_merge($data, $result->getData()['data']['rows'] ?? []);
        }

        $responseDTO->setData($data);
        return $responseDTO;
    }

    /**
     * raw Data Product
     * @param string $dateFrom
     * @param string $dateTo
     * @return DTOResponse
     */
    public function rawDataProduct($dateFrom, $dateTo)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        $responseDTO->setData([]);
        $data = [];

        $result = $this->_rawDataProduct($dateFrom, $dateTo, 1, $this->getSize());
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }
        $data = array_merge($data, $result->getData()['data']['rows'] ?? []);

        $totalRecord = $result->getData()['data']['total_records'] ?? 0;
        for ($page = 2; $page <= ceil($totalRecord / $this->getSize()); $page++) {
            $result = $this->_rawDataProduct($dateFrom, $dateTo, $page, $this->getSize());
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }
            $data = array_merge($data, $result->getData()['data']['rows'] ?? []);
        }

        $responseDTO->setData($data);
        return $responseDTO;
    }

    /**
     * raw Data Keyword
     * @param string $dateFrom
     * @param string $dateTo
     * @return DTOResponse
     */
    public function rawDataKeyword($dateFrom, $dateTo)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        $responseDTO->setData([]);
        $data = [];

        $result = $this->_rawDataKeyword($dateFrom, $dateTo, 1, $this->getSize());
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }
        $data = array_merge($data, $result->getData()['data']['rows'] ?? []);

        $totalRecord = $result->getData()['data']['total_records'] ?? 0;
        for ($page = 2; $page <= ceil($totalRecord / $this->getSize()); $page++) {
            $result = $this->_rawDataKeyword($dateFrom, $dateTo, $page, $this->getSize());
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }
            $data = array_merge($data, $result->getData()['data']['rows'] ?? []);
        }

        $responseDTO->setData($data);
        return $responseDTO;
    }

    /**
     * @return DTOResponse
     */
    public function getProductInfo()
    {
        // Final data
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        $responseDTO->setData([]);
        $data = [];

        $result = $this->_rawProductInfo(0, $this->getSize());
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        // Merge data
        $data = array_merge($data, $result->getData()['data'] ?? []);
        $metaData = $result->getData()['paging'];
        $totalStep = ceil($metaData['total'] / $this->getSize());
        for ($step = 1; $step <= $totalStep; $step++) {
            $offset = $step * $this->getSize();
            $result = $this->_rawProductInfo($offset, $this->getSize());
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }
            $data = array_merge($data, $result->getData()['data'] ?? []);
        }

        $responseDTO->setData($data);
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function tikiSellerDomain()
    {
        $result = '';
        switch ($this->venture) {
            case Models\Venture::VIETNAM:
                $result = 'https://sellercenter.tiki.vn';
                break;
        }
        return $result;
    }

    /**
     * @return string
     */
    public function tikiAntsDomain()
    {
        $result = '';
        switch ($this->venture) {
            case Models\Venture::VIETNAM:
                $result = 'https://tiki.ants.vn';
                break;
        }
        return $result;
    }

    /**
     * @return string
     */
    public function tikiDomain()
    {
        $result = '';
        switch ($this->venture) {
            case Models\Venture::VIETNAM:
                $result = 'https://tiki.vn';
                break;
        }
        return $result;
    }
}