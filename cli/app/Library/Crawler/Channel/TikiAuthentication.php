<?php

namespace App\Library\Crawler\Channel;

use App\Library\LogError;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use Exception;

class TikiAuthentication
{
    /**
     * @var
     */
    private $ch;

    /**
     * @var
     */
    private $username;

    /**
     * @var
     */
    private $password;

    /**
     * @var
     */
    private $ventureId;

    #region Common

    /**
     * buildDataPushSlack
     * @param $requestURLOrFunctionName
     * @param $requestData
     * @param $responseData
     * @return false|string
     */
    private function buildDataPushSlack($requestURLOrFunctionName, $requestData, $responseData)
    {
        return json_encode([
            "requestURLOrFunctionName" => $requestURLOrFunctionName,
            "requestData" => $requestData,
            "responseData" => $responseData
        ]);
    }

    /**
     * verifyAndWriteLogHmac
     * @param $result
     * @param $response
     * @param $fnName
     * @param null $requestData
     * @return DTOResponse
     */
    private function verifyAndWriteLogHmac($result, $response, $fnName, $requestData = null)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!$result) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Response null or decode error');
            LogError::getInstance()->slack($this->buildDataPushSlack($fnName, $requestData ?? null, $response), config('slack.tiki'));
        }
        if (isset($result['errors'])) {
            $error = current($result['errors']);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($error['detail']);
            LogError::getInstance()->slack($this->buildDataPushSlack($fnName, $requestData ?? null, $result), config('slack.tiki'));
        }
        return $responseDTO;
    }

    /**
     * writeLogForSimulatorAction
     * @param $result
     * @param $fnName
     * @param $postData
     * @param $response
     * @return DTOResponse
     */
    private function writeLogForSimulatorAction($result, $fnName, $postData, $response)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!$result) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Response null or decode error');
            LogError::getInstance()->slack($this->buildDataPushSlack($fnName, $postData, $response), config('slack.tiki'));
        }
        if (isset($result['errors']) && count($result['errors']) > 0) {
            $error = current($result['errors']);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($error);
            LogError::getInstance()->slack($this->buildDataPushSlack($fnName, $postData, $result), config('slack.tiki'));
        }
        return $responseDTO;
    }
    #endregion

    /**
     * TikiAuthentication constructor.
     * @param $username
     * @param $password
     * @param $ventureId
     */
    public function __construct($username, $password, $ventureId)
    {
        $this->username = $username;
        $this->password = $password;
        $this->ventureId = $ventureId;

        $this->ch = curl_init();
    }

    /**
     * Tokopedia destruct.
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }

    #region Login SellerCenter
    public function checkSellerCenterToken()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Check already has seller center token or not
        $sellerCenterTokenPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerCenterToken.txt";
        if (!file_exists($sellerCenterTokenPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing seller center token');
            return $responseDTO;
        }
        $sellerCenterToken = file_get_contents($sellerCenterTokenPath);

        curl_setopt($this->ch, CURLOPT_URL, 'https://sellercenter.tiki.vn/api/tiki_api?path=seller/sellers&page=1&limit=100&order_by=created_at|desc');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: sellercenter.tiki.vn';
        $headers[] = "Authorization: Bearer $sellerCenterToken";
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Accept: */*';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://sellercenter.tiki.vn/new/';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';
        $headers[] = 'If-None-Match: W/\"355d925b1124a0ac7745f2c122ce906c22775432\"';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            LogError::getInstance()->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', curl_error($this->ch)), config('slack.tiki'));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        if (isset($result['status_code'])) {
            $message = $result['message'];
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($message);
            return $responseDTO;
        }

        // Response success
        return $responseDTO;
    }

    public function getCredentials()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Check already has seller center token or not
        $sellerCenterTokenPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerCenterToken.txt";
        if (!file_exists($sellerCenterTokenPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing seller center token');
            return $responseDTO;
        }
        $sellerCenterToken = file_get_contents($sellerCenterTokenPath);

        // Check already has shop ads token or not
        $shopAdsTokenPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/shopAdsToken.txt";
        if (!file_exists($shopAdsTokenPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing shop ads token');
            return $responseDTO;
        }
        $shopAdsToken = file_get_contents($shopAdsTokenPath);

        // Check already has user id, account id or not
        $credentialsPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/credentials.txt";
        if (!file_exists($credentialsPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing user id and account id');
            return $responseDTO;
        }
        $credentials = json_decode(file_get_contents($credentialsPath), true);
        $userId = $credentials['userId'];;
        $accountId = $credentials['accountId'];

        // Check already has seller id or not
        $sellerInfoPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerInfo.txt";
        if (!file_exists($sellerInfoPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing seller id');
            return $responseDTO;
        }
        $sellerInfo = json_decode(file_get_contents($sellerInfoPath), true);
        $sellerId = $sellerInfo['sellerId'];

        $credentials = [
            'shopAdsToken' => $shopAdsToken,
            'userId' => $userId,
            'accountId' => $accountId,
            'sellerCenterToken' => $sellerCenterToken,
            'ventureId' => $this->ventureId,
            'sellerId' => $sellerId
        ];

        $responseDTO->setData($credentials);
        return $responseDTO;
    }

    public function getToken()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/tokens/")) mkdir(SYS_PATH . "/storage/app/tokens/");
        if (!file_exists(SYS_PATH . "/storage/app/tokens/tiki/")) mkdir(SYS_PATH . "/storage/app/tokens/tiki/");
        if (!file_exists(SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/");

        $result = $this->_generateSellerCenterToken();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $result = $this->_getSellerInfo();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $result = $this->_getShopAdsToken();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        // Response success
        return $responseDTO;
    }

    private function _generateSellerCenterToken()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, 'https://sellercenter.tiki.vn/api/token/request');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);

        $postData = [
            'email' => $this->username,
            'password' => $this->password
        ];
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: sellercenter.tiki.vn';
        $headers[] = 'Accept: application/json';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Origin: https://sellercenter.tiki.vn';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://sellercenter.tiki.vn/new/';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            LogError::getInstance()->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', curl_error($this->ch)), config('slack.tiki'));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $tokenPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerCenterToken.txt";
        if (file_exists($tokenPath)) {
            unlink($tokenPath);
        }
        file_put_contents($tokenPath, $result['token']);

        // Response success
        return $responseDTO;
    }

    private function _getSellerId()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $tokenPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerCenterToken.txt";
        if (!file_exists($tokenPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing seller center token');
            return $responseDTO;
        }
        $sellerCenterToken = file_get_contents($tokenPath);

        curl_setopt($this->ch, CURLOPT_URL, 'https://sellercenter.tiki.vn/api/user?include=permissions,roles');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: sellercenter.tiki.vn';
        $headers[] = "Authorization: Bearer $sellerCenterToken";
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Accept: */*';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://sellercenter.tiki.vn/';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';
        $headers[] = 'If-None-Match: W/\"05699d71ec5a6c69cbaba6b3e64f7e3ce0cd5ad6\"';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            LogError::getInstance()->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', curl_error($this->ch)), config('slack.tiki'));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $sellerIdPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerId.txt";
        if (file_exists($sellerIdPath)) {
            unlink($sellerIdPath);
        }
        file_put_contents($sellerIdPath, json_encode($result['seller_id']));

        // Response success
        return $responseDTO;
    }

    private function _getSellerInfo()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $tokenPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerCenterToken.txt";
        if (!file_exists($tokenPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing seller center token');
            return $responseDTO;
        }
        $sellerCenterToken = file_get_contents($tokenPath);

        curl_setopt($this->ch, CURLOPT_URL, 'https://sellercenter.tiki.vn/api/tiki_api?path=seller/sellers&page=1&limit=100&order_by=created_at|desc');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: sellercenter.tiki.vn';
        $headers[] = "Authorization: Bearer $sellerCenterToken";
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Accept: */*';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://sellercenter.tiki.vn/new/';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';
        $headers[] = 'If-None-Match: W/\"355d925b1124a0ac7745f2c122ce906c22775432\"';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            LogError::getInstance()->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', curl_error($this->ch)), config('slack.tiki'));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $sellerInfoPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerInfo.txt";
        if (file_exists($sellerInfoPath)) {
            unlink($sellerInfoPath);
        }
        $data = current($result['data']);
        $sellerInfo = [
            'sellerId' => isset($data['id']) ? $data['id'] : null,
            'secretKey' => isset($data['secret']) ? $data['secret'] : null
        ];
        file_put_contents($sellerInfoPath, json_encode($sellerInfo));

        // Response success
        return $responseDTO;
    }

    private function _getShopAdsToken()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $sellerInfoPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/sellerInfo.txt";
        if (!file_exists($sellerInfoPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing seller info');
            return $responseDTO;
        }
        $sellerInfo = json_decode(file_get_contents($sellerInfoPath), true);
        $secretKey = $sellerInfo['secretKey'];
        $sellerId = $sellerInfo['sellerId'];
        if (empty($secretKey)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing secret key');
            return $responseDTO;
        }
        if (empty($sellerId)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing seller id');
            return $responseDTO;
        }

        $url = "https://tiki.ants.vn/v3/authenticate/market-place/55?token=$secretKey&seller_id=$sellerId&lang=vi-VN";
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: tiki.ants.vn';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9';
        $headers[] = 'Sec-Fetch-Site: cross-site';
        $headers[] = 'Sec-Fetch-Mode: nested-navigate';
        $headers[] = 'Referer: https://sellercenter.tiki.vn/new/';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        // This api return a HTML string
        $result = $response;

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            LogError::getInstance()->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', curl_error($this->ch)), config('slack.tiki'));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Return a HTML string
        $html = $response;
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $xpath = new \DOMXPath($dom);
        $scriptTags = $xpath->query('//body//script[not(@src)]');
        $scriptTag = $scriptTags->item(0)->nodeValue;
        $arrVariables = explode(';', $scriptTag);
        $userId = null;
        $shopAdsToken = null;
        foreach ($arrVariables as $variable) {
            if (strstr($variable, 'data_user')) {
                $variableValue = explode('data_user', $variable);
                $dataUser = json_decode(substr($variableValue[1], 15, -2), true);
                $userId = $dataUser['user_id'];
                $shopAdsToken = $dataUser['token'];
                break;
            }
        }

        // Save credentials info
        $credentialsPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/credentials.txt";
        if (file_exists($credentialsPath)) {
            unlink($credentialsPath);
        }
        $credentials = [
            'userId' => $userId,
            'accountId' => $userId
        ];
        file_put_contents($credentialsPath, json_encode($credentials));

        // Save shop ads token
        $shopAdsTokenPath = SYS_PATH . "/storage/app/tokens/tiki/$this->ventureId/$this->username/shopAdsToken.txt";
        if (file_exists($shopAdsTokenPath)) {
            unlink($shopAdsTokenPath);
        }
        file_put_contents($shopAdsTokenPath, $shopAdsToken);

        // Response success
        return $responseDTO;
    }
    #endregion
}