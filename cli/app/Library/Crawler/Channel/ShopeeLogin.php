<?php
namespace App\Library\Crawler\Channel;

use App\Models\ShopChannel;
use App\Models\Venture;
use Illuminate\Support\Str;

class ShopeeLogin
{
    const COOKIE_STATE_EMPTY = 0;
    const COOKIE_STATE_NEED_VERIFY = 1;
    const COOKIE_STATE_COMPLETED = 2;

    protected $shopChannelId;
    protected $username;
    protected $password;
    protected $cookieState;
    protected $cookieString;
    protected $cookieInFile;
    protected $ventureId;
    protected $subAccountId;
    protected $credentialAssoc;
    protected $cookiesPath;

    public function __construct($shopChannelId, $username, $password, $cookieState, $cookieInFile, $ventureId, $subAccountId, $credentialAssoc)
    {
        $this->shopChannelId = $shopChannelId;
        $this->username = $username;
        $this->password = $password;
        $this->cookieState = $cookieState;
        $this->cookieInFile = $cookieInFile;
        $this->ventureId = $ventureId;
        $this->subAccountId = $subAccountId;
        $this->credentialAssoc = $credentialAssoc;

        $this->_setCookiePath();
    }

    public function login($otp = '')
    {
        if ($this->subAccountId) {
            list($cookieString, $cookieState) = $this->_loginSubAccount($otp);
        }  else {
            list($cookieString, $cookieState) = $this->_loginMainAccount();
        }

        if ($cookieState != $this->cookieState || $cookieString != $this->credentialAssoc['cookieString']) {
            $this->credentialAssoc['cookieString'] = $cookieString;
            $this->credentialAssoc['cookieState'] = $cookieState;

            ShopChannel::updateCredential($this->shopChannelId, json_encode($this->credentialAssoc));
        }

        $this->cookieString= $cookieString;
        return [$cookieString, $cookieState];
    }

    public function shopeeSellerDomain()
    {
        $result = '';
        switch ($this->ventureId) {
            case Venture::VIETNAM:
                $result = 'https://banhang.shopee.vn';
                break;
            case Venture::PHILIPPINES:
                $result = 'https://seller.shopee.ph';
                break;
            case Venture::MALAYSIA:
                $result = 'https://seller.shopee.com.my';
                break;
            case Venture::SINGAPORE:
                $result = 'https://seller.shopee.sg';
                break;
            case Venture::INDONESIA:
                $result = 'https://seller.shopee.co.id';
                break;
            case Venture::THAILAND:
                $result = 'https://seller.shopee.co.th';
                break;
        }
        return $result;
    }

    private function _getCookie($result)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        return implode('; ', $matches[1]);
    }

    private function _setCookiePath()
    {
        $cookiesPath = SYS_PATH . "/storage/app/cookies/shopee/$this->ventureId/$this->username.txt";
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/shopee/")) mkdir(SYS_PATH . "/storage/app/cookies/shopee/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/shopee/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/shopee/$this->ventureId/");

        $this->cookiesPath = $cookiesPath;
    }

    private function _loginMainAccount()
    {
        $ch = curl_init();
        $params = [
            'captcha' => '',
            'captcha_key' => '447ba8b633054132a275d220008d28ad',
            'remember' => 'false',
            'password_hash' => hash('sha256', md5($this->password)),
        ];
        if (is_numeric($this->username)) {
            $params['phone'] = $this->username;
        } else {
            if (filter_var($this->username, FILTER_VALIDATE_EMAIL)) {
                $params['email'] = $this->username;
            } else {
                $params['username'] = $this->username;
            }
        }
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v1/login/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiesPath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiesPath);
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        curl_close($ch);
        var_dump($result);
        if ($httpcode == 481) {
            $state = static::COOKIE_STATE_NEED_VERIFY;
        } elseif ($httpcode == 200) {
            $state = static::COOKIE_STATE_COMPLETED;
        } else {
            $state = static::COOKIE_STATE_EMPTY;
        }

        return [$this->_getCookie($result), $state];

    }

    private function _loginSubAccount($otp = '')
    {
        $ch = curl_init();
        $params = [
            'captcha' => '',
            'captcha_key' => '447ba8b633054132a275d220008d28ad',
            'remember' => 'false',
            'subaccount' => $this->username,
            'password_hash' => md5($this->password),
        ];
        if ($otp) {
            $params['vcode'] = $otp;
        }
        $randomString = Str::random(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/login/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/account/signin';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieString;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiesPath);
        curl_close($ch);
        $this->cookieString = $this->_getCookie($result);


        if ($httpcode == 481) {
            return ['', static::COOKIE_STATE_NEED_VERIFY];
        }

        $subAccountData = $this->credentialAssoc['subaccount_data'] ?? [];
        $shopId = $subAccountData['shop_id'] ?? 0;

        $url = $this->switchShop($shopId);
        $sig = explode('?', $url)[1] ?? '';

        return $this->loginBySig($sig);
    }

    /**
     * switch Shop
     * @param $shopId
     * @return string
     */
    private function switchShop($shopId)
    {
        $shopId = intval($shopId);
        if (!$shopId) return false;

        $randomString = Str::random(32);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/subaccount/switch_shop/$shopId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/portal/shop';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieString;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        curl_close($ch);
        return json_decode($result, true)['url'] ?? '';
    }

    private function loginBySig($sig)
    {
        if (!$sig) return false;
        $randomString = Str::random(32);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/login/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sig);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/?' . $sig;
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieString;

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiesPath);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        $result = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        curl_close($ch);

        if ($httpcode == 481) {
            $state = static::COOKIE_STATE_NEED_VERIFY;
        } elseif ($httpcode == 200) {
            $state = static::COOKIE_STATE_COMPLETED;
        } else {
            $state = static::COOKIE_STATE_EMPTY;
        }

        return [$this->_getCookie($result), $state];
    }

}