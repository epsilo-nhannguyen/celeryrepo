<?php

namespace App\Library\Crawler\Channel;

use App\Library\LogError;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use Exception;

class TokopediaAuthentication
{
    /**
     * @var
     */
    private $ch;

    /**
     * @var LogError
     */
    private $logService;

    /**
     * @var
     */
    private $username;

    /**
     * @var
     */
    private $password;

    /**
     * @var
     */
    private $ventureId;

    #region Common

    /**
     * buildDataPushSlack
     * @param $requestURLOrFunctionName
     * @param $requestData
     * @param $responseData
     * @return false|string
     */
    private function buildDataPushSlack($requestURLOrFunctionName, $requestData, $responseData)
    {
        return json_encode([
            "requestURLOrFunctionName" => $requestURLOrFunctionName,
            "requestData" => $requestData,
            "responseData" => $responseData
        ]);
    }

    /**
     * verifyAndWriteLogHmac
     * @param $result
     * @param $response
     * @param $fnName
     * @param null $requestData
     * @return DTOResponse
     */
    private function verifyAndWriteLogHmac($result, $response, $fnName, $requestData = null)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!$result) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Response null or decode error');
            $this->logService->slack($this->buildDataPushSlack($fnName, $requestData ?? null, $response));
        }
        if (isset($result['errors'])) {
            $error = current($result['errors']);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($error['detail']);
            $this->logService->slack($this->buildDataPushSlack($fnName, $requestData ?? null, $result));
        }
        return $responseDTO;
    }

    /**
     * writeLogForSimulatorAction
     * @param $result
     * @param $fnName
     * @param $postData
     * @param $response
     * @return DTOResponse
     */
    private function writeLogForSimulatorAction($result, $fnName, $postData, $response)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!$result) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Response null or decode error');
            $this->logService->slack($this->buildDataPushSlack($fnName, $postData, $response));
        }
        if (isset($result['errors']) && count($result['errors']) > 0) {
            $error = current($result['errors']);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($error['detail']);
            $this->logService->slack($this->buildDataPushSlack($fnName, $postData, $result));
        }
        return $responseDTO;
    }
    #endregion

    /**
     * TokopediaAuthentication constructor.
     * @param $username
     * @param $password
     * @param $ventureId
     */
    public function __construct($username, $password, $ventureId)
    {
        $this->username = $username;
        $this->password = $password;
        $this->ventureId = $ventureId;

        $this->ch = curl_init();
        $this->logService = new LogError();
    }

    /**
     * Tokopedia destruct.
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }

    /**
     * @return DTOResponse
     */
    public function checkCookies()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Check already has cookies or not
        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists($cookiesPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing cookies');
            return $responseDTO;
        }

        // Get credential
        $credentialsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/credentials.txt";
        if (!file_exists($credentialsPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing credentials');
            return $responseDTO;
        }
        $credentials = json_decode(file_get_contents($credentialsPath), true);
        $shopId = $credentials['shopId'];
        $userId = $credentials['userId'];

        // Check cookies validity
        curl_setopt($this->ch, CURLOPT_URL, 'https://gql.tokopedia.com/');
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, '[{"operationName":"Account","variables":{},"query":"query Account {\n  user {\n    id\n    isLoggedIn\n    name\n    profilePicture\n    completion\n    phoneVerified: phone_verified\n    __typename\n  }\n  userShopInfo {\n    info {\n      shopId: shop_id\n      shopName: shop_name\n      shopDomain: shop_domain\n      shopAvatar: shop_avatar\n      isOfficial: shop_is_official\n      __typename\n    }\n    owner {\n      isPowerMerchant: is_gold_merchant\n      pmStatus: pm_status\n      __typename\n    }\n    __typename\n  }\n  wallet {\n    ovoCash: cash_balance\n    ovoPoints: point_balance\n    linked\n    __typename\n  }\n  walletPending: goalPendingBalance {\n    pendingBalance: point_balance_text\n    __typename\n  }\n  balance: saldo {\n    balanceStr: deposit_fmt\n    __typename\n  }\n  tokopoints {\n    status {\n      points {\n        reward\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  tokopointsSumCoupon {\n    sumCouponStr\n    __typename\n  }\n}\n"},{"operationName":"dynamicPlaceHolder","variables":{"navSource":""},"query":"query dynamicPlaceHolder($navSource: String!) {\n  universe_placeholder(navsource: $navSource) {\n    data {\n      keyword\n      placeholder\n      __typename\n    }\n    __typename\n  }\n}\n"},{"operationName":"NotificationCounterQuery","variables":{},"query":"query NotificationCounterQuery {\n  notifications {\n    total_notif\n    total_cart\n    resolution\n    resolution_as {\n      resolution_as_seller\n      resolution_as_buyer\n      __typename\n    }\n    sales {\n      sales_new_order\n      sales_shipping_status\n      sales_shipping_confirm\n      __typename\n    }\n    inbox {\n      inbox_talk\n      inbox_ticket\n      inbox_review\n      inbox_friend\n      inbox_message\n      inbox_wishlist\n      inbox_reputation\n      __typename\n    }\n    chat {\n      unreads\n      __typename\n    }\n    buyerOrderStatus {\n      paymentStatus\n      confirmed\n      processed\n      shipped\n      arriveAtDestination\n      __typename\n    }\n    __typename\n  }\n}\n"},{"operationName":"PopularKeyword","variables":{},"query":"query PopularKeyword {\n  trending_keywords {\n    keywords {\n      url\n      keyword\n      __typename\n    }\n    __typename\n  }\n}\n"}]');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: gql.tokopedia.com';
        $headers[] = 'Accept: */*';
        $headers[] = 'X-Source: tokopedia-lite';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Te: Trailers';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure!');
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        foreach ($result as $item) {
            $data = $item['data'];
            if (array_key_exists('user', $data) && array_key_exists('userShopInfo', $data)) {
                if ($data['user']['id'] != $userId && $data['userShopInfo']['info']['shopId'] != $shopId) {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage('Cookie has expired or invalid');
                }
            }
        }

        // Response success
        return $responseDTO;
    }

    /**
     * @return DTOResponse
     */
    public function getShopCookies()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Check already has cookies or not
        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists($cookiesPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing cookies');
            return $responseDTO;
        }
        $cookieString = file_get_contents($cookiesPath);

        // Get credential
        $credentialsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/credentials.txt";
        if (!file_exists($cookiesPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing credentials');
            return $responseDTO;
        }
        $credentials = json_decode(file_get_contents($credentialsPath), true);
        $shopId = $credentials['shopId'];
        $userId = $credentials['userId'];

        $cookies = [
            'shopId' => intval($shopId),
            'tkpdUserId' => intval($userId),
            'cookieString' => $cookieString
        ];

        $responseDTO->setData($cookies);
        return $responseDTO;
    }

    #region Login
    public function triggerSendOTP()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get cookies when load login page by Selenium
        $seleniumTokopedia = new \App\Library\Selenium\Channel\Tokopedia($this->username, $this->password, $this->ventureId);
        $seleniumTokopedia->getBrowserCookies();

        // Check already has cookies or not
        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists($cookiesPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing cookies get from load login page by Selenium');
            return $responseDTO;
        }

        if (is_numeric($this->username)) {
            $result = $this->_loadLoginPageWithUsername();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            $result = $this->_loadOTPPage();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            // Return a HTML string
            $html = $result->getData();
            $dom = new \DOMDocument();
            @$dom->loadHTML($html);

            $smsId = 'cotp__method--sms';
            $mailId = 'cotp__method--mail';
            $tokenId = 'Token';
            $msisdnId = 'MSISDN';
            $otpTypeId = 'OTPType';
            $emailId = 'Email';
            $numberOfOTPDigitId = 'NumberofOTPDigit';
            $tk = $dom->getElementById($tokenId)->getAttribute('value') ?? null;
            $msisdn = $dom->getElementById($msisdnId)->getAttribute('value') ?? null;
            $otpType = $dom->getElementById($otpTypeId)->getAttribute('value') ?? null;
            $email = $dom->getElementById($emailId)->getAttribute('value') ?? null;
            $numberOfOTPDigit = $dom->getElementById($numberOfOTPDigitId)->getAttribute('value') ?? null;

            $otpVerifyParamsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/otpVerifyParams.txt";
            if (file_exists($otpVerifyParamsPath)) {
                unlink($otpVerifyParamsPath);
            }
            $otpVerifyParams = [
                'tk' => $tk,
                'msisdn' => $msisdn,
                'otpType' => $otpType,
                'email' => $email,
                'numberOfOTPDigit' => $numberOfOTPDigit,
                'mode' => 1
            ];
            file_put_contents($otpVerifyParamsPath, json_encode($otpVerifyParams));

            if (!empty($dom->getElementById($smsId))) {
                $result = $this->_requestSMS($tk, $otpType, $msisdn, $email, $numberOfOTPDigit);
                if (!$result->getIsSuccess()) {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage($result->getMessage());
                    return $responseDTO;
                }
            } else {
                if (!empty($dom->getElementById($mailId))) {
                    $result = $this->_requestEmail($tk, $otpType, $msisdn, $email, $numberOfOTPDigit);
                    if (!$result->getIsSuccess()) {
                        $responseDTO->setIsSuccess(false);
                        $responseDTO->setMessage($result->getMessage());
                        return $responseDTO;
                    }
                } else {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage('The method send OTP by sms or mail is not found');
                    return $responseDTO;
                }
            }
        } else {
            $result = $this->_akam();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            $result = $this->_loadLoginPageWithUsername();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            $result = $this->_authorize();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            $result = $this->_loadOTPPage();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            // Return a HTML string
            $html = $result->getData();
            $dom = new \DOMDocument();
            @$dom->loadHTML($html);

            $smsId = 'cotp__method--sms';
            $mailId = 'cotp__method--mail';
            $tokenId = 'Token';
            $msisdnId = 'MSISDN';
            $otpTypeId = 'OTPType';
            $emailId = 'Email';
            $numberOfOTPDigitId = 'NumberofOTPDigit';
            $tk = $dom->getElementById($tokenId)->getAttribute('value') ?? null;
            $msisdn = $dom->getElementById($msisdnId)->getAttribute('value') ?? null;
            $otpType = $dom->getElementById($otpTypeId)->getAttribute('value') ?? null;
            $email = $dom->getElementById($emailId)->getAttribute('value') ?? null;
            $numberOfOTPDigit = $dom->getElementById($numberOfOTPDigitId)->getAttribute('value') ?? null;

            $otpVerifyParamsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/otpVerifyParams.txt";
            if (file_exists($otpVerifyParamsPath)) {
                unlink($otpVerifyParamsPath);
            }
            $otpVerifyParams = [
                'tk' => $tk,
                'msisdn' => $msisdn,
                'otpType' => $otpType,
                'email' => $email,
                'numberOfOTPDigit' => $numberOfOTPDigit,
                'mode' => 5
            ];
            file_put_contents($otpVerifyParamsPath, json_encode($otpVerifyParams));

            if (!empty($dom->getElementById($smsId))) {
                $result = $this->_requestSMS($tk, $otpType, $msisdn, $email, $numberOfOTPDigit);
                if (!$result->getIsSuccess()) {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage($result->getMessage());
                    return $responseDTO;
                }
            } else {
                if (!empty($dom->getElementById($mailId))) {
                    $result = $this->_requestEmail($tk, $otpType, $msisdn, $email, $numberOfOTPDigit);
                    if (!$result->getIsSuccess()) {
                        $responseDTO->setIsSuccess(false);
                        $responseDTO->setMessage($result->getMessage());
                        return $responseDTO;
                    }
                } else {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage('The method send OTP by sms or mail is not found');
                    return $responseDTO;
                }
            }
        }

        // Response success
        return $responseDTO;
    }

    public function resendOTP()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Check already has cookies or not
        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists($cookiesPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing cookies get from load login page by Selenium');
            return $responseDTO;
        }

        $result = $this->_loadOTPPage();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        // Return a HTML string
        $html = $result->getData();
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);

        $smsId = 'cotp__method--sms';
        $mailId = 'cotp__method--mail';
        $tokenId = 'Token';
        $msisdnId = 'MSISDN';
        $otpTypeId = 'OTPType';
        $emailId = 'Email';
        $numberOfOTPDigitId = 'NumberofOTPDigit';
        $tk = $dom->getElementById($tokenId)->getAttribute('value') ?? null;
        $msisdn = $dom->getElementById($msisdnId)->getAttribute('value') ?? null;
        $otpType = $dom->getElementById($otpTypeId)->getAttribute('value') ?? null;
        $email = $dom->getElementById($emailId)->getAttribute('value') ?? null;
        $numberOfOTPDigit = $dom->getElementById($numberOfOTPDigitId)->getAttribute('value') ?? null;

        $otpVerifyParamsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/otpVerifyParams.txt";
        if (file_exists($otpVerifyParamsPath)) {
            unlink($otpVerifyParamsPath);
        }

        if (is_numeric($this->username)) {
            $mode = 1;
        } else {
            $mode = 5;
        }
        $otpVerifyParams = [
            'tk' => $tk,
            'msisdn' => $msisdn,
            'otpType' => $otpType,
            'email' => $email,
            'numberOfOTPDigit' => $numberOfOTPDigit,
            'mode' => $mode
        ];
        file_put_contents($otpVerifyParamsPath, json_encode($otpVerifyParams));

        if (!empty($dom->getElementById($smsId))) {
            $result = $this->_requestSMS($tk, $otpType, $msisdn, $email, $numberOfOTPDigit);
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }
        } else {
            if (!empty($dom->getElementById($mailId))) {
                $result = $this->_requestEmail($tk, $otpType, $msisdn, $email, $numberOfOTPDigit);
                if (!$result->getIsSuccess()) {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage($result->getMessage());
                    return $responseDTO;
                }
            } else {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('The method send OTP by sms or mail is not found');
                return $responseDTO;
            }
        }

        // Response success
        return $responseDTO;
    }

    private function _akam()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://www.tokopedia.com/akam/11/pixel_441aadd1');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_HEADER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 1);

        $postData = "ap=true&bt=%7B%22charging%22%3Atrue%2C%22chargingTime%22%3A0%2C%22dischargingTime%22%3A%22Infinity%22%2C%22level%22%3A1%2C%22onchargingchange%22%3Anull%2C%22onchargingtimechange%22%3Anull%2C%22ondischargingtimechange%22%3Anull%2C%22onlevelchange%22%3Anull%7D&fonts=26%2C32%2C33%2C34%2C37%2C38%2C39%2C43%2C44%2C45%2C65&fh=b8357c1d7a34c2b3f404ebf4ab9d827ee185ce30&timing=%7B%221%22%3A65%2C%222%22%3A247%2C%223%22%3A368%2C%224%22%3A770%2C%22profile%22%3A%7B%22bp%22%3A1%2C%22sr%22%3A1%2C%22dp%22%3A0%2C%22lt%22%3A0%2C%22ps%22%3A0%2C%22cv%22%3A35%2C%22fp%22%3A0%2C%22sp%22%3A0%2C%22br%22%3A1%2C%22ieps%22%3A0%2C%22av%22%3A0%2C%22z1%22%3A23%2C%22jsv%22%3A1%2C%22nav%22%3A1%2C%22nap%22%3A1%2C%22crc%22%3A0%2C%22z2%22%3A0%2C%22z3%22%3A1%2C%22z4%22%3A7%2C%22fonts%22%3A13%7D%2C%22main%22%3A1166%2C%22compute%22%3A65%2C%22send%22%3A784%7D&bp=1038350511%2C-1979380391%2C1738406762%2C749224105&sr=%7B%22inner%22%3A%5B1296%2C150%5D%2C%22outer%22%3A%5B1296%2C741%5D%2C%22screen%22%3A%5B70%2C27%5D%2C%22pageOffset%22%3A%5B0%2C0%5D%2C%22avail%22%3A%5B1296%2C741%5D%2C%22size%22%3A%5B1366%2C768%5D%2C%22client%22%3A%5B1281%2C150%5D%2C%22colorDepth%22%3A24%2C%22pixelDepth%22%3A24%7D&dp=%7B%22XDomainRequest%22%3A0%2C%22createPopup%22%3A0%2C%22removeEventListener%22%3A1%2C%22globalStorage%22%3A0%2C%22openDatabase%22%3A1%2C%22indexedDB%22%3A1%2C%22attachEvent%22%3A0%2C%22ActiveXObject%22%3A0%2C%22dispatchEvent%22%3A1%2C%22addBehavior%22%3A0%2C%22addEventListener%22%3A1%2C%22detachEvent%22%3A0%2C%22fireEvent%22%3A0%2C%22MutationObserver%22%3A1%2C%22HTMLMenuItemElement%22%3A0%2C%22Int8Array%22%3A1%2C%22postMessage%22%3A1%2C%22querySelector%22%3A1%2C%22getElementsByClassName%22%3A1%2C%22images%22%3A1%2C%22compatMode%22%3A%22CSS1Compat%22%2C%22documentMode%22%3A0%2C%22all%22%3A1%2C%22now%22%3A1%2C%22contextMenu%22%3A0%7D&lt=1580666996351%2B7&ps=true%2Ctrue&cv=3c410e989ddca88222eee673af9586f59704269a&fp=false&sp=false&br=Chrome&ieps=false&av=false&z=%7B%22a%22%3A1142599528%2C%22b%22%3A1%2C%22c%22%3A0%7D&zh=&jsv=1.7&nav=%7B%22userAgent%22%3A%22Mozilla%2F5.0%20(X11%3B%20Linux%20x86_64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36%22%2C%22appName%22%3A%22Netscape%22%2C%22appCodeName%22%3A%22Mozilla%22%2C%22appVersion%22%3A%225.0%20(X11%3B%20Linux%20x86_64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36%22%2C%22appMinorVersion%22%3A0%2C%22product%22%3A%22Gecko%22%2C%22productSub%22%3A%2220030107%22%2C%22vendor%22%3A%22Google%20Inc.%22%2C%22vendorSub%22%3A%22%22%2C%22buildID%22%3A0%2C%22platform%22%3A%22Linux%20x86_64%22%2C%22oscpu%22%3A0%2C%22hardwareConcurrency%22%3A8%2C%22language%22%3A%22en-US%22%2C%22languages%22%3A%5B%22en-US%22%2C%22en%22%2C%22vi%22%2C%22zh-CN%22%5D%2C%22systemLanguage%22%3A0%2C%22userLanguage%22%3A0%2C%22doNotTrack%22%3Anull%2C%22msDoNotTrack%22%3A0%2C%22cookieEnabled%22%3Atrue%2C%22geolocation%22%3A1%2C%22vibrate%22%3A1%2C%22maxTouchPoints%22%3A0%2C%22webdriver%22%3A0%2C%22plugins%22%3A%5B%22Chrome%20PDF%20Plugin%22%2C%22Chrome%20PDF%20Viewer%22%2C%22Native%20Client%22%5D%7D&crc=%7B%22window.chrome%22%3A%7B%22app%22%3A%7B%22isInstalled%22%3Afalse%2C%22InstallState%22%3A%7B%22DISABLED%22%3A%22disabled%22%2C%22INSTALLED%22%3A%22installed%22%2C%22NOT_INSTALLED%22%3A%22not_installed%22%7D%2C%22RunningState%22%3A%7B%22CANNOT_RUN%22%3A%22cannot_run%22%2C%22READY_TO_RUN%22%3A%22ready_to_run%22%2C%22RUNNING%22%3A%22running%22%7D%7D%2C%22runtime%22%3A%7B%22OnInstalledReason%22%3A%7B%22CHROME_UPDATE%22%3A%22chrome_update%22%2C%22INSTALL%22%3A%22install%22%2C%22SHARED_MODULE_UPDATE%22%3A%22shared_module_update%22%2C%22UPDATE%22%3A%22update%22%7D%2C%22OnRestartRequiredReason%22%3A%7B%22APP_UPDATE%22%3A%22app_update%22%2C%22OS_UPDATE%22%3A%22os_update%22%2C%22PERIODIC%22%3A%22periodic%22%7D%2C%22PlatformArch%22%3A%7B%22ARM%22%3A%22arm%22%2C%22MIPS%22%3A%22mips%22%2C%22MIPS64%22%3A%22mips64%22%2C%22X86_32%22%3A%22x86-32%22%2C%22X86_64%22%3A%22x86-64%22%7D%2C%22PlatformNaclArch%22%3A%7B%22ARM%22%3A%22arm%22%2C%22MIPS%22%3A%22mips%22%2C%22MIPS64%22%3A%22mips64%22%2C%22X86_32%22%3A%22x86-32%22%2C%22X86_64%22%3A%22x86-64%22%7D%2C%22PlatformOs%22%3A%7B%22ANDROID%22%3A%22android%22%2C%22CROS%22%3A%22cros%22%2C%22LINUX%22%3A%22linux%22%2C%22MAC%22%3A%22mac%22%2C%22OPENBSD%22%3A%22openbsd%22%2C%22WIN%22%3A%22win%22%7D%2C%22RequestUpdateCheckStatus%22%3A%7B%22NO_UPDATE%22%3A%22no_update%22%2C%22THROTTLED%22%3A%22throttled%22%2C%22UPDATE_AVAILABLE%22%3A%22update_available%22%7D%7D%7D%7D&t=efeccdcdd54af4decb4e7b2b302e1bc6bffeb583&u=fc34394829b45ae191ce84f3f8d1956f&nap=10321144241322243122&fc=true";
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: www.tokopedia.com';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Accept: */*';
        $headers[] = 'Origin: https://www.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://www.tokopedia.com/login';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        // This api not return a json data
        $result = $response;

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Get cookie ak_bmsc
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        $ak_bmsc = $matches[1];

        // Replace cookie ak_bmsc
        $browserCookies = explode(';', file_get_contents($cookiesPath));
        $arrCookiesAfterReplace = [];
        foreach ($browserCookies as $browserCookie) {
            if (strpos($browserCookie, 'ak_bmsc') !== false) {
                array_push($arrCookiesAfterReplace, current($ak_bmsc));
            } else {
                array_push($arrCookiesAfterReplace, $browserCookie);
            }
        }
        $strCookiesAfterReplace = implode(';', $arrCookiesAfterReplace);
        file_put_contents($cookiesPath, $strCookiesAfterReplace);

        // Response success
        return $responseDTO;
    }

    private function _loadLoginPageWithUsername()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://www.tokopedia.com/api/v1/account/register/check');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_POST, 1);

        $postData = "id=" . urlencode($this->username) . "&captcha_token=03AOLTBLToParBNOklFgHMk4nS0UbhuWA8IpRIaZHiX-sXYyvVbDu0Yg3jPkfOgiNinBcJeGWxd-pEfNlUuO1_B1a2ZZkcF3YWxooWiE-mfxDSDyd1fmHPwofmNajAWtYDlwk7d-FkTZn_7KGIB81oBEXdqi_FQmVm5Yr3vGTmbqTPBxjuKa6o-4IRLaIXbX1INL0FPwtHyK33uSYS25VZaTFHkj112YuPZXFnBbTUomkpnO0t3Gf9jGpF0_MkcDAd9qj-TWbI8sy6lJBcvCoA6XvbnphXP0YoDODc5TnrfED_5P5G4n4Gfg0gf2ClGanlkmqRfUQkl2VlcKHMLS0V3Uqwx6wel5gFMEgmVALGCyS-LChbiy7sF8qV24CGHRiBPppdXCEr87v1geseu_16jHvgK-DFaMutXXG5lP6giOzUYMTjfa3XYcQ";
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: www.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Origin: https://www.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://www.tokopedia.com/login';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response message if invalid username
        if (isset($result['data']['isExist'])) {
            $isExist = $result['data']['isExist'];
            if ($isExist == false) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Invalid username');
                return $responseDTO;
            }
        }
        // Response message if invalid phone
        if (isset($result['data']['is_success'])) {
            $isSuccess = $result['data']['is_success'];
            if ($isSuccess == false) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage(current($result['message_error']));
                return $responseDTO;
            }
        }

        // Response success
        return $responseDTO;
    }

    private function _authorize()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://accounts.tokopedia.com/api/authorize');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_POST, 1);

        $postData = "email=" . urlencode($this->username) . "&password=" . urlencode($this->password) . "&remember_me=1&redirect_uri=https%253A%252F%252Fwww.tokopedia.com%252Fappauth%252Fcode&response_type=code&state=eyJyZWYiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIiwidXVpZCI6Ijc5MWQ0ZDEwLTQ5Y2UtNGM1ZS1iMTJjLWNiZjA5MmQ5ODI1OSIsInAiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIn0&client_id=1001";
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: accounts.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Origin: https://www.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-site';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://www.tokopedia.com/login';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $otpPageUrlPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/otpPageUrl.txt";
        if (file_exists($otpPageUrlPath)) {
            unlink($otpPageUrlPath);
        }
        $otpPageUrl = [
            'url' => $result['data']['redirect_url']
        ];
        file_put_contents($otpPageUrlPath, json_encode($otpPageUrl));

        // Response message if invalid password
        $status = $result['header']['status'];
        if ($status == 'failed') {
            $message = current($result['header']['message']);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($message);
            return $responseDTO;
        }

        // Response success
        return $responseDTO;
    }

    /**
     * Load OTP page to determine the verification method
     */
    private function _loadOTPPage()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        if (is_numeric($this->username)) {
            $url = "https://accounts.tokopedia.com/otp/c/page?otp_type=112&msisdn=$this->username&popup=false&header=true&redirect_parent=false&ld=https%3A%2F%2Faccounts.tokopedia.com%2Flpn%2Fusers%3Fphone%3D$this->username%26client_id%3D%26redirect_uri%3D";
        } else {
            $otpPageUrlPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/otpPageUrl.txt";
            if (!file_exists($otpPageUrlPath)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Missing url of otp page');
                return $responseDTO;
            }

            $otpPageUrl = json_decode(file_get_contents($otpPageUrlPath), true);
            $url = $otpPageUrl['url'];
        }
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: accounts.tokopedia.com';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Sec-Fetch-User: ?1';
        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: navigate';
        $headers[] = 'Referer: https://accounts.tokopedia.com/login?ld=https%3A%2F%2Fta.tokopedia.com%2Fv2%2Fmanage';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        // This api return a HTML string
        $result = $response;

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * @param $tk
     * @param $otpType
     * @param $msisdn
     * @param $email
     * @param $numberOTPDigit
     * @return DTOResponse
     */
    private function _requestSMS($tk, $otpType, $msisdn, $email, $numberOTPDigit)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://accounts.tokopedia.com/otp/c/ajax/request-sms');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_POST, 1);

        $postData = "tk=$tk&otp_type=$otpType&msisdn=$msisdn&email=$email&original_param=&user_id=&signature=&number_otp_digit=$numberOTPDigit";
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: accounts.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Origin: https://accounts.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://accounts.tokopedia.com/otp/c/page?allownold=1&b=&func_param=last_decline%7Chttps%3A%2F%2Faccounts.tokopedia.com%2Fauthorize%3Fclient_id%3D1001%26login_type%3D%26login_using%3Dotp%26p%3Dhttps%253A%252F%252Fwww.tokopedia.com%26redirect_uri%3Dhttps%25253A%25252F%25252Fwww.tokopedia.com%25252Fappauth%25252Fcode%26response_type%3Dcode%26state%3DeyJyZWYiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIiwidXVpZCI6IjlhODc1NDJiLTIzMmItNDgyMC1hYjZkLTc1NmNmZmUxNzAxYyIsInAiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIn0%26theme%3D&load_func_url=https%3A%2F%2Faccounts.tokopedia.com%2Flogin%2Fassets%3Ftype%3Dsqbl&msisdn=&otp_type=134';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        return $responseDTO;
    }

    /**
     * @param $tk
     * @param $otpType
     * @param $msisdn
     * @param $email
     * @param $numberOTPDigit
     * @return DTOResponse
     */
    private function _requestEmail($tk, $otpType, $msisdn, $email, $numberOTPDigit)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://accounts.tokopedia.com/otp/c/ajax/request-email');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_POST, 1);

        $postData = "tk=$tk&otp_type=$otpType&msisdn=$msisdn&email=$email&original_param=&ld=&user_id=&signature=&number_otp_digit=$numberOTPDigit";
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: accounts.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Origin: https://accounts.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://accounts.tokopedia.com/otp/c/page?allownold=1&b=&func_param=last_decline%7Chttps%3A%2F%2Faccounts.tokopedia.com%2Fauthorize%3Fclient_id%3D1001%26login_type%3D%26login_using%3Dotp%26p%3Dhttps%253A%252F%252Fwww.tokopedia.com%26redirect_uri%3Dhttps%25253A%25252F%25252Fwww.tokopedia.com%25252Fappauth%25252Fcode%26response_type%3Dcode%26state%3DeyJyZWYiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIiwidXVpZCI6ImU0ZGFmZDQ5LTIzYmEtNGFkOC1hZTE1LWE5YmVjMGNhNzdkZCIsInAiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIn0%26theme%3D&load_func_url=https%3A%2F%2Faccounts.tokopedia.com%2Flogin%2Fassets%3Ftype%3Dsqbl&msisdn=&otp_type=134';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        return $responseDTO;
    }

    /**
     * @param $otp
     * @return DTOResponse
     * @throws Exception
     */
    public function getCookies($otp)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $result = $this->_OTPVerify($otp);
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $result = $this->_authorizeAfterOTPVerify();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $result = $this->_appAuth();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $result = $this->_getShopId();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $result = $this->_getDID();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        // Response success
        return $responseDTO;
    }

    /**
     * @param $otp
     * @return DTOResponse
     */
    private function _OTPVerify($otp)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $otpVerifyParamsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/otpVerifyParams.txt";
        if (!file_exists($otpVerifyParamsPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing otp verify params');
            return $responseDTO;
        }
        $otpVerifyParams = json_decode(file_get_contents($otpVerifyParamsPath), true);
        $tk = $otpVerifyParams['tk'];
        $otpType = $otpVerifyParams['otpType'];
        $msisdn = $otpVerifyParams['msisdn'];
        $email = $otpVerifyParams['email'];
        $mode = $otpVerifyParams['mode'];

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://accounts.tokopedia.com/otp/c/ajax/verify');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_POST, 1);

        $postData = "code=$otp&tk=$tk&otp_type=$otpType&bc=0&msisdn=$msisdn&f=eyJ1c2VyX2FnZW50IjoiTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvNzkuMC4zOTQ1LjEzMCBTYWZhcmkvNTM3LjM2IiwibGFuZ3VhZ2UiOiJlbi1VUyIsImNvbG9yX2RlcHRoIjoyNCwicGl4ZWxfcmF0aW8iOjEsImhhcmR3YXJlX2NvbmN1cnJlbmN5Ijo4LCJzZXNzaW9uX3N0b3JhZ2UiOjEsImxvY2FsX3N0b3JhZ2UiOjEsImluZGV4ZWRfZGIiOjEsIm9wZW5fZGF0YWJhc2UiOjEsImNwdV9jbGFzcyI6InVua25vd24iLCJkb19ub3RfdHJhY2siOiJ1bmtub3duIiwicmVndWxhcl9wbHVnaW5zIjpbIkNocm9tZSBQREYgUGx1Z2luOjpQb3J0YWJsZSBEb2N1bWVudCBGb3JtYXQ6OmFwcGxpY2F0aW9uL3gtZ29vZ2xlLWNocm9tZS1wZGZ%2BcGRmIiwiQ2hyb21lIFBERiBWaWV3ZXI6Ojo6YXBwbGljYXRpb24vcGRmfnBkZiIsIk5hdGl2ZSBDbGllbnQ6Ojo6YXBwbGljYXRpb24veC1uYWNsfixhcHBsaWNhdGlvbi94LXBuYWNsfiJdLCJjYW52YXMiOiJaVFExWmpVeU9UQmxNRFkwT0dGaVpUWmtOREppWVRnMFpEWmlNVFppT0dRMk5ESTVaamd5T0E9PSIsIndlYmdsIjoiT1RGbE5HVTNOVGs1WXpobFpXVmhOalU0WmpBelpETTROalkyTjJOa016RmhNR0ZpTm1abU9RPT0iLCJhZGJsb2NrIjpmYWxzZSwiaGFzX2xpZWRfbGFuZ3VhZ2VzIjpmYWxzZSwiaGFzX2xpZWRfcmVzb2x1dGlvbiI6ZmFsc2UsImhhc19saWVkX29zIjpmYWxzZSwiaGFzX2xpZWRfYnJvd3NlciI6ZmFsc2UsInRvdWNoX3BvaW50X2NvdW50IjowLCJ0b3VjaF9ldmVudF9pbmQiOmZhbHNlLCJ0b3VjaF9wcm9wZXJ0eV9pbmQiOmZhbHNlLCJqc19mb250cyI6WyJBcmlhbCIsIkNvdXJpZXIiLCJDb3VyaWVyIE5ldyIsIkhlbHZldGljYSIsIk1TIEdvdGhpYyIsIk1TIFBHb3RoaWMiLCJUaW1lcyIsIlRpbWVzIE5ldyBSb21hbiJdLCJhdWRpb19zYW1wbGVfcmF0ZSI6IjQ0MTAwIiwiYXVkaW9fbWF4X2NoYW5uZWxfY291bnQiOiIyIiwiYXVkaW9faW5wdXRfY291bnQiOiIxIiwiYXVkaW9fb3V0cHV0X2NvdW50IjoiMCIsImF1ZGlvX2NoYW5uZWxfY291bnQiOiIyIiwiY2hhbm5lbF9jb3VudF9tb2RlIjoiZXhwbGljaXQiLCJjaGFubmVsX2ludGVycHJldGF0aW9uIjoic3BlYWtlcnMiLCJ3cml0aW5nX3NjcmlwdCI6WyJMYXRpbiIsIkNoaW5lc2UiLCJBcmFiaWMiLCJEZXZhbmFnYXJpIiwiQ3lyaWxsaWMiLCJCZW5nYWxpL0Fzc2FtZXNlIiwiS2FuYSIsIkd1cm11a2hpIiwiSGFuZ3VsIiwiVGVsdWd1IiwiVGFtaWwiLCJNYWxheWFsYW0iLCJCdXJtZXNlIiwiVGhhaSIsIkthbm5hZGEiLCJHdWphcmF0aSIsIkxhbyIsIk9kaWEiLCJHZS1leiIsIlNpbmhhbGEiLCJBcm1lbmlhbiIsIktobWVyIiwiR3JlZWsiLCJMb250YXJhIiwiSGVicmV3IiwiVGliZXRhbiIsIkdlb3JnaWFuIiwiTW9kZXJuIFlpIiwiVGlmaW5hZ2giLCJTeXJpYWMiLCJUaGFhbmEiLCJJbnVrdGl0dXQiLCJDaGVyb2tlZSJdLCJ6b29tX2xldmVsX3BlcmNlbnQiOiIxMDAlIiwic2NyZWVuX29yaWVudGF0aW9uIjoibGFuZHNjYXBlIiwiY29va2llc19pbmQiOnRydWUsInNjcmVlbl9yZXNvbHV0aW9uIjoiMTM2Niw3NjgiLCJhdmFpbGFibGVfc2NyZWVuX3Jlc29sdXRpb24iOiIxMjk2LDc0MSIsInRpbWV6b25lIjoiR01UICs3IiwiZGV2aWNlX3N5c3RlbSI6IkxpbnV4IHg4Nl82NCJ9&email=$email&original_param=&mode=$mode&user_id=&signature=";
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: accounts.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Origin: https://accounts.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://accounts.tokopedia.com/otp/c/page?allownold=1&b=&func_param=last_decline%7Chttps%3A%2F%2Faccounts.tokopedia.com%2Fauthorize%3Fclient_id%3D1001%26login_type%3D%26login_using%3Dotp%26p%3Dhttps%253A%252F%252Fwww.tokopedia.com%26redirect_uri%3Dhttps%25253A%25252F%25252Fwww.tokopedia.com%25252Fappauth%25252Fcode%26response_type%3Dcode%26state%3DeyJyZWYiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIiwidXVpZCI6ImQ0NjJkMWNjLTljNmUtNDI3NS04MTE1LTIyNjQzYTkyNzhkZCIsInAiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIn0%26theme%3D&load_func_url=https%3A%2F%2Faccounts.tokopedia.com%2Flogin%2Fassets%3Ftype%3Dsqbl&msisdn=&otp_type=134';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response message if invalid otp code
        if (isset($result['success']) && $result['success'] == false) {
            $message = $result['error_message'] == 'Kode Verifikasi salah.' ? 'OTP code is incorrect.' : $result['error_message'];
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($message);
            return $responseDTO;
        }

        $authorizeParamsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/authorizeParams.txt";
        if (file_exists($authorizeParamsPath)) {
            unlink($authorizeParamsPath);
        }
        $authorizeParams = [
            'validateToken' => $result['validate_token']
        ];
        file_put_contents($authorizeParamsPath, json_encode($authorizeParams));

        // Response success
        return $responseDTO;
    }

    private function _authorizeAfterOTPVerify()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $authorizeParamsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/authorizeParams.txt";
        if (!file_exists($authorizeParamsPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing authorize params');
            return $responseDTO;
        }
        $authorizeParams = json_decode(file_get_contents($authorizeParamsPath), true);
        $validateToken = $authorizeParams['validateToken'];

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://accounts.tokopedia.com/api/authorize');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_POST, 1);

        if (is_numeric($this->username)) {
            $postData = "client_id=&redirect_uri=&phone=$this->username&email=$this->username&v=true&validate_token=$validateToken&login_type=phone&theme=&state=&log-data=&code=";
        } else {
            $postData = "client_id=1001&login_type=&login_using=otp&p=https%3A%2F%2Fwww.tokopedia.com&redirect_uri=https%253A%252F%252Fwww.tokopedia.com%252Fappauth%252Fcode&response_type=code&state=&theme=&validate_token=$validateToken";
        }
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: accounts.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Origin: https://accounts.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://accounts.tokopedia.com/otp/c/page?allownold=1&b=&func_param=last_decline%7Chttps%3A%2F%2Faccounts.tokopedia.com%2Fauthorize%3Fclient_id%3D1001%26login_type%3D%26login_using%3Dotp%26p%3Dhttps%253A%252F%252Fwww.tokopedia.com%26redirect_uri%3Dhttps%25253A%25252F%25252Fwww.tokopedia.com%25252Fappauth%25252Fcode%26response_type%3Dcode%26state%3DeyJyZWYiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIiwidXVpZCI6Ijc3YjE2Y2NhLTUxM2YtNGIxMS04Y2VmLWZmY2NkZDMyODdmMiIsInAiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tIn0%26theme%3D&load_func_url=https%3A%2F%2Faccounts.tokopedia.com%2Flogin%2Fassets%3Ftype%3Dsqbl&msisdn=&otp_type=134';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Get and store params from redirect url
        $redirectUrl = $result['data']['redirect_url'];
        $parts = parse_url($redirectUrl);
        parse_str($parts['query'], $query);
        $redirectUrlParamsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/redirectUrlParams.txt";
        if (file_exists($redirectUrlParamsPath)) {
            unlink($redirectUrlParamsPath);
        }
        $redirectUrlParams = [
            'code' => $query['code'],
            'state' => $query['state'],
            'userId' => $query['userid'],
        ];
        file_put_contents($redirectUrlParamsPath, json_encode($redirectUrlParams));

        // Get and store Tokopedia user id
        $credentialsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/credentials.txt";
        if (file_exists($credentialsPath)) {
            unlink($credentialsPath);
        }
        $credentials = [
            'userId' => $query['userid'],
        ];
        file_put_contents($credentialsPath, json_encode($credentials));

        // Response success
        return $responseDTO;
    }

    private function _appAuth()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $redirectUrlParamsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/redirectUrlParams.txt";
        if (!file_exists($redirectUrlParamsPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing redirect url params');
            return $responseDTO;
        }
        $redirectUrlParams = json_decode(file_get_contents($redirectUrlParamsPath), true);
        $code = $redirectUrlParams['code'];
        $state = $redirectUrlParams['state'];
        $userId = $redirectUrlParams['userId'];

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, "https://www.tokopedia.com/appauth/code?code=$code&is_reg=&is_socmed_register=&login=&login_type=&state=$state&theme=&userid=$userId");
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: www.tokopedia.com';
        $headers[] = 'Cache-Control: max-age=0';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Sec-Fetch-User: ?1';
        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9';
        $headers[] = 'Sec-Fetch-Site: none';
        $headers[] = 'Sec-Fetch-Mode: navigate';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        // This api not return a json data
        $result = $response;

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        return $responseDTO;
    }

    private function _getShopId()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://www.tokopedia.com/');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: www.tokopedia.com';
        $headers[] = 'Cache-Control: max-age=0';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Sec-Fetch-User: ?1';
        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9';
        $headers[] = 'Sec-Fetch-Site: none';
        $headers[] = 'Sec-Fetch-Mode: navigate';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        // This api return a HTML string
        $result = $response;

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Return a HTML string
        $html = $response;
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $xpath = new \DOMXPath($dom);
        $scriptTags = $xpath->query('//body//script[not(@src)]');
        $scriptTag = $scriptTags->item(1)->nodeValue;
        $arrVariables = explode(';', $scriptTag);
        $shopId = null;
        foreach ($arrVariables as $variable) {
            if (strstr($variable, 'window.__cache')) {
                $variableValue = explode('window.__cache=', $variable);
                $windowCache = json_decode($variableValue[1], true);
                $shopInfo = $windowCache['$ROOT_QUERY.userShopInfo.info'];
                $shopId = $shopInfo['shop_id'];
                break;
            }
        }

        // Store Tokopedia shop id
        $credentialsPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/credentials.txt";
        if (!file_exists($credentialsPath)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('File credentials.txt not found');
            return $responseDTO;
        }
        $credentials = json_decode(file_get_contents($credentialsPath), true);
        $credentials['shopId'] = $shopId;
        file_put_contents($credentialsPath, json_encode($credentials));

        // Response success
        return $responseDTO;
    }

    private function _getDID()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        $cookies = file_get_contents($cookiesPath);
        parse_str(strtr($cookies, array('&' => '%26', '+' => '%2B', ';' => '&')), $cookies);
        foreach ($cookies as $key => $value) {
            if ($key == "DID") {
                // Store cookie DID
                $didPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/DID.txt";
                file_put_contents($didPath, json_encode([
                    'DID=' . $value
                ]));

                // Response success
                return $responseDTO;
            }
        }

        $responseDTO->setIsSuccess(false);
        $responseDTO->setMessage('Cookie DID not found');
        return $responseDTO;
    }

    public function extendCookies()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (is_numeric($this->username)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Currently, not support extend cookies when login by phone number');
            return $responseDTO;
        } else {
            // Get cookies when load login page by Selenium
            $seleniumTokopedia = new \App\Library\Selenium\Channel\Tokopedia($this->username, $this->password, $this->ventureId);
            $seleniumTokopedia->getBrowserCookies();

            // Check already has cookies or not
            $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
            if (!file_exists($cookiesPath)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Missing cookies get from load login page by Selenium');
                return $responseDTO;
            }

            // Replace cookie DID
            $didPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/DID.txt";
            if (!file_exists($didPath)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Missing cookie DID');
                return $responseDTO;
            }
            $did = json_decode(file_get_contents($didPath), true);
            $browserCookies = explode(';', file_get_contents($cookiesPath));
            $arrCookiesAfterReplace = [];
            foreach ($browserCookies as $browserCookie) {
                if (strpos($browserCookie, 'DID') !== false) {
                    array_push($arrCookiesAfterReplace, current($did));
                } else {
                    array_push($arrCookiesAfterReplace, $browserCookie);
                }
            }
            $strCookiesAfterReplace = implode(';', $arrCookiesAfterReplace);
            file_put_contents($cookiesPath, $strCookiesAfterReplace);

            $result = $this->_loadLoginPageWithUsername();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            $result = $this->_retryAuthorize();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            $result = $this->_appAuth();
            if (!$result->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($result->getMessage());
                return $responseDTO;
            }

            try {
                $seleniumTokopedia->loginByCookie();
            } catch (\Exception $e) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($e->getMessage());
                return $responseDTO;
            }
        }

        // Response success
        return $responseDTO;
    }

    private function _retryAuthorize()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $cookiesPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/cookies.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/")) mkdir(SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/");

        curl_setopt($this->ch, CURLOPT_URL, 'https://accounts.tokopedia.com/api/authorize');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);

        $postData = "email=" . urlencode($this->username) . "&password=" . urlencode($this->password) . "&remember_me=1&redirect_uri=https%253A%252F%252Fwww.tokopedia.com%252Fappauth%252Fcode&response_type=code&state=eyJsZCI6Imh0dHBzOi8vdGEudG9rb3BlZGlhLmNvbS92Mi9tYW5hZ2UiLCJwIjoiaHR0cHM6Ly93d3cudG9rb3BlZGlhLmNvbSIsInJlZiI6Imh0dHBzOi8vdGEudG9rb3BlZGlhLmNvbS92Mi9tYW5hZ2U_bGFuZz1pZCIsInV1aWQiOiJiOTI2OGQ0Ni05ZmViLTQxN2EtOTNhMi04ODYxZTJiNTNjMTIifQ&client_id=1001";
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: accounts.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Origin: https://accounts.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Referer: https://accounts.tokopedia.com/login?ld=https%3A%2F%2Fta.tokopedia.com%2Fv2%2Fmanage';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8,zh-CN;q=0.7,zh;q=0.6';

        $headers[] = 'Cookie: ' . file_get_contents($cookiesPath);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $otpPageUrlPath = SYS_PATH . "/storage/app/cookies/tokopedia/$this->ventureId/$this->username/otpPageUrl.txt";
        if (file_exists($otpPageUrlPath)) {
            unlink($otpPageUrlPath);
        }
        $otpPageUrl = [
            'url' => $result['data']['redirect_url']
        ];
        file_put_contents($otpPageUrlPath, json_encode($otpPageUrl));

        // Response message if invalid password
        $status = $result['header']['status'];
        if ($status == 'failed') {
            $message = current($result['header']['message']);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($message);
            return $responseDTO;
        }

        // Response success
        return $responseDTO;
    }
    #endregion
}