<?php

namespace App\Library\Crawler\Channel;


use App\Library;
use App\Models;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Repository\RepositoryBusiness\DTO\DTOShopAds;
use App\Repository\RepositoryBusiness\DTO\DTOShopAdsKeyword;
use Exception;
use Illuminate\Support\Str;


class Shopee
{

    /**
     * @var int
     */
    const KEYWORD_TURN_OFF = 0;

    /**
     * @var int
     */
    const KEYWORD_TURN_ON = 1;

    /**
     * @var int
     */
    const KEYWORD_DEFINED = 0;

    /**
     * @var int
     */
    const KEYWORD_EXPANSION = 1;

    /**
     * @var int
     */
    const SKU_RESUME = 1;

    /**
     * @var int
     */
    const SKU_PAUSE = 2;

    private $_config;

    public $cookiePath;
    public $shopChannel;
    public $venture;
    public $username;
    public $password;
    public $shopIdentifier;
    public $sis;
    protected $subAccountId;
    public $credential;
    public $cookieContent;
    public $cookieState;

    public function __construct($shopChannelId = null)
    {
        if (!$shopChannelId) return;
        $this->shopChannel = Library\Formater::stdClassToArray(Models\ShopChannel::getById($shopChannelId));
        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($this->shopChannel[Models\ShopChannel::COL_FK_CHANNEL] ?? 0));
        $this->venture = $channelInfo[Models\Channel::COL_FK_VENTURE] ?? 0;
        $credentialData = json_decode($this->shopChannel[Models\ShopChannel::COL_SHOP_CHANNEL_API_CREDENTIAL] ?? '{}', true);
        $this->shopIdentifier = intval($credentialData['shop_id'] ?? 0);
        $this->subAccountId = intval($credentialData['subaccount_id'] ?? 0);
        $this->username = $credentialData['username'] ?? '';
        $this->password = $credentialData['password'] ?? '';
        $this->credential = $credentialData;
        $this->cookieContent = $credentialData['cookieString'] ?? '';
        $this->cookieState = $credentialData['cookieState'] ?? ShopeeLogin::COOKIE_STATE_EMPTY;

        if ( ! $this->subAccountId) {
            $cookiesPath = SYS_PATH . "/storage/app/cookies/shopee/$this->venture/$this->username.txt";
            if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
            if (!file_exists(SYS_PATH . "/storage/app/cookies/shopee/")) mkdir(SYS_PATH . "/storage/app/cookies/shopee/");
            if (!file_exists(SYS_PATH . "/storage/app/cookies/shopee/$this->venture/")) mkdir(SYS_PATH . "/storage/app/cookies/shopee/$this->venture/");

            $this->cookiePath = $cookiesPath;
        }
    }

    public function setCredential($usrName, $password, $venture, $sis)
    {
        $this->username = $usrName;
        $this->password = $password;
        $this->venture = $venture;
        $this->sis = $sis;
        if (!$this->shopChannel) {
            throw new \Exception('Please set param shop_channel_id in __construct');
        }
    }

    public function setVenture($venture)
    {
        $this->venture = $venture;
        return $this;
    }

    private function testCookie()
    {
        $res = json_decode($this->getWallet(), true);
        # Main account
        $errorCode = $res['err_code'] ?? null;
        $errorMessage = $res['err_msg'] ?? null;
        if ($errorCode == 0 && $errorMessage == 'success') {
            return true;
        }
        # Sub account
        if ($errorCode == null || $errorMessage == null)
            return ! (($res['errcode'] ?? 0) == 2 || ($res['message'] ?? '') == 'token not found');
        # Default false
        return false;
    }

    public function loginOtp($otp)
    {
        $shopeeAuth = new ShopeeLogin(
            $this->shopChannel[Models\ShopChannel::COL_SHOP_CHANNEL_ID],
            $this->username,
            $this->password,
            $this->cookieState,
            $this->cookieContent,
            $this->venture,
            $this->subAccountId,
            $this->credential
        );
        list($cookieString, $cookieState) = $shopeeAuth->login($otp);

        if ($cookieState == ShopeeLogin::COOKIE_STATE_COMPLETED) {
            return true;
        }

        $this->cookieContent = $cookieString;

        return false;
    }

    public function getOtp()
    {
        $shopeeAuth = new ShopeeLogin(
            $this->shopChannel[Models\ShopChannel::COL_SHOP_CHANNEL_ID],
            $this->username,
            $this->password,
            $this->cookieState,
            $this->cookieContent,
            $this->venture,
            $this->subAccountId,
            $this->credential
        );
        $shopeeAuth->login();
    }

    public function login()
    {
        $username = $this->username;
        $password = $this->password;
        if (!$username || !$password) return false;
        if ($this->cookieState == ShopeeLogin::COOKIE_STATE_NEED_VERIFY) return false;
        if ($this->cookieContent != "" && $this->testCookie()) {
        #if ($this->subAccountId && $this->testCookie()) {
            return true;
        } else {
            $shopeeAuth = new ShopeeLogin(
                $this->shopChannel[Models\ShopChannel::COL_SHOP_CHANNEL_ID],
                $this->username,
                $this->password,
                $this->cookieState,
                $this->cookieContent,
                $this->venture,
                $this->subAccountId,
                $this->credential
            );
            list($cookieString, $cookieState) = $shopeeAuth->login();

            // Login Success
            if ($cookieState == ShopeeLogin::COOKIE_STATE_COMPLETED) {
                $this->cookieState = $cookieState;
                $this->cookieContent = $cookieString;
                return true;
            }

            // All else => Failure
            $this->cookieState = $cookieState;
            $this->cookieContent = $cookieString;
            return false;
        }
    }

    /**
     * @param int $shopId
     * @return bool|string
     */
    private function switchShop($shopId)
    {
        $shopId = intval($shopId);
        if (!$shopId) return false;

        $randomString = Library\Common::randomString(32);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/subaccount/switch_shop/$shopId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/portal/shop';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        curl_close($ch);

        if (php_sapi_name() == 'cli')
            print_r("\n$result\n");

        return json_decode($result, true)['url'] ?? '';
    }

    /**
     * @param string $sig
     * @param int $shopId
     * @param string $shopName
     * @return bool
     */
    private function loginBySig($sig, $shopId, $shopName)
    {
        if (!$sig) return false;
        $subAccountIdentify = str_replace(' ', '', $shopName) . '.' . $shopId;
        $cookiesPath = SYS_PATH . "/storage/app/cookies/shopee/$this->venture/$subAccountIdentify.txt";
        if (!file_exists(SYS_PATH . "/storage/app/")) mkdir(SYS_PATH . "/storage/app/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/")) mkdir(SYS_PATH . "/storage/app/cookies/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/shopee/")) mkdir(SYS_PATH . "/storage/app/cookies/shopee/");
        if (!file_exists(SYS_PATH . "/storage/app/cookies/shopee/$this->venture/")) mkdir(SYS_PATH . "/storage/app/cookies/shopee/$this->venture/");
        if (file_exists($cookiesPath)) {
            unlink($cookiesPath);
        }

        $randomString = Library\Common::randomString(32);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/login/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sig);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/?' . $sig;
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiesPath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiesPath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        curl_close($ch);
        $this->cookiePath = $cookiesPath;
        if (php_sapi_name() == 'cli')
            print_r("\n$result\n");
        return file_exists($this->cookiePath) && (json_decode($result, true)['username'] ?? false);
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/adsAccounts/$this->shopIdentifier/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return $result['pas/ads-account']['balance'] ?? null;
    }

    /**
     * @return string
     */
    public function getWallet()
    {
        $ch = curl_init();
        $randomString = Str::random(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/adsAccounts/$this->shopIdentifier/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($this->subAccountId) {
            $headers[] = 'Cookie: '.$this->cookieContent;
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        } else {
            curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        }

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $result = curl_exec($ch);
        curl_close($ch);
        #$result = json_decode($result, true);
        return $result;
    }

    public static function testLogin($username, $password, $venture)
    {
        if (!$username || !$password) {
            return false;
        }
        //$cookiesPath = SYS_PATH."/storage/app/cookies/shopee/$this->venture/$username.txt";
        //if (!file_exists(SYS_PATH."/storage/app/cookies/shopee/$this->venture/")) mkdir(SYS_PATH."/storage/app/cookies/shopee/$this->venture/");
        //if (file_exists($cookiesPath)) { unlink($cookiesPath); }
        $ch = curl_init();
        $params = [
            'captcha' => '',
            'captcha_key' => '447ba8b633054132a275d220008d28ad',
            'remember' => 'false',
            'password_hash' => hash('sha256', md5($password)),
        ];
        if (is_numeric($username)) {
            $params['phone'] = $username;
        } else {
            $validatorEmail = new Zend_Validate_EmailAddress();
            if ($validatorEmail->isValid($username)) {
                $params['email'] = $username;
            } else {
                $params['username'] = $username;
            }
        }
        $domain = '';
        switch ($venture) {
            case Models\Venture::VIETNAM:
                $domain = 'https://banhang.shopee.vn';
                break;

            case Models\Venture::PHILIPPINES:
                $domain = 'https://seller.shopee.ph';
                break;

            case Models\Venture::MALAYSIA:
                $domain = 'https://seller.shopee.com.my';
                break;

            case Models\Venture::SINGAPORE:
                $domain = 'https://seller.shopee.sg';
                break;

            case Models\Venture::INDONESIA:
                $domain = 'https://seller.shopee.co.id';
                break;

            case Models\Venture::THAILAND:
                $domain = 'https://seller.shopee.co.th';
                break;
        }

        curl_setopt($ch, CURLOPT_URL, $domain . "/api/v1/login/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result, true);
        return isset($result['username']) || (isset($result['message']) && $result['message'] == 'error_need_otp');
    }

    /**
     * search Channel Id And Status
     * @return array
     * example: [
     *      channelId_1 => status_1,
     *      channelId_2 => status_1,
     *      channelId_3 => status_2,
     *      channelId_4 => status_3,
     *      channelId_5 => status_4,
     *      ]
     */
    public function searchChannelIdAndStatus()
    {
        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $result = curl_exec($ch);
        curl_close($ch);

        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__;
        $result = json_decode($result, true);
        return array_column($result['pas/campaign-ads'] ?? [], 'status', 'id');
    }

    /**
     * search Keyword And Status
     * @return array
     * example array return:
     * [
     *      channelAdsId_1 => [
     *          keyword_1 => 1,
     *          keyword_2 => 0,
     *          keyword_3 => 1,
     *      ],
     *      channelAdsId_2 => [
     *          keyword_4 => 0,
     *          keyword_2 => 0,
     *          keyword_1 => 1,
     *      ],
     *      ...
     * ]
     */
    public function searchKeywordAndStatus()
    {
        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $result = curl_exec($ch);
        curl_close($ch);

        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__;

        $assocChannelAdsIdKeywordData = [];

        $result = json_decode($result, true);
        foreach ($result['pas/campaign-ads'] ?? [] as $product) {
            $channelAdsId = $product['id'];
            $assocChannelAdsIdKeywordData[$channelAdsId] = [];

            foreach ($product['keywords'] as $keyword) {
                $assocChannelAdsIdKeywordData[$channelAdsId][$keyword['keyword']] = $keyword['status'];
            }
        }

        return $assocChannelAdsIdKeywordData;
    }

    /**
     * add New Keyword
     * @param int $adsId
     * @param array $keywordData
     * @return bool
     */
    public function addNewKeyword($adsId, $keywordData)
    {
        $adsId = intval($adsId);

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $resultCampaignAds = curl_exec($ch);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $keywordArray = $resultCampaignAds['pas/campaign-ads'][0]['keywords'];
        if (!isset($keywordArray)) return false;

        $resultCampaignAds['pas/campaign-ads'][0]['keywords'] = array_merge($keywordArray, $keywordData);

        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->shopeeSellerDomain());
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($resultCampaignAds));

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return isset($result['pas/campaign-ads']) && $result['pas/campaign-ads']['success'];
    }

    /**
     * add New Keyword And Modify Sku
     * @param int $adsId
     * @param int $status
     * @param array $keywordData
     * @param int $dateFrom
     * @param int $dateTo
     * @param float $totalQuota
     * @param float $dailyQuota
     * @return bool
     * ---------------
     * 1 is ongoing, 2 is paused
     */
    public function addNewKeywordAndModifySku($adsId, $status, $keywordData, $dateFrom, $dateTo, $totalQuota, $dailyQuota)
    {
        $adsId = intval($adsId);
        $status = intval($status);
        $dateFrom = intval($dateFrom);
        $dateTo = intval($dateTo);
        $totalQuota = floatval($totalQuota);
        $dailyQuota = floatval($dailyQuota);

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $resultCampaignAds = curl_exec($ch);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $keywordArray = $resultCampaignAds['pas/campaign-ads'][0]['keywords'];
        if (!isset($keywordArray)) return false;

        $resultCampaignAds['pas/campaign-ads'][0]['keywords'] = array_merge($keywordArray, $keywordData);

        $resultCampaignAds['pas/campaign-ads'][0]['status'] = $status;

        $resultCampaignAds['pas/campaign-ads'][0]['start_timestamp'] = $dateFrom;
        $resultCampaignAds['pas/campaign-ads'][0]['end_timestamp'] = $dateTo;

        $resultCampaignAds['pas/campaign-ads'][0]['total_quota'] = $totalQuota;
        $resultCampaignAds['pas/campaign-ads'][0]['daily_quota'] = $dailyQuota;

        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->shopeeSellerDomain());
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($resultCampaignAds));

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return isset($result['pas/campaign-ads']) && $result['pas/campaign-ads']['success'];
    }

    /**
     * turn Keyword
     * @param int $adsId
     * @param array $keywordData
     * @param int $status
     * @return bool
     */
    public function turnKeywordWithStatus($adsId, $keywordData, $status)
    {
        $adsId = intval($adsId);

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $resultCampaignAds = curl_exec($ch);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $keywordArray = $resultCampaignAds['pas/campaign-ads'][0]['keywords'];
        if (!$keywordArray) return false;

        foreach ($keywordArray as &$keywordInfo) {
            if (!in_array($keywordInfo['keyword'], $keywordData)) continue;
            $keywordInfo['status'] = $status;
        }

        $resultCampaignAds['pas/campaign-ads'][0]['keywords'] = $keywordArray;
        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->shopeeSellerDomain());
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($resultCampaignAds));

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return isset($result['pas/campaign-ads']) && $result['pas/campaign-ads']['success'];
    }

    /**
     * change Match Type Keyword With Status
     * @param int $adsId
     * @param array $keywordData
     * @param int $status
     * @return bool
     * $keywordData is [
     *      keyword_1, keyword_2, keyword_3, ...
     * ]
     * define is Application_Crawler_Channel_Shopee::KEYWORD_DEFINED
     * expansion is Application_Crawler_Channel_Shopee::KEYWORD_EXPANSION
     */
    public function changeMatchTypeKeywordWithStatus($adsId, $keywordData, $status)
    {
        $adsId = intval($adsId);
        $keywordData = array_map('trim', (array)$keywordData);
        $status = intval($status);

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $resultCampaignAds = curl_exec($ch);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $keywordArray = $resultCampaignAds['pas/campaign-ads'][0]['keywords'];
        if (!$keywordArray) return false;

        foreach ($keywordArray as &$keywordInfo) {
            if (!in_array($keywordInfo['keyword'], $keywordData)) continue;
            $keywordInfo['match_type'] = $status;
        }

        $resultCampaignAds['pas/campaign-ads'][0]['keywords'] = $keywordArray;
        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->shopeeSellerDomain());
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($resultCampaignAds));

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return isset($result['pas/campaign-ads']) && $result['pas/campaign-ads']['success'];
    }

    /**
     * change Bidding Price
     * @param int $adsId
     * @param array $keywordData
     * @return bool
     */
    public function changeBiddingPrice($adsId, $keywordData)
    {
        $adsId = intval($adsId);

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $resultCampaignAds = curl_exec($ch);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $keywordArray = $resultCampaignAds['pas/campaign-ads'][0]['keywords'];
        if (!$keywordArray) return false;

        $inputKeywordArray = array_keys($keywordData);
        foreach ($keywordArray as &$keywordInfo) {
            if (!in_array($keywordInfo['keyword'], $inputKeywordArray)) continue;
            $keywordInfo['price'] = $keywordData[$keywordInfo['keyword']];
        }

        $resultCampaignAds['pas/campaign-ads'][0]['keywords'] = $keywordArray;
        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->shopeeSellerDomain());
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($resultCampaignAds));

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return isset($result['pas/campaign-ads']) && $result['pas/campaign-ads']['success'];
    }

    /**
     * turn Sku
     * $status = 1 is Resume, $status = 2 is Pause
     * @param int $adsId
     * @param int $status
     * @return bool
     */
    public function turnSkuWithStatus($adsId, $status)
    {
        $adsId = intval($adsId);

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $resultCampaignAds = curl_exec($ch);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $resultCampaignAds['pas/campaign-ads'][0]['status'] = $status;

        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->shopeeSellerDomain());
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($resultCampaignAds));

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return isset($result['pas/campaign-ads']) && $result['pas/campaign-ads']['success'];
    }

    /**
     * turn Keyword
     * @param int $itemId
     * @param int $dateFrom
     * @param int $dateTo
     * @param float $totalQuota
     * @param float $dailyQuota
     * @param array $keywordArray
     * [keyword 1 => [price 1, match type 1], keyword 2 => [price 2, match type 2]]
     * 0 is define, 1 is extend
     * @return int
     */
    public function addNewSku($itemId, $dateFrom, $dateTo, $totalQuota, $dailyQuota, $keywordArray)
    {
        $keywords = [];
        foreach ($keywordArray as $keyword => $data) {
            $keywords[] = [
                'keyword' => $keyword,
                'price' => floatval($data[0]),
                'match_type' => $data[1],
                'status' => 1
            ];
        }
        $param = [
            'pas/campaign-ads' => [
                [
                    'status' => 1,
                    'start_timestamp' => $dateFrom,
                    'end_timestamp' => $dateTo,
                    'daily_quota' => $dailyQuota,
                    'total_quota' => $totalQuota,
                    'name' => null,
                    'target' => null,
                    'placement' => 0,
                    'itemid' => $itemId,
                    # 'state' => 'ended',
                    'state' => 'ongoing',
                    'keywords' => $keywords
                ]
            ]
        ];

        $ch = curl_init();

        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/?event=4&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($param));

        $headers = [];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/new/$itemId";
        $headers[] = 'Content-Type: application/json;charset=utf-8';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return $result['pas/campaign-ads']['success'][0] ?? 0;
    }

    /**
     * modify Timeline Sku
     * @param int $adsId
     * @param int $dateFrom
     * @param int $dateTo
     * @return bool
     */
    public function modifyTimelineSku($adsId, $dateFrom, $dateTo)
    {
        $adsId = intval($adsId);
        $dateFrom = intval($dateFrom);
        $dateTo = intval($dateTo);

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $resultCampaignAds = curl_exec($ch);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $resultCampaignAds['pas/campaign-ads'][0]['start_timestamp'] = $dateFrom;
        $resultCampaignAds['pas/campaign-ads'][0]['end_timestamp'] = $dateTo;

        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->shopeeSellerDomain());
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($resultCampaignAds));

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return isset($result['pas/campaign-ads']) && $result['pas/campaign-ads']['success'];
    }

    /**
     * modify Budget Sku
     * @param int $adsId
     * @param float $totalQuota
     * @param float $dailyQuota
     * @return bool
     */
    public function modifyBudgetSku($adsId, $totalQuota, $dailyQuota)
    {
        $adsId = intval($adsId);
        $totalQuota = floatval($totalQuota);
        $dailyQuota = floatval($dailyQuota);

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?placement=0&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $resultCampaignAds = curl_exec($ch);
        curl_close($ch);
        $resultCampaignAds = json_decode($resultCampaignAds, true);
        $resultCampaignAds['pas/campaign-ads'][0]['total_quota'] = $totalQuota;
        $resultCampaignAds['pas/campaign-ads'][0]['daily_quota'] = $dailyQuota;

        $from = strtotime('today');
        $to = strtotime('tomorrow') - 1;

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaignAds/$adsId/?SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = [];
        $headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en-US;q=0.9,en;q=0.8,ja;q=0.7';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId?from=$from&group=today&to=$to";
        $headers[] = 'Authority: ' . str_replace('https://', '', $this->shopeeSellerDomain());
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($resultCampaignAds));

        $result = curl_exec($ch);
        curl_close($ch);
        if (php_sapi_name() == 'cli')
            echo "\n" . __FUNCTION__ . "\n$result\n";
        $result = json_decode($result, true);
        return isset($result['pas/campaign-ads']) && $result['pas/campaign-ads']['success'];
    }

    /**
     * get Suggestions Price By Keyword
     * @param int $adsId
     * @param int $itemId
     * @param string $keyword
     * @return array
     * array format [
     * keyword_1 => recommend_price_1,
     * keyword_2 => recommend_price_2,
     * ...
     * ]
     */
    public function getSuggestionsPriceByKeyword($adsId, $itemId, $keyword)
    {
        $adsId = intval($adsId);
        $itemId = intval($itemId);
        $keyword = strtolower(trim($keyword));

        $ch = curl_init();
        $randomString = Library\Common::randomString(32);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/keyword/suggestions/?product=$itemId&keyword=$keyword&count=100&curated=true&SPC_CDS=$randomString&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = [];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = "Referer: " . $this->shopeeSellerDomain() . "/portal/marketing/pas/assembly/$adsId";
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        curl_close($ch);

        if (php_sapi_name() == 'cli') echo "\n" . __FUNCTION__ . "\n$result\n";

        $result = json_decode($result, true)['keywords'] ?? [];
        return array_column($result, 'recommend_price', 'keyword');
    }

    public function getOrderCancelNote($orderNumber)
    {
        $ch = curl_init();
        $limit = 40;
        $offset = 0;
        $search = $orderNumber;
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/orders/?is_massship=false&limit=$limit&offset=$offset&search=$search&type=cancelled");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $result = curl_exec($ch);
        $order = current(json_decode($result, true)['orders'] ?? []);
        return $this->extractSOCancelNote($order);
    }

    public function extractSOCancelNote($order)
    {
        if (!$order) return "";
        $note = '';
        $detailNote = '';
        $userId = $order["userid"];
        $cancelUserId = $order["cancel_userid"];
        $cancelReasonExt = $order['cancel_reason_ext'];
        $isBuyerCancelToShip = $order['is_buyercancel_toship'];
        $instantBuyerCancelToShip = $order['instant_buyercancel_toship'];

        $paymentMethod = $order['payment_method'];
        $logisticsStatus = $order['logistics_status'];

        $label = $this->translate("action_label_cancelled");
        if ($cancelUserId <= 0) {
            $note = $this->translate("v2_action_buyer_cancelled_by_system_summary_text");
            $detailNote =
                $cancelReasonExt == self::CANCEL_REASON_SYSTEM_PAYMENT_REJECTED ||
                $cancelReasonExt == self::CANCEL_REASON_SYSTEM_UNPAID ||
                $cancelReasonExt == self::CANCEL_REASON_SYSTEM_UNDERPAID ?
                    $this->translate("action_seller_cancelled_by_system_rejected_detail_text") :
                    $this->translate("v2_action_buyer_cancelled_by_system_summary_text");
        } elseif ($cancelUserId == $userId) {
            $note = $this->translate("v2_action_seller_cancelled_by_buyer_summary_text");
            $detailNote = $this->translate("v2_action_seller_cancelled_by_buyer_detail_text");
            if ($isBuyerCancelToShip && !$instantBuyerCancelToShip) {
                $note = $detailNote = $this->translate("ps_sales_status_text_accepted_cancellation_request");
            }
        } else {
            $note = $this->translate("v2_action_seller_cancelled_by_seller_summary_text");
            $detailNote = $this->translate("v2_action_seller_cancelled_by_seller_detail_text");
        }
        if ($cancelReasonExt == self::CANCEL_REASON_LOGISTICS_PICKUP_FAILED) {
            $detailNote = $this->translate("v2_action_seller_cancelled_pickup_failed_detail_text");
        } elseif (
            $paymentMethod == self::PAY_COD &&
            $logisticsStatus == self::LOGISTICS_DELIVERY_FAILED
        ) {
            $detailNote = $this->translate("v2_action_seller_cancelled_failed_cod_detail_text");
        } else {
            if ($cancelReasonExt === self::CANCEL_REASON_LOGISTICS_DELIVERY_FAILED) {
                $detailNote = $this->translate("v2_action_buyer_cancelled_delivery_failed_detail_text");
            }
            if ($cancelReasonExt === self::CANCEL_REASON_LOGISTICS_COD_REJECTED) {
                $detailNote = $this->translate("v2_action_seller_cancelled_cod_rejected_detail_text");
            }
        }
        return $note;
        #return $detailNote;
    }

    public function shopeeSellerDomain()
    {
        $result = '';
        switch ($this->venture) {
            case Models\Venture::VIETNAM:
                $result = 'https://banhang.shopee.vn';
                break;
            case Models\Venture::PHILIPPINES:
                $result = 'https://seller.shopee.ph';
                break;
            case Models\Venture::MALAYSIA:
                $result = 'https://seller.shopee.com.my';
                break;
            case Models\Venture::SINGAPORE:
                $result = 'https://seller.shopee.sg';
                break;
            case Models\Venture::INDONESIA:
                $result = 'https://seller.shopee.co.id';
                break;
            case Models\Venture::THAILAND:
                $result = 'https://seller.shopee.co.th';
                break;
        }
        return $result;
    }

    public function shopeeDomain()
    {
        $result = '';
        switch ($this->venture) {
            case Models\Venture::VIETNAM:
                $result = 'https://shopee.vn';
                break;
            case Models\Venture::PHILIPPINES:
                $result = 'https://shopee.ph';
                break;
            case Models\Venture::MALAYSIA:
                $result = 'https://shopee.com.my';
                break;
            case Models\Venture::SINGAPORE:
                $result = 'https://shopee.sg';
                break;
            case Models\Venture::INDONESIA:
                $result = 'https://shopee.co.id';
                break;
            case Models\Venture::THAILAND:
                $result = 'https://shopee.co.th';
                break;
        }
        return $result;
    }

    private function translate($input)
    {
        $data = [
            'action_label_cancelled' => "Đã hủy",
            'v2_action_buyer_cancelled_by_system_summary_text' => "Đã hủy tự động bởi hệ thống Shopee",
            'action_seller_cancelled_by_system_rejected_detail_text' => "Đơn hàng đã bị hủy do Người mua không thanh toán đúng hạn",
            'v2_action_seller_cancelled_by_buyer_summary_text' => "Đã hủy bởi Người mua",
            'ps_sales_status_text_accepted_cancellation_request' => "Yêu cầu hủy đơn hàng của Người mua đã được chấp nhận",
            'v2_action_seller_cancelled_by_seller_summary_text' => "Đã hủy bởi bạn",
            'v2_action_seller_cancelled_pickup_failed_detail_text' => "Đơn hàng đã bị hủy vì bạn đã không giao hàng đúng hạn.",
            'v2_action_seller_cancelled_failed_cod_detail_text' => "Đơn hàng này đã bị hủy vì Người mua đã không thanh toán khi nhận hàng.",
            'v2_action_buyer_cancelled_delivery_failed_detail_text' => "Đơn hàng đã bị hủy vì giao hàng thất bại.",
            'v2_action_seller_cancelled_cod_rejected_detail_text' => "Đơn hàng đã bị hủy vì yêu cầu thanh toán COD của Người mua đã bị từ chối.",
            'v2_action_seller_cancelled_by_seller_detail_text' => "Bạn đã hủy đơn hàng này.",
            'v2_action_seller_cancelled_by_buyer_detail_text' => "Người mua đã hủy đơn hàng này",
        ];
        return $data[$input] ?? $input;
    }

    public function order($offset, $type)
    {
        $ch = curl_init();
        $url = $this->shopeeSellerDomain() . "/api/v2/orders/?is_massship=false&limit=50&offset=$offset&search=&type=$type";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $result = curl_exec($ch);
        #print_r($result);
        return json_decode($result, true);
    }

    public function orderReturned($offset)
    {
        $ch = curl_init();
        $url = $this->shopeeSellerDomain() . "/api/v2/returns/?limit=50&offset=$offset";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $result = curl_exec($ch);
        #print_r($result);
        return json_decode($result, true);
    }

    public function getDataCenterDashboard($from, $to)
    {
        $ch = curl_init();
        $url = $this->shopeeSellerDomain() . "/api/mydata/v1/metrics/shop/summary/?start_time=$from&end_time=$to&period=yesterday";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $result = curl_exec($ch);
        #print_r($result);
        return json_decode($result, true);
    }

    public function getProductStatisticalData()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v3/product/get_product_statistical_data/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $result = curl_exec($ch);
        #print_r($result);
        return json_decode($result, true);
    }

    public function getPurchaseAble($page)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain()
            . "/api/v3/product/page_product_list/?page_number=$page&page_size=24&list_type=live&list_order_type=");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $result = curl_exec($ch);
        return json_decode($result, true);
    }

    public function getShopReview($url)
    {
        $pasteArr = explode('/', trim($url, '/'));
        $pasteUsrName = end($pasteArr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeDomain() . '/api/v2/shop/get?username=' . $pasteUsrName);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        $data = json_decode($result, true)['data'];
        $userId = $data['userid'];
        $follower = $data['follower_count'] ?? 0;
        $responseRate = $data['response_rate'] ?? 0;
        $responseTime = $data['response_time'] ?? 0;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://shopee.vn/buyer/$userId/rating_summary/?&__classic__=1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);

        $totalReviews = $data['score_as_seller'][0] ?? 0;
        unset($data['score_as_seller'][0]);


        return $resultResponse = array(
            'responseRate' => $responseRate,
            'leadTime' => $responseTime,
            'followerNumber' => $follower,
            'ratingData' => array(
                'score' => $data['score_as_seller'] ?? 0,
                'total' => $totalReviews,
                'detail' => $data['seller_rating_count'] ?? []
            ),
        );
    }

    public function orderInfo($orderNumber)
    {
        $url = "{$this->shopeeSellerDomain()}/api/v2/orders/?is_massship=false&search=$orderNumber";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $result = curl_exec($ch);
        return json_decode($result, true)['orders'][0] ?? null;
    }

    public function boostProduct($itemId)
    {
        if (!empty($this->_config)) {
            $this->_config = $this->getBoostProductConfig()['data'] ?? [];
        }
        $coolDown = intval($this->_config['cooldown_seconds'] ?? 144000);
        $domain = $this->shopeeSellerDomain();
        $arrayParams = [
            "id" => intval($itemId)
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $domain . '/api/v3/product/boost_product');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayParams));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);

        $headers = array();
        $headers[] = "Origin: $domain";
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36 OPR/62.0.3331.43';
        $headers[] = 'Content-Type: application/json;charset=UTF-8';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = "Referer: $domain/portal/product/list/all";
        $headers[] = "Authority: " . str_replace('https://', '', $domain);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            if (php_sapi_name() == 'cli') {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);

            return false;
        }
        curl_close($ch);

        if ($result['message'] == 'success') {
            $result = json_decode($result, true);
            $result['cooldown_seconds'] = $coolDown;
        } else {
            $result = false;
        }

        return $result;
    }

    public function crawSubAccountProduct()
    {
        $pageSize = 24;
        $pageNumber = 1;
        $response = $this->_crawSubAccountProduct($pageSize, $pageNumber);
        $pageInfo = $response['data']['page_info'] ?? [];

        $totalPage = ceil(($pageInfo['total'] ?? 0) / $pageSize);
        $products = $response['data']['list'] ?? [];
        $result = $products;
        for ($i = 2; $i <= $totalPage; $i++) {
            $response = $this->_crawSubAccountProduct($pageSize, $i);
            $result = array_merge($result, $response['data']['list'] ?? []);
        }

        return $result;
    }

    private function _crawSubAccountProduct($pageSize, $pageNumber)
    {
        $ch = curl_init();
        $randomString = Library\Common::randomString(32);

        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v3/product/page_product_list/?SPC_CDS={$randomString}&SPC_CDS_VER=2&page_number={$pageNumber}&page_size={$pageSize}&list_type=&search_type=name&source=seller_center");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        //$headers[] = 'Referer: https://seller.shopee.ph/portal/product/list/all?page=1';
        $headers[] = 'Connection: keep-alive';
        //$headers[] = 'Cookie: _gcl_au=1.1.858761477.1563008885; SPC_F=ZbvXUQi938RVQ38HAC6FCBX1XqKUYXjW; _fbp=fb.1.1563008892172.433851794; _ga=GA1.2.1416296000.1563008892; SPC_T_IV=\"lEAo11xOHembAOHjRwd+MA==\"; SPC_T_ID=\"G94ybpGSRnU2WC5WFKLsnWGJKIyVyUW5Qsk2DMu4bHBWyvEN+kTmCuIACu1kQTS8OCURnCcXFPjMyjKch6v6S+Tu//DF60epbm5UUj1Xah0=\"; SPC_T_F=1; SC_DFP=2jVgtTLTVQ8VaxUA87z2CvL5EYVTdn9H; SC_SSO=\"wuqk11rfhzzis9HO81eFz0sC6lGGO+/Mk9nFjb3y1F5hOAsNhoMeryl0ViqI4qKx\"; SPC_SC_TK=c2a4357712820a7d0a2435ec5b1fe6c9; SC_SSO_U=46855; SPC_SC_UD=17916242; SPC_SC_SA_TK=b2fdfd4db1c2b4e0bb8de18390ce906d; SPC_SC_SA_UD=46855; SPC_CDS=caa032c9-81f6-493e-931f-64d3ab5f48fd';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response ? json_decode($response, true) : [];
    }

    /**
     * @param int $dateFrom
     * @return Library\Response\Channel
     * @throws Exception
     */
    public function crawSubAccountOrder($dateFrom)
    {
        $isLoop = true;
        $offset = 0;
        $limit = 40;
        $result = new Library\Response\Channel();
        $orderData = [];
        do {
            $responseOrders = $this->_crawSubAccountOrder($offset, $limit);
            $arrayProducts = $responseOrders['products'] ?? [];
            $dicProduct = [];
            foreach ($arrayProducts as $product) {
                $dicProduct[$product['itemid']] = $product;
            }

            $arrayOrders = $responseOrders['orders'];
            if (!$arrayOrders) $isLoop = false;

            foreach ($arrayOrders as $order) {
                $createTime = $order['create_time'];
                if ($createTime < $dateFrom) {
                    $isLoop = false;
                    break;
                } else {

                    $channelOrder = new Library\DataStructure\ChannelOrder();
                    $channelOrder->setOrderNumber($order['ordersn']);
                    $channelOrder->setOrderId('');
                    $channelOrder->setVoucherAmount($order['voucher_price']);
                    $channelOrder->setVoucherCode($order['voucher_code']);
                    $channelOrder->setTotalPrice($order['total_price']);
                    $channelOrder->setCreatedAt($createTime);
                    $channelOrder->setStatus('unknown');
                    $channelOrder->setAddressCity($order['seller_address']['city']);
                    $channelOrder->setAddressPhone($order['seller_address']['phone']);
                    $channelOrder->setFullJson(json_encode($order));
                    $channelOrder->setChannelStatusId(null);
                    $channelOrderItemArray = [];
                    $index = 1;
                    foreach ($order['order_items'] as $orderItem) {
                        $channelOrderItem = new Library\DataStructure\ChannelOrder\Item();
                        $explode = explode('-', $orderItem);
                        $itemId = $explode[2];
                        $item = $dicProduct[$itemId];
                        $channelOrderItem->setSku($item['parent_sku']);
                        $channelOrderItem->setName($item['name']);
                        $channelOrderItem->setChannelSku($item['parent_sku']);
                        $channelOrderItem->setItemPrice($item['price']);
                        $channelOrderItem->setVoucherCode('');
                        $channelOrderItem->setVoucherAmount(0);
                        $channelOrderItem->setItemId($itemId);
                        $channelOrderItem->setCreatedAt($createTime);
                        $channelOrderItem->setUpdatedAt(strtotime('now'));
                        $channelOrderItem->setStatusId(null);
                        $channelOrderItem->setDateString(date('Y-m-d H:i:s'));
                        $channelOrderItem->setShipmentProvider($order['actual_carrier']);
                        $channelOrderItem->setInternalId($this->_genInternalId($order['ordersn'], $index));
                        $channelOrderItem->setFullJson(json_encode($item));
                        $channelOrderItemArray[] = $channelOrderItem;

                        $index++;
                    }

                    $channelOrder->setArrayItem($channelOrderItemArray);

                    $orderData[] = $channelOrder;
                }
            }

            if ($isLoop) {
                $offset += 40;
            }
        } while ($isLoop);
        $result->setOrders($orderData);
        return $result;
    }

    private function _genInternalId($orderNumber, $index)
    {
        return sprintf(
            '%s-%s-%s-%s',
            $this->shopChannel['fk_shop_master'],
            $this->shopChannel['fk_channel'],
            $orderNumber,
            $index
        );
    }

    private function _crawSubAccountOrder($offset = 0, $limit = 40)
    {
        $randomString = Library\Common::randomString(32);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/orders/?SPC_CDS={$randomString}&SPC_CDS_VER=2&is_massship=false&limit={$limit}&offset={$offset}&sort_type=sort_desc");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);

    }

    /**
     * Shop Ads
     */

    /**
     * @return bool|mixed|string
     */
    public function crawlShopAdsKeyword()
    {
        $randomString = Library\Common::randomString(32);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaign/list/?SPC_CDS={$randomString}&placement_list=[3]&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Sc-Fe-Ver: drc_v191219';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/portal/marketing/pas/assembly/shop';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        curl_close($ch);
        return $result;
    }

    /**
     * @param $adsId
     * @param $startTime
     * @param $endTime
     * @param int $isGetDetail
     * @return bool|mixed|string
     */
    public function crawlShopAdsKeywordPerformance($adsId, $startTime, $endTime, $isGetDetail = 0)
    {
        $randomString = Library\Common::randomString(32);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/report/detail_report_by_keyword/?start_time={$startTime}&end_time={$endTime}&placement_list=%5B3%5D&agg_interval=96&need_detail={$isGetDetail}&adsid={$adsId}&SPC_CDS={$randomString}&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Sc-Fe-Ver: v191219-fulfillment';
        $headers[] = 'Connection: keep-alive';
        $headers[] = "Referer: {$this->shopeeSellerDomain()}/portal/marketing/pas/assembly/shop";
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        curl_close($ch);
        return $result;
    }

    /**
     * @param string $keyword
     * @return DTOResponse
     * @throws Exception
     */
    public function getSuggestionsPriceShopAdsByKeywordName($keyword)
    {
        $randomString = Library\Common::randomString(32);
        $ch = curl_init();
        $url = $this->shopeeSellerDomain() . "/api/v2/pas/suggest/keyword/?SPC_CDS={$randomString}&SPC_CDS_VER=2&keyword={$keyword}&count=100&placement=3";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Sc-Fe-Ver: v191219-fulfillment';
        $headers[] = 'Connection: keep-alive';
        #$headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/portal/marketing/pas/assembly/shop';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        $result = json_decode($result, true);
        curl_close($ch);
        if ($result && !isset($result['err_code'])) {
            $isSuccess = $this->login();
            if (!$isSuccess) {
                $isSuccess = $this->login();
            }
            if (!$isSuccess) {
                $isSuccess = $this->login();
            }

            if (!$isSuccess) throw new Exception('cannot re-login');
            $randomString = Library\Common::randomString(32);
            $ch = curl_init();
            $url = $this->shopeeSellerDomain() . "/api/v2/pas/suggest/keyword/?SPC_CDS={$randomString}&SPC_CDS_VER=2&keyword={$keyword}&count=100&placement=3";

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
            curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");

            $headers = array();
            $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
            $headers[] = 'Accept: application/json, text/plain, */*';
            $headers[] = 'Accept-Language: en-US,en;q=0.5';
            $headers[] = 'Sc-Fe-Ver: v191219-fulfillment';
            $headers[] = 'Connection: keep-alive';
            #$headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/portal/marketing/pas/assembly/shop';
            $headers[] = 'Te: Trailers';
            $headers[] = 'Cookie: '.$this->cookieContent;
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }

            $result = json_decode($result, true);
            curl_close($ch);
        }
        // Handle channel response
        if (!$result)
            throw new Exception('Channel with null value or error (GET)');
        if ($result['err_code'] != 0)
            throw new Exception('Error channel response error code');
        if (!isset($result['data']))
            throw new Exception('Channel edit response params');

        $data = [];
        foreach ($result['data'] as $keyword) {
            $data[$keyword['keyword']] = $keyword;
        }

        // Build response
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess($result['err_code'] === 0 ? true : false);
        $responseDTO->setData($data);
        return $responseDTO;
    }

    /**
     * @param DTOShopAds|null $shopAdsDTO
     * @param DTOShopAdsKeyword[]|null $arrayShopAdsKeywordDTO
     * @return DTOResponse
     * @throws Exception
     */
    public function editShopAds($shopAdsDTO = null, $arrayShopAdsKeywordDTO = null)
    {
        // Build response
        $responseDTO = new DTOResponse();

        if (is_null($shopAdsDTO) && is_null($arrayShopAdsKeywordDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required at less of $shopAdsDTO or $arrayShopAdsKeywordDTO');
            return $responseDTO;
        }
        if (!is_null($shopAdsDTO))
            if (!($shopAdsDTO instanceof DTOShopAds)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required DTOShopAds Instance');
                return $responseDTO;
            }

        // Get current shop ads data on channel
        $randomString = Library\Common::randomString(32);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaign/list/?SPC_CDS={$randomString}&placement_list=[3]&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Sc-Fe-Ver: drc_v191219';
        $headers[] = 'Connection: keep-alive';
        #$headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/portal/marketing/pas/assembly/shop';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Error: ' . curl_error($ch));
            return $responseDTO;
        }
        $result = json_decode($result, true);
        curl_close($ch);

        // Handle channel response
        if (!$result) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel with null value');
            return $responseDTO;
        }
        if (isset($result['err_code']) && $result['err_code'] != 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Error channel response error code');
            return $responseDTO;
        }
        if (!isset($result['data'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel edit response params');
            return $responseDTO;
        }
        if (!isset($result['data']['campaign_ads_list'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel edit response params');
            return $responseDTO;
        }

        // Prepare data by data response
        $campaignAdsList = $result['data']['campaign_ads_list'];
        $advertisement = $campaignAdsList[0]['advertisements'][0];
        $campaign = $campaignAdsList[0]['campaign'];

        // Want modify shopAds
        if (!is_null($shopAdsDTO)) {
            $timeLineStart = $shopAdsDTO->getTimeLineFrom();
            $timeLineEnd = $shopAdsDTO->getTimeLineTo();
            $budgetDaily = $shopAdsDTO->getDailyQuota();
            $budgetTotal = $shopAdsDTO->getTotalQuota();
            $campaign['status'] = $shopAdsDTO->getStatus() ?? $campaign['status'];
            $campaign['start_time'] = $timeLineStart ?? $campaign['start_time'];
            $campaign['end_time'] = $timeLineEnd ?? $campaign['end_time'];
            if (!is_null($budgetTotal) || !is_null($budgetDaily)) {
                if ($budgetDaily == 0 && $budgetTotal == 0) {
                    $campaign['daily_quota'] = 0;
                    $campaign['total_quota'] = 0;
                } elseif ($budgetDaily != 0 && $budgetTotal != 0) {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage('Can not update both daily and total');
                    return $responseDTO;
                } elseif ($budgetDaily != 0) {
                    $campaign['daily_quota'] = $budgetDaily;
                    $campaign['total_quota'] = 0;
                } elseif ($budgetTotal != 0) {
                    $campaign['daily_quota'] = 0;
                    $campaign['total_quota'] = $budgetTotal;
                }
            }
        }

        // Want edit keyword
        if (!is_null($arrayShopAdsKeywordDTO)) {
            // Insert or update keyword
            $listKeyword = $advertisement['extinfo']['keywords'];
            $arrKeywordName = [];
            unset($advertisement['extinfo']);

            // Build array keyword by keyword name use for search
            foreach ($listKeyword as $key => $keyword) {
                $arrKeywordName[$keyword['keyword']] = $key;
            }

            foreach ($arrayShopAdsKeywordDTO as $shopAdsKeywordDTO) {
                if (!($shopAdsKeywordDTO instanceof DTOShopAdsKeyword)) {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage('Not is a DTOShopAdsKeyword Object');
                    return $responseDTO;
                }
                if (!array_key_exists($shopAdsKeywordDTO->getKeywordName(), $arrKeywordName)) {
                    if (is_null($shopAdsKeywordDTO->getStatus())) {
                        $responseDTO->setIsSuccess(false);
                        $responseDTO->setMessage('Required status');
                        return $responseDTO;
                    }
                    if (is_null($shopAdsKeywordDTO->getMatchType())) {
                        $responseDTO->setIsSuccess(false);
                        $responseDTO->setMessage('Required match type');
                        return $responseDTO;
                    }
                    if (is_null($shopAdsKeywordDTO->getBiddingPrice())) {
                        $responseDTO->setIsSuccess(false);
                        $responseDTO->setMessage('Required bidding price');
                        return $responseDTO;
                    }
                    if (is_null($shopAdsKeywordDTO->getKeywordName())) {
                        $responseDTO->setIsSuccess(false);
                        $responseDTO->setMessage('Required keyword name');
                        return $responseDTO;
                    }
                    $listKeyword[] = [
                        'status' => $shopAdsKeywordDTO->getStatus(),
                        'match_type' => $shopAdsKeywordDTO->getMatchType(),
                        'price' => $shopAdsKeywordDTO->getBiddingPrice(),
                        'keyword' => $shopAdsKeywordDTO->getKeywordName(),
                        'algorithm' => '',
                    ];
                } else {
                    $indexKeywordInList = $arrKeywordName[$shopAdsKeywordDTO->getKeywordName()];
                    // Update data keyword
                    if ($shopAdsKeywordDTO->getStatus() !== null)
                        $listKeyword[$indexKeywordInList]['status'] = $shopAdsKeywordDTO->getStatus();
                    if ($shopAdsKeywordDTO->getMatchType() !== null)
                        $listKeyword[$indexKeywordInList]['match_type'] = $shopAdsKeywordDTO->getMatchType();
                    if ($shopAdsKeywordDTO->getBiddingPrice() !== null)
                        $listKeyword[$indexKeywordInList]['price'] = $shopAdsKeywordDTO->getBiddingPrice();
                }
            }
            // Mapping data advertisements (Insert new extinfo)
            $advertisement['extinfo']['keywords'] = $listKeyword;
        }

        // Rebuild request params to PUT to channel
        $params = [
            'campaign_ads_list' => [
                0 => [
                    'advertisements' => [
                        0 => $advertisement
                    ],
                    'campaign' => $campaign
                ]
            ]
        ];
        $params = json_encode($params);

        // Send request update to channel
        $randomString = Library\Common::randomString(32);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaign/?SPC_CDS={$randomString}&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Content-Type: application/json;charset=utf-8';
        $headers[] = 'Sc-Fe-Ver: v191219-fulfillment';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Error: ' . curl_error($ch));
            return $responseDTO;
        }

        $result = json_decode($result, true);
        curl_close($ch);

        // Handle channel response
        if (!$result) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel with null value or error (PUT)');
            return $responseDTO;
        }

        // Check success
        if ($result['err_code'] != 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setData($result);
            return $responseDTO;
        } else {
            if ($result['data']['res_list'][0]['err_code'] != 0) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setData($result);
                return $responseDTO;
            }
        }
        $responseDTO->setIsSuccess(true);
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * @param DTOShopAds $shopAdsDTO
     * @param DTOShopAdsKeyword[] $arrayShopAdsKeywordDTO
     * @return DTOResponse
     * @throws Exception
     */
    public function createShopAds($shopAdsDTO, $arrayShopAdsKeywordDTO)
    {
        // Build response
        $responseDTO = new DTOResponse();

        if (!($shopAdsDTO instanceof DTOShopAds)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required DTOShopAds instance');
            return $responseDTO;
        }
        if (empty($arrayShopAdsKeywordDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required $arrayShopAdsKeywordDTO');
            return $responseDTO;
        }

        // Info ShopAds
        $campaign = [];
        // Info ShopAds Keywords
        $listKeyword = [];

        // Verify data
        if (is_null($shopAdsDTO->getStatus())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required status');
            return $responseDTO;
        }
        if (is_null($shopAdsDTO->getTimeLineFrom())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required time line from');
            return $responseDTO;
        }
        if (is_null($shopAdsDTO->getTimeLineTo())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required time line to');
            return $responseDTO;
        }
        if (is_null($shopAdsDTO->getDailyQuota())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required daily quota');
            return $responseDTO;
        }
        if (is_null($shopAdsDTO->getTotalQuota())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required total quota');
            return $responseDTO;
        }

        // Build shopInfo
        $campaign['status'] = $shopAdsDTO->getStatus();
        $campaign['start_time'] = $shopAdsDTO->getTimeLineFrom();
        $campaign['end_time'] = $shopAdsDTO->getTimeLineTo();
        $campaign['daily_quota'] = 0;
        $campaign['total_quota'] = 0;
        $budgetDaily = $shopAdsDTO->getDailyQuota();
        $budgetTotal = $shopAdsDTO->getTotalQuota();
        if ($budgetDaily == 0 && $budgetTotal == 0) {
            $campaign['daily_quota'] = 0;
            $campaign['total_quota'] = 0;
        } elseif ($budgetDaily != 0 && $budgetTotal != 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Can not update both daily and total');
            return $responseDTO;
        } elseif ($budgetDaily != 0) {
            $campaign['daily_quota'] = $budgetDaily;
        } elseif ($budgetTotal != 0) {
            $campaign['total_quota'] = $budgetTotal;
        }

        // Build list keywords
        foreach ($arrayShopAdsKeywordDTO as $shopAdsKeywordDTO) {

            // Verify data
            if (!($shopAdsKeywordDTO instanceof DTOShopAdsKeyword)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Not is a DTOShopAdsKeyword Instance');
                return $responseDTO;
            }
            if (is_null($shopAdsKeywordDTO->getStatus())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required status');
                return $responseDTO;
            }
            if (is_null($shopAdsKeywordDTO->getMatchType())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required match type');
                return $responseDTO;
            }
            if (is_null($shopAdsKeywordDTO->getBiddingPrice())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required bidding price');
                return $responseDTO;
            }
            if (is_null($shopAdsKeywordDTO->getKeywordName())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required keyword name');
                return $responseDTO;
            }

            $listKeyword[] = [
                'status' => $shopAdsKeywordDTO->getStatus(),
                'match_type' => $shopAdsKeywordDTO->getMatchType(),
                'price' => $shopAdsKeywordDTO->getBiddingPrice(),
                'keyword' => $shopAdsKeywordDTO->getKeywordName(),
                'algorithm' => '',
            ];
        }

        // Build request params to POST to channel
        $params = [
            'ads_audit_event' => null,
            'campaign_ads_list' => [
                0 => [
                    'advertisements' => [
                        0 => [
                            'status' => 1,
                            'placement' => 3,
                            'adsid' => null,
                            'campaignid' => null,
                            'extinfo' => [
                                'keywords' => $listKeyword
                            ],
                        ]
                    ],
                    'campaign' => $campaign,
                    #'history_balance' => 0
                ]
            ]
        ];

        $params = json_encode($params);

        // Send request update to channel
        $randomString = Library\Common::randomString(32);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/campaign/?SPC_CDS={$randomString}&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_COOKIE, "SPC_CDS=$randomString");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Content-Type: application/json;charset=utf-8';
        $headers[] = 'Sc-Fe-Ver: v191219-fulfillment';
        #$headers[] = 'Origin: ' . $this->shopeeSellerDomain();
        $headers[] = 'Connection: keep-alive';
        #$headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/portal/marketing/pas/assembly';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Error: ' . curl_error($ch));
            return $responseDTO;
        }
        $result = json_decode($result, true);
        curl_close($ch);

        // Handle channel response
        if (!$result)
        {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel with null value or error (POST)');
            return $responseDTO;
        }

        // Check success
        if ($result['err_code'] != 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setData($result);
            return $responseDTO;
        } else {
            if ($result['data']['res_list'][0]['err_code'] != 0) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setData($result);
                return $responseDTO;
            }
        }

        $responseDTO->setIsSuccess(true);
        $responseDTO->setData($result);
        return $responseDTO;
    }

//    private function _logSimulation($requestURL, $method, $params, $response)
//    {
//        $path = SYS_PATH . '/public/';
//        $log_filename = "log";
//        $log_msg = '';
//        if (!file_exists($path . $log_filename)) {
//            // create directory/folder uploads.
//            mkdir($path . $log_filename, 0777, true);
//        }
//        $log_msg .= 'Channel response at: ' . date('d-m-Y H:i:s') . PHP_EOL;
//        $log_msg .= 'Request: ' . $requestURL . PHP_EOL;
//        $log_msg .= 'Method: ' . $method . PHP_EOL;
//        $log_msg .= 'Params: ' . $params . PHP_EOL;
//        $log_msg .= 'Response: ' . $response . PHP_EOL;
//        $log_file_data = $path . $log_filename . '/' . $log_filename . '.txt';
//        // if you don't add `FILE_APPEND`, the file will be erased each time you add a log
//        file_put_contents($log_file_data, $log_msg . "************\n", FILE_APPEND);
//    }

    const CANCEL_REASON_SYSTEM_UNPAID = 100;
    const CANCEL_REASON_SYSTEM_UNDERPAID = 101;
    const CANCEL_REASON_SYSTEM_PAYMENT_REJECTED = 102;
    const CANCEL_REASON_LOGISTICS_PICKUP_FAILED = 201;
    const CANCEL_REASON_LOGISTICS_DELIVERY_FAILED = 202;
    const CANCEL_REASON_LOGISTICS_COD_REJECTED = 203;

    const PAY_CYBERSOURCE = 1;
    const PAY_BANK_TRANSFER = 2;
    const PAY_OFFLINE_PAYMENT = 3;
    const PAY_IPAY88 = 4;
    const PAY_FREE = 5;
    const PAY_COD = 6;
    const PAY_ESUN = 7;
    const PAY_BILL_PAYMENT = 8;
    const PAY_INDOMARET = 13;
    const PAY_KREDIVO = 14;
    const PAY_NICEPAY_CC = 15;
    const PAY_AIRPAY = 18;
    const PAY_SHOPEE_WALLET = 20;
    const PAY_AKULAKU = 21;
    const PAY_INSTALLMENT = 23;
    const PAY_IBANKING = 29;

    const NONE = 0;
    const OUT_OF_STOCK = 1;
    const CUSTOMER_REQUEST = 2;
    const UNDELIVERABLE_AREA = 3;
    const CANNOT_SUPPORT_COD = 4;
    const LOST_PARCEL = 5;
    const SYSTEM_UNPAID = 100;
    const SYSTEM_UNDERPAID = 101;
    const SYSTEM_PAYMENT_REJECTED = 102;
    const LOGISTICS_REQUEST_CANCELED = 200;
    const LOGISTICS_PICKUP_FAILED = 201;
    const LOGISTICS_DELIVERY_FAILED = 202;
    const LOGISTICS_COD_REJECTED = 203;
    const BACKEND_LOGISTICS_NOT_STARTED = 204;
    const BACKEND_ESCROW_TERMINATED = 300;
    const BACKEND_INACTIVE_SELLER = 301;
    const BACKEND_SELLER_DID_NOT_SHIP = 302;
    const RULE_ENGINE_AUTO_CANCEL = 400;

    /**
     * @return mixed
     */
    public function getPlacementList()
    {
        $ch = curl_init();
        $randomString = Str::random(8);
        curl_setopt($ch, CURLOPT_URL, $this->shopeeSellerDomain() . "/api/v2/pas/toggle/?SPC_CDS={$randomString}&SPC_CDS_VER=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        #curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        #curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Sc-Fe-Ver: v191129-mkt-hotfix';
        $headers[] = 'Dnt: 1';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Te: Trailers';
        $headers[] = 'Cookie: '.$this->cookieContent;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return json_decode($result, true);
    }
}