<?php

namespace App\Library\Crawler\Channel;

use App\Library\LogError;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\ProductAdsDTO;
use App\Models;
use Exception;

class Tokopedia
{
    #region Common
    /**
     * BASE_URL
     * @type string
     */
    const BASE_URL = 'https://ta.tokopedia.com';

    /**
     * BASE_URL_GQL
     */
    const BASE_URL_GQL = 'https://gql.tokopedia.com';

    /**
     * @var
     */
    private $ch;

    /**
     * @var LogError
     */
    private $logService;

    /**
     * @var
     */
    private $cookieString;

    /**
     * @var
     */
    private $tkpdUserId;

    /**
     * @var
     */
    private $shopId;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var string
     */
    private $timeZone;

    /**
     * @var TokopediaAuthentication
     */
    private $auth;

    /**
     * Tokopedia constructor.
     * @param $shopChannelId
     */
    public function __construct($shopChannelId)
    {
        if (!$shopChannelId) return;
        $shopChannelInfo = Models\ShopChannel::getById($shopChannelId);
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        $ventureInfo = Models\Venture::getById($channelInfo->fk_venture);
        $shopChannelCredential = json_decode($shopChannelInfo->shop_channel_api_credential, true);
        $username = $shopChannelCredential['username'];
        $password = $shopChannelCredential['password'];
        $ventureId = $channelInfo->fk_venture;

        $this->shopChannelId = $shopChannelId;
        $this->timeZone = $ventureInfo->venture_timezone;
        $this->auth = new TokopediaAuthentication($username, $password, $ventureId);
        $this->ch = curl_init();
        $this->logService = new LogError();
    }

    /**
     * @return int
     */
    public function getShopChannelId(): int
    {
        return $this->shopChannelId;
    }

    /**
     * @return string
     */
    public function getTimeZone(): string
    {
        return $this->timeZone;
    }

    /**
     * Tokopedia destruct.
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }

    /**
     * @return DTOResponse
     */
    public function login()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $retry = 3;
        $count = 0;
        $isSuccess = 0;
        do {
            $result = $this->auth->checkCookies();
            if ($result->getIsSuccess()) {
                $isSuccess = 1;
                break;
            }
            $this->auth->extendCookies();
            $count++;
        } while ($count <= $retry);
        if (!$isSuccess) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $result = $this->auth->getShopCookies();
        if (!$result->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($result->getMessage());
            return $responseDTO;
        }

        $responseDTO->setData($result->getData());

        return $responseDTO;
    }

    /**
     * @param $authData
     */
    public function setAuthentication($authData)
    {
        $this->shopId = $authData['shopId'];
        $this->tkpdUserId = $authData['tkpdUserId'];
        $this->cookieString = $authData['cookieString'];
    }

    /**
     * Check Hmac
     * @param $hmacData
     * @return DTOResponse
     */
    private function checkHmacData($hmacData)
    {
        $responseDTO = new DTOResponse();
        if (!$hmacData) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('HmacData null');
            return $responseDTO;
        }
        if (!isset($hmacData['hmac'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing hmac params');
            return $responseDTO;
        }
        if (!isset($hmacData['access_date'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing access_date params');
            return $responseDTO;
        }
        if (!isset($hmacData['content'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing content params');
            return $responseDTO;
        }
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * buildDataPushSlack
     * @param $requestURLOrFunctionName
     * @param $requestData
     * @param $responseData
     * @return false|string
     */
    private function buildDataPushSlack($requestURLOrFunctionName, $requestData, $responseData)
    {
        return json_encode([
            "requestURLOrFunctionName" => $requestURLOrFunctionName,
            "requestData" => $requestData,
            "responseData" => $responseData
        ]);
    }

    /**
     * verifyAndWriteLogHmac
     * @param $result
     * @param $response
     * @param $fnName
     * @param null $requestData
     * @return DTOResponse
     */
    private function verifyAndWriteLogHmac($result, $response, $fnName, $requestData = null)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!$result) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Response null or decode error');
            $this->logService->slack($this->buildDataPushSlack($fnName, $requestData ?? null, $response));
        }
        if (isset($result['errors'])) {
            $error = current($result['errors']);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($error['detail']);
            $this->logService->slack($this->buildDataPushSlack($fnName, $requestData ?? null, $result));
        }
        return $responseDTO;
    }

    /**
     * writeLogForSimulatorAction
     * @param $result
     * @param $fnName
     * @param $postData
     * @param $response
     * @return DTOResponse
     */
    private function writeLogForSimulatorAction($result, $fnName, $postData, $response)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!$result) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Response null or decode error');
            $this->logService->slack($this->buildDataPushSlack($fnName, $postData, $response));
        }
        if (isset($result['errors']) && count($result['errors']) > 0) {
            $error = current($result['errors']);
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($error['detail']);
            $this->logService->slack($this->buildDataPushSlack($fnName, $postData, $result));
        }
        return $responseDTO;
    }
    #endregion

    #region Manual Ads Flow
    /**
     * checkInputCreateGroupAds
     * @param $groupDTO GroupAdsDTO
     * @param $productDTO ProductAdsDTO[]
     * @return DTOResponse
     */
    private function checkInputCreateGroupAds($groupDTO, $productDTO)
    {
        // Response DTO
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($groupDTO instanceof GroupAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required is GroupAdsDTO instance');
            return $responseDTO;
        }

        if (!is_array($productDTO) || count($productDTO) <= 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing productDTO');
            return $responseDTO;
        }

        foreach ($productDTO as $product) {
            if (!($product instanceof ProductAdsDTO)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required is ProductAdsDTO instance');
                return $responseDTO;
            }
        }

        if (is_null($groupDTO->getName()) || trim($groupDTO->getName()) == '') {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing groupDTO name');
            return $responseDTO;
        }
        if (is_null($groupDTO->getIsUnlimitBudget())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing groupDTO budget type');
            return $responseDTO;
        }
        if (!$groupDTO->getCostPerClick()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing groupDTO max cost');
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * @return DTOResponse
     */
    private function getHMACForGetBidInfoOfGroupAds()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=POST&url=%2Fv2.1%2Fdashboard%2Fbid_info&content=&content_type=application%2Fjson&version=v2');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure!');
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * @param $productAdsDTO ProductAdsDTO[]
     * @return DTOResponse
     */
    public function getBidInfoForGroupAds($productAdsDTO)
    {
        // Response DTO
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get authenticate for header request
        $hmacData = $this->getHMACForGetBidInfoOfGroupAds();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Verify input data
        $listSKUId = [];
        foreach ($productAdsDTO as $product)
        {
            if(!($product instanceof ProductAdsDTO))
            {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Not is ProductAdsDTO instance');
                return $responseDTO;
            }
            $listSKUId[] = intval($product->getProductId());
        }

        // Build params
        $params = [
            "data" => [
                "shop_id" => $this->shopId,
                "request_type" => "product",
                "source" => "dashboard_user_add_product",
                "data_suggestions" => [
                    0 => [
                        "ids" => $listSKUId,
                        "type" => "product"
                    ]
                ]
            ]
        ];

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.1/dashboard/bid_info');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure!');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $params, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getHMACForCreateGroup
     * @return DTOResponse
     */
    private function getHMACForCreateGroup()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=POST&url=%2Fv2.2%2Fpromo%2Fgroup&content=&content_type=&version=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure!');
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * createGroupAds
     * @param $groupDTO GroupAdsDTO
     * @param $productDTO ProductAdsDTO[]
     * @return DTOResponse
     * @throws Exception
     */
    public function createGroupAds($groupDTO, $productDTO)
    {
        // Response DTO
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get authenticate for header request
        $hmacData = $this->getHMACForCreateGroup();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Verify input DTO
        $rsCheckInput = $this->checkInputCreateGroupAds($groupDTO, $productDTO);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get ListProduct on channel to verify
        $listProduct = $this->getListProductAndShopShowCase();
        if (!$listProduct->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($listProduct->getMessage());
            return $responseDTO;
        }

        $listProduct = $listProduct->getData();
        if (is_null($listProduct)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Get list product and shop showCase with null value');
            return $responseDTO;
        }

        if (count($listProduct) > 1)
            $listProduct = current($listProduct);

        if (!isset($listProduct['data'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Error or channel change params');
            return $responseDTO;
        }

        $listProduct = current($listProduct['data']);
        if (!isset($listProduct['data'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Error or channel change params');
            return $responseDTO;
        }

        $listProduct = $listProduct['data'];
        $listProduct = array_column($listProduct, 'ad_id', 'product_id');

        // Verify product want add to GroupAds and build up postData (Product-SKU only)
        $buildDataSKU = [];

        foreach ($productDTO as $product) {
            if (is_null($product->getAdsId())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Missing productDTO AdsId');
                return $responseDTO;
            }

            if (is_null($product->getProductId())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Missing productDTO ProductId');
                return $responseDTO;
            }

            if (!array_key_exists(intval($product->getProductId()), $listProduct)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Product Id Wrong');
                return $responseDTO;
            }

            if ($listProduct[intval($product->getProductId())] != intval($product->getAdsId())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Ads Id Wrong');
                return $responseDTO;
            }

            $buildDataSKU[] = [
                "ad" => [
                    "ad_id" => $product->getAdsId() . '',
                    "ad_type" => "1"
                ],
                "product_id" => $product->getProductId() . '',
                "shop_id" => $this->shopId . '',
                "source" => "dashboard_add_product", // Default by Toko
                "group_schedule" => "0", // Default (Unknow)
                "sticker_id" => "3" // Default (Unknow)
            ];
        }

        // Get suggest bid info
        $bidInfo = $this->getBidInfoForGroupAds($productDTO);
        if (!$bidInfo->getIsSuccess())
        {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($bidInfo->getMessage());
            return $responseDTO;
        }
        $suggestBidPrice = $bidInfo->getData();
        if (!isset($suggestBidPrice['data']) || !isset(current($suggestBidPrice['data'])['suggestion_bid']))
        {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel change bid info params');
            return $responseDTO;
        }
        $bidInfo = current($suggestBidPrice['data']);
        $minBid = $bidInfo['min_bid'];
        $maxBid = $bidInfo['max_bid'];
        $multiplier = $bidInfo['multiplier'];
        $suggestBidPrice = $bidInfo['suggestion_bid'];

        // If is daily budget set default value (### False is unlimit budget)
        if (!$groupDTO->getIsUnlimitBudget()) {
            $groupDTO->setDailyBudgetCost($groupDTO->getCostPerClick() * $multiplier);
        } elseif (!$groupDTO->getDailyBudgetCost()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required daily budget cost if is daily budget');
            return $responseDTO;
        }

        // Verify bid price and daily budget
        if ($groupDTO->getCostPerClick() < $minBid) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Bid price min value is ' . $minBid);
            return $responseDTO;
        }
        if ($groupDTO->getCostPerClick() > $maxBid) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Bid price max value is ' . $maxBid);
            return $responseDTO;
        }
        if ($groupDTO->getDailyBudgetCost() < $groupDTO->getCostPerClick() * $multiplier) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Daily budget min value is ' . $groupDTO->getCostPerClick() * $multiplier);
            return $responseDTO;
        }
        if ($groupDTO->getDailyBudgetCost() > GroupAdsDTO::MAX_DAILY_BUDGET_VALUE) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Daily budget max value is ' . GroupAdsDTO::MAX_DAILY_BUDGET_VALUE);
            return $responseDTO;
        }

        // Build post data
        $postData = [
            "data" => [
                "shop_id" => $this->shopId . '',
                "ads" => $buildDataSKU,
                "group_budget" => $groupDTO->getIsUnlimitBudget() . '', // 0 - Is not limit budget ; 1 - Is daily budget
                "group_name" => $groupDTO->getName(),
                "is_create_ads" => true, // Default by Toko
                "is_create_aff" => false, // Default by Toko
                "price_bid" => $groupDTO->getCostPerClick(),
                "price_daily" => $groupDTO->getDailyBudgetCost(),
                "source" => "dashboard_add_product", // Default by Toko
                "suggested_bid_value" => $suggestBidPrice,
                "group_schedule" => "0", // Default (Unknow)
                "group_total" => "1", // Default (Unknow)
                "is_suggested_bid_button" => "-11", // Default by Toko
                "previous_bid" => 0, // Default by Toko
                "sticker_id" => "3" // Default by Toko
            ]
        ];
        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.2/promo/group');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure!');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getListProductAndShopShowCase
     * @return DTOResponse
     */
    public function getListProductAndShopShowCase()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $postData = '[{"operationName":"GetListProduct","variables":{"rows":50,"shopID": %d},"query":"query GetListProduct($shopID: Int!, $start: Int, $keyword: String, $sortBy: String, $etalase: String, $rows: Int = 50) {\n  topadsGetListProduct(shopID: $shopID, keyword: $keyword, etalase: $etalase, sortBy: $sortBy, start: $start, rows: $rows) {\n    data {\n      product_id: productID\n      product_name: productName\n      product_price_num: productPriceNum\n      product_image: productImage\n      ad_id: adID\n      ad_status: adStatus\n      group_name: groupName\n      group_id: groupID\n      product_rating: productRating\n      product_review_count: productReviewCount\n      __typename\n    }\n    eof\n    errors {\n      code\n      detail\n      title\n      object {\n        type\n        text\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"},{"operationName":"ShopShowcase","variables":{"shopID": "%d","isAllowManage":false,"hideEmpty":true,"hideGroup":true},"query":"query ShopShowcase($shopID: String!, $isAllowManage: Boolean, $hideEmpty: Boolean, $hideGroup: Boolean) {\n  shopShowcasesByShopID(shopId: $shopID, hideNoCount: $hideEmpty, hideShowcaseGroup: $hideGroup, isOwner: $isAllowManage) {\n    result {\n      id\n      title: name\n      link: uri\n      __typename\n    }\n    error {\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}]';
        $postData = sprintf($postData, $this->shopId, $this->shopId);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL_GQL);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: gql.tokopedia.com';
        $headers[] = 'Accept: */*';
        $headers[] = 'X-Source: tokopedia-lite';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(self::BASE_URL_GQL, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, self::BASE_URL_GQL, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $result = current($result);

        if (!isset($result['data']) || !isset($result['data']['topadsGetListProduct']) || !isset($result['data']['topadsGetListProduct']['errors']) || count($result['data']['topadsGetListProduct']['errors']) > 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel change response');
            $this->logService->slack($this->buildDataPushSlack(self::BASE_URL_GQL, $postData, $result));
            return $responseDTO;
        }

        // Success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * checkInputCreateKeywordAds
     * @param $groupDTO GroupAdsDTO
     * @param $keywordDTO KeywordAdsDTO[]
     * @return DTOResponse
     */
    private function checkInputCreateKeywordAds($groupDTO, $keywordDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($groupDTO instanceof GroupAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required is GroupAdsDTO instance');
            return $responseDTO;
        }

        if (is_null($groupDTO->getGroupId())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required group ads id');
            return $responseDTO;
        }

        if (!is_array($keywordDTO) || count($keywordDTO) <= 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing keywordDTO');
            return $responseDTO;
        }

        foreach ($keywordDTO as $keyword) {
            if (!($keyword instanceof KeywordAdsDTO)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required is KeywordAdsDTO instance');
                return $responseDTO;
            }

            if (is_null($keyword->getKeywordName()) || trim($keyword->getKeywordName()) == '') {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Missing keyword name');
                return $responseDTO;
            }

            if (!is_int($keyword->getBidPrice()) || $keyword->getBidPrice() <= 0) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Missing bid price');
                return $responseDTO;
            }

            if (!is_string($keyword->getMatchType()) || strlen($keyword->getMatchType()) == 0) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Missing keyword match type');
                return $responseDTO;
            }
        }

        return $responseDTO;
    }

    /**
     * getHMACForCreateKeyword
     * @return DTOResponse
     */
    private function getHMACForCreateKeyword()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=POST&url=%2Fv2.1%2Fpromo%2Fkeyword&content=&content_type=application%2Fjson&version=v1');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * createKeywordAds
     * @param $groupDTO GroupAdsDTO
     * @param $keywordDTO KeywordAdsDTO[]
     * @return DTOResponse
     * @throws Exception
     */
    public function createKeywordAds($groupDTO, $keywordDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get and verify HMAC
        $hmacData = $this->getHMACForCreateKeyword();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Verify input data
        $rsCheckInput = $this->checkInputCreateKeywordAds($groupDTO, $keywordDTO);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get BidInfo of keyword
        $bidInfo = $this->getBidInfoKeyWord($groupDTO->getGroupId());
        $bidInfo = current($bidInfo->getData()['data']);

        // Check data not null
        if (is_null($bidInfo)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Please try again!');
            return $responseDTO;
        }

        foreach ($keywordDTO as $keyword)
        {
            if ($keyword->getBidPrice() < $bidInfo['min_bid']) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage("Bid must be in multiples of Rp" . $bidInfo['min_bid']);
                return $responseDTO;
            }
        }

        $postData = null;

        foreach ($keywordDTO as $keyword) {
            $data = [
                "group_id" => $groupDTO->getGroupId() . '', // Get after create group success
                "keyword_tag" => $keyword->getKeywordName(),
                "keyword_type_id" => $keyword->getMatchType(),
                "shop_id" => $this->shopId . '',
                "toggle" => "1", // Default by Toko,
                "status" => "1", // Default by Toko,
                "source" => "dashboard_user_add_keyword_positive", // Default by Toko,
                "price_bid" => $keyword->getBidPrice(),
            ];
            $postData['data'][] = $data;
        }
        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.1/promo/keyword?');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }


    /**
     * @param $groupId
     * @param $productId
     * @return DTOResponse
     */
    public function getSuggestKeyword($groupId, $productId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $postData = '[{"operationName":"GetKeywordSuggestionV2","variables":{"groupID":%d,"productID":"%d","shopID":%d},"query":"query GetKeywordSuggestionV2($productID: String!, $groupID: Int = 0, $shopID: Int!) {\n  topAdsGetKeywordSuggestionV2(product_ids: $productID, group_id: $groupID, shop_id: $shopID, type: 1) {\n    data {\n      product_id\n      keyword\n      bid_suggest\n      total_search\n      min_bid\n      competition\n      __typename\n    }\n    errors {\n      code\n      detail\n      object {\n        type\n        text\n        __typename\n      }\n      title\n      __typename\n    }\n    __typename\n  }\n}\n"}]';
        $postData = sprintf($postData, $groupId, $productId ,$this->shopId);

        curl_setopt($this->ch, CURLOPT_URL, 'https://gql.tokopedia.com/');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: gql.tokopedia.com';
        $headers[] = 'Accept: */*';
        $headers[] = 'Sec-Fetch-Dest: empty';
        $headers[] = 'X-Source: tokopedia-lite';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Origin: https://ta.tokopedia.com';
        $headers[] = 'Sec-Fetch-Site: same-site';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $result = current($result);

        if (!isset($result['data']) || !isset($result['data']['topAdsGetKeywordSuggestionV2']) || !isset($result['data']['topAdsGetKeywordSuggestionV2']['errors']) || count($result['data']['topAdsGetKeywordSuggestionV2']['errors']) > 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel change response or have error');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, $postData, $result));
            return $responseDTO;
        }

        // Success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Active/Inactivate Automatic Ads
    /**
     * checkInputActiveAutoAds
     * @param $groupAdsDTO GroupAdsDTO
     * @param $isCheckAutoAdsBudgetCost
     * @return DTOResponse
     */
    private function checkInputActiveAutoAds($groupAdsDTO, $isCheckAutoAdsBudgetCost)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($groupAdsDTO instanceof GroupAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Required is GroupAdsDTO instance");
            return $responseDTO;
        }

        if ($isCheckAutoAdsBudgetCost) {
            if (is_null($groupAdsDTO->getAutoAdsBudgetCost()) || trim($groupAdsDTO->getAutoAdsBudgetCost()) == '') {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage("Missing groupDTO AutoAdsBudgetCost");
                return $responseDTO;
            }
        }

        return $responseDTO;
    }

    /**
     * getHmacForActiveAutoAds
     * @return DTOResponse
     */
    private function getHmacForActiveAutoAds()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=POST&url=%2Fv2.1%2Fpromo%2Fauto&content=&content_type=application%2Fjson&version=v2');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * Active Automation Ads
     * @param $groupDTO GroupAdsDTO
     * @return bool|mixed|string
     * @throws Exception
     */
    public function activeAutoAds($groupDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputActiveAutoAds($groupDTO, true);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get authenticate for header request
        $hmacData = $this->getHmacForActiveAutoAds();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Params
        $postData = [
            "shop_id" => $this->shopId,
            "action" => $groupDTO->getToggleOnAdsGroup(),
            "daily_budget" => $groupDTO->getAutoAdsBudgetCost(),
            "source" => "edit_panel_autoads", // Default By Tokopedia
            "channel" => "dashboard", // Default By Tokopedia
        ];
        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.1/promo/auto');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * Inactivate Auto Ads
     * @param $groupDTO GroupAdsDTO
     * @return DTOResponse
     * @throws Exception
     */
    public function inactivateAutoAds($groupDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputActiveAutoAds($groupDTO, false);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get authenticate for header request
        $hmacData = $this->getHmacForActiveAutoAds();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $postData = [
            "shop_id" => $this->shopId,
            "action" => $groupDTO->getToggleOffAdsGroup(),
            "daily_budget" => $groupDTO->getAutoAdsBudgetCost(),
            "source" => "edit_panel_autoads",
            "channel" => "dashboard",
        ];
        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.1/promo/auto');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Add Sku To Ad Group
    /**
     * checkInputAddSKU
     * @param $groupAdsDTO GroupAdsDTO
     * @param $arrProductAdsDTO ProductAdsDTO[]
     * @return DTOResponse
     */
    private function checkInputAddSKU($groupAdsDTO, $arrProductAdsDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($groupAdsDTO instanceof GroupAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Required is GroupAdsDTO instance");
            return $responseDTO;
        }

        if (empty($groupAdsDTO->getGroupId())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Missing group id");
            return $responseDTO;
        }

        if (!is_array($arrProductAdsDTO) || count($arrProductAdsDTO) <= 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Missing productAdsDTO");
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * getHMACForAddSkuToAdsGroup
     * @return DTOResponse
     */
    private function getHMACForAddSkuToAdsGroup()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=POST&url=%2Fv2.2%2Fpromo&content=&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * addSkuToAdGroup
     * @param $groupAdsDTO GroupAdsDTO
     * @param $arrProductAdsDTO ProductAdsDTO[]
     * @return DTOResponse
     * @throws Exception
     */
    public function addSkuToAdsGroup($groupAdsDTO, $arrProductAdsDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputAddSKU($groupAdsDTO, $arrProductAdsDTO);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get authenticate for header request
        $hmacData = $this->getHMACForAddSkuToAdsGroup();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Get bid price info
        $result = $this->getBidInfo($arrProductAdsDTO);
        $bidInfo = current($result->getData()['data']);

        $arrayAd = [];
        foreach ($arrProductAdsDTO as $productAdsDTO) {
            if (!($productAdsDTO instanceof ProductAdsDTO)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage("Required is ProductAdsDTO instance");
                return $responseDTO;
            }

            if ($productAdsDTO->getAdsId() != 0) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage("Only allow add product not advertised");
                return $responseDTO;
            }

            $ad = [
                'ad' => [
                    'ad_id' => strval($productAdsDTO->getAdsId()),
                    'ad_type' => strval(1),
                    'price_bid' => $bidInfo['suggestion_bid'],
                    'suggested_bid_value' => $bidInfo['suggestion_bid']
                ],
                'product_id' => strval($productAdsDTO->getProductId()),
                'group_id' => strval($groupAdsDTO->getGroupId()),
                'shop_id' => strval($this->shopId),
                'source' => 'dashboard_add_product',
                'group_schedule' => strval(0),
                'sticker_id' => strval(3)
            ];

            array_push($arrayAd, $ad);
        }

        // Param
        $postData = null;
        $postData['data'] = $arrayAd;
        $postData['is_create_ads'] = true;
        $postData['is_create_aff'] = false;
        $postData['shop_id'] = strval($this->shopId);
        $postData = json_encode($postData);

        // cUrl
        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.2/promo');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getHMACForGetBidInfo
     * @return DTOResponse
     */
    private function getHMACForGetBidInfo()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=POST&url=%2Fv2.1%2Fdashboard%2Fbid_info&content=&content_type=application%2Fjson&version=v2');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getBidInfo
     * @param $arrProductAdsDTO
     * @return DTOResponse
     * @throws Exception
     */
    public function getBidInfo($arrProductAdsDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get authenticate for header request
        $hmacData = $this->getHMACForGetBidInfo();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Verify input data
        if (!is_array($arrProductAdsDTO) || count($arrProductAdsDTO) <= 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Missing productAdsDTO");
            return $responseDTO;
        }

        $ids = [];
        foreach ($arrProductAdsDTO as $productAdsDTO) {
            if (!($productAdsDTO instanceof ProductAdsDTO)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage("Required is ProductAdsDTO instance");
                return $responseDTO;
            }

            if (empty($productAdsDTO->getProductId())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage("Missing product id");
                return $responseDTO;
            }

            array_push($ids, intval($productAdsDTO->getProductId()));
        }

        $postData = null;
        $data = [
            'shop_id' => $this->shopId,
            'source' => 'dashboard_user_add_product', // Default by Tokopedia
            'request_type' => 'product', // Default by Tokopedia
            'data_suggestions' => [
                [
                    'ids' => $ids,
                    'type' => 'product' // Default by Tokopedia
                ]
            ]
        ];
        $postData['data'] = $data;
        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.1/dashboard/bid_info');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Method: POST';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Edit Ads Group Information
    /**
     * Verify And Write Log EditAdsGroupInformation
     * @param $groupAdsDTO GroupAdsDTO
     * @return DTOResponse
     */
    private function checkInputEditAdsGroup($groupAdsDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($groupAdsDTO instanceof GroupAdsDTO)){
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required is GroupAdsDTO instance');
            return $responseDTO;
        }

        if (is_null($groupAdsDTO->getGroupId()) || trim($groupAdsDTO->getGroupId()) == ''){
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing groupDTO id');
            return $responseDTO;
        }

        if (is_null($groupAdsDTO->getName()) || trim($groupAdsDTO->getName()) == ''){
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing groupDTO name');
            return $responseDTO;
        }

        if (is_null($groupAdsDTO->getCostPerClick()) || trim($groupAdsDTO->getCostPerClick()) == ''){
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing max cost or max cost is not be in multiple of Rp50');
            return $responseDTO;
        }

        if (is_null($groupAdsDTO->getIsUnlimitBudget())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing groupDTO BudgetType');
            return $responseDTO;
        }

        if (is_null($groupAdsDTO->getDailyBudgetCost()) || trim($groupAdsDTO->getDailyBudgetCost()) == ''){
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing groupDTO DailyBudgetCost');
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * getHmacInfoAdsGroupBeforeEdit
     * @param $groupId
     * @return DTOResponse
     */
    private function getHmacInfoAdsGroupBeforeEdit($groupId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=GET&url=%2Fv2%2Fpromo%2Fgroup&content=group_id%3D' . $groupId . '%26shop_id%3D' . $this->shopId . '&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * @param $groupId
     * @return DTOResponse
     */
    private function getInfoAdsGroupBeforeEdit($groupId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get authenticate for header request
        $hmacData = $this->getHmacInfoAdsGroupBeforeEdit($groupId);
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2/promo/group?group_id=' . $groupId . '&shop_id=' . $this->shopId);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: id-ID';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: GET';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);


        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * @return DTOResponse
     */
    private function getHmacBidInfoForGroupAdsByGroup()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, 'https://ta.tokopedia.com/v1/dashboard/hmac?method=POST&url=%2Fv2.1%2Fdashboard%2Fbid_info&content=&content_type=application%2Fjson&version=v2');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'Sec-Fetch-Dest: empty';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * @param $groupAdsId string
     * @return DTOResponse
     */
    public function getBidInfoForGroupAdsByGroup($groupAdsId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get authenticate for header request
        $hmacData = $this->getHmacBidInfoForGroupAdsByGroup();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $prams = [
            "data" => [
                "request_type" => "group",
                "shop_id" => $this->shopId,
                "data_suggestions" => [
                    0 => [
                        "ids" => [
                            0 => intval($groupAdsId)
                        ],
                        "type" => "group"
                    ]
                ],
                "source" => "du_group_product"
            ]
        ];

        curl_setopt($this->ch, CURLOPT_URL, 'https://ta.tokopedia.com/v2.1/dashboard/bid_info');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($prams));
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'Sec-Fetch-Dest: empty';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * Get Hmac For Edit Ads Group Information
     * @return DTOResponse
     */
    private function getHmacForEditAdsGroup()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=PATCH&url=%2Fv2%2Fpromo%2Fgroup&content=&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * Edit Ads Group Information
     * @param $groupDTO GroupAdsDTO
     * @return mixed
     * @throws Exception
     */
    public function editAdsGroup($groupDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputEditAdsGroup($groupDTO);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get Info Ads Group Before Edit
        $result = $this->getInfoAdsGroupBeforeEdit($groupDTO->getGroupId());
        if (!isset($result->getData()['data'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Channel change params");
            return $responseDTO;
        }
        $infoAdsGroupBeforeEdit = $result->getData()['data'];

        // Verify Data
        if (is_null($infoAdsGroupBeforeEdit)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Group Id not found");
            return $responseDTO;
        }

        // Get and check bid info
        $bidInfo = $this->getBidInfoForGroupAdsByGroup($groupDTO->getGroupId());
        if (!$bidInfo->getIsSuccess())
        {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($bidInfo->getMessage());
            return $responseDTO;
        }
        $suggestBidPrice = $bidInfo->getData();
        if (!isset($suggestBidPrice['data']) || !isset(current($suggestBidPrice['data'])['suggestion_bid']))
        {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel change bid info params');
            return $responseDTO;
        }
        $bidInfo = current($suggestBidPrice['data']);
        $minBid = $bidInfo['min_bid'];
        $maxBid = $bidInfo['max_bid'];
        $multiplier = $bidInfo['multiplier'];

        // Verify bid price and daily budget
        if ($groupDTO->getCostPerClick() < $minBid) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Bid price min value is ' . $minBid);
            return $responseDTO;
        }
        if ($groupDTO->getCostPerClick() > $maxBid) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Bid price max value is ' . $maxBid);
            return $responseDTO;
        }
        if ($groupDTO->getIsUnlimitBudget()) {
            if ($groupDTO->getDailyBudgetCost() < $groupDTO->getCostPerClick() * $multiplier) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Daily budget min value is ' . $groupDTO->getCostPerClick() * $multiplier);
                return $responseDTO;
            }
            if ($groupDTO->getDailyBudgetCost() > GroupAdsDTO::MAX_DAILY_BUDGET_VALUE) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Daily budget max value is ' . GroupAdsDTO::MAX_DAILY_BUDGET_VALUE);
                return $responseDTO;
            }
        } else {
            $groupDTO->setDailyBudgetCost($groupDTO->getCostPerClick() * $multiplier);
        }

        // Set Info Ads Group To DTO
        $this->settingValueForAdsGroup($groupDTO, $infoAdsGroupBeforeEdit);

        // Get authenticate for header request
        $hmacData = $this->getHmacForEditAdsGroup();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $startDate = date('d/m/Y');
        $endDate = date('d/m/Y', strtotime("+1 month"));
        $time = date('h:i A', strtotime("+30 minutes"));

        $postData = [
            "data" => [
                "group_id" => $groupDTO->getGroupId(),
                "group_name" => $groupDTO->getName(),
                "group_type" => $groupDTO->getGroupType(),
                "shop_id" => (string)$this->shopId,
                "price_bid" => $groupDTO->getCostPerClick(),
                "group_budget" => intval($groupDTO->getIsUnlimitBudget()) . '', // 0 - Is not limit budget ; 1 - Is daily budget
                "price_daily" => $groupDTO->getDailyBudgetCost(),
                "group_schedule" => $groupDTO->getGroupSchedule(), // Get Default Value
                "group_start_date" => $startDate,
                "group_start_time" => $time,
                "group_end_date" => $endDate,
                "group_end_time" => $time,
                "sticker_id" => $groupDTO->getStickerId(),
                "source" => $groupDTO->getSourceGroupAds(),
                "toggle" => $groupDTO->getStatusAdsGroup(),
                "suggested_bid_value" => $groupDTO->getSuggestedBidValue(),
                "is_suggestion_bid_button" => $groupDTO->getIsSuggestionBidButton(), // Default Value
                "old_price_bid" => $groupDTO->getOldPriceBid()
            ]
        ];

        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2/promo/group');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PATCH');

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: PATCH';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * settingValueForAdsGroup
     * @param $groupAdsDTO GroupAdsDTO
     * @param $data
     */
    private function settingValueForAdsGroup($groupAdsDTO, $data)
    {
        if (isset($data['group_type'])) {
            $groupAdsDTO->setGroupType($data['group_type']);
        }

        if (isset($data['sticker_id'])) {
            $groupAdsDTO->setStickerId($data['sticker_id']);
        }

        if (isset($data['source'])) {
            $groupAdsDTO->setSourceGroupAds($data['source']);
        }

        if (isset($data['suggested_bid_value'])) {
            $groupAdsDTO->setSuggestedBidValue($data['suggested_bid_value']);
        }

        if (isset($data['price_bid'])) {
            $groupAdsDTO->setOldPriceBid($data['price_bid']);
        }

        if (isset($data['status'])) {
            $stt = $data['status'] == "1" || $data['status'] == "2"
                ? $groupAdsDTO->getStatusActive() : $groupAdsDTO->getStatusDeactivate();
            $groupAdsDTO->setStatusAdsGroup($stt);
        }
    }
    #endregion

    #region Crawl Group Info
    /**
     * getHmacAdsGroupInformationn
     * @param $startDate
     * @param $endDate
     * @param $page
     * @return DTOResponse
     */
    private function getHmacAdsGroupInformation($startDate, $endDate, $page)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $data = json_encode([
            'shopId' => $this->shopId,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'page' => $page
        ]);
        $url = self::BASE_URL . '/v1/dashboard/hmac?method=GET&url=%2Fv1.1%2Fdashboard%2Fgroups&content=page=' . $page . '%26start_date%3D' . $startDate . '%26end_date%3D' . $endDate . '%26separate_statistic%3Dtrue%26group_type%3D1%26shop_id%3D' . $this->shopId . '&content_type=';

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__, $data);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getAdsGroupInformation
     * @param int $startDate
     * @param int $endDate
     * @param $page
     * @return DTOResponse
     * @throws Exception
     */
    public function getAdsGroupInformation($startDate, $endDate)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!is_int($startDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required $startDate');
            return $responseDTO;
        }

        if (!is_int($endDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required $endDate');
            return $responseDTO;
        }

        $startDate = date('Y-m-d', $startDate);
        $endDate = date('Y-m-d', $endDate);
        $page = 1;
        $count = 0;
        $totalGroupAds = [];

        do {
            // Get authenticate for header request
            $hmacData = $this->getHmacAdsGroupInformation($startDate, $endDate, $page);
            if (!$hmacData->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($hmacData->getMessage());
                return $responseDTO;
            }

            // Hmac Data
            $hmacData = $hmacData->getData();

            // Verify HMAC
            $isSuccess = $this->checkHmacData($hmacData);
            if (!$isSuccess->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($isSuccess->getMessage());
                return $responseDTO;
            }

            $url = self::BASE_URL . '/v1.1/dashboard/groups?page=' . $page . '&start_date=' . $startDate . '&end_date=' . $endDate . '&separate_statistic=true&group_type=1&shop_id=' . $this->shopId;
            $data = json_encode([
                'shopId' => $this->shopId,
                'startDate' => $startDate,
                'endDate' => $endDate
            ]);

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

            curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'Authority: ta.tokopedia.com';
            $headers[] = 'Content-Md5: ' . $hmacData['content'];
            $headers[] = 'Accept-Language: en-US';
            $headers[] = 'Authorization: ' . $hmacData['hmac'];
            $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
            $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
            $headers[] = 'X-Requested-With: XMLHttpRequest';
            $headers[] = 'X-Date: ' . $hmacData['access_date'];
            $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
            $headers[] = 'X-Device: desktop';
            $headers[] = 'X-Method: GET';
            $headers[] = 'Accept-Encoding: gzip, deflate, br';
            $headers[] = 'Cookie: ' . $this->cookieString;
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($this->ch);
            $result = json_decode($response, true);

            if (curl_errno($this->ch)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('CURL Failure');
                $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
                return $responseDTO;
            }

            // Push error to slack
            $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $data, $response);

            // Response simulation to channel failure
            if (!$isSuccess->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($isSuccess->getMessage());
                return $responseDTO;
            }

            $data = $result['data'];
            $count += count($data);
            $total = $result['page']['total'] ?? 0;
            $totalGroupAds = array_merge($totalGroupAds, $data);
            $page++;

        } while ($count < $total);

        // Response success
        $responseDTO->setData($totalGroupAds);
        return $responseDTO;
    }
    #endregion

    #region Crawl Product Info Of Group Ads
    /**
     * checkInputGetProductInfo
     * @param $startDate
     * @param $endDate
     * @param $page
     * @return DTOResponse
     */
    private function checkInputGetProductInfo($startDate, $endDate)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!intval($startDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required $startDate and is int value');
            return $responseDTO;
        }

        if (!intval($endDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required $endDate and is int value');
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * getHMACForGetProductInfo
     * @param $startDate
     * @param $endDate
     * @param $page
     * @return DTOResponse
     */
    private function getHMACForGetProductInfo($startDate, $endDate, $page)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $data = json_encode([
            'shopId' => $this->shopId,
            'page' => $page,
            'startDate' => $startDate,
            'endDate' => $endDate
        ]);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=GET&url=%2Fv1.1%2Fdashboard%2Fgroup_products&content=src%3Dgroup%26page%3D' . $page . '%26start_date%3D' . $startDate . '%26end_date%3D' . $endDate . '%26separate_statistic%3Dtrue%26shop_id%3D' . $this->shopId . '%26type%3Dproduct&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__, $data);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getProductInfoOfGroupAds
     * @param $groupAdsDTO
     * @param $startDate
     * @param $endDate
     * @return DTOResponse
     * @throws Exception
     */
    public function getProductInfoOfGroupAds($startDate, $endDate)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputGetProductInfo($startDate, $endDate);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        $startDate = date('Y-m-d', $startDate);
        $endDate = date('Y-m-d', $endDate);
        $page = 1;
        $count = 0;
        $totalProduct = [];

        do {
            // Get authenticate for header request
            $hmacData = $this->getHMACForGetProductInfo($startDate, $endDate, $page);
            if (!$hmacData->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($hmacData->getMessage());
                return $responseDTO;
            }

            // Hmac Data
            $hmacData = $hmacData->getData();

            // Verify HMAC
            $isSuccess = $this->checkHmacData($hmacData);
            if (!$isSuccess->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($isSuccess->getMessage());
                return $responseDTO;
            }

            $url = self::BASE_URL . '/v1.1/dashboard/group_products?src=group&page=' . $page . '&start_date=' . $startDate . '&end_date=' . $endDate . '&separate_statistic=true&shop_id=' . $this->shopId . '&type=product';
            $data = json_encode([
                'shopId' => $this->shopId,
                'page' => $page,
                'startDate' => $startDate,
                'endDate' => $endDate
            ]);

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

            curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'Authority: ta.tokopedia.com';
            $headers[] = 'Accept-Language: en-US';
            $headers[] = 'Content-Md5: ' . $hmacData['content'];
            $headers[] = 'Accept-Language: en-US';
            $headers[] = 'Authorization: ' . $hmacData['hmac'];
            $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
            $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
            $headers[] = 'X-Requested-With: XMLHttpRequest';
            $headers[] = 'X-Date: ' . $hmacData['access_date'];
            $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
            $headers[] = 'X-Device: desktop';
            $headers[] = 'X-Method: GET';
            $headers[] = 'Accept-Encoding: gzip, deflate, br';
            $headers[] = 'Cookie: ' . $this->cookieString;
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($this->ch);
            $result = json_decode($response, true);

            if (curl_errno($this->ch)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('CURL Failure');
                $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
                return $responseDTO;
            }

            // Push error to slack
            $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $data, $response);

            // Response simulation to channel failure
            if (!$isSuccess->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($isSuccess->getMessage());
                return $responseDTO;
            }

            $data = $result['data'];
            $count += count($data);
            $total = $result['page']['total'] ?? 0;
            $totalProduct = array_merge($totalProduct, $data);
            $page++;

        } while ($count < $total);

        // Response success
        $responseDTO->setData($totalProduct);
        return $responseDTO;
    }
    #endregion

    #region Keywords Performance
    /**
     * getHMACForKeywordsPerformance
     * @param $startDate
     * @param $endDate
     * @param $page
     * @return DTOResponse
     */
    private function getHMACForKeywordsPerformance($startDate, $endDate, $page)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        if (!is_int($startDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Start date must be convert to integer with format date is Y-m-d');
            return $responseDTO;
        }

        if (!is_int($endDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('End date must be convert to integer with format date is Y-m-d');
            return $responseDTO;
        }

        if (!is_int($page)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Page number must be integer');
            return $responseDTO;
        }

        // Convert to string date
        $startDate = date('Y-m-d', $startDate);
        $endDate = date('Y-m-d', $endDate);

        $urlPrefix = urlencode('/v1.1/dashboard/keywords');
        $urlParamContent = '&content=';
        $urlParams = 'page=' . $page . '&start_date=' . $startDate . '&end_date=' . $endDate . '&is_positive=1&separate_statistic=true&shop_id=' . $this->shopId;
        $url = self::BASE_URL . '/v1/dashboard/hmac?method=GET&url=' . $urlPrefix . $urlParamContent . urlencode($urlParams) . '&content_type=';

        curl_setopt($this->ch, CURLOPT_URL, $url);

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getKeywordsPerformance
     * @param $startDate
     * @param $endDate
     * @return DTOResponse
     * @throws Exception
     */
    public function getKeywordsPerformance($startDate, $endDate)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        if (!is_int($startDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Start date must be convert to integer with format date is Y-m-d');
            return $responseDTO;
        }

        if (!is_int($endDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('End date must be convert to integer with format date is Y-m-d');
            return $responseDTO;
        }

        $page = 1;
        $count = 0;
        $totalKeywords = [];
        do {
            // Get authenticate for header request
            $hmacData = $this->getHMACForKeywordsPerformance($startDate, $endDate, $page);
            if (!$hmacData->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($hmacData->getMessage());
                return $responseDTO;
            }

            // Hmac Data
            $hmacData = $hmacData->getData();

            // Verify HMAC
            $isSuccess = $this->checkHmacData($hmacData);
            if (!$isSuccess->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($isSuccess->getMessage());
                return $responseDTO;
            }

            // Convert to string date
            $startDateFormatted = date('Y-m-d', $startDate);
            $endDateFormatted = date('Y-m-d', $endDate);

            $url = self::BASE_URL . '/v1.1/dashboard/keywords?page=' . $page . '&start_date=' . $startDateFormatted . '&end_date=' . $endDateFormatted . '&is_positive=1&separate_statistic=true&shop_id=' . $this->shopId;

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0';
            $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
            $headers[] = 'Accept-Language: en-US';
            $headers[] = 'Content-Md5: ' . $hmacData['content'];
            $headers[] = 'Authorization: ' . $hmacData['hmac'];
            $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
            $headers[] = 'X-Date: ' . $hmacData['access_date'];
            $headers[] = 'X-Device: desktop';
            $headers[] = 'X-Method: GET';
            $headers[] = 'X-Requested-With: XMLHttpRequest';
            $headers[] = 'Cookie: ' . $this->cookieString;
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($this->ch);
            $result = json_decode($response, true);

            if (curl_errno($this->ch)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('CURL Failure');
                $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
                return $responseDTO;
            }

            // Push error to slack
            $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

            // Response simulation to channel failure
            if (!$isSuccess->getIsSuccess()) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage($isSuccess->getMessage());
                return $responseDTO;
            }

            $data = $result['data'];
            $count += count($data);
            $total = $result['page']['total'] ?? 0;
            $totalKeywords = array_merge($totalKeywords, $data);
            $page++;
        } while ($count < $total);

        // Response success
        $responseDTO->setData($totalKeywords);
        return $responseDTO;
    }
    #endregion

    #region Active/Deactivate Ads Group
    /**
     * checkInputForActiveAdsGroup
     * @param $groupAdsDTO GroupAdsDTO
     * @return DTOResponse
     */
    private function checkInputForActiveAdsGroup($groupAdsDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($groupAdsDTO instanceof GroupAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required is GroupAdsDTO instance');
            return $responseDTO;
        }

        if (is_null($groupAdsDTO->getGroupId()) || trim($groupAdsDTO->getGroupId()) == '') {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing GroupAdsDTO id');
            return $responseDTO;
        }

        if (is_null($groupAdsDTO->getToggleAdsGroup()) || !is_bool($groupAdsDTO->getToggleAdsGroup())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Missing GroupAdsDTO getToggleAdsGroup\nOr param must be type bool");
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * getHmacForActiveAdsGroup
     * @return mixed
     */
    private function getHmacForActiveAdsGroup()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=PATCH&url=%2Fv2%2Fpromo%2Fgroup%2Fbulk&content=&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * ActiveAndDeactivateAdsGroup
     * @param $groupDTO GroupAdsDTO
     * @return mixed
     * @throws Exception
     */
    public function activeAndDeactivateAdsGroup($groupDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputForActiveAdsGroup($groupDTO);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get authenticate for header request
        $hmacData = $this->getHmacForActiveAdsGroup();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $toggle = $groupDTO->getToggleOnAdsGroup();

        if (!$groupDTO->getToggleAdsGroup()) {
            $toggle = $groupDTO->getToggleOffAdsGroup();
        }

        $postData = [
            "data" => [
                "action" => $toggle,
                "groups" => [[
                    "group_id" => (string)$groupDTO->getGroupId()
                ]],
                "shop_id" => (string)$this->shopId
            ]
        ];
        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2/promo/group/bulk');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PATCH');

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: id-ID';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: PATCH';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Active/Deactivate SKU In Ads Group
    /**
     * checkInputForActiveSKU
     * @param $productAdsDTO ProductAdsDTO
     * @return DTOResponse
     */
    private function checkInputForActiveSKU($productAdsDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($productAdsDTO instanceof ProductAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required is ProductAdsDTO instance');
            return $responseDTO;
        }

        if (is_null($productAdsDTO->getAdsId()) || trim($productAdsDTO->getAdsId()) == '') {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing getAdsId id');
            return $responseDTO;
        }

        if (is_null($productAdsDTO->getToggleSKU()) || !is_bool($productAdsDTO->getToggleSKU())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Missing ProductAdsDTO getToggleSKU\nOr param must be type bool");
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * getHmacForActiveSKU
     * @return mixed
     */
    private function getHmacForActiveSKU()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=PATCH&url=%2Fv2%2Fpromo%2Fbulk&content=&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * activeOrDeactivateSKU
     * @param $productDTO ProductAdsDTO
     * @return mixed
     * @throws Exception
     */
    public function activeOrDeactivateSKU($productDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputForActiveSKU($productDTO);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get authenticate for header request
        $hmacData = $this->getHmacForActiveSKU();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $toggle = $productDTO->getToggleOnSKU();

        if (!$productDTO->getToggleSKU()) {
            $toggle = $productDTO->getToggleOffSKU();
        }

        $postData = [
            "data" => [
                "action" => $toggle,
                "shop_id" => (string)$this->shopId,
                "ads" => [[
                    "ad_id" => (string)$productDTO->getAdsId()
                ]]
            ]
        ];
        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2/promo/bulk');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PATCH');

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: PATCH';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Active/Deactivate Keyword In Ads Group
    /**
     * checkInputForActiveKeywordInAdsGroup
     * @param $keywordDTO KeywordAdsDTO
     * @return DTOResponse
     */
    private function checkInputForActiveKeyword($keywordDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($keywordDTO instanceof KeywordAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required is KeywordAdsDTO instance');
            return $responseDTO;
        }

        if (is_null($keywordDTO->getKeywordId()) || trim($keywordDTO->getKeywordId()) == '') {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing getKeywordId id');
            return $responseDTO;
        }

        if (is_null($keywordDTO->getToggleKeyword()) || !is_bool($keywordDTO->getToggleKeyword())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Missing KeywordAdsDTO getToggleKeyword\nOr param must be type bool");
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * getHmacForActiveKeyword
     * @return DTOResponse
     */
    private function getHmacForActiveKeyword()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=PATCH&url=%2Fv2%2Fpromo%2Fkeyword%2Fbulk&content=&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * activeOrDeactivateKeyword
     * @param $keywordDTO KeywordAdsDTO
     * @return DTOResponse
     * @throws Exception
     */
    public function activeOrDeactivateKeyword($keywordDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputForActiveKeyword($keywordDTO);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get authenticate for header request
        $hmacData = $this->getHmacForActiveKeyword();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $toggle = $keywordDTO->getToggleOnKeyword();

        if (!$keywordDTO->getToggleKeyword()) {
            $toggle = $keywordDTO->getToggleOffKeyword();
        }

        $postData = [
            "data" => [
                "action" => $toggle,
                "shop_id" => (string)$this->shopId,
                "keywords" => [[
                    "keyword_id" => (string)$keywordDTO->getKeywordId()
                ]]
            ]
        ];
        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2/promo/keyword/bulk');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PATCH');

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: PATCH';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Crawl Suggest Keyword For Product / SKU
    /**
     * getHMACForGetSuggestKeyword
     * @param $listStringProductId
     * @return DTOResponse
     */
    private function getHMACForGetSuggestKeyword($listStringProductId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=GET&url=%2Fv2.2%2Fkeyword_suggest&content=product_ids%3D' . $listStringProductId . '&content_type=&version=v1');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getSuggestKeywordForProduct
     * @param ProductAdsDTO[] $arrayProductAdsDTO
     * @return DTOResponse
     * @throws Exception
     */
    public function getSuggestKeywordForProduct($arrayProductAdsDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $listProductId = [];

        if (!$arrayProductAdsDTO || !is_array($arrayProductAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required $arrayProductAdsDTO');
            return $responseDTO;
        }

        foreach ($arrayProductAdsDTO as $productAdsDTO) {
            if (!($productAdsDTO instanceof ProductAdsDTO)) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required is ProductAdsDTO instance');
                return $responseDTO;
            }

            if (is_null($productAdsDTO->getProductId())) {
                $responseDTO->setIsSuccess(false);
                $responseDTO->setMessage('Required product id');
                return $responseDTO;
            }

            $listProductId[] = $productAdsDTO->getProductId();
        }

        $listStringProductId = implode('%2C', $listProductId);


        // Get authenticate for header request
        $hmacData = $this->getHMACForGetSuggestKeyword($listStringProductId);
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.2/keyword_suggest?product_ids=' . $listStringProductId);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: GET';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $listStringProductId, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Edit Keyword Information
    /**
     * checkInputEditKeywordInformation
     * @param $keywordAdsDTO KeywordAdsDTO
     * @return DTOResponse
     * @throws Exception
     */
    private function checkInputEditKeywordInformation($keywordAdsDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!($keywordAdsDTO instanceof KeywordAdsDTO)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required is KeywordAdsDTO instance');
            return $responseDTO;
        }

        if (is_null($keywordAdsDTO->getKeywordId()) || trim($keywordAdsDTO->getKeywordId()) == '') {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing KeywordAdsDTO id');
            return $responseDTO;
        }

        if (is_null($keywordAdsDTO->getMatchType())) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing KeywordAdsDTO match type');
            return $responseDTO;
        }

        if (is_null($keywordAdsDTO->getBidPrice()) || trim($keywordAdsDTO->getBidPrice()) == '') {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Missing KeywordAdsDTO bid price');
            return $responseDTO;
        }

        return $responseDTO;
    }

    /**
     * checkMinBidPrice
     * @param $keywordDTO KeywordAdsDTO
     * @param $bidInfo
     * @return DTOResponse
     */
    private function checkMinBidPrice($keywordDTO, $bidInfo)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        foreach ($bidInfo as &$value)
        {
            if ($value['id'] == $keywordDTO->getKeywordGroupId() && $value['min_bid'] > $keywordDTO->getBidPrice())
            {
                {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage("Bid must be in multiples of Rp" . $value['min_bid']);
                }
            }
        }

        return $responseDTO;
    }

    /**
     * checkTypeKeyword
     * Max count of keyword group equal two.
     * Equivalent with two type keyword: General Search & Specific Search
     * When keyword group count equal one and type input not match with present keyword type
     * New keyword will be create with bellow info:
     *  Id: automatic create.
     *  Name: same with name of present keyword.
     *  Group: present group.
     *  Bid price: input from user.
     *  Type: input from user.
     * @param $keywordDTO KeywordAdsDTO
     * @param $groupKeyword
     * @return DTOResponse
     */
    private function checkTypeKeyword($keywordDTO, $groupKeyword)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $groupCount = count($groupKeyword);
        $typeName = $keywordDTO->getKeywordTypeId() == $keywordDTO->getKeywordGeneralSearchId()
            ? $keywordDTO->getKeywordGeneralSearchName() : $keywordDTO->getKeywordSpecificSearchName();

        if ($groupCount == 1) {
            return $responseDTO;
        }

        if ($groupCount < 1) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Keyword not found');
        } else {
            foreach ($groupKeyword as &$data)
            {
                if ($data["keyword_id"] == $keywordDTO->getKeywordId() && $data["keyword_type_desc"] != $typeName)
                {
                    $responseDTO->setIsSuccess(false);
                    $responseDTO->setMessage("The keywords you are trying to add has been added in this group.");
                }
            }
        }

        return $responseDTO;
    }

    /**
     * getHmacToGetKeywordInformation
     * @param $keywordId
     * @return DTOResponse
     */
    private function getHmacToGetKeywordInformation($keywordId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=GET&url=%2Fv2%2Fpromo%2Fkeyword&content=keyword_id%3D'.$keywordId.'%26shop_id%3D'.$this->shopId.'&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getKeywordInformation
     * @param $keywordId
     * @return DTOResponse
     */
    public function getKeywordInformation($keywordId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get authenticate for header request
        $hmacData = $this->getHmacToGetKeywordInformation($keywordId);
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2/promo/keyword?keyword_id='.$keywordId.'&shop_id='.$this->shopId);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: GET';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        $postData = [
            'keyword_id' => $keywordId,
            'shop_id' => $this->shopId,
        ];

        $postData = json_encode($postData);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $info = $result['data'];
        // Check data not null
        if (is_null($info)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage("Keyword not found");
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getHmacToGetBidInfo
     * @return DTOResponse
     */
    private function getHmacToGetBidInfo()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=POST&url=%2Fv2.1%2Fdashboard%2Fbid_info&content=&content_type=application%2Fjson&version=v2');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getBidInfoKeyWord
     * @param $groupId
     * @return DTOResponse
     * @throws Exception
     */
    public function getBidInfoKeyWord($groupId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $defaultData = [
            'data' => [
                [
                    'min_bid' => 600 // Same channel
                ]
            ]
        ];

        if (!$groupId) {
            $responseDTO->setData($defaultData);
            return $responseDTO;
        }
        // Get authenticate for header request
        $hmacData = $this->getHmacToGetBidInfo();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $postData = [
            "data" => [
                "request_type" => "keyword",
                "shop_id" => $this->shopId,
                "data_suggestions" => [[
                    "ids" => [
                        (integer)$groupId
                    ],
                    "type" => "group"
                ]],
                "source" => "du_keyword_positive"
            ]
        ];

        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.1/dashboard/bid_info');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Error when no data
        if (!isset($result['data'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Bid Info Not Found!');
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getHmacToGetGroupKeywordInformation
     * @param $keywordDTO KeywordAdsDTO
     * @return DTOResponse
     */
    private function getHmacToGetGroupKeywordInformation($keywordDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        $url = self::BASE_URL . '/v1/dashboard/hmac?method=GET&url=%2Fv1.1%2Fdashboard%2Fkeywords&content=group%3D'.$keywordDTO->getKeywordGroupId().'%26keyword%3D'.urlencode(trim($keywordDTO->getKeywordName())).'%26is_positive%3D'.$keywordDTO->getIsPositive().'%26separate_statistic%3D'.$keywordDTO->getSeparateStatistic().'%26shop_id%3D'.$this->shopId.'&content_type=';

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__, $url);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getGroupKeywordInformation
     * @param $keywordDTO KeywordAdsDTO
     * @return DTOResponse
     * @throws Exception
     */
    public function getGroupKeywordInformation($keywordDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Get authenticate for header request
        $hmacData = $this->getHmacToGetGroupKeywordInformation($keywordDTO);
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        $url = self::BASE_URL . '/v1.1/dashboard/keywords?group='.$keywordDTO->getKeywordGroupId().'&keyword='.urlencode(trim($keywordDTO->getKeywordName())).'&is_positive='.$keywordDTO->getIsPositive().'&separate_statistic='.$keywordDTO->getSeparateStatistic().'&shop_id='.$this->shopId;

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: GET';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        $postData = [
            'group' => $keywordDTO->getKeywordGroupId(),
            'keyword' => $keywordDTO->getKeywordName(),
            'is_positive' => $keywordDTO->getIsPositive(),
            'separate_statistic' => $keywordDTO->getSeparateStatistic(),
            'shop_id' => $this->shopId,
        ];

        $postData = json_encode($postData);

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Error when no data
        if (!isset($result['data'])) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Keyword Information Not Found!');
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * getHmacForEditKeyword
     * @return DTOResponse
     */
    private function getHmacForEditKeyword()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v1/dashboard/hmac?method=PATCH&url=%2Fv2%2Fpromo%2Fkeyword&content=&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: vi,en;q=0.9';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * editKeywordInformation
     * @param $keywordDTO KeywordAdsDTO
     * @return DTOResponse
     * @throws Exception
     */
    public function editKeywordInformation($keywordDTO)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        // Verify input data
        $rsCheckInput = $this->checkInputEditKeywordInformation($keywordDTO);
        if(!$rsCheckInput->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($rsCheckInput->getMessage());
            return $responseDTO;
        }

        // Get authenticate for header request
        $hmacData = $this->getHmacForEditKeyword();
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }

        // Hmac Data
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Get Keyword Information Before Edit
        $keywordInfo = $this->getKeywordInformation($keywordDTO->getKeywordId());
        if (!$keywordInfo->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($keywordInfo->getMessage());
            return $responseDTO;
        }
        $keywordInfo = $keywordInfo->getData()['data'];

        // Setting value to edit keyword
        $this->settingValueToEditKeyword($keywordDTO, $keywordInfo);

        // Get Bid Info of keyword
        $bidInfo = $this->getBidInfoKeyWord($keywordDTO->getKeywordGroupId());
        if (!$bidInfo->getIsSuccess()) {
            $bidInfo->setIsSuccess(false);
            $bidInfo->setMessage($bidInfo->getMessage());
            return $responseDTO;
        }

        // Verify bid price input
        $bidInfo = $bidInfo->getData()['data'];
        $errBidPrice = $this->checkMinBidPrice($keywordDTO, $bidInfo);
        if (!$errBidPrice->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($errBidPrice->getMessage());
            return $responseDTO;
        }

        // Get keyword group information
        $keywordGroup = $this->getGroupKeywordInformation($keywordDTO);
        if (!$keywordGroup->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($keywordGroup->getMessage());
            return $responseDTO;
        }

        // Verify Keyword type
        $keywordGroup = $keywordGroup->getData()['data'];
        $errKwType = $this->checkTypeKeyword($keywordDTO, $keywordGroup);
        if (!$errKwType->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($errKwType->getMessage());
            return $responseDTO;
        }

        $postData = [
            "data" => [[
                "keyword_id" => (string)$keywordDTO->getKeywordId(),
                "keyword_type_id" => $keywordDTO->getMatchType(),
                "shop_id" => (string)$this->shopId,
                "group_id" => $keywordDTO->getKeywordGroupId(),
                "price_bid" => intval($keywordDTO->getBidPrice()),
                "source" => $keywordDTO->getKeywordSource(),
                "keyword_tag" => $keywordDTO->getKeywordName(),
                "toggle" => $keywordDTO->getStatusKeywordAds()
            ]]
        ];

        $postData = json_encode($postData);

        curl_setopt($this->ch, CURLOPT_URL, 'https://ta.tokopedia.com/v2/promo/keyword');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PATCH');

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: PATCH';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $postData, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * settingValueToEditKeyword
     * @param $keywordDTO KeywordAdsDTO
     * @param $data
     * @throws Exception
     */
    private function settingValueToEditKeyword($keywordDTO, $data)
    {
        if (isset($data['keyword_tag'])) {
            $keywordDTO->setKeywordName($data['keyword_tag']);
        }

        if (isset($data['group_id'])) {
            $keywordDTO->setKeywordGroupId($data['group_id']);
        }

        if (isset($data['status'])) {
            $stt = $data['status'] == "1" || $data['status'] == "2"
                ? $keywordDTO->getStatusActive() : $keywordDTO->getStatusDeactivate();
            $keywordDTO->setStatusKeywordAds($stt);
        }
    }
    #endregion

    #region Crawl total product and total keyword of group ads
    /**
     * @return mixed
     */
    private function getHMACForGetTotalProductAndTotalKeywordOfGroup()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, 'https://ta.tokopedia.com/v1/dashboard/hmac?method=POST&url=%2Fv1.1%2Fdashboard%2Ftotal_ad_keyword&content=&content_type=');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            LogError::getInstance()->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }


    /**
     * @param $listGroupAds
     * @return mixed
     * @throws Exception
     */
    public function getTotalProductAndTotalKeywordOfGroup($listGroupAds)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $hmacData = $this->getHMACForGetTotalProductAndTotalKeywordOfGroup();
        $this->checkHmacData($hmacData);

        $data = [
            "data" => [
                "shop_id" => $this->shopId,
                "ids" => $listGroupAds
            ]
        ];

        curl_setopt($this->ch, CURLOPT_URL, 'https://ta.tokopedia.com/v1.1/dashboard/total_ad_keyword');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept-Language: en-US';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
        $headers[] = 'X-Requested-With: XMLHttpRequest';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: POST';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            LogError::getInstance()->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $data, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Crawl Order
    #endregion

    #region Crawl Product
    /**
     * @return DTOResponse
     */
    public function getAllProduct()
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        curl_setopt($this->ch, CURLOPT_URL, 'https://tome.tokopedia.com/v2/shop/'. $this->shopId . '/product_list?lang=en&cache=clear');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: tome.tokopedia.com';
        $headers[] = 'Accept: */*';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, null, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        if ( ! isset($result['data'])) {
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'Channel change params!', $result));
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Channel change params');
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result['data'] ?? []);
        return $responseDTO;
    }
    #endregion

    #region Crawl Category
    #endregion

    #region Crawl Dashboard Info
    /**
     * @param $startDate
     * @param $endDate
     * @return DTOResponse
     */
    private function getHMACForGetDashBoardTypeProduct($startDate, $endDate)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $data = json_encode([
            'shopId' => $this->shopId,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
        $url = self::BASE_URL . '/v1/dashboard/hmac?method=GET&url=%2Fv2.2%2Fdashboard%2Fstatistics&content=start_date%3D' . $startDate . '%26end_date%3D' . $endDate . '%26type%3D1%26shop_id%3D' . $this->shopId . '&content_type=&version=v1';

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__, $data);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return DTOResponse
     */
    public function getDashBoardTypeProduct($startDate, $endDate)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!is_int($startDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required $startDate');
            return $responseDTO;
        }

        if (!is_int($endDate)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required $endDate');
            return $responseDTO;
        }

        $startDate = date('Y-m-d', $startDate);
        $endDate = date('Y-m-d', $endDate);

        $data = [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'shopId' => $this->shopId
        ];

        // Get and verify HMAC
        $hmacData = $this->getHMACForGetDashBoardTypeProduct($startDate, $endDate);
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        curl_setopt($this->ch, CURLOPT_URL, self::BASE_URL . '/v2.2/dashboard/statistics?start_date=' . $startDate . '&end_date=' . $endDate . '&type=1&shop_id=' . $this->shopId);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: GET';
        $headers[] = 'Accept-Encoding: gzip, deflate, br';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $data, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion

    #region Get Suggest Keyword For Add SKU To Group Ads
    /**
     * @param $arrayProductId array
     * @return DTOResponse
     */
    private function getHmacForGetSuggestKeywordForAddSKU($arrayProductId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $stringArrayId = implode('%2C', $arrayProductId);

        curl_setopt($this->ch, CURLOPT_URL, 'https://ta.tokopedia.com/v1/dashboard/hmac?method=GET&url=%2Fv2.2%2Fkeyword_suggest&content=product_ids%3D'.$stringArrayId.'&content_type=&version=v1');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Sec-Fetch-Dest: empty';
        $headers[] = 'X-Device: desktop';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->verifyAndWriteLogHmac($result, $response, __FUNCTION__, $stringArrayId);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }

    public function getSuggestKeywordForAddSKU($arrayProductId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        $stringArrayId = implode('%2C', $arrayProductId);

        // Get and verify HMAC
        $hmacData = $this->getHmacForGetSuggestKeywordForAddSKU($arrayProductId);
        if (!$hmacData->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($hmacData->getMessage());
            return $responseDTO;
        }
        $hmacData = $hmacData->getData();

        // Verify HMAC
        $isSuccess = $this->checkHmacData($hmacData);
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        curl_setopt($this->ch, CURLOPT_URL, 'https://ta.tokopedia.com/v2.2/keyword_suggest?product_ids=' . $stringArrayId);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: ta.tokopedia.com';
        $headers[] = 'Content-Md5: ' . $hmacData['content'];
        $headers[] = 'Authorization: ' . $hmacData['hmac'];
        $headers[] = 'Content-Type: ' . $hmacData['content_type'];
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'X-Tkpd-Userid: ' . $this->tkpdUserId;
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
        $headers[] = 'X-Date: ' . $hmacData['access_date'];
        $headers[] = 'X-Device: desktop';
        $headers[] = 'X-Method: GET';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,vi;q=0.8';
        $headers[] = 'Cookie: ' . $this->cookieString;
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        if($response == "[]") {
            $responseDTO->setData([]);
            return $responseDTO;
        }
        $result = json_decode($response, true);

        if (curl_errno($this->ch)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('CURL Failure');
            $this->logService->slack($this->buildDataPushSlack(__FUNCTION__, 'CURL Failure!', $this->ch));
            return $responseDTO;
        }

        // Push error to slack
        $isSuccess = $this->writeLogForSimulatorAction($result, __FUNCTION__, $stringArrayId, $response);

        // Response simulation to channel failure
        if (!$isSuccess->getIsSuccess()) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage($isSuccess->getMessage());
            return $responseDTO;
        }

        // Response success
        $responseDTO->setData($result);
        return $responseDTO;
    }
    #endregion
}