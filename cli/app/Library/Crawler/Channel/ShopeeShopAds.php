<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 14/01/2020
 * Time: 06:37
 */

namespace App\Library\Crawler\Channel;


use App\Application\Functions\Strings;
use Illuminate\Support\Str;

class ShopeeShopAds extends Shopee
{
    public $shopChannelId;

    public function __construct($shopChannelId = null)
    {
        $this->shopChannelId = $shopChannelId;
        parent::__construct($shopChannelId);

    }

    /**
     * @return mixed
     */
    public function getCookiePath()
    {
        return $this->cookiePath;
    }

    /**
     * @param mixed $cookiePath
     */
    public function setCookiePath($cookiePath)
    {
        $this->cookiePath = $cookiePath;
    }

    /**
     * @param $itemId
     * @param $dateTimeFrom
     * @param $dateTimeTo
     * @param int $isHasSearchTerm
     * @return bool|string
     */
    public function getPerformanceByDatetime($itemId, $dateTimeFrom, $dateTimeTo, $isHasSearchTerm = 0)
    {
        $dateFrom = strtotime($dateTimeFrom);
        $dateTo = strtotime($dateTimeTo);

        $randomString = Str::random(32);
        $url = $this->shopeeSellerDomain() . '/api/v2/pas/report/detail_report_by_keyword/?start_time=' . $dateFrom . '&end_time=' . $dateTo . '&placement_list=%5B0%5D&agg_interval=96&need_detail=' . $isHasSearchTerm . '&itemid=' . $itemId . '&SPC_CDS=' . $randomString . '&SPC_CDS_VER=2';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = [];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Connection: keep-alive';
        #$headers[] = 'Referer: ' . $this->shopeeSellerDomain() . '/portal/marketing/pas/assembly/' . $adsId . '?from=' . $dateFrom . '&to=' . $dateTo . '&group=' . $group;
        $headers[] = 'Te: Trailers';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        curl_close($ch);

        return $result;
    }

    /**
     * Get shop performance
     * @param string $dateFrom
     * @param string $dateTo
     * @return string
     */
    public function getShopPerformance($dateFrom, $dateTo, $adsId)
    {
        if ($dateFrom == date('Y-m-d 00:00:00')) {
            $agg_interval = 1;
        } else {
            $agg_interval = 4;
        }
        $dateFrom = strtotime($dateFrom);
        $dateTo = strtotime($dateTo);
        $randomString = Str::random(32);
        $url = $this->shopeeSellerDomain()."/api/v2/pas/report/detail_report_by_time/?start_time={$dateFrom}&end_time={$dateTo}&placement_list=%5B3%5D&agg_interval={$agg_interval}&adsid={$adsId}&SPC_CDS={$randomString}&SPC_CDS_VER=2";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authority: seller.shopee.ph';
        $headers[] = 'Accept: application/json, text/plain, */*';
        $headers[] = 'Sec-Fetch-Dest: empty';
        $headers[] = 'Sc-Fe-Ver: v200220-marketing';
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36';
        $headers[] = 'Sec-Fetch-Site: same-origin';
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Accept-Language: en-US,en;q=0.9';

        if ($this->subAccountId) {
            $headers[] = 'Cookie: '.$this->cookieContent;
        } else {
            curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }
}