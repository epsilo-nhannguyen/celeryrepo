<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 19/12/2019
 * Time: 14:23
 */

namespace App\Library;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelBusiness extends Model
{
    protected $connection = 'master_business';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}