<?php


namespace App\Library;
use Exception;


class ElasticSearch
{
    protected $ch;

    private $url;

    const SEARCH = '_search';
    const CREATE = '_create';

    public function __construct($isShowMessage = 0)
    {
        $host = env('ELASTICSEARCH_HOST', 'https://search-epsilo-dwh-7q4t52wkw5el5gvl5eaxwvxhhu.ap-southeast-1.es.amazonaws.com');
        $this->url = trim($host, '/');

        Common::setIsShowMessage($isShowMessage);
    }

    protected function execute($index, $action, $param)
    {
        $arrayActionAllow = [self::CREATE, self::SEARCH];
        if ( ! in_array($action, $arrayActionAllow)) {
            throw new Exception('$action must be in array ['. implode(',', $arrayActionAllow).']');
        }

        if ($action == self::CREATE) {
            $method = 'POST';
        } else {
            $method = 'GET';
        }

        $url = "$this->url/$index/$action?pretty";

        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 60);
        #curl_setopt($this->ch, CURLOPT_VERBOSE, 1);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $method);

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($param));

        $headers = [];
        $headers[] = 'Content-Type: application/json';

        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($this->ch);

        Common::showMessage(PHP_EOL.PHP_EOL.$result);

        if (curl_errno($this->ch)) {
            throw new Exception('Error: ' . curl_error($this->ch));
        }

        curl_close($this->ch);

        return json_decode($result, true);
    }

    public function search($index, $param)
    {
        return $this->execute($index, self::SEARCH, $param);
    }

}
