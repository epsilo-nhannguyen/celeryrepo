<?php

namespace App\Library;

use App\Models\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use stdClass;

class Cache
{
    /**
     * @var stdClass
     */
    private static $data;

    private static $container;

    private static $key = '';

    private static $_instance;

    private static $redis;

    private function __construct()
    {
        self::$redis = Redis::connection('redis_for_cache');
    }

    /**
     * @return Cache
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function get($key)
    {
        return self::$redis->get($key);
    }

    public static function save($key, $data, $timeLife = null)
    {
        self::$redis->set($key, $data);
        if ($timeLife != null) {
            self::$redis->expire($key, $timeLife);
        }
    }

    /**
     * @return string
     */
    public function tokenPassport()
    {
        return md5(__FUNCTION__);
    }

    /**
     * @return string
     */
    public function getAllVersion()
    {
        return md5(__FUNCTION__);
    }

    /**
     * @return string
     */
    public function getAllVenture()
    {
        return __FUNCTION__;
    }

    /**
     * @return string
     */
    public function getAllInfServer()
    {
        return md5(__FUNCTION__);
    }

    /**
     * @return string
     */
    public function getAllInfCrontab()
    {
        return md5(__FUNCTION__);
    }

    /**
     * @return string
     */
    public static function getAllInfCrontabServer()
    {
        return md5(__FUNCTION__);
    }

    /**
     * @return mixed
     */
    public static function resetInfCrontabServer()
    {
        $key = self::getAllInfCrontabServer();
        return self::$redis->del([$key]);
    }
}