<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:54
 */

namespace App\Library\Factory;


use App\Library;


class Channel extends Library\Factory
{
    /**
     * @param $classIdentifier
     * @param $config
     * @return mixed
     * @throws Library\Exceptions\FactoryException
     */
    public static function build($classIdentifier, $config = null)
    {
        parent::setClassFactory(__CLASS__);
        return parent::build($classIdentifier, $config);
    }

}