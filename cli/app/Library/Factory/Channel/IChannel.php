<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:35
 */

namespace App\Library\Factory\Channel;

use App\Library;


interface IChannel
{
    /**
     * @return string
     */
    public function getCredential();

    /**
     * @param $credential
     */
    public function setCredential($credential);

    /**
     * @param $shopId
     */
    public function setShopId($shopId);

    /**
     * @return int
     */
    public function getShopId();

    /**
     * @param $channelId
     */
    public function setChannelId($channelId);

    /**getApiCredential
     * @return int
     */
    public function getChannelId();

    /**
     * @param int $updateAtFrom
     * @param int $updateAtTo
     * @param int $limit
     * @param int $offset
     * @return Library\Response\Channel
     */
    public function pullOrder($updateAtFrom, $updateAtTo, $limit, $offset);

    /**
     * [sku => ['rrp', 'selling_price' 'stock']]
     * @param array $arraySku
     * @return array
     */
    public function pullProduct($arraySku);
}
