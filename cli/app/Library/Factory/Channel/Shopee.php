<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 18:07
 */

namespace App\Library\Factory\Channel;


use App\Library;
use Exception;


class Shopee implements Library\Factory\Channel\IChannel
{
    /**
     * @var int
     */
    private $discountId;

    /**
     * @var int
     */
    private $channelId;

    /**
     * @var int
     */
    private $shopId;

    /**
     * @var string
     */
    private $credential;

    /**
     * @var string
     */
    private $shopKey = null;

    /**
     * @var int
     */
    private $shopIdentifier = null;

    /**
     * @var int
     */
    private $partnerId = null;

    /**
     * @var string
     */
    private $url = null;

    /**
     * @var string
     */
    private $authUrl = null;

    /**
     * @var string
     */
    private $appKey = null;

    /**
     * @var string
     */
    private $callBackUrl = null;


    /**
     * CREDENTIAL PARAM
     */
    const CREDENTIAL_APP_KEY = 'key';
    const CREDENTIAL_SHOP_ID = 'shop_id';
    const CREDENTIAL_PARTNER_ID = 'partner_id';
    const CREDENTIAL_URL = 'url';
    const CREDENTIAL_AUTH_URL = 'auth_url';
    const CREDENTIAL_CALLBACK_URL = 'callback_url';
    const CREDENTIAL_DISCOUNT_ID = 'discount_id';

    /**
     * ACTION
     */
    const ACTION_GET_SALE_ORDER = '/orders/basics';
    const ACTION_GET_SALE_ORDER_STATUS = '/orders/get';
    const ACTION_GET_SALE_ORDER_DETAIL = '/orders/detail';
    const ACTION_GET_AIRWAY_BILL = '/logistics/airway_bill/get_mass';
    const ACTION_GET_LOGISTICS_ADDRESS = '/logistics/address/get';
    const ACTION_GET_LOGISTICS_TIME_SLOT = '/logistics/timeslot/get';
    const ACTION_GET_LOGISTICS_INIT = '/logistics/init';
    const ACTION_GET_LOGISTICS_INIT_PARAM = '/logistics/init_parameter/get';
    const ACTION_CANCEL_ORDER = '/orders/cancel';

    const ACTION_GET_CATEGORIES = '/item/categories/get';
    const ACTION_GET_ATTRIBUTES = '/item/attributes/get';
    const ACTION_GET_ITEM_LIST = '/items/get';
    const ACTION_GET_ITEM_DETAIL = '/item/get';
    const ACTION_CREATE_PRODUCT = '/item/add';
    const ACTION_UPDATE_PRODUCT = '/item/update';
    const ACTION_PRODUCT_ADD_IMAGE = '/item/img/add';
    const ACTION_PRODUCT_DELETE_IMAGE = '/item/img/delete';
    const ACTION_DELETE_PRODUCT = '/item/delete';
    const ACTION_GET_LOGISTICS = '/logistics/channel/get';

    const ACTION_UPDATE_PRICE = '/items/update_price';
    const ACTION_UPDATE_STOCK = '/items/update_stock';
    const ACTION_UPDATE_VARIATION_PRICE = '/items/update_variation_price';
    const ACTION_UPDATE_VARIATION_STOCK = '/items/update_variation_stock';

    const ACTION_ADD_VARIATION = '/item/add_variations';
    const ACTION_DELETE_VARIATION = '/item/delete_variation';

    /**
     * config Value
     */
    const CONFIG_ENTRIES_PER_PAGE = 100;
    /**
     * MODE SHIPPING
     */
    const MODE_SHIPPING_DROP_OFF = 'dropoff';
    const MODE_SHIPPING_PICK_UP = 'pickup';
    const MODE_SHIPPING_NON_INTEGRATED = 'non_integrated';


    const DISCOUNT_STATUS_UPCOMING = 'UPCOMING';
    const DISCOUNT_STATUS_ONGOING = 'ONGOING';
    const DISCOUNT_STATUS_EXPIRED = 'EXPIRED';
    const DISCOUNT_STATUS_ALL = 'ALL';

    const PARAM_ORDER_SERIAL_NUMBER = 'ordersn';

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * @return int
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param int $shopId
     * @return int|void
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * @param int $channelId
     * @return int|void
     */
    public function setChannelId($channelId)
    {
        $this->channelId = $channelId;
    }

    /**
     * @param string $credential
     * @return mixed|void
     * @throws Exception
     */
    public function setCredential($credential)
    {
        if (is_null($credential)) {
            throw new \Exception("Cannot find shop channel by shop id $this->shopId & channel id $this->channelId");
        }
        $this->credential = $credential;
        $apiCredential = json_decode($credential, true);
        $this->shopKey = $apiCredential[self::CREDENTIAL_APP_KEY];
        $this->shopIdentifier = intval($apiCredential[self::CREDENTIAL_SHOP_ID]);
        $this->partnerId = intval($apiCredential[self::CREDENTIAL_PARTNER_ID]);
        $this->url = $apiCredential[self::CREDENTIAL_URL];
        $this->authUrl = $apiCredential[self::CREDENTIAL_AUTH_URL];
        $this->callBackUrl = $apiCredential[self::CREDENTIAL_CALLBACK_URL] ?? '';
        $this->discountId = $apiCredential[self::CREDENTIAL_DISCOUNT_ID] ?? 0;
    }

    private function saveDiscountId()
    {
        if (is_null($this->credential)) {
            throw new \Exception("Cannot find shop channel by shop id $this->shopId & channel id $this->channelId");
        }
        $data = json_decode($this->credential, true);
        $data[self::CREDENTIAL_DISCOUNT_ID] = $this->discountId;
        $this->credential = json_encode($data);
        Model_ShopChannel::getInstance()->updateByShopChannel($this->shopId, $this->channelId, array(
            DbTable_Shop_Channel::COL_SHOP_CHANNEL_API_CREDENTIAL => $this->credential
        ));
    }

    /**
     * @param string $action
     * @param string $method
     * @param array $data
     * @return array
     */
    protected function execute($action, $method, $data)
    {
        $body = array(
            'partner_id' => $this->partnerId,
            'shopid' => intval($this->shopIdentifier), //shop_id
            'timestamp' => time(),
        );
        $url = $this->url . $action;
        if ($data) {
            $body = array_merge($body, $data);
        }
        $bodyJson = json_encode($body);
        $signature = hash_hmac('SHA256', $url . '|' . $bodyJson, $this->shopKey); //secret
        $header = 'Authorization: ' . $signature;

        $request = new Library\CurlBuilder();
        $request->setMethod($method);
        $request->setUrl($url);
        $request->setParam($body);
        $request->header($header);
        $request->isJson();

        $response = $request->execute();
        return $response;
    }

    /**
     * @param array $arrayOrderNumber
     * @return array
     */
    public function getOrderDetails($arrayOrderNumber)
    {
        $params = array(
            'ordersn_list' => (array) $arrayOrderNumber,
        );
        $response = $this->execute(self::ACTION_GET_SALE_ORDER_DETAIL, 'POST', $params);
        $result = $response['orders'];
        return $result;
    }

    /**
     * @param int $updateAtFrom
     * @param int $updateAtTo
     * @param int $limit
     * @param int $offset
     * @return Library\Response\Channel
     * @throws Exception
     */
    public function pullOrder($updateAtFrom, $updateAtTo, $limit, $offset)
    {
        $result = new Library\Response\Channel();

        $arrayOrder = [];
        $params = [
            'update_time_from' => $updateAtFrom,
            'update_time_to' => $updateAtTo,
            'pagination_entries_per_page' => intval($limit),
            'pagination_offset' => intval($offset),
        ];

        $response = $this->execute(self::ACTION_GET_SALE_ORDER, 'POST', $params);
        $arrayOrder = array_merge($arrayOrder, $response['orders'] ?? []);
        $arrayOrderNumber = array_merge(array_column($arrayOrder, 'ordersn'));
        $chunkArrayOrderNumber = array_chunk($arrayOrderNumber, 50); //Shopee allow 50 per request
        $arrayOrderDetail = [];
        foreach ($chunkArrayOrderNumber as $chunkItem) {
            $arrayOrderDetail = array_merge($arrayOrderDetail, $this->getOrderDetails($chunkItem));
        }
        $arrayOrders = [];
        foreach ($arrayOrderDetail as $order) {

            $orderObject = new Library\DataStructure\ChannelOrder();

            $itemsOrigin = $order['items'];
            $items = [];
            foreach ($itemsOrigin as $_item) {
                $newItem = $_item;
                unset($newItem['variation_quantity_purchased']);
                for ($i = 0; $i < $_item['variation_quantity_purchased']; $i++) {
                    $items[] = $newItem;
                }
            }
            usort($items, function ($a, $b) {
                $swap = 1;
                $noSwap = -1;
                $itemSkuA = $a['variation_sku'] ? $a['variation_sku'] : $a['item_sku'];
                $itemSkuB = $b['variation_sku'] ? $b['variation_sku'] : $b['item_sku'];
                if ($itemSkuA == $itemSkuB) {
                    if ($a['variation_original_price'] < $b['variation_original_price']) {
                        $result = $swap;
                    } else {
                        $result = $noSwap;
                    }

                } elseif ($itemSkuA > $itemSkuB) {
                    $result = $swap;
                } else {
                    $result = $noSwap;
                }

                return $result;
            });

            #prepare data for order --start
            $orderNumber = $order['ordersn'];
            $totalAmount = $order['total_amount'];
            $orderCreatedAt = $order['create_time'];
            $orderStatus = $order['order_status'];
            $orderAddressCity = $order['recipient_address']['city'] ?? '';
            $orderAddressPhone = $order['recipient_address']['phone'];
            $orderUnsetItem = $order;
            unset($orderUnsetItem['items']);
            $orderFullJson = json_encode($orderUnsetItem);
            #prepare data
//            $orderStatusRow = Crontab_Model_SaleOrderChannelStatus::getInstance()->getByName(
//                $order['order_status'],
//                $this->getChannelId()
//            );
            $orderStatusId = null;
            $orderUpdateTime = $order['update_time'];
            $orderShippingProvider = $order['shipping_carrier'];
            #prepare data for order --end

            #mapping data to order --start
            $orderObject->setOrderNumber($orderNumber);
            $orderObject->setOrderId('');
            $orderObject->setVoucherAmount('');
            $orderObject->setVoucherCode('');
            $orderObject->setTotalPrice($totalAmount);
            $orderObject->setCreatedAt($orderCreatedAt);
            $orderObject->setStatus($orderStatus);
            $orderObject->setAddressCity($orderAddressCity);
            $orderObject->setAddressPhone($orderAddressPhone);
            $orderObject->setFullJson($orderFullJson);
            $orderObject->setChannelStatusId($orderStatusId);
            #mapping data to order --end

            $index = 1;
            $arrayItems = array_map(function ($item) use ($orderObject, $orderUpdateTime, &$index, $orderShippingProvider) {
                $itemObject = new Library\DataStructure\ChannelOrder\Item();

                #prepare data for order item --start
                $itemSku = $item['variation_sku'] ? $item['variation_sku'] : $item['item_sku'];
                $itemName = $item['item_name'];
                $itemPrice = $item['variation_discounted_price'];
                $itemId = $item['item_id'];
//                $itemStatusRow = Crontab_Model_SaleOrderItemChannelStatus::getInstance()->getByName(
//                    $orderObject->getStatus(),
//                    $this->getChannelId()
//                );
                $itemStatusId = null;
                $itemDateString = date('Y-m-d H:i:s', $orderObject->getCreatedAt());
                $itemFullJson = json_encode($item);
                $itemInternalId = Manager::genInternalId(
                    $this->getShopId(),
                    $this->getChannelId(),
                    $orderObject->getOrderNumber(),
                    $index
                );
                #prepare data for order item --end

                #mapping data to order item --start
                $itemObject->setSku($itemSku);
                $itemObject->setName($itemName);
                $itemObject->setChannelSku($itemSku);
                $itemObject->setItemPrice($itemPrice);
                $itemObject->setVoucherCode('');
                $itemObject->setVoucherAmount(0);
                $itemObject->setItemId($itemId);
                $itemObject->setCreatedAt($orderObject->getCreatedAt());
                $itemObject->setUpdatedAt($orderUpdateTime);
                $itemObject->setStatusId($itemStatusId);
                $itemObject->setDateString($itemDateString);
                $itemObject->setShipmentProvider($orderShippingProvider);
                $itemObject->setFullJson($itemFullJson);
                $itemObject->setInternalId($itemInternalId);
                if ($orderObject->getStatus() == 'COMPLETED') {
                    $itemObject->setChannelDeliveredAt($orderUpdateTime);
                }
                #mapping data to order item --end
                $index++;
                return $itemObject;
            }, $items);
            $orderObject->setArrayItem($arrayItems);
            $arrayOrders[] = $orderObject;
        }
        $result->setOrders($arrayOrders);
        return $result;
    }

    /**
     * @param $orderNumber
     * @param $modeShipping
     * @return mixed
     */
    private function getParameterForInit($orderNumber, $modeShipping)
    {
        $params = array(
            'ordersn' => $orderNumber
        );
        $response = $this->execute(self::ACTION_GET_SALE_ORDER, 'POST', $params);
        return $response[$modeShipping];
    }

    /**
     * @param $orderNumber
     * @return string
     */
    public function getLogisticsAddress($orderNumber)
    {
        $addressData = $this->execute(
            '/logistics/address/get',
            'POST',
            [
                self::PARAM_ORDER_SERIAL_NUMBER => $orderNumber
            ]
        );
        $addressId = $addressData['address_list'][0]['address_id'] ?? null;
        if ($this->shopIdentifier == '76652247') {
            $addressId = 11130164;
        }
        if ($this->shopIdentifier == '93936854') {
            $addressId = 11175795;
        }

        if (Application_Env::getEnvName() == 'staging') {
            foreach ($addressData['address_list'] as $addressInfo) {
                if (trim($addressInfo['address']) == '12 Nguyễn Văn Hưởng') {
                    $addressId = $addressInfo['address_id'];
                }
            }
        }
        return $addressId;
    }

    /**
     * @param $ordersn
     * @return array
     * @throws \Exception
     */
    protected function getShippingLabel($ordersn)
    {
        $params['ordersn_list'] = array($ordersn);
        $response = $this->execute('/logistics/airway_bill/get_mass', 'POST', $params);
        if (!$this->isGetAwbSuccess($ordersn, $response)) {
            throw new \Exception('Cant get Awb OrderNumber: '.$ordersn);
        }
        $url = $response['result']['airway_bills'][0]['airway_bill'];
        $curl = new Library\CurlBuilder();
        $curl->setUrl($url);
        $data = $curl->getFile();
        $curlContentType = $curl->getContentTypeFile();
        $explodeContentType = explode(';', $curlContentType);
        $contentType = current($explodeContentType);
        if ($contentType == 'binary/octet-stream') {
            $contentType = 'application/pdf';
        }
        return [
            'document_type' => 'shippingLabel',
            'mime_type' => $contentType,
            'file' => base64_encode($data)
        ];
    }

    /**
     * @param $ordersn
     * @param array $response
     * @return bool
     */
    private function isGetAwbSuccess($ordersn, $response)
    {
        $awb = $response['result']['airway_bills'][0] ?? [];
        return isset($awb['airway_bill']) && $awb['ordersn'] == $ordersn;
    }

    /**
     * @param string $orderNumber
     * @param string $addressId
     * @param int $orderCreatedAt
     * @return string
     */
    public function getTimeSlot($orderNumber, $addressId, $orderCreatedAt)
    {
        $params = [
            'ordersn' => $orderNumber,
            'address_id' => $addressId,
        ];
        $response = $this->execute(self::ACTION_GET_LOGISTICS_TIME_SLOT, 'POST', $params);
        if (empty($response['pickup_time'])) {
            return 0;
        }

        $data = $response['pickup_time'];

        $dateToShipCutOff = $this->getDateToShip($orderCreatedAt);

        $validData = [];
        foreach ($data as $index=>$time) {
            if (date('w', $time['date']) == 0){
                continue;
            }

            $availableTime = intval($time['pickup_time_id']);
            $availableDate = strtotime(date('Y-m-d', $availableTime));
            if ($dateToShipCutOff == $availableDate) {
                return $availableTime;
            }

            array_push($validData, $time);
        }

        if (!$validData) return 0;

        $minPickupTime = strtotime(date('Y-m-d', intval($validData[0]['pickup_time_id'])));
        if ($dateToShipCutOff<$minPickupTime){
            return $validData[0]['pickup_time_id'];
        }

        return $validData[count($validData)-1]['pickup_time_id'];
    }

    /**
     * get Date To Ship
     * @param int $createdAt
     * @return int
     */
    public function getDateToShip($createdAt)
    {
        $channelInfo = Model_Channel::getInstance()->getByIdFromCache($this->getChannelId());
        $dateToShipNumber = $channelInfo[DbTable_Channel::COL_CHANNEL_DATE_TO_SHIP];
        $businessDayFrom = intval(date('w', strtotime($channelInfo[DbTable_Channel::COL_CHANNEL_BUSINESS_DAY_FROM])));
        $businessDayTo = intval(date('w', strtotime($channelInfo[DbTable_Channel::COL_CHANNEL_BUSINESS_DAY_TO])));

        $shopChannelInfo = Model_ShopChannel::getInstance()->getByShopIdChannelId($this->getShopId(), $this->getChannelId());
        $shopChannelDateToShip = Model_ShopChannelDateToShip::getInstance()->searchByShopChannelId($shopChannelInfo[DbTable_Shop_Channel::COL_SHOP_CHANNEL_ID]);
        if ($shopChannelDateToShip) {
            foreach ($shopChannelDateToShip as $dateToShip) {
                $dateToShipFrom = $dateToShip[DbTable_Shop_Channel_Date_To_Ship::COL_SHOP_CHANNEL_DATE_TO_SHIP_TIME_FROM];
                $dateToShipTo = $dateToShip[DbTable_Shop_Channel_Date_To_Ship::COL_SHOP_CHANNEL_DATE_TO_SHIP_TIME_TO];

                $dateToShipFrom = strtotime(date('Y-m-d '.$dateToShipFrom, $createdAt));
                $dateToShipTo = strtotime(date('Y-m-d '.$dateToShipTo, $createdAt));
                if ($createdAt >= $dateToShipFrom && $createdAt <= $dateToShipTo) {
                    $dateToShipNumber = $dateToShip[DbTable_Shop_Channel_Date_To_Ship::COL_SHOP_CHANNEL_DATE_TO_SHIP_DATE];
                }
            }
        } else {
            $shopInfo = Model_ShopMaster::getInstance()->getByIdFromCache($this->getShopId());
            $organizationDateToShip = Model_OrganizationDateToShip::getInstance()->searchByOrganizationIdAndChannelId(
                $shopInfo[DbTable_Shop_Master::COL_FK_ORGANIZATION], $this->getChannelId()
            );
            foreach ($organizationDateToShip as $dateToShip) {
                $dateToShipFrom = $dateToShip[DbTable_Organization_Date_To_Ship::COL_ORGANIZATION_DATE_TO_SHIP_TIME_FROM];
                $dateToShipTo = $dateToShip[DbTable_Organization_Date_To_Ship::COL_ORGANIZATION_DATE_TO_SHIP_TIME_TO];

                $dateToShipFrom = strtotime(date('Y-m-d '.$dateToShipFrom, $createdAt));
                $dateToShipTo = strtotime(date('Y-m-d '.$dateToShipTo, $createdAt));
                if ($createdAt >= $dateToShipFrom && $createdAt <= $dateToShipTo) {
                    $dateToShipNumber = $dateToShip[DbTable_Organization_Date_To_Ship::COL_ORGANIZATION_DATE_TO_SHIP_DATE];
                }
            }
        }

        $dateToShipCutOff = strtotime(date('Y-m-d', $createdAt) . ' + '.$dateToShipNumber.' day');
        $businessDay = intval(date('w', $dateToShipCutOff));
        $isDateToShipCutOffValid = $businessDay >= $businessDayFrom && $businessDay <= $businessDayTo;
        if (!$isDateToShipCutOffValid) {
            $dateToShipCutOff = strtotime('next monday');
        }
        return $dateToShipCutOff;
    }

    /**
     * @param array $orderInfo
     * @param array $arrayItem
     * @return Application_Response_Channel
     */
    public function confirmed($orderInfo, $arrayItem)
    {
        $orderNumber = $orderInfo[DbTable_Sale_Order::COL_SALE_ORDER_NUMBER];
        $orderCreatedAt = $orderInfo[DbTable_Sale_Order::COL_SALE_ORDER_ORDER_CREATED_AT];
        /*$saleOrder = Application_Registry::loadSaleOrder(
            $orderNumber,
            $this->getShopId(),
            $this->getChannelId(),
            true
        );*/
        $result = new Application_Response_Channel();
        //$json = json_decode($saleOrder[DbTable_Sale_Order::COL_SALE_ORDER_DATA], true);
        /*if ($json['order_status'] != 'READY_TO_SHIP') {
            $result->setIsSuccess(false);
            return $result;
        }*/
        //$paramForInit = $this->getParameterForInit($orderNumber, self::MODE_SHIPPING_PICK_UP);
        $addressId = $this->getLogisticsAddress($orderNumber);
        $pickupTimeId = $this->getTimeSlot($orderNumber, $addressId, $orderCreatedAt);
        if (!$pickupTimeId) {
            $result->setIsSuccess(false);
            return $result;
        }
        $paramForInit['address_id'] = $addressId;
        $paramForInit['pickup_time_id'] = strval($pickupTimeId);
        $params = [
            'ordersn' => $orderNumber,
            'pickup' => $paramForInit,
        ];

        $response = $this->execute(self::ACTION_GET_LOGISTICS_INIT, 'POST', $params);
        if (!isset($response['tracking_number'])) {
            $check = $this->getOrderDetails((array) $orderNumber);
            if (!isset($check['tracking_no'])) {
                //$result->setIsSuccess(false);
            }
        } else {
            $trackingNumber = $response['tracking_number'];
            if ($trackingNumber) {
                $itemArray = Application_Registry::loadSaleOrderItem(
                    $orderNumber,
                    $this->getShopId(),
                    $this->getChannelId()
                );
                foreach ($itemArray as $item) {
                    Model_SaleOrderItem::getInstance()->updateTrackingNumber(
                        $item[DbTable_Sale_Order_Item::COL_SALE_ORDER_ITEM_ID],
                        $trackingNumber
                    );
                }
                Model_SaleOrder::getInstance()->updateTrackingNumberByOrderShopChannel(
                    $orderNumber,
                    $this->getShopId(),
                    $this->getChannelId(),
                    $trackingNumber
                );
            } else {
                $result->setIsSuccess(false);
            }
        }
        return $result;
    }

    /**
     * @param array $orderInfo
     * @param array $arrayItem
     * @return Application_Response_Channel
     */
    public function readyToShip($orderInfo, $arrayItem)
    {
        $orderNumber = $orderInfo[DbTable_Sale_Order::COL_SALE_ORDER_NUMBER];
        $orderCreatedAt = $orderInfo[DbTable_Sale_Order::COL_SALE_ORDER_ORDER_CREATED_AT];
        $result = new Application_Response_Channel();
        $addressId = $this->getLogisticsAddress($orderNumber);
        $pickUpTimeId = $this->getTimeSlot($orderNumber, $addressId, $orderCreatedAt);
        if (!$pickUpTimeId) {
            $result->setIsSuccess(false);
            return $result;
        }
        $response = $this->execute(
            '/logistics/init',
            'POST',
            [
                'ordersn' => $orderNumber,
                'pickup' => [
                    'address_id' => $addressId,
                    'pickup_time_id' => $pickUpTimeId
                ]
            ]
        );
        return $result;
    }

    /**
     * @param string $orderNumber
     * @return Application_Response_Channel
     */
    public function getAwb($orderNumber)
    {
        $result = new Application_Response_Channel();
        $trackingNo = $this->getTrackingNumber($orderNumber);
        if ($trackingNo) {
            $ordersn = $orderNumber;
            $response = [
                'tracking_number' => $trackingNo,
                'awb' => [
                    'Invoice' => null,
                    'ShippingLabel' => $this->getShippingLabel($ordersn),
                ]
            ];
            $result->setAwb($response);
        } else {
            $result->setIsSuccess(false);
        }
        return $result;
    }


    /**
     * @param $orderNumber
     * @return mixed
     */
    public function getTrackingNumber($orderNumber)
    {
        $saleOrder = Application_Registry::loadSaleOrder(
            $orderNumber,
            $this->getShopId(),
            $this->getChannelId(),
            true
        );
        $trackingNo = $saleOrder[DbTable_Sale_Order::COL_SALE_ORDER_TRACKING_NUMBER];
        if ($trackingNo) {
            return $trackingNo;
        }

        /*$addressId = $this->getLogisticsAddress($orderNumber);

        $pickUpTimeId = $this->getTimeSlot($orderNumber, $addressId);
        if (!$pickUpTimeId) {
            return '';
        }
        $response = $this->execute(
            self::ACTION_GET_LOGISTICS_INIT,
            'POST',
            [
                self::PARAM_ORDER_SERIAL_NUMBER => $orderNumber,
                'pickup' => [
                    'address_id' => $addressId,
                    'pickup_time_id' => $pickUpTimeId
                ]
            ]
        );
        $trackingNo = ($response['tracking_number'] ?? '');*/
        $order = $this->getOrderDetails($orderNumber)[0];
        return $trackingNo ? $trackingNo : ($order['tracking_no'] ?? '');
    }

    /**
     * @param string $discountStatus
     * @param bool $includeItems
     * @return array
     */
    private function _getAllDiscount($discountStatus = self::DISCOUNT_STATUS_ALL, $includeItems = false)
    {
        $offset = 0;
        $dataPerPage = self::CONFIG_ENTRIES_PER_PAGE;
        $discountGetParams['pagination_offset'] = $offset;
        $discountGetParams['discount_status'] = $discountStatus;
        $discountGetParams['pagination_entries_per_page'] = $dataPerPage;
        $result = [];
        do {
            $i = 0;
            do {
                $data = $this->execute('/discounts/get', 'POST', $discountGetParams);
                $dataOngoing = $data['discount'] ?? null;
                if ($i++ >= 5) { return []; }
            } while (is_null($dataOngoing) );
            $more = $data['more'];
            if ($includeItems) {
                foreach ($dataOngoing as $discount) {
                    $discount['items'] = [];
                    do {
                        $data = $this->execute('/discount/detail', 'POST', array(
                            'discount_id' => $discount['discount_id'],
                            'pagination_offset' => count($discount['items'])
                        ));
                        $discount['items'] = array_merge($discount['items'], $data['items']);
                    } while ($data['more'] ?? 0);
                    $result[] = $discount;
                }
            } else {
                $result = array_merge($result, $dataOngoing);
            }
            $offset += $dataPerPage;
            $discountGetParams['pagination_offset'] = $offset;
        } while ($more);
        return $result;
    }

    private function _removeItemFromDiscount($discountId, $itemId)
    {
        return $this->execute(
            '/discount/item/delete',
            'POST',
            [
                'discount_id' => intval($discountId),
                'item_id' => intval($itemId)
            ]
        );
    }

    private function _removeFromAllDiscount($itemId)
    {
        $key = Application_Cache_Service::getInstance()->getAllShopeeDiscount($this->shopId, $this->channelId);
        $data = Application_Cache::getInstance()->load($key);
        if (!$data) {
            $dataOngoing = $this->_getAllDiscount(self::DISCOUNT_STATUS_ONGOING, 1);
            $dataUpcoming = $this->_getAllDiscount(self::DISCOUNT_STATUS_UPCOMING, 1);
            $data = array_merge($dataOngoing, $dataUpcoming);
            Application_Cache::getInstance()->save($data, $key, 60 * 60 );
        }
        $result = [];
        foreach ((array)$data as $discount) {
            foreach ((array)$discount['items'] as $discountItem) {
                if ($itemId == ($discountItem['item_id'] ?? 0)) {
                    $result[] = json_encode($this->_removeItemFromDiscount($discount['discount_id'], $itemId));
                }
            }
        }
        if (Application_Env::isDev()) {
            print_r($result);
        }
    }

    /**
     * @param $discountId
     * @throws Exception
     */
    public function getValidDiscount($discountId) {
        $data = $this->execute('/discount/detail', 'POST', array('discount_id' => $discountId));
        if (($data['end_time'] ?? 0) < strtotime('now')) {
            $discount = $this->execute('/discount/add','POST', array(
                'discount_name' => trim(Application_Env::shopeeDiscountName()),
                'start_time' => strtotime('+60 second'),
                'end_time' => strtotime('+ 1 year'),
            ));
            $this->discountId = $discount['discount_id'] ?? 0;
        }
        $this->saveDiscountId();
    }

    /**
     * @param float $price
     * @param string $sku
     * @return Application_Response_Channel
     * @throws Exception
     */
    public function syncOnsitePriceV2($price, $sku)
    {
        $id = intval($this->_getShopProductChannelIdentify($sku));
        $priceInfo = Model_ProductShopChannel::getInstance()->getAdjustmentPrice(
            $sku,
            $this->shopId,
            $this->channelId
        );
        $equalRRP = trim($price) == trim($priceInfo[DbTable_Price_Adjustment::COL_PRICE_ADJUSTMENT_NEW] ?? 0);
        $result = new Application_Response_Channel();
        $this->getValidDiscount($this->discountId);
        $this->_removeItemFromDiscount($this->discountId, $id);
        if ($equalRRP) {
            $this->_removeFromAllDiscount($id);
        } else {
            $discount = $this->execute(
                '/discount/items/add',
                'POST',
                array(
                    'discount_id' => $this->discountId,
                    'items' => array(array(
                        'item_id' => intval($id),
                        'item_promotion_price' => floatval($price),
                        'purchase_limit' => 999,
                    ))
                )
            );
            if (!($discount['count'] ?? '')) {
                $result->setIsSuccess(false);
                $this->_removeFromAllDiscount($id);
            }
        }
        return $result;
    }

    /**
     * @param float $price
     * @param string $sku
     * @return Application_Response_Channel
     * @throws Exception
     */
    public function syncOnsitePrice($price, $sku)
    {
        $id = intval($this->_getShopProductChannelIdentify($sku));
        $priceInfo = Model_ProductShopChannel::getInstance()->getAdjustmentPrice(
            $sku,
            $this->shopId,
            $this->channelId
        );
        $equalRRP = trim($price) == trim($priceInfo[DbTable_Price_Adjustment::COL_PRICE_ADJUSTMENT_NEW] ?? 0);
        $result = new Application_Response_Channel();
        if ($this->discountId) {
            $this->_removeItemFromDiscount($this->discountId, $id);
            if ($equalRRP) {
                $this->_removeFromAllDiscount($id);
            } else {
                $discount = $this->execute(
                    '/discount/items/add',
                    'POST',
                    array(
                        'discount_id' => $this->discountId,
                        'items' => array(array(
                            'item_id' => intval($id),
                            'item_promotion_price' => floatval($price),
                            'purchase_limit' => 999,
                        ))
                    )
                );
                if (!($discount['count'] ?? '')) {
                    $result->setIsSuccess(false);
                    $this->_removeFromAllDiscount($id);
                }
            }
        } else {
            if (!$equalRRP) {
                $discount = $this->execute('/discount/add','POST', array(
                        'discount_name' => trim($id),
                        'start_time' => strtotime('+60 second'),
                        'end_time' => strtotime('+ 1 year'),
                        'items' => array(
                            array(
                                'item_id' => intval($id),
                                'item_promotion_price' => floatval($price),
                                'purchase_limit' => 999,
                            )
                        )
                    )
                );
                $this->discountId = $discount['discount_id'] ?? 0;
                $this->saveDiscountId();
                if ($discount['count'] ?? '') {
                    $result->setIsSuccess(false);
                    $this->_removeFromAllDiscount($id);
                }
            } else {
                $this->_removeFromAllDiscount($id);
            }
        }
        return $result;
    }

    /**
     * Sync stock to channel
     * @param float $stock
     * @param string $sku
     * @return Application_Response_Channel
     */
    public function syncStock($stock, $sku)
    {
        $itemId = intval($this->_getShopProductChannelIdentify($sku));
        $dataUpdate = array(
            'item_id' => $itemId,
            'stock' => intval($stock)
        );
        $result = new Application_Response_Channel();
        $response = $this->execute('/items/update_stock', 'POST', $dataUpdate);
        if (isset($response['error']) || !$response) {
            $result->setIsSuccess(false);
        }
        return $result;
    }

    /**
     * @param string $shopSku
     * @return int|null
     */
    private function _getShopProductChannelIdentify($shopSku)
    {
        $data = $this->_getItemShopSkuMapping();
        if (!isset($data[$shopSku])) {
            $data = $this->_getItemShopSkuMapping(true);
        }
        return empty($data[$shopSku]) ? null : $data[$shopSku];
    }

    /**
     * @param bool $force
     * @return false|mixed
     */
    public function _getItemShopSkuMapping($force = false)
    {
        $key = Application_Cache_Service::getInstance()->shopeeHashMapByChannel($this->getChannelId(), $this->getShopId());
        $hashShopSkuToItem = Application_Cache::getInstance()->load($key);
        if ($force || !$hashShopSkuToItem) {
            $numTry = 0;
            do {
                $response = $this->_getItemList($numTry * 100);
                $itemList = $response['items'];
                if (is_array($itemList)) {
                    foreach ($itemList as $item) {
                        #$itemDetailResponse = $this->_getItemDetail($item['item_id']);
                        #$itemDetail = $itemDetailResponse['item'];
                        if ($item['status'] == 'DELETED' || $item['status'] == 'BANNED'){
                            // ignore delete/banned items
                            continue;
                        }
                        $hashShopSkuToItem[$item['item_sku']] = $item['item_id'];
                    }
                }
                $numTry++;
            } while ($response['more']);
            Application_Cache::getInstance()->save($hashShopSkuToItem, $key, null);
        }
        return $hashShopSkuToItem;
    }

    /**
     * get Item detail
     * @param $itemId
     * @return array
     */
    protected function _getItemDetail($itemId)
    {
        $itemId = intval($itemId);
        $params = [
            'item_id' => $itemId
        ];
        $response = $this->execute('/item/get', 'POST', $params);
        return $response;
    }

    /**
     * get Item list
     * @param $offset
     * @return array
     */
    protected function _getItemList($offset)
    {
        $offset = intval($offset);
        $params = [
            'pagination_offset' => $offset,
            'pagination_entries_per_page' => 100
        ];
        $response = $this->execute('/items/get', 'POST', $params);
        return $response;
    }

    /**
     * @param $orderNumber
     * @return bool
     * @throws Exception
     */
    public function convertStatusToEpsilo($orderNumber)
    {
        $orderEpsiloStatus = null;
        $force = true;
        $saleOrder = Application_Registry::loadSaleOrder(
            $orderNumber, $this->getShopId(), $this->getChannelId(), $force
        );

        $dataSaleOrderWarehouse = Crontab_Model_SaleOrderWarehouse::getInstance()->getBySaleOrderId(
            $saleOrder[DbTable_Sale_Order::COL_SALE_ORDER_ID]
        );

        $isSendWarehouse = array_filter(array_column(
            $dataSaleOrderWarehouse,
            DbTable_Sale_Order_Warehouse::COL_SALE_ORDER_WAREHOUSE_REF_CODE
        ));
        $venture = $saleOrder[DbTable_Shop_Master::COL_FK_VENTURE];
        $allChannel = Model_Channel::getInstance()->getAllByVenture(
            $venture,
            Application_Constant_Db_Channel::INCLUDE_ON_OFF_LINE
        );
        $assocChannelIdCode = Application_Function_Array::buildArrayInKeyAttribute(
            $allChannel,
            DbTable_Channel::COL_CHANNEL_ID,
            DbTable_Channel::COL_CHANNEL_CODE
        );
        $shopeeStatus = array_filter(
            Model_SaleOrderChannelStatus::getInstance()->getAll(),
            function ($item) use ($assocChannelIdCode) {
                $channelId = $item[DbTable_Sale_Order_Channel_Status::COL_FK_CHANNEL];
                if (isset($assocChannelIdCode[$channelId])) {
                    $code = strtoupper($assocChannelIdCode[$channelId]);
                    if ($code == Application_Constant_Db_Channel::SHOPEE_CODE) {
                        return true;
                    }
                }

                return false;
            }
        );
        /*$assocShopeeStatusIdName = Application_Function_Array::buildArrayInKeyAttribute(
            $shopeeStatus,
            DbTable_Sale_Order_Channel_Status::COL_SALE_ORDER_CHANNEL_STATUS_ID,
            DbTable_Sale_Order_Channel_Status::COL_SALE_ORDER_CHANNEL_STATUS_NAME
        );
        $channelStatus = $saleOrder[DbTable_Sale_Order::COL_FK_SALE_ORDER_CHANNEL_STATUS];*/

        $itemEpsiloStatus = null;
        /*if (!isset($assocShopeeStatusIdName[$channelStatus])) {
            Application_Notification_Strategy::getInstance()->setSlave(
                new Application_Notification_Screen()
            );

            Application_Notification_Strategy::getInstance()->alert(
                'status not exit: '.$channelStatus
            );
        }*/
        #$channelStatusName = strtoupper($assocShopeeStatusIdName[$channelStatus] ?? '');
        $channelStatusName = strtoupper($saleOrder[DbTable_Sale_Order::COL_SALE_ORDER_STATUS]);
        $unpaid = $channelStatusName == 'UNPAID';
        $retryShip = $channelStatusName == 'RETRY_SHIP';
        $readyToShip = $channelStatusName == 'READY_TO_SHIP';
        $cancelled = $channelStatusName == 'CANCELLED';
        $toConfirmReceive = $channelStatusName == 'TO_CONFIRM_RECEIVE';
        $shipped = $channelStatusName == 'SHIPPED';
        $completed = $channelStatusName == 'COMPLETED';
        $returned = $channelStatusName == 'TO_RETURN';
        $cancelRequest = $channelStatusName == 'IN_CANCEL';
        $isHasTrackingNumber = $saleOrder[DbTable_Sale_Order::COL_SALE_ORDER_TRACKING_NUMBER];

        #Pending
        if ($retryShip || $unpaid || ($readyToShip && !$isHasTrackingNumber)) {
            $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::PENDING;
        }

        if ($readyToShip && $isHasTrackingNumber) {

            if ($isSendWarehouse) {
                $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::READY_TO_SHIP;
            } else {
                $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::PENDING;
            }
        }

        if ($cancelled) {
            $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::CANCELED;
        }

        if ($toConfirmReceive || $shipped) {
            $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::SHIPPED;
        }

        if ($completed) {
            $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::DELIVERED;
        }

        if ($returned) {
            $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::RETURNED;
        }

        if ($cancelRequest) {
            $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::BEING_CANCEL;
        }

        if (!$itemEpsiloStatus) {
            $itemEpsiloStatus = Application_Constant_Db_SaleOrder_ItemEpsiloStatus::UNKNOWN;
        }

        if ($itemEpsiloStatus) {
            $detailItemsArray = Application_Registry::loadSaleOrderItemDetail(
                $orderNumber, $this->getShopId(), $this->getChannelId(), $force
            );

            $itemsArray = Application_Registry::loadSaleOrderItem(
                $orderNumber, $this->getShopId(), $this->getChannelId(), $force
            );
            $arrayItemId = array_column($itemsArray, DbTable_Sale_Order_Item::COL_SALE_ORDER_ITEM_ID);
            Model_SaleOrderItem::getInstance()->updateEpsiloStatus(
                $arrayItemId,
                $itemEpsiloStatus
            );
            $arrayItemDetailId = array_column(
                $detailItemsArray, DbTable_Sale_Order_Item_Detail::COL_SALE_ORDER_ITEM_DETAIL_ID
            );
            Model_SaleOrderItemDetail::getInstance()->updateEpsiloStatus(
                $arrayItemDetailId,
                $itemEpsiloStatus
            );

            $orderEpsiloStatus = Application_Factory_Channel_Manager::chooseOrderEpsiloStatus([$itemEpsiloStatus]);
            Model_SaleOrder::getInstance()->updateEpsiloStatus(
                $saleOrder[DbTable_Sale_Order::COL_SALE_ORDER_ID],
                $orderEpsiloStatus
            );

        }

        return $orderEpsiloStatus;
    }

    /**
     * @param $price
     * @param $sku
     * @return Application_Response_Channel
     */
    public function syncRRP($price, $sku)
    {
        $itemId = intval($this->_getShopProductChannelIdentify($sku));
        $price = floatval($price);
        $data = [
            'item_id' => $itemId, 'price' => $price
        ];
        $response = $this->execute('/items/update_price', 'POST', $data);
        $result = new Application_Response_Channel();
        if (isset($response['error']) || !$response) {
            $result->setIsSuccess(false);
        }
        return $result;
    }

    public function nextOffset($offset, $limit)
    {
        return $offset + $limit;
    }

    /**
     * @param $rrp
     * @param $postSub
     * @param $sku
     * @return Application_Response_Channel
     * @throws Exception
     */
    public function syncPrice($rrp, $postSub, $sku)
    {
        $result = new Application_Response_Channel();
        $id = intval($this->_getShopProductChannelIdentify($sku));
        $this->getValidDiscount($this->discountId);
        #$this->_removeItemFromDiscount($this->discountId, $id);
        $this->_removeFromAllDiscount($id);
        $equalRRP = trim($rrp) == trim($postSub);
        $response = $this->updateItemPrice($id, $rrp);
        $result->setIsSuccess(($response['item']['modified_time'] ?? null) ? true : false);
        if ($equalRRP) {
            return $result;
        }
        $discount = $this->addDiscount($this->discountId, $id, 0, $postSub);
        if (!($discount['count'] ?? '')) {
            $result->setIsSuccess(false);
            $this->_removeFromAllDiscount($id);
        }else{
            $result->setIsSuccess(true);
        }
        return $result;
    }

    /**
     * @param $itemId
     * @param $price
     * @return array
     */
    protected function updateItemPrice($itemId, $price)
    {
        $itemId = intval($itemId);
        $price = floatval($price);
        $params = array('item_id' => $itemId, 'price' => $price);
        return $this->execute('/items/update_price', 'POST',$params);
    }

    protected function addDiscount($discountId, $itemId, $variationId, $price)
    {
        $item = array(
            'item_id' => intval($itemId),
            'purchase_limit' => 999,
        );
        if (!$variationId) {
            $item['item_promotion_price'] = floatval($price);
        } else {
            $item['variations'] = array(
                array(
                    'variation_id' => intval($variationId),
                    'variation_promotion_price' => floatval($price),
                )
            );
        }

        $params = array(
            'discount_id' => $discountId,
            'items' => array($item)
        );
        return $this->execute('/discount/items/add', 'POST', $params);
    }

    /**
     * @param $itemId
     * @return array
     */
    public function getItemDetail($itemId)
    {
        $params = array(
            'item_id' => $itemId
        );
        return $this->execute('/item/get', 'POST', $params);
    }

    /**
     * @param array $arraySku
     * @return array
     */
    public function pullProduct($arraySku)
    {
        $numTry = 0;
        $result = [];
        do {
            $response = $this->_getItemList($numTry * 100);
            $itemList = $response['items'];
            if (is_array($itemList)) {
                foreach ($itemList as $item) {
                    if (in_array($item['item_sku'], $arraySku)) {
                        $itemDetailResponse = $this->_getItemDetail($item['item_id']);
                        if ($itemDetailResponse) {
                            $result[$item['item_sku']] = [
                                'postSub' => $itemDetailResponse['item']['price'],
                                'rrp' => $itemDetailResponse['item']['original_price'],
                                'stock' => $itemDetailResponse['item']['stock'] ?? 0
                            ];
                        }
                    }
                }
            }
            $numTry++;
        } while ($response['more']);
        return $result;
    }

    public function pullAllProduct($updateFrom, $updateTo)
    {
        $numTry = 0;
        $result = [];
        do {
            $offset = intval($numTry * 100);
            $params = [
                'pagination_offset' => $offset,
                'pagination_entries_per_page' => 100
            ];
            if ($updateFrom) {
                $params['update_time_from'] = $updateFrom;
            }
            if ($updateTo) {
                $params['update_time_to'] = $updateTo;
            }
            $response = $this->execute('/items/get', 'POST', $params);
            $itemList = $response['items'];
            if (is_array($itemList)) {
                foreach ($itemList as $item) {
                    $itemDetailResponse = $this->_getItemDetail($item['item_id']);
                    if ($itemDetailResponse) {
                        $channelProduct = new Library\DataStructure\ChannelProduct();

                        $channelProduct->setSku($itemDetailResponse['item']['item_sku']);
                        #$channelProduct->setActive($itemDetailResponse['item']['item_sku']);
                        $channelProduct->setAllocationStock($itemDetailResponse['item']['stock']);
                        $channelProduct->setFbxStock(null);
                        $channelProduct->setChannelIdentify($itemDetailResponse['item']['item_id']);
                        $channelProduct->setName($itemDetailResponse['item']['name']);
                        $channelProduct->setRrp($itemDetailResponse['item']['original_price']);
                        $channelProduct->setSellingPrice($itemDetailResponse['item']['price']);
                        $channelProduct->setChannelCategory($itemDetailResponse['item']['category_id']);
                        $channelProduct->setCreatedAt($itemDetailResponse['item']['create_time']);
                        $channelProduct->setUpdatedAt($itemDetailResponse['item']['update_time']);
                        $channelProduct->setRawData(json_encode($itemDetailResponse));
                        $result[] = $channelProduct;
                    }
                }
            }
            $numTry++;
            if (!isset($response['more'])) {
                $response['more'] = false;
            }
        } while ($response['more']);
        return $result;
    }

    /**
     * @return array
     */
    public function pullAllCategory()
    {
        $result = [];
        $data = $this->execute('/item/categories/get', 'POST', []);
        $data = $data['categories'];
        $mappingIdName = array_column($data, 'category_name', 'category_id');
        #init category level 5
        foreach ($data as $item) {
            if (!$item['has_children']) {
                $channelIdentify = $item['category_id'];
                if (!isset($result[$channelIdentify])) {
                    $obj = new Library\DataStructure\ChannelCategory($channelIdentify);
                    $obj->setCategoryByLevel(5, $item['category_id']);
                    $result[$channelIdentify] = $obj;
                }
            }
        }

        #init category level 4
        foreach ($result as $channelIdentify => $obCategory) {
            /** @var Library\DataStructure\ChannelCategory $obCategory */
            foreach ($data as $item) {
                if ($obCategory->getCategoryByLevel(5) == $item['category_id']) {
                    foreach ($data as $item2) {
                        if ($item2['category_id'] == $item['parent_id']) {
                            $obCategory->setCategoryByLevel(4, $item2['category_id']);
                            break;
                        }
                    }

                    break;
                }
            }
        }

        #init category level 3
        foreach ($result as $channelIdentify => $obCategory) {
            /** @var Library\DataStructure\ChannelCategory $obCategory */
            foreach ($data as $item) {
                if ($obCategory->getCategoryByLevel(4) == $item['category_id']) {
                    foreach ($data as $item2) {
                        if ($item2['category_id'] == $item['parent_id']) {
                            $obCategory->setCategoryByLevel(3, $item2['category_id']);
                            break;
                        }
                    }

                    break;
                }
            }
        }

        #init category level 2
        foreach ($result as $channelIdentify => $obCategory) {
            /** @var Library\DataStructure\ChannelCategory $obCategory */
            foreach ($data as $item) {
                if ($obCategory->getCategoryByLevel(3) == $item['category_id']) {
                    foreach ($data as $item2) {
                        if ($item2['category_id'] == $item['parent_id']) {
                            $obCategory->setCategoryByLevel(2, $item2['category_id']);
                            break;
                        }
                    }

                    break;
                }
            }
        }

        #init category level 1
        foreach ($result as $channelIdentify => $obCategory) {
            /** @var Library\DataStructure\ChannelCategory $obCategory */
            foreach ($data as $item) {
                if ($obCategory->getCategoryByLevel(2) == $item['category_id']) {
                    foreach ($data as $item2) {
                        if ($item2['category_id'] == $item['parent_id']) {
                            $obCategory->setCategoryByLevel(1, $item2['category_id']);
                            break;
                        }
                    }

                    break;
                }
            }
        }

        foreach ($result as $key => $value) {
            for ($i = 1; $i <=5 ; $i++) {
                $result[$key]->setCategoryByLevel($i, $mappingIdName[$result[$key]->getCategoryByLevel($i)] ?? '');
            }
        }

        foreach ($result as $key => $value) {
            $value->adjustCategory();
            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getBoostedItems()
    {
        return  $this->execute('/items/get_boosted', 'POST', []);
    }
}