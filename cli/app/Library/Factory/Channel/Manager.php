<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 17:29
 */

namespace App\Library\Factory\Channel;


use App\Library;
use App\Models;
use Exception;


class Manager
{
    /**
     * @var Library\Factory\Channel\IChannel $handler
     */
    protected $handler;

    /**
     * @var int
     */
    protected $shopId;

    /**
     * @var int
     */
    protected $channelId;

    /**
     * Manager constructor.
     * @param $shopId
     * @param $channelId
     * @throws Exception
     */
    public function __construct($shopId, $channelId)
    {
        $this->setShopId($shopId);
        $this->setChannelId($channelId);
        $channelCode = null;
        $allChannel = Library\Formater::stdClassToArray(Models\Channel::getAll());
        foreach ($allChannel as $channel) {
            if ($this->getChannelId() == $channel[Models\Channel::COL_CHANNEL_ID]) {
                $channelCode = $channel[Models\Channel::COL_CHANNEL_CODE];
                break;
            }
        }
        $channelCodeFormatted = ucfirst(strtolower($channelCode));

        try {
            $this->handler = Library\Factory\Channel::build($channelCodeFormatted);
        } catch (Exception $exception) {
            throw new Exception('Channel Info not found');
        }


        $shopInfo = Library\Formater::stdClassToArray(Models\ShopMaster::getById($shopId));
        if (!$shopInfo) {
            throw new Exception('shop Info not found');
        }

        $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($this->getShopId(), $this->getChannelId()));
        if (!$shopChannelInfo) {
            throw new Exception('Shop and channel be not linked');
        }

        $credential = $shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_API_CREDENTIAL];

        $this->handler->setCredential($credential);
        $this->handler->setShopId($this->getShopId());
        $this->handler->setChannelId($this->getChannelId());
    }

    /**
     * @return int
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * @return int
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param int $channelId
     */
    protected function setChannelId($channelId)
    {
        $this->channelId = $channelId;
    }

    /**
     * @param int $updateAtFrom
     * @param int $updateAtTo
     * @return Library\Response\Channel
     * @throws Exception
     */
    public function pullOrder($updateAtFrom, $updateAtTo)
    {
        $this->handler->setShopId($this->getShopId());
        $this->handler->setChannelId($this->getChannelId());
        $result = new Library\Response\Channel();
        $offset = 0;
        $orders = [];
        do {
            $data = $this->handler->pullOrder(
                $updateAtFrom,
                $updateAtTo,
                env('PULL_SALE_ORDER_LIMIT_PER_TURN'),
                $offset
            );
            $orders = array_merge($orders, $data->getOrders());
            $isMore = count($data->getOrders()) == env('PULL_SALE_ORDER_LIMIT_PER_TURN');
            $isMore = $data->more ?? $isMore;
            if ($isMore) {
                $offset = $this->handler->nextOffset(
                    $offset,
                    env('PULL_SALE_ORDER_LIMIT_PER_TURN')
                );
            }
        } while ($isMore);
        $result->setOrders($orders);
        return $result;
    }

    /**
     * @param \stdClass|Library\DataStructure\ChannelOrder $order
     * @return string
     */
    private function _buildUniqueString($order)
    {
        $result = '';
        if ($order instanceof Library\DataStructure\ChannelOrder) {
            $arrayItems = $order->getArrayItem();
            usort($arrayItems, function ($a, $b) {
                /** @var Library\DataStructure\ChannelOrder\Item $a */
                /** @var Library\DataStructure\ChannelOrder\Item $b */
                return $a->getInternalId() <=> $b->getInternalId();
            });
            foreach ($arrayItems as $item) {
                /** @var Library\DataStructure\ChannelOrder\Item $item */
                $result .= $item->getInternalId() . '|' . $item->getStatusId() . '|';
            }
        } else {
            $arrayItems = $order->data->arrayItem;

            usort($arrayItems, function ($a, $b) {
                return $a->internalId <=> $b->internalId;
            });
            foreach ($arrayItems as $item) {
                $result .= $item->internalId . '|' . $item->statusId . '|';
            }
        }
        return $result;
    }

    /**
     * @param int $updateAtFrom
     * @param int $updateAtTo
     * @throws Exception
     */
    public function import($updateAtFrom, $updateAtTo)
    {
        $this->handler->setShopId($this->getShopId());
        $this->handler->setChannelId($this->getChannelId());
        $offset = 0;
        do {
            $data = $this->handler->pullOrder(
                $updateAtFrom,
                $updateAtTo,
                env('PULL_SALE_ORDER_LIMIT_PER_TURN'),
                $offset
            );

            $orderNumberChannel = [];
            foreach ($data->getOrders() as $row) {
                /** @var Library\DataStructure\ChannelOrder $row */
                $orderNumberChannel[] = trim($row->getOrderNumber());
            }

            $saleOrderDB = Models\Mongo\Manager::getInstance()->select(
                'sale_order',
                [
                    'shopId' => intval($this->getShopId()),
                    'channelId' => intval($this->getChannelId()),
                    'orderNumber' => ['$in' => $orderNumberChannel]
                ],
                []
            )->toArray();

            $assocOrderDatabaseUniqueString = [];
            $orderNumberUpdate = [];
            foreach ($saleOrderDB as $saleOrder) {
                $orderNumber = $saleOrder->orderNumber;
                if (!in_array($orderNumber, $orderNumberUpdate)) {
                    $orderNumberUpdate[] = $orderNumber;
                    $assocOrderDatabaseUniqueString[$orderNumber] = $this->_buildUniqueString($saleOrder);
                }
            }

            $saleOrderArray = [];
            $assocOrderPullUniqueString = [];
            foreach ($data->getOrders() as $row) {
                /** @var Library\DataStructure\ChannelOrder $row */
                $items = [];
                $importAt = 0;
                $assocOrderPullUniqueString[$row->getOrderNumber()] = $this->_buildUniqueString($row);
                foreach ($row->getArrayItem() as $item) {
                    /** @var Library\DataStructure\ChannelOrder\Item $item */
                    $_sku = $item->getSku();
                    $_itemId = $item->getItemId();
                    if (!$_sku) {
                        $_sku = $_itemId;
                    }
                    $items[] = [
                        'sku'   => trim($_sku),
                        'name'  => trim($item->getName()),
                        'channelSku'   => trim($item->getChannelSku()),
                        'itemPrice'    => floatval($item->getItemPrice()),
                        'voucherCode'  => trim($item->getVoucherCode()),
                        'voucherAmount' => floatval($item->getVoucherAmount()),
                        'itemId'        => trim($item->getItemId()),
                        'createdAt'     => intval($item->getCreatedAt()),
                        'updatedAt'     => intval($item->getUpdatedAt()),
                        'statusId'      => intval($item->getStatusId()),
                        'dateString'    => trim($item->getDateString()),
                        'shipmentProvider'   => trim($item->getShipmentProvider()),
                        'fullJson'           => trim($item->getFullJson()),
                        'internalId'         => trim($item->getInternalId()),
                        'shippingType'       => trim($item->getShippingType()),
                        'channelDeliveredAt' => intval($item->getChannelDeliveredAt())
                    ];

                    if ($item->getUpdatedAt() && $importAt < $item->getUpdatedAt()) {
                        $importAt = $item->getUpdatedAt();
                    }
                }

                $orderNumber = trim($row->getOrderNumber());
                $_data = [
                    'orderNumber' => $orderNumber,
                    'orderId' => trim($row->getOrderId()),
                    'voucherAmount' => floatval($row->getVoucherAmount()),
                    'voucherCode' => trim($row->getVoucherCode()),
                    'totalPrice' => floatval($row->getTotalPrice()),
                    'createdAt' => $row->getCreatedAt(),
                    'status' => trim($row->getStatus()),
                    'addressCity' => trim($row->getAddressCity()),
                    'addressPhone' => trim($row->getAddressPhone()),
                    'fullJson' => trim($row->getFullJson()),
                    'channelStatusId' => intval($row->getChannelStatusId()),
                    'arrayItem' => $items
                ];
                if (!$importAt) {
                    $importAt = Library\Common::getCurrentTimestamp();
                }
                if (in_array($orderNumber, $orderNumberUpdate)) {
                    if ($assocOrderPullUniqueString[$orderNumber] != $assocOrderDatabaseUniqueString[$orderNumber]) {
                        Models\Mongo\Manager::getInstance()->update(
                            'sale_order',
                            [
                                'orderNumber' => $orderNumber,
                                'shopId'      => intval($this->getShopId()),
                                'channelId'   => intval($this->getChannelId()),
                            ],
                            [
                                'updateAt'    => intval($importAt),
                                'data'        => $_data,
                                'is_imported' => 0
                            ],
                            []
                        );
                    }

                } else {
                    $saleOrderArray[] = [
                        'orderNumber' => $orderNumber,
                        'shopId'      => intval($this->getShopId()),
                        'channelId'   => intval($this->getChannelId()),
                        'updateAt'    => intval($importAt),
                        'data'        => $_data

                    ];
                }
            }

            if ($saleOrderArray) {
                Models\Mongo\Manager::getInstance()->batchInsert(
                    'sale_order',
                    $saleOrderArray
                );
            }

            $isMore = count($data->getOrders()) == env('PULL_SALE_ORDER_LIMIT_PER_TURN');
            if ($isMore) {
                $offset = $this->handler->nextOffset(
                    $offset,
                    env('PULL_SALE_ORDER_LIMIT_PER_TURN')
                );
            }
        } while ($isMore);
    }

    /**
     * @param int $updateAtFrom
     * @param int $updateAtTo
     * @return Library\Response\Channel
     * @throws Exception
     */
    public function exportOrder($updateAtFrom, $updateAtTo)
    {
        $this->handler->setShopId($this->getShopId());
        $this->handler->setChannelId($this->getChannelId());
        $result = new Library\Response\Channel();
        $orders = [];

        $saleOrderData = Models\Mongo\Manager::select(
            'sale_order',
            [
                'shopId' => intval($this->getShopId()),
                'channelId' => intval($this->getChannelId()),
                'updateAt' => ['$gte' => intval($updateAtFrom), '$lte' => intval($updateAtTo)],
                '$or' => [
                    ['is_imported' => 0],
                    ['is_imported' => ['$exists' => 0]]
                ]
            ],
            [],
            [
                'sort' => [
                    'updateAt' => 1
                ]
            ]
        );
        foreach ($saleOrderData as $saleOrderInfo) {
            $order = Library\Formater::stdClassToArray($saleOrderInfo->data);

            $orderObject = new Library\DataStructure\ChannelOrder();
            $orderObject->setOrderNumber($order['orderNumber']);
            $orderObject->setOrderId($order['orderId']);
            $orderObject->setVoucherAmount($order['voucherAmount']);
            $orderObject->setVoucherCode($order['voucherCode']);
            $orderObject->setTotalPrice($order['totalPrice']);
            $orderObject->setCreatedAt($order['createdAt']);
            $orderObject->setStatus($order['status']);
            $orderObject->setAddressCity($order['addressCity']);
            $orderObject->setAddressPhone($order['addressPhone']);
            $orderObject->setFullJson($order['fullJson']);
            $orderObject->setChannelStatusId($order['channelStatusId']);

            $arrayItems = [];
            foreach ($order['arrayItem'] as $item) {
                $itemObject = new Library\DataStructure\ChannelOrder\Item();
                $itemObject->setSku($item['sku']);
                $itemObject->setName($item['name']);
                $itemObject->setChannelSku($item['channelSku']);
                $itemObject->setItemPrice($item['itemPrice']);;
                $itemObject->setVoucherCode($item['voucherCode']);
                $itemObject->setVoucherAmount($item['voucherAmount']);
                $itemObject->setItemId($item['itemId']);
                $itemObject->setCreatedAt($item['createdAt']);
                $itemObject->setUpdatedAt($item['updatedAt']);
                $itemObject->setStatusId($item['statusId']);
                $itemObject->setDateString($item['dateString']);
                $itemObject->setShipmentProvider($item['shipmentProvider']);
                $itemObject->setFullJson($item['fullJson']);
                $itemObject->setInternalId($item['internalId']);
                $itemObject->setShippingType($item['shippingType']);
                $itemObject->setShippingType($item['shippingType']);
                $itemObject->setChannelDeliveredAt($item['channelDeliveredAt']);

                $arrayItems[] = $itemObject;
            }
            $orderObject->setArrayItem($arrayItems);

            $orders[] = $orderObject;
        }
        $result->setOrders($orders);
        return $result;
    }

    /**
     * @param array $orderInfo
     * @param array $arrayItem
     * @return Library\Response\Channel
     */
    public function confirmed($orderInfo, $arrayItem)
    {
        $this->handler->setShopId($this->getShopId());
        $this->handler->setChannelId($this->getChannelId());
        return $this->handler->confirmed($orderInfo, $arrayItem);
    }

    /**
     * @param $orderNumber
     * @return Library\Response\Channel
     */
    public function cancelOrder($orderNumber)
    {
        $response = new Library\Response\Channel();
        $response->setIsSuccess(false);
        return $response;
    }

    /**
     * @param $arraySku
     * @return array
     */
    public function pullProduct($arraySku)
    {
        $response = $this->handler->pullProduct($arraySku);
        return $response;
    }

    /**
     * @param $updateFrom
     * @param $updateTo
     * @return array
     */
    public function pullAllProduct($updateFrom, $updateTo)
    {
        $data = [];
        if (method_exists($this->handler, 'pullAllProduct')) {
            $data = $this->handler->pullAllProduct($updateFrom, $updateTo);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function pullAllCategory()
    {
        $data = [];
        if (method_exists($this->handler, 'pullAllCategory')) {
            $data = $this->handler->pullAllCategory();
        }

        return $data;
    }

    /**
     * @param Library\Response\Channel $data
     * @param int $shopId
     * @param int $channelId
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public static function handleOrder(Library\Response\Channel $data, $shopId, $channelId)
    {
        $orderNumberChannel = [];
        foreach ($data->getOrders() as $row) {
            /** @var Library\DataStructure\ChannelOrder $row */
            $orderNumberChannel[] = trim($row->getOrderNumber());
        }

        $saleOrderDB = Models\Mongo\Manager::getInstance()->select(
            'sale_order',
            [
                'shopId' => intval($shopId),
                'channelId' => intval($channelId),
                'orderNumber' => ['$in' => $orderNumberChannel]
            ],
            []
        )->toArray();

        $assocOrderDatabaseUniqueString = [];
        $orderNumberUpdate = [];
        foreach ($saleOrderDB as $saleOrder) {
            $orderNumber = $saleOrder->orderNumber;
            if (!in_array($orderNumber, $orderNumberUpdate)) {
                $orderNumberUpdate[] = $orderNumber;
                $assocOrderDatabaseUniqueString[$orderNumber] = self::__buildUniqueString($saleOrder);
            }
        }

        $saleOrderArray = [];
        $assocOrderPullUniqueString = [];
        foreach ($data->getOrders() as $row) {
            /** @var Library\DataStructure\ChannelOrder $row */
            $items = [];
            $importAt = 0;
            $assocOrderPullUniqueString[$row->getOrderNumber()] = self::__buildUniqueString($row);
            foreach ($row->getArrayItem() as $item) {
                /** @var Library\DataStructure\ChannelOrder\Item $item */
                $_sku = $item->getSku();
                $_itemId = $item->getItemId();
                if (!$_sku) $_sku = $_itemId;

                $items[] = [
                    'sku'   => trim($_sku),
                    'name'  => trim($item->getName()),
                    'channelSku'   => trim($item->getChannelSku()),
                    'itemPrice'    => floatval($item->getItemPrice()),
                    'voucherCode'  => trim($item->getVoucherCode()),
                    'voucherAmount' => floatval($item->getVoucherAmount()),
                    'itemId'        => trim($item->getItemId()),
                    'createdAt'     => intval($item->getCreatedAt()),
                    'updatedAt'     => intval($item->getUpdatedAt()),
                    'statusId'      => intval($item->getStatusId()),
                    'dateString'    => trim($item->getDateString()),
                    'shipmentProvider'   => trim($item->getShipmentProvider()),
                    'fullJson'           => trim($item->getFullJson()),
                    'internalId'         => trim($item->getInternalId()),
                    'shippingType'       => trim($item->getShippingType()),
                    'channelDeliveredAt' => intval($item->getChannelDeliveredAt())
                ];

                if ($item->getUpdatedAt() && $importAt < $item->getUpdatedAt()) {
                    $importAt = $item->getUpdatedAt();
                }
            }

            $orderNumber = trim($row->getOrderNumber());
            $_data = [
                'orderNumber' => $orderNumber,
                'orderId' => trim($row->getOrderId()),
                'voucherAmount' => floatval($row->getVoucherAmount()),
                'voucherCode' => trim($row->getVoucherCode()),
                'totalPrice' => floatval($row->getTotalPrice()),
                'createdAt' => $row->getCreatedAt(),
                'status' => trim($row->getStatus()),
                'addressCity' => trim($row->getAddressCity()),
                'addressPhone' => trim($row->getAddressPhone()),
                'fullJson' => trim($row->getFullJson()),
                'channelStatusId' => intval($row->getChannelStatusId()),
                'arrayItem' => $items
            ];

            if (!$importAt) Library\Common::getCurrentTimestamp();

            if (in_array($orderNumber, $orderNumberUpdate)) {
                if ($assocOrderPullUniqueString[$orderNumber] != $assocOrderDatabaseUniqueString[$orderNumber]) {
                    Models\Mongo\Manager::getInstance()->update(
                        'sale_order',
                        [
                            'orderNumber' => $orderNumber,
                            'shopId'      => intval($shopId),
                            'channelId'   => intval($channelId),
                        ],
                        [
                            'updateAt'    => intval($importAt),
                            'data'        => $_data,
                            'is_imported' => 0
                        ],
                        []
                    );
                }
            } else {
                $saleOrderArray[] = [
                    'orderNumber' => $orderNumber,
                    'shopId'      => intval($shopId),
                    'channelId'   => intval($channelId),
                    'updateAt'    => intval($importAt),
                    'data'        => $_data

                ];
            }
        }

        if ($saleOrderArray) {
            Models\Mongo\Manager::getInstance()->batchInsert(
                'sale_order',
                $saleOrderArray
            );
        }
    }

    /**
     * @param \stdClass|Library\DataStructure\ChannelOrder $order
     * @return string
     */
    private static function __buildUniqueString($order)
    {
        $result = '';
        if ($order instanceof Library\DataStructure\ChannelOrder) {
            $arrayItems = $order->getArrayItem();
            usort($arrayItems, function ($a, $b) {
                /** @var Library\DataStructure\ChannelOrder\Item $a */
                /** @var Library\DataStructure\ChannelOrder\Item $b */
                return $a->getInternalId() <=> $b->getInternalId();
            });
            foreach ($arrayItems as $item) {
                /** @var Library\DataStructure\ChannelOrder\Item $item */
                $result .= $item->getInternalId() . '|' . $item->getStatusId() . '|';
            }
        } else {
            $arrayItems = $order->data->arrayItem;

            usort($arrayItems, function ($a, $b) {
                return $a->internalId <=> $b->internalId;
            });
            foreach ($arrayItems as $item) {
                $result .= $item->internalId . '|' . $item->statusId . '|';
            }
        }
        return $result;
    }

    /**
     * @param int $shopId
     * @param int $channelId
     * @param string $orderNumber
     * @param int $index
     * @return string
     */
    public static function genInternalId($shopId, $channelId, $orderNumber, $index)
    {
        return sprintf(
            '%s-%s-%s-%s',
            $shopId,
            $channelId,
            $orderNumber,
            $index
        );
    }

}
