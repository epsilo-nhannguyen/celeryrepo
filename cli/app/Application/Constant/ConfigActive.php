<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 5/6/16
 * Time: 4:32 PM
 */
namespace App\Application\Constant;
interface ConfigActive
{
    /**
     * @var int
     */
    const INACTIVE = 0;

    /**
     * @var int
     */
    const ACTIVE = 1;

    /**
     * @var int
     */
    const TRASH = 2;

}