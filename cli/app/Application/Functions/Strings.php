<?php
namespace App\Application\Functions;
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2/11/15
 * Time: 9:26 AM
 */
class Strings
{
    /**
     * Decode un-expect character from FCK string
     * @param string $value
     * @return mixed
     */
    static public function decodeFCK($value)
    {
        return str_replace('\"', '"', $value);
    }

    /**
     * Convert string without VN
     * @param string $title
     * @param boolean $removeSymbol
     * @param string $replacement
     * @return string
     */
    static function convertNoVn($title, $removeSymbol = true, $replacement = ' ')
    {
        $map = array ();
        $quotedReplacement = preg_quote( $replacement, '/' );
        $default = array (
            '/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|å/' => 'a',
            '/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|ë/' => 'e',
            '/ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|î/' => 'i',
            '/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|ø/' => 'o',
            '/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|ů|û/' => 'u',
            '/ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ/' => 'y',
            '/đ|Đ/' => 'd',
            '/ç/' => 'c',
            '/ñ/' => 'n',
            '/ä|æ/' => 'ae',
            '/ö/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/ß/' => 'ss',
            '/\\s+/' => $replacement,
            sprintf( '/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement ) => ''
        );
        if ($removeSymbol) {
            $default['/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu'] = '';
        }
        $title = urldecode( $title );
        $map = array_merge( $map, $default );
        return  preg_replace( array_keys( $map ), array_values( $map ), $title );
    }

    static function getFormatUrl($p_strString)
    {
        $strResult 	= '';
        $arrReplace = array(' ');
        $arrRemove 	= array(',','.','~','`','!','@','#','$','%','^','*','(',')','_','=','+','[',']','{','}','|','\\',';',':',"'",'"',',','<','>','?','/','*','“','”','–',' -','quot','lsquo','&amp','&','‘');
        $arrPlace	= array('  ', '   ');

        $strResult	= trim($p_strString);
        $strResult 	= self::convertNoVn($strResult);
        $strResult 	= str_replace($arrRemove, "", $strResult);
        $strResult 	= str_replace($arrPlace, " ", $strResult);
        $strResult 	= str_replace($arrReplace, "-", $strResult);

        return strtolower($strResult);
    }

    /**
     * Attach prefix
     * @param string $prefix
     * @param string $string
     * @param string $symbol
     * @return string
     */
    static function attachPrefix($prefix, $string, $symbol='-')
    {
        $prefix = $prefix . $symbol;
        $lengthPrefix = strlen($prefix);
        $part = substr($string, 0, $lengthPrefix);
        if (strtolower($prefix) == strtolower($part)) {
            $string = substr($string, $lengthPrefix);
        }
        $result = $prefix . $string;
        return $result;
    }

    static function truncate($string, $length, $stopanywhere=false) {
        //truncates a string to a certain char length, stopping on a word if not specified otherwise.
        if (strlen($string) > $length) {
            //limit hit!
            $string = substr($string,0,($length -3));
            if ($stopanywhere) {
                //stop anywhere
                $string .= '...';
            } else{
                //stop on a word.
                $string = substr($string,0,strrpos($string,' ')).'...';
            }
        }
        return $string;
    }

    public static function randomString($length = 8)
    {
        $characters = '123456789abcdefghjkmnpqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function camelCaseToSnakeCase($string)
    {
        $filter = new Zend_Filter_Word_CamelCaseToUnderscore();
        return $filter->filter($string);
    }

    /**
     * redirect With Params
     * @param string $url
     * @param array $params
     * @return string
     */
    public static function redirectWithParams($url, $params)
    {
        return '/login/redirect-with-params?s=' . base64_encode(json_encode(
            [
                Application_Constant_Global_RedirectPage::URL => $url,
                Application_Constant_Global_RedirectPage::PARAMS => $params
            ]
        ));
    }

    /**
     * build Key Item Sold
     * @param string $type (PriceAdjustment, PromotionBrandAdjustment, PromotionChannelAdjustment, PromotionSellerAdjustment)
     * @param string $productShopChannelId
     * @param string $action
     * @return string
     */
    public static function buildKeyItemSold($type, $productShopChannelId, $action = 'Create')
    {
        return sprintf('itemSold%s%sIdEqual%s', $action, $type, $productShopChannelId);
    }

    /**
     * build Link Sale Report
     * @param string $createFrom
     * @param string $createTo
     * @param string $client
     * @param string $channel
     * @param string $brand
     * @param string $search
     * @return string
     */
    public static function buildLinkSaleReport($createFrom, $createTo, $client, $channel = '0', $brand = '0', $search = '')
    {
        return sprintf(
            '/order-management/report-product?channel=%s&client=%s&brand=%s&search=%s&createFrom=%s&createTo=%s',
            $channel,
            $client,
            $brand,
            $search,
            $createFrom,
            $createTo
        );
    }

    /**
     * Convert string without typing
     * @param string $title
     * @param boolean $removeSymbol
     * @param string $replacement
     * @return string
     */
    static function convertNoUnicodeCombination($title, $removeSymbol = true, $replacement = ' ')
    {
        $map = array ();
        $quotedReplacement = preg_quote($replacement, '/');
        $default = array (
            '/à|À/' => 'à',
            '/ầ |Ầ/' => 'ầ',
            '/ẵ|Ẵ/' => 'ẵ',

            '/ề |Ề/' => 'ề',

            '/ồ |Ồ/' => 'ồ',

            '/ử|Ử/' => 'ử',

            '/đ|Đ/' => 'd',
            '/ç/' => 'c',
            '/ñ/' => 'n',
            '/ä|æ/' => 'ae',
            '/ö/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/ß/' => 'ss',
            '/\\s+/' => $replacement,
            sprintf( '/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement ) => ''
        );
        if ($removeSymbol) {
            $default['/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu'] = '';
        }
        $map = array_merge( $map, $default );
        return  preg_replace( array_keys( $map ), array_values( $map ), $title );
    }

    public static function strToHex($string)
    {
        $hex = '';
        for ($i=0; $i<strlen($string); $i++){
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0'.$hexCode, -2);
        }
        return strToUpper($hex);
    }

    public static function hexToStr($hex)
    {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
        }
        return $string;
    }

    public static function hideCharacterRandom($string)
    {
        $chars = str_repeat('*', intval(strlen($string) / 2));
        return substr_replace(
            $string,
            $chars,
            rand(0, strlen($string) - 1),
            intval(strlen($string) / 2)
        );
    }

    private static function mask($str, $first, $last)
    {
        $len = strlen($str);
        $toShow = $first + $last;
        return substr($str, 0, $len <= $toShow ? 0 : $first) . str_repeat("*", $len - ($len <= $toShow ? 0 : $toShow)) . substr($str, $len - $last, $len <= $toShow ? 0 : $last);
    }

    public static function maskEmail($email)
    {
        $mail_parts = explode("@", $email);
        $domain_parts = explode('.', $mail_parts[1]);

        $mail_parts[0] = self::mask($mail_parts[0], 2, 1); // show first 2 letters and last 1 letter
        $domain_parts[0] = self::mask($domain_parts[0], 2, 1); // same here
        $mail_parts[1] = implode('.', $domain_parts);

        return implode("@", $mail_parts);
    }

}