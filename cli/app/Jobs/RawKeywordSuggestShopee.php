<?php

namespace App\Jobs;

use App\Library;
use App\Models\MakProgrammaticKeywordProduct;
use App\Models\MakRecommendationKeywordSuggest;
use App\Models\MakRecommendationLogKeywordSuggest;
use App\Models\Mongo\Manager;
use App\Models\ProductShopChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use SebastianBergmann\CodeCoverage\Report\PHP;
use App\Models\MakProgrammaticKeyword;

class RawKeywordSuggestShopee implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if (!$data) return;

//        $shopId = intval($data['shopId']);
//        $channelId = intval($data['channelId']);
        $shopChannelId = intval($data['shopChannelId']);
        $ventureId = intval($data['ventureId']);
        $organizationId = intval($data['organizationId']);

        $dataProduct = ProductShopChannel::searchByShopChannelId($shopChannelId)->toArray();
        $dataKeywords = MakProgrammaticKeyword::searchByOrganizationIdAndVentureId($organizationId, $ventureId)->toArray();
        Library\Common::setTimezoneByVentureId($ventureId);
        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);

        $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();
        foreach ($dataProduct as $product) {
            $itemId = $product[ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ITEM_ID];
            $PSCId = $product[ProductShopChannel::COL_PRODUCT_SHOP_CHANNEL_ID];
            $curlTotal = curl_init($crawler->shopeeSellerDomain() . '/api/v2/pas/suggest/keyword/?SPC_CDS_VER=2&keyword=&itemid=' . $itemId . '&count=100&placement=0');
            curl_setopt($curlTotal, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($curlTotal, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlTotal, CURLOPT_COOKIEFILE, $crawler->cookiePath);
            curl_setopt($curlTotal, CURLOPT_COOKIEJAR, $crawler->cookiePath);
            curl_setopt($curlTotal, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curlTotal, CURLOPT_TIMEOUT, 5);

            $resultTotal = curl_exec($curlTotal);
            curl_close($curlTotal);
            $totalItem = json_decode($resultTotal, true);
            if (!empty($totalItem['data'])) {
                $logKWId = MakRecommendationLogKeywordSuggest::insertGetId([
                    MakRecommendationLogKeywordSuggest::COL_FK_SHOP_CHANNEL => $shopChannelId,
                    MakRecommendationLogKeywordSuggest::COL_FK_PRODUCT_SHOP_CHANNEL => $PSCId,
                    MakRecommendationLogKeywordSuggest::COL_MAK_RECOMMENDATION_LOG_KEYWORD_SUGGEST_CREATED_AT => Library\Common::getCurrentTimestamp(),
                    MakRecommendationLogKeywordSuggest::COL_MAK_RECOMMENDATION_LOG_KEYWORD_SUGGEST_JSON => json_encode($totalItem['data']),
                ]);
                $data = MakRecommendationLogKeywordSuggest::getById($logKWId)->toArray();
                $dataKW = json_decode($data[MakRecommendationLogKeywordSuggest::COL_MAK_RECOMMENDATION_LOG_KEYWORD_SUGGEST_JSON]);
                foreach ($dataKW as $item) {
                    $keyword = trim($item->keyword);
                    $recommendPrice = floatval($item->recommend_price);
                    $searchVolume = $item->search_volume;
                    $relevance = $item->relevance;
                    $type = false;
                    foreach ($dataKeywords as $dataKeyword) {
                        if ($dataKeyword[MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_NAME] === $keyword) {
                            $dataKeywordTrue = $dataKeyword;
                            $type = true;
                        }
                    }
                    try {
                        DB::beginTransaction();
                        if ($type) {
                            $idKW = $dataKeywordTrue[MakProgrammaticKeyword::COL_MAK_PROGRAMMATIC_KEYWORD_ID];
                            $idKWP = MakProgrammaticKeywordProduct::getIdByKP($idKW, $PSCId);
                            if (empty($idKWP)) {
                                $idKWP = MakProgrammaticKeywordProduct::insertGetId($idKW, $PSCId, '', '', '', 2);
                            }else{
                                $idKWP = $idKWP->{MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID};
                            }

                            MakRecommendationKeywordSuggest::updateOrCreate(
                                [MakRecommendationKeywordSuggest::COL_FK_PROGRAMMATIC_KEYWORD_PRODUCT => $idKWP],
                                [
                                    MakRecommendationKeywordSuggest::COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_CREATED_BY => 2,
                                    MakRecommendationKeywordSuggest::COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_CREATED_AT => Library\Common::getCurrentTimestamp(),
                                    MakRecommendationKeywordSuggest::COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_QUALITY => $relevance,
                                    MakRecommendationKeywordSuggest::COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_SEARCH => $searchVolume,
                                    MakRecommendationKeywordSuggest::COL_MAK_RECOMMENDATION_KEYWORD_SUGGEST_PRICE => $recommendPrice,
                                ]
                            );
                        }
                        DB::commit();
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        var_dump($item);
                        dd($ex->getMessage());
                    }
                }
            }
        }
    }

}