<?php


namespace App\Jobs\Shopee\KeywordBidding;


use App\Jobs\Dashboard\Shop\FetchDataMetricIntraday;
use App\Jobs\Dashboard\Shop\ImportMetricDaily;
use App\Library\Crawler\Channel\ShopeeKeywordBidding;
use App\Library\QueueName;
use App\Library\UniversalCurrency;
use App\Models\Currency;
use App\Models\ModelBusiness\MakShopPerformanceHour;
use App\Models\ShopChannel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CrawlShopPerformanceByHour implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var ShopeeKeywordBidding
     */
    protected $shopeeKeywordBidding;
    protected $date;
    protected $timezone;
    protected $shopChannelId;
    public function __construct($shopeeKeywordBidding, $date, $timezone)
    {
        $this->shopeeKeywordBidding = $shopeeKeywordBidding;
        $this->date = $date;
        $this->timezone = $timezone;
    }

    public function handle()
    {
        try {
            date_default_timezone_set($this->timezone);
            $shopChannel = ShopChannel::find($this->shopeeKeywordBidding->shopChannelId);
            $beginDate = "{$this->date} 00:00:00";
            $endDate = "{$this->date} 23:59:59";

            $response = $this->shopeeKeywordBidding->getShopPerformance($beginDate, $endDate);

            $jsonDecode = json_decode($response, true);
            if (!$jsonDecode) {
                return;
            }

            if (!isset($jsonDecode['data'])) {
                return;
            }

            $assocDateHour = [];
            foreach ($jsonDecode['data'] as $row) {
                $_date = date('Y-m-d', $row['timestamp']);
                $_hour = date('H', $row['timestamp']);
                $assocDateHour[$_date][$_hour] = $assocDateHour[$_date][$_hour] ?? [
                    'impression' => 0,
                    'shop_item_impression' => 0,
                    'order_amount' => 0,
                    'click' => 0,
                    'location_in_ads' => 0,
                    'cost' => 0,
                    'shop_item_click' => 0,
                    'order' => 0,
                    'order_gmv' => 0,
                ];

                $assocDateHour[$_date][$_hour] = [
                    'impression' => $assocDateHour[$_date][$_hour]['impression'] + $row['impression'],
                    'shop_item_impression' => $assocDateHour[$_date][$_hour]['shop_item_impression'] + $row['shop_item_impression'],
                    'order_amount' => $assocDateHour[$_date][$_hour]['order_amount'] + $row['order_amount'],
                    'click' => $assocDateHour[$_date][$_hour]['click'] + $row['click'],
                    'location_in_ads' => $assocDateHour[$_date][$_hour]['location_in_ads'] + ($row['location_in_ads'] ?? 0),
                    'cost' => $assocDateHour[$_date][$_hour]['cost'] + $row['cost'],
                    'shop_item_click' => $assocDateHour[$_date][$_hour]['shop_item_click'] + $row['shop_item_click'],
                    'order' => $assocDateHour[$_date][$_hour]['order'] + $row['order'],
                    'order_gmv' => $assocDateHour[$_date][$_hour]['order_gmv'] + $row['order_gmv'],
                ];
            }

            $collection = collect([]);
            foreach ($assocDateHour as $_date => $_hourData) {
                foreach ($_hourData as $_hour => $item) {
                    $collection->push([
                        'impression' => $item['impression'],
                        'shop_item_impression' => $item['shop_item_impression'],
                        'order_amount' => $item['order_amount'],
                        'click' => $item['click'],
                        'location_in_ads' => $item['location_in_ads'] ?? null,
                        'cost' => $item['cost'],
                        'shop_item_click' => $item['shop_item_click'],
                        'order' => $item['order'],
                        'order_gmv' => $item['order_gmv'],
                        'date' => $_date,
                        'hour' => $_hour,
                        'timestamp' => strtotime('now'),
                        'shop_channel_id' => $this->shopeeKeywordBidding->shopChannelId,
                    ]);
                }
            }

            if ($collection->count()) {
                MakShopPerformanceHour::saveMulti($collection);
            }
            $dataPushQueue = [];
            foreach ($collection as $item) {
                $dataPushQueue[$this->date][$item['hour']] = [
                    'impression' => $item['impression'],
                    'click' => $item['click'],
                    'item_sold' => $item['order_amount'],
                    'gmv' => $item['order_gmv'],
                    'cost' => $item['cost'],
                ];
            }
            $assocCurrency = Currency::getAll()->pluck('currency_code', 'currency_id');
            $assocCurrencyDataPushQueue = [];
            foreach ($dataPushQueue as $date => $assocHour) {
                foreach ($assocHour as $hour => $data) {
                    $assocCost = UniversalCurrency::buildCurrencyData(
                        $data['cost'],
                        date('Y-m', strtotime($this->date)),
                        $shopChannel->venture_id
                    );
                    $assocGmv = UniversalCurrency::buildCurrencyData(
                        $data['gmv'],
                        date('Y-m', strtotime($this->date)),
                        $shopChannel->venture_id
                    );
                    foreach ($assocCurrency as $currencyId => $currencyCode) {
                        $assocCurrencyDataPushQueue[$currencyCode][$date][$hour]['impression'] = $data['impression'];
                        $assocCurrencyDataPushQueue[$currencyCode][$date][$hour]['click'] = $data['click'];
                        $assocCurrencyDataPushQueue[$currencyCode][$date][$hour]['item_sold'] = $data['item_sold'];
                        $assocCurrencyDataPushQueue[$currencyCode][$date][$hour]['gmv'] = $assocGmv[$currencyId];
                        $assocCurrencyDataPushQueue[$currencyCode][$date][$hour]['cost'] = $assocCost[$currencyId];
                    }
                }
            }
            foreach ($assocCurrencyDataPushQueue as $currencyCode => $dataPushed) {
                $job = new ImportMetricDaily(
                    $this->shopeeKeywordBidding->shopChannelId,
                    'Keyword Bidding',
                    'SHOPEE',
                    $dataPushed,
                    $currencyCode
                );

                dispatch($job->onQueue(QueueName::CALCULATE_DASHBOARD_SHOP_INTRADAY_2));
            }



        } catch (\Exception $exception) {
            echo $exception->getMessage();
            echo PHP_EOL;
            echo $exception->getFile();
            echo PHP_EOL;
            echo $exception->getLine();
        }
    }
}