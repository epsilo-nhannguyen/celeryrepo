<?php


namespace App\Jobs\Shopee;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CrawlOrder implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    protected $shopChannelId;

    public function __construct($shopChannelId)
    {
        $this->shopChannelId = $shopChannelId;
    }

    public function handle()
    {

    }
}