<?php

namespace App\Jobs;

use App\Helpers\PusherHelper;
use App\Models\ModelBusiness\TokoAdsKeyword;
use App\Models\ModelBusiness\TokoAdsSku;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Pusher\Pusher;

class TokoAdsEditInfoKeywordJob implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $pusherDTO;
    private $data;

    /**
     * TokoAdsEditInfoKeywordJob constructor.
     * @param $pusherDTO DTOPusher
     * @param $data
     */
    public function __construct($pusherDTO, $data)
    {
        //
        $this->pusherDTO = $pusherDTO;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Pusher\PusherException
     */
    public function handle()
    {
        //
        $params = $this->data;
        if($params['type'] == 'match_type'){
            $queryResult = TokoAdsKeyword::queryUpdateMatchType([$params['id']], $params['value']);
        } else {
            $queryResult = TokoAdsKeyword::queryUpdateMaxCost([$params['id']], $params['value']);
        }

        if ($queryResult) {
            $data = ['type' => 'success'];
        } else {
            $data = ['type' => 'failed'];
        }
        $pusher = new PusherHelper();
        $pusher->triggerAction($this->pusherDTO->getChannelName(), $this->pusherDTO->getEventName(), $data);

    }
}
