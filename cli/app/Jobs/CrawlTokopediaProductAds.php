<?php
namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use App\Jobs;
use App\Library;

class CrawlTokopediaProductAds implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Time retries
     * @var int
     */
    public $tries = 2;

    private $shopChannelId;
    private $tokopediaCredential;

    public function __construct($shopChannelId, $tokopediaCredential)
    {
        $this->shopChannelId = $shopChannelId;
        $this->tokopediaCredential = $tokopediaCredential;
    }

    public function handle()
    {
        // New Tokopedia crawler, log error instance and login
        $crawlTokopedia = new Library\Crawler\Channel\Tokopedia($this->shopChannelId);
        $crawlTokopedia->setAuthentication($this->tokopediaCredential);

        // Set timezone
        // Library\Common::setTimezone($crawlTokopedia->getTimeZone());

        // Prepare time data
        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime($startDate . ' +1 days'));
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        // Call crawl data
        $data = $crawlTokopedia->getProductInfoOfGroupAds($startDate, $endDate);
        if (!$data->getIsSuccess()) {
            $data = $crawlTokopedia->getProductInfoOfGroupAds($startDate, $endDate);
        }
        if (!$data->getIsSuccess()) {
            $data = $crawlTokopedia->getProductInfoOfGroupAds($startDate, $endDate);
        }

        // Verify crawl data params
        if (!$data->getIsSuccess()) {
            $message = json_encode([
                "reason" => 'Job ' . __CLASS__ . ' - Tokopedia - Shop Channel Id: ' . $this->shopChannelId . ' - Crawl failure',
                "data" => $data->getMessage()
            ]);
            Library\LogError::getInstance()->slack($message);
            return;
        }

        // push queue
        $job = new Jobs\Toko\KeywordAds\ImportSku($this->shopChannelId, $crawlTokopedia->getTimeZone(), $data);
        dispatch($job->onQueue(Library\QueueName::TOKO_KEYWORD_ADS_IMPORT_SKU));
    }
}