<?php

namespace App\Jobs;

use App\Helpers\PusherHelper;
use App\Models\ModelBusiness\TokoAdsCampaign;
use App\Models\ModelBusiness\TokoAdsKeyword;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Pusher\Pusher;

class TokoAdsEditCampaignJob implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $pusherDTO;
    private $data;

    /**
     * TokoAdsEditCampaignJob constructor.
     * @param $pusherDTO DTOPusher
     * @param $data
     */
    public function __construct($pusherDTO, $data)
    {
        //
        $this->pusherDTO = $pusherDTO;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Pusher\PusherException
     */
    public function handle()
    {
        //
//        $params = [
//            'campaignId' => $campaignId,
//            'campaignName' => $campaignName,
//            'dailyBudget' => $dailyBudget,
//            'maxCost' => $maxCost
//        ];

        $params = $this->data;
        if($params['isUnLimitBudget'] == 'no_limit'){
            $params['dailyBudget'] = 0;
        }
        $queryResultTimeLine = TokoAdsCampaign::queryUpdateTimeLineById($params['campaignId'], $params['timeLineFrom'], $params['timeLineTo']);
        $queryResult = TokoAdsCampaign::queryUpdateInfo($params['campaignId'], $params['dailyBudget'], $params['maxCost'], $params['campaignName']);
        if ($queryResult && $queryResultTimeLine) {
            $data = ['type' => 'success'];
        } else {
            $data = ['type' => 'failed'];
        }
        $pusher = new PusherHelper();
        $pusher->triggerAction($this->pusherDTO->getChannelName(), $this->pusherDTO->getEventName(), $data);

    }
}
