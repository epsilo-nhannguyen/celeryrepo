<?php
namespace App\Jobs;

use App\Jobs\Toko\KeywordAds\ImportSkuPerformance;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use App\Library;

class CrawlTokopediaProductAdsPerformance implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Time retries
     * @var int
     */
    public $tries = 2;

    private $shopChannelId;
    private $dateFrom;
    private $dateTo;
    private $tokopediaCredential;

    public function __construct($shopChannelId, $tokopediaCredential, $dateFrom, $dateTo)
    {
        $this->shopChannelId = $shopChannelId;
        $this->tokopediaCredential = $tokopediaCredential;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function handle()
    {
        // New Tokopedia crawler, log error instance and login
        $crawlTokopedia = new Library\Crawler\Channel\Tokopedia($this->shopChannelId);
        $crawlTokopedia->setAuthentication($this->tokopediaCredential);

        // Set timezone
        // Library\Common::setTimezone($crawlTokopedia->getTimeZone());

        // Prepare time data
        $startDate = $this->dateFrom ?? date('Y-m-d');
        $channelCreateDate = $startDate;
        $endDate = $this->dateTo ?? date('Y-m-d', strtotime($startDate . ' +1 days'));
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        // Call crawl data
        $data = $crawlTokopedia->getProductInfoOfGroupAds($startDate, $endDate);
        if (!$data->getIsSuccess()) {
            $data = $crawlTokopedia->getProductInfoOfGroupAds($startDate, $endDate);
        }
        if (!$data->getIsSuccess()) {
            $data = $crawlTokopedia->getProductInfoOfGroupAds($startDate, $endDate);
        }

        // Verify crawl data params
        if (!$data->getIsSuccess()) {
            $message = json_encode([
                "reason" => 'Job ' . __CLASS__ . ' - Tokopedia - Shop Channel Id: ' . $this->shopChannelId . ' - Crawl failure',
                "data" => $data->getMessage()
            ]);
            Library\LogError::getInstance()->slack($message);
            return;
        }

        // Push Queue Insert or Update Database\
        $job = new ImportSkuPerformance($channelCreateDate, $data->getData());
        dispatch($job->onQueue(Library\QueueName::TOKO_KEYWORD_ADS_IMPORT_SKU_PERFORMANCE));

        print_r($data->getData());
    }
}