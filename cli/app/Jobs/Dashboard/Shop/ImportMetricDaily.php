<?php


namespace App\Jobs\Dashboard\Shop;


use App\Models\Currency;
use App\Services\Dashboard\Forecast\ForecastManager;
use App\Services\Dashboard\Shop\Daily\MetricModel;
use App\Services\Dashboard\Shop\Database\Database;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Library;
use App\Models;
use App\Models\ModelBusiness;
use App\Models\Mongo;
use App\Models\ShopChannel;
use App\Services\Dashboard;
use App\Services\ShopeeKeywordWalletBalance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class ImportMetricDaily implements ShouldQueue
{
    use Queueable, InteractsWithQueue, SerializesModels;

    protected $shopId;

    protected $toolName;

    protected $channelCode;

    protected $dateHourMetricValue;

    protected $currencyCode;

    public function __construct($shopId, $toolName, $channelCode, $dateHourMetricValue, $currencyCode)
    {
        $this->shopId = $shopId;
        $this->toolName = $toolName;
        $this->channelCode = $channelCode;
        $this->dateHourMetricValue = $dateHourMetricValue;
        $this->currencyCode = $currencyCode;
    }

    public function handle()
    {
        $toolId = Database::getToolId($this->toolName, $this->channelCode);
        $collection = collect();
        $forecast = new ForecastManager();
        $assocShopMetricHourData = [];

        foreach ((array) $this->dateHourMetricValue as $date => $assocHour) {
            foreach ((array) $assocHour as $hour => $assocMetric) {
                $model = new MetricModel();
                $model->setDate($date);
                $model->setToolId($toolId);
                $model->setShopId($this->shopId);
                $model->setHour($hour);
                $model->setCurrencyCode($this->currencyCode);
                $model->setAssocMetricValue($assocMetric);
                $collection->push($model);

                if (
                    $date == Carbon::create()->format('Y-m-d')
                    && intval($hour) == intval(Carbon::create()->format('H'))
                ) {
                    foreach ($assocMetric as $metric => $value) {
                        if ($metric == 'wallet_balance') {
                            continue;
                        }
                        $valueInitForecast = Database::getByShopDateHour(
                            $this->shopId, $date, intval($hour)-1,  $metric, $toolId, $this->currencyCode
                        );
                        $collectionForecast = $forecast->forecast($metric, $valueInitForecast, $this->currencyCode, $toolId, $this->shopId);
                        foreach ($collectionForecast as $model) {
                            $assocShopMetricHourData[$model->getShopId()][$model->getDate()][$model->getHour()][$metric]
                                = $model->getAssocMetricValue()[$metric] ?? 0;
                        }
                    }
                }
            }
        }

        foreach ($assocShopMetricHourData as $_shop => $_assocDate) {
            foreach ($_assocDate as $_date => $_assocHourMetric) {
                foreach ($_assocHourMetric as $_hour => $_assocMetricValue) {
                    if (!$_assocMetricValue) {
                        continue;
                    }
                    $model = new MetricModel();
                    $model->setShopId($_shop);
                    $model->setCurrencyCode($this->currencyCode);
                    $model->setToolId($toolId);
                    $model->setDate($_date);
                    $model->setHour($_hour);
                    $model->setAssocMetricValue($_assocMetricValue);
                    $collection = $collection->filter(function (MetricModel $item) use ($_shop, $_date, $_hour) {
                        return $item->getShopId() == $_shop && $item->getDate() == $_date && $item->getHour() != $_hour;
                    });
                    $collection->push($model);
                }
            }

        }
        if ($collection->count()) {
            Database::saveDaily($collection, $this->currencyCode, $toolId);
        }
    }
}