<?php


namespace App\Jobs\Dashboard\Shop;


use App\Models\ModelChart\ChartKeywordCampaignMetric;
use App\Models\ModelChart\ChartKeywordCampaignMetricIntraday;
use App\Services\Dashboard\Shop\ConvertingIntradayToDaily;
use App\Services\Dashboard\Shop\PrimaryData;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class FetchDataMetricDaily implements ShouldQueue
{
    use SerializesModels, Queueable, InteractsWithQueue;

    protected $arrayShopChannelId;
    protected $date;
    protected $connectionDb;

    public function __construct($arrayShopChannelId, $date, $connectionDb)
    {
        $this->arrayShopChannelId = $arrayShopChannelId;
        $this->date = $date;
        $this->connectionDb = $connectionDb;
    }

    public function handle()
    {
        #$collectionInDb = ChartKeywordCampaignMetric::getByArrayShop($this->arrayShopChannelId, $this->date, $this->connection);
        $collectionInDbIntraday = ChartKeywordCampaignMetricIntraday::getByDate($this->arrayShopChannelId, $this->date, $this->connectionDb);
        $collectionInDbIntradayGrouped = $collectionInDbIntraday->groupBy([
            'shop_channel_id',
            'mak_programmatic_campaign_id',
            'date',
            'currency_id'
        ]);
        $collectionDailyToSave = collect([]);
        foreach ($collectionInDbIntradayGrouped as $shopChannelId => $assocCampaignIdDataCurrency) {
            foreach ($assocCampaignIdDataCurrency as $campaignId => $assocDateCurrencyData) {
                foreach ($assocDateCurrencyData as $date => $assocCurrencyData) {
                    foreach ($assocCurrencyData as $currencyId => $collection) {
                        /** @var Collection $collection */
                        $collectionTransformed = $collection->map(function ($item) {
                            $primaryData = new PrimaryData();
                            $primaryData->setImpression($item->impression);
                            $primaryData->setClick($item->click);
                            $primaryData->setItemSold($item->item_sold);
                            $primaryData->setGmv($item->gmv);
                            $primaryData->setCost($item->cost);
                            $primaryData->setTotalSku($item->total_sku);
                            $primaryData->setTotalKeyword($item->total_keyword);
                            $primaryData->setGmvTotal($item->total_gmv);
                            $primaryData->setItemSoldTotal($item->total_item_sold);
                            $primaryData->setWallet($item->wallet_balance);
                            $primaryData->calculateSecondaryData();
                            return $primaryData;
                        });
                        $primaryDataDaily = ConvertingIntradayToDaily::execute($collectionTransformed);
                        $collectionDailyToSave->push([
                            'shop_channel_id' => $shopChannelId,
                            'mak_programmatic_campaign_id' => $campaignId,
                            'date' => $date,
                            'impression' => $primaryDataDaily->getImpression(),
                            'click' => $primaryDataDaily->getClick(),
                            'item_sold' => $primaryDataDaily->getItemSold(),
                            'gmv' => $primaryDataDaily->getGmv(),
                            'cost' => $primaryDataDaily->getCost(),
                            'ctr' => $primaryDataDaily->getSecondaryData()->getCtr(),
                            'cr' => $primaryDataDaily->getSecondaryData()->getCr(),
                            'cpc' => $primaryDataDaily->getSecondaryData()->getCpc(),
                            'cpi' => $primaryDataDaily->getSecondaryData()->getCpi(),
                            'cir' => $primaryDataDaily->getSecondaryData()->getCir(),
                            'total_sku' => $primaryDataDaily->getTotalSku(),
                            'total_keyword' => $primaryDataDaily->getTotalKeyword(),
                            'total_gmv' => $primaryDataDaily->getGmvTotal(),
                            'total_item_sold' => $primaryDataDaily->getItemSoldTotal(),
                            'percent_gmv_division_total' => $primaryDataDaily->getSecondaryData()->getPercentContributionGmv(),
                            'percent_item_sold_division_total' => $primaryDataDaily->getSecondaryData()->getPercentContributionItemSold(),
                            'currency_id' => $currencyId,
                            'wallet_balance' => $primaryDataDaily->getWallet()
                        ]);
                    }
                }

            }
        }

        ChartKeywordCampaignMetric::saveCollection($collectionDailyToSave, $this->connectionDb);
    }
}