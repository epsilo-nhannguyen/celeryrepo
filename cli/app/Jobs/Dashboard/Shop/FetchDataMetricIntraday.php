<?php


namespace App\Jobs\Dashboard\Shop;


use App\Library;
use App\Models;
use App\Models\ModelBusiness;
use App\Models\Mongo;
use App\Models\ShopChannel;
use App\Services\Dashboard;
use App\Services\ShopeeKeywordWalletBalance;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class FetchDataMetricIntraday implements ShouldQueue
{
    use SerializesModels, InteractsWithQueue, Queueable;

    protected $shopChannelId;
    protected $date;
    protected $hour;
    protected $impression;
    protected $click;
    protected $itemSold;
    protected $gmv;
    protected $cost;
    protected $tool;

    public function __construct($shopChannelId, $date, $hour, $impression, $click, $itemSold, $gmv, $cost, $tool)
    {
        $this->shopChannelId = $shopChannelId;
        $this->date = $date;
        $this->hour = $hour;
        $this->impression = $impression;
        $this->click = $click;
        $this->itemSold = $itemSold;
        $this->gmv = $gmv;
        $this->cost = $cost;
        $this->tool = $tool;
    }

    public function handle()
    {
        $shopChannel = ShopChannel::find($this->shopChannelId);
        if (!$shopChannel) {
            return;
        }

        date_default_timezone_set($shopChannel->venture_timezone);
        $ventureId = $shopChannel->venture_id;
        $yearMonth = date('Y-m', strtotime($this->date));
        $year = date('Y', strtotime($this->date));
        $month = date('m', strtotime($this->date));
        $day = date('d', strtotime($this->date));

        $channelCode = strtolower($shopChannel->channel_code);
        $totalSku = $totalKeyword = 0;
        $campaignId = null;
        if ($channelCode == 'shopee') {
            switch ($this->tool) {
                case 'keyword_bidding':
                    $firstCampaign = Models\MakProgrammaticCampaign::getByShopChannelId($this->shopChannelId);
                    if (!$firstCampaign) {
                        return;
                    }
                    $campaignId = $firstCampaign->mak_programmatic_campaign_id;
                    list($totalSku, $totalKeyword) = $this->_getTotalSkuAndKeyword(
                        $campaignId,
                        Carbon::create($year, $month, $day, $this->hour, 0, 0),
                        Carbon::create($year, $month, $day, $this->hour, 59, 59),
                        ! Carbon::create($year, $month, $day, $this->hour)->isCurrentHour()
                    );
                    break;

                case 'shop_ads':
                    $firstCampaign = ModelBusiness\ShopAds::getByShopChannelId($this->shopChannelId);
                    if (!$firstCampaign) {
                        return;
                    }
                    $campaignId = $firstCampaign->id;
                    $totalKeyword = count(Models\ModelBusiness\ShopAdsKeyword::getByShopAdsId($campaignId)->toArray());
                    list($totalSku, $totalKeyword) = [0, $totalKeyword];
                    break;

                default:break;
            }
        }

        if (!$campaignId) {
            return;
        }

        $serviceWalletBalance = new ShopeeKeywordWalletBalance($this->shopChannelId);

        $walletData = $serviceWalletBalance->getListByDate($this->date)->toArray();

        $saleOrderData = Models\Mongo\SaleOrder::getDataSaleOrderDateFromTo(
            $shopChannel->fk_shop_master, $shopChannel->fk_channel,
            Carbon::create($year, $month, $day, $this->hour, 0, 0)->getTimestamp(),
            Carbon::create($year, $month, $day, $this->hour, 59, 59)->getTimestamp()
        );
        $saleOrderData = (array) current((array) $saleOrderData);

        $gmvChannel = floatval($saleOrderData['totalPrice'] ?? 0);
        $itemSoldChannel = intval($saleOrderData['totalItem'] ?? 0);
        $walletHour = $walletData[$this->hour] ?? null;

        $primaryData = new Dashboard\Shop\PrimaryData();
        $primaryData->setImpression($this->impression);
        $primaryData->setClick($this->click);
        $primaryData->setItemSold($this->itemSold);
        $primaryData->setGmv($this->gmv);
        $primaryData->setCost($this->cost);
        $primaryData->setGmvTotal($gmvChannel);
        $primaryData->setItemSoldTotal($itemSoldChannel);
        $primaryData->setTotalKeyword($totalKeyword);
        $primaryData->setTotalSku($totalSku);
        $primaryData->setWallet($walletHour);

        $primaryData->calculateSecondaryData();
        $currencyCodeArray = Models\Currency::getAll()->pluck('currency_code', 'currency_id');

        $convertingCurrency = new Dashboard\Shop\ConvertingCurrency(
            Carbon::create($year, $month, $day, $this->hour)->getTimestamp()
        );
        $collection = collect([]);

        foreach ($currencyCodeArray as $currencyId => $currencyCode) {
            $_primaryData = $convertingCurrency->convert($primaryData, $ventureId, $currencyCode);

            $collection->push([
                'shop_channel_id' => $this->shopChannelId,
                'mak_programmatic_campaign_id' => $campaignId,
                'date' => $this->date,
                'hour' => $this->hour,
                'impression' => $_primaryData->getImpression(),
                'click' => $_primaryData->getClick(),
                'item_sold' => $_primaryData->getItemSold(),
                'gmv' => $_primaryData->getGmv(),
                'cost' => $_primaryData->getCost(),
                'ctr' => $_primaryData->getSecondaryData()->getCtr(),
                'cr' => $_primaryData->getSecondaryData()->getCr(),
                'cpc' => $_primaryData->getSecondaryData()->getCpc(),
                'cpi' => $_primaryData->getSecondaryData()->getCpi(),
                'cir' => $_primaryData->getSecondaryData()->getCir(),
                'total_sku' => $_primaryData->getTotalSku(),
                'total_keyword' => $_primaryData->getTotalKeyword(),
                'total_gmv' => $_primaryData->getGmvTotal(),
                'total_item_sold' => $_primaryData->getItemSoldTotal(),
                'percent_gmv_total' => $_primaryData->getSecondaryData()->getPercentContributionGmv(),
                'percent_item_sold_total' => $_primaryData->getSecondaryData()->getPercentContributionItemSold(),
                'currency_id' => $currencyId,
                'is_forecast' => ! Carbon::create($year, $month, $day, $this->hour)->isCurrentHour(),
                'wallet_balance' => $_primaryData->getWallet()
            ]);
        }

        if (Carbon::create($year, $month, $day, $this->hour)->isCurrentHour()) {
            #do forecast
            $forecast = new Dashboard\Shop\Forecast($this->shopChannelId, $this->tool);
            $collectionForecast = $forecast->execute($campaignId);
            foreach ($collectionForecast as $hour => $primaryData) {
                foreach ($currencyCodeArray as $currencyId => $currencyCode) {
                    $_primaryData = $convertingCurrency->convert($primaryData, $ventureId, $currencyCode);

                    $collection->push([
                        'shop_channel_id' => $this->shopChannelId,
                        'mak_programmatic_campaign_id' => $campaignId,
                        'date' => $this->date,
                        'hour' => $hour,
                        'impression' => $_primaryData->getImpression(),
                        'click' => $_primaryData->getClick(),
                        'item_sold' => $_primaryData->getItemSold(),
                        'gmv' => $_primaryData->getGmv(),
                        'cost' => $_primaryData->getCost(),
                        'ctr' => $_primaryData->getSecondaryData()->getCtr(),
                        'cr' => $_primaryData->getSecondaryData()->getCr(),
                        'cpc' => $_primaryData->getSecondaryData()->getCpc(),
                        'cpi' => $_primaryData->getSecondaryData()->getCpi(),
                        'cir' => $_primaryData->getSecondaryData()->getCir(),
                        'total_sku' => $_primaryData->getTotalSku(),
                        'total_keyword' => $_primaryData->getTotalKeyword(),
                        'total_gmv' => $_primaryData->getGmvTotal(),
                        'total_item_sold' => $_primaryData->getItemSoldTotal(),
                        'percent_gmv_total' => $_primaryData->getSecondaryData()->getPercentContributionGmv(),
                        'percent_item_sold_total' => $_primaryData->getSecondaryData()->getPercentContributionItemSold(),
                        'currency_id' => $currencyId,
                        'is_forecast' => ! Carbon::create($year, $month, $day, $this->hour)->isCurrentHour(),
                        'wallet_balance' => $_primaryData->getWallet()
                    ]);
                }
            }

        }
        Dashboard\Shop\Database\Database::save($channelCode, $this->tool, $collection);
    }

    /**
     * get Total Sku And Keyword
     * @param int $campaignId
     * @param string $dateFrom
     * @param string $dateTo
     * @param bool $isRunAgain
     * @return array
     */
    private function _getTotalSkuAndKeyword($campaignId, $dateFrom, $dateTo, $isRunAgain)
    {
        $shopChannelId = $this->shopChannelId;
        if ( ! $isRunAgain) {
            $dateFrom = Library\Common::convertDatetimeToTimestamp($dateFrom);
            $dateTo = Library\Common::convertDatetimeToTimestamp($dateTo);

            $productIdArrayInLog = array_values(array_unique(array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticLogSku::searchCampaignIdByDateFromTo(
                    $campaignId, $dateFrom, $dateTo, [
                        Models\MakProgrammaticAction::PAUSE_SKU,
                        Models\MakProgrammaticAction::RESUME_SKU
                    ]
                )),
                Models\MakProgrammaticLogSku::COL_FK_PRODUCT_SHOP_CHANNEL
            )));

            $productIdArray = array_column(
                Library\Formater::stdClassToArray(Models\ProductAds::searchByCampaignIdAndState($campaignId, Models\ProductAds::STATE_ONGOING)),
                Models\ProductAds::COL_FK_PRODUCT_SHOP_CHANNEL
            );

            $totalSku = count(array_diff($productIdArray, $productIdArrayInLog)) + count($productIdArrayInLog);

            $keywordProductIdArrayInLog = array_values(array_unique(array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticLogKeyword::searchCampaignIdByDateFromTo(
                    $campaignId, $dateFrom, $dateTo, [
                        Models\MakProgrammaticAction::DEACTIVATE_KEYWORD,
                        Models\MakProgrammaticAction::ACTIVATE_KEYWORD
                    ]
                )),
                Models\MakProgrammaticLogKeyword::COL_FK_MAK_PROGRAMMATIC_KEYWORD_PRODUCT
            )));

            $keywordProductIdArray = array_column(
                Library\Formater::stdClassToArray(Models\MakProgrammaticKeywordProduct::searchByCampaignIdAndState(
                    $campaignId, Models\MakProgrammaticKeywordProduct::STATUS_RESTORE
                )),
                Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            );

            $totalKeyword = count(array_diff($keywordProductIdArray, $keywordProductIdArrayInLog)) + count($keywordProductIdArrayInLog);
        } else {
            $keywordProductData = Library\Formater::stdClassToArray(
                Models\MakPerformanceHour::searchByViewGreater0(
                    $shopChannelId, Library\Common::getDateFromDatetime($dateFrom), intval(Library\Common::getHourFromDatetime($dateFrom))
                )
            );
            $assocCampaignIdProduct = array_count_values(array_column(
                $keywordProductData,
                Models\MakPerformanceHour::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                Models\MakProgrammaticKeywordProduct::COL_FK_PRODUCT_SHOP_CHANNEL
            ));
            $assocCampaignIdKeywordProduct = array_count_values(array_column(
                $keywordProductData,
                Models\MakPerformanceHour::COL_FK_MAK_PROGRAMMATIC_CAMPAIGN,
                Models\MakProgrammaticKeywordProduct::COL_MAK_PROGRAMMATIC_KEYWORD_PRODUCT_ID
            ));

            $totalSku = $assocCampaignIdProduct[$campaignId] ?? 0;
            $totalKeyword = $assocCampaignIdKeywordProduct[$campaignId] ?? 0;
        }

        return [$totalSku, $totalKeyword];
    }
}