<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 28/09/2019
 * Time: 18:15
 */

namespace App\Jobs;

use App\Library;
use App\Models;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;


class yRawKeywordProduct implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if ( ! $data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);
        $shopChannelId = intval($data['shopChannelId']);
        $ventureId = intval($data['ventureId']);

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        if ($channelInfo[Models\Channel::COL_CHANNEL_CODE] != Models\Channel::SHOPEE_CODE) return;

        Library\Common::setTimezoneByVentureId($ventureId);

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        if ( ! $isLogin) $isLogin = $crawler->login();
        if ( ! $isLogin) $isLogin = $crawler->login();
        if ( ! $isLogin) $isLogin = $crawler->login();

        $curlTotal = curl_init($crawler->shopeeSellerDomain() . '/api/v2/pas/campaignAds/?placement=0&SPC_CDS_VER=2');
        curl_setopt($curlTotal, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curlTotal, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlTotal, CURLOPT_COOKIEFILE, $crawler->cookiePath);
        curl_setopt($curlTotal, CURLOPT_COOKIEJAR, $crawler->cookiePath);
        curl_setopt($curlTotal, CURLOPT_FOLLOWLOCATION, 1);

        $resultTotal = curl_exec($curlTotal);
        curl_close($curlTotal);
        $totalItem = json_decode($resultTotal,true);

        $totalAds = $totalItem['pas/campaign-ads'] ?? [];
        $arrayProductIdentify = array_column($totalAds, 'itemid');

        $arrayProductIdentifyInDatabase = $assocIdentifyToObjectId = [];
        $keywordProductMongo = Models\Mongo\KeywordProduct::searchByProductIdentifyArray($shopId, $channelId, $arrayProductIdentify);
        foreach ($keywordProductMongo as $bson) {
            /** @var MongoDB\BSON\ObjectId $mongoObjectId */
            $mongoObjectId = $bson->_id;
            $arrayProductIdentifyInDatabase[] = $bson->productIdentify;
            $assocIdentifyToObjectId[$bson->productIdentify] = $mongoObjectId;
        }
        $arrayIdentifyNeedInsert = array_diff($arrayProductIdentify, $arrayProductIdentifyInDatabase);

        $arrayInsert = [];
        foreach ($totalAds as $ads) {
            $keywords = [];
            foreach ($ads['keywords'] as $keyword) {
                $keywords[] = [
                    'matchType' => $keyword['match_type'] == 1
                        ? Models\MakProgrammaticKeywordProduct::MATCH_TYPE_EXPANSION : Models\MakProgrammaticKeywordProduct::MATCH_TYPE_DEFINED,
                    'keyword' => trim($keyword['keyword']),
                    'price' => floatval($keyword['price']),
                    'status' => $keyword['status'] == 1 ? Models\MakProgrammaticKeywordProduct::STATUS_RESTORE : Models\MakProgrammaticKeywordProduct::STATUS_DELETED,
                ];
            }

            if (in_array($ads['itemid'], $arrayIdentifyNeedInsert)) {
                #prepage data for batch-insert
                $arrayInsert[] = [
                    'shopId' => intval($shopId),
                    'channelId' => intval($channelId),
                    'productIdentify' => trim($ads['itemid']),
                    'isImported' => 0,
                    'isGetPaidReports' => 0,
                    'updatedAt' => Library\Common::getCurrentTimestamp(),
                    'data' => [
                        'totalQuota' => floatval($ads['total_quota']),
                        'dailyQuota' => floatval($ads['daily_quota']),
                        'startTime' => trim($ads['start_time']),
                        'endTime' => trim($ads['end_time']),
                        'state' => trim($ads['state']),
                        'adsId' => trim($ads['id']),
                        'keywords' => $keywords
                    ],
                    'raw' => $ads
                ];
            } else {
                #do update
                $mongoObjectId = $assocIdentifyToObjectId[$ads['itemid']];
                Models\Mongo\KeywordProduct::update(
                    $mongoObjectId,
                    $shopId,
                    $channelId,
                    $ads['itemid'],
                    $ads['total_quota'],
                    $ads['daily_quota'],
                    $ads['start_time'],
                    $ads['end_time'],
                    $ads['state'],
                    $ads['id'],
                    $keywords,
                    $ads
                );
            }
        }

        if ( ! $arrayInsert) return;
        Models\Mongo\KeywordProduct::batchInsert($arrayInsert);

        return;
    }
}