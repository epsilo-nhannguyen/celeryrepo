<?php
namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportCampaignPerformance extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlCampaignData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlCampaignData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlCampaignData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->crawlCampaignData = $crawlCampaignData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all campaign info by shopchannelid
        $allCampaginInfo = Models\ModelBusiness\Tiki\TikiAdsCampaign::getAllCampaignByShopChannelId($this->shopChannelId)->pluck('id', 'channel_campaign_id')->toArray();

        // Get all current campaign performance by channel campaign id and date in DB
        $allCampaginPerformance = Models\ModelBusiness\Tiki\TikiAdsCampaignPerformance::getAllPerformanceByChannelCampaignIdAndDate(array_keys($allCampaginInfo), $this->crawlFromDate)->mapWithKeys(function ($item) {
            return [$item->channel_campaign_id => [
                'id' => $item->id,
                'impression' => $item->impression,
                'click' => $item->click,
                'item_sold' => $item->item_sold,
                'cost' => $item->cost,
                'gmv' => $item->gmv
            ]];
        })->toArray();

        foreach ($this->crawlCampaignData as $campaignPerformance) {
            $recordCollection = collect([
                'impression' => $campaignPerformance['impression'],
                'click' => $campaignPerformance['click'],
                'item_sold' => $campaignPerformance['ot_item_sold_ins'],
                'cost' => floatval($campaignPerformance['proceeds']),
                'gmv' => floatval($campaignPerformance['ot_pur_revenue_ins']),
                'updated_at' =>  Library\Common::getCurrentTimestamp()
            ]);

            if (!array_key_exists($campaignPerformance['lineitem_id'], $allCampaginPerformance)) {
                $recordCollection->put('campaign_id', intval($allCampaginInfo[$campaignPerformance['lineitem_id']]) ?? null);
                $recordCollection->put('channel_campaign_id', $campaignPerformance['lineitem_id']);
                $recordCollection->put('channel_create_date', $this->crawlFromDate);
                $recordCollection->put('created_at', Library\Common::getCurrentTimestamp());
                $recordInsertCollection->push($recordCollection);
            }
            else {
                // Check has diff in data
                if (!$this->_checkHasDiffCampaignData($allCampaginPerformance[$campaignPerformance['lineitem_id']],
                    $campaignPerformance['impression'],
                    $campaignPerformance['click'],
                    $campaignPerformance['ot_item_sold_ins'],
                    floatval($campaignPerformance['proceeds']),
                    floatval($campaignPerformance['ot_pur_revenue_ins']))) {
                    continue;
                }
                Models\ModelBusiness\Tiki\TikiAdsCampaignPerformance::updatedById($allCampaginPerformance[$campaignPerformance['lineitem_id']]['id'], $recordCollection->toArray());
            }
        }

        // Batch insert campaign info
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsCampaignPerformance ::batchInsert($recordInsertCollection->toArray());
        }

        // Push another job
        $job = new CrawlGroupAds($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlFromDate, $this->crawlToDate);
        dispatch($job->onQueue(config('queue.tiki.crawl_data')));
    }

    /**
     * @param array $dbData
     * @param int $impression
     * @param int $click
     * @param int $item_sold
     * @param float $cost
     * @param float $gmv
     * @return bool
     */
    private function _checkHasDiffCampaignData($dbData, $impression, $click, $item_sold, $cost, $gmv)
    {
        if ($dbData['impression'] != $impression) {
            return true;
        } elseif ($dbData['click'] != $click) {
            return true;
        } elseif ($dbData['item_sold'] != $item_sold) {
            return true;
        } elseif ($dbData['cost'] != $cost) {
            return true;
        } elseif ($dbData['gmv'] != $gmv) {
            return true;
        }
        return false;
    }
}