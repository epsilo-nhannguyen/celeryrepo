<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportGroupAds extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlGroupAdsData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlGroupAdsData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlGroupAdsData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->crawlGroupAdsData = $crawlGroupAdsData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all campaign info by shop channel id
        $allCampaginInfo = Models\ModelBusiness\Tiki\TikiAdsCampaign::getAllCampaignByShopChannelId($this->shopChannelId)->pluck('id', 'channel_campaign_id')->toArray();

        // Get all current group ads by shop in DB [['channel_group_ads_id' => [data]],[],...]
        $allDBShopGroupAds = Models\ModelBusiness\Tiki\TikiAdsGroupAds::getAllGroupAdsByShopChannelId($this->shopChannelId)->mapWithKeys(function ($item) {
            return [$item->channel_group_ads_id => [
                'id' => $item->id,
                'campaign_id' => $item->campaign_id,
                'channel_campaign_id' => $item->channel_campaign_id,
                'status' => $item->status,
                'name' => $item->name,
                'price' => $item->price
            ]];
        })->toArray();

        foreach ($this->crawlGroupAdsData as $groupAdsData) {
            #region Dev logic and mapping data
            $campaignId = $groupAdsData['lineitem_id'];
            $groupAdsId = $groupAdsData['campaign_id'];
            $status = strtolower($groupAdsData['status']);
            $name = $groupAdsData['campaign_name']['title'];
            $price = floatval($groupAdsData['bid_price']);
            $type = $groupAdsData['campaign_type'];
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;

            // Mapping status
            $status = str_replace('campaign ', '', $status); // Remove text campaign
            // Handle if have tiki "shit"
            if (!in_array($status, ['eligible', 'paused', 'ended', 'pending', 'removed'])) {
                Library\LogError::getInstance()->slack('Tiki (Group Ads) have new status (shit): ' . $status, 'epsilo-m-tiki');
                continue;
            }
            if ($status == 'eligible') {
                $status = 'ongoing';
            } elseif ($status == 'pending') {
                $status = 'scheduled';
            } elseif ($status == 'removed') {
                $status = 'deleted';
            }

            // Build record data
            $recordCollection = collect([
                'status' => $status,
                'name' => $name,
                'price' => $price,
                'updated_at' => $updatedAt,
                'updated_by' => $updatedBy,
            ]);

            #endregion
            if (!array_key_exists($groupAdsData['campaign_id'], $allDBShopGroupAds)) {
                $recordCollection->put('campaign_id', $allCampaginInfo[$campaignId] ?? null);
                $recordCollection->put('channel_campaign_id', $campaignId);
                $recordCollection->put('channel_group_ads_id', $groupAdsId);
                $recordCollection->put('shop_channel_id', $this->shopChannelId);
                $recordCollection->put('type', $type);
                $recordCollection->put('created_at', $createdAt);
                $recordCollection->put('created_by', $createdBy);

                // Push to batch insert
                $recordInsertCollection->push($recordCollection);
            } else {
                if ($this->_checkHasDiffData($allDBShopGroupAds[$groupAdsId], $name, $status, $price)) {
                    Models\ModelBusiness\Tiki\TikiAdsGroupAds::updatedById($allDBShopGroupAds[$groupAdsId]['id'], $recordCollection->toArray());
                }
            }
        }

        // Batch insert campaign info
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsGroupAds::batchInsert($recordInsertCollection->toArray());
        }

        // Push another job
        $job = new ImportGroupAdsPerformance($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlGroupAdsData, $this->crawlFromDate, $this->crawlToDate);
        dispatch($job->onQueue(config('queue.tiki.import_data')));
    }

    /**
     * @param array $dbData
     * @param string $name
     * @param string $status
     * @param float $price
     * @return bool
     */
    private function _checkHasDiffData($dbData, $name, $status, $price)
    {
        if ($dbData['name'] != $name) {
            return true;
        } elseif ($dbData['status'] != $status) {
            return true;
        } elseif ($dbData['price'] != $price) {
            return true;
        }
        return false;
    }
}