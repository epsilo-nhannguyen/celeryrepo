<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportGroupAdsPerformance extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlGroupAdsData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlGroupAdsData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlGroupAdsData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->crawlGroupAdsData = $crawlGroupAdsData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all group ads info by shopchannelid
        $allGroupAdsInfo = Models\ModelBusiness\Tiki\TikiAdsGroupAds::getAllGroupAdsByShopChannelId($this->shopChannelId)->pluck('id', 'channel_group_ads_id')->toArray();

        // Get all current campaign performance by channel campaign id and date in DB
        $allGroupAdsPerformance = Models\ModelBusiness\Tiki\TikiAdsGroupAdsPerformance::getAllPerformanceByChannelGroupAdsIdAndDate(array_keys($allGroupAdsInfo), $this->crawlFromDate)->mapWithKeys(function ($item) {
            return [$item->channel_group_ads_id => [
                'id' => $item->id,
                'impression' => $item->impression,
                'click' => $item->click,
                'item_sold' => $item->item_sold,
                'cost' => $item->cost,
                'gmv' => $item->gmv
            ]];
        })->toArray();

        foreach ($this->crawlGroupAdsData as $groupAdsData) {
            // Dev mapping data
            $impression = $groupAdsData['impression'];
            $click = $groupAdsData['click'];
            $item_sold = $groupAdsData['ot_item_sold_ins'];
            $cost = floatval($groupAdsData['proceeds']);
            $gmv = floatval($groupAdsData['ot_pur_revenue_ins']);

            // Don't care deleted of status deleted
            $status = strtolower($groupAdsData['status']);
            $status = str_replace('ad group ', '', $status); // Remove text campaign
            $status = str_replace('campaign ', '', $status); // Remove text campaign
            if ($status == 'removed' && !$this->_isHaveData($impression, $click, $item_sold, $cost, $gmv))
                continue;

            // Build record
            $recordCollection = collect([
                'impression' => $impression,
                'click' => $click,
                'item_sold' => $item_sold,
                'cost' => $cost,
                'gmv' => $gmv,
                'updated_at' => Library\Common::getCurrentTimestamp()
            ]);

            if (!array_key_exists($groupAdsData['campaign_id'], $allGroupAdsPerformance)) {
                $recordCollection->put('group_id', intval($allGroupAdsInfo[$groupAdsData['campaign_id']]) ?? null);
                $recordCollection->put('channel_campaign_id', $groupAdsData['lineitem_id']);
                $recordCollection->put('channel_group_ads_id', $groupAdsData['campaign_id']);
                $recordCollection->put('channel_create_date', $this->crawlFromDate);
                $recordCollection->put('created_at', Library\Common::getCurrentTimestamp());
                $recordInsertCollection->push($recordCollection);
            } else {
                // Check has diff in data
                if (!$this->_checkHasDiffData($allGroupAdsPerformance[$groupAdsData['campaign_id']],
                    $impression,
                    $click,
                    $item_sold,
                    $cost,
                    $gmv)) {
                    continue;
                }
                Models\ModelBusiness\Tiki\TikiAdsGroupAdsPerformance::updatedById($allGroupAdsPerformance[$groupAdsData['campaign_id']]['id'], $recordCollection->toArray());
            }
        }

        // Batch insert campaign info
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsGroupAdsPerformance::batchInsert($recordInsertCollection->toArray());
        }

        // Push job crawl SKU
        $job = new CrawlSkuAds($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlFromDate, $this->crawlToDate);
        dispatch($job->onQueue(config('queue.tiki.crawl_data')));

        // Push job crawl Keyword
        $jobKeyword = new CrawlKeywordAds($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlFromDate, $this->crawlToDate);
        dispatch($jobKeyword->onQueue(config('queue.tiki.crawl_data')));
    }

    /**
     * @param array $dbData
     * @param int $impression
     * @param int $click
     * @param int $item_sold
     * @param float $cost
     * @param float $gmv
     * @return bool
     */
    private function _checkHasDiffData($dbData, $impression, $click, $item_sold, $cost, $gmv)
    {
        if ($dbData['impression'] != $impression) {
            return true;
        } elseif ($dbData['click'] != $click) {
            return true;
        } elseif ($dbData['item_sold'] != $item_sold) {
            return true;
        } elseif ($dbData['cost'] != $cost) {
            return true;
        } elseif ($dbData['gmv'] != $gmv) {
            return true;
        }
        return false;
    }

    /**
     * @param int $impression
     * @param int $click
     * @param int $item_sold
     * @param float $cost
     * @param float $gmv
     * @return bool
     */
    private function _isHaveData($impression, $click, $item_sold, $cost, $gmv)
    {
        if ($impression != 0)
            return true;
        elseif ($click != 0)
            return true;
        elseif ($item_sold != 0)
            return true;
        elseif ($cost != 0)
            return true;
        elseif ($gmv != 0)
            return true;
        return false;
    }
}