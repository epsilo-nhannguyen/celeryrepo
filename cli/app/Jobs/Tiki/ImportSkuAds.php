<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportSkuAds extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlGroupAdsSKUData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlGroupAdsSKUData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlGroupAdsSKUData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->crawlGroupAdsSKUData = $crawlGroupAdsSKUData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all product shop channel by shop channel id
        $allProductShopChannel = Models\ModelBusiness\ProductShopChannel::searchByShopChannelId($this->shopChannelId)->pluck('product_shop_channel_id', 'product_shop_channel_shop_sku')->toArray();

        // Get all group ads sku by shop channel id
        $allGroupAdsSku = Models\ModelBusiness\Tiki\TikiAdsSku::getAllSkuByShopChannelId($this->shopChannelId)->pluck('id', 'product_shop_item_id')->toArray();

        // District channel sku
        $districtSKU = collect($this->crawlGroupAdsSKUData)->mapWithKeys(function ($item) {
            return [
                current($item['pro_sku'])['title'] => current($item['pro_sku'])['title']
            ];
        })->toArray();

        foreach ($districtSKU as $skuData) {
            #region Dev logic and mapping data
            $productChannelId = $allProductShopChannel[$skuData] ?? null;
            $shopChannelId = $this->shopChannelId;
            $channelSKUId = $skuData;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;

            // Build record data
            $recordCollection = collect([
                'product_id' => $productChannelId,
                'shop_channel_id' => $shopChannelId,
                'channel_sku_id' => null,
                'product_shop_item_id' => $channelSKUId,
                'created_at' => $createdAt,
                'created_by' => $createdBy,
                'updated_at' => $updatedAt,
                'updated_by' => $updatedBy,
            ]);

            if (!array_key_exists($channelSKUId, $allGroupAdsSku)) {
                $recordInsertCollection->push($recordCollection);
            } else {
                continue;
            }
        }

        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsSku::batchInsert($recordInsertCollection->toArray());
        }

        // Push job import relationship group ads vs sku
        $job = new ImportGroupAdsSkuAds($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlGroupAdsSKUData, $this->crawlFromDate, $this->crawlToDate);
        dispatch($job->onQueue(config('queue.tiki.import_data')));
    }
}