<?php
namespace App\Jobs\Tiki;


use App\Jobs\Job;
use App\Library;


class CrawCampaignData extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * CrawlProductInfo constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     */
    public function __construct($shopChannelId, $credentialData, $timezone)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
    }

    public function handle()
    {
        $tiki = new Library\Crawler\Channel\Tiki($this->shopChannelId);
        $loginSuccess = $tiki->login();
        if ( ! $loginSuccess->getIsSuccess()) {
            Library\LogError::getInstance()->slack('Some thing error - Login failure - Shop channel: ' . $this->shopChannelId, config('slack.tiki'));
        }

        // Push job crawl Keyword
        $jobKeyword = new CrawlKeywordAds($this->shopChannelId, $loginSuccess->getData(), $this->timezone);
        dispatch($jobKeyword->onQueue(config('queue.tiki.crawl_data')));

        // Push job crawl SKU
        $jobSku = new CrawlSkuAds($this->shopChannelId, $loginSuccess->getData(), $this->timezone);
        dispatch($jobSku->onQueue(config('queue.tiki.crawl_data')));

    }

}