<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Models;
use App\Library;

class ImportGroupAdsKeywordAds extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlGroupAdsKeywordData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlGroupAdsKeywordData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlGroupAdsKeywordData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->crawlGroupAdsKeywordData = $crawlGroupAdsKeywordData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all group ads by shop channel id
        $allGroupAds = Models\ModelBusiness\Tiki\TikiAdsGroupAds::getAllGroupAdsByShopChannelId($this->shopChannelId)->pluck('id', 'channel_group_ads_id')->toArray();

        // Get all group ads sku by shop channel id
        $allKeywordAds = Models\ModelBusiness\Tiki\TikiAdsKeyword::getAllKeywordByShopChannelId($this->shopChannelId)->pluck('id', 'channel_keyword_id')->toArray();

        // Get all relationship group ads keyword ads
        $allGroupAdsKeywordRelationship = Models\ModelBusiness\Tiki\TikiAdsGroupKeywordRelationship::getAllGroupAdsKeywordAdsByDbKeywordId(array_values($allKeywordAds))->mapWithKeys(function ($item) {
            return [
                $item->channel_group_ads_id . '-' . $item->channel_keyword_id => [
                    'id' => $item->id,
                    'status' => $item->status,
                    'bidding_price' => $item->bidding_price,
                    'match_type' => $item->match_type
                ]
            ];
        })->toArray();

        foreach ($this->crawlGroupAdsKeywordData as $keywordData) {
            #region Dev logic and mapping data
            $groupAdsId = $keywordData['campaign_id'];
            $keywordId = $keywordData['keyword_id'];
            $assocGroupAdsId_keywordAdsId = $groupAdsId . '-' . $keywordId;
            $status = strtolower($keywordData['status']);
            $matchType = $keywordData['matching_type'];
            $biddingPrice = floatval($keywordData['bid_price']);
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;

            // Mapping Status and Match Type
            if (!in_array($status, ['normal', 'pause', 'removed'])) {
                Library\LogError::getInstance()->slack('Tiki (Keyword Ads) have new status (shit): ' . $status . ' - channel data: ' . json_encode($keywordData), 'epsilo-m-tiki');
                continue;
            }
            if (!in_array($matchType, ['broad_match_modifier', 'phrase', 'exact', 'none'])) {
                Library\LogError::getInstance()->slack('Tiki (Keyword Ads) have new match type (shit): ' . $matchType . ' - channel data: ' . json_encode($keywordData), 'epsilo-m-tiki');
                continue;
            }

            // Status
            if ($status == 'normal') {
                $status = 1;
            } elseif ($status == 'pause') {
                $status = 2;
            } else {
                $status = 0;
            }

            // Match Type
            if ($matchType == 'broad_match_modifier') {
                $matchType = 'broad_match';
            } elseif ($matchType == 'exact') {
                $matchType = 'exact_match';
            } elseif ($matchType == 'none') {
                // Don't import none match type (Waiting BA confirm)
                continue;
            }
            #endregion

            // Relationship from epsilo
            $epsiloGroupAdsId = $allGroupAds[$groupAdsId] ?? null;
            $epsiloKeywordAdsId = $allKeywordAds[$keywordId] ?? null;
            if (!$epsiloGroupAdsId) {
                # Tiki have error - don't have group ads
//                Library\LogError::getInstance()->slack('Tiki (Keyword Ads) has some thing error: Channel group id: ' . $groupAdsId . ' not exit in epsilo', 'epsilo-m-tiki');
                continue;
            }
            if (!$epsiloKeywordAdsId) {
                # Tiki have error - don't have keyword ads
//                Library\LogError::getInstance()->slack('Tiki (Keyword Ads) has some thing error: Channel keyword id: ' . $keywordId . ' not exit in epsilo', 'epsilo-m-tiki');
                continue;
            }

            // Build record data
            $recordCollection = collect([
                'bidding_price' => $biddingPrice,
                'match_type' => $matchType,
                'status' => $status,
                'updated_at' => $updatedAt,
                'updated_by' => $updatedBy,
            ]);

            if (!array_key_exists($assocGroupAdsId_keywordAdsId, $allGroupAdsKeywordRelationship)) {
                $recordCollection->put('group_ads_id', $epsiloGroupAdsId);
                $recordCollection->put('keyword_id', $epsiloKeywordAdsId);
                $recordCollection->put('created_at', $createdAt);
                $recordCollection->put('created_by', $createdBy);

                $recordInsertCollection->push($recordCollection);
            } else {
                if ($this->_checkHasDiffData($allGroupAdsKeywordRelationship[$assocGroupAdsId_keywordAdsId], $biddingPrice, $status, $matchType)) {
                    Models\ModelBusiness\Tiki\TikiAdsGroupKeywordRelationship::updatedById($allGroupAdsKeywordRelationship[$assocGroupAdsId_keywordAdsId]['id'], $recordCollection->toArray());
                }
            }
        }

        // Batch insert
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsGroupKeywordRelationship::batchInsert($recordInsertCollection->toArray());
        }

        // Push job import performance
        $jobImport = new ImportGroupAdsKeywordAdsPerformance($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlGroupAdsKeywordData, $this->crawlFromDate, $this->crawlToDate);
        dispatch($jobImport->onQueue(config('queue.tiki.import_data')));
    }

    /**
     * @param array $dbData
     * @param float $biddingPrice
     * @param int $status
     * @param string $matchType
     * @return bool
     */
    private function _checkHasDiffData($dbData, $biddingPrice, $status, $matchType)
    {
        if ($dbData['bidding_price'] != $biddingPrice) {
            return true;
        } elseif ($dbData['status'] != $status) {
            return true;
        } elseif ($dbData['match_type'] != $matchType) {
            return true;
        }
        return false;
    }
}