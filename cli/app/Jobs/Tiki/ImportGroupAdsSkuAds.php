<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportGroupAdsSkuAds extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlGroupAdsSKUData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlGroupAdsSKUData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlGroupAdsSKUData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->crawlGroupAdsSKUData = $crawlGroupAdsSKUData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all group ads by shop channel id
        $allGroupAds = Models\ModelBusiness\Tiki\TikiAdsGroupAds::getAllGroupAdsByShopChannelId($this->shopChannelId)->pluck('id', 'channel_group_ads_id')->toArray();

        // Get all group ads sku by shop channel id
        $allGroupAdsSku = Models\ModelBusiness\Tiki\TikiAdsSku::getAllSkuByShopChannelId($this->shopChannelId)->pluck('id', 'product_shop_item_id')->toArray();

        // Get all relationship group ads sku ads
        $allGroupAdsSkuRelationship = Models\ModelBusiness\Tiki\TikiAdsGroupSkuRelationship::getAllGroupAdsSKUAdsByDbSKUId(array_values($allGroupAdsSku))->mapWithKeys(function ($item) {
            return [
                $item->channel_sku_creative_id => [
                    'id' => $item->id,
                    'status' => $item->status
                ]
            ];
        })->toArray();

        foreach ($this->crawlGroupAdsSKUData as $skuData) {
            #region Dev logic and mapping data
            $groupAdsId = $skuData['campaign_id'];
            $creativeId = $skuData['creative_id'];
            $skuId = current($skuData['pro_sku'])['title'];
            $status = strtolower($skuData['status']); // Base on group ads status
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;

            // Mapping status
            $status = str_replace('ad group ', '', $status); // Remove text campaign
            $status = str_replace('campaign ', '', $status); // Remove text campaign
            // Handle if have tiki "shit"
            if (!in_array($status, ['paused', 'ended', 'pending', 'removed', 'approved - product shortage', 'approved', 'under review'])) {
                Library\LogError::getInstance()->slack('Tiki (Sku Ads) have new status (shit): ' . $status . ' - channel data: ' . json_encode($skuData), 'epsilo-m-tiki');
                continue;
            }
            if ($status == 'approved' || $status == 'under review') {
                $status = 'ongoing';
            } elseif ($status == 'pending') {
                $status = 'scheduled';
            } elseif ($status == 'removed') {
                $status = 'deleted';
            } elseif ($status == 'approved - product shortage') {
                $status = 'out_of_stock';
            }
            // Note not handle not active status (not_activated)

            // Relationship from epsilo
            $epsiloGroupAdsId = $allGroupAds[$groupAdsId] ?? null;
            $epsiloSkuAdsId = $allGroupAdsSku[$skuId] ?? null;
            if (!$epsiloSkuAdsId) {
                # Tiki have error - don't have sku ads
//                Library\LogError::getInstance()->slack('Tiki (Sku Ads) has some thing error: Channel sku id: ' . $skuId . ' not exit in epsilo', 'epsilo-m-tiki');
                continue;
            }
            if (!$epsiloGroupAdsId) {
                # Tiki have error - don't have group ads
//                Library\LogError::getInstance()->slack('Tiki (Sku Ads) has some thing error: Channel group id: ' . $groupAdsId . ' not exit in epsilo', 'epsilo-m-tiki');
                continue;
            }
            #endregion

            // Build record data
            $recordCollection = collect([
                'status' => $status,
                'updated_at' => $updatedAt,
                'updated_by' => $updatedBy,
            ]);

            if (!array_key_exists($creativeId, $allGroupAdsSkuRelationship)) {
                $recordCollection->put('group_ads_id', $epsiloGroupAdsId);
                $recordCollection->put('sku_id', $epsiloSkuAdsId);
                $recordCollection->put('channel_sku_creative_id', $creativeId);
                $recordCollection->put('created_at', $createdAt);
                $recordCollection->put('created_by', $createdBy);

                $recordInsertCollection->push($recordCollection);
            } else {
                if ($this->_checkHasDiffData($allGroupAdsSkuRelationship[$creativeId], $status)) {
                    Models\ModelBusiness\Tiki\TikiAdsGroupSkuRelationship::updatedById($allGroupAdsSkuRelationship[$creativeId]['id'], $recordCollection->toArray());
                }
            }
        }

        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsGroupSkuRelationship::batchInsert($recordInsertCollection->toArray());
        }

        // Import performance (Job)
        $jobImport = new ImportGroupAdsSkuAdsPerformance($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlGroupAdsSKUData, $this->crawlFromDate, $this->crawlToDate);
        dispatch($jobImport->onQueue(config('queue.tiki.import_data')));
    }

    /**
     * @param array $dbData
     * @param string $status
     * @return bool
     */
    private function _checkHasDiffData($dbData, $status)
    {
        if ($dbData['status'] != $status) {
            return true;
        }
        return false;
    }
}