<?php
namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportKeywordAds extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlKeywordData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlKeywordData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlKeywordData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->timezone = $timezone;
        $this->credentialData = $credentialData;
        $this->crawlKeywordData = $crawlKeywordData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all keyword ads
        $allKeywordAds = Models\ModelBusiness\Tiki\TikiAdsKeyword::getAllKeywordByShopChannelId($this->shopChannelId)->pluck('id', 'channel_keyword_id')->toArray();

        // District channel sku
        $districtKeyword = array_column($this->crawlKeywordData, 'keyword_name', 'keyword_id');

        // Remove all syntax keyword name of tiki
        $districtKeyword = preg_replace('/[\+"\[\]]/', ' ', $districtKeyword);

        // Loop
        foreach ($districtKeyword as $keywordId => $keywordName) {
            // Dev mapping data
            $channelKeywordName = trim($keywordName);
            $channelKeywordId = $keywordId;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;

            // Build record data
            $recordCollection = collect([
                'name' => $channelKeywordName,
                'shop_channel_id' => $this->shopChannelId,
                'channel_keyword_id' => $channelKeywordId,
                'created_at' => $createdAt,
                'created_by' => $createdBy
            ]);

            if (!array_key_exists($keywordId, $allKeywordAds)) {
                $recordInsertCollection->push($recordCollection);
            } else {
                continue;
            }
        }

        // Batch insert
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsKeyword::batchInsert($recordInsertCollection->toArray());
        }

        // Push Job Import Group Ads Keyword Ads
        $jobKeyword = new ImportGroupAdsKeywordAds($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlKeywordData, $this->crawlFromDate, $this->crawlToDate);
        dispatch($jobKeyword->onQueue(config('queue.tiki.import_data')));
    }
}