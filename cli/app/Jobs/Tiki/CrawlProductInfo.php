<?php
namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library\Crawler\Channel\Tiki;
use App\Library;

class CrawlProductInfo extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * CrawlProductInfo constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     */
    public function __construct($shopChannelId, $credentialData, $timezone)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
    }

    public function handle()
    {
        $tikiCrawl = new Tiki($this->shopChannelId);
        $tikiCrawl->setCredential($this->credentialData);

        $data = $tikiCrawl->getProductInfo();

        if (!$data->getIsSuccess()) {
            Library\LogError::getInstance()->slack('Crawl Tiki Product Info Failure - ' . __FUNCTION__, config('slack.tiki'));
        }

        $jobImport = new ImportProductInfo($this->shopChannelId, $this->timezone, $data->getData());
        dispatch($jobImport->onQueue(config('queue.tiki.import_data')));
    }
}