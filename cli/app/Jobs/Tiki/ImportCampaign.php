<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportCampaign extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlCampaignData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlCampaignData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlCampaignData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->timezone = $timezone;
        $this->credentialData = $credentialData;
        $this->crawlCampaignData = $crawlCampaignData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all current campaign by shop in DB [['channel_campaign_id' => [data]],[],...]
        $allDBShopCampaign = Models\ModelBusiness\Tiki\TikiAdsCampaign::getAllCampaignByShopChannelId($this->shopChannelId)->mapWithKeys(function ($item) {
            return [$item->channel_campaign_id => [
                'id' => $item->id,
                'name' => $item->name,
                'status' => $item->status,
                'daily_budget' => $item->daily_budget,
                'from' => $item->from,
                'to' => $item->to
            ]];
        })->toArray();

        // User had removed one or more campaign on SC
        if (count($this->crawlCampaignData) < count($allDBShopCampaign)) {
            $listChannelCampaignId = array_column($this->crawlCampaignData, 'status', 'lineitem_id');
            $listCampaignHadRemove = array_diff_key($allDBShopCampaign, $listChannelCampaignId);
            foreach ($listCampaignHadRemove as $campaginHadRemove) {
                Models\ModelBusiness\Tiki\TikiAdsCampaign::updatedById($campaginHadRemove['id'], ['status' => 'deleted']);
            }
        }

        // Loop all campaign data
        foreach ($this->crawlCampaignData as $campaignData) {

            #region Dev logic and mapping data
            $campaignId = intval($campaignData['lineitem_id']);
            $status = strtolower($campaignData['status']);
            // Handle if have tiki "shit"
            if (!in_array($status, ['eligible', 'paused', 'ended', 'pending'])) {
                Library\LogError::getInstance()->slack('Tiki have new status (shit): ' . $status, 'epsilo-m-tiki');
                continue;
            }
            // Mapping status
            if ($status == 'eligible') {
                $status = 'ongoing';
            } elseif ($status == 'pending') {
                $status = 'scheduled';
            }
            $name = $campaignData['lineitem_name']['title'];
            $dailyBudget = intval($campaignData['daily_budget']);
            $type = $campaignData['targeting_type'];
            $from = date('Y-m-d', strtotime($campaignData['from_date']));
            $to = date('Y-m-d', strtotime($campaignData['to_date']));
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            #endregion

            // Build record data
            $recordCollection = collect([
                'channel_campaign_id' => $campaignId,
                'status' => $status,
                'name' => $name,
                'daily_budget' => $dailyBudget,
                'from' => $from,
                'to' => $to,
                'updated_at' => $updatedAt,
                'updated_by' => $updatedBy,
            ]);

            // Check data is insert or update
            if (!array_key_exists($campaignId, $allDBShopCampaign)) {
                // Add some attributes into insert record
                $recordCollection->put('shop_channel_id', $this->shopChannelId);
                $recordCollection->put('code', null);
                $recordCollection->put('type', $type);
                $recordCollection->put('created_at', $createdAt);
                $recordCollection->put('created_by', $createdBy);

                // Push to collect and wait to batch insert
                $recordInsertCollection->push($recordCollection);
            } else {
                if ($this->_checkHasDiffCampaignData($allDBShopCampaign[$campaignId], $name, $status, $dailyBudget, $from, $to)) {
                    Models\ModelBusiness\Tiki\TikiAdsCampaign::updatedById($allDBShopCampaign[$campaignId]['id'], $recordCollection->toArray());
                }
            }
        }

        // Batch insert campaign info
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsCampaign::batchInsert($recordInsertCollection->toArray());
        }

        //Push new job insert performance
        $jobImportCampaignPerformance = new ImportCampaignPerformance($this->shopChannelId, $this->credentialData, $this->timezone, $this->crawlCampaignData, $this->crawlFromDate, $this->crawlToDate);
        dispatch($jobImportCampaignPerformance->onQueue(config('queue.tiki.import_data')));
    }

    /**
     * @param array $dbData
     * @param string $name
     * @param string $status
     * @param int $budget
     * @param string $from
     * @param string $to
     * @return bool
     */
    private function _checkHasDiffCampaignData($dbData, $name, $status, $budget, $from, $to)
    {
        if ($dbData['name'] != $name) {
            return true;
        } elseif ($dbData['status'] != $status) {
            return true;
        } elseif ($dbData['daily_budget'] != $budget) {
            return true;
        } elseif ($dbData['from'] != $from) {
            return true;
        } elseif ($dbData['to'] != $to) {
            return true;
        }
        return false;
    }
}