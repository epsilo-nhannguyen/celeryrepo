<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;

class CrawlGroupAds extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string Format Y-m-d
     */
    private $dateFrom;

    /**
     * @var string Format Y-m-d
     */
    private $dateTo;

    /**
     * CrawlProductInfo constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param null|string $dateFrom (Y-m-d)
     * @param null|string $dateTo (Y-m-d)
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $dateFrom = null, $dateTo = null)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Prepare data
        $dateFrom = $this->dateFrom ?? date('Y-m-d');
        $dateTo = $this->dateTo ?? date('d-m-Y', strtotime($this->dateFrom . ' + 1 days'));

        // Format date data to (d-m-Y)
        $dateFromFormat = date('d-m-Y', strtotime($dateFrom));
        $dateToFormat = date('d-m-Y', strtotime($dateTo));

        $tikiCrawl = new Library\Crawler\Channel\Tiki($this->shopChannelId);
        $tikiCrawl->setCredential($this->credentialData);

        $data = $tikiCrawl->rawDataAdGroup($dateFromFormat, $dateToFormat);
        if (!$data->getIsSuccess() || count($data->getData()) <= 0) {
            $data = $tikiCrawl->rawDataAdGroup($dateFromFormat, $dateToFormat);
        }
        if (!$data->getIsSuccess() || count($data->getData()) <= 0) {
            $data = $tikiCrawl->rawDataAdGroup($dateFromFormat, $dateToFormat);
        }
        if (!$data->getIsSuccess() || count($data->getData()) <= 0) {
            Library\LogError::getInstance()->slack('Crawl Tiki Group Ads No Data - ' . __FUNCTION__, config('slack.tiki'));
            return;
        }


        if (!$data->getIsSuccess()) {
            Library\LogError::getInstance()->slack('Crawl Tiki Group Ads Failure - ' . __FUNCTION__, config('slack.tiki'));
        }

        $jobImport = new ImportGroupAds($this->shopChannelId, $this->credentialData, $this->timezone, $data->getData() ?? [], $dateFrom, $dateTo);
        dispatch($jobImport->onQueue(config('queue.tiki.import_data')));
    }
}