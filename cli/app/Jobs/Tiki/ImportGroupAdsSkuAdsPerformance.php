<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportGroupAdsSkuAdsPerformance extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlGroupAdsSKUData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlGroupAdsSKUData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlGroupAdsSKUData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->crawlGroupAdsSKUData = $crawlGroupAdsSKUData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all group ads sku by shop channel id
        $allAdsSku = Models\ModelBusiness\Tiki\TikiAdsSku::getAllSkuByShopChannelId($this->shopChannelId)->pluck('id', 'product_shop_item_id')->toArray();

        // Get all relationship group ads sku ads
        $allGroupAdsSkuRelationship = Models\ModelBusiness\Tiki\TikiAdsGroupSkuRelationship::getAllGroupAdsSKUAdsByDbSKUId(array_values($allAdsSku))->pluck('id', 'channel_sku_creative_id')->toArray();

        // Get all record performance by channel sku id and date
        $allGroupAdsSkuPerformance = Models\ModelBusiness\Tiki\TikiAdsGroupAdsSkuAdsPerformance::getAllPerformanceByChannelSkuAdsCreativeIdAndDate(array_keys($allGroupAdsSkuRelationship), $this->crawlFromDate)->mapWithKeys(function ($item) {
            return [$item->channel_sku_creative_id => [
                'id' => $item->id,
                'impression' => $item->impression,
                'click' => $item->click,
                'item_sold' => $item->item_sold,
                'cost' => $item->cost,
                'gmv' => $item->gmv
            ]];
        })->toArray();

        // Loop
        foreach ($this->crawlGroupAdsSKUData as $skuPerformance) {
            #Dev logic mapping data
            $creativeId = $skuPerformance['creative_id'];
            $groupAdsId = $skuPerformance['campaign_id'];
            $skuId = current($skuPerformance['pro_sku'])['title'];
            $impression = $skuPerformance['impression'];
            $click = $skuPerformance['click'];
            $item_sold = $skuPerformance['ot_item_sold_ins'];
            $cost = floatval($skuPerformance['proceeds']);
            $gmv = floatval($skuPerformance['ot_pur_revenue_ins']);

            // Don't care deleted of status deleted
            $status = strtolower($skuPerformance['status']);
            if ($status == 'removed' && !$this->_isHaveData($impression, $click, $item_sold, $cost, $gmv))
                continue;

            $recordCollection = collect([
                'impression' => $impression,
                'click' => $click,
                'item_sold' => $item_sold,
                'cost' => $cost,
                'gmv' => $gmv,
                'updated_at' => Library\Common::getCurrentTimestamp()
            ]);

            // Insert or Update
            if (!array_key_exists($creativeId, $allGroupAdsSkuPerformance)) {
                if (array_key_exists($creativeId, $allGroupAdsSkuRelationship)) {
                    $recordCollection->put('group_ads_sku_id', intval($allGroupAdsSkuRelationship[$creativeId]) ?? null);
                    $recordCollection->put('channel_group_ads_id', $groupAdsId);
                    $recordCollection->put('channel_sku_id', $skuId);
                    $recordCollection->put('channel_sku_creative_id', $creativeId);
                    $recordCollection->put('channel_create_date', $this->crawlFromDate);
                    $recordCollection->put('created_at', Library\Common::getCurrentTimestamp());
                    $recordInsertCollection->push($recordCollection);
                }
            } else {
                // Check has diff in data
                if (!$this->_checkHasDiffData($allGroupAdsSkuPerformance[$creativeId],
                    $impression,
                    $click,
                    $item_sold,
                    $cost,
                    $gmv)) {
                    continue;
                }
                Models\ModelBusiness\Tiki\TikiAdsGroupAdsSkuAdsPerformance::updatedById($allGroupAdsSkuPerformance[$creativeId]['id'], $recordCollection->toArray());
            }
        }

        // Batch insert campaign info
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsGroupAdsSkuAdsPerformance::batchInsert($recordInsertCollection->toArray());
        }
    }

    /**
     * @param array $dbData
     * @param int $impression
     * @param int $click
     * @param int $item_sold
     * @param float $cost
     * @param float $gmv
     * @return bool
     */
    private function _checkHasDiffData($dbData, $impression, $click, $item_sold, $cost, $gmv)
    {
        if ($dbData['impression'] != $impression) {
            return true;
        } elseif ($dbData['click'] != $click) {
            return true;
        } elseif ($dbData['item_sold'] != $item_sold) {
            return true;
        } elseif ($dbData['cost'] != $cost) {
            return true;
        } elseif ($dbData['gmv'] != $gmv) {
            return true;
        }
        return false;
    }

    /**
     * @param int $impression
     * @param int $click
     * @param int $item_sold
     * @param float $cost
     * @param float $gmv
     * @return bool
     */
    private function _isHaveData($impression, $click, $item_sold, $cost, $gmv)
    {
        if ($impression != 0)
            return true;
        elseif ($click != 0)
            return true;
        elseif ($item_sold != 0)
            return true;
        elseif ($cost != 0)
            return true;
        elseif ($gmv != 0)
            return true;
        return false;
    }
}