<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportGroupAdsKeywordAdsPerformance extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $credentialData;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $crawlFromDate;

    /**
     * @var string
     */
    private $crawlToDate;

    /**
     * @var array
     */
    private $crawlGroupAdsKeywordData;

    /**
     * ImportCampaign constructor.
     * @param int $shopChannelId
     * @param array $credentialData
     * @param string $timezone
     * @param array $crawlGroupAdsKeywordData
     * @param string $crawlFromDate
     * @param string $crawlToDate
     */
    public function __construct($shopChannelId, $credentialData, $timezone, $crawlGroupAdsKeywordData, $crawlFromDate, $crawlToDate)
    {
        $this->shopChannelId = $shopChannelId;
        $this->credentialData = $credentialData;
        $this->timezone = $timezone;
        $this->crawlGroupAdsKeywordData = $crawlGroupAdsKeywordData;
        $this->crawlFromDate = $crawlFromDate;
        $this->crawlToDate = $crawlToDate;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Record insert collection to batch insert
        $recordInsertCollection = collect([]);

        // Get all group ads info by shop channel id
        $allGroupAdsInfo = Models\ModelBusiness\Tiki\TikiAdsGroupAds::getAllGroupAdsByShopChannelId($this->shopChannelId)->pluck('id', 'channel_group_ads_id')->toArray();

        // Get all group ads keyword ads by db group ads id
        $allGroupAdsKeywordAds = Models\ModelBusiness\Tiki\TikiAdsGroupKeywordRelationship::getAllGroupAdsKeywordAdsByDbGroupAdsId(array_values($allGroupAdsInfo))->mapWithKeys(function ($item) {
            return [
                $item->channel_group_ads_id . '-' . $item->channel_keyword_id => $item->id
            ];
        })->toArray();

        // Get all performance keyword ads
        $allGroupAdsKeywordAdsPerformance = Models\ModelBusiness\Tiki\TikiAdsGroupAdsKeywordAdsPerformance::getAllPerformanceByChannelGroupAdsAndDate(array_keys($allGroupAdsInfo), $this->crawlFromDate)->mapWithKeys(function ($item) {
            return [$item->channel_group_ads_id . '-' . $item->channel_keyword_id => [
                'id' => $item->id,
                'impression' => $item->impression,
                'click' => $item->click,
                'item_sold' => $item->item_sold,
                'cost' => $item->cost,
                'gmv' => $item->gmv
            ]];
        })->toArray();

        foreach ($this->crawlGroupAdsKeywordData as $keywordData) {
            #region Dev logic and mapping data
            $groupAdsId = $keywordData['campaign_id'];
            $keywordId = $keywordData['keyword_id'];
            $assocGroupAdsId_keywordAdsId = $groupAdsId . '-' . $keywordId;
            $impression = $keywordData['impression'];
            $click = $keywordData['click'];
            $item_sold = $keywordData['ot_item_sold_ins'];
            $cost = floatval($keywordData['proceeds']);
            $gmv = floatval($keywordData['ot_pur_revenue_ins']);

            // Don't care deleted of status deleted
            $status = strtolower($keywordData['status']);
            if ($status == 'removed' && !$this->_isHaveData($impression, $click, $item_sold, $cost, $gmv))
                continue;

            // Relationship from epsilo
            $epsiloGroupAdsKeywordAdsId = $allGroupAdsKeywordAds[$assocGroupAdsId_keywordAdsId] ?? null;
            if (!$epsiloGroupAdsKeywordAdsId) {
                # Tiki have error - don't have group ads keyword ads in epsilo
                #Library\LogError::getInstance()->slack('Tiki (Keyword Ads) has some thing error: Channel group id: ' . $groupAdsId . ' not exit in epsilo', 'epsilo-m-tiki');
                continue;
            }

            $recordCollection = collect([
                'impression' => $impression,
                'click' => $click,
                'item_sold' => $item_sold,
                'cost' => $cost,
                'gmv' => $gmv,
                'updated_at' => Library\Common::getCurrentTimestamp()
            ]);

            if (!array_key_exists($assocGroupAdsId_keywordAdsId, $allGroupAdsKeywordAdsPerformance)) {
                // Handel tiki error
                $haveDuplicateRecord = $recordInsertCollection->where('group_ads_keyword_id', $epsiloGroupAdsKeywordAdsId)->first();
                if ($haveDuplicateRecord) {
                    // Log content duplicate
                    Library\LogError::logErrorToTextFile($this->crawlGroupAdsKeywordData);
                    Library\LogError::getInstance()->slack('Tiki (Keyword Ads) has some thing error: Have duplicate data please check log: ' . $assocGroupAdsId_keywordAdsId, 'epsilo-m-tiki');
                    continue;
                }

                $recordCollection->put('group_ads_keyword_id', $epsiloGroupAdsKeywordAdsId);
                $recordCollection->put('channel_group_ads_id', $groupAdsId);
                $recordCollection->put('channel_keyword_id', $keywordId);
                $recordCollection->put('channel_create_date', $this->crawlFromDate);
                $recordCollection->put('created_at', Library\Common::getCurrentTimestamp());

                $recordInsertCollection->push($recordCollection);
            } else {
                // Check has diff in data
                if (!$this->_checkHasDiffData($allGroupAdsKeywordAdsPerformance[$assocGroupAdsId_keywordAdsId],
                    $impression,
                    $click,
                    $item_sold,
                    $cost,
                    $gmv)) {
                    continue;
                }
                Models\ModelBusiness\Tiki\TikiAdsGroupAdsKeywordAdsPerformance::updatedById($allGroupAdsKeywordAdsPerformance[$assocGroupAdsId_keywordAdsId]['id'], $recordCollection->toArray());
            }
        }

        // Batch insert campaign info
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\Tiki\TikiAdsGroupAdsKeywordAdsPerformance::batchInsert($recordInsertCollection->toArray());
        }
    }

    /**
     * @param array $dbData
     * @param int $impression
     * @param int $click
     * @param int $item_sold
     * @param float $cost
     * @param float $gmv
     * @return bool
     */
    private function _checkHasDiffData($dbData, $impression, $click, $item_sold, $cost, $gmv)
    {
        if ($dbData['impression'] != $impression) {
            return true;
        } elseif ($dbData['click'] != $click) {
            return true;
        } elseif ($dbData['item_sold'] != $item_sold) {
            return true;
        } elseif ($dbData['cost'] != $cost) {
            return true;
        } elseif ($dbData['gmv'] != $gmv) {
            return true;
        }
        return false;
    }

    /**
     * @param int $impression
     * @param int $click
     * @param int $item_sold
     * @param float $cost
     * @param float $gmv
     * @return bool
     */
    private function _isHaveData($impression, $click, $item_sold, $cost, $gmv)
    {
        if ($impression != 0)
            return true;
        elseif ($click != 0)
            return true;
        elseif ($item_sold != 0)
            return true;
        elseif ($cost != 0)
            return true;
        elseif ($gmv != 0)
            return true;
        return false;
    }
}