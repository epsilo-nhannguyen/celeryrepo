<?php

namespace App\Jobs\Tiki;

use App\Jobs\Job;
use App\Library;
use App\Models;

class ImportProductInfo extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var array
     */
    private $crawlProductData;

    /**
     * ImportProductInfo constructor.
     * @param int $shopChannelId
     * @param string $timezone
     * @param array $crawlProductData
     */
    public function __construct($shopChannelId, $timezone, $crawlProductData)
    {
        $this->shopChannelId = $shopChannelId;
        $this->timezone = $timezone;
        $this->crawlProductData = $crawlProductData;
    }

    public function handle()
    {
        // Set timezone
        Library\Common::setTimezone($this->timezone);

        // Hit database and prepare data
        $shopChannelInfo = Models\ModelBusiness\ShopChannel::getById($this->shopChannelId);
        $channelId = $shopChannelInfo->fk_channel;
        $shopMasterInfo = Models\ModelBusiness\ShopMaster::getById($shopChannelInfo->fk_shop_master);
        $channelInfo = Models\ModelBusiness\Channel::getById($channelId);
        $ventureInfo = Models\ModelBusiness\Venture::getById($channelInfo->fk_venture);
        $organizationId = $shopMasterInfo->fk_organization;
        $ventureCode = $ventureInfo->venture_code;
        $channelCode = Models\Channel::TIKI_CODE;

        // Prepare data
        $recordInsertCollection = collect([]);

        // AssocItemId_productId
        $assocItemId_productId = collect([]);
        $itemIdChannelArray = array_column($this->crawlProductData, 'master_id');
        if ($itemIdChannelArray) {
            $assocItemId_productId = Models\ModelBusiness\ProductShopChannel::searchByShopChannelId($this->shopChannelId, $itemIdChannelArray)
                ->pluck('product_shop_channel_id', 'product_shop_channel_item_id');
        }

        // Loop all product response
        foreach ($this->crawlProductData as $productData) {
            // AssocCode_category
            $assocCode_category = collect([]);

            // Get categories
            $categoryData = $productData['categories'];

            // AssocCategoryId_categoryName
            $assocCategoryId_categoryName = array_column($categoryData, 'name', 'id');

            // Last category
            $lastCategoty = null;

            // Build categories data
            foreach ($categoryData as $category) {
                $isUnknowCategory = false;
                if ($category['is_primary'] == true) {
                    $lastCategoty = $category;
                }
                $level = [];
                $categoryIdentify = $category['id'];
                $categoryPath = $category['path'];
                if ($categoryPath == '1/2')
                    continue;
                $categoryPath = explode('/', $categoryPath);
                foreach ($categoryPath as $levelId) {
                    if ($levelId == '1' || $levelId == '2')
                        continue;
                    if (!array_key_exists(intval($levelId), $assocCategoryId_categoryName)) {
                        $isUnknowCategory = true;
                        continue;
                    }
                    $level[] = $assocCategoryId_categoryName[intval($levelId)];
                }
                $categoryName = implode('>', $level);
                $categoryCode = collect([$channelCode, $ventureCode, $categoryIdentify])->implode('-');
                if (!$assocCode_category->has($categoryCode) && !$isUnknowCategory) {
                    $assocCode_category->put($categoryCode, collect([
                        'channel_category_code' => $categoryCode,
                        'channel_category_name' => $categoryName,
                        'channel_category_identify' => $categoryIdentify,
                        'fk_channel' => $channelId,
                        'fk_organization' => $organizationId,
                        'channel_category_level_1' => $level[0] ?? null,
                        'channel_category_level_2' => $level[1] ?? null,
                        'channel_category_level_3' => $level[2] ?? null,
                        'channel_category_level_4' => $level[3] ?? null,
                        'channel_category_level_5' => $level[4] ?? null,
                        'channel_category_created_at' => Library\Common::getCurrentTimestamp(),
                        'channel_category_created_by' => Models\Admin::EPSILO_SYSTEM
                    ]));
                }
            }

            // Check and import category
            $categoryCodeCollection = $assocCode_category->keys();
            $categoryCodeCollectionInDb = Models\ModelBusiness\ChannelCategory::searchByOrganizationIdAndChannelId($organizationId, $channelId, $categoryCodeCollection)
                ->pluck('channel_category_code')->all();

            $categoryInsertCollection = $assocCode_category->filter(function ($item, $key) use ($categoryCodeCollectionInDb) {
                return !in_array($key, $categoryCodeCollectionInDb);
            })->values();

            if ($categoryInsertCollection->count()) {
                Models\ModelBusiness\ChannelCategory::batchInsert($categoryInsertCollection->toArray());
                sleep(1);
            }

            // AssocCategory_codeId
            $assocCategory_codeId = Models\ModelBusiness\ChannelCategory::searchByOrganizationIdAndChannelId($organizationId, $channelId)
                ->pluck('channel_category_id', 'channel_category_code');

            // Build data insert
            $itemId = $productData['sku'];
            $productMasterId = $productData['master_id'];
            $name = $productData['name'];
            $stockAllocationStock = $productData['inventory']['qty_available'];
            $stockFbx = null;
            $stockFbxCollectedAt = null;
            $priceRrpChannel = $productData['list_price'] ?? 0;
            $pricePostsubChannel = $productData['current_price'] ?? null;
            $pulledAt = Library\Common::getCurrentTimestamp();
            $data = json_encode($productData);
            $shopSku = null;
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            $categoryIdentify = $lastCategoty['id'] ?? null;
            $channelCategoryId = $assocCategory_codeId->get(collect([$channelCode, $ventureCode, $categoryIdentify])->implode('-'));

            $recordCollection = collect([
                'product_shop_channel_name' => $name,
                'product_shop_channel_stock_allocation_stock' => $stockAllocationStock,
                'product_shop_channel_stock_fbx' => $stockFbx,
                'product_shop_channel_stock_fbx_collected_at' => $stockFbxCollectedAt,
                'product_shop_channel_price_rrp_channel' => $priceRrpChannel,
                'product_shop_channel_price_postsub_channel' => $pricePostsubChannel,
                'product_shop_channel_pulled_at' => $pulledAt,
                'product_shop_channel_data' => $data,
                'product_shop_channel_updated_at' => $updatedAt,
                'product_shop_channel_updated_by' => $updatedBy,
                'fk_channel_category' => $channelCategoryId,
            ]);

            $productId = $assocItemId_productId->get($productMasterId);
            if ($productId) {
                Models\ModelBusiness\ProductShopChannel::updatedById($productId, $recordCollection->toArray());
            } else {
                $recordCollection->put('fk_shop_channel', $this->shopChannelId);
                $recordCollection->put('product_shop_channel_seller_sku', $productMasterId);
                $recordCollection->put('product_shop_channel_item_id', $productMasterId);
                $recordCollection->put('product_shop_channel_shop_sku', $itemId);
                $recordCollection->put('product_shop_channel_created_at', $createdAt);
                $recordCollection->put('product_shop_channel_created_by', $createdBy);

                $recordInsertCollection->push($recordCollection);
            }

            // Batch insert when have more 10 product
//            if ($recordInsertCollection->count() >= 10) {
//                Models\ModelBusiness\ProductShopChannel::batchInsert($recordInsertCollection->toArray());
//                $recordInsertCollection = collect([]);
//            }
        }

        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\ProductShopChannel::batchInsert($recordInsertCollection->toArray());
        }
    }
}