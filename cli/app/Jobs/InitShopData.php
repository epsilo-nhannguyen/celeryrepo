<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 16/10/2019
 * Time: 15:44
 */

namespace App\Jobs;


use App\Library;
use App\Models;
use App\Services;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;


class InitShopData implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if ( ! $data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);
        $organizationId = intval($data['organizationId']);
        $userId = intval($data['userId']);

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));

        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];
        Library\Common::setTimezoneByVentureId($ventureId);

        $channelCode = $channelInfo[Models\Channel::COL_CHANNEL_CODE];
        if ($channelCode == Models\Channel::SHOPEE_CODE) {
            $api = new Library\Extend\Channel\Shopee($shopId, $channelId);
            $data = $api->getStoreInfo();
            $storeName = $data['shop_name'];
            $code = Services\Store::generateCodeByName($storeName, $organizationId, $ventureId);

            Models\ShopMaster::changeName($storeName, $code, $userId, $shopId, $channelId);
        }

        $message = Models\ChannelCategory::importFromSample($channelId, $organizationId, Models\Admin::EPSILO_SYSTEM);

        if ( ! $message) {
            Models\Mongo\MakSetup::updateSetupDone($shopId, $channelId, Models\Mongo\MakSetup::SETUP_PULL_CATEGORY);
        }

        return;
    }
}