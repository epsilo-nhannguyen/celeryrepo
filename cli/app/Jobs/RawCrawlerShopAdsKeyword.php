<?php

namespace App\Jobs;

use App\Library;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class RawCrawlerShopAdsKeyword implements ShouldQueue
{
    use Queueable, SerializesModels;

    const IS_SHOP_ADS = 3;

    protected $message;

    /**
     * RawCrawlerShopAdsKeyword constructor.
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    public function handle()
    {
        $data = json_decode($this->message, true);
        if (!$data) return;

        print_r($data);
        $shopChannelId = intval($data['shopChannelId']);

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();
        if (!$isLogin) $isLogin = $crawler->login();

        $crawlData = $crawler->crawlShopAdsKeyword();
        $listAdsKeyword = [];
        $shopAdsInfo = [];

        if (!$crawlData || $crawlData['err_code'] != 0)
            return;
        if (!isset($crawlData['data']))
            return;
        if (isset($crawlData['data']['campaign_ads_list']))
            return;

        $campaignAdsList = $crawlData['data']['campaign_ads_list'];

        foreach ($campaignAdsList as $campaign) {
            if (!isset($campaign['advertisements']))
                return;
            if (!isset($campaign['$campaign']))
                return;

            // Get list ads key word & ads id & campaign id - On Advertisements
            foreach ($campaign['advertisements'] as $advertisement) {
                if ($advertisement['placement'] == self::IS_SHOP_ADS) {
                    if (!isset($advertisement['extinfo']) || !isset($advertisement['extinfo']['keywords']))
                        return;
                    $listAdsKeyword = $advertisement['extinfo']['keywords'];
                    unset($advertisement['extinfo']);
                    $shopAdsInfo['advertisement'] = $advertisement;
                }
            }

            // Get shop ads campaign info
            $shopAdsInfo = array_merge($shopAdsInfo, $campaign['$campaign']);
        }

        $finalData = [
            "listAdsKeyword" => $listAdsKeyword,
            "shopAdsInfo" => $shopAdsInfo
        ];

        print_r($finalData);
    }
}