<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 16:50
 */

namespace App\Jobs;

use App\Library;
use App\Models;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;


class RawCrawlerAds implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if ( ! $data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);
        $ventureId = intval($data['ventureId']);
        $shopChannelId = intval($data['shopChannelId']);
        $dateFrom = trim($data['dateFrom']);
        $dateTo = trim($data['dateTo']);
        $arrayAds = (array) $data['arrayAds'] ?? [];

        Library\Common::setTimezoneByVentureId($ventureId);

        $today = date('Y-m-d');
        if ($dateFrom && $dateTo) {
            $arrayDays = Library\Common::dayOfBetween($dateFrom, $dateTo, '+1 day');
        } else {
            $arrayDays = [$today];
        }

        $crawler = new Library\Crawler\Channel\Shopee($shopChannelId);
        $isLogin = false;

        if ( ! $isLogin) $isLogin = $crawler->login();
        if ( ! $isLogin) $isLogin = $crawler->login();
        if ( ! $isLogin) $isLogin = $crawler->login();

        foreach ($arrayDays as $day) {
            $dataInsert = [];
            foreach ($arrayAds as $adsId) {
                $curl = curl_init($crawler->shopeeSellerDomain() . '/api/v2/pas/reports/'.$adsId.'/?start='.$day.'&end='.$day.'&placement=0&is_export=1');
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_COOKIEFILE, $crawler->cookiePath);
                curl_setopt($curl, CURLOPT_COOKIEJAR, $crawler->cookiePath);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

                $result = curl_exec($curl);
                curl_close($curl);

                $dateInsert = $day;
                if ($day == $today) {
                    $dateInsert = date('Y-m-d H:i:s');
                }
                $dataInsert[] = [
                    'shopId' => $shopId,
                    'channelId' => $channelId,
                    'dateHour' => strtotime($dateInsert),
                    'dateHourString' => $dateInsert,
                    'adsId' => trim($adsId),
                    'dataReport' => $result,
                    'isImported' => 0
                ];
            }

            if ( ! $dataInsert) return;
            Models\Mongo\CrawlerAds::batchInsert($dataInsert);
        }

        return;
    }
}