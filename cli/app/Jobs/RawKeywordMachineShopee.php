<?php

namespace App\Jobs;

use App\Library;
use App\Models\MakProgrammaticKeywordProduct;
use App\Models\MakRecommendationKeywordSuggest;
use App\Models\MakRecommendationLogKeywordSuggest;
use App\Models\Mongo\Manager;
use App\Models\ProductShopChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use SebastianBergmann\CodeCoverage\Report\PHP;
use App\Models\MakProgrammaticKeyword;

class RawKeywordMachineShopee implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if (!$data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);
        $shopChannelId = intval($data['shopChannelId']);

        $MLKWShopee = new Library\MLSWShopee($shopId,$channelId,$shopChannelId);
        $MLKWShopee->runSavePriceRange();


    }

}