<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 08/10/2019
 * Time: 11:22
 */

namespace App\Jobs;


use App\Library;
use App\Models;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;


class PullOrder implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if ( ! $data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);
        $updateAtFrom = intval($data['updateAtFrom']);
        $updateAtTo = intval($data['updateAtTo']);
        $isSubAccount = boolval($data['isSubAccount'] ?? 0);

        Library\Common::setTimezoneByChannelId($channelId);

        if ( ! $isSubAccount) {
            /** @var Library\Factory\Channel\Manager $factoryManager*/
            $factoryManager = new Library\Factory\Channel\Manager($shopId, $channelId);
            $factoryManager->import($updateAtFrom, $updateAtTo);
        } else {
            $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));

            $crawlerShopee = new Library\Crawler\Channel\Shopee($shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID]);
            $isLogin = $crawlerShopee->login();
            if ( ! $isLogin) $isLogin = $crawlerShopee->login();
            if ( ! $isLogin) $isLogin = $crawlerShopee->login();
            if ( ! $isLogin) $isLogin = $crawlerShopee->login();

            Library\Factory\Channel\Manager::handleOrder($crawlerShopee->crawSubAccountOrder($updateAtTo), $shopId, $channelId);
        }

        $shopChannelMongo = Models\Mongo\ShopChannel::getByShopIdChannelId($shopId, $channelId);
        if ($shopChannelMongo) {
            Models\Mongo\ShopChannel::updateLastTimePullOrder($shopId, $channelId, $updateAtTo);
        } else {
            Models\Mongo\ShopChannel::insertLastTimePullOrder($shopId, $channelId, $updateAtTo);
        }

        return;
    }
}