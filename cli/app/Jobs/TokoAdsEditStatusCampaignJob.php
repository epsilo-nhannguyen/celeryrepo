<?php

namespace App\Jobs;

use App\Helpers\PusherHelper;
use App\Models\ModelBusiness\TokoAdsCampaign;
use App\Models\ModelBusiness\TokoAdsKeyword;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Pusher\Pusher;

class TokoAdsEditStatusCampaignJob implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $pusherDTO;
    private $groupAdsDTO;
    private $recordId;
    private $params;

    /**
     * TokoAdsEditStatusCampaignJob constructor.
     * @param DTOPusher $pusherDTO
     * @param GroupAdsDTO $groupAdsDTO
     * @param $recordId
     */
    public function __construct($pusherDTO, $groupAdsDTO, $recordId, $params)
    {
        //
        $this->pusherDTO = $pusherDTO;
        $this->groupAdsDTO = $groupAdsDTO;
        $this->recordId = $recordId;
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Pusher\PusherException
     */
    public function handle()
    {
        //
        $params = $this->params;
        $queryResult = TokoAdsCampaign::queryUpdateStatus($this->recordId, $params['value']);
        if ($queryResult) {
            $data = ['type' => 'success'];
        } else {
            $data = ['type' => 'failed'];
        }
        $pusher = new PusherHelper();
        $pusher->triggerAction($this->pusherDTO->getChannelName(), $this->pusherDTO->getEventName(), $data);
    }
}
