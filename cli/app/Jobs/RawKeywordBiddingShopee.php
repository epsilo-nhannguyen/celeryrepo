<?php

namespace App\Jobs;

use App\Library;
use App\Models\MakProgrammaticCampaign;
use App\Models\MakProgrammaticKeywordProduct;
use App\Models\MakRecommendationBiddingPrice;
use App\Models\MakRecommendationKeywordSuggest;
use App\Models\MakRecommendationLogBiddingPrice;
use App\Models\MakRecommendationLogKeywordSuggest;
use App\Models\MakRecommendationPriceRange;
use App\Models\MakRecommendationSkSelect;
use App\Models\MakCrawRanking;
use App\Models\Mongo\Manager;
use App\Models\ProductShopChannel;
use App\Models\ProductShopee;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use SebastianBergmann\CodeCoverage\Report\PHP;
use App\Models\MakProgrammaticKeyword;
use Symfony\Component\Console\Helper\Helper;

class RawKeywordBiddingShopee implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if (!$data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);
        $shopChannelId = intval($data['shopChannelId']);

        $smartCampaign = MakProgrammaticCampaign::getSmartCampaign($shopChannelId);
        if (!empty($smartCampaign)) {
            $ratePercent = $smartCampaign->{MakProgrammaticCampaign::COL_SMART_CAMPAIGN_COVER_RATE_PERCENT};
            // get product Banned
            $dataProductBanned = ProductShopee::getAllProductByShopchannelId($shopChannelId);
            $dataBanned = [];
            foreach ($dataProductBanned as $banned) {
                $productId = $banned->{ProductShopee::COL_FK_PRODUCT_SHOP_CHANNEL};
                array_push($dataBanned, $productId);
            }

            //get SK select
            $dataSKSelect = MakRecommendationSkSelect::getByShopChannel($shopChannelId);
            $dataSK = json_decode($dataSKSelect->{MakRecommendationSkSelect::COL_MAK_RECOMMENDATION_SK_SELECT_JSON}, true);
            $BDMinPrice = env('BIDDING_MIN_PRICE', 500);
            $dataBidding = [];
            $MLKWShopee = new Library\MLSWShopee($shopId, $channelId, $shopChannelId);
            try {
                //get price
                $dataBidding = MakRecommendationBiddingPrice::getByShopChannel($shopChannelId)->toArray();
                $dataPriceOldBD = [];
                if (!empty($dataBidding)) {
                    foreach ($dataBidding as $dbd){
                        $adsIdBD = $dbd[MakRecommendationLogBiddingPrice::COL_FK_ADS_ID];
                        $logJsonPrice = json_decode($dbd[MakRecommendationLogBiddingPrice::COL_MAK_RECOMMENDATION_LOG_BIDDING_PRICE_KEYWORD_JSON], true);
                        foreach ($logJsonPrice as $kw) {
                            $biddingPrice = $kw[1];
                            $keywordProductId = $kw[2];
                            $dataPriceOldBD[$adsIdBD][$keywordProductId] = $biddingPrice;
                        }
                    }
                }
                foreach ($dataSK as $SK) {
                    $kw = $SK['keyword'];
                    foreach ($SK as $i => $item) {
                        if ($i === 'keyword') {
                            continue;
                        }
                        $itemId = $item['itemId'];
                        $kwpId = $item['kwpId'];
                        $sku = $item['sku'];
                        $productId = $item['productId'];
                        $priceLimit = $item['priceLimit'];
                        $adsId = $item['adsId'];
                        //check product banned
                        if (in_array($productId, $dataBanned)) {
                            continue;
                        }
                        // check ranking
                        $ranking = MakCrawRanking::getRank($kw, $sku);
                        if (!empty($ranking) && $ranking->{MakCrawRanking::COL_MAK_CRAW_RANKING_RANK} < 10) {
                            continue;
                        } else {
                            if (!empty($dataPriceOldBD[$adsId][$kwpId])) {
                                $biddingPrice = $dataPriceOldBD[$kwpId] + $BDMinPrice;
                                if ($biddingPrice > $priceLimit) {
                                    $biddingPrice = $priceLimit;
                                }
                                $dataBidding[$adsId][$kw] = [$biddingPrice, $kwpId];
                            } else {
                                $machineOld = MakRecommendationPriceRange::getByKeywordProduct($shopChannelId, $kwpId);
                                if (!empty($machineOld)) {
                                    $priceBDJson = $machineOld->{MakRecommendationPriceRange::COL_MAK_RECOMMENDATION_PRICE_RANGE_JSON};
                                    $priceBDRate = Library\Formater::convertTotalRangePricePerPercent($priceBDJson);
                                    $priceBD = $priceBDRate['percent_' . $ratePercent];
                                    if ($priceBD < $BDMinPrice) {
                                        $priceBD = $BDMinPrice;
                                    }

                                    if ($priceBD > $priceLimit) {
                                        $priceBD = $priceLimit;
                                    }
                                    $dataBidding[$adsId][$kw] = [$priceBD, $kwpId];
                                } else {
                                    $priceBDJson = $MLKWShopee->getPriceRange($kw, $shopId, $itemId, $MLKWShopee->bidding);
                                    $priceBDJson = json_encode($priceBDJson);
                                    $priceBDRate = Library\Formater::convertTotalRangePricePerPercent($priceBDJson);
                                    $priceBD = $priceBDRate['percent_' . $ratePercent];
                                    if ($priceBD < $BDMinPrice) {
                                        $priceBD = $BDMinPrice;
                                    }

                                    if ($priceBD > $priceLimit) {
                                        $priceBD = $priceLimit;
                                    }
                                    $dataBidding[$adsId][$kw] = [$priceBD, $kwpId];
                                }
                            }

                        }

                    }
                }
                if(!empty($dataBidding)){
                    foreach ($dataBidding as $adsId => $item){
                        $MLKWShopee->modifyBiddingPrice($adsId,$item);
                    }
                }

            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }

        }


    }

}