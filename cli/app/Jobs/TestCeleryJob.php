<?php
namespace App\Jobs;

class TestCeleryJob extends Job
{
    protected $x;
    protected $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function handle()
    {
        $result = intval($this->x) + intval($this->y);
        echo $result . PHP_EOL;
    }
}