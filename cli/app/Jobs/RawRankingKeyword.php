<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 30/09/2019
 * Time: 16:51
 */

namespace App\Jobs;


use App\Library;
use App\Models;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;


class RawRankingKeyword implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if ( ! $data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);
        $shopeeShopId = $data['shopeeShopId'];
        $dataKeyword = $data['dataKeyword'];
        $totalPage = intval($data['totalPage'] ?? 1);

        if ( ! $shopeeShopId || ! $dataKeyword) return;

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        $ventureId = $channelInfo[Models\Channel::COL_FK_VENTURE];
        Library\Common::setTimezoneByVentureId($ventureId);

        $channelCode = $channelInfo[Models\Channel::COL_CHANNEL_CODE];
        $arrayInsert = [];

        foreach ($dataKeyword as $keywordName => $arrayItemIdNeed) {
            if ( ! $arrayItemIdNeed) continue;

            switch (strtoupper($channelCode)) {
                case Models\Channel::SHOPEE_CODE:
                    $assocItemIdRank = Library\Crawler\Channel\Frontend\Shopee::findProductRank(
                        $ventureId, $keywordName, $totalPage, $arrayItemIdNeed, $shopeeShopId
                    );

                    if ($assocItemIdRank) {
                        $arrayInsert[] = Models\Mongo\MakRankingKeyword::buildDocument($channelId, $shopId, $keywordName, $assocItemIdRank);
                    }

                    break;

                default:
                    break;
            }
        }

        if ( ! $arrayInsert) return;
        $arrayInsertChunk = array_chunk($arrayInsert, 200);
        foreach ($arrayInsertChunk as $chunk) {
            Models\Mongo\MakRankingKeyword::batchInsert($chunk);
        }

        return;
    }
}