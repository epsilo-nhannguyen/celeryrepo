<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 08/10/2019
 * Time: 11:26
 */

namespace App\Jobs;


use App\Library;
use App\Models;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;


class PullProduct implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if ( ! $data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);
        $arrayItemId = $data['arrayItemId'] ?? [];
        $arrayItemId = array_map('trim', (array) $arrayItemId);
        $updateAtFrom = intval($data['updateAtFrom'] ?? 0);
        $updateAtTo = intval($data['updateAtTo'] ?? 0);
        $isSubAccount = boolval($data['isSubAccount'] ?? 0);

        Library\Common::setTimezoneByChannelId($channelId);

        $channelInfo = Library\Formater::stdClassToArray(Models\Channel::getById($channelId));
        $channelCode = $channelInfo[Models\Channel::COL_CHANNEL_CODE];

        if ($channelCode == Models\Channel::SHOPEE_CODE) {
            $allItem = [];

            if ( ! $isSubAccount) {
                $apiShopee = new Library\Extend\Channel\Shopee($shopId, $channelId);
                foreach ($arrayItemId as $itemId) {
                    $itemDetailResponse = $apiShopee->getItemDetail($itemId);
                    if ( ! $itemDetailResponse) continue;

                    $channelProduct = new Library\DataStructure\ChannelProduct();
                    $_itemSku = $itemDetailResponse['item']['item_sku'];
                    $_itemId = $itemDetailResponse['item']['item_id'];
                    if (!$_itemSku) $_itemSku = $_itemId;

                    $channelProduct->setSku($_itemSku);
                    $channelProduct->setAllocationStock($itemDetailResponse['item']['stock']);
                    $channelProduct->setFbxStock(null);
                    $channelProduct->setChannelIdentify($_itemId);
                    $channelProduct->setName($itemDetailResponse['item']['name']);
                    $channelProduct->setRrp($itemDetailResponse['item']['original_price']);
                    $channelProduct->setSellingPrice($itemDetailResponse['item']['price']);
                    $channelProduct->setChannelCategory($itemDetailResponse['item']['category_id']);
                    $channelProduct->setCreatedAt($itemDetailResponse['item']['create_time']);
                    $channelProduct->setUpdatedAt($itemDetailResponse['item']['update_time']);
                    $channelProduct->setRawData(json_encode($itemDetailResponse));
                    $allItem[] = $channelProduct;
                }
            } else {
                $shopChannelInfo = Library\Formater::stdClassToArray(Models\ShopChannel::getByShopIdChannelId($shopId, $channelId));

                $crawlerShopee = new Library\Crawler\Channel\Shopee($shopChannelInfo[Models\ShopChannel::COL_SHOP_CHANNEL_ID]);
                $isLogin = $crawlerShopee->login();
                if ( ! $isLogin) $isLogin = $crawlerShopee->login();
                if ( ! $isLogin) $isLogin = $crawlerShopee->login();
                if ( ! $isLogin) $isLogin = $crawlerShopee->login();

                $data = $crawlerShopee->crawSubAccountProduct();

                foreach ($data as $itemDetailResponse) {
                    $channelProduct = new Library\DataStructure\ChannelProduct();
                    $rrp = $itemDetailResponse['price_before_discount'] ?? 0;
                    $price = $itemDetailResponse['price'] ?? 0;
                    if (!$rrp) $rrp = $price;
                    if (!$itemDetailResponse['parent_sku']) $itemDetailResponse['parent_sku'] = $itemDetailResponse['id'];

                    $arrayItemId[] = $itemDetailResponse['id'];
                    $category = end($itemDetailResponse['category_path']);
                    $channelProduct->setSku($itemDetailResponse['parent_sku']);
                    $channelProduct->setAllocationStock($itemDetailResponse['stock']);
                    $channelProduct->setFbxStock(null);
                    $channelProduct->setChannelIdentify($itemDetailResponse['id']);
                    $channelProduct->setName($itemDetailResponse['name']);
                    $channelProduct->setRrp($rrp);
                    $channelProduct->setSellingPrice($price);
                    $channelProduct->setChannelCategory($category);
                    $channelProduct->setCreatedAt($itemDetailResponse['create_time']);
                    $channelProduct->setUpdatedAt($itemDetailResponse['modify_time']);
                    if ($itemDetailResponse['status'] == 1) {
                        $itemDetailResponse['item']['status'] = 'NORMAL';
                    } else {
                        $itemDetailResponse['item']['status'] = 'UNLIST';
                    }
                    $channelProduct->setRawData(json_encode($itemDetailResponse));
                    $allItem[] = $channelProduct;
                }
            }
            $itemIdInDb = array_column(Models\Mongo\Product::searchByIdentifyArray($shopId, $channelId, $arrayItemId), 'identify');

            $identifyNeedInsert = array_diff($arrayItemId, $itemIdInDb);
            $dataInsert = [];

            foreach ($allItem as $product) {
                /** @var Library\DataStructure\ChannelProduct $product */
                if (in_array($product->getChannelIdentify(), $identifyNeedInsert)) {
                    $dataInsert[] = [
                        'shopId' => $shopId,
                        'channelId' => $channelId,
                        'identify' => $product->getChannelIdentify(),
                        'isImported' => 0,
                        'updatedAt' => Library\Common::getCurrentTimestamp(),
                        'data' => $product->extractArray()
                    ];
                } else {
                    Models\Mongo\Product::save($shopId, $channelId, $product, false);
                }
            }

            if ($dataInsert) {
                Models\Mongo\Product::batchInsert($dataInsert);
            }
        } else {

            $factoryManager = new Library\Factory\Channel\Manager($shopId, $channelId);
            $productArray = $factoryManager->pullAllProduct($updateAtFrom, $updateAtTo);

            foreach ($productArray as $product) {
                /** @var Library\DataStructure\ChannelProduct $product */
                Models\Mongo\Product::save($shopId, $channelId, $product, true);
            }

            $shopChannelMongo = Models\Mongo\ShopChannel::getByShopIdChannelId($shopId, $channelId);
            if ($shopChannelMongo) {
                Models\Mongo\ShopChannel::updateLastTimePullProduct($shopId, $channelId, $updateAtTo);
            } else {
                Models\Mongo\ShopChannel::insertLastTimePullProduct($shopId, $channelId, $updateAtTo);
            }
        }

        return;
    }
}