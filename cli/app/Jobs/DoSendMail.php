<?php

namespace App\Jobs;

use App\Library;
use Illuminate\Support\Facades\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class DoSendMail implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if ( ! $data) return;

        $email = $data['email'];
        $fullName = $data['fullName'];
        $subject = $data['subject'];
        $pathTemplate = $data['pathTemplate']; #ex: email.user
        $assocKeyData = (array) $data['assocKeyData'];
        $pathAttachmentFileArray = (array) $data['pathAttachmentFileArray'];

        Mail::to($email, $fullName)->send(new Library\Email($subject, $pathTemplate, $assocKeyData, $pathAttachmentFileArray));

        return;
    }
}