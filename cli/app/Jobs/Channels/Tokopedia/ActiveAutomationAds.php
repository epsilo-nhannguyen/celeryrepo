<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Jobs;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO;
use App\Models;

class ActiveAutomationAds extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var GroupAdsDTO
     */
    private $groupAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * @var int
     */
    private $recordId;

    private $params;

    private $object;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId
     * @param $groupAdsDTO
     * @param $pusherDTO
     * @param $recordId
     * @param $params
     * @param $object
     */
    public function __construct($shopChannelId, $groupAdsDTO, $pusherDTO, $recordId = null, $params = null, $object = null)
    {
        $this->shopChannelId = $shopChannelId;
        $this->groupAdsDTO = $groupAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->recordId = $recordId;
        $this->params = $params;
        $this->object = $object;
    }

    public function handle()
    {
        // Verify Job Request Data
        if (!$this->shopChannelId) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->groupAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }

        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess()) {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->activeAutoAds($this->groupAdsDTO);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        // Push job to queue when success
        $object = json_decode($this->object ?? '{}', true);
        $job = new Jobs\Toko\KeywordAds\EditStatusCampaign($this->recordId, $this->object, $this->pusherDTO);
        dispatch($job->onQueue('tokopedia_save'));
    }
}