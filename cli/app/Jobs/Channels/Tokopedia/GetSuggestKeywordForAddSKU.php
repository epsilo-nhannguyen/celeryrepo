<?php
namespace App\Jobs\Channels\Tokopedia;

use App\Helpers\PusherHelper;
use App\Jobs\Job;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Models;

class GetSuggestKeywordForAddSKU extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var array
     */
    private $arrayProductId;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId int
     * @param $arrayProductId array
     * @param $pusherDTO DTOPusher
     */
    public function __construct($shopChannelId, $arrayProductId, $pusherDTO)
    {
        $this->shopChannelId = $shopChannelId;
        $this->arrayProductId = $arrayProductId;
        $this->pusherDTO = $pusherDTO;
    }

    public function handle()
    {
        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess())
        {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->getSuggestKeywordForAddSKU($this->arrayProductId);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        $keywordRecommendChannel = $data->getData();

        $keywordRecommend = collect([]);
        foreach ($keywordRecommendChannel as $keyword) {
            $item = collect([
                'keywordName' => $keyword['keyword'],
                'searchVolume' => $keyword['total_search'],
                'biddingPrice' => $keyword['bid_suggest']
            ]);

            $keywordRecommend->push($item);
        }

        $pusherDto = new PusherHelper();
        $pusherDto->triggerAction($this->pusherDTO->getChannelName(), $this->pusherDTO->getEventName(), $keywordRecommend);
    }
}