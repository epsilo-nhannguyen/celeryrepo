<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Application\Constant\ConfigActive as Constant_ConfigActive;
use App\Application\Functions\Strings;
use App\Jobs\Job;
use App\Library\Cache;
use App\Library\Crawler\Channel\TokopediaAuthentication;
use App\Library\CurlBuilder;
use App\Models\ModelBusiness\Admin;
use App\Models\ModelBusiness\Channel;
use App\Models\ModelBusiness\ShopChannel;
use App\Models\ModelBusiness\ShopChannelUser;
use App\Models\ModelBusiness\ShopMaster;
use App\Models\ModelBusiness\Venture;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Helpers\PusherHelper;
use Carbon\Carbon;
use Exception;

class VerifyAccount extends Job
{
    use JobHandlerTrait;

    #region Init Variable
    /**
     * @var
     */
    private $email;

    /**
     * @var
     */
    private $username;

    /**
     * @var
     */
    private $password;

    /**
     * @var
     */
    private $channelCode;

    /**
     * @var
     */
    private $ventureId;

    /**
     * @var
     */
    private $isVerifyByPhone;

    /**
     * @var
     */
    private $urlPassportAPI;

    /**
     * @var 
     */
    private $emailPassportAPI;

    /**
     * @var 
     */
    private $passwordPassportAPI;

    /**
     * @var string
     */
    private $storeName;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * Create a new job instance.
     * Authentication account constructor.
     *
     * @param $email
     * @param $username
     * @param $password
     * @param $channelCode
     * @param $ventureId
     * @param $isVerifyByPhone
     */
    public function __construct($email, $username, $password, $channelCode, $ventureId, $isVerifyByPhone, $pusherDTO)
    {
        $this->email = $email;
        $this->username = $username;
        $this->password = $password;
        $this->channelCode = $channelCode;
        $this->ventureId = $ventureId;
        $this->isVerifyByPhone = $isVerifyByPhone;
        $this->pusherDTO = $pusherDTO;

        $this->urlPassportAPI = env('URL_PASSPORT_API', 'https://api-staging-passport.epsilo.io');
        $this->emailPassportAPI = env('EMAIL_PASSPORT_API', 'epsilo-m@aad.asia');
        $this->passwordPassportAPI = env('PASSWORD_PASSPORT_API', '123456');

        $this->storeName = 'Untitled Store';
    }
    #endregion

    #region Main
    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        try {
            // Verify Job Request Params
            if (!$this->_checkRequestParam()) {
                return;
            }

            // Get user information by email
            $userInfo = Admin::getByEmail($this->email);
            // Check email have match with exist any account in epsilo
            if (empty($userInfo)) {
                $message = app('translator')->get('This email doesn’t match any account. Please try again or sign up if you are new to Epsilo.');
                $this->jobFailure($message);
                return;
            }

            // get channel information
            $channelInfo = Channel::getByChannelCodeAndVentureId($this->channelCode, $this->ventureId);
            if (empty($channelInfo)) {
                $message = app('translator')->get('This channel does not exist. Please try again.');
                $this->jobFailure($message);
                return;
            }

            // Set channel_id
            $channelId = $channelInfo->{Channel::COL_CHANNEL_ID};

            // Get shop channel information
            $shopChannels = ShopChannel::getByChannelAndVenture($this->channelCode, $this->ventureId);

            // Check shop channel linked
            $rsMessage = $this->_checkShopChannelLinked($shopChannels);

            // return when result not null
            if(!empty($rsMessage)) {
                $this->jobFailure($rsMessage);
                return;
            }

            // Tokopedia Authentication
            $tokopedia = new TokopediaAuthentication($this->username, $this->password, $this->ventureId);

            // check account exist and send OTP for user
            $resultAuth = $tokopedia->triggerSendOTP();

            // result verify account
            if (!$resultAuth->getIsSuccess()) {
                $this->jobFailure($resultAuth->getMessage());
                return;
            }

            // Set init shop_channel_id
            $shopChannelId = null;

            // Insert new shop passport
            $rsMessage = $this->_createShopPassport();

            // result
            if(!$rsMessage["isSuccess"]) {
                $this->jobFailure($rsMessage["message"]);
                return;
            }

            // Set id for shop_channel_id
            $shopChannelId = $rsMessage["data"]['shopChannelId'];

            // set param shopMaster
            $shopMasterInfo = $this->_setShopMasterInfo($userInfo);

            // set param shopChannel
            $shopChannelInfo = $this->_setShopChannelInfo($channelId, $shopChannelId, $userInfo);

            // set param shopChannelUser
            $shopChannelUserInfo = $this->_setShopChannelUserInfo($userInfo);

            // create new shop authentication in shopMaster
            $rsInsert = $this->_insertShopMaster($shopMasterInfo, $shopChannelInfo, $shopChannelUserInfo);

            // Return message error when insert fail
            if (!empty($rsInsert)) {
                $this->jobFailure($rsInsert);
                return;
            }

            $data = [
                "channelId" =>$channelId,
                "shopChannelId" => $shopChannelId
            ];
        } catch (\Exception $exception) {
            $message = app('translator')->get($exception->getMessage());
            $this->jobFailure($message);
            return;
        }

        // Success verify account
        $this->jobSuccess($data);
    }

    /**
     * @return bool
     */
    private function _checkRequestParam()
    {
        if (!$this->email) {
            $this->jobFailure('Missing params');
            return false;
        }

        if (!$this->username) {
            $this->jobFailure('Missing params');
            return false;
        }

        // Check password when verify by email
        if (!$this->isVerifyByPhone) {
            if (!$this->password) {
                $this->jobFailure('Missing params');
                return false;
            }
        }

        if (!$this->channelCode) {
            $this->jobFailure('Missing params');
            return false;
        }

        if (!$this->ventureId) {
            $this->jobFailure('Missing params');
            return false;
        }

        return true;
    }
    /**
     * @param $shopChannels
     * @return mixed|null
     */
    private function _checkShopChannelLinked($shopChannels)
    {
        $result = null;
        $shopChannelInfo = null;

        foreach ($shopChannels as $sc) {
            $credential = json_decode($sc->{ShopChannel::COL_SHOP_CHANNEL_API_CREDENTIAL}, true);
            if (!empty($credential) && array_key_exists('username', $credential)) {
                if ($credential['username'] == $this->username) {
                    $shopChannelInfo = $sc;
                    break;
                }
            }
        }

        if (!empty($shopChannelInfo)) {
            if ($shopChannelInfo->{ShopChannel::COL_SHOP_CHANNEL_LINKED} ||
                (!$shopChannelInfo->{ShopChannel::COL_SHOP_CHANNEL_LINKED} && $this->email != $shopChannelInfo->{Admin::COL_ADMIN_EMAIL})
            ) {
                $email = Strings::maskEmail($shopChannelInfo->{Admin::COL_ADMIN_EMAIL});
                $result = app('translator')->get('This '.ShopChannel::COL_SHOP_CHANNEL_NAME.' account has been connected to an existed Epsilo account ' . $email . ' already. You can sign in here.');
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @throws \Pusher\PusherException
     */
    private function jobSuccess($data)
    {
        print_r('Success!!!');
        echo PHP_EOL;

        $pusher = new PusherHelper();
        $data = [
            'data' => $data,
            'message' => '',
            'type' => 'success'
        ];
        $channelName = $this->pusherDTO->getChannelName();
        $eventName = $this->pusherDTO->getEventName();
        $pusher->triggerAction($channelName, $eventName, $data);
    }
    #endregion

    #region Insert Passport
    /**
     * @return array
     * @throws \App\Application\Exceptions\PassportException
     */
    private function _createShopPassport()
    {
        $result = [
            "isSuccess" => true,
            "message" => '',
            "data" => ''
        ];

        // Get venture information
        $ventureInfo = Venture::getByIdFromCache($this->ventureId);

        $params = [
            'shopName' => $this->storeName,
            'orgEmail' => $this->email,
            'user' => $this->email,
            'channel' => $this->channelCode,
            'country' => $ventureInfo[Venture::COL_VENTURE_CODE]
        ];

        $response = $this->_callPassport('/v1/shop/create', 'POST', $params);

        if ($response['code'] == 0 || empty($response['data'])) {
            $result["isSuccess"] = false;
            $result["message"] = app('translator')->get('There was an error. Please try again.');
        }

        $result["data"] = $response['data'];

        return $result;
    }

    /**
     * @param string $action
     * @param string $method
     * @param array $params
     * @return bool|mixed|string
     */
    private function _callPassport($action, $method, $params)
    {
        $method = strtoupper(trim($method));
        $requestUrl = $this->urlPassportAPI . $action;
        $curl = new CurlBuilder();
        $curl->setUrl($requestUrl);
        $curl->setMethod($method);
        $curl->setParam($params);
        $curl->isJson();
        $curl->header('Accept: application/json');
        $curl->header('Authorization: Bearer ' . $this->_getToken());
        $curl->setTimeout(5);
        return $curl->execute(true);
    }

    /**
     * @return string
     */
    private function _getToken()
    {
        $tokenKey = Cache::getInstance()->tokenPassport();
        $token = Cache::getInstance()->get($tokenKey);
        if (!$token) {
            $params = [
                'email' => $this->emailPassportAPI,
                'password' => $this->passwordPassportAPI,
            ];

            $curl = new CurlBuilder();
            $curl->setUrl($this->urlPassportAPI . '/auth/login');
            $curl->setMethod('POST');
            $curl->setParam($params);
            $curl->isJson();
            $curl->header('Accept: application/json');
            $curl->setTimeout(3);
            $response = $curl->execute(true);

            $expirationTime = $response['data']['expiration_date'] ?? strtotime('+30 days');
            $timeLife = $expirationTime - time();

            $token = $response['data']['token'] ?? '';

            if ($token) {
                Cache::getInstance()->save($tokenKey, $token, $timeLife);
            }
        }
        return $token;
    }
    #endregion

    #region Insert ShopMaster
    /**
     * @param $shopMasterInfo
     * @param $shopChannelInfo
     * @param $shopChannelUserInfo
     * @return array
     */
    private function _insertShopMaster($shopMasterInfo, $shopChannelInfo, $shopChannelUserInfo)
    {
        $message = null;

        // Insert new shop authentication (new shop need link)
        $message = ShopMaster::insertNewStore($shopMasterInfo, $shopChannelInfo, $shopChannelUserInfo);
        if ($message) {
            $message = app('translator')->get('Server have some error! Please alert to developer.');
        }

        return $message;
    }

    /**
     * @param $userInfo
     * @return array
     */
    private function _setShopMasterInfo($userInfo)
    {
        $shopMasterCode = null;
        return $shopMasterInfo = [
            ShopMaster::COL_FK_CONFIG_ACTIVE => Constant_ConfigActive::ACTIVE,
            ShopMaster::COL_SHOP_MASTER_NAME => $this->storeName,
            ShopMaster::COL_SHOP_MASTER_CODE => $shopMasterCode,
            ShopMaster::COL_FK_VENTURE => $this->ventureId,
            ShopMaster::COL_SHOP_MASTER_CREATED_BY => $userInfo->{Admin::COL_ADMIN_ID},
            ShopMaster::COL_SHOP_MASTER_CREATED_AT => Carbon::now()->timestamp,
            ShopMaster::COL_FK_ORGANIZATION => $userInfo->{Admin::COL_FK_ORGANIZATION},
        ];
    }

    /**
     * @param $channelId
     * @param $shopChannelId
     * @param $userInfo
     * @return array
     */
    private function _setShopChannelInfo($channelId, $shopChannelId, $userInfo)
    {
        $shopChannelInfo = [];
        $credential = json_encode(['username' => $this->username, 'password' => $this->password]);

        if (!empty($shopChannelId)) {
            $shopChannelInfo[ShopChannel::COL_SHOP_CHANNEL_ID] = $shopChannelId;
        }

        $shopChannelInfo[ShopChannel::COL_SHOP_CHANNEL_NAME] = $this->storeName;
        $shopChannelInfo[ShopChannel::COL_FK_CHANNEL] = $channelId;
        $shopChannelInfo[ShopChannel::COL_SHOP_CHANNEL_LINKED] = 0;
        $shopChannelInfo[ShopChannel::COL_SHOP_CHANNEL_INIT] = 1;
        $shopChannelInfo[ShopChannel::COL_FK_CONFIG_ACTIVE] = Constant_ConfigActive::INACTIVE;
        $shopChannelInfo[ShopChannel::COL_SHOP_CHANNEL_CREATED_AT] = Carbon::now()->timestamp;
        $shopChannelInfo[ShopChannel::COL_SHOP_CHANNEL_CREATED_BY] = $userInfo->{Admin::COL_ADMIN_ID};
        $shopChannelInfo[ShopChannel::COL_SHOP_CHANNEL_API_CREDENTIAL] = $credential;
        $shopChannelInfo[ShopChannel::COL_SHOP_CHANNEL_API_CREDENTIAL_UPDATED_AT] = Carbon::now()->timestamp;

        return $shopChannelInfo;
    }

    /**
     * @param $userInfo
     * @return array
     */
    private function _setShopChannelUserInfo($userInfo)
    {
        return $shopChannelUserInfo = [
            ShopChannelUser::COL_FK_USER => $userInfo->{Admin::COL_ADMIN_ID},
            ShopChannelUser::COL_SHOP_CHANNEL_USER_CREATED_BY => $userInfo->{Admin::COL_ADMIN_ID},
            ShopChannelUser::COL_SHOP_CHANNEL_USER_CREATED_AT => Carbon::now()->timestamp,
        ];
    }
    #endregion
}