<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Helpers\PusherHelper;

trait JobHandlerTrait
{
    public function jobFailure($message)
    {
        print_r('Failure!!!');
        echo PHP_EOL;
        print_r($message);
        echo PHP_EOL;

        if ($this->pusherDTO) {
            $pusher = new PusherHelper();
            $data = [
                'data' => [],
                'message' => $message,
                'type' => 'error'
            ];
            $channelName = $this->pusherDTO->getChannelName();
            $eventName = $this->pusherDTO->getEventName();
            $pusher->triggerAction($channelName, $eventName, $data);
        }
    }
}