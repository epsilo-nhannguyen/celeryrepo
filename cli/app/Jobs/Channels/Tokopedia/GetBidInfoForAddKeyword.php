<?php
namespace App\Jobs\Channels\Tokopedia;

use App\Helpers\PusherHelper;
use App\Jobs\Job;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use App\Models;

class GetBidInfoForAddKeyword extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var GroupAdsDTO
     */
    private $groupAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    private $keywordData;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId
     * @param $groupAdsDTO
     * @param $pusherDTO
     * @param $keywordData
     */
    public function __construct($shopChannelId, $groupAdsDTO, $pusherDTO, $keywordData = [])
    {
        $this->shopChannelId = $shopChannelId;
        $this->groupAdsDTO = $groupAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->keywordData = $keywordData;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess())
        {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->getBidInfoKeyWord($this->groupAdsDTO->getGroupId());

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        $biddingPriceResponse = $data->getData();
        $biddingPrice = current(current($biddingPriceResponse))['min_bid'] ?? 0;

        $keywordResponse = collect([]);
        foreach ($this->keywordData as $keywordName) {
            $keywordResponse->put($keywordName, $biddingPrice);
        }

        $pusher = new PusherHelper();
        $pusher->triggerAction($this->pusherDTO->getChannelName(), $this->pusherDTO->getEventName(), $keywordResponse);
    }
}