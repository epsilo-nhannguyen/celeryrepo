<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Jobs;
use App\Jobs\Job;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO;
use App\Models;

class ActiveDeactivateKeywordInGroupAds extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var KeywordAdsDTO
     */
    private $keywordAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * @var int
     */
    private $recordId;

    /**
     * @var null | mixed
     */
    private $params;

    private $object;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId
     * @param $keywordAdsDTO
     * @param $pusherDTO
     * @param $recordId
     * @param $params
     * @param $object
     */
    public function __construct($shopChannelId, $keywordAdsDTO, $pusherDTO, $recordId, $params = null, $object = null)
    {
        $this->shopChannelId = $shopChannelId;
        $this->keywordAdsDTO = $keywordAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->recordId = $recordId;
        $this->params = $params;
        $this->object = $object;
    }

    public function handle()
    {
        // Verify Job Request Data
        if (!$this->shopChannelId) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->keywordAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->recordId) {
            $this->jobFailure('Missing params');
            return;
        }

        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess()) {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->activeOrDeactivateKeyword($this->keywordAdsDTO);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        $object = json_decode($this->object ?? '{}', true);
        $isInTerminal = $object['isInTerminal'] ?? 0;
        if ($isInTerminal) {
            $job = new Jobs\Toko\KeywordAds\EditStatusKeyword($this->recordId, $this->object);
            dispatch($job->onQueue('tokopedia_save'));
        } else {
            $job = new Jobs\TokoAdsEditStatusKeywordJob($this->pusherDTO, $this->params);
            dispatch($job->onQueue('tokopedia_save'));
        }
    }
}