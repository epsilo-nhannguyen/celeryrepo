<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Jobs;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\ProductAdsDTO;
use App\Models;

class CreateGroupAds extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var GroupAdsDTO
     */
    private $groupAdsDTO;

    /**
     * @var ProductAdsDTO[]
     */
    private $arrayProductAdsDTO;

    /**
     * @var KeywordAdsDTO[]
     */
    private $arrayKeywordAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    private $recordId;

    private $params;

    private $object;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId
     * @param $groupAdsDTO
     * @param $arrayProductAdsDTO
     * @param $pusherDTO
     * @param $recordId
     * @param $params
     * @param $object
     * @param $arrayKeywordAdsDTO
     */
    public function __construct($shopChannelId, $groupAdsDTO, $arrayProductAdsDTO, $pusherDTO, $recordId = null, $params = null, $object = null, $arrayKeywordAdsDTO = [])
    {
        $this->shopChannelId = $shopChannelId;
        $this->groupAdsDTO = $groupAdsDTO;
        $this->arrayProductAdsDTO = $arrayProductAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->recordId = $recordId;
        $this->params = $params;
        $this->object = $object;
        $this->arrayKeywordAdsDTO = $arrayKeywordAdsDTO;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        // Verify Job Request Data
        if (!$this->shopChannelId) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->groupAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->arrayProductAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }

        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess())
        {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->createGroupAds($this->groupAdsDTO, $this->arrayProductAdsDTO);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        $responseData = $data->getData();
        $campaignData = $responseData['data'] ?? [];
        $channelCampaignId = $campaignData['group_id'] ?? 0;
        $object = json_decode($this->object ?? '{}', true);

        $job = new Jobs\Toko\KeywordAds\UpdateCampaign($this->recordId, $this->object, null, $data);
        dispatch($job->onQueue('tokopedia_save'));

        $this->groupAdsDTO->setGroupId(strval($channelCampaignId));
        $job = new AddKeywordToGroupAds(
            $this->shopChannelId, $this->groupAdsDTO, $this->arrayKeywordAdsDTO, null, $this->recordId, null, $this->object
        );
        dispatch($job->onQueue('tokopedia_simulation'));
    }
}