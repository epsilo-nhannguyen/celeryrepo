<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Library\Crawler\Channel\TokopediaAuthentication;
use App\Models\ModelBusiness\ShopChannel;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Helpers\PusherHelper;
use Exception;

class VerifyOTP extends Job
{
    use JobHandlerTrait;

    #region Init Variable
    /**
     * @var
     */
    private $otp;

    /**
     * @var
     */
    private $username;

    /**
     * @var
     */
    private $ventureId;

    /**
     * @var
     */
    private $channelId;

    /**
     * @var
     */
    private $shopChannelId;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * Create a new job instance.
     * Authentication account constructor.
     *
     * @param $otp
     * @param $username
     * @param $ventureId
     * @param $channelId
     * @param $shopChannelId
     * @param $pusherDTO DTOPusher
     */
    public function __construct($otp, $username, $ventureId, $channelId, $shopChannelId,$pusherDTO)
    {
        $this->otp = $otp;
        $this->username = $username;
        $this->ventureId = $ventureId;
        $this->channelId = $channelId;
        $this->shopChannelId = $shopChannelId;
        $this->pusherDTO = $pusherDTO;
    }
    #endregion

    #region Main
    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        try {
            // Verify Job Request Params
            if (!$this->_checkRequestParam()) {
                return;
            }

            // Tokopedia Authentication
            $tokopedia = new TokopediaAuthentication($this->username, '', $this->ventureId);

            // check account exist and send OTP for user
            $resultVerifyOTP = $tokopedia->getCookies($this->otp);

            // result verify account
            if (!$resultVerifyOTP->getIsSuccess()) {
                $this->jobFailure($resultVerifyOTP->getMessage());
                return;
            }

            ShopChannel::updateShopChannelLinked($this->shopChannelId, $this->channelId);
        } catch (\Exception $exception) {
            $message = app('translator')->get($exception->getMessage());
            $this->jobFailure($message);
            return;
        }

        // Success verify account
        $this->jobSuccess();
    }

    /**
     * @return bool
     */
    private function _checkRequestParam()
    {
        if (!$this->otp) {
            $this->jobFailure('Missing params');
            return false;
        }

        if (!$this->username) {
            $this->jobFailure('Missing params');
            return false;
        }

        if (!$this->ventureId) {
            $this->jobFailure('Missing params');
            return false;
        }

        return true;
    }

    /**
     * @throws \Pusher\PusherException
     */
    private function jobSuccess()
    {
        print_r('Success!!!');
        echo PHP_EOL;

        $pusher = new PusherHelper();
        $data = [
            'data' => [],
            'url' => env('URL_CALLBACK_AFTER_LOGIN_WITH_ACCOUNT_LINKED', '/store/chart'),
            'message' => '',
            'type' => 'success'
        ];
        $channelName = $this->pusherDTO->getChannelName();
        $eventName = $this->pusherDTO->getEventName();
        $pusher->triggerAction($channelName, $eventName, $data);
    }
    #endregion
}