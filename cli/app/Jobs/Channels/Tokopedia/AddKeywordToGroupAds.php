<?php
namespace  App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Jobs;
use App\Library;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO;
use App\Models;

class AddKeywordToGroupAds extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var GroupAdsDTO
     */
    private $groupAdsDTO;

    /**
     * @var KeywordAdsDTO[]
     */
    private $arrayKeywordAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    private $recordId;

    private $params;

    private $object;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId
     * @param $groupAdsDTO
     * @param $arrayKeywordAdsDTO
     * @param $pusherDTO
     * @param $recordId
     * @param $params
     * @param $object
     */
    public function __construct($shopChannelId, $groupAdsDTO, $arrayKeywordAdsDTO, $pusherDTO, $recordId = null, $params = null, $object = null)
    {
        $this->shopChannelId = $shopChannelId;
        $this->groupAdsDTO = $groupAdsDTO;
        $this->arrayKeywordAdsDTO = $arrayKeywordAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->recordId = $recordId;
        $this->params = $params;
        $this->object = $object;
    }

    public function handle()
    {
        // Verify Job Request Data
        if (!$this->shopChannelId) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->groupAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->arrayKeywordAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }

        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess())
        {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->createKeywordAds($this->groupAdsDTO, $this->arrayKeywordAdsDTO);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        // push queue craw keyword
        $job = new Jobs\CrawlTokopediaKeywordAds($this->shopChannelId, $loginResponse->getData(), $this->object);
        dispatch($job->onQueue(Library\QueueName::TOKO_KEYWORD_ADS_CRAW_KEYWORD));

    }
}