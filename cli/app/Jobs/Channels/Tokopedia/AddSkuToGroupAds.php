<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Jobs\Toko\KeywordAds\UpdateCampaign;
use App\Jobs\Toko\KeywordAds\UpdateSku;
use App\Library\Crawler\Channel\Tokopedia;
use App\Models;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\ProductAdsDTO;

class AddSkuToGroupAds extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var GroupAdsDTO
     */
    private $groupAdsDTO;

    /**
     * @var ProductAdsDTO[]
     */
    private $arrayProductAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * @var int
     */
    private $recordId;

    private $object;

    private $params;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId
     * @param $groupAdsDTO
     * @param $arrayProductAdsDTO
     * @param $pusherDTO
     * @param $recordId
     * @param $params
     * @param $object
     */
    public function __construct($shopChannelId, $groupAdsDTO, $arrayProductAdsDTO, $pusherDTO, $recordId = null, $params = null, $object = null)
    {
        $this->shopChannelId = $shopChannelId;
        $this->groupAdsDTO = $groupAdsDTO;
        $this->arrayProductAdsDTO = $arrayProductAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->recordId = $recordId;
        $this->params = $params;
        $this->object = $object;
    }

    public function handle()
    {
        // Verify Job Request Data
        if (!$this->shopChannelId) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->groupAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->arrayProductAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }

        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess()) {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->addSkuToAdsGroup($this->groupAdsDTO, $this->arrayProductAdsDTO);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        $job = new UpdateSku($this->recordId, $this->object, null, $data);
        dispatch($job->onQueue('tokopedia_save'));

    }
}