<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Jobs\TokoAdsEditInfoKeywordJob;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO;
use App\Models;

class EditKeywordInformation extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var KeywordAdsDTO
     */
    private $keywordAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * @var int
     */
    private $recordId;

    /**
     * @var
     */
    private $params;

    /**
     * EditKeywordInformation constructor.
     * @param $shopChannelId
     * @param $keywordAdsDTO
     * @param $pusherDTO
     * @param $recordId
     * @param $params
     */
    public function __construct($shopChannelId, $keywordAdsDTO, $pusherDTO, $recordId, $params)
    {
        $this->shopChannelId = $shopChannelId;
        $this->keywordAdsDTO = $keywordAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->recordId = $recordId;
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        // Verify Job Request Data
        if (!$this->keywordAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }

        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Shop channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess()) {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->editKeywordInformation($this->keywordAdsDTO);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        // Push job to queue when success
        $params = $this->params;
        $job = new TokoAdsEditInfoKeywordJob($this->pusherDTO, $params);
        dispatch($job->onQueue('tokopedia_save'));
    }
}