<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Library\Crawler\Channel\TokopediaAuthentication;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Helpers\PusherHelper;
use Exception;

class ResendOTP extends Job
{
    use JobHandlerTrait;

    #region Init Variable
    /**
     * @var
     */
    private $username;

    /**
     * @var
     */
    private $password;

    /**
     * @var
     */
    private $ventureId;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * Create a new job instance.
     * Authentication account constructor.
     *
     * @param $username
     * @param $password
     * @param $ventureId
     * @param $pusherDTO DTOPusher
     */
    public function __construct($username, $password, $ventureId, $pusherDTO)
    {
        $this->username = $username;
        $this->password = $password;
        $this->ventureId = $ventureId;
        $this->pusherDTO = $pusherDTO;
    }
    #endregion

    #region Main
    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        try {
            // Verify Job Request Params
            if (!$this->_checkRequestParam()) {
                return;
            }

            // Tokopedia Authentication
            $tokopedia = new TokopediaAuthentication($this->username, $this->password, $this->ventureId);

            // check account exist and send OTP for user
            $resultVerifyOTP = $tokopedia->resendOTP();

            // result verify account
            if (!$resultVerifyOTP->getIsSuccess()) {
                $this->jobFailure($resultVerifyOTP->getMessage());
                return;
            }
        } catch (\Exception $exception) {
            $message = app('translator')->get($exception->getMessage());
            $this->jobFailure($message);
            return;
        }

        // Success verify account
        $this->jobSuccess();
    }

    /**
     * @return bool
     */
    private function _checkRequestParam()
    {
        if (!$this->username) {
            $this->jobFailure('Missing params');
            return false;
        }

        if (!$this->ventureId) {
            $this->jobFailure('Missing params');
            return false;
        }

        return true;
    }

    /**
     * @throws \Pusher\PusherException
     */
    private function jobSuccess()
    {
        print_r('Success!!!');
        echo PHP_EOL;

        $pusher = new PusherHelper();
        $data = [
            'data' => [],
            'message' => '',
            'type' => 'success'
        ];
        $channelName = $this->pusherDTO->getChannelName();
        $eventName = $this->pusherDTO->getEventName();
        $pusher->triggerAction($channelName, $eventName, $data);
    }
    #endregion
}