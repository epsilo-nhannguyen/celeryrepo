<?php
namespace App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\ProductAdsDTO;
use App\Models;

class GetSuggestKeyword extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var GroupAdsDTO
     */
    private $groupAdsDTO;

    /**
     * @var ProductAdsDTO
     */
    private $productAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId int
     * @param $groupAdsDTO GroupAdsDTO
     * @param $productAdsDTO ProductAdsDTO
     * @param $pusherDTO DTOPusher
     */
    public function __construct($shopChannelId, $groupAdsDTO, $productAdsDTO, $pusherDTO)
    {
        $this->shopChannelId = $shopChannelId;
        $this->groupAdsDTO = $groupAdsDTO;
        $this->productAdsDTO = $productAdsDTO;
        $this->pusherDTO = $pusherDTO;
    }

    public function handle()
    {
        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess())
        {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->getSuggestKeyword($this->groupAdsDTO->getGroupId(), $this->productAdsDTO->getProductId());

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        print_r($data);

        // Push queue to FE
//        $job = new TokoAdsEditCampaignJob($this->pusherDTO, $this->groupAdsDTO, $this->recordId);
//        dispatch($job->onQueue('tokopedia_save'));
    }
}