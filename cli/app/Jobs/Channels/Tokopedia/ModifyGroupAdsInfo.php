<?php
namespace App\Jobs\Channels\Tokopedia;

use App\Jobs\Job;
use App\Jobs\TokoAdsEditCampaignJob;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\GroupAdsDTO;
use App\Models;
use App\Library\Crawler\Channel\Tokopedia;

class ModifyGroupAdsInfo extends Job
{
    use JobHandlerTrait;
    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var GroupAdsDTO
     */
    private $groupAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * @var int
     */
    private $recordId;


    private $params;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId int
     * @param $groupAdsDTO GroupAdsDTO
     * @param $pusherDTO DTOPusher
     * @param $recordId int
     * @param $params
     */
    public function __construct($shopChannelId, $groupAdsDTO, $pusherDTO, $recordId, $params)
    {
        $this->shopChannelId = $shopChannelId;
        $this->groupAdsDTO = $groupAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->recordId = $recordId;
        $this->params = $params;
    }

    public function handle()
    {
        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess())
        {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->editAdsGroup($this->groupAdsDTO);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        $job = new TokoAdsEditCampaignJob($this->pusherDTO, $this->params);
        dispatch($job->onQueue('tokopedia_save'));
    }
}