<?php

namespace App\Jobs\Channels\Tokopedia;

use App\Jobs;
use App\Jobs\Job;
use App\Library\Crawler\Channel\Tokopedia;
use App\Repository\RepositoryBusiness\DTO\DTOPusher;
use App\Repository\RepositoryBusiness\Tokopedia\DTO\ProductAdsDTO;
use App\Models;

class ActiveDeactivateSkuInGroupAds extends Job
{
    use JobHandlerTrait;

    /**
     * @var int
     */
    private $shopChannelId;

    /**
     * @var ProductAdsDTO
     */
    private $productAdsDTO;

    /**
     * @var DTOPusher
     */
    private $pusherDTO;

    /**
     * @var int
     */
    private $recordId;

    private $object;

    private $params;

    /**
     * Create a new job instance.
     *
     * @param $shopChannelId
     * @param $productAdsDTO
     * @param $pusherDTO
     * @param $recordId
     * @param $params
     * @param $object
     */
    public function __construct($shopChannelId, $productAdsDTO, $pusherDTO, $recordId, $params = null, $object = null)
    {
        $this->shopChannelId = $shopChannelId;
        $this->productAdsDTO = $productAdsDTO;
        $this->pusherDTO = $pusherDTO;
        $this->recordId = $recordId;
        $this->params = $params;
        $this->object = $object;
    }

    public function handle()
    {
        // Verify Job Request Data
        if (!$this->shopChannelId) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->productAdsDTO) {
            $this->jobFailure('Missing params');
            return;
        }
        if (!$this->recordId) {
            $this->jobFailure('Missing params');
            return;
        }

        // Check Shop Cookie
        $shopChannelInfo = Models\ShopChannel::getById($this->shopChannelId);
        if (!$shopChannelInfo) {
            $this->jobFailure('Channel id not exits');
            return;
        }
        $channelInfo = Models\Channel::getById($shopChannelInfo->fk_channel);
        if ($channelInfo->channel_code != Models\Channel::TOKOPEDIA_CODE) {
            $this->jobFailure('Is not channel TOKOPEDIA');
            return;
        }

        $tokopediaService = new Tokopedia($this->shopChannelId);
        $loginResponse = $tokopediaService->login();
        if (!$loginResponse->getIsSuccess()) {
            $this->jobFailure('Login Failure');
            return;
        }
        $tokopediaService->setAuthentication($loginResponse->getData());
        $data = $tokopediaService->activeOrDeactivateSKU($this->productAdsDTO);

        if (!$data->getIsSuccess()) {
            $this->jobFailure($data->getMessage());
            return;
        }

        $object = json_decode($this->object ?? '{}', true);
        $isInTerminal = $object['isInTerminal'] ?? 0;
        if ($isInTerminal) {
            $job = new Jobs\Toko\KeywordAds\EditStatusSku($this->recordId, $this->object);
            dispatch($job->onQueue('tokopedia_save'));
        } else {
            $job = new Jobs\TokoAdsEditStatusSkuJob($this->pusherDTO, $this->params);
            dispatch($job->onQueue('tokopedia_save'));
        }

    }
}