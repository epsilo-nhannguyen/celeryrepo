<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 14/01/2020
 * Time: 06:31
 */

namespace App\Jobs\Toko\KeywordAds;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Library;
use App\Models;


class ImportSkuPerformance implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    private $channelCreateDate;

    public function __construct($channelCreateDate, $data)
    {
        $this->data = $data;
        $this->channelCreateDate = $channelCreateDate;
    }

    public function handle()
    {
        $channelCreateDate = $this->channelCreateDate;
        $data = $this->data;
        $arrayAddNew = [];
        $updatedAt = Library\Common::getCurrentTimestamp();
        $createdAt = Library\Common::getCurrentTimestamp();
//        $data = [["ad_id"=>47900319,"item_id"=>664709169,"ad_status"=>2,"ad_status_desc"=>"Not Delivered","ad_status_toogle"=>1,"ad_price_bid_fmt"=>"Rp 350","ad_price_daily_fmt"=>"-","ad_price_daily_spent_fmt"=>"Rp 0","ad_type"=>1,"ad_price_daily_bar"=>"","ad_editable"=>1,"ad_start_date"=>"10/02/2020","ad_start_time"=>"00=>38 WIB","ad_end_date"=>"Continuous","ad_end_time"=>"","ad_moderated"=>0,"ad_moderated_reason"=>"","ad_promo_code"=>"","ad_platform"=>"","user_target"=>"","product_name"=>"MB2 V2","product_uri"=>"https=>//www.tokopedia.com/chhung/mb2-v2","product_image_uri"=>"https=>//ecs7.tokopedia.net/img/cache/100-square/product-1/2020/1/16/85959535/85959535_e9c4edc4-aebc-4790-b443-f983d722b803_959_959","product_active"=>1,"group_name"=>"GangterHungChauAABA","group_id"=>3542425,"shop_id"=>0,"owner_shop"=>["id"=>0,"name"=>"","domain"=>""],"stat_avg_click"=>"","stat_total_spent"=>"","stat_total_impression"=>"","stat_total_click"=>"","stat_total_ctr"=>"","stat_total_conversion"=>"","stat_total_sold"=>"","stat_avg_position"=>0,"stat_avg_position_fmt"=>"-","stat_total_gross_profit"=>"","stat_total_follow"=>"","RawStatTtlSpent"=>0,"RawStatTtlImpression"=>0,"RawStatTtlClick"=>0,"RawStatTtlCTR"=>0,"RawStatConversion"=>0,"RawStatConversionIndirect"=>0,"label_edit"=>"Edit","label_per_click"=>"Per Click","label_per_impression"=>"Per 1K Impressions","label_of"=>"of"]];
        foreach ($data as $item) {
            $productId = Models\ModelBusiness\TokoAdsSku::getProductIdByChannel($item['group_id'], $item['ad_id']);
            $campaignId = Models\ModelBusiness\TokoAdsCampaign::getCampaignIdByChannelCampaignId($item['group_id']);
            $isAuto = Models\ModelBusiness\TokoAdsCampaign::getIsAutoByChannel($item['group_id']);
            $formatItem = [
                'campaign_id' => $campaignId,
                'product_id' => $productId,
                'channel_campaign_id' => $item['group_id'],
                'channel_product_id' => $item['item_id'],
                'impression' => $item['stat_total_impression'],
                'click' => $item['stat_total_click'],
                'all_sold' => $item['stat_total_sold'],
                'cost' => $item['stat_total_spent'],
                'ctr' => $item['stat_total_ctr'],
                'channel_create_date' => $channelCreateDate,
                'item_sold' => $item['stat_total_conversion'],
                'average' => $item['stat_avg_click'],
                'updated_at' => $updatedAt,
            ];
            if($isAuto){
                $formatItem['is_auto'] = $isAuto;
            }
            if (Models\ModelBusiness\TokoAdsSkuPerformance::isSkuPerformanceFromChannelExists($item['group_id'], $channelCreateDate)) {
                Models\ModelBusiness\TokoAdsSkuPerformance::updateIfExistSkuPerformanceFromChannel($item['group_id'], $channelCreateDate, $formatItem);
            } else {
                $formatItem['created_at'] = $createdAt;
                $arrayAddNew[] = $formatItem;
            }

        }

        if ($arrayAddNew) {
            Models\ModelBusiness\TokoAdsSkuPerformance::addNewIfNonExistsSkuPerformanceFromChannel($arrayAddNew);
        }

        print_r('Done Task Import ');
    }
}