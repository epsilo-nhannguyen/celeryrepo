<?php

namespace App\Jobs\Toko\KeywordAds;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use App\Helpers\PusherHelper;
use Illuminate\Support\Collection;
use Pusher\Pusher;
use App\Models;
use App\Library;
use App\Repository;


class UpdateCampaign implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $recordId;
    private $object;
    private $pusherDTO;

    /**
     * @var Repository\RepositoryBusiness\DTO\DTOResponse
     */
    private $DTOResponse;

    public function __construct($recordId, $object, $pusherDTO = null, $DTOResponse = null)
    {
        $this->recordId = $recordId;
        $this->object = $object;
        $this->pusherDTO = $pusherDTO;
        $this->DTOResponse = $DTOResponse;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Pusher\PusherException
     */
    public function handle()
    {
        $data = json_decode($this->object ?? '{}', true);

        $responseData = $this->DTOResponse->getData();
        $campaignData = $responseData['data'] ?? [];
        $channelCampaignId = $campaignData['group_id'] ?? null;
        $productAdsData = $campaignData['ads'] ?? [];

        $campaignStatus = $data['campaignStatus'];
        $record = [
            'channel_campaign_id' => $channelCampaignId,
            'config_active_id' => $campaignStatus,
            'updated_by' => Models\Admin::EPSILO_SYSTEM,
            'updated_at' => Library\Common::getCurrentTimestamp(),
        ];
        Models\ModelBusiness\TokoAdsCampaign::updatedById($this->recordId, $record);

        $assocItemId_productId = $data['assocItemId_productId'] ?? [];

        foreach ($productAdsData as $productAds) {
            $itemId = $productAds['product_id'];
            $channelProductId = $productAds['ad']['ad_id'];
            $skuStatus = $data['skuStatus'];
            if ($productAds['ad_schedule'] == 1) {
                $skuStatus = Models\ModelBusiness\TokoAdsSku::STATUS_SCHEDULED;
            }

            $productId = $assocItemId_productId[$itemId] ?? null;
            if ( ! $productId) continue;

            $recordProduct = [
                'channel_product_id' => $channelProductId,
                'status' => $skuStatus,
                'updated_by' => Models\Admin::EPSILO_SYSTEM,
                'updated_at' => Library\Common::getCurrentTimestamp(),
            ];
            Models\ModelBusiness\TokoAdsSku::updatedByProductId($productId, $recordProduct);
        }
    }
}
