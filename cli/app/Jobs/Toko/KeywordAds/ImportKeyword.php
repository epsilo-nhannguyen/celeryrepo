<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 14/01/2020
 * Time: 06:31
 */

namespace App\Jobs\Toko\KeywordAds;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Repository;
use App\Jobs;
use App\Library;
use App\Models;


class ImportKeyword implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $shopChannelId;

    public $timezone;

    /**
     * @var DTOResponse
     */
    public $DTOResponse;

    private $object;

    public function __construct($shopChannelId, $timezone, $DTOResponse, $object = null)
    {
        $this->shopChannelId = $shopChannelId;
        $this->timezone = $timezone;
        $this->DTOResponse = $DTOResponse;
        $this->object = $object;
    }

    public function handle()
    {
        Library\Common::setTimezone($this->timezone);

        $response = $this->DTOResponse->getData() ?? [];

        $assocChannelCampaignId_campaignId = Models\ModelBusiness\TokoAdsCampaign::searchByShopChannelId($this->shopChannelId)
            ->pluck('id', 'channel_campaign_id');
        $assocCampaignChannelKeyword_keywordId = $assocCampaignKeywordNameMatchType_keywordId = collect([]);
        $keywordData = Models\ModelBusiness\TokoAdsKeyword::searchByCampaignIdArray($assocChannelCampaignId_campaignId->values()->toArray());
        foreach ($keywordData as $keyword) {
            $assocCampaignChannelKeyword_keywordId->put($keyword->campaign_id.'_'.$keyword->channel_keyword_id, $keyword->id);
            $assocCampaignKeywordNameMatchType_keywordId->put($keyword->campaign_id.'_'.$keyword->name.'_'.$keyword->match_type, $keyword->id);
        }

        $recordInsertCollection = collect([]);
        foreach ($response as $item) {
            $channelCampaignId = $item['group_id'];
            $channelKeywordId = $item['keyword_id'];
            $name = $item['keyword_tag'];
            $configActiveId = $item['keyword_status_desc'] === 'Active' ? 1 : 0;
            $maxCost = $item['keyword_price_bid'];
            $matchType = $item['keyword_type_desc'] === 'General Search' ? 'broad_match' : 'exact_match';
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            $campaignId = $assocChannelCampaignId_campaignId->get($channelCampaignId, null);

            $recordCollecttion = collect([
                'channel_keyword_id' => $channelKeywordId,
                'name' => $name,
                'config_active_id' => $configActiveId,
                'max_cost' => $maxCost,
                'match_type' => $matchType,
                'updated_at' => $updatedAt,
                'updated_by' => $updatedBy,
            ]);

            $keywordId = $assocCampaignChannelKeyword_keywordId->get($campaignId.'_'.$channelKeywordId);
            if ( ! $keywordId) {
                $keywordId = $assocCampaignKeywordNameMatchType_keywordId->get($campaignId.'_'.$name.'_'.$matchType);
            }

            if ($keywordId) {
                Models\ModelBusiness\TokoAdsKeyword::updatedById($keywordId, $recordCollecttion->toArray());
            } else {
                if ( ! $campaignId) continue;

                $recordCollecttion->put('campaign_id', $campaignId);
                $recordCollecttion->put('created_at', $createdAt);
                $recordCollecttion->put('created_by', $createdBy);

                $recordInsertCollection->push($recordCollecttion);
            }
        }

        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\TokoAdsKeyword::batchInsert($recordInsertCollection->toArray());
            sleep(1);
        }

        if ($this->object) {
            $data = json_decode($this->object ?? '{}', true);

            $keywordStatusChannel = false;
            $keywordStatus = Models\ConfigActive::INACTIVE;
            $assocCampaignId_keywordNeedInactive = $data['assocCampaignId_keywordNeedInactive'] ?? [];
            foreach ($assocCampaignId_keywordNeedInactive as $campaignId => $keywordNeedInactive) {
                $keywordArray = array_keys($keywordNeedInactive);
                $keywordInDb = Models\ModelBusiness\TokoAdsKeyword::searchByCampaignId($campaignId, $keywordArray);
                foreach ($keywordInDb as $keyword) {
                    $keywordId = $channelKeywordId = null;

                    foreach ($keywordNeedInactive as $keywordName => $matchType) {
                        if ($keywordName == $keyword->name && $matchType == $keyword->match_type) {
                            $keywordId = $keyword->id;
                            $channelKeywordId = $keyword->channel_keyword_id;
                            break;
                        }
                    }
                    if ( ! $keywordId) continue;

                    $keywordAdsDTO = new Repository\RepositoryBusiness\Tokopedia\DTO\KeywordAdsDTO();
                    $keywordAdsDTO->setKeywordId(strval($channelKeywordId));
                    $keywordAdsDTO->setToggleKeyword($keywordStatusChannel);

                    $object = json_encode([
                        'status' => $keywordStatus,
                        'isInTerminal' => 1,
                    ]);
                    $job = new Jobs\Channels\Tokopedia\ActiveDeactivateKeywordInGroupAds($this->shopChannelId, $keywordAdsDTO, null, $keywordId, null, $object);
                    dispatch($job->onQueue('tokopedia_simulation'));
                }
            }
        }

    }
}