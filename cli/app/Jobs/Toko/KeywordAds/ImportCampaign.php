<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 14/01/2020
 * Time: 06:31
 */

namespace App\Jobs\Toko\KeywordAds;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Library;
use App\Models;


class ImportCampaign implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $shopChannelId;

    public $timezone;

    /**
     * @var DTOResponse
     */
    public $DTOResponse;

    public function __construct($shopChannelId, $timezone, $DTOResponse)
    {
        $this->shopChannelId = $shopChannelId;
        $this->timezone = $timezone;
        $this->DTOResponse = $DTOResponse;
    }

    public function handle()
    {
        Library\Common::setTimezone($this->timezone);

        $response = $this->DTOResponse->getData() ?? [];

        $shopChannelInfo = Models\ModelBusiness\ShopChannel::getById($this->shopChannelId);
        $shopMasterInfo = Models\ModelBusiness\ShopMaster::getById($shopChannelInfo->fk_shop_master);
        $organizationId = $shopMasterInfo->fk_organization;

        $assocChannelCampaignId_campaignId = collect([]);
        $channelCampaignIdArray = array_column($response, 'group_id');
        if ($channelCampaignIdArray) {
            $assocChannelCampaignId_campaignId = Models\ModelBusiness\TokoAdsCampaign::searchByShopChannelId(
                $this->shopChannelId, $channelCampaignIdArray
            )->pluck('id', 'channel_campaign_id');
        }

        $recordInsertCollection = collect([]);
        foreach ($response as $item) {
            $channelCampaignId = $item['group_id'];
            $configActiveId = $item['group_status_desc'] === 'Active' ? 1 : 0;
            $name = $item['group_name'];
            $dailyBudget = floatval($item['group_price_daily']) > 0 ? floatval($item['group_price_daily']) : null;
            $maxCost = $item['group_price_bid'];
            $from = Library\Formater::dateSlashToHyphen($item['group_start_date']);
            $to = $item['group_end_date'] !== 'Continuous' ? Library\Formater::dateSlashToHyphen($item['group_end_date']) : null;
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            #$isAuto = 0 || 1;

            $recordCollecttion = collect([
                'name' => $name,
                'config_active_id' => $configActiveId,
                'daily_budget' => $dailyBudget,
                'max_cost' => $maxCost,
                'from' => $from,
                'to' => $to,
                'updated_at' => $updatedAt,
                'updated_by' => $updatedBy,
            ]);

            $campaignId = $assocChannelCampaignId_campaignId->get($channelCampaignId);
            if ($campaignId) {
                Models\ModelBusiness\TokoAdsCampaign::updatedById($campaignId, $recordCollecttion->toArray());
            } else {
                $code = Models\ModelBusiness\TokoAdsCampaign::generateByInOrganization($organizationId);

                $recordCollecttion->put('channel_campaign_id', $channelCampaignId);
                $recordCollecttion->put('shop_channel_id', $this->shopChannelId);
                $recordCollecttion->put('code', substr($code,0,12).random_int(10000, 99999).substr($code,-1));
                #$recordCollecttion->put('is_auto', $isAuto);
                $recordCollecttion->put('created_at', $createdAt);
                $recordCollecttion->put('created_by', $createdBy);

                $recordInsertCollection->push($recordCollecttion);
            }
        }

        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\TokoAdsCampaign::batchInsert($recordInsertCollection->toArray());
        }
    }
}