<?php

namespace App\Jobs\Toko\KeywordAds;

use App\Library;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use App\Helpers\PusherHelper;
use Pusher\Pusher;
use App\Models;
use App\Repository;

class EditStatusCampaign implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $recordId;
    private $object;
    /**
     * @var Repository\RepositoryBusiness\DTO\DTOPusher
     */
    private $pusherDTO;


    public function __construct($recordId, $object, $pusherDTO = null)
    {
        $this->recordId = $recordId;
        $this->object = $object;
        $this->pusherDTO = $pusherDTO;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Pusher\PusherException
     */
    public function handle()
    {
        $data = json_decode($this->object ?? '{}', true);

        $status = $data['status'];
        $record = [
            'config_active_id' => $status,
            'updated_by' => Models\Admin::EPSILO_SYSTEM,
            'updated_at' => Library\Common::getCurrentTimestamp(),
        ];
        Models\ModelBusiness\TokoAdsCampaign::updatedById($this->recordId, $record);
        $dataResponse = [
            'type' => 'success'
        ];

        if ($this->pusherDTO) {
            $pusher = new PusherHelper();
            $pusher->triggerAction($this->pusherDTO->getChannelName(), $this->pusherDTO->getEventName(), $dataResponse);
        }
    }
}
