<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 14/01/2020
 * Time: 06:31
 */

namespace App\Jobs\Toko\KeywordAds;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Library;
use App\Models;


class ImportSku implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $shopChannelId;

    public $timezone;

    /**
     * @var DTOResponse
     */
    public $DTOResponse;

    public function __construct($shopChannelId, $timezone, $DTOResponse)
    {
        $this->shopChannelId = $shopChannelId;
        $this->timezone = $timezone;
        $this->DTOResponse = $DTOResponse;
    }

    public function handle()
    {
        Library\Common::setTimezone($this->timezone);

        $response = $this->DTOResponse->getData() ?? [];

        $campaignData = Models\ModelBusiness\TokoAdsCampaign::searchByShopChannelId($this->shopChannelId);
        $assocChannelCampaignId_campaignId = $campaignData->pluck('id', 'channel_campaign_id');
        $assocChannelCampaignId_maxCost = $campaignData->pluck('max_cost', 'channel_campaign_id');

        $assocItemId_productId = Models\ModelBusiness\ProductShopChannel::searchByShopChannelId($this->shopChannelId)
            ->pluck('product_shop_channel_id', 'product_shop_channel_item_id');

        $skuData = Models\ModelBusiness\TokoAdsSku::searchByShopChannelId($this->shopChannelId);
        $assocProductId_skuId = $skuData->pluck('id', 'product_id');
        $assocChannelProductId_skuId = $skuData->pluck('id', 'channel_product_id');

        $recordInsertCollection = collect([]);
        foreach ($response as $item) {
            $itemId = $item['item_id'];
            $channelCampaignId = $item['group_id'];
            $channelProductId = $item['ad_id'];
            $status = $this->mappingStatusChannelWithSystem($item['ad_status_desc']);
            $from = strtotime(Library\Formater::dateSlashToHyphen($item['ad_start_date']));
            $to = $item['ad_end_date'] !== 'Continuous' ? strtotime(Library\Formater::dateSlashToHyphen($item['ad_end_date']).' 23:59:59') : null;
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            $productId = $assocItemId_productId->get($itemId, null);
            $campaignId = $assocChannelCampaignId_campaignId->get($channelCampaignId, null);
            $maxCost = $assocChannelCampaignId_maxCost->get($channelCampaignId, 0);

            $recordCollecttion = collect([
                'campaign_id' => $campaignId,
                'product_id' => $productId,
                'status' => $status,
                'max_cost' => $maxCost,
                'from' => $from,
                'to' => $to,
                'updated_at' => $updatedAt,
                'updated_by' => $updatedBy,
            ]);

            $skuId = $assocProductId_skuId->get($productId);
            if ( ! $skuId) {
                $skuId = $assocChannelProductId_skuId->get($channelProductId);
            }

            if ($skuId) {
                Models\ModelBusiness\TokoAdsSku::updatedById($skuId, $recordCollecttion->toArray());
            } else {
                $recordCollecttion->put('shop_channel_id', $this->shopChannelId);
                $recordCollecttion->put('channel_product_id', $channelProductId);
                $recordCollecttion->put('created_at', $createdAt);
                $recordCollecttion->put('created_by', $createdBy);

                $recordInsertCollection->push($recordCollecttion);
            }
        }

        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\TokoAdsSku::batchInsert($recordInsertCollection->toArray());
        }
    }

    /**
     * mapping Status Channel With System
     * @param string $statusChannel
     * @return string|null
     */
    private function mappingStatusChannelWithSystem($statusChannel)
    {
        switch ($statusChannel) {
            case 'Active':
                $statusSystem = 'ongoing';
                break;
            case 'Inactive':
                $statusSystem = 'paused';
                break;
            case 'Not Delivered':
                $statusSystem = 'not_delivered';
                break;
                /*case 'Scheduled':
                    $statusSystem = 'scheduled';
                    break;
                case 'Ended':
                    $statusSystem = 'ended';
                    break;
                case 'Closed':
                    $statusSystem = 'closed';*/
                break;
            default:
                $statusSystem = null;
                break;
        }

        return $statusSystem;
    }

}