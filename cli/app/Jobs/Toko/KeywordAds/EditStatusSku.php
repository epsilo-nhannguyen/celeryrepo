<?php

namespace App\Jobs\Toko\KeywordAds;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use App\Helpers\PusherHelper;
use Pusher\Pusher;
use App\Models;
use App\Library;
use App\Repository;

class EditStatusSku implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $recordId;
    private $object;
    private $pusherDTO;

    public function __construct($recordId, $object, $pusherDTO = null)
    {
        $this->recordId = $recordId;
        $this->object = $object;
        $this->pusherDTO = $pusherDTO;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Pusher\PusherException
     */
    public function handle()
    {
        $data = json_decode($this->object ?? '{}', true);

        $status = $data['status'];
        $record = [
            'status' => $status,
            'updated_by' => Models\Admin::EPSILO_SYSTEM,
            'updated_at' => Library\Common::getCurrentTimestamp(),
        ];
        Models\ModelBusiness\TokoAdsSku::updatedById($this->recordId, $record);

    }
}
