<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 14/01/2020
 * Time: 06:31
 */

namespace App\Jobs\Toko\KeywordAds;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Library;
use App\Models;


class ImportCampaignPerformance implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $channelCreateDate;
    private $data;

    public function __construct($channelCreateDate, $data)
    {
        $this->channelCreateDate = $channelCreateDate;
        $this->data = $data;
    }

    public function handle()
    {
        // Set timezone
        // Library\Common::setTimezone($crawlTokopedia->getTimeZone());

        $channelCreateDate = $this->channelCreateDate;
        $data = $this->data;
        $updatedAt = Library\Common::getCurrentTimestamp();
        $createdAt = Library\Common::getCurrentTimestamp();
        $arrayAddNew = [];

//        $data = [["group_id" => 3496389, "total_item" => 1, "total_item_fmt" => "1", "total_keyword" => 0, "total_keyword_fmt" => "", "group_status" => 1, "group_status_desc" => "Active", "group_status_toogle" => 1, "group_price_bid_fmt" => "Rp 350", "group_price_bid" => 350, "group_price_daily" => 0, "group_price_daily_fmt" => "Not Limited", "group_price_daily_spent_fmt" => "", "group_price_daily_bar" => "", "group_editable" => 1, "group_start_date" => "21/01/2020", "group_start_time" => "12=>47 WIB", "group_end_date" => "Continuous", "group_end_time" => "", "group_moderated" => 0, "group_moderated_reason" => "", "group_name" => "GangterBBB", "group_type" => "product", "stat_avg_click" => "", "stat_total_spent" => "", "stat_total_impression" => "", "stat_total_click" => "", "stat_total_ctr" => "", "stat_total_conversion" => "", "stat_total_sold" => "", "stat_avg_position" => 0, "stat_avg_position_fmt" => "-", "stat_max_position" => 10, "stat_low_bid_alert" => 0, "label_edit" => "Edit", "label_per_click" => "Per Click", "label_of" => "of"], ["group_id" => 3484263, "total_item" => 1, "total_item_fmt" => "1", "total_keyword" => 0, "total_keyword_fmt" => "", "group_status" => 1, "group_status_desc" => "Active", "group_status_toogle" => 1, "group_price_bid_fmt" => "Rp 400", "group_price_bid" => 400, "group_price_daily" => 0, "group_price_daily_fmt" => "Not Limited", "group_price_daily_spent_fmt" => "", "group_price_daily_bar" => "", "group_editable" => 1, "group_start_date" => "16/01/2020", "group_start_time" => "10=>03 WIB", "group_end_date" => "Continuous", "group_end_time" => "", "group_moderated" => 0, "group_moderated_reason" => "", "group_name" => "T1", "group_type" => "product", "stat_avg_click" => "", "stat_total_spent" => "", "stat_total_impression" => "", "stat_total_click" => "", "stat_total_ctr" => "", "stat_total_conversion" => "", "stat_total_sold" => "", "stat_avg_position" => 0, "stat_avg_position_fmt" => "-", "stat_max_position" => 10, "stat_low_bid_alert" => 0, "label_edit" => "Edit", "label_per_click" => "Per Click", "label_of" => "of"], ["group_id" => 3542425, "total_item" => 1, "total_item_fmt" => "1", "total_keyword" => 0, "total_keyword_fmt" => "", "group_status" => 1, "group_status_desc" => "Active", "group_status_toogle" => 1, "group_price_bid_fmt" => "Rp 350", "group_price_bid" => 350, "group_price_daily" => 14000, "group_price_daily_fmt" => "Rp 14.000", "group_price_daily_spent_fmt" => "Rp 0", "group_price_daily_bar" => "0.000", "group_editable" => 1, "group_start_date" => "10/02/2020", "group_start_time" => "00=>38 WIB", "group_end_date" => "Continuous", "group_end_time" => "", "group_moderated" => 0, "group_moderated_reason" => "", "group_name" => "GangterHungChauAABA", "group_type" => "product", "stat_avg_click" => "", "stat_total_spent" => "", "stat_total_impression" => "", "stat_total_click" => "", "stat_total_ctr" => "", "stat_total_conversion" => "", "stat_total_sold" => "", "stat_avg_position" => 0, "stat_avg_position_fmt" => "-", "stat_max_position" => 10, "stat_low_bid_alert" => 0, "label_edit" => "Edit", "label_per_click" => "Per Click", "label_of" => "of"]];
        foreach ($data as $item) {
            $campaignId = Models\ModelBusiness\TokoAdsCampaign::getCampaignIdByChannelCampaignId($item['group_id']);
            $formatItem = [
                'campaign_id' => $campaignId,
                'channel_campaign_id' => $item['group_id'],
                'impression' => $item['stat_total_impression'],
                'click' => $item['stat_total_click'],
                'all_sold' => $item['stat_total_sold'],
                'cost' => $item['stat_total_spent'],
                'ctr' => $item['stat_total_ctr'],
                'item_sold' => $item['stat_total_conversion'],
                'average' => $item['stat_avg_click'],
                'channel_create_date' => $channelCreateDate,
                'rank' => $item['stat_avg_position'],
                'updated_at' => $updatedAt,
            ];
            if (Models\ModelBusiness\TokoAdsCampaignPerformance::isCampaignPerformanceFromChannelExists($item['group_id'], $channelCreateDate)) {
                Models\ModelBusiness\TokoAdsCampaignPerformance::updateIfExistCampaignPerformanceFromChannel($item['group_id'], $channelCreateDate, $formatItem);
            } else {
                $formatItem['created_at'] = $createdAt;
                $arrayAddNew[] = $formatItem;
            }
        }
        if ($arrayAddNew) {
            Models\ModelBusiness\TokoAdsCampaignPerformance::addNewIfNonExistsCampaignPerformanceFromChannel($arrayAddNew);
        }

        print_r('Done Task Import ');
    }
}