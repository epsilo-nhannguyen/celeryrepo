<?php
/**
 * Created by PhpStorm.
 * User: kiet
 * Date: 14/01/2020
 * Time: 06:31
 */

namespace App\Jobs\Toko\KeywordAds;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repository\RepositoryBusiness\DTO\DTOResponse;
use App\Library;
use App\Models;


class ImportProduct implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $shopChannelId;

    public $timezone;

    /**
     * @var DTOResponse
     */
    public $DTOResponse;

    public function __construct($shopChannelId, $timezone, $DTOResponse)
    {
        $this->shopChannelId = $shopChannelId;
        $this->timezone = $timezone;
        $this->DTOResponse = $DTOResponse;
    }

    public function handle()
    {
        Library\Common::setTimezone($this->timezone);

        $response = $this->DTOResponse->getData() ?? [];
        $shopChannelInfo = Models\ModelBusiness\ShopChannel::getById($this->shopChannelId);
        $channelId = $shopChannelInfo->fk_channel;

        $shopMasterInfo = Models\ModelBusiness\ShopMaster::getById($shopChannelInfo->fk_shop_master);
        $channelInfo = Models\ModelBusiness\Channel::getById($channelId);
        $ventureInfo = Models\ModelBusiness\Venture::getById($channelInfo->fk_venture);

        $organizationId = $shopMasterInfo->fk_organization;
        $ventureCode = $ventureInfo->venture_code;
        $channelCode = Models\Channel::TOKOPEDIA_CODE;

        $assocCode_category = collect([]);
        $categoryData = array_column($response, 'category');
        foreach ($categoryData as $category) {
            $categoryIdentify = $category['id'];
            $level1 = $level2 = $level3 = $level4 = $level5 = null;

            $categoryDetail = $category['detail'] ?? [];
            foreach ($categoryDetail as $item) {
                switch ($item['level'] ?? 0) {
                    case 1:
                        $level1 = $item['text'] ?? null;
                        break;
                    case 2:
                        $level2 = $item['text'] ?? null;
                        break;
                    case 3:
                        $level3 = $item['text'] ?? null;
                        break;
                    case 4:
                        $level4 = $item['text'] ?? null;
                        break;
                    case 5:
                        $level5 = $item['text'] ?? null;
                        break;
                }
            }

            $categoryCode = collect([$channelCode, $ventureCode, $categoryIdentify])->implode('-');
            $categoryName = collect([
                $level1,
                $level2,
                $level3,
                $level4,
                $level5,
            ])->filter()->implode('>');

            if ( ! $assocCode_category->has($categoryCode)) {
                $assocCode_category->put($categoryCode, collect([
                    'channel_category_code' => $categoryCode,
                    'channel_category_name' => $categoryName,
                    'channel_category_identify' => $categoryIdentify,
                    'fk_channel' => $channelId,
                    'fk_organization' => $organizationId,
                    'channel_category_level_1' => $level1,
                    'channel_category_level_2' => $level2,
                    'channel_category_level_3' => $level3,
                    'channel_category_level_4' => $level4,
                    'channel_category_level_5' => $level5,
                    'channel_category_created_at' => Library\Common::getCurrentTimestamp(),
                    'channel_category_created_by' => Models\Admin::EPSILO_SYSTEM
                ]));
            }
        }

        $categoryCodeCollection = $assocCode_category->keys();
        $categoryCodeCollectionInDb = Models\ModelBusiness\ChannelCategory::searchByOrganizationIdAndChannelId($organizationId, $channelId, $categoryCodeCollection)
            ->pluck('channel_category_code')->all();

        $categoryInsertCollection = $assocCode_category->filter(function ($item, $key) use ($categoryCodeCollectionInDb) {
            return ! in_array($key, $categoryCodeCollectionInDb);
        })->values();

        if ($categoryInsertCollection->count()) {
            Models\ModelBusiness\ChannelCategory::batchInsert($categoryInsertCollection->toArray());
            sleep(1);
        }

        $assocCategory_codeId = Models\ModelBusiness\ChannelCategory::searchByOrganizationIdAndChannelId($organizationId, $channelId)
            ->pluck('channel_category_id' ,'channel_category_code');

        $assocItemId_productId = collect([]);
        $itemIdChannelArray = array_column($response, 'product_id');
        if ($itemIdChannelArray) {
            $assocItemId_productId = Models\ModelBusiness\ProductShopChannel::searchByShopChannelId($this->shopChannelId, $itemIdChannelArray)
                ->pluck('product_shop_channel_id', 'product_shop_channel_item_id');
        }

        $recordInsertCollection = collect([]);
        foreach ($response as $item) {
            $itemId = $item['product_id'];
            $name = $item['name'];
            $stockAllocationStock = $item['stock'];
            $stockFbx = null;
            $stockFbxCollectedAt = null;
            $priceRrpChannel = $item['price']['value'] ?? 0;
            $pricePostsubChannel = null;
            $pulledAt = Library\Common::getCurrentTimestamp();
            $data = json_encode($item);
            $shopSku = null;
            $updatedAt = Library\Common::getCurrentTimestamp();
            $updatedBy = Models\Admin::EPSILO_SYSTEM;
            $createdAt = Library\Common::getCurrentTimestamp();
            $createdBy = Models\Admin::EPSILO_SYSTEM;
            $categoryIdentify = $item['category']['id'] ?? null;
            $channelCategoryId = $assocCategory_codeId->get(collect([$channelCode, $ventureCode, $categoryIdentify])->implode('-'));

            $recordCollecttion = collect([
                'product_shop_channel_name' => $name,
                'product_shop_channel_stock_allocation_stock' => $stockAllocationStock,
                'product_shop_channel_stock_fbx' => $stockFbx,
                'product_shop_channel_stock_fbx_collected_at' => $stockFbxCollectedAt,
                'product_shop_channel_price_rrp_channel' => $priceRrpChannel,
                'product_shop_channel_price_postsub_channel' => $pricePostsubChannel,
                'product_shop_channel_pulled_at' => $pulledAt,
                'product_shop_channel_data' => $data,
                'product_shop_channel_shop_sku' => $shopSku,
                'product_shop_channel_updated_at' => $updatedAt,
                'product_shop_channel_updated_by' => $updatedBy,
                'fk_channel_category' => $channelCategoryId,
            ]);

            $productId = $assocItemId_productId->get($itemId);
            if ($productId) {
                Models\ModelBusiness\ProductShopChannel::updatedById($productId, $recordCollecttion->toArray());
            } else {
                $recordCollecttion->put('fk_shop_channel', $this->shopChannelId);
                $recordCollecttion->put('product_shop_channel_seller_sku', $itemId);
                $recordCollecttion->put('product_shop_channel_item_id', $itemId);
                $recordCollecttion->put('product_shop_channel_created_at', $createdAt);
                $recordCollecttion->put('product_shop_channel_created_by', $createdBy);

                $recordInsertCollection->push($recordCollecttion);
            }
        }
        
        if ($recordInsertCollection->count()) {
            Models\ModelBusiness\ProductShopChannel::batchInsert($recordInsertCollection->toArray());
        }
    }
}