<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 26/09/2019
 * Time: 15:27
 */

namespace App\Jobs;


use App\Library;
use App\Models;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;


class RawCategory implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode($this->message, true);
        if ( ! $data) return;

        $shopId = intval($data['shopId']);
        $channelId = intval($data['channelId']);

        Library\Common::setTimezoneByChannelId($channelId);

        $factoryManager = new Library\Factory\Channel\Manager($shopId, $channelId);
        $categoryArray = $factoryManager->pullAllCategory();

        foreach ($categoryArray as $item) {
            /** @var Library\DataStructure\ChannelCategory $item */
            Models\Mongo\Category::save($shopId, $channelId, $item);
        }

        return;
    }
}