<?php

namespace App\Repository\RepositoryBusiness\DTO;

class DTOShopAds
{
    const PAUSE = 2;
    const ONGOING = 1;

    protected $identify;
    protected $status;
    protected $dailyQuota;
    protected $totalQuota;
    protected $timeLineFrom;
    protected $timeLineTo;

    /**
     * @return mixed
     */
    public function getIdentify()
    {
        return $this->identify;
    }

    /**
     * @param mixed $identify
     */
    public function setIdentify($identify)
    {
        $this->identify = $identify;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDailyQuota()
    {
        return $this->dailyQuota;
    }

    /**
     * @param mixed $dailyQuota
     */
    public function setDailyQuota($dailyQuota)
    {
        $this->dailyQuota = $dailyQuota;
    }

    /**
     * @return mixed
     */
    public function getTotalQuota()
    {
        return $this->totalQuota;
    }

    /**
     * @param mixed $totalQuota
     */
    public function setTotalQuota($totalQuota)
    {
        $this->totalQuota = $totalQuota;
    }

    /**
     * @return mixed
     */
    public function getTimeLineFrom()
    {
        return $this->timeLineFrom;
    }

    /**
     * @param mixed $timeLineFrom
     */
    public function setTimeLineFrom($timeLineFrom)
    {
        $this->timeLineFrom = $timeLineFrom;
    }

    /**
     * @return mixed
     */
    public function getTimeLineTo()
    {
        return $this->timeLineTo;
    }

    /**
     * @param mixed $timeLineTo
     */
    public function setTimeLineTo($timeLineTo)
    {
        $this->timeLineTo = $timeLineTo;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }
}