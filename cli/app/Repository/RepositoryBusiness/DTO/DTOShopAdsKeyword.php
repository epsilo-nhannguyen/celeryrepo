<?php

namespace App\Repository\RepositoryBusiness\DTO;

class DTOShopAdsKeyword
{
    protected $identify;
    protected $keywordName;
    protected $status;
    protected $matchType;
    protected $biddingPrice;

    /**
     * @return mixed
     */
    public function getIdentify()
    {
        return $this->identify;
    }

    /**
     * @param mixed $identify
     */
    public function setIdentify($identify)
    {
        $this->identify = $identify;
    }

    /**
     * @return mixed
     */
    public function getKeywordName()
    {
        return $this->keywordName;
    }

    /**
     * @param mixed $keywordName
     */
    public function setKeywordName($keywordName)
    {
        $this->keywordName = $keywordName;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMatchType()
    {
        return $this->matchType;
    }

    /**
     * @param mixed $matchType
     */
    public function setMatchType($matchType)
    {
        $this->matchType = $matchType;
    }

    /**
     * @return mixed
     */
    public function getBiddingPrice()
    {
        return $this->biddingPrice;
    }

    /**
     * @param mixed $biddingPrice
     */
    public function setBiddingPrice($biddingPrice)
    {
        $this->biddingPrice = $biddingPrice;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }

}