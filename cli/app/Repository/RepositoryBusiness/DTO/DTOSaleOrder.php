<?php


namespace App\Repository\RepositoryBusiness\DTO;


class DTOSaleOrder
{
    protected $shopChannelId;

    protected $orderNumber;

    protected $createdAt;

    protected $id;

    protected $customerAddressCity;

    protected $customerPhone;

    protected $voucherAmount;

    protected $voucherCode;

    protected $rawData;

    protected $arrayItem;

    /**
     * @return mixed
     */
    public function getShopChannelId()
    {
        return $this->shopChannelId;
    }

    /**
     * @param mixed $shopChannelId
     */
    public function setShopChannelId($shopChannelId)
    {
        $this->shopChannelId = $shopChannelId;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddressCity()
    {
        return $this->customerAddressCity;
    }

    /**
     * @param mixed $customerAddressCity
     */
    public function setCustomerAddressCity($customerAddressCity)
    {
        $this->customerAddressCity = $customerAddressCity;
    }

    /**
     * @return mixed
     */
    public function getCustomerPhone()
    {
        return $this->customerPhone;
    }

    /**
     * @param mixed $customerPhone
     */
    public function setCustomerPhone($customerPhone)
    {
        $this->customerPhone = $customerPhone;
    }

    /**
     * @return mixed
     */
    public function getVoucherAmount()
    {
        return $this->voucherAmount;
    }

    /**
     * @param mixed $voucherAmount
     */
    public function setVoucherAmount($voucherAmount)
    {
        $this->voucherAmount = $voucherAmount;
    }

    /**
     * @return mixed
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }

    /**
     * @param mixed $voucherCode
     */
    public function setVoucherCode($voucherCode)
    {
        $this->voucherCode = $voucherCode;
    }

    /**
     * @return mixed
     */
    public function getArrayItem()
    {
        return $this->arrayItem;
    }

    /**
     * @param mixed $arrayItem
     */
    public function setArrayItem(array $arrayItem)
    {
        foreach ($arrayItem as $item) {
            if ( ! ($item instanceof DTOSaleOrderItem)) {
                throw new \Exception('param must be instanceof App\Repository\RepositoryBusiness\DTO\DTOSaleOrderItem');
            }
        }
        $this->arrayItem = $arrayItem;
    }

    /**
     * @return mixed
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param mixed $rawData
     */
    public function setRawData($rawData)
    {
        $this->rawData = $rawData;
    }


}