<?php

namespace App\Repository\RepositoryBusiness\Tokopedia\DTO;

use App\Repository\RepositoryBusiness\DTO\DTOResponse;

class KeywordAdsDTO
{
    #region Setting Default Value
    const GENERAL_SEARCH = "11";
    const GENERAL_SEARCH_NAME = "General Search";
    const SPECIFIC_SEARCH = "21";
    const SPECIFIC_SEARCH_NAME = "Specific Search";
    const TOGGLE_ON_KEYWORD = "toggle_on";
    const TOGGLE_OFF_KEYWORD = "toggle_off";
    const IS_POSITIVE = 1;
    const SEPARATE_STATISTIC = true;
    const KEYWORD_SOURCE = "du_keyword_positive";
    const STATUS_ACTIVE = "on";
    const STATUS_DEACTIVATE = "off";
    #endregion

    #region Common Keyword Ads Property
    private $keywordId;
    private $keywordName;
    private $bidPrice;
    private $matchType;
    #endregion

    #region Active/Deactivate Keyword In Ads Group
    private $toggleKeyword;
    private $toggleOnKeyword = self::TOGGLE_ON_KEYWORD;
    private $toggleOffKeyword = self::TOGGLE_OFF_KEYWORD;
    #endregion

    #region Keyword Information
    private $keywordTypeId; // typeId = "11": General Search / typeId = "21": Specific Search
    private $keywordGeneralSearchId = self::GENERAL_SEARCH;
    private $keywordGeneralSearchName = self::GENERAL_SEARCH_NAME;
    private $keywordSpecificSearchId = self::SPECIFIC_SEARCH;
    private $keywordSpecificSearchName = self::SPECIFIC_SEARCH_NAME;
    private $keywordGroupId;
    private $isPositive = self::IS_POSITIVE;
    private $separateStatistic = self::SEPARATE_STATISTIC;
    private $keywordSource = self::KEYWORD_SOURCE;
    private $statusKeywordAds = "";
    private $statusActive = self::STATUS_ACTIVE;
    private $statusDeactivate = self::STATUS_DEACTIVATE;
    private $minBid;
    #endregion

    /**
     * @return mixed
     */
    public function getKeywordId()
    {
        return $this->keywordId;
    }

    /**
     * @param mixed $keywordId
     * @return DTOResponse
     */
    public function setKeywordId($keywordId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!is_string($keywordId) || is_null($keywordId)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required string value');
        }
        $this->keywordId = $keywordId;
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getKeywordName()
    {
        return $this->keywordName;
    }

    /**
     * @param string $keywordName
     * @return DTOResponse
     */
    public function setKeywordName($keywordName)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (preg_match('#[^a-zA-Z0-9- ]#', $keywordName)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Keyword name can not have special characters');
        }
        $this->keywordName = trim(preg_replace('/\s+/', ' ', $keywordName));
        return $responseDTO;
    }

    /**
     * @return int
     */
    public function getBidPrice()
    {
        return $this->bidPrice;
    }

    /**
     * @param int $bidPrice
     * @return DTOResponse
     */
    public function setBidPrice($bidPrice)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!is_int($bidPrice)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required int value');
        }
        if (round($bidPrice) % 50 != 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Bid price must be in multiple of Rp50');
        }
        $this->bidPrice = $bidPrice;
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getMatchType()
    {
        return $this->matchType;
    }

    /**
     * @param boolean $matchType
     * @return DTOResponse
     */
    public function setMatchType($matchType)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!is_bool($matchType)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Match type required bool value (true/false)');
            return $responseDTO;
        }
        $this->matchType = $matchType ? self::GENERAL_SEARCH : self::SPECIFIC_SEARCH;
        return $responseDTO;
    }

    /**
     * @return bool
     */
    public function getToggleKeyword()
    {
        return $this->toggleKeyword;
    }

    /**
     * @param bool $toggleKeyword
     * @return DTOResponse
     */
    public function setToggleKeyword($toggleKeyword)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if (!is_bool($toggleKeyword)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required bool value');
        }
        $this->toggleKeyword = $toggleKeyword;
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getToggleOnKeyword()
    {
        return $this->toggleOnKeyword;
    }

    /**
     * @return string
     */
    public function getToggleOffKeyword()
    {
        return $this->toggleOffKeyword;
    }

    /**
     * @return mixed
     */
    public function getKeywordTypeId()
    {
        return $this->keywordTypeId;
    }

    /**
     * @param mixed $keywordTypeId
     * @return DTOResponse
     */
    public function setKeywordTypeId($keywordTypeId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!is_bool($keywordTypeId)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required string value');
        }

        $this->keywordTypeId = $keywordTypeId;

        return $responseDTO;
    }

    /**
     * @return mixed
     */
    public function getKeywordGroupId()
    {
        return $this->keywordGroupId;
    }

    /**
     * @param mixed $keywordGroupId
     * @return DTOResponse
     */
    public function setKeywordGroupId($keywordGroupId)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!is_bool($keywordGroupId)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required string value');
        }

        $this->keywordGroupId = $keywordGroupId;

        return $responseDTO;
    }

    /**
     * @return mixed
     */
    public function getIsPositive()
    {
        return $this->isPositive;
    }

    /**
     * @param mixed $isPositive
     * @return DTOResponse
     */
    public function setIsPositive($isPositive)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!is_bool($isPositive)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required bool value');
        }

        $this->isPositive = $isPositive;

        return $responseDTO;
    }

    /**
     * @return mixed
     */
    public function getSeparateStatistic()
    {
        return $this->separateStatistic;
    }

    /**
     * @param mixed $separateStatistic
     * @return DTOResponse
     */
    public function setSeparateStatistic($separateStatistic)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!is_bool($separateStatistic)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required bool value');
        }

        $this->separateStatistic = $separateStatistic;

        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getKeywordSource()
    {
        return $this->keywordSource;
    }

    /**
     * @return string
     */
    public function getStatusKeywordAds()
    {
        return $this->statusKeywordAds;
    }

    /**
     * @param string $statusKeywordAds
     * @return DTOResponse
     */
    public function setStatusKeywordAds($statusKeywordAds)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);

        if (!is_bool($statusKeywordAds)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required string value');
        }

        $this->statusKeywordAds = $statusKeywordAds;

        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getStatusActive()
    {
        return $this->statusActive;
    }

    /**
     * @return string
     */
    public function getStatusDeactivate()
    {
        return $this->statusDeactivate;
    }

    /**
     * @return string
     */
    public function getKeywordGeneralSearchId()
    {
        return $this->keywordGeneralSearchId;
    }

    /**
     * @return string
     */
    public function getKeywordSpecificSearchId()
    {
        return $this->keywordSpecificSearchId;
    }

    /**
     * @return string
     */
    public function getKeywordGeneralSearchName()
    {
        return $this->keywordGeneralSearchName;
    }

    /**
     * @return string
     */
    public function getKeywordSpecificSearchName()
    {
        return $this->keywordSpecificSearchName;
    }

    /**
     * @return mixed
     */
    public function getMinBid()
    {
        return $this->minBid;
    }

    /**
     * @param mixed $minBid
     */
    public function setMinBid($minBid)
    {
        $this->minBid = $minBid;
    }
}