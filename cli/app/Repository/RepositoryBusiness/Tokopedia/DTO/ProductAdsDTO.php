<?php

namespace App\Repository\RepositoryBusiness\Tokopedia\DTO;

use App\Repository\RepositoryBusiness\DTO\DTOResponse;

class ProductAdsDTO
{
    #region Setting Default Value
    const DEFAULT_ADS_ID = 0;
    const TOGGLE_ON_SKU = "toggle_on";
    const TOGGLE_OFF_SKU = "toggle_off";
    #endregion

    #region Common Product Ads Property
    private $adsId = self::DEFAULT_ADS_ID;
    private $productId;
    #endregion

    #region Active/Deactivate SKU In Ads Group
    private $toggleSKU;
    private $toggleOnSKU = self::TOGGLE_ON_SKU;
    private $toggleOffSKU = self::TOGGLE_OFF_SKU;
    #endregion

    /**
     * @return string
     */
    public function getAdsId()
    {
        return $this->adsId;
    }

    /**
     * @param string $adsId
     * @return DTOResponse
     */
    public function setAdsId($adsId)
    {
        $responseDTO = new DTOResponse();
        if (!is_string($adsId) || strlen($adsId) == 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Ads id required string value');
            return $responseDTO;
        }
        $this->adsId = $adsId;
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     * @return DTOResponse
     */
    public function setProductId($productId)
    {
        $responseDTO = new DTOResponse();
        if (!is_string($productId) || strlen($productId) == 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Ads id required string value');
            return $responseDTO;
        }
        $this->productId = $productId;
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * @return bool
     */
    public function getToggleSKU()
    {
        return $this->toggleSKU;
    }

    /**
     * @param bool $toggleSKU
     * @return DTOResponse
     */
    public function setToggleSKU($toggleSKU)
    {
        $responseDTO = new DTOResponse();
        $responseDTO->setIsSuccess(true);
        if(!is_bool($toggleSKU)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Required bool value');
            return $responseDTO;
        }
        $this->toggleSKU = $toggleSKU;
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getToggleOnSKU()
    {
        return $this->toggleOnSKU;
    }

    /**
     * @return string
     */
    public function getToggleOffSKU()
    {
        return $this->toggleOffSKU;
    }
}