<?php

namespace App\Repository\RepositoryBusiness\Tokopedia\DTO;

use App\Repository\RepositoryBusiness\DTO\DTOResponse;

class GroupAdsDTO
{
    #region Setting Default Value
    const MAX_DAILY_BUDGET_VALUE = 10000000;
    const IS_UN_LIMIT_BUDGET = 0; // 0 - Is not limit budget ; 1 - Is daily budget
    const AUTO_ADS_BUDGET_COST = 25000; // Default when inactivate auto ads
    const IS_ACTIVE_ADS_GROUP = true; // Default Active Ads Group
    const GROUP_TYPE = "1";
    const GROUP_SCHEDULE = "0";
    const STICKER_ID = "3";
    const SOURCE_GROUP_ADS = "du_group_product";
    const SUGGESTED_BIG_VALUE = 0;
    const IS_SUGGESTION_BID_BUTTON = "-1";
    const STATUS_ACTIVE = "on";
    const STATUS_DEACTIVATE = "off";
    const TOGGLE_ON_ADS_GROUP = "toggle_on";
    const TOGGLE_OFF_ADS_GROUP = "toggle_off";
    #endregion

    #region Common Group Ads Property
    private $groupId;
    private $name;
    private $costPerClick;
    private $dailyBudgetCost;
    private $isUnlimitBudget = self::IS_UN_LIMIT_BUDGET;
    private $autoAdsBudgetCost = self::AUTO_ADS_BUDGET_COST;
    private $isActiveAdsGroup = self::IS_ACTIVE_ADS_GROUP;
    #endregion

    #region Property Ads Group For Edit
    private $groupType = self::GROUP_TYPE;
    private $groupSchedule = self::GROUP_SCHEDULE;
    private $stickerId = self::STICKER_ID;
    private $sourceGroupAds = self::SOURCE_GROUP_ADS;
    private $suggestedBidValue = self::SUGGESTED_BIG_VALUE;
    private $is_suggestion_bid_button = self::IS_SUGGESTION_BID_BUTTON;
    private $oldPriceBid;
    private $statusAdsGroup = "";
    private $statusActive = self::STATUS_ACTIVE;
    private $statusDeactivate = self::STATUS_DEACTIVATE;
    #endregion

    #region Active/Deactivate Ads Group
    private $toggleAdsGroup;
    private $toggleOnAdsGroup = self::TOGGLE_ON_ADS_GROUP;
    private $toggleOffAdsGroup = self::TOGGLE_OFF_ADS_GROUP;
    #endregion

    /**
     * @return string
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     * @return DTOResponse
     */
    public function setGroupId($groupId)
    {
        $responseDTO = new DTOResponse();
        if (!is_string($groupId) || strlen($groupId) == 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Group is can not be null and must be string value');
            return $responseDTO;
        }
        $this->groupId = $groupId;
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return integer
     */
    public function getCostPerClick()
    {
        return $this->costPerClick;
    }

    /**
     * @param integer $costPerClick
     * @return DTOResponse
     */
    public function setCostPerClick($costPerClick)
    {
        $responseDTO = new DTOResponse();
        if (!is_integer($costPerClick)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Cost Per Click must be a number');
            return $responseDTO;
        }
        if (round($costPerClick) % 50 != 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Bid price must be in multiple of Rp50');
            return $responseDTO;
        }
        $this->costPerClick = $costPerClick;
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * @return integer
     */
    public function getDailyBudgetCost()
    {
        return $this->dailyBudgetCost;
    }

    /**
     * @param integer $dailyBudgetCost
     * @return DTOResponse
     */
    public function setDailyBudgetCost($dailyBudgetCost)
    {
        $responseDTO = new DTOResponse();
        if (!is_integer($dailyBudgetCost)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Budget Cost must be a number');
            return $responseDTO;
        }
        if (round($dailyBudgetCost) % 50 != 0) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Daily budget cost must be in multiple of Rp50');
            return $responseDTO;
        }
        $this->dailyBudgetCost = $dailyBudgetCost;
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * @return boolean
     */
    public function getIsUnlimitBudget()
    {
        return $this->isUnlimitBudget;
    }

    /**
     * @param boolean $isUnlimitBudget
     * @return DTOResponse
     */
    public function setIsUnlimitBudget($isUnlimitBudget)
    {
        $responseDTO = new DTOResponse();
        if (!is_bool($isUnlimitBudget)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Budget type required bool value (true/false)');
            return $responseDTO;
        }
        $this->isUnlimitBudget = $isUnlimitBudget ? 0 : 1;
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * @return int
     */
    public function getAutoAdsBudgetCost()
    {
        return $this->autoAdsBudgetCost;
    }

    /**
     * @param int $autoAdsBudgetCost
     */
    public function setAutoAdsBudgetCost($autoAdsBudgetCost)
    {
        $this->autoAdsBudgetCost = $autoAdsBudgetCost;
    }

    /**
     * @return bool
     */
    public function getIsActiveAdsGroup()
    {
        return $this->isActiveAdsGroup;
    }

    /**
     * @param bool $isActiveAdsGroup
     * @return DTOResponse
     */
    public function setIsActiveAdsGroup($isActiveAdsGroup)
    {
        $responseDTO = new DTOResponse();
        if (!is_bool($isActiveAdsGroup)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Group active type required bool value');
            return $responseDTO;
        }
        $this->isActiveAdsGroup = $isActiveAdsGroup;
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * @param string $groupType
     */
    public function setGroupType($groupType)
    {
        $this->groupType = $groupType;
    }

    /**
     * @return string
     */
    public function getGroupSchedule()
    {
        return $this->groupSchedule;
    }

    /**
     * @param string $groupSchedule
     */
    public function setGroupSchedule($groupSchedule)
    {
        $this->groupSchedule = $groupSchedule;
    }

    /**
     * @return string
     */
    public function getStickerId()
    {
        return $this->stickerId;
    }

    /**
     * @param string $stickerId
     */
    public function setStickerId($stickerId)
    {
        $this->stickerId = $stickerId;
    }

    /**
     * @return string
     */
    public function getSourceGroupAds()
    {
        return $this->sourceGroupAds;
    }

    /**
     * @param string $sourceGroupAds
     */
    public function setSourceGroupAds($sourceGroupAds)
    {
        $this->sourceGroupAds = $sourceGroupAds;
    }

    /**
     * @return int
     */
    public function getSuggestedBidValue()
    {
        return $this->suggestedBidValue;
    }

    /**
     * @param int $suggestedBidValue
     */
    public function setSuggestedBidValue($suggestedBidValue)
    {
        $this->suggestedBidValue = $suggestedBidValue;
    }

    /**
     * @return string
     */
    public function getIsSuggestionBidButton()
    {
        return $this->is_suggestion_bid_button;
    }

    /**
     * @param string $is_suggestion_bid_button
     */
    public function setIsSuggestionBidButton($is_suggestion_bid_button)
    {
        $this->is_suggestion_bid_button = $is_suggestion_bid_button;
    }

    /**
     * @return mixed
     */
    public function getOldPriceBid()
    {
        return $this->oldPriceBid;
    }

    /**
     * @param mixed $oldPriceBid
     */
    public function setOldPriceBid($oldPriceBid)
    {
        $this->oldPriceBid = $oldPriceBid;
    }

    /**
     * @return string
     */
    public function getStatusAdsGroup()
    {
        return $this->statusAdsGroup;
    }

    /**
     * @param string $statusAdsGroup
     */
    public function setStatusAdsGroup($statusAdsGroup)
    {
        $this->statusAdsGroup = $statusAdsGroup;
    }

    /**
     * @return string
     */
    public function getStatusActive()
    {
        return $this->statusActive;
    }

    /**
     * @return string
     */
    public function getStatusDeactivate()
    {
        return $this->statusDeactivate;
    }

    /**
     * @return bool
     */
    public function getToggleAdsGroup()
    {
        return $this->toggleAdsGroup;
    }

    /**
     * @param bool $toggleAdsGroup
     * @return DTOResponse
     */
    public function setToggleAdsGroup($toggleAdsGroup)
    {
        $responseDTO = new DTOResponse();
        if (!is_bool($toggleAdsGroup)) {
            $responseDTO->setIsSuccess(false);
            $responseDTO->setMessage('Group toggle status required bool value');
            return $responseDTO;
        }
        $this->toggleAdsGroup = $toggleAdsGroup;
        $responseDTO->setIsSuccess(true);
        return $responseDTO;
    }

    /**
     * @return string
     */
    public function getToggleOnAdsGroup()
    {
        return $this->toggleOnAdsGroup;
    }

    /**
     * @return string
     */
    public function getToggleOffAdsGroup()
    {
        return $this->toggleOffAdsGroup;
    }
}