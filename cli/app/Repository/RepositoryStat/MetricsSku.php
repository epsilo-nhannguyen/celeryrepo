<?php


namespace App\Repository\RepositoryStat;


use Illuminate\Support\Facades\DB;
use App\Library;


class MetricsSku
{

    /**
     * save data
     * @param string $table
     * @param float $value
     * @param string $time
     * @param int $productId
     * @param string $itemId
     * @param int|null $shopId
     */
    public static function save($table, $value, $time, $productId, $itemId, $shopId = null)
    {
        $tableInfo = self::getByTimeProductId($table, $time, $productId);
        if ( ! $tableInfo) {
            self::insert($table, $value, $time, $productId, $itemId, $shopId);
        } else {
            self::updateById($table, $tableInfo->id, $value);
        }
    }

    /**
     * get By Time Product Id
     * @param string $table
     * @param string $time
     * @param int $productId
     * @return mixed
     */
    public static function getByTimeProductId($table, $time, $productId)
    {
        $model = DB::connection('master_stat_datasource')->table($table)
            ->where('time', $time)
            ->where('product_id', $productId)
        ;

        return $model->get()->first();
    }

    /**
     * insert data
     * @param string $table
     * @param float $value
     * @param string $time
     * @param int $productId
     * @param string $itemId
     * @param int|null $shopId
     * @return bool
     */
    public static function insert($table, $value, $time, $productId, $itemId, $shopId = null)
    {
        return DB::connection('master_stat_datasource')->table($table)->insert([
            'value' => $value,
            'time' => $time,
            'product_id' => $productId,
            'item_id' => $itemId,
            'shop_id' => $shopId,
            'created_at' => Library\Common::getCurrentTimestamp()
        ]);
    }

    /**
     * update By Id
     * @param string $table
     * @param int $id
     * @param float $value
     * @return int
     */
    public static function updateById($table, $id, $value)
    {
        return DB::connection('master_stat_datasource')->table($table)
            ->where('id', $id)
            ->update([
                'value' => $value,
                'created_at' => Library\Common::getCurrentTimestamp()
            ])
        ;
    }

}