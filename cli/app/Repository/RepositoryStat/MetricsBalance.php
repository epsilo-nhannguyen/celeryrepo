<?php


namespace App\Repository\RepositoryStat;


class MetricsBalance extends Metrics
{

    /**
     * @var string
     */
    const METRIC_AVERAGE_LOCAL = '_2_2_211_0245';
    const METRIC_AVERAGE_USD = '_2_2_211_0246';
    const METRIC_AVERAGE_L7D = '_2_2_212_0247';
    const METRIC_AVERAGE_L14D = '_2_2_212_0248';
    const METRIC_AVERAGE_L30D = '_2_2_212_0249';
    const METRIC_MIN_LOCAL = '_2_2_211_0250';
    const METRIC_MIN_USD = '_2_2_211_0251';
    const METRIC_MIN_L7D = '_2_2_212_0252';
    const METRIC_MIN_L14D = '_2_2_212_0253';
    const METRIC_MIN_L30D = '_2_2_212_0254';
    const METRIC_MAX_LOCAL = '_2_2_211_0255';
    const METRIC_MAX_USD = '_2_2_211_0256';
    const METRIC_MAX_L7D = '_2_2_212_0257';
    const METRIC_MAX_L14D = '_2_2_212_0258';
    const METRIC_MAX_L30D = '_2_2_212_0259';

}