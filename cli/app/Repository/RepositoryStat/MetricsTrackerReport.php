<?php


namespace App\Repository\RepositoryStat;


class MetricsTrackerReport extends Metrics
{
    # begin have daily, weekly, monthly
    /**
     * @var string
     */
    const METRIC_TOTAL_SHOP = '_1_1_223_0060';

    const METRIC_TOTAL_GMV_SO_ALL_SHOP_USD = '_1_1_211_0057';
    const METRIC_TOTAL_ITEM_SOLD_SO_ALL_SHOP = '_1_1_213_0058';
    const METRIC_TOTAL_ORDER_SO_ALL_SHOP = '_1_1_213_0059';
    const METRIC_TOTAL_GMV_ADS_ALL_SHOP_USD = '_1_2_211_0260';
    const METRIC_TOTAL_ITEM_SOLD_ADS_ALL_SHOP = '_1_2_213_0261';
    const METRIC_TOTAL_ORDER_ADS_ALL_SHOP = '_1_2_213_0262';

    const METRIC_TOTAL_COST_ADS_LOCAL = '_1_2_211_0131';
    const METRIC_TOTAL_COST_ADS_USD = '_1_2_211_0132';
    const METRIC_TOTAL_GMV_SO_USD = '_1_2_211_0047';
    const METRIC_TOTAL_GMV_ADS_USD = '_1_2_211_0062';
    const METRIC_TOTAL_ITEM_SOLD_SO = '_1_2_213_0116';
    const METRIC_TOTAL_ITEM_SOLD_ADS = '_1_2_213_0121';
    const METRIC_TOTAL_ORDER_SO = '_1_2_213_0148';
    const METRIC_TOTAL_ORDER_ADS = '_1_2_213_0153';

    const METRIC_TOTAL_IMPRESSION_ADS = '_2_2_513_0278';
    const METRIC_TOTAL_CLICK_ADS = '_2_2_513_0284';
    const METRIC_CPC_ADS = '_2_2_511_0282';
    const METRIC_PERCENT_CTR_ADS = '_2_2_512_0286';
    const METRIC_PERCENT_CR_ADS = '_2_2_512_0288';
    const METRIC_PERCENT_CIR_ADS = '_2_2_512_0291';

    const METRIC_AVERAGE_POSITION_SHOP = '_2_2_513_0280';
    const METRIC_NUMBER_SKU_RUN_ADS = '_2_2_513_0295';
    const METRIC_NUMBER_SKU_KEYWORD_RUN_ADS = '_2_2_513_0296';
    const METRIC_NUMBER_SKU_KEYWORD_HAVE_COST_NO_SALE = '_2_2_513_0297';
    const METRIC_TOTAL_WASTED_COST_USD = '_2_2_511_0298';

    const METRIC_NUMBER_SELLING_SKU = '_2_2_513_0291';
    const METRIC_NUMBER_SELLING_KEYWORD = '_2_2_513_0292';
    const METRIC_NUMBER_SELLING_SKU_KEYWORD = '_2_2_513_0293';
    const METRIC_ATR = '_2_1_513_0069';
    const METRIC_GMV_LOSS_FROM_OOB = '_2_2_511_0300';

    # end have daily, weekly, monthly

}