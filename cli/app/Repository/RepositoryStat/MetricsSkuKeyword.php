<?php


namespace App\Repository\RepositoryStat;


use Illuminate\Support\Facades\DB;
use App\Library;


class MetricsSkuKeyword
{

    /**
     * save data
     * @param string $table
     * @param float $value
     * @param string $time
     * @param int $productKeywordId
     * @param int $productId
     * @param string $itemId
     * @param int $keywordId
     * @param string $keywordName
     * @param int|null $shopId
     */
    public static function save($table, $value, $time, $productKeywordId, $productId, $itemId, $keywordId, $keywordName, $shopId = null)
    {
        $tableInfo = self::getByTimeProductKeywordId($table, $time, $productKeywordId);
        if ( ! $tableInfo) {
            self::insert($table, $value, $time, $productKeywordId, $productId, $itemId, $keywordId, $keywordName, $shopId);
        } else {
            self::updateById($table, $tableInfo->id, $value);
        }
    }

    /**
     * get By Time Product Keyword Id
     * @param string $table
     * @param string $time
     * @param int $productKeywordId
     * @return mixed
     */
    public static function getByTimeProductKeywordId($table, $time, $productKeywordId)
    {
        $model = DB::connection('master_stat_datasource')->table($table)
            ->where('time', $time)
            ->where('product_keyword_id', $productKeywordId)
        ;

        return $model->get()->first();
    }

    /**
     * insert data
     * @param string $table
     * @param float $value
     * @param string $time
     * @param int $productKeywordId
     * @param int $productId
     * @param string $itemId
     * @param int $keywordId
     * @param string $keywordName
     * @param int|null $shopId
     * @return bool
     */
    public static function insert($table, $value, $time, $productKeywordId, $productId, $itemId, $keywordId, $keywordName, $shopId = null)
    {
        return DB::connection('master_stat_datasource')->table($table)->insert([
            'value' => $value,
            'time' => $time,
            'product_keyword_id' => $productKeywordId,
            'product_id' => $productId,
            'item_id' => $itemId,
            'keyword_id' => $keywordId,
            'keyword_name' => $keywordName,
            'shop_id' => $shopId,
            'created_at' => Library\Common::getCurrentTimestamp()
        ]);
    }

    /**
     * update By Id
     * @param string $table
     * @param int $id
     * @param float $value
     * @return int
     */
    public static function updateById($table, $id, $value)
    {
        return DB::connection('master_stat_datasource')->table($table)
            ->where('id', $id)
            ->update([
                'value' => $value,
                'created_at' => Library\Common::getCurrentTimestamp()
            ])
        ;
    }

}