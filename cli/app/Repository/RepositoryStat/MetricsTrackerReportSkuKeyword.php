<?php


namespace App\Repository\RepositoryStat;


class MetricsTrackerReportSkuKeyword extends MetricsSkuKeyword
{
    # begin have daily, weekly, monthly
    /**
     * @var string
     */
    const METRIC_TOTAL_IMPRESSION_ADS = '_2_2_513_0355';
    const METRIC_TOTAL_CLICK_ADS = '_2_2_513_0361';
    const METRIC_TOTAL_COST_ADS_USD = '_2_2_511_0367';
    const METRIC_TOTAL_ITEM_SOLD_ADS = '_2_2_513_0351';
    const METRIC_TOTAL_GMV_ADS_USD = '_2_2_511_0353';
    const METRIC_AVERAGE_POSITION = '_2_2_51ID_0357';

    # end have daily, weekly, monthly

}