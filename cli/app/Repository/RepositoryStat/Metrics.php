<?php


namespace App\Repository\RepositoryStat;


use Illuminate\Support\Facades\DB;
use App\Library;


class Metrics
{

    /**
     * save data
     * @param string $table
     * @param float $value
     * @param string $time
     * @param int|null $shopId
     */
    public static function save($table, $value, $time, $shopId = null)
    {
        $tableInfo = self::getByTimeShopId($table, $time, $shopId);
        if ( ! $tableInfo) {
            self::insert($table, $value, $time, $shopId);
        } else {
            self::updateById($table, $tableInfo->id, $value);
        }
    }

    /**
     * get By Time Shop Id
     * @param string $table
     * @param string $time
     * @param int|null $shopId
     * @return mixed
     */
    public static function getByTimeShopId($table, $time, $shopId = null)
    {
        $model = DB::connection('master_stat')->table($table)
            ->where('time', $time)
            ->when($shopId, function ($query) use ($shopId) {
                return $query->where('shop_id', $shopId);
            }, function ($query) {
                return $query->whereNull('shop_id');
            })
        ;

        return $model->get()->first();
    }

    /**
     * insert data
     * @param string $table
     * @param float $value
     * @param string $time
     * @param int|null $shopId
     * @return bool
     */
    public static function insert($table, $value, $time, $shopId = null)
    {
        return DB::connection('master_stat')->table($table)->insert([
            'value' => $value,
            'time' => $time,
            'shop_id' => $shopId,
            'created_at' => Library\Common::getCurrentTimestamp()
        ]);
    }

    /**
     * update By Id
     * @param string $table
     * @param int $id
     * @param float $value
     * @return int
     */
    public static function updateById($table, $id, $value)
    {
        return DB::connection('master_stat')->table($table)
            ->where('id', $id)
            ->update([
                'value' => $value,
                'created_at' => Library\Common::getCurrentTimestamp()
            ]);
    }

}