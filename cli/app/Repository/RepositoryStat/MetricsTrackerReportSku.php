<?php


namespace App\Repository\RepositoryStat;


class MetricsTrackerReportSku extends MetricsSku
{
    # begin have daily, weekly, monthly
    /**
     * @var string
     */
    const METRIC_RANKING_ITEM_SOLD = '_2_2_513_0305';

    # end have daily, weekly, monthly

}